import { HeaderComponent } from "./../pages/header/header.component";
import { FormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BreadcrumbsComponent } from "./../pages/breadcrumbs/breadcrumbs.component";
import { TranslateModule } from "@ngx-translate/core";
import { ProposalComponent } from "../pages/proposal/proposal.component";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatIconModule } from "@angular/material/icon";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSelectModule } from "@angular/material/select";
import { FailedPaymentComponent } from "../pages/failed-payment/failed-payment.component";
import { IasDashComponent } from "../pages/ias-dash/ias-dash.component";
import { MybreakinsComponent } from "../pages/mybreakins/mybreakins.component";
import { MypoliciesComponent } from "../pages/mypolicies/mypolicies.component";
import { PendingPaymentComponent } from "../pages/pending-payment/pending-payment.component";
import { QcCasesComponent } from "../pages/qc-cases/qc-cases.component";
import { SavedQuotesComponent } from "../pages/saved-quotes/saved-quotes.component";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { NgxPaginationModule } from "ngx-pagination";
import { SwitchLanguageService } from "../services/switch-language.service";

@NgModule({
  declarations: [
    BreadcrumbsComponent,
    HeaderComponent,
    ProposalComponent,
    PendingPaymentComponent,
    MypoliciesComponent,
    IasDashComponent,
    MybreakinsComponent,
    FailedPaymentComponent,
    SavedQuotesComponent,
    QcCasesComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule.forRoot(),
    FormsModule,
    MatExpansionModule,
    MatIconModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatDatepickerModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    MatPaginatorModule,
  ],
  exports: [
    BreadcrumbsComponent,
    HeaderComponent,
    ProposalComponent,
    PendingPaymentComponent,
    MypoliciesComponent,
    IasDashComponent,
    MybreakinsComponent,
    FailedPaymentComponent,
    SavedQuotesComponent,
    QcCasesComponent,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    MatExpansionModule,
    MatIconModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatDatepickerModule,
    FormsModule,
  ],
  providers: [SwitchLanguageService],
})
export class SharedModule {}
