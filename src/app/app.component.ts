import { Component } from '@angular/core';
import {Router, Event as NavigationEvent, NavigationEnd } from "@angular/router";
import { filter } from "rxjs/operators";
import { CommonService } from './services/common.service';

declare let gtag: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  auth:any;
  constructor(public router: Router,public cs: CommonService) { 
    this.auth =  JSON.parse(localStorage.getItem('AuthToken'));

    this.router.events.subscribe(event => {
      if(event instanceof NavigationEnd){
        var url = event.urlAfterRedirects;
         
          if(url.indexOf('?')!=-1){
            url = url.substring(0,url.indexOf('?'));
          }
          (<any>window).ga('set', 'page', url);
          (<any>window).ga('send', 'pageview');
  
       }
    })

    console.clear();

    // this.router.events.subscribe(event => {
    //   if(event instanceof NavigationEnd){
    //       var url = event.urlAfterRedirects;
    //       if(url.indexOf('?')!=-1){
    //         url = url.substring(0,url.indexOf('?'));
    //       }

    //       gtag('config','UA-178128082-1', 
    //             {
    //               'page_path': url
    //             }
    //            );
    //    }
    // })
 

  }
  title = 'nysa';
  
  onActive(){
    window.scroll(0,0);
  }

}
