import { Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Injectable({
  providedIn: "root",
})
export class SwitchLanguageService {
  constructor(public translate: TranslateService) {
    translate.addLangs([
      "English",
      "Hindi",
      "Tamil",
      // 'Marathi' ,
      // 'Bengali',
      "Gujarati",
      "Oriya",
      "Telugu",
    ]);
    // translate.setDefaultLang('English');
  }
}
