import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from "@angular/common/http";
import { commonData } from "../commonData/commonData";
import { BehaviorSubject, Observable, Subject, throwError } from "rxjs";
import { catchError, timeout } from "rxjs/operators";
import { Router } from "@angular/router";
import * as CryptoJS from "crypto-js";
import * as moment from "moment";
import { Guid } from "guid-typescript";
import swal from "sweetalert";
import { NotifierService } from "angular-notifier";
import "rxjs/add/observable/merge";
import "rxjs/add/operator/map";
declare var $: any;
declare var jsPDF: any;
declare var jspdf: any;
declare var html2canvas: any;
import { stringify } from "zipson";
@Injectable({
  providedIn: "root",
})
export class CommonService {
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json; charset=utf-8",
      authorization: "",
      "cache-control": "no-cache",
    }),
  };

  payAuthToken: any;
  loaderStatus = false;
  headerStatus = false;
  authToken: any;
  currentDate: any;
  custToken: any;
  byte = [];
  skeletonloader = false;
  isPlutusEnable = false;
  showProposal = false;
  // crypto
  keySize = 256;
  ivSize = 128;
  saltSize = 256;
  iterations = 1000;
  ip: any;
  phauthToken: any;
  isBack = false;
  isPdfModal: boolean = false;
  isShowBackButton = false;
  isFromRenewalModify = false;
  styleChanges = false;
  data$: BehaviorSubject<any>;
  IsPOSTransaction_flag: boolean = false; //digitalPOS variables

  constructor(
    public http: HttpClient,
    private router: Router,
    private notifier: NotifierService
  ) {
    this.currentDate = new Date();
    this.notifier = notifier;

    this.data$ = new BehaviorSubject({
      flag: false,
    });

    /**
     * Setting the flag for POS Transaction
     */
    if (localStorage.getItem("IsPOSTransaction")) {
      this.IsPOSTransaction_flag = JSON.parse(
        localStorage.getItem("IsPOSTransaction")
      );
    }
  }

  private laguage = new BehaviorSubject("English");

  getLanguage(): Observable<any> {
    return this.laguage.asObservable();
  }

  setLanguage(data) {
    this.laguage.next(data);
  }

  public showNotification(type: string, message: string): void {
    this.notifier.notify(type, message);
  }

  capturescreen() {
    let DATA = document.getElementById("premium_breakup");
    html2canvas(DATA).then((canvas) => {
      const FILEURI = canvas.toDataURL("image/png");
      var imgWidth = 210;
      var pageHeight = 295;

      var imgHeight = (canvas.height * imgWidth) / canvas.width;
      var heightLeft = imgHeight;

      var doc = new jspdf("p", "mm");
      var position = 0;

      doc.addImage(FILEURI, "PNG", 0, position, imgWidth, imgHeight);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(FILEURI, "PNG", 0, position, imgWidth, imgHeight);
        heightLeft -= pageHeight;
      }
      return doc;
      // doc.save('Nysa.pdf');
    });
  }

  convertToPdf(elem, img) {
    var doc = new jsPDF("p", "pt", "a4");
    var data = doc.autoTableHtmlToJson(elem, img);
    var imgData =
      "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD//gAfQ29tcHJlc3NlZCBieSBqcGVnLXJlY29tcHJlc3P/2wCEAAQEBAQEBAQEBAQGBgUGBggHBwcHCAwJCQkJCQwTDA4MDA4MExEUEA8QFBEeFxUVFx4iHRsdIiolJSo0MjRERFwBBAQEBAQEBAQEBAYGBQYGCAcHBwcIDAkJCQkJDBMMDgwMDgwTERQQDxAUER4XFRUXHiIdGx0iKiUlKjQyNEREXP/CABEIAGABaAMBIgACEQEDEQH/xAAdAAEAAgIDAQEAAAAAAAAAAAAABwgFBgEECQID/9oACAEBAAAAAL/AAAAAAAAAAAHz9AAAAAAA0zzwje49wwAAA6UC65ZTLVHlPXINzlsKZXdqXqVgMHl6zRT8egiStoBHsZ2OBF+Cm0af5+29qfKd0fJO08Mek1cMJBuNtXJ8fU5unSOy0rZOAZ+7Whd2Le3lu3FsrSFEe/QbJmB3afjo+ce37ZWiyFjfOSweGn+EcZvtOPRuEJLod661ipB6mRvG8+QVbalHYys/1Xl+I5ggiVZjo5d/UZ/Ky1nu/Xqu/od3NDmSuX6z9Bk4aNFW/STC1m+IX02Sujp8ja/n9dyO+Rd+WQ1jF7xI0Q4SWpBcUXgK81RJ/t3F2y4rmQNG3oHGj/G98mk7p9AABxT+LLywbZvpUHmDa48kWKJY1PKbH0/zsDVbjoTRF2fjbeN1lfDAAzv6ms69I3KG+hGmU13PZDAaPZOLsdJc80pkOMNx46WxxJbTGx4ACweVAHBycadnMryOAcgAAAAAAAAAf//EABsBAQACAwEBAAAAAAAAAAAAAAADBAECBQYH/9oACAECEAAAAAAAAEWvQ2t19Zo/SbeTrlqqWazkQQfS7PJ6e3LvXPNXa3RhnrTbU+Pj516ul65tmWHXSxjXOkrMe1V8u9v1OdnOJcTVZNJPN88O36oA12AAf//EABwBAQACAwEBAQAAAAAAAAAAAAADBAIFBgEHCP/aAAgBAxAAAAAADz0AF2XSY1Z84c+ex6mwVbRWsum67afnOttdb5sadToK0+vmhnixt7h9y5SL58hiwlmyqSeMonkmNp9N4yj28eElDLXb6pYpwXQ1HPgE8AAD/8QAMBAAAgICAgEDAwAKAwEAAAAABQYEBwEDAAIIERYXEBUgEhMUGCExNDdAUDA2OGD/2gAIAQEAAQgA/wDhcSNHbvnX1/1Vhv4euAGwuScrdd3SVt7zusmR07429KAn2zk3EiFP8cmVHiIe2eUPeRa1DkZiL8Tya6dJOdBZZZQzSK0lwnLivRoTnOSuLNHOzY8LxE+zv3katqkrcKBSPJ2xt+e+2EveVZ+PJ1dWM07wIqBOdhYryMtQqTHC47C2hEwL92ZGHysIbN3fSr/vN2Vo29cyalviFYBDC6XvK5GFBOCAy1Q9kONhbD8xj8hGncw2GQg9QK2XZCUAWLPLptYIbRR5bmy4aBSGIly22Uq/eA6QK8ZpTkmhWOV+VxzJoqtmieO8ZWA0eX2KSe/K7yRIRWjGRF+OpooZr7M81+Dw8CEcTtIkXR7PvJHtNL1AngFNOgscq0WKA0uxgsL8azcrQylAOdmzpq17NndvM9mJpPm+zodmVfSigpj6TQI7+340FIoYVCh4HxJ/i8nTZ82Z1vCPoruogKGN8fAeDFli92y5nyU7uRDt0oSrhANYHtBOw61BWMIjjiKf4+LSawj2SDcbD7lsZlndPHkB0C1oNkZtsfIG2S4x5HjQc19lkyuYO/aiEfoAt5miQIGisIIvy3/rEnlE/wBqlLlm2GOrdfyWlLEm63Bbw04qe4ZLjOJqLTquO0y1hmUVdYrUteqGGBGeD7VfIxUw/djVh6rIoJqMZ8eyT12HHQiYHu5iXnf2JZjSaseQ0R1tNYrZsurmwYMd7xsM8gAAJVch3nczDqh7wVoXzJXDWtPUbgn2gOreZGbvGH+2vrn6mzEQCNkz5NhuM5xYJMyRX9Jn3HppJEbareGjqMHYP541id+5pKGeXKbwvVs0zMIgLYyuK2E1+VkCdpPK03t442EtpZE/BY36/U4AEmYXEe4bkeWMcAHeT5/cQd4IPPjCvd/bjme6TdG+LMlxpSTc1db1MNiXYnkmU+89YtfIL8/70Bpf3eJHkmisaL0CDIoIOMFRfJGrJhrpqdwMTdIjSo+6LW+srISBnRpJbK/FPSmAEeW/9Uk8ozv16VUpevlnpkZkpErIPbF7AhXeEm4zM8ly+8fU+MfvEt3r5ZYxiYkYw/8Ap8UsvXlff+fLQ54t5j5QyPXT5M9OpCxl4eNK3CxznLpWqH5Dj2iAWWuzT5Pf9FS+U3H0RqxTumjHfEG9Ouwpd/eHqqlrzK8YcZ7Vr+F5tOzprkDYqOWVwhXuVZqwsPrYQ0hLx5HsXeayDl3VHj7pe/VH0VCn9VNTj6tlpVrtsoPBDcrmgIleM+lky1KYBwFbAx8l4mBN+7OwSN8UVqLs69zCokrSTBzAW23xp7NbEYY5qEnRUJWHLcd8olKd5u4pux4jxP0/4rXjfXwHdomTXtIy6qUhWjKfjNBV2UOfkczj1/hlz8fkJvlSJ+gT45GwcebAgJFOpSLuxPHW3UGLPyD74gUc8hIEcaMzV8Yyi9E52EVxYK0F7rAevKrX660ytkFUpvsr2IXfuW1UHyfuC7uHVnJ5TJLfF6j9QBBaEXYApKQk46SEVYpocJZdzoyH6OORHzY/ojjQbc999RhjeKmMPqkvgzaevZVlgKvZtOgILwTyxhi9K2M2hOgdvqlFkVyq5ASvpn+WeWPs2lNR6VjnjyK7jkTcR3M02W1Nxufqp6r8bZPQsTxjHXGMYxbyl7u9m4c3QOkC9ZY0w2aAWQoIsRZrKX1aaCGEcZ/hjPC9hgxLWMTt35ds/o9c54kWIBd9hTUIbrGX08gKFkeufXrjP4Oz+DRI8GQaj7sb4+ndj/Bzj1xnHGQb2HGSkDdvr79vm69QoYv/AGJOjrsFIqbd22+sqDCjDouqLEPFNYcOUK7ftM+CoCbQ7W9My6NVbqUAo+HNlsCEEGX++vd2yti7Wr83zHdjR24JuaW+2mplVqXsVobd7MPYWGx7DiYPEdp+13DVWCy7j3yzycAYnRFJ5sxgElgSIDQLFaN7/ORGLP8ALPKvkx1y0bQGyEfTOfHFltOWZtGxRI/ebksbkzakISfBCLPcYLstgJzJZDUbeMIVdl+zQ32yqKLSRs1laGkqATqWsIs8QTOg4SMjBPTXmd7yWMc95LGOe8ljHPeSxjnvJYxz3ksY57yWMc95LGOe8ljHPeSxjnvJYxz3kscGk4BHR+0wfq0Jwdl69O8hbriCAnfcdv6OMfz+l6bC/dH3jA5hL39KNys9KdCMsxw7sjLWUI50P2LYpqtzbQh6GQwYTllsBAn2xTFfBzCVULIU4EUGwRSZruJLhZclJBCgL0pR9FNTQfShV+cwz/exxvAdBlpsJZwptX/U95zLL5Y6m4brPO6V+1FEyDrYAuKR5Y7sUpICgLFEkOllBu5mu1SfPtgqeJKfdgRLIct02pxLIZbXlyYg4HUvwzglkrFZ9urmv9fYasxMU2D9u+MG/nxg38+MG/nxg38+MG/nxg38+MG/nxg38+MG/nxg38+MG/nxg38WBGAgSAP7f8Xpjnpjnpj6emPp6Y4/p8pzAdgepWXIaqBGA4vpj6+mPp6Y56Y56Y56Y56Y56Y/33//xABJEAACAgECAwUCBg8FCAMAAAABAgMEEQAFEhMhBhQxQVEykxAVICJxgQcjQlJUYWJjcnOCoaKzwyQzQFCRFjBDRVNgdJKxstL/2gAIAQEACT8A/wCxZkLjxUMM/wCVkyTOTHWroQHml9B6D1Ot3mq0ycJSqOYoVX0OOr/SdTyLIDkMHIOfp1NbfsxJSFwyXw0gaNx9r7vJ6n/EXYataIZeWVwiqPpOttt7tLnAdRyYj9GQWOuyU0MWcOYp+KRf2XVdW1sV5Og8ijDxVh5EfA1IwVa0PPM0RkPOkHHo1uA2zXqCCIx9IxlmOSdVjvG4REpKyPwVomHkX8zra9qggHpBLJ/qS+tgpT1SQJHp8cUgH0OXB1OktYbY9yu5HRiUygP16O2Ga3ZiroBVPtSsF+/1uMdeFAFzj50khHsxqOpJ12agSEHCS32Luf2IyNUNoK+Ub1ZV/qa21aG7MheHlOWhn4OpA8wRrugleo1i0Z4jJ7TYQDBGmqGnSEMcXIhKEyyZJySTqQmns4FKFfIOBmU6qO81yUQxOykR8R9WwQBrbpqdtPuJVxkffKfBlPqNWZIhZs7fBMEYjjjNSRijeoyNbTWud/SyX5zspQxcGMY/S1XjrzXY2d44ySq4YrgE/LtzVrcVZTHNC5R0JceBGt3t35YtwVI3tTNKVXljoC3y7s9O1EK3KmgcxyJmwgOCutxs3bR3KwnNsStK+BjplvkycUmCIIFPz5X9Bq0RCpPIqoSIYV/EPM+p0a5u3qq27FyUjEUbjiCAn2Qo0o7mzJFE4XHMESheP69SZq2qneAh8BLCwGfrDaYKqqWZj0AA0xbvt+aZf0C3zR9Q0xg3beapM7g4kiSQc2b68uF0jvtNCLvVsA44+uEjJ/KOtuqwU1XgEUcSqgX0wBjW87lAJ53l5UPJEcfGc8KZTVqWSKzaEIeYjmNDGxnbUfFDtsM15/QFBwp/E2pn+KqEr1KUWfmcMZw0n0udUorG87nELCvMgbu8DdUWPPgSOpOnet3ecSpPAqCUYBBXLA9DnW8blNYplykcxj4CWUr14VGjmGGx3OH9CsOX+8gnSYn3SWW9J9DngT+FdKQz7lLOufNJ/tin/RtdpKaX3meWjUlUmWAsvVgGwHXzwuuzVYwHpFu0QJpsf1nt1nP4zj8eo446MHaGrFWSM5QRLVlC41/07/8AS1+DyfzG1D3izM/JqVg3CZZCM/Uq+Z1v+2bQ9lOdR280OYrxn2TI7tlQ+qEdLtLQ5nGiZ5cwibD49GXUW2SONyu1KrzQsBDHBIftjkHyUafb95265C0iGqnJPoQpwMEafZq9AIll9rSEvNFXk8C5Oq4r3IYxWtwr1USoyHK/iIOqFBAbK2bO47gXMMeUCrEqR4y51tlSCeSaOKG/TLLCeb7DkP8AcNraa1WkKwnsb1fR3gGfuI1XGX0m27ntNrDizViaJ2izhivXoy6euZLdzlPzo+MFOAtrYA9VDHBPYgoyTo8vmSfBdUIb29B44ZpZcmKKWTwiVVxxPqLa79PcuRHNNQWSN6UolWQcYbIdDjGj0G6WvkNhIlyceLN5KNTEwRsUhT7lQPTTHbNofqsrrmaYfm012l3Wav3tIRRtTh4SWBYsiqFAPwKeRUpcnPq8rDUvBPLUNSL9OweVpOIWb8IkHpEp4pD9Sg6Qii23vBH6CVHy2rK1E3FIGhtP7AaHiHA58s8et3r7pvUsZWslc82JHP3cjeGBq7UIlkBnk7mnDDAPbc6lzFtVFMj8/Y+e37guo8T2iKVd/wBUnEf3vpGSeKZ45VboQ6tgg67RU6E9ajBDPWsNwSI0SAEKD7Q1PENtgTD2J64Yzyeqh/BRqWLuUFOR9tiECxGUop+f9DHAGiXtXrSRg+skz4/+ToYgp1Yq0Q/JiUKNVmmtVYeVfgQZZ4U6iRfUpqw1exHIpjlVyjRsD0YMOoxrfaW92JIjx2IMSRyRP4IW8H9Cddp7Ecybosy7FUxPVSfgdQ35j2jlQdeHBf8A6WvweT+Y2gTSHfEJ8g5MeipgelCYyvs8soOHH1a6wx3tyeUr4cAQof4teVnef5+vS5/T0P8AkEv8rX4af5cWivPG7Tc73aY0ObfO2wQlE6uZJJ34BraqVjcoAYbN3cHYQrJEuZCFTBITXaGLc7E1SVkSCsK0MADjIQZJOvw1f5J1EqB6CStgeLyEsx10Efa8NKX/APK0Vw9VAmfOQyLwa8PjS18iUhKkQ4/10vQf+oOtrn3EV0D1KqcIjebPjKW8hrae4GnOIeAS8wEFcgg4XUn2nbq/NkX89P6/QukLyyMFVRpQLNsixMfPr4a3w7dBBaFl8Q83mEKVA8V9ddoWvywwSxxxGsIgrSDHFniOqCWqzEMCejo48GjYdVI12qu1YT4R2IUnI+sFNdpb9xfvIESv/wDvW2R1kYgyP1aWQ+ruep120dJb1hpuAU8iNfBUB4/Ian7wK3MLTFOAyvI5csRpbFDdJf7yxTKgOfV0YEHXbaXg9BSGf/vpLW7TxkMouOOSWH5tANbiNshnMQaRIQ+I4yG4AuV12ne6tCwJxXNURh3QZXJ4z8A6ahm2u/L855qRAjZvVkII19lLcqu2Wuk8FaExcY95qnJZ3L8NusJZR6lOgC6334vG3Cx/wOdzOdw/lL95r7Me41aUIKxQw1iEQe81u9jepEkeX4wfKThy5ZHBYvgqDjX2RohtQVkrPY27m2q8Z8kcSgakmubnbA71es4Mr+eB96ut+Fjv0t1+6934AneZOP2+M5xrfO4HbxNgcjncZlK/lL97q3ye90Gp87g4uHiTh4uHXaJpo94nMveRW4TF81Bjh4jn2Ndsbm3WJIFjui1AlqC0y+D8sleAjW7z792ieUyrZmQRQxNjAKRrnqNdpYNuuTTvO8NuIyIJJRiTBHir67dwSbmg5aRR0ytWKH0Qceu00Md3b5zNJagp4SToUACF9Wu8GhVSvzuDg4+Dzxk41uQ2zdnC944k4opiowGOCCr6+yLFYgpIvc44q/zTKCBxznoXIXW4R3G73LPzY0KDD/Iyxa00v7Cv8Ax8YXZp1/VxgRj966V5pLd6VowOuIw2E+oKBqIPFEQXJGVZvvE/F6nQwANGydwFvunHyxyeb6Zzoy8h5lgVYVDOXYE+BI9NQXRHuyB4Io4Q02Cof54zqG2bm6AGCGGMOw4mCgP1GCSfgjtS7peCGNYYwyIHJGXORjwyfl+Q0lpHolFmE8YQgvnwwT6aWzNdvnEEFWMSP7QUZyRjJOBoYyPkc9hblaOJK6B3JUZPQkaRkEiK3Cwww4hnB/H/AIOPosz4BHQo/Uf6g6scDTyBEicZAZjjAI0+Xrbca6OOhaTgwW+s6ovQpqw5hkGJpseXXUKxQxKFRB4AawIqlWWc/RGpbQLX5O07TGQ+JQfOBP0yKdEvBb5V2UDzScj537KBtR1fi6FIhcLw8bjCGV8HyHDgagrSDYQEje1xd3hMP3bBepIkPQamq2paMUjrNAnAAY2Ax9BDagqyy1DNFDauFu7V1A5KN83JJKqcDRgsHbuAxzwoEyXLDh/hyNb9sOyCi39m2lpYbNqceh4GbDairwWrNt69tXjLoQCyhkyegJTUUc2/9oBBLAjrxhYnGp6f+0E8MTXb9rCV4Cy5JxrcqO7IsJkivU1CrkKHx0+CRYKqd4tEscKscU3Fn6AH1T7zV20SQ7PVdggd41+YAW6DA/eddpuzsFhLYjGxQNHYl5ecZJjY622r8Y3a8Eji1OkMFYSpxFmMjJnXaXaN/rboUWx3BR/Z3ckYDp5rru0U9fi77esLxqhT2gB6LoV5ZdmlDTtWBEUqj7e0hB8OJQARrc9r2jbtrDLNfvlSZpEOMIDoRPd22dYzYhGEmV84OrsNdXJCmVwvER6a3yl75db5S98ut8pe+XW+UvfLrfKXvl1vlL3y63yl75db5S98ut8pe+XW+UvfLrfKXvl1vlL3y6tRzxcRXjjIYZHkCPkBo7EeQs0RAYD0PqNXGtzJnk8ScKpnzx1yfkULVme/Yihda0TyMIgeNieHyOMaqs96vtkU3KRMubCETMBrarddNo2ZKtfnwPHkqgiUIG8Twg62O7HZMMxqwSwOsjmQmTgjBGTgKF12J3e3LvUvHE8UDcXPiySJAeqqxfVGx8fbrVmWpVSMmcGds8RQdR1IwNbXaO9bhzmirCFzOD/cx5T+LVC1DvW5XebZiCMlk1UbgKAeOvsbX69ppkG4bjPVLWJZgMng6FhHpAo2/bIXj/TqgOT9JxovJHtlaPbNsDeyBGmCV+ga7Ibjvmz3YQahqI7jjCqFBKEeGCNdiK2wSSAw01DTGcwk5bjErHHwbbb5G9xw1nsLE/JEcyoHDuBgDK9dVLMtOtOi3kqgmWZcEliF6kFup19j27s2zyTIk1qWtmy+WCO8zDJCjy4tdmtz3jsnWqRJVqUkZ4+JUxggfl67KtstOhDI1SusPBCrkCJQGACscEkka7HbpuMu4SzLWlrxEqQ8vGDxnoEbW2T0btiLkVxNEyBeZ5JxeIQIBr7Gm47v2k70RTk4J+7keGSYiMjWxVNpu3G51mtVZ3APgoJkZzkDVZXrQRN1Zwvz3OqUXvRqlF70apRe9GqUXvRqlF70apRe9GqUXvRqlF70apRe9GqUXvRqlF70apRe9GgAYoxx483PVv3/AO7HwAfIGt6bboZnUzskfG0iL14PEYBOusNOERgkYLt4s5/Gx66HyRofAPgH+ff/xAA1EQACAQMCBAIJAwMFAAAAAAABAgMABBEFEhMhMUFRcQYUFiIwQlZhkhAgMiMzkTVSc4Gx/9oACAECAQE/APiEEdR8WSaKL+44FRXEMxIjfJHarW0mvJeFCBuxnmcChY3LXD20ab5EOG29BR0S/ClgiHHUBhmrbTrm6WV4woEZw244q2sLq7yYY8qOrHkKn0m9t4zKyKyAZJU5xR0+4FoL07REfvzrS7C3trZLmcKZGXdluiitYlT1WMRpG4lyAxGccs5FNazJBHcso4TnCnP7FtWa0kuww2o4Ujvz/YLZjaG73DaJNmO+f11DUeA62kHOdl3E/wCxfHzPardZog91JGX93kWPPzq2kZryN84LP/7WgoI4ru7boBj/AAMmoTIukzXFuuZ5izMV65Jq1ttTSKW4jkMK/MXOCcedITbaK8jn+pPkk/dzWqxyw2Ntb2iMYzgNsGe32r1fU7e0UGYJFIdojJ94lu2K1thb2dpZp9v8KKiEeq6YkQkCuoUHlnDLQDM7xgseHuOFyU6dftVz/oll/wAhpkjt7G2mWNWklZsswzgL2o21tE11KIVOLVZQjcwrGiYRFp0vqsW6Z9r+7ywDip1jtbLUI0jDKLkAA8wMgGpLVWjtrmGBAXhZnVjhFx81LbW7yaU5jjPF3B9owrYqGO0urm5AtEVbZXIUfOfvTScTSHPDVcXI/iMA8v0JwCa0m9N7c6nfEhnN0yjPZE5KKvLgpYRNM4G4ZYnkMCtERryU3+0i3XKw5+c938vCk1GVLeSBJwIeYcDFQalNaBhDclF5ZHbnVxf3MygXFwxUkAAnAzUt5czxJDJJmNMYXAHSotSvoUEcdwwUdAcHFPd3MkiSyTMzqcqT2NXN5LcyIbiXc+MLmoLqSF2MExV167TUt9dShg8pw38sADPnihqE8MKxF14ScwGUEDPnQupsFdwKlt20qCAfECvW7jMxMhJlXa+e4o3ExWFN/KI5QeBo310d+ZchzuYEDBNLfXKuz8TOU2EEArt8MV6/c/0cMo4RJTCgYqK6nhmaeN8Oc5Pjmpb2eaLguVEe7dtVQBn9CMgik9FdbsdYdrCdVsppdzNkclJzgqe9aholtqclo1zJIYoOsIOEfw3U22CEiNQAi4VQK4bIUg2nEoQsfLrTMX2cQsEaRm6dFXpWJHEKyF8Lukz3wOlNJMIkUl92wtnnzz0FFpxKsIYkui8/DHU0jTvPjcQA55c/4irssnBkVSxV+nmKfixgou7fyYkZ5s3l4UzzNKqqWBDhT16Dqay8zKrF8tKcr2CrVs00koZmPzFhz/6Fapruv2t9PBY6G08CEBZMN71e0vpX9OH8Xr2l9K/pw/i9e0vpX9OH8Xr2l9K/pw/i9e0vpX9OH8XrQtU1vULiVNR0sWsKJkMQwLN4DPwtihzJj3iMZ+P/AP/EADURAAICAQQAAgcHAwUBAAAAAAECAwQRAAUSIRMxBhQiQUJhkRAVIzBRVnEgMlIWJDVTc8H/2gAIAQMBAT8A/MyD5H82ptt6+SKlZ5APNvJR/JOr2z7jtyq9usURvJgQV+o1atRVI/FlzxzjrRu11gSw78EYZHLzOhvNIsAWYZ95XrVjcK9ZokcsTIMrxGdWL1atgSyYY/COzqDdak8giV2DnyDDGhuEBtGoORk/jrW5Xp7Fhq8JYIp44X4jraY39Ycu8imPBIBxnvGDpLMTzvXUnxEGSMf0NaVbUdXieToWz7uv6DZUWhV4nkU55+30b2E7vOZZ8rUiPtn/ACP+I1u0m22zU9H6N0QZlCusSFlIHwkjW+1oK3o3ZrHtIoFVC3ZyMAa3xzJLVqr5k5+vWpeB3SGCwcQxBVAby6GrNjbXkirvGJW+EIM4zpgLG7pGg9iEAAfJBrbHiluzz2nXmMleZ14+2z2mKxc5EGfEA6HHWzqZ7dq03z+rHUhk2zcXkMZZWJI92Q2iQESTCjxOPbYDeY6+eq3/ADFz/wAx/wDNBpLF2xEzsscSrgKcZJ9+hYsSrWjMzD/ctEWHRZRoCYy34vWZeMK8k77yRnURezboO8hDGuSSOie9JaKvYrzTOQkoVGX+5s/DprE6x7mgkceHxKcj7QzqaS1Wr1ybLM1hkBY/CPlpU4brGPELfgHzOSO/t2zbIodngonkoaMeIVPFiW7PY1sW3Vv9Tbi9dcQVMhBnPtHrXppvCScdqrvkKwaYj9R5LqTZZDarvJTf1iQK0WQcsPcQNSbK1woXol2blxIHZ4ef01U2yMOwqVcuqliQMkKPM6ip14ZXmjjw7Zyc/rqTbqcrF3gHI+ZHWkqV443iSIKrDBx79VdvEMMz1YCIkIMhHYGehnVmjziiazXzHICULDzA6yNR0q0RVki7XyyScfxnSbWluz+FC7zydYQkE4/jTVIuQPEhgvDIJBx+h16pXxEBGAIzyXHuOhXiDSvx7kGH+ehSrDhiPBQcVIJyBpqVYoE4Yw3MEHvl+udChW/FypPiDD5Y96lrQzRLDImUGMfLGoqcEUgmUMX48csxPX21vTKmu2jxQ4uJFxCY6ZgMZzqnvt2jHdWAgSWm5PIf7gflqnF63erxSP1JKObMfcT2SToXYbSWNzMic6L2Y4QSMkOAI8DUUMdY2vUkgaeGnFAMsPblm7c9nvA0TTqtuEtQVzJKsFMKMcC7AGQgfpqGltsm4WpkSv4PrCw8MKQoQAs3tHybUUW1yUZ9yMEapVtTAR/9gcfhqfkNWIdsr7TzECOz1lIkHDJmc56OeXs+WMa9H0isDc6U06xJNWzyY4A4MDqqKNxktTeCaoLRIjBSY4YR5HkfN/kNQVNuh2+eeVYXV60kw6UkMxwqZJzldBK22xzSwR1xHHRTw5uQMjyzDBbzzgZ1vMG21KLxQwKSTEsMg4ZJAyzZBJIOtu2fZbNSKa5vKwzNktHleu9fcHo3+4F+qa+4PRv9wL9U19wejf7gX6pr7g9G/wBwL9U19wejf7gX6prd9u2ilDG1DcvWZGbBUYwB+px+UbMxrrUL/gq5cLj4j1n8/wD/2Q==";

    doc.addImage(imgData, "JPEG", 40, 40, 200, 80);
    doc.autoTable(data.columns, data.rows, {
      tableLineColor: [189, 195, 199],
      tableLineWidth: 0.75,
      styles: {
        font: "Meta",
        lineColor: [44, 62, 80],
        lineWidth: 0.55,
      },
      headStyles: {
        fillColor: [255, 255, 255],
        fontSize: 0,
        lineWidth: 0,
      },
      bodyStyles: {
        fillColor: [216, 216, 216],
        textColor: 50,
      },
      alternateRowStyles: {
        fillColor: [250, 250, 250],
      },
      startY: 120,

      drawRow: function (row, data) {
        // Colspan
        doc.setFontStyle("bold");
        doc.setFontSize(10);
        if ($(row.raw[0]).hasClass("innerHeader")) {
          doc.setTextColor(200, 0, 0);
          doc.setFillColor(110, 214, 84);
          doc.rect(data.settings.margin.left, row.y, data.table.width, 20, "F");
          doc.autoTableText(
            "",
            data.settings.margin.left + data.table.width / 2,
            row.y + row.height / 2,
            {
              halign: "center",
              valign: "middle",
            }
          );
          /*  data.cursor.y += 20; */
        }

        if (row.index % 5 === 0) {
          var posY = row.y + row.height * 6 + data.settings.margin.bottom;
          if (posY > doc.internal.pageSize.height) {
            data.addPage();
          }
        }
      },

      drawCell: function (cell, data) {
        // Rowspan
        if ($(cell.raw).hasClass("innerHeader")) {
          // doc.addImage(imgData, 'JPEG', 15, 40, 200, 114);
          doc.setTextColor(200, 0, 0);
          doc.autoTableText(
            cell.text + "",
            data.settings.margin.left + data.table.width / 2,
            data.row.y + data.row.height / 2,
            {
              halign: "center",
              valign: "middle",
            }
          );

          return false;
        }
      },
    });
    return doc;
  }

  mobileValidation(mobile: any) {
    if (
      mobile.startsWith("6") ||
      mobile.startsWith("7") ||
      mobile.startsWith("8") ||
      mobile.startsWith("9")
    ) {
      if (
        mobile == "9999999999" ||
        mobile == "7777777777" ||
        mobile == "6666666666" ||
        mobile == "8888888888"
      ) {
        return false;
      } else {
        return true;
      }
      return true;
    } else {
      return false;
    }
  }

  // For Payment
  post1(endPoint: any, body: any) {
    this.payAuthToken = JSON.parse(localStorage.getItem("PayAuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.payAuthToken);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.baseURL1 + endPoint, body, { headers: headers })
      .toPromise();
  }

  testpost(endPoint: any, body: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.testURL + endPoint, body, { headers: headers })
      .toPromise();
  }

  /**
   * DigitalPOS Change
   * for passing the correct token in addPaymentRequest Api from pendingPayment Pay Button
   * 29-10-2021
   * @param endPoint
   * @param body
   * @returns
   */
  postPayms(endPoint: any, body: any) {
    let pos_flag = JSON.parse(localStorage.getItem("IsPOSTransaction"));
    if (pos_flag == true) {
      let accessTokenPOS = JSON.parse(localStorage.getItem("accessTokenPOS"));
      let headers = new HttpHeaders();
      headers = headers.append("Authorization", "Bearer " + accessTokenPOS);
      headers = headers.append("Content-Type", "application/json");
      return this.http
        .post(commonData.paymsSanity + endPoint, body, { headers: headers })
        .toPromise();
    } else {
      let paymsToken = JSON.parse(localStorage.getItem("paymsToken"));
      let headers = new HttpHeaders();
      headers = headers.append("Authorization", "Bearer " + paymsToken);
      headers = headers.append("Content-Type", "application/json");
      return this.http
        .post(commonData.paymsSanity + endPoint, body, { headers: headers })
        .toPromise();
    }
  }

  postLomPayms(endPoint: any, body: any) {
    let paymsToken = JSON.parse(localStorage.getItem("LombardPayAuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + paymsToken);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.lombardPay_url + endPoint, body, { headers: headers })
      .toPromise();
  }

  post2(endPoint: any, body: any) {
    this.payAuthToken = JSON.parse(localStorage.getItem("LombardPayAuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.payAuthToken);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.lombardPay + endPoint, body, { headers: headers })
      .toPromise();
  }

  get2(endPoint: any) {
    this.authToken = JSON.parse(localStorage.getItem("LombardPayAuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .get(commonData.lombardPay + endPoint, { headers: headers })
      .toPromise();
    // return this.http.get(commonData.baseURL + endPoint).toPromise();
  }

  // Post Service
  post(endPoint: any, body: any) {
    this.authToken = JSON.parse(localStorage.getItem("AuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.baseURL + endPoint, body, { headers: headers })
      .toPromise();
  }

  uploadDocument(postObj, remark, fileObj?): Observable<any> {
    this.authToken = JSON.parse(localStorage.getItem("AuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    const formData: FormData = new FormData();
    formData.append("ProposalNo", postObj.ProposalNo);
    formData.append("TransactionType", postObj.TransactionType);
    formData.append("PolicySubType", postObj.PolicySubType);
    formData.append("PendencyClearanceRemark", remark);
    formData.append("PendencyID", postObj.PendencyID);

    if (fileObj) {
      for (let file of fileObj) {
        formData.append("file", file, file.name);
      }
    }
    return this.http.post(commonData.baseURL + "/DocUpload", formData, {
      headers: headers,
    });
  }

  // payment request
  postpaymentreq(endPoint: any, body: any) {
    let headers = new HttpHeaders();
    headers = headers.append(
      "Authorization",
      "XI4eJDn9zzR3NQUrFturYsv5uxfAkTNw3Pga/crCM0o="
    );
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.baseURL + endPoint, body, { headers: headers })
      .toPromise();
  }

  // Pemium Break-UP
  getBreakUPDetails(endPoint: any) {
    let headers = new HttpHeaders();
    headers = headers.append(
      "Authorization",
      "XI4eJDn9zzR3NQUrFturYsv5uxfAkTNw3Pga/crCM0o="
    );
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .get(commonData.baseURL + endPoint, { headers: headers })
      .toPromise();
  }

  // saveErrorReq(endPoint: any, body:any){
  //   let headers = new HttpHeaders();
  //   headers = headers.append('Content-Type', 'application/json');
  //   return this.http.post(commonData.baseURL + endPoint, body, { headers: headers }).toPromise();
  // }

  // post with no authentication for TECH OPS
  postOps(endPoint: any, body: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.baseURL + endPoint, body, { headers: headers })
      .toPromise();
  }

  postOpsPayms(endPoint: any, body: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.baseURL1 + endPoint, body, { headers: headers })
      .toPromise();
  }

  //For 2.0 services
  // postBiz(endPoint: any, body: any) {
  //   this.authToken = JSON.parse(localStorage.getItem('bizToken'));
  //   let headers = new HttpHeaders();
  //   headers = headers.append("Authorization", "Bearer "+ this.authToken.token);
  //   headers = headers.append('Content-Type', 'application/json');
  //   return this.http.post(commonData.bizURL + endPoint, body, { headers: headers }).toPromise();
  // }

  getBizAdd(endPoint: any) {
    this.authToken = JSON.parse(localStorage.getItem("addOnbizToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .get(commonData.bizURL + endPoint, { headers: headers })
      .toPromise();
    // return this.http.post(commonData.baseURL + endPoint, body, { headers }).toPromise();
  }

  getBizAdd1(endPoint: any) {
    this.authToken = JSON.parse(localStorage.getItem("addOnbizToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .get(commonData.bizURL1 + endPoint, { headers: headers })
      .toPromise();
    // return this.http.post(commonData.baseURL + endPoint, body, { headers }).toPromise();
  }

  // Get Service
  getBitLy(endPoint: any) {
    this.authToken = JSON.parse(localStorage.getItem("AuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .get(commonData.baseURL + endPoint, { headers: headers })
      .toPromise();
  }

  // Get Service
  // getClaim(endPoint: any) {
  //   let headers = new HttpHeaders();
  //   headers = headers.append('Content-Type', 'application/json');
  //   return this.http.get(commonData.claimUrl + endPoint, { headers: headers }).toPromise();

  // }

  getWithAuth(endPoint: any) {
    // this.custToken = JSON.parse(localStorage.getItem('custToken'));
    let headers = new HttpHeaders();
    // headers = headers.append("Authorization", "Bearer " + this.custToken);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .get(commonData.baseURL + endPoint, { headers: headers })
      .toPromise();
  }

  getWithAuth1(endPoint: any, body: any) {
    // this.custToken = JSON.parse(localStorage.getItem('custToken'));
    let headers = new HttpHeaders();
    // headers = headers.append("Authorization", "Bearer " + this.custToken);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.baseURL + endPoint, body, { headers: headers })
      .toPromise();
  }

  loginTechops(endPoint: any, body: any) {
    // this.custToken = JSON.parse(localStorage.getItem('custToken'));
    let headers = new HttpHeaders();
    // headers = headers.append('Content-Type', 'application/json');
    headers = headers.append("Ilomid", body.userName);
    headers = headers.append("Password", btoa(body.pwd));

    return this.http
      .post(commonData.baseURL1 + endPoint, "", { headers: headers })
      .toPromise();
  }

  getURL(endPoint: any) {
    this.authToken = JSON.parse(localStorage.getItem("AuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    // headers = headers.append('Content-Type', 'application/text');
    headers = headers.append("responseType", "text");
    return this.http
      .get(commonData.baseURL + endPoint, { headers: headers })
      .toPromise();
    // return this.http.get(commonData.baseURL + endPoint).toPromise();
  }

  // Get Service
  getAddons(endPoint: any) {
    // this.authToken = JSON.parse(localStorage.getItem('AuthToken'));
    let headers = new HttpHeaders();
    // headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.addonsURL + endPoint, { headers: headers })
      .toPromise();
    // return this.http.get(commonData.baseURL + endPoint).toPromise();
  }

  hospifundsave(endPoint: any, body: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.mobURL1 + endPoint, body, { headers: headers })
      .toPromise();
  }

  gettext(endPoint: any) {
    this.authToken = JSON.parse(localStorage.getItem("AuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    // headers = headers.append('Content-Type', 'application/json');
    return this.http
      .get(commonData.baseURL + endPoint, { headers: headers })
      .toPromise();
    // return this.http.get(commonData.baseURL + endPoint).toPromise();
  }

  // Undefined or Null Check
  isUndefineORNull(data: any) {
    if (data == undefined || data == "" || data == null) {
      return true;
    } else {
      return false;
    }
  }

  // Get Authorization
  // getAuth(endPoint:any, body:any) {
  //   let headers = new HttpHeaders();
  //   headers = headers.append('Content-Type', 'application/json');
  //   return this.http.post(commonData.baseURL + endPoint, body, { headers: headers }).toPromise();
  // }

  encryptCustomerData(obj: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.paymsSanity + "EncryptIPUID", JSON.stringify(obj), {
        headers: headers,
      })
      .toPromise();
  }

  isNumber(text) {
    if (text) {
      var reg = new RegExp("[0-9]+$");
      return reg.test(text);
    }
    return false;
  }

  removeSpecial(text) {
    if (text) {
      var lower = text.toLowerCase();
      var upper = text.toUpperCase();
      var result = "";
      for (var i = 0; i < lower.length; ++i) {
        if (
          this.isNumber(text[i]) ||
          lower[i] != upper[i] ||
          lower[i].trim() === ""
        ) {
          result += text[i];
        }
      }
      return result;
    }
    return "";
  }

  // Payment Authorization
  getPayAuth() {
    // let imid = JSON.parse(localStorage.getItem('IPartnerUserID'));
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    // Sanity
    // headers = headers.append("UserName", "O4JXPkH0QLPJc2Uvb5YbvA==");
    // headers = headers.append("Password", "Ks9LPV0LyrcFuKY8NrrPwg==");

    // // New keys production
    headers = headers.append("Username", "G5*byte*s2klYfoL9Hpdx*byte*JfxmQ==");
    headers = headers.append("Password", "C58bt48qkCFvFpOkZB8wzw==");

    return this.http
      .post(commonData.baseURL1 + "Authenticate", "", { headers: headers })
      .toPromise();
  }

  getPaymsToken() {
    this.authToken = JSON.parse(localStorage.getItem("AuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .get(commonData.baseURL + "Payment/GetPAYMSToken", { headers: headers })
      .toPromise()
      .catch((err: any) => {
        if (err.status === 401 || err.status === 403) {
          window.location.href = commonData.loginURL;
        }
      });
  }

  getPaymsTokenRenewalPDF() {
    let authToken = JSON.parse(localStorage.getItem("RenewAuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + authToken.output);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .get(commonData.baseURL + "Payment/GetPAYMSToken", { headers: headers })
      .toPromise()
      .catch((err: any) => {
        if (err.status === 401 || err.status === 403) {
          window.location.href = commonData.loginURL;
        }
      });
  }

  getVahaanToken() {
    let body = {
      username: "iwqIU6Z/t8o=",
      password: "VYxkVlge5rq0WHkO7bucmQ==",
    };
    let str = JSON.stringify(body);
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.baseURLForRedis + "api/v1/token", str, {
        headers: headers,
      })
      .toPromise()
      .catch((err: any) => {
        if (err.status === 401 || err.status === 403) {
          window.location.href = commonData.loginURL;
        }
      });
  }

  getExshowroomToken() {
    this.UpdateHttpRequest();
    return this.http
      .get(commonData.baseURL + "middleware/token?Scope=13", this.httpOptions)
      .toPromise();
  }

  getPaymsTokenForCustomer(iuserId) {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    headers = headers.append("UserName", commonData.paymsUser);
    headers = headers.append("Password", commonData.paymsPass);
    headers = headers.append("IPartnerUserID", iuserId);
    return this.http
      .get(commonData.paymsSanity + "Authenticate", { headers: headers })
      .toPromise();
  }

  getLomPayms(endPoint: any) {
    let paymsToken = JSON.parse(localStorage.getItem("paymsToken"));
    // this.payAuthToken = JSON.parse(localStorage.getItem('LombardPayAuthToken'));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + paymsToken);
    headers = headers.append("Content-Type", "application/json");
    // return this.http.post(commonData.paymsSanity + endPoint, body, { headers: headers }).toPromise();
    return this.http
      .get(commonData.lombardPay_url + endPoint, { headers: headers })
      .toPromise();
  }

  // Get Service
  lombardPayURL: string;
  getlombardPay() {
    this.authToken = JSON.parse(localStorage.getItem("AuthToken"));
    /**
     * Digital POS Change
     * For Passing URL For Normal Nysa Flow and Digital POS Flows
     */
    if (this.IsPOSTransaction_flag == true) {
      this.lombardPayURL = "Payment/DigitalPOS/GetLombardPayToken";
    } else {
      this.lombardPayURL = "Payment/GetLombardPayToken";
    }
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .get(commonData.baseURL + this.lombardPayURL, { headers: headers })
      .toPromise();
  }

  // Token Validity
  checkTokenValidity() {
    this.authToken = JSON.parse(localStorage.getItem("AuthToken"));
    if (this.currentDate > new Date(this.authToken.expiry)) {
      return true;
    } else {
      return false;
    }
  }

  // New Token for Payment
  getNewToken(token: any, endPoint: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + token);
    headers = headers.append("Content-Type", "application/json");
    let body = { ExistingToken: token };
    let str = JSON.stringify(body);
    return this.http
      .post(commonData.baseURL1 + endPoint, str, { headers: headers })
      .toPromise();
  }

  // Go to Dashboard
  geToDashboard() {
    this.get("Token/CreateIpartnerToken").subscribe(
      (res) => {
        window.location.href =
          commonData.dashBoardURL +
          res.expiry +
          "&signature=" +
          res.signature +
          "&iPartner_Token=" +
          res.iPartner_Token +
          "&pType=" +
          commonData.pType;
      },
      (err) => {
        window.location.href = commonData.loginURL;
      }
    );
  }

  geToDashboardapproval(pcode, type) {
    this.get("Token/CreateIpartnerToken").subscribe(
      (res) => {
        window.location.href =
          commonData.dashBoardURL +
          res.expiry +
          "&signature=" +
          res.signature +
          "&iPartner_Token=" +
          res.iPartner_Token +
          "&pType=" +
          commonData.pType +
          "&pCode=" +
          pcode +
          "&tType=" +
          type;
      },
      (err) => {
        window.location.href = commonData.loginURL;
      }
    );
  }

  goTopending() {
    this.get("Token/GetTokenToRedirectOnPendingPayment").subscribe(
      (res) => {
        window.location.href =
          commonData.dashBoardURL +
          res.expiry +
          "&signature=" +
          res.signature +
          "&iPartner_Token=" +
          res.iPartner_Token +
          "&pType=" +
          commonData.pType +
          "&rType=" +
          commonData.rType;
      },
      (err) => {
        window.location.href = commonData.loginURL;
      }
    );
  }

  goToTravel() {
    this.get("Token/GetTokenToRedirectOnPendingPayment").subscribe(
      (res) => {
        window.location.href =
          commonData.dashBoardURL +
          res.expiry +
          "&signature=" +
          res.signature +
          "&iPartner_Token=" +
          res.iPartner_Token +
          "&pType=" +
          commonData.pType +
          "&rPanel=TR";
      },
      (err) => {
        window.location.href = commonData.loginURL;
      }
    );
  }

  encryptLikePython(value: any) {
    var key = CryptoJS.enc.Utf8.parse("e78538a1-9bae-4a3e-9bc9-");
    var iv = CryptoJS.enc.Utf8.parse("d45913c1-a-6bc3-");
    var encrypted = CryptoJS.AES.encrypt(
      CryptoJS.enc.Utf8.parse(value.toString()),
      key,
      {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7,
      }
    );
    return encrypted.toString();
  }

  encryptUsingGUID(value: any, guid: any) {
    // var key = CryptoJS.enc.Utf8.parse(JSON.stringify(guid));
    var key = CryptoJS.enc.Utf8.parse(guid);
    var iv = CryptoJS.enc.Utf8.parse("d45913c1-a-6bc3-");
    var encrypted = CryptoJS.AES.encrypt(
      CryptoJS.enc.Utf8.parse(value.toString()),
      key,
      {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7,
      }
    );
    return encrypted.toString();
  }

  goToBitly(imid: any) {
    // let url = "https://nysa-services.icicilombard.com/#/swap?agentId=" + imid;
    // console.log(url);
    window.location.href =
      "https://nysa-services.icicilombard.com/#/swap?agentId=" + imid;
  }

  goToHealth() {
    this.get("Token/GetTokenToRedirectOnPendingPayment").subscribe(
      (res) => {
        window.location.href =
          commonData.dashBoardURL +
          res.expiry +
          "&signature=" +
          res.signature +
          "&iPartner_Token=" +
          res.iPartner_Token +
          "&pType=" +
          commonData.pType +
          "&rPanel=HL";
      },
      (err) => {
        window.location.href = commonData.loginURL;
      }
    );
  }

  // Clear Local Storage
  clearLocalStorage() {
    // localStorage.removeItem("calQuoteReq");
    // localStorage.removeItem("calQuoteRes");
    // localStorage.removeItem("dataforBack");
    // localStorage.removeItem("PropDatareq");
    // localStorage.removeItem("PropDatares");
    // localStorage.removeItem("propdataforBack");
    // localStorage.removeItem("customerData")
    localStorage.removeItem("orderReq");
    localStorage.removeItem("orderRes");
    localStorage.removeItem("PFReq");
    localStorage.removeItem("PFRes");
    localStorage.removeItem("newProposalNumber");
    localStorage.removeItem("newPolicyNumber");
    localStorage.removeItem("rtoDetail");
    localStorage.removeItem("isPendingPayment");
    localStorage.removeItem("multipleProposal");
    localStorage.removeItem("multiTotalAmount");
    localStorage.removeItem("allriskmultipleProposal");
    localStorage.removeItem("allriskmultiTotalAmount");
  }

  clearPropData() {
    localStorage.removeItem("PropDatareq");
    localStorage.removeItem("PropDatares");
    localStorage.removeItem("propdataforBack");
    localStorage.removeItem("customerData");
    // localStorage.removeItem("scpaPropDatareq");
    // localStorage.removeItem("scpaPropDatares");
    // localStorage.removeItem("EVPropDatareq");
    // localStorage.removeItem("EVPropDatares");
  }

  clearQuoteDate() {
    localStorage.removeItem("calQuoteReq");
    localStorage.removeItem("calQuoteReq4w");
    localStorage.removeItem("calQuoteRes");
    localStorage.removeItem("calQuoteRes4w");
    localStorage.removeItem("dataforBack");
    localStorage.removeItem("calQuoteTPReq");
    localStorage.removeItem("calQuoteTPRes");
    // localStorage.removeItem("EVQuoteReq");
    // localStorage.removeItem("EVQuoteRes");
    // localStorage.removeItem("scpaQuoteReq");
    // localStorage.removeItem("scpaQuoteRes");
  }

  // gotologin
  geToLogin() {
    this.get("iPartnerToken").subscribe(
      (res) => {
        window.location.href = commonData.loginURL;
      },
      (err) => {
        window.location.href = commonData.loginURL;
      }
    );
    // .catch((err) => {
    //   window.location.href = commonData.loginURL;
    // })
  }

  goToTechOpsLogin() {
    this.router.navigateByUrl("techops/login");
  }

  // Download PDF
  save(file: string, fileName: string) {
    if ((window.navigator as any).msSaveOrOpenBlob) {
      (navigator as any).msSaveBlob(file, fileName);
    } else {
      const downloadLink = document.createElement("a");
      downloadLink.style.display = "none";
      document.body.appendChild(downloadLink);
      downloadLink.setAttribute("href", file);
      downloadLink.setAttribute("download", fileName);
      downloadLink.click();
      document.body.removeChild(downloadLink);
    }
  }

  replaceAll(str, search, replace) {
    return str.replace(new RegExp(search, "g"), replace);
  }

  // for Nysa
  // DECRYPTION USING CBC TRIPLE DES
  decryptUsingTripleDES(encrypted: string): string {
    const keyHex = CryptoJS.enc.Utf8.parse("e78538a1-9bae-4a3e-9bc9-");
    const iv = CryptoJS.enc.Utf8.parse("ef17086a");
    const mode = CryptoJS.mode.CBC;
    const decrypted = CryptoJS.TripleDES.decrypt(encrypted, keyHex, {
      iv,
      mode,
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
  }

  // DECRYPTION USING AES
  decryptUsingAES(encrypted: string): string {
    const hash = CryptoJS.MD5("e78538a1-9bae-4a3e-9bc9-").toString();
    const decrypted = CryptoJS.AES.decrypt(encrypted, hash);
    return decrypted.toString(CryptoJS.enc.Utf8);
  }

  encryptPDF(msg, pass) {
    var salt = CryptoJS.lib.WordArray.random(this.saltSize / 8);
    var key = CryptoJS.PBKDF2(pass, salt, {
      keySize: this.keySize / 32,
      iterations: this.iterations,
    });
    var iv = CryptoJS.lib.WordArray.random(this.ivSize / 8);
    var encrypted = CryptoJS.AES.encrypt(msg, key, {
      iv: iv,
      padding: CryptoJS.pad.Pkcs7,
      mode: CryptoJS.mode.CBC,
    });
    var encryptedHex = this.base64ToHex(encrypted.toString());
    var base64result = this.hexToBase64new(
      salt.toString() + iv.toString() + encryptedHex
    );
    return base64result;
  }

  // FOr Plutus
  encrypt(msg, pass) {
    var salt = CryptoJS.lib.WordArray.random(this.saltSize / 8);
    var key = CryptoJS.PBKDF2(pass, salt, {
      keySize: this.keySize / 32,
      iterations: this.iterations,
    });
    var iv = CryptoJS.lib.WordArray.random(this.ivSize / 8);
    var encrypted = CryptoJS.AES.encrypt(msg, key, {
      iv: iv,
      padding: CryptoJS.pad.Pkcs7,
      mode: CryptoJS.mode.CBC,
    });
    var encryptedHex = this.base64ToHex(encrypted.toString());
    var base64result = this.hexToBase64(
      salt.toString() + iv.toString() + encryptedHex
    );
    return base64result;
  }

  hexToBase64(str) {
    return btoa(
      String.fromCharCode.apply(
        null,
        str
          .replace(/\r|\n/g, "")
          .replace(/([\da-fA-F]{2}) ?/g, "0x$1 ")
          .replace(/ +$/, "")
          .split(" ")
      )
    );
  }

  hexToBase64new(str) {
    var bString = "";
    for (var i = 0; i < str.length; i += 2) {
      bString += String.fromCharCode(parseInt(str.substr(i, 2), 16));
    }
    return btoa(bString);
  }

  base64ToHex(str) {
    for (
      var i = 0, bin = atob(str.replace(/[ \r\n]+$/, "")), hex = [];
      i < bin.length;
      ++i
    ) {
      var tmp = bin.charCodeAt(i).toString(16);
      if (tmp.length === 1) tmp = "0" + tmp;
      hex[hex.length] = tmp;
    }
    return hex.join("");
  }

  checkForNewPaymentModule() {
    this.authToken = JSON.parse(localStorage.getItem("AuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .get(commonData.baseURL + "PaymsPayment/IsPaymentModuleNew", {
        headers: headers,
      })
      .toPromise();
  }

  // Access/GetApplicationAccess
  /**
   *
   * @param req
   * DigitalPOS Change
   * setting the pos url for redirecting proper flow
   * 30-10-2021
   * @param res
   * @param isInsta
   * @param isMulti
   */
  /**
   * calling  the paymsToken api before redirecting to plutus payment page
   * Author :- Sumit
   * date :- 21-01-2022
   */
  redirectToPlutus(
    req: any,
    res: any,
    isInsta: any,
    isPreInsta:any,
    isMulti: any,
    isAdvance: any
  ) {
    this.getPaymsToken().then((resp: any) => {
      localStorage.setItem("paymsToken", JSON.stringify(resp.accessToken));
      this.redirectToPlutusPayment(req, res, isInsta, isPreInsta, isMulti, isAdvance);
    });
  }

  redirectToPlutusPayment(
    req: any,
    res: any,
    isInsta: any,
    isPreInsta:any,
    isMulti: any,
    isAdvance: any
  ) {
    let body;
    let isInsta1: any;
    // let bankType = localStorage.getItem("bankType");
    // if (bankType == "INT") {
    //   isInsta1 = 1;
    // } else {
    //   isInsta1 = 0;
    // }
    if (isMulti == true) {
      let multiProp = JSON.parse(localStorage.getItem("multipleProposal"));
      let totalAmt = localStorage.getItem("multiTotalAmount");
      let dealID = JSON.parse(localStorage.getItem("DealID"));
      let correlationId = Guid.raw();
      body = {
        CorrelationID: correlationId,
        Amount: totalAmt,
        DealID: dealID,
        UserFlag: 0,
        IsInsta: isInsta,
        MultiFlag: 1,
        MultiProposal: multiProp,
        PidFlag: isAdvance,
        PreInsta: isPreInsta,
      };
    } else {
      let email1 = req.ProposalDetails ? req.ProposalDetails.CustomerDetails.Email : req.CustomerDetails.Email;
      let mobile1 = req.ProposalDetails ? req.ProposalDetails.CustomerDetails.MobileNumber : req.CustomerDetails.MobileNumber;
      let email = this.encrypt(
        email1,
        commonData.aesSecretKey
      );
      let phone = this.encrypt(
        mobile1,
        commonData.aesSecretKey
      );
      console.log(req.DealId);
      if (localStorage.getItem("isAllRisk") == "true") {
        body = {
          CorrelationID: res.CorrelationId,
          Amount: Math.round(res.GSTSERVICERESPONSE.GSTSERVICEDETAILS.FINALAMOUNT),
          ProposalNo: res.proposalNumber,
          DealID: req.DealId ? req.DealId : req.DealID,
          CustomerID: res.customerId,
          Email: email,
          ContactNo: phone,
          UserFlag: 0,
          IsInsta: isInsta,
          PidFlag: isAdvance,
          PreInsta: isPreInsta,
        };
      } else  {
        body = {
          CorrelationID: res.correlationId,
          Amount: res.finalPremium,
          ProposalNo: res.generalInformation.proposalNumber,
          DealID: req.DealId ? req.DealId : req.DealID,
          CustomerID: res.generalInformation.customerId,
          Email: email,
          ContactNo: phone,
          UserFlag: 0,
          IsInsta: isInsta,
          PidFlag: isAdvance,
          PreInsta: isPreInsta,
        };
      }

    
    }

    let str = JSON.stringify(body);
    this.loaderStatus = true;

    this.postPayms("Redirection/AddPaymentRequest", str)
      .then((res: any) => {
        if (res.Status == "success") {
          this.loaderStatus = false;
          let breakinFlag = localStorage.getItem("breakinFlag");
          let data = localStorage.getItem("breakinData");
          if (breakinFlag == "true") {
            this.clearBreakinInspection(data).then(() => {
              this.IsPOSTransaction_flag = JSON.parse(
                localStorage.getItem("IsPOSTransaction")
              );
              if (this.IsPOSTransaction_flag == true) {
                let mobileNo = JSON.parse(
                  localStorage.getItem("POSmobileNumber")
                );
                location.href =
                  res.URL + "&MOBILENO=" + mobileNo + "&channelName=DigitalPOS"; // Extended the url for POS
              } else {
                location.href = res.URL;
              }
            });
          } else {
            this.IsPOSTransaction_flag = JSON.parse(
              localStorage.getItem("IsPOSTransaction")
            );
            if (this.IsPOSTransaction_flag == true) {
              let mobileNo = JSON.parse(
                localStorage.getItem("POSmobileNumber")
              );
              location.href =
                res.URL + "&MOBILENO=" + mobileNo + "&channelName=DigitalPOS"; // Extended the url for POS
            } else {
              location.href = res.URL;
            }
          }
        } else {
          // this.router.navigateByUrl("payment");
          this.loaderStatus = false;
        }
      })
      .catch((err: any) => {
        // this.router.navigateByUrl("payment");
        this.loaderStatus = false;
      });
  }

  //renewal payment tokan IAS
  redirectToPlutusIAS(
    req: any,
    res: any,
    isInsta: any,
    preInsta: any,
    isMulti: any,
    isAdvance: any
  ) {
    this.getPaymsToken().then((resp: any) => {
      localStorage.setItem("paymsToken", JSON.stringify(resp.accessToken));
      this.redirectToPlutusPaymentIAS(req, res, isInsta, preInsta, isMulti, isAdvance);
    });
  }

  //Renewal IAS
  redirectToPlutusPaymentIAS(
    req: any,
    res: any,
    isInsta: any,
    preInsta: any,
    isMulti: any,
    isAdvance: any
  ) {
    let body;
    let isInsta1: any;
    console.log(req, res);
    // let bankType = localStorage.getItem("bankType");
    // if (bankType == "INT") {
    //   isInsta1 = 1;
    // } else {
    //   isInsta1 = 0;
    // }
    if (isMulti == true) {
      let multiProp = JSON.parse(localStorage.getItem("multipleProposal"));
      let totalAmt = localStorage.getItem("multiTotalAmount");
      let dealID = JSON.parse(localStorage.getItem("DealID"));
      let correlationId = Guid.raw();
      body = {
        CorrelationID: correlationId,
        Amount: totalAmt,
        DealID: dealID,
        UserFlag: 0,
        IsInsta: isInsta,
        MultiFlag: 1,
        MultiProposal: multiProp,
        PidFlag: isAdvance,
        PreInsta: 0,
      };
    } else {
      let email1 = req.ProposalDetails ? req.ProposalDetails.CustomerDetails.Email : req.CustomerDetails.Email;
      let mobile1 = req.ProposalDetails ? req.ProposalDetails.CustomerDetails.MobileNumber : req.CustomerDetails.MobileNumber;
      let dealID = JSON.parse(localStorage.getItem("DealID"));
      let email = this.encrypt(
        email1,
        commonData.aesSecretKey
      );
      let phone = this.encrypt(
        mobile1,
        commonData.aesSecretKey
      );

      body = {
        CorrelationID: res.correlationId,
        Amount: res.finalPremium,
        ProposalNo: res.generalInformation.proposalNumber,
        DealID: dealID,
        CustomerID: res.generalInformation.customerId,
        Email: email,
        ContactNo: phone,
        UserFlag: 0,
        IsInsta: isInsta,
        PidFlag: isAdvance,
        PreInsta: 0,
      };
    }

    let str = JSON.stringify(body);
    this.loaderStatus = true;

    this.postPayms("Redirection/AddPaymentRequest", str)
      .then((res: any) => {
        if (res.Status == "success") {
          this.loaderStatus = false;
          location.href = res.URL;
        } else {
          // this.router.navigateByUrl("payment");
          this.loaderStatus = false;
        }
      })
      .catch((err: any) => {
        // this.router.navigateByUrl("payment");
        this.loaderStatus = false;
      });
  }


  //renewal payment tokan for breakin
  redirectToPlutusBreakin(
    req: any,
    res: any,
    isInsta: any,
    preInsta: any,
    isMulti: any,
    isAdvance: any
  ) {
    this.getPaymsToken().then((resp: any) => {
      localStorage.setItem("paymsToken", JSON.stringify(resp.accessToken));
      this.redirectToPlutusPaymentBreakin(req, res, isInsta, preInsta, isMulti, isAdvance);
    });
  }

  //Renewal Breakin
  redirectToPlutusPaymentBreakin(
    req: any,
    res: any,
    isInsta: any,
    preInsta: any,
    isMulti: any,
    isAdvance: any
  ) {
    let body;
    let isInsta1: any;
    let bankType = localStorage.getItem("bankType");
    if (bankType == "INT") {
      isInsta1 = 1;
    } else {
      isInsta1 = 0;
    }
    if (isMulti == true) {
      let multiProp = JSON.parse(localStorage.getItem("multipleProposal"));
      let totalAmt = localStorage.getItem("multiTotalAmount");
      let dealID = JSON.parse(localStorage.getItem("DealID"));
      let correlationId = Guid.raw();
      body = {
        CorrelationID: correlationId,
        Amount: totalAmt,
        DealID: dealID,
        UserFlag: 0,
        IsInsta: isInsta1,
        MultiFlag: 1,
        MultiProposal: multiProp,
        PidFlag: isAdvance,
        PreInsta: 0,
      };
    } else {
      let dealID = JSON.parse(localStorage.getItem("DealID"));
      let email1 = req.ProposalDetails ? req.ProposalDetails.CustomerDetails.Email : req.CustomerDetails.Email;
      let mobile1 = req.ProposalDetails ? req.ProposalDetails.CustomerDetails.MobileNumber : req.CustomerDetails.MobileNumber;
      let email = this.encrypt(
        email1,
        commonData.aesSecretKey
      );
      let phone = this.encrypt(
        mobile1,
        commonData.aesSecretKey
      );

      body = {
        CorrelationID: res.correlationId,
        Amount: res.finalPremium,
        ProposalNo: res.generalInformation.proposalNumber,
        DealID: dealID,
        CustomerID: res.generalInformation.customerId,
        Email: email,
        ContactNo: phone,
        UserFlag: 0,
        IsInsta: isInsta1,
        PidFlag: isAdvance,
        PreInsta: 0,
      };
    }

    let str = JSON.stringify(body);
    this.loaderStatus = true;

    this.postPayms("Redirection/AddPaymentRequest", str)
      .then((res: any) => {
        if (res.Status == "success") {
          this.loaderStatus = false;
          location.href = res.URL;
        } else {
          // this.router.navigateByUrl("payment");
          this.loaderStatus = false;
        }
      })
      .catch((err: any) => {
        // this.router.navigateByUrl("payment");
        this.loaderStatus = false;
      });
  }

  clearBreakinInspection(data): Promise<any> {
    return new Promise((resolve: any) => {
      let breakinData = JSON.parse(data);
      let propRes = JSON.parse(breakinData.proposalRS);
      let quoteReq = JSON.parse(breakinData.quoteRQ);
      let startDate = moment(new Date()).format("DD-MM-YYYY");

      let body = {
        InspectionId: breakinData.inspectionID,
        DealNo: quoteReq.DealId,
        ReferenceDate: startDate, //this.propRes.generalInformation.proposalDate,
        InspectionStatus: "OK",
        CorrelationId: breakinData.corelationID,
        ReferenceNo: propRes.generalInformation.proposalNumber,
      };

      let str = JSON.stringify(body);
      this.postBiz("/breakin/clearinspectionstatus", str).subscribe((res) => {
        // return res;
        resolve();
      });
    });
  }

  // New Observabable code
  // Headers
  UpdateHttpRequest() {
    this.authToken = JSON.parse(localStorage.getItem("AuthToken"));
    this.httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json; charset=utf-8",
        authorization: "Bearer " + this.authToken.token,
        "cache-control": "no-cache",
      }),
    };
  }

  UpdateHttpRequestForRedis() {
    this.authToken = JSON.parse(localStorage.getItem("AuthTokenRedis"));
    this.httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json; charset=utf-8",
        authorization: "Bearer " + this.authToken.access,
        "cache-control": "no-cache",
      }),
    };
  }

  // Get Authorization
  getAuth(endPoint: any, body: any): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    return this.http.post(commonData.baseURL + endPoint, body, {
      headers: headers,
    });
  }

  // Get Services
  get(endPoint: any): Observable<any> {
    this.UpdateHttpRequest();
    return this.http
      .get(commonData.baseURL + endPoint, this.httpOptions)
      .pipe(catchError(this.errorHandler));
  }

  getBizzTalkQuote(endPoint: any, body: any) {
    let bizzTalkToken = JSON.parse(localStorage.getItem("bizzTalkToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + bizzTalkToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.biztalkURL + endPoint, body, { headers: headers })
      .toPromise();
  }

  get1(endPoint: any) {
    this.UpdateHttpRequest();
    return this.http
      .get(commonData.baseURL + endPoint, this.httpOptions)
      .toPromise();
  }

  getCrawler(endPoint: any): Observable<any> {
    this.UpdateHttpRequest();
    let headers = new HttpHeaders();
    // Sanity
    // headers = headers.append("Authorization", "Basic " + btoa("gAAAAABhQuptpInxm1QQNmRsB8Yz2NA3kWYrmxyagD7VOLJ0Ek44Keu7JfZ8N-Fhjbq7_Ll9mjE0F4IzNZoCXM545IO6osstcr85Y9OsytlJlXD2cp8s8mM=:gAAAAABhQuptUMmTGt2Ywreu9S0cZsXbl9TPokZGFUVDQ3OuEWN6gfCR0DmhwTht8yxw55eWqJ_WNYaecShjXX4-FVIu9egKfBgKfecGIi3vpK1StNXtdvo=")); //Sanity
    // Production
    headers = headers.append('Authorization', 'Basic ' + btoa('gAAAAABhSy64LRZxS0G9jL59FYoZvEevcrFuBbgDBKkj1I5kGrEi_pCLBOaPkqGakp2hYVZoFMGKxhPiXObVazvU79rjY51m7O92hvCRwcfxYd6YJ1q2NNM=:gAAAAABhSy64Zh6UTSu9GsSvoX2vUw95rDSI0VBybVgZh_fwnYvyu3QsaIlRxZlrddEtyJjfLNk-095i1X1xeU72LAdcoIZIS_AemAmUPlDnUFc6SX3grO4=')); // Prod
    return this.http
      .get(commonData.crawlerURL + endPoint, { headers: headers })
      .pipe(catchError(this.errorHandler));
  }

  getRTO(endPoint: any): Observable<any> {
    this.UpdateHttpRequest();
    return this.http
      .get(commonData.baseURL + endPoint, this.httpOptions)
      .pipe(timeout(commonData.rtoTimeout), catchError(this.errorHandler));
  }

  getRTORedis(endPoint: any, body: any): Observable<any> {
    this.UpdateHttpRequestForRedis();
    return this.http
      .post(commonData.baseURLForRedis + endPoint, body, this.httpOptions)
      .pipe(timeout(commonData.rtoTimeout), catchError(this.errorHandler));
  }

  getRedisExShowroom(endPoint: any, body: any) {
    this.UpdateHttpRequestForRedis();
    return this.http
      .post(commonData.baseURLForRedis + endPoint, body, this.httpOptions)
      .pipe(timeout(commonData.rtoTimeout), catchError(this.errorHandler));
  }

  getPHT(endPoint: any) {
    this.phauthToken = JSON.parse(localStorage.getItem("phToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.phauthToken);
    headers = headers.append("Content-Type", "application/json");
    return this.http.post(commonData.baseURL + endPoint, { headers: headers });
  }

  postPHT(endPoint: any, body: any) {
    this.phauthToken = JSON.parse(localStorage.getItem("phToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.phauthToken);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.baseURL + endPoint, body, { headers: headers })
      .toPromise();
  }

  //For 2.0 services
  postBiz(endPoint: any, body: any): Observable<any> {
    this.authToken = JSON.parse(localStorage.getItem("bizToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http.post(commonData.bizURL + endPoint, body, {
      headers: headers,
    });
  }

  allRiskEBpostBiz(endPoint: any, body: any): Observable<any> {
    // this.authToken = JSON.parse(localStorage.getItem("bizToken"));
    this.authToken = JSON.parse(localStorage.getItem("allRiskbizToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http.post(commonData.bizURL1 + endPoint, body, {
      headers: headers,
    });
  }

  getAllRiskData(endPoint: any): Observable<any> {
      let headers = new HttpHeaders();
      headers = headers.append("Content-Type", "application/json");
      return this.http
      .get(commonData.baseURL + endPoint, { headers: headers })
      .pipe(catchError(this.errorHandler));
  }

  postExBiz(endPoint: any, body: any): Observable<any> {
    this.authToken = JSON.parse(localStorage.getItem("exShowroomToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http.post(commonData.bizURL + endPoint, body, {
      headers: headers,
    });
  }

  postILSBIAS(endPoint: any, body: any): Observable<any> {
    this.authToken = JSON.parse(localStorage.getItem("iasToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http.post(commonData.bizURL1 + endPoint, body, {
      headers: headers,
    });
  }

  //tp crwaler changes by monika
  postCrawlerBiz(endPoint: any, body: any): Observable<any> {
    // let crawlerToken = JSON.parse(localStorage.getItem("bizTokenCrawler"));
    let crawlerToken = JSON.parse(localStorage.getItem("newCrawlerToken"));
    let token = this.isUndefineORNull(crawlerToken) ? "" : crawlerToken.access;
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + token);
    headers = headers.append("Content-Type", "application/json");
    return this.http.post(commonData.crawlerURL + endPoint, body, {
      headers: headers,
    });
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }

  createBizToken(): Promise<any> {
    return new Promise((resolve: any) => {
      this.get("middleware/token?Scope=2").subscribe((res: any) => {
        localStorage.setItem("bizToken", JSON.stringify(res));
        resolve();
      });
    });
  }

  // createBizzTalkToken(): Promise<any> {
  //   return new Promise((resolve: any) => {
  //     this.getBizzTalk("middleware/token?Scope=esbmotorquote").subscribe((res: any) => {
  //       localStorage.setItem("bizToken", JSON.stringify(res));
  //       resolve();
  //     });
  //   });
  // }
  
  createIASToken(): Promise<any> {
    return new Promise((resolve: any) => {
      this.get("middleware/token?Scope=10").subscribe((res: any) => {
        localStorage.setItem("iasToken", JSON.stringify(res));
        resolve();
      });
    });
  }

  getQuote(proposalDetails, quoteReq, quoteRes, productCode): Promise<any> {
    return new Promise((resolve: any) => {
      let correlationId = Guid.raw();
      quoteReq.CorrelationId = correlationId;
      if (proposalDetails.PropPreviousPolicyDetails == true) {
        quoteReq.isNoPrevInsurance = false;
        let body;
        body = {
          PreviousPolicyDetails: {
            PreviousPolicyType: proposalDetails.PreviousPolicyType,
            PreviousPolicyStartDate: moment(
              proposalDetails.PreviousPolicyStartDate
            ).format("YYYY-MM-DD"),
            PreviousPolicyEndDate: moment(
              proposalDetails.PreviousPolicyEndDate
            ).format("YYYY-MM-DD"),
            BonusOnPreviousPolicy: proposalDetails.BonusOnPreviousPolicy,
            PreviousPolicyNumber: proposalDetails.PreviousPolicyNumber,
            ClaimOnPreviousPolicy: proposalDetails.ClaimOnPreviousPolicy,
            TotalNoOfODClaims: proposalDetails.TotalNoOfODClaims,
            NoOfClaimsOnPreviousPolicy: proposalDetails.TotalNoOfODClaims,
            PreviousInsurerName: proposalDetails.companyCode,
            PreviousPolicyTenure: proposalDetails.PreviousPolicyTenure,
            PreviousVehicleSaleDate: "0001-01-01T00:00:00",
          },
        };

        let obj1 = quoteReq;
        let obj2 = body;
        quoteReq = Object.assign(obj1, obj2);
      } else {
        quoteReq.isNoPrevInsurance = true;
        quoteReq.PreviousPolicyDetails = null;
      }

      let URL;
      if (productCode == "2311") {
        URL = "/proposal/privatecarcalculatepremium";
      } else if (productCode == "2312") {
        URL = "/proposal/twowheelercalculatepremium";
      }

      if (proposalDetails.PropPreviousPolicyDetails == true) {
        if (
          proposalDetails.PreviousPolicyType == "" ||
          proposalDetails.PreviousPolicyType == null ||
          proposalDetails.PreviousPolicyType == undefined
        ) {
          this.loaderStatus = false;
          swal({
            text: "Kindly select Previous Policy Type",
            closeOnClickOutside: false,
          });
        } else if (
          proposalDetails.PreviousPolicyEndDate == "" ||
          proposalDetails.PreviousPolicyEndDate == null ||
          proposalDetails.PreviousPolicyEndDate == undefined
        ) {
          this.loaderStatus = false;
          swal({
            text: "Kindly insert Previous Policy End date",
            closeOnClickOutside: false,
          });
        } else if (
          proposalDetails.PreviousPolicyNumber == "" ||
          proposalDetails.PreviousPolicyNumber == null ||
          proposalDetails.PreviousPolicyNumber == undefined
        ) {
          this.loaderStatus = false;
          swal({
            text: "Kindly insert Previous Policy number",
            closeOnClickOutside: false,
          });
        } else {
          this.loaderStatus = true;
          this.createBizToken().then(() => {
            this.postBiz(URL, quoteReq).subscribe(
              (res: any) => {
                if (res.status == "Success") {
                  localStorage.setItem("calQuoteReq", JSON.stringify(quoteReq));
                  localStorage.setItem("calQuoteRes", JSON.stringify(res));
                  this.saveofflineQuote(quoteReq, res, productCode);
                } else {
                  swal({ text: res.message, closeOnClickOutside: false });
                }
                this.loaderStatus = false;
              },
              (err) => {
                this.loaderStatus = false;
              }
            );
          });
        }
      } else {
        this.loaderStatus = true;
        this.createBizToken().then(() => {
          this.postBiz(URL, quoteReq).subscribe(
            (res: any) => {
              if (res.status == "Success") {
                localStorage.setItem("calQuoteReq", JSON.stringify(quoteReq));
                localStorage.setItem("calQuoteRes", JSON.stringify(res));
                this.saveofflineQuote(quoteReq, res, productCode);
              } else {
                swal({ text: res.message, closeOnClickOutside: false });
              }
              this.loaderStatus = false;
            },
            (err) => {
              this.loaderStatus = false;
            }
          );
        });
      }
      resolve();
    });
  }

  saveofflineQuote(quoteReq, quoteRes, productCode) {
    let body, URL;
    body = {
      quoteRQ: quoteReq,
      quoteRS: quoteRes,
      IsDigitalPOS: this.IsPOSTransaction_flag, // Added digitalPOS parameter with value true or false based on value in localStorage
    };
    if (productCode == "2312") {
      URL = "Quote/tw/SaveQuote";
    }
    if (productCode == "2311") {
      URL = "Quote/fw/SaveQuote";
    }
    let str = JSON.stringify(body);
    this.post(URL, str).then((res: any) => {
      console.log(res);
    });
  }

  getIpAddress() {
    return this.http
      .get("http://api.ipify.org/?format=json")
      .map((response) => response || {});
  }

  getIpdetails(data: any): Promise<any> {
    return new Promise((resolve: any) => {
      this.ip = data.ip;
      resolve();
    });
  }

  getlocationdetails() {
    return this.http
      .get("http://ipwhois.app/json/" + this.ip)
      .map((response) => response || {});
  }

  myBrowser() {
    if (
      (navigator.userAgent.indexOf("Opera") ||
        navigator.userAgent.indexOf("OPR")) != -1
    ) {
      return "Opera";
    } else if (navigator.userAgent.indexOf("Chrome") != -1) {
      return "Chrome";
    } else if (navigator.userAgent.indexOf("Safari") != -1) {
      return "Safari";
    } else if (navigator.userAgent.indexOf("Firefox") != -1) {
      return "Firefox";
    } else if (
      navigator.userAgent.indexOf("MSIE") != -1 ||
      !!document["documentMode"] == true
    ) {
      return "IE";
    } else {
      return "unknown";
    }
  }

  getBrowserVersion() {
    var userAgent = navigator.userAgent,
      tem,
      matchTest =
        userAgent.match(
          /(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i
        ) || [];
    if (/trident/i.test(matchTest[1])) {
      tem = /\brv[ :]+(\d+)/g.exec(userAgent) || [];
      return "IE " + (tem[1] || "");
    }
    if (matchTest[1] === "Chrome") {
      tem = userAgent.match(/\b(OPR|Edge)\/(\d+)/);
      if (tem != null) return tem.slice(1).join(" ").replace("OPR", "Opera");
    }
    matchTest = matchTest[2]
      ? [matchTest[1], matchTest[2]]
      : [navigator.appName, navigator.appVersion, "-?"];
    if ((tem = userAgent.match(/version\/(\d+)/i)) != null)
      matchTest.splice(1, 1, tem[1]);
    return matchTest.join(" ");
  }

  pointingChange() {
    // Local URL
    // location.href = "http://localhost:4200/#/payment-confirmation";
    // Sanity URL
    // location.href = "https://nysa-uat.insurancearticlez.com/nysaui/#/payment-confirmation";
    // location.href = "https://nysa-uat.insurancearticlez.com/produi/#/payment-confirmation";
    // location.href = "https://nysa-uat.insurancearticlez.com/nysaSanity/#/payment-confirmation";
    // Production URL
    location.href = "https://ipartnerms.icicilombard.com/nysa/#/payment-confirmation";
    // location.href = "https://nysa.icicilombard.com/nysa/#/payment-confirmation";
    // location.href = "https://172.30.34.16/nysa/#/payment-confirmation";
    // location.href = "https://172.30.37.139/nysa/#/payment-confirmation";
    //203
    // location.href ="https://ilanysaweb05.prodicicilombard.com/nysa/#/payment-confirmation";
    //212
    // location.href ="https://ilanysaweb04.prodicicilombard.com/nysa/#/payment-confirmation";
    // location.href ="https://ilanysaweb01.prodicicilombard.com/nysa/#/payment-confirmation";
  }

  quotepointing() {
    // Local URL
    // location.href = "http://localhost:4200/#/quote";
    // Sanity URL
    // location.href = "https://nysa-uat.insurancearticlez.com/nysaui/#/quote";
    // location.href = "https://nysa-uat.insurancearticlez.com/produi/#/quote";
    // location.href = "https://nysa-uat.insurancearticlez.com/nysaSanity/#/quote";
    // Production URL
    location.href = "https://ipartnerms.icicilombard.com/nysa/#/quote";
    //  location.href = "https://nysa.icicilombard.com/nysa/#/quote";
    // location.href = "https://172.30.37.139/nysa/#/quote";
    // location.href = "https://172.30.34.16/nysa/#/quote";
    //203
    // location.href ="https://ilanysaweb05.prodicicilombard.com/nysa/#/quote";
    //212
    // location.href ="https://ilanysaweb04.prodicicilombard.com/nysa/#/quote";
    // location.href ="https://ilanysaweb01.prodicicilombard.com/nysa/#/quote";
  }

  /**
   * DigitalPOS API's Change Start
   */

  /**
   * get accessToken for pos user
   * 29-10-2021
   * @param mobileNo
   * @returns
   */
  getPOSPaymsToken(mobileNo) {
    let headers = new HttpHeaders();
    headers = headers.append("UserName", commonData.digitalPOS_username);
    headers = headers.append("Password", commonData.digitalPOS_password);
    headers = headers.append("IPartnerUserID", mobileNo);
    return this.http
      .get(commonData.paymsSanity + "Authenticate", { headers: headers })
      .toPromise();
  }

  getPOSToken(endPoint, body) {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.DIGITAL_URL + endPoint, body, { headers: headers })
      .toPromise();
  }

  GetPOSMobile() {
    this.authToken = JSON.parse(localStorage.getItem("AuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .get(commonData.baseURL + "NysaRedirection/GetPOSMobileNumber", {
        headers: headers,
      })
      .toPromise();
  }

  GetPOSDealId(postObj: any) {
    let authToken = JSON.parse(localStorage.getItem("digitalPosAuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.DIGITAL_URL + "/POS/GetDealId", postObj, {
        headers: headers,
      })
      .toPromise();
  }

  getProductCode(policyNumber: any) {
    let data = policyNumber.split("/");
    if (data[0] == 3001) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Get PaymsToken For POS Customer
   * @param iuserId
   * @returns
   */
  getPaymsTokenForPOSCustomer(iuserId) {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    headers = headers.append("UserName", commonData.digitalPOS_username);
    headers = headers.append("Password", commonData.digitalPOS_password);
    headers = headers.append("IPartnerUserID", iuserId);
    return this.http
      .get(commonData.paymsSanity + "Authenticate", { headers: headers })
      .toPromise();
  }

  // For Payment POS Mobile App
  postPaymentPOS(endPoint: any, body: any, token: any) {
    this.payAuthToken = token;
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.baseURL1 + endPoint, body, { headers: headers })
      .toPromise();
  }

  getManufactureNModelListFull() {
    this.authToken = JSON.parse(localStorage.getItem("AuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .get(
        commonData.baseURL + "ManufactureNModel/GetManufactureNModelListFull",
        { headers: headers }
      )
      .toPromise();
  }

  getVehicleAge(sDate: any, eDate: any) {
    var startDate = new Date(sDate);
    var endDate = new Date(eDate);

    var Time = endDate.getTime() - startDate.getTime();
    let daysDiff = Time / (1000 * 3600 * 24);
    return (daysDiff / 365.25);
    // return Math.round(daysDiff/365.25);
  }

  /**
   * Encryption and decryption of make model list with compression
   * Author :- Sumit
   * date :- 10-02-2022
   * @param value
   */
  encryptLikePythonForRTO(value: any) {
    var key = CryptoJS.enc.Utf8.parse("e78538a1-9bae-4a3e-9bc9-");
    var iv = CryptoJS.enc.Utf8.parse("d45913c1-a-6bc3-");
    var encrypted = CryptoJS.AES.encrypt(
      CryptoJS.enc.Utf8.parse(stringify(value)),
      key,
      {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7,
      }
    );
    return encrypted;
  }

  decryptLikePythonForRTO(value: any) {
    var key = CryptoJS.enc.Utf8.parse("e78538a1-9bae-4a3e-9bc9-");
    var iv = CryptoJS.enc.Utf8.parse("d45913c1-a-6bc3-");
    var decrypted = CryptoJS.AES.decrypt(value, key, {
      keySize: 128 / 8,
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    }).toString(CryptoJS.enc.Utf8);

    return decrypted;
  }

  sortByDOL(data: any) {
    return data.sort((a: any, b: any) => {
      return <any>new Date(b.DateOfLoss) - <any>new Date(a.DateOfLoss);
    });
  }

  NCBLogic(dateOfLoss: any, policyStartDate: any, typeOfClaim: any) {
    var DOLoss = new Date(dateOfLoss);
    var pStartDate = new Date(policyStartDate);
    console.log("For IIB", DOLoss, pStartDate, typeOfClaim);
    var Time = pStartDate.getTime() - DOLoss.getTime();
    let daysDiff = Time / (1000 * 3600 * 24);
    let yearOfLoss = daysDiff / 365.25;
    console.log('For IIB', yearOfLoss);
    if (yearOfLoss < 1 || yearOfLoss == 1) {
      return '0';
    } else if (yearOfLoss > 1 && yearOfLoss <= 2) {
      return '20';
    } else if (yearOfLoss > 2 && yearOfLoss <= 3) {
      return '25';
    } else if (yearOfLoss > 3 && yearOfLoss <= 4) {
      return '35';
    } else if (yearOfLoss > 4 && yearOfLoss <= 5) {
      return '45';
    } else if (yearOfLoss > 5) {
      return '50';
    } else {
      return '0';
    }
  }

  NCBLogicOnDate(policyStartDate: any, lastClaimMadeOn: any) {
    console.log("NCB Logic on date", policyStartDate, lastClaimMadeOn);
    var lastClaimOn = new Date(lastClaimMadeOn);
    var pStartDate = new Date(policyStartDate);
    console.log("For IIB", lastClaimOn, pStartDate);
    var Time = pStartDate.getTime() - lastClaimOn.getTime();
    let daysDiff = Time / (1000 * 3600 * 24);
    let years = daysDiff / 365.25;
    console.log('Year validation NCB', years);
    if (years < 1 || years == 1) {
      return '20';
    } else if (years > 1 && years <= 2) {
      return '25';
    } else if (years > 2 && years <= 3) {
      return '35';
    } else if (years > 3 && years <= 4) {
      return '45';
    } else if (years > 4 && years <= 5) {
      return '50';
    } else {
      return '0';
    }

  }

  createRenewalsToken(endPoint: any, body: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.renewalURL + endPoint, body, { headers: headers })
      .toPromise();
  }

  postForRenewals(endPoint: any, body: any) {
    let renewalAuthToken = JSON.parse(localStorage.getItem("renewalAuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + renewalAuthToken.access);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.renewalURL + endPoint, body, { headers: headers })
      .toPromise();
  }

  getForRenewals(endPoint: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "XI4eJDn9zzR3NQUrFturYsv5uxfAkTNw3Pga/crCM0o=");
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .get(commonData.renewalURL + endPoint, { headers: headers })
      .toPromise();
  }

  postForRenewalsPayment(endPoint: any, body: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.baseURL + endPoint, body, { headers: headers })
      .toPromise();
  }

  //NCB recovery dashboard
  createNcbToken(endPoint: any, body: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.ncbURL + endPoint, body, { headers: headers })
      .toPromise();
  }

  postForNcbDashboard(endPoint: any, body: any) {
    let ncbAuthToken = JSON.parse(localStorage.getItem("AuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + ncbAuthToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.ncbURL + endPoint, body, { headers: headers }).toPromise();
  }

  // getForNcbDashboard(endPoint: any) {
  //   let ncbToken = JSON.parse(localStorage.getItem("NCBToken"));
  //   let headers = new HttpHeaders();
  //   headers = headers.append("Authorization", "Bearer " + ncbToken.token);
  //   headers = headers.append("Content-Type", "application/json");
  //   return this.http
  //     .get(commonData.ncbURL + endPoint, { headers: headers })
  //     .toPromise();
  // }

  postForNcbLinkDashboard(endPoint: any, body: any) {
    let ncbAuthToken = JSON.parse(localStorage.getItem("AuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + ncbAuthToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.ncbURL + endPoint, body, { headers: headers }).toPromise();
  }

  createNcbPayToken(endPoint: any, body: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.ncbPayURL + endPoint, body, { headers: headers })
      .toPromise();
  }

  postForPayNcbDashboard(endPoint: any, body: any) {
    let ncbAuthToken = JSON.parse(localStorage.getItem("AuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + ncbAuthToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.ncbPayURL + endPoint, body, { headers: headers }).toPromise();
  }

  getPaymsTokenNCBPage() {
    let authToken = JSON.parse(localStorage.getItem("NCBToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + authToken);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .get(commonData.baseURL + "Payment/GetPAYMSToken", { headers: headers })
      .toPromise()
      .catch((err: any) => {
        if (err.status === 401 || err.status === 403) {
          window.location.href = commonData.loginURL;
        }
      });
  }

  getForIssuanceReport(){
    let authToken = JSON.parse(localStorage.getItem("AuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    headers = headers.append("Authorization", "Bearer " + authToken.token);
    return this.http
      .get(commonData.baseURL + "Token/GetIpartnerUser", { headers: headers })
      .toPromise();
  }

  postForIssuanceReport(agentID:any){
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post('https://cldilmobilapp01.insurancearticlez.com/MobileAPI/api/common/PostIpartToken', agentID, { headers: headers })
      .toPromise();
  }

  postSCPA(endPoint: any, body: any) {
    let SCPAToken = JSON.parse(localStorage.getItem("SCPAToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + SCPAToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.scpaURL + endPoint, body, { headers: headers })
      .toPromise();
  }

  // tp crawler change by monika
  getCrawlerNewToken(endPoint: any) {
    let body = {
      username: "iwqIU6Z/t8o=",
      password: "VYxkVlge5rq0WHkO7bucmQ==",
    };
    let str = JSON.stringify(body);
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .post(commonData.crawlerURL + endPoint, str, {
        headers: headers,
      })
      .toPromise()
      .catch((err: any) => {
        if (err.status === 401 || err.status === 403) {
          window.location.href = commonData.loginURL;
        }
      });
  }


  //================= Digital POS API's End ===================//

  digitalPOSUserTabChange(tabName) {
    switch (tabName) {
      case "NysaPolicies":
        $("#qQuoteMenu").removeClass("active");
        $("#penPayMenu").removeClass("active");
        $("#saveQoteMenu").removeClass("active");
        $("#myBreakinMenu").removeClass("active");
        $("#qcCasesMenu").removeClass("active");
        $("#nysaPloMenu").addClass("active");
        $("#iasDashboardMenu").removeClass("active");

        $(".tab-content #qQote").removeClass("active show");
        $(".tab-content #nysaPlo").addClass("active show");
        $(".tab-content #penPay").removeClass("active show");
        $(".tab-content #saveQote").removeClass("active show");
        $(".tab-content #myBreakin").removeClass("active show");
        $(".tab-content #qcCases").removeClass("active show");
        $(".tab-content #iasDashboard").removeClass("active show");
        break;
      case "PendingPayment":
        $("#qQuoteMenu").removeClass("active");
        $("#penPayMenu").addClass("active");
        $("#saveQoteMenu").removeClass("active");
        $("#myBreakinMenu").removeClass("active");
        $("#qcCasesMenu").removeClass("active");
        $("#nysaPloMenu").removeClass("active");
        $("#iasDashboardMenu").removeClass("active");

        $(".tab-content #qQote").removeClass("active show");
        $(".tab-content #nysaPlo").removeClass("active show");
        $(".tab-content #penPay").addClass("active show");
        $(".tab-content #saveQote").removeClass("active show");
        $(".tab-content #myBreakin").removeClass("active show");
        $(".tab-content #qcCases").removeClass("active show");
        $(".tab-content #iasDashboard").removeClass("active show");
        break;
      case "BreakIn":
        $("#qQuoteMenu").removeClass("active");
        $("#penPayMenu").removeClass("active");
        $("#saveQoteMenu").removeClass("active");
        $("#myBreakinMenu").addClass("active");
        $("#qcCasesMenu").removeClass("active");
        $("#nysaPloMenu").removeClass("active");
        $("#iasDashboardMenu").removeClass("active");

        $(".tab-content #qQote").removeClass("active show");
        $(".tab-content #nysaPlo").removeClass("active show");
        $(".tab-content #penPay").removeClass("active show");
        $(".tab-content #saveQote").removeClass("active show");
        $(".tab-content #myBreakin").addClass("active show");
        $(".tab-content #qcCases").removeClass("active show");
        $(".tab-content #iasDashboard").removeClass("active show");
        break;
      case "SavedProposal":
        $("#qQuoteMenu").removeClass("active");
        $("#penPayMenu").removeClass("active");
        $("#saveQoteMenu").addClass("active");
        $("#myBreakinMenu").removeClass("active");
        $("#qcCasesMenu").removeClass("active");
        $("#nysaPloMenu").removeClass("active");
        $("#iasDashboardMenu").removeClass("active");

        $(".tab-content #qQote").removeClass("active show");
        $(".tab-content #nysaPlo").removeClass("active show");
        $(".tab-content #penPay").removeClass("active show");
        $(".tab-content #saveQote").addClass("active show");
        $(".tab-content #myBreakin").removeClass("active show");
        $(".tab-content #qcCases").removeClass("active show");
        $(".tab-content #iasDashboard").removeClass("active show");
        break;
      case "QC":
        $("#qQuoteMenu").removeClass("active");
        $("#penPayMenu").removeClass("active");
        $("#saveQoteMenu").removeClass("active");
        $("#myBreakinMenu").removeClass("active");
        $("#qcCasesMenu").addClass("active");
        $("#nysaPloMenu").removeClass("active");
        $("#iasDashboardMenu").removeClass("active");

        $(".tab-content #qQote").removeClass("active show");
        $(".tab-content #nysaPlo").removeClass("active show");
        $(".tab-content #penPay").removeClass("active show");
        $(".tab-content #saveQote").removeClass("active show");
        $(".tab-content #myBreakin").removeClass("active show");
        $(".tab-content #qcCases").addClass("active show");
        $(".tab-content #iasDashboard").removeClass("active show");
        break;
    case "IAS":
        $("#qQuoteMenu").removeClass("active");
        $("#penPayMenu").removeClass("active");
        $("#saveQoteMenu").removeClass("active");
        $("#myBreakinMenu").removeClass("active");
        $("#qcCasesMenu").removeClass("active");
        $("#nysaPloMenu").removeClass("active");
        $("#iasDashboardMenu").addClass("active");

        $(".tab-content #qQote").removeClass("active show");
        $(".tab-content #nysaPlo").removeClass("active show");
        $(".tab-content #penPay").removeClass("active show");
        $(".tab-content #saveQote").removeClass("active show");
        $(".tab-content #myBreakin").removeClass("active show");
        $(".tab-content #qcCases").removeClass("active show");
        $(".tab-content #iasDashboard").addClass("active show");
        break;
      default:
        $("#qQuoteMenu").addClass("active");
        $("#penPayMenu").removeClass("active");
        $("#saveQoteMenu").removeClass("active");
        $("#myBreakinMenu").removeClass("active");
        $("#qcCasesMenu").removeClass("active");
        $("#nysaPloMenu").removeClass("active");
        $("#iasDashboardMenu").removeClass("active");

        $(".tab-content #qQote").addClass("active show");
        $(".tab-content #nysaPlo").removeClass("active show");
        $(".tab-content #penPay").removeClass("active show");
        $(".tab-content #saveQote").removeClass("active show");
        $(".tab-content #myBreakin").removeClass("active show");
        $(".tab-content #qcCases").removeClass("active show");
        $(".tab-content #iasDashboard").removeClass("active show");
        break;
    }
  }

  /**
   * Author :- Sumit
   * date :- 20-01-2022
   * Initialised the StateList and PreviousInsurerList array
   */
  StateListData: any = [
    {
      statE_ID: 165,
      statE_NAME: "ANDAMAN & NICOBAR ISLANDS",
      gsT_STATE_CODE: "35",
      status: null,
      message: null,
    },
    {
      statE_ID: 124,
      statE_NAME: "ANDHRA PRADESH",
      gsT_STATE_CODE: "37",
      status: null,
      message: null,
    },
    {
      statE_ID: 146,
      statE_NAME: "ARUNACHAL PRADESH",
      gsT_STATE_CODE: "12",
      status: null,
      message: null,
    },
    {
      statE_ID: 54,
      statE_NAME: "ASSAM",
      gsT_STATE_CODE: "18",
      status: null,
      message: null,
    },
    {
      statE_ID: 147,
      statE_NAME: "BIHAR",
      gsT_STATE_CODE: "10",
      status: null,
      message: null,
    },
    {
      statE_ID: 160,
      statE_NAME: "CHANDIGARH",
      gsT_STATE_CODE: "4",
      status: null,
      message: null,
    },
    {
      statE_ID: 144,
      statE_NAME: "CHHATTISGARH",
      gsT_STATE_CODE: "22",
      status: null,
      message: null,
    },
    {
      statE_ID: 162,
      statE_NAME: "DADRA & NAGAR HAVELI",
      gsT_STATE_CODE: "26",
      status: null,
      message: null,
    },
    {
      statE_ID: 161,
      statE_NAME: "DAMAN & DIU",
      gsT_STATE_CODE: "25",
      status: null,
      message: null,
    },
    {
      statE_ID: 74,
      statE_NAME: "DELHI",
      gsT_STATE_CODE: "7",
      status: null,
      message: null,
    },
    {
      statE_ID: 145,
      statE_NAME: "GOA",
      gsT_STATE_CODE: "30",
      status: null,
      message: null,
    },
    {
      statE_ID: 67,
      statE_NAME: "GUJARAT",
      gsT_STATE_CODE: "24",
      status: null,
      message: null,
    },
    {
      statE_ID: 127,
      statE_NAME: "HARYANA",
      gsT_STATE_CODE: "6",
      status: null,
      message: null,
    },
    {
      statE_ID: 148,
      statE_NAME: "HIMACHAL PRADESH",
      gsT_STATE_CODE: "2",
      status: null,
      message: null,
    },
    {
      statE_ID: 149,
      statE_NAME: "JAMMU & KASHMIR",
      gsT_STATE_CODE: "1",
      status: null,
      message: null,
    },
    {
      statE_ID: 138,
      statE_NAME: "JHARKHAND",
      gsT_STATE_CODE: "20",
      status: null,
      message: null,
    },
    {
      statE_ID: 59,
      statE_NAME: "KARNATAKA",
      gsT_STATE_CODE: "29",
      status: null,
      message: null,
    },
    {
      statE_ID: 62,
      statE_NAME: "KERALA",
      gsT_STATE_CODE: "32",
      status: null,
      message: null,
    },
    {
      statE_ID: 163,
      statE_NAME: "LAKSHADWEEP",
      gsT_STATE_CODE: "31",
      status: null,
      message: null,
    },
    {
      statE_ID: 64,
      statE_NAME: "MADHYA PRADESH",
      gsT_STATE_CODE: "23",
      status: null,
      message: null,
    },
    {
      statE_ID: 55,
      statE_NAME: "MAHARASHTRA",
      gsT_STATE_CODE: "27",
      status: null,
      message: null,
    },
    {
      statE_ID: 150,
      statE_NAME: "MANIPUR",
      gsT_STATE_CODE: "14",
      status: null,
      message: null,
    },
    {
      statE_ID: 151,
      statE_NAME: "MEGHALAYA",
      gsT_STATE_CODE: "17",
      status: null,
      message: null,
    },
    {
      statE_ID: 152,
      statE_NAME: "MIZORAM",
      gsT_STATE_CODE: "15",
      status: null,
      message: null,
    },
    {
      statE_ID: 153,
      statE_NAME: "NAGALAND",
      gsT_STATE_CODE: "13",
      status: null,
      message: null,
    },
    {
      statE_ID: 192,
      statE_NAME: "NEW DELHI",
      gsT_STATE_CODE: "7",
      status: null,
      message: null,
    },
    {
      statE_ID: 132,
      statE_NAME: "ORISSA",
      gsT_STATE_CODE: "21",
      status: null,
      message: null,
    },
    {
      statE_ID: 774,
      statE_NAME: "OTHER TERRITORY",
      gsT_STATE_CODE: "97",
      status: null,
      message: null,
    },
    {
      statE_ID: 164,
      statE_NAME: "PONDICHERRY",
      gsT_STATE_CODE: "34",
      status: null,
      message: null,
    },
    {
      statE_ID: 69,
      statE_NAME: "PUNJAB",
      gsT_STATE_CODE: "3",
      status: null,
      message: null,
    },
    {
      statE_ID: 56,
      statE_NAME: "RAJASTHAN",
      gsT_STATE_CODE: "8",
      status: null,
      message: null,
    },
    {
      statE_ID: 154,
      statE_NAME: "SIKKIM",
      gsT_STATE_CODE: "11",
      status: null,
      message: null,
    },
    {
      statE_ID: 58,
      statE_NAME: "TAMIL NADU",
      gsT_STATE_CODE: "33",
      status: null,
      message: null,
    },
    {
      statE_ID: 434,
      statE_NAME: "TELANGANA",
      gsT_STATE_CODE: "36",
      status: null,
      message: null,
    },
    {
      statE_ID: 155,
      statE_NAME: "TRIPURA",
      gsT_STATE_CODE: "16",
      status: null,
      message: null,
    },
    {
      statE_ID: 65,
      statE_NAME: "UTTAR PRADESH",
      gsT_STATE_CODE: "9",
      status: null,
      message: null,
    },
    {
      statE_ID: 342,
      statE_NAME: "UTTARAKHAND",
      gsT_STATE_CODE: "5",
      status: null,
      message: null,
    },
    {
      statE_ID: 156,
      statE_NAME: "UTTARANCHAL",
      gsT_STATE_CODE: "5",
      status: null,
      message: null,
    },
    {
      statE_ID: 72,
      statE_NAME: "WEST BENGAL",
      gsT_STATE_CODE: "19",
      status: null,
      message: null,
    },
  ];

  PreviousInsurerList: any = [
    {
      companyCode: "ACKO",
      companyName: "ACKO GENERAL INSURANCE LIMITED",
      shortName: "ACKO",
      message: null,
    },
    {
      companyCode: "BANZ",
      companyName: "BAJAJ ALLIANZ GENERAL INSURANCE COMPANY LIMITED",
      shortName: "BAJAJ ALIANZ",
      message: null,
    },
    {
      companyCode: "BAXA",
      companyName: "BHARATI AXA GENERAL INSURANCE CO. LTD.",
      shortName: "BHARATI AXA",
      message: null,
    },
    {
      companyCode: "CHOLA",
      companyName: "CHOLAMANDALAM MS GENERAL INSURANCE COMPANY LTD.",
      shortName: "CHOLAMANDALAM",
      message: null,
    },
    {
      companyCode: "DHFL",
      companyName: "DHFL GENERAL INSURANCE LIMITED",
      shortName: "DHFL",
      message: null,
    },
    {
      companyCode: "EDEL",
      companyName: "EDELWEISS GENERAL INSURANCE COMPANY",
      shortName: "EDELWEISS GENER",
      message: null,
    },
    {
      companyCode: "FGICL",
      companyName: "FUTURE GENERALI INDIA INSURANCE CO LTD.",
      shortName: "FGI",
      message: null,
    },
    {
      companyCode: "GODI",
      companyName: "GO DIGIT GENERAL INSURANCE CO LTD",
      shortName: "GODIGIT",
      message: null,
    },
    {
      companyCode: "HDFC",
      companyName: "HDFC ERGO GENERAL INSURANCE COMPANY LTD.",
      shortName: "HDFC ERGO",
      message: null,
    },
    {
      companyCode: "ICLB",
      companyName: "ICICI LOMBARD GENERAL INSURANCE COMPANY LTD.",
      shortName: "ICICI LOMBARD",
      message: null,
    },
    {
      companyCode: "IFTM",
      companyName: "IFFCO TOKIO GENERAL INSURANCE COMPANY LIMITED",
      shortName: "IFFCO TOKIO",
      message: null,
    },
    {
      companyCode: "KOTA",
      companyName: "KOTAK GENERAL INSURANCE COMPANY LIMITED",
      shortName: "KOTAK GENERAL",
      message: null,
    },
    {
      companyCode: "LIBE",
      companyName: "LIBERTY VIDEOCON GENERAL INSURANCE COMPANY LIMITED",
      shortName: "LIBERTY",
      message: null,
    },
    {
      companyCode: "MAGM",
      companyName: "MAGMA HDI GENERAL INSURANCE COMPANY LIMIED",
      shortName: "MAGM",
      message: null,
    },
    {
      companyCode: "NIC",
      companyName: "NATIONAL INSURANCE COMPANY LIMITED",
      shortName: "NATIONAL",
      message: null,
    },
    {
      companyCode: "OIC",
      companyName: "ORIENTAL INSURANCE COMPANY LTD.",
      shortName: "ORIENTAL",
      message: null,
    },
    {
      companyCode: "HO",
      companyName: "RAHEJA QBE GENERAL INSURANCE COMPANY LIMITED",
      shortName: "RAHEJA QBE",
      message: null,
    },
    {
      companyCode: "RGIC",
      companyName: "RELIANCE GENERAL INSURANCE COMPANY LTD.",
      shortName: "RELIANCE",
      message: null,
    },
    {
      companyCode: "RSAICL",
      companyName: "ROYAL SUNDARAM",
      shortName: "ROYAL SUNDARAM",
      message: null,
    },
    {
      companyCode: "SBI",
      companyName: "SBI GENERAL INSURANCE COMPANY LIMITED",
      shortName: "SBI GIC",
      message: null,
    },
    {
      companyCode: "SGIC",
      companyName: "SHRIRAM GENERAL INSURANCE COMPANY",
      shortName: "SHRIRAM GIC",
      message: null,
    },
    {
      companyCode: "TAIG",
      companyName: "TATA AIG",
      shortName: "TATA AIG",
      message: null,
    },
    {
      companyCode: "NIA",
      companyName: "THE NEW INDIA ASSURANCE COMPANY LTD.",
      shortName: "NIA",
      message: null,
    },
    {
      companyCode: "UIIC",
      companyName: "UNITED INDIA INSURANCE COMPANY LTD.",
      shortName: "UNITED INDIA",
      message: null,
    },
    {
      companyCode: "USGI",
      companyName: "UNIVERSAL SOMPO GENERAL INSURANCE CO. LTD.",
      shortName: "UNIVERSAL SOMPO",
      message: null,
    },
  ];

  // PreviousInsurerList: any = [
  //   {
  //     "companyCode": "GIC",
  //     "companyName": "GENERAL INSURANCE CORPORATION OF INDIA",
  //     "shortName": "GIC",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "ICLB",
  //     "companyName": "ICICI LOMBARD GENERAL INSURANCE COMPANY LTD.",
  //     "shortName": "ICICI LOMBARD",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "NIA",
  //     "companyName": "THE NEW INDIA ASSURANCE COMPANY LTD.",
  //     "shortName": "NIA",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "UIIC",
  //     "companyName": "UNITED INDIA INSURANCE COMPANY LTD.",
  //     "shortName": "UNITED INDIA",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "NIC",
  //     "companyName": "NATIONAL INSURANCE COMPANY LIMITED",
  //     "shortName": "NATIONAL",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "OIC",
  //     "companyName": "ORIENTAL INSURANCE COMPANY LTD.",
  //     "shortName": "ORIENTAL",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "RGIC",
  //     "companyName": "RELIANCE GENERAL INSURANCE COMPANY LTD.",
  //     "shortName": "RELIANCE",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "RSUND",
  //     "companyName": "ROYAL SUNDARAM",
  //     "shortName": "ROYAL SUNDARAM",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "TAIG",
  //     "companyName": "TATA AIG",
  //     "shortName": "TATA AIG",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "IFTM",
  //     "companyName": "IFCO TOKYO MARINE",
  //     "shortName": "IFFCO TOKIO",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "BANZ",
  //     "companyName": "BAJAJ ALLIANZ",
  //     "shortName": "BAJAJ ALIANZ",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "CHOLA",
  //     "companyName": "CHOLAMANDALAM MS GENERAL INSURANCE COMPANY LTD.",
  //     "shortName": "CHOLAMANDALAM",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "HDFC",
  //     "companyName": "HDFC CHUBB GENERAL INSURANCE COMPANY LTD.",
  //     "shortName": "HDFC CHUBB   ",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "UNITED INDIA",
  //     "companyName": "UNITED INDIA INSURANCE COMPANY LTD.",
  //     "shortName": "UIIC",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "FGIC",
  //     "companyName": "FUTURE GENERALI INDIA INSURANCE CO LTD.",
  //     "shortName": "FGI",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "USGI",
  //     "companyName": "UNIVERSAL SOMPO GENERAL INSURANCE CO. LTD.",
  //     "shortName": "USGI",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "BAJAJ ALLIANZ GIC",
  //     "companyName": "BAJAJ ALLIANZ GENERAL INSURANCE CO. LTD.",
  //     "shortName": "BAJAJ ALLIANZ",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "SHRI RAM GIC",
  //     "companyName": "SHRI RAM GENERAL INSURANCE CO. LTD.",
  //     "shortName": "SHRI RAM GIC",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "L&T GIC",
  //     "companyName": "L&T GENERAL INSURANCE CO. LTD.",
  //     "shortName": "L&T GIC",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "AXA",
  //     "companyName": "AXA INSURANCE COMPANY",
  //     "shortName": "AXA INS. CO.",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "L&T",
  //     "companyName": "L&T GENERAL INSURANCE CO. LTD..",
  //     "shortName": "L GIC",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "MAGMA",
  //     "companyName": "MAGMA HDI GENERAL INSURANCE CO.LTD.",
  //     "shortName": "MAGMA",
  //     "message": null
  //   },
  //   {
  //     "companyCode": "BAXA",
  //     "companyName": "BHARATI AXA GENERAL INSURANCE CO.LTD.",
  //     "shortName": "BHARATI AXA",
  //     "message": null
  //   }
  // ]

  getManufactureNModelListAllRisk() {
    this.authToken = JSON.parse(localStorage.getItem("AuthToken"));
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + this.authToken.token);
    headers = headers.append("Content-Type", "application/json");
    return this.http
      .get(
        commonData.baseURL + "ManufactureNModel/GetManufactureNModelListAllRisk",
        { headers: headers }
      )
      .toPromise();
  }
}
