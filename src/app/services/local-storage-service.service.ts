import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageServiceService {

  constructor() { }

  saveToLocalStorage(key, value){
    localStorage.setItem(key, value)
  }

  removeFromLocalStorage(key){
    localStorage.removeItem(key)
  }

  findInLocalStorage(key){
    return localStorage.getItem(key)
  }
}
