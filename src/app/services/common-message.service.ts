import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonMessageService {

  constructor() { }

  errorMsg(inputMsg:any){
    if(inputMsg.includes('Tenure not found for combination of RTO Cluster')){
      return 'We do not have a verified quote for your RTO Code and Vehicle Model combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.';
    }else if(inputMsg.includes('Tariff Rate not found for combination of RTO Cluster')){
      return 'We do not have a verified tariff rate for your RTO Code and Vehicle Model combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.';
    }else if(inputMsg.includes('Tenure value  not available from  both Masters')){
      return 'Kindly ask your relationship manager to get the respective Tenure checked.';
    }else if(inputMsg.includes('Base Rate not found for "theInput.brmsVehicleDetails.coverNameinWF')){
      return 'We do not have a verified tariff rate for the selected <cover name> add-on for your vehicle. Please get in touch with your relationship manager to help you get a quote for your vehicle.'
    }else if(inputMsg.includes('No rule is executed from the Addon Route Map Master for the cover')){
      return 'We  do not have a verified premium rate for your vehicle model and RTO Code combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.'
    }else if(inputMsg.includes('Cover not found: "it.riskCoverName')){
      return 'We  do not have a verified quote for the selected coverage type, vehicle model and RTO combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.';
    }else if(inputMsg.includes('Policy is Declined as GenericTP is false in Routemap')){
      return 'We  were unable to verify your Third-party insurance policy details for the entered vehicle number and RTO combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.'
    }else if(inputMsg.includes('Policy is Declined as GenericTP and ExceptionalTP are false in Routemap')){
      return 'We  were unable to verify your Third-party insurance policy details for the entered vehicle number and RTO combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.'
    }else if(inputMsg.includes('BasicTp Premium not Found from Generic/ExceptionalTP Master , hence Policy is Declined.')){
      return 'We  were unable to verify your Third-party insurance policy details for the entered vehicle number and RTO combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.'
    }else if(inputMsg.includes('TBasicTrailer TPPremium is not  Found from BasicTPTrailerMaster , hence Policy is Declined.')){
      return 'We  were unable to verify the Third-party insurance premium details for your vehicle. Please get in touch with your relationship manager to help you get a quote for your vehicle.';
    }else if(inputMsg.includes('There is no rule executed in the IMT Tariff Master , hence the policy is declined')){
      return 'Tariff rates for your vehicle details is not defined by India Motor Tariff board. Please get in touch with your relationship manager to help you get a quote.'
    }else if(inputMsg.includes('The Generic OD Master is required for final Basic OD Rate Calculation but no rule is executed from the Generic OD Master , hence the policy is declined')){
      return 'We  do not have a verified Own Damage coverage tariff rate for your Vehicle model and RTO Code combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.'
    }else if(inputMsg.includes('The IL Tariff Master is required for final Basic OD Rate Calculation but no rule is executed from the IL Tariff Master , hence the policy is declined')){
      return 'We  do not have a verified quote for the selected coverage type, vehicle model and RTO combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.';
    }else if(inputMsg.includes('Generic Master or IL Tariff Master is required for final Basic OD Rate Calculation but Both are set false in the Route Map, hence the policy is declined')){
      return 'We  do not have a verified quote for the selected coverage type, vehicle model and RTO combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.';
    }else if(inputMsg.includes('There is no rule executed from the Exceptional OD Master and Generic and IL Tariff Masters are false in the Route Map Master, hence the policy is declined')){
      return 'We  do not have a verified quote for the selected coverage type, vehicle model and RTO combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.';
    }else if(inputMsg.includes('There is no rule executed from the Generic OD Master , hence the policy is declined')){
      return 'We  do not have a verified quote for the selected coverage type, vehicle model and RTO combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.';
    }else if(inputMsg.includes('There is no rule executed from the IL Tariff Master , hence the policy is declined')){
      return 'We  do not have a verified quote for the selected coverage type, vehicle model and RTO combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.';
    }else if(inputMsg.includes('There is no rule executed from the Basic OD Trailer Master , hence the policy is declined')){
      return 'We  do not have a verified quote for the selected coverage type, vehicle model and RTO combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.';
    }else if(inputMsg.includes('There is no rule executed in the IMT Tariff Master , hence the policy is declined')){
      return 'We  do not have a verified quote for the selected coverage type, vehicle model and RTO combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.';
    }else if(inputMsg.includes('There is no rule executed from the Fixed OD Master , hence the policy is declined')){
      return 'We  do not have a verified quote for the selected coverage type, vehicle model and RTO combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.';
    }else if(inputMsg.includes('The Loading or Discount on Basic OD Rate does not lie between the limits set in the Rate Validation Master , hence the policy is declined')){
      return 'Loading limit: It looks like you have exceeded the limit to avail of additional coverage (loading limit) for your vehicle. Please get in touch with our relationship manager to get a quote. Discount: It looks like you have exceeded the discount limit that you can avail of for this vehicle registration. Please get in touch with our relationship manager to get a quote.';
    }else if(inputMsg.includes('There is no rule satisfying in the Rate Validation Master , hence the policy is declined')){
      return 'We  do not have a verified quote for the selected coverage type, vehicle model and RTO combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.';
    }else if(inputMsg.includes('There is no rule executed in the Route Map Master , hence the policy is declined')){
      return 'We  do not have a verified quote for the selected coverage type, vehicle model and RTO combination. Please get in touch with your relationship manager to help you get a quote for your vehicle.';
    }else{
      return inputMsg;
    }
  }
}
