import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, pipe, Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SendMessageService {
  private quotereq = new BehaviorSubject<any>({});
  private quoteres = new BehaviorSubject<any>({});
  private propreq = new BehaviorSubject<any>({});
  private propres = new BehaviorSubject<any>({});
  private phdata = new BehaviorSubject<any>({});
  constructor() {}

  // getMessage(){
  //   return this.subject.getValue();
  // }

  sendphdata(message: any) {
    this.phdata.next(message);
  }
  Receievephdata(): Observable<any> {
    return this.phdata.asObservable();
  }

  // Quote Request
  sendQuotereqData(message: any) {
    this.quotereq.next(message);
  }
  ReceieveQuotereqData(): Observable<any> {
    return this.quotereq.asObservable();
  }

  // Quote Response
  sendQuoteresData(message: any) {
    this.quoteres.next(message);
  }
  ReceieveQuoteresData(): Observable<any> {
    return this.quoteres.asObservable();
  }

  // Proposal Request
  sendproposalreqData(message: any) {
    this.propreq.next(message);
  }
  ReceieveproposalreqData(): Observable<any> {
    return this.propreq.asObservable();
  }

  // Proposal Response
  sendproposalresData(message: any) {
    this.propres.next(message);
  }
  ReceieveproposalresData(): Observable<any> {
    return this.propres.asObservable();
  }
}
