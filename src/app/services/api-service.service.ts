import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { commonData } from "../commonData/commonData";
import { CommonService } from "src/app/services/common.service";
import { Observable } from "rxjs";
import swal from "sweetalert";
import { parse } from "zipson";
@Injectable({
  providedIn: "root",
})
export class ApiServiceService {
  constructor(public http: HttpClient, public cs: CommonService) {}
  //GetRTOLocation - for getRTO
  getRTOLocation(rtoCode: any, classCode: any) {
    return this.cs.get(
      "RTO/GetRTOLocationName?rtoLocationCode=" +
        rtoCode +
        "&classCode=" +
        classCode
    );
  }
  //GetManufacturerModel - for getRTO
  getModelName(modelCode: any, classCode: any) {
    return this.cs.get(
      "RTO/GetVehicleDetails?modelCode=" + modelCode + "&classCode=" + classCode
    );
  }
  //getManufacturerModel
  getManuModelDetails(modelName: any, classCode: any) {
    return this.cs.get(
      "ManufactureNModel/GetManufactureNModelList?manufactureName=" +
        modelName +
        "&classCode=" +
        classCode
    );
  }
  //getInsurerName
  getInsurerName(insurername: any) {
    return this.cs.get(
      "PrevPolicy/GetPreviousInsurer?insurerName=" + insurername
    );
  }

  // ObjectSorting
  sortObjectByKey(object:any){
    return Object.assign(Object.entries(object).sort().map(([key, value]) => {
      return {
         [key]: value
      }
   }));
  }
  
  isCheck(count:any){
    let count1 = count/2;
    console.log(count1);
    if(count1%2 == 0){
      return false;
    }else{
      return true;
    }
  }

  getPreviousInsurerName() {
    /**
     * Added Condition On Calling PrevPolicy/GetPreviousInsurer api if data is already present in localStorage
     * Author :- Sumit
     * date :- 12-01-2022
     */
    return this.cs.PreviousInsurerList;
    // if (localStorage.getItem("PreviousInsurerList")) {
    //   let decryptedPreviousInsurerList = localStorage.getItem(
    //     "PreviousInsurerList"
    //   );
    //   let PreviousInsurerList = this.decrypt(decryptedPreviousInsurerList);
    //   return JSON.parse(PreviousInsurerList);
    // } else {
    //   this.cs.get("PrevPolicy/GetPreviousInsurer").subscribe((res: any) => {
    //     let encryptedPreviousInsurerList = this.encrypt(res);
    //     localStorage.setItem(
    //       "PreviousInsurerList",
    //       encryptedPreviousInsurerList
    //     );
    //     return res;
    //   });
    // }
  }

  /**
   * get manufacture model list from localStorage
   * Author :- Sumit
   * date :- 31-01-2022
   */
  // getManufactureModelListFromLocalStorage() {
  //   if (localStorage.getItem("ManufactureModelList")) {
  //     let encryptedrtoList = localStorage.getItem("ManufactureModelList");
  //     let decryptedrtoList = JSON.parse(this.decrypt(encryptedrtoList));
  //     return decryptedrtoList;
  //   } else {
  //     this.cs.getManufactureNModelListFull().then((resp: any) => {
  //       console.log(resp);
  //       // let ManufactureModelList = this.encrypt(resp);
  //       // localStorage.setItem("ManufactureModelList", ManufactureModelList);
  //       let manumod_4w = resp.filter((option) => {
  //         return (
  //           option.vehicleclasscode == "45"
  //         );
  //       });
  //       let ManufactureModelList_4w = this.encrypt(manumod_4w);
  //       localStorage.setItem("ManufactureModelList_4w", ManufactureModelList_4w);
  //       let manumod_2w = resp.filter((option) => {
  //         return (
  //           option.vehicleclasscode == "37"
  //         );
  //       });
  //       let ManufactureModelList_2w = this.encrypt(manumod_2w);
  //       localStorage.setItem("ManufactureModelList_2w", ManufactureModelList_2w);
  //     });
  //   }
  // }

  manufactureListNew(productCode: any) {
    if (
      localStorage.getItem("ManufactureModelList_4w") ||
      localStorage.getItem("ManufactureModelList_2w") ||
      localStorage.getItem("ManufactureModelList_AllRisk")
    ) {
      if (productCode == "2312" || productCode == "2320") {
        let encryptedrtoList = localStorage.getItem("ManufactureModelList_2w");
        let decryptedrtoList =
          this.cs.decryptLikePythonForRTO(encryptedrtoList);
        const parsed = parse(decryptedrtoList);
        return parsed;
      } else if (productCode == "2311" || productCode == "2319") {
        let encryptedrtoList = localStorage.getItem("ManufactureModelList_4w");
        let decryptedrtoList =
          this.cs.decryptLikePythonForRTO(encryptedrtoList);
        const parsed = parse(decryptedrtoList);
        return parsed;
      } else if (productCode == commonData.allriskProductCode) {
        let encryptedrtoList = localStorage.getItem("ManufactureModelList_AllRisk");
        let decryptedrtoList =
          this.cs.decryptLikePythonForRTO(encryptedrtoList);
        const parsed = parse(decryptedrtoList);
        return parsed;
      }
    } else {
      if (productCode == commonData.allriskProductCode) {
        this.cs.getManufactureNModelListAllRisk().then((resp: any) => {
          let manumod_allRisk = resp;
          return manumod_allRisk;
        });
      } else {
        this.cs.getManufactureNModelListFull().then((resp: any) => {
          if (productCode == "2312" || productCode == "2320") {
            let manumod_4w = resp.makeNModelOptimiseRS.filter((option: any) => {
              return option.vehicleclasscode == "45";
            });
            return manumod_4w;
          } else if (productCode == "2311" || productCode == "2319") {
            let manumod_2w = resp.makeNModelOptimiseRS.filter((option: any) => {
              return option.vehicleclasscode == "37";
            });
            return manumod_2w;
          }
        });
      }
      
    }
  }

  key: any = CryptoJS.enc.Utf8.parse(commonData.encryption_key);
  iv: any = CryptoJS.enc.Utf8.parse(commonData.encryption_key);
  encrypt(data: any) {
    if (data != undefined && data != null) {
      let EncryptedJSON = CryptoJS.AES.encrypt(
        CryptoJS.enc.Utf8.parse(JSON.stringify(data)),
        this.key,
        {
          keySize: 128 / 8,
          iv: this.iv,
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7,
        }
      ).toString();
      return EncryptedJSON;
    } else {
      return null;
    }
  }

  decrypt(data: any) {
    if (data != undefined && data != null) {
      let key: any = CryptoJS.enc.Utf8.parse(commonData.encryption_key);
      let iv: any = CryptoJS.enc.Utf8.parse(commonData.encryption_key);
      let DecryptedJSON = CryptoJS.AES.decrypt(data, key, {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7,
      }).toString(CryptoJS.enc.Utf8);

      return DecryptedJSON;
    } else {
      return null;
    }
  }

  //getRTO Details
  getRTODetails(regNum: any) {
    return this.cs.getRTO(
      "RTODetails/GetRTODetails?registrationNumber=" + regNum
    );
  }
  //freezeRTO
  freezeRTOData(code: any, classCode: any) {
    return this.cs.getRTO(
      "RTO/GetRTO?regCode=" + code + "&classCode=" + classCode
    );
  }

  //getRTO Details Amit
  getRTODetailsForVahaan(body: any) {
    return this.cs.getRTORedis("ZoopVehicleDetails", body);
  }

  // For Sanity
  // getRTODetailsForVahaanNew(body: any) {
  //   return this.cs.getRTORedis("vahanAdvanced/ClaimVahanDetails", body);
  // }

  // For Prod
  getRTODetailsForVahaanNew(body: any) {
    return this.cs.getRTORedis("ClaimVahanDetails", body);
  }
  
  // RefreshToken
  refreshToken() {
    return this.cs.get("Token/RefreshToken");
  }
  
  //PolicyTenure
  calculatePrevTenure(prevstartdate: any, prevEndDate: any) {
    return this.cs.get(
      "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
        prevstartdate +
        "&prevEndDate=" +
        prevEndDate
    );
  }
  //getDeal
  getDeal(username: any, productCode: any) {
    return this.cs.get(
      "Deal/GetDealFromIM?imID=" + username + "&productCode=" + productCode
    );
  }
  //getDealDetails
  getDealDetails(dealid: any) {
    return this.cs.get("Deal/GetDealDetailsFromDeal?dealID=" + dealid);
  }
  //exshowroom - 2w
  get2Wexshow(
    modelName: any,
    modelcode: any,
    productCode: any,
    firstReg: any,
    polstar: any
  ) {
    return this.cs.get(
      "ManufactureNModel/GetExShowroomPriceTW?manufacturer=" +
        modelName +
        "&modelcode=" +
        modelcode +
        "&productCode=" +
        productCode +
        "&firstRegDate=" +
        firstReg +
        "&policyStartDate=" +
        polstar
    );
  }
  //exshowroom - 4w
  get4wexshow(
    rtoCode,
    modelName,
    modelcode,
    makecode,
    productCode,
    firstReg,
    polstar
  ) {
    return this.cs.get(
      "ManufactureNModel/GetExShowroomPriceFW?rtocode=" +
        rtoCode +
        "&manufacturer=" +
        modelName +
        "&modelcode=" +
        modelcode +
        "&makeCode=" +
        makecode +
        "&productCode=" +
        productCode +
        "&firstRegDate=" +
        firstReg +
        "&policyStartDate=" +
        polstar
    );
  }
  //getHospifund
  getHospi(imid) {
    return this.cs.get("Deal/GetHospiFundDealFromIM?imid=" + imid);
  }
  //saveHospifund
  savehf(body) {
    return this.cs.post("Hospifund/ProceedToHospifund", body);
  }
  //sendMail&SMS
  sendmailsms(body) {
    return this.cs.post(
      "CustomerCommunication/SendPaymentConfirmationOnEMAILAndMobile",
      body
    );
  }
  //getSubLocation
  getSubLocation(postObj) {
    return this.cs.get("BreakIn/GetSubLocation?locationName=" + postObj);
  }
  // getiibServices
  getiibServices(regno) {
    return this.cs.get(
      "IIBClaim/GetClaimStatus?VehicleRegistrationNo=" + regno
    );
  }
  //errorlogs
  adderrorlogs(body) {
    return this.cs.post("Tracker/AddError", body);
  }
  // Aadrilla Service
  getAadrillaData(regno) {
    return this.cs.get(
      "RTODetails/AadrilaGetRTODetails?registrationNumber=" + regno
    );
  }

  searchTPvalidation(string: any) {
    var res = string.match(
      /TP Policy End Date should be greater than OD policy Start Date/g
    );
    if (res != null) {
      return true;
    } else {
      return false;
    }
  }
  searchword(string: any) {
    var res = string.match(
      /ClsBLZRuleDefinition|GetBlazeDateTypeCheck|BLAZE status is  DECLINE|BLAZE/g
    );
    if (res != null) {
      return true;
    } else {
      return false;
    }
  }

  searchpathfinder(string: any) {
    var res = string.match(/pathfinder/g);
    if (res != null) {
      return true;
    } else {
      return false;
    }
  }

  searchloadingdiscounterror(string: any) {
    var res = string.match(
      /The Loading or Discount on Basic OD Rate does not lie between the limits set in the Rate Validation Master , hence the policy is declined/g
    );
    if (res != null) {
      return true;
    } else {
      return false;
    }
  }

  invalidmanufacture(string: any) {
    var res = string.match(/VEHICLE MANUFACTURER CODE/g);
    if (res != null) {
      return true;
    } else {
      return false;
    }
  }

  tperror(string: any) {
    var res = string.match(/GenericTP | ExceptionalTP/g);
    if (res != null) {
      return true;
    } else {
      return false;
    }
  }

  consumables(string: any) {
    var res = string.match(/Base Rate not found for Consumables/g);
    if (res != null) {
      return true;
    } else {
      return false;
    }
  }

  ncb(string: any) {
    var res = string.match(
      /90 days gap|previous policy and the current policy|No Claim Bonus/g
    );
    if (res != null) {
      return true;
    } else {
      return false;
    }
  }

  exshowroom(string: any) {
    var res = string.match(/ex-showroom|exceeded|limit allowable/g);
    if (res != null) {
      return true;
    } else {
      return false;
    }
  }

  ZerDep(string: any) {
    var res = string.match(/ZeroDepreciation/g);
    if (res != null) {
      return true;
    } else {
      return false;
    }
  }

  specialcharacter(string: any) {
    var res = string.match(/Special Characters/g);
    if (res != null) {
      return true;
    } else {
      return false;
    }
  }

  dealExpired(string: any) {
    var res = string.match(/Deal Expired/g);
    if (res != null) {
      return true;
    } else {
      return false;
    }
  }

  prevpolerr(string: any) {
    var res = string.match(
      /Previous Policy End Date Should Be Less Than Current Policy Effective Date/g
    );
    if (res != null) {
      return true;
    } else {
      return false;
    }
  }

  servicefail(string: any) {
    var res = string.match(/Service Call Failed/g);
    if (res != null) {
      return true;
    } else {
      return false;
    }
  }

  financialerror(string: any) {
    var res = string.match(
      /Past Policy-Financial Year|Current Policy-Financial Year /g
    );
    if (res != null) {
      return true;
    } else {
      return false;
    }
  }

  handleerrors(error) {
    let blazeerror = this.searchword(error);
    let pathfinderror = this.searchpathfinder(error);
    let TPvalidate = this.searchTPvalidation(error);
    let loaddiserror = this.searchloadingdiscounterror(error);
    let manufacturercode = this.invalidmanufacture(error);
    let consumables = this.consumables(error);
    let TPerror = this.tperror(error);
    let NCB = this.ncb(error);
    let exshow = this.exshowroom(error);
    let zd = this.ZerDep(error);
    let specialchar = this.specialcharacter(error);
    let dealexpire = this.dealExpired(error);
    let prevpolerr = this.prevpolerr(error);
    let servicefail = this.servicefail(error);
    let finerror = this.financialerror(error);
    if (blazeerror == true) {
      error = "blaze";
    }
    if (pathfinderror == true) {
      error = "pathfinder";
    }
    if (TPvalidate == true) {
      error = "TPvalidate";
    }
    if (loaddiserror == true) {
      error = "loaddiserror";
    }
    if (manufacturercode == true) {
      error = "invalidmanucode";
    }
    if (TPerror == true) {
      error = "tperr";
    }
    if (consumables == true) {
      error = "consume";
    }
    if (NCB == true) {
      error = "ncberror";
    }
    if (exshow == true) {
      error = "exshowerr";
    }
    if (zd == true) {
      error = "zderr";
    }
    if (specialchar == true) {
      error = "special";
    }
    if (dealexpire == true) {
      error = "dealexpire";
    }
    if (prevpolerr == true) {
      error = "preverr";
    }
    if (servicefail == true) {
      error = "servicefailerr";
    }
    if (finerror == true) {
      error = "finerr";
    }
    switch (error) {
      case "BonusOnPreviousPolicyFXD does not have a valid value":
        error = "Bonus On Previous Policy does not have a valid value";
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
      case "invalidmanucode":
        error =
          "We could not fetch correct model. Kindly insert model details manually";
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
      case "tperr":
        error = "This segment is not eligible for booking TP policy";
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
      case "finerr":
        error =
          "Previous policy end date should be less than the current year policy start date. Rectify the previous year date and proceed";
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
      case "preverr":
        error =
          "Previous policy end date should be less than the current year policy start date. Rectify the previous year date and proceed";
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
      case "servicefailerr":
        error =
          "Unable to fetch plan details, please re-initiate the transaction by entering registration number";
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
      case "zderr":
        error = "Base Rate not found for Zero Depreciation";
        this.cs.loaderStatus = false;
        // swal({closeOnClickOutside: false, text: error })
        break;
      case "special":
        error =
          "Please enter only alphabets and number in Engine number field. Don’t enter special characters. ";
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
      case "dealexpire":
        error =
          "Deal id is expired/deactivated, please ask your Relationship manager to activate the deal";
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
      case "ncberror":
        error =
          "No claim bonus is not applicable, as the gap between previous policy end date and current policy start date is more than 90 days";
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
      case "exshowerr":
        error =
          "The ex-showroom price entered has exceeded the limit allowable";
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
      case "consume":
        error = "Consumable is not available for this vehicle age";
        this.cs.loaderStatus = false;
        // swal({closeOnClickOutside: false, text: error })
        break;
      case "Not a valid Voluntary Excess Value":
        error = "Not a valid Voluntary Excess Value";
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
      // case 'Error in Calculate: \n No Claim Bonus Is Not Applicable as,there is more than 90 days gap between the previous policy and the current policy.':
      //   error = 'No claim bonus is not applicable, as the gap between previous policy end date and current policy start date is more than 90 days'
      //   this.cs.loaderStatus = false;
      //   swal({closeOnClickOutside: false, text: error })
      // break;
      case "blaze":
        error =
          "Rule is not set in Blaze. Please get in touch with your Relationship manager";
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
      case "pathfinder":
        error =
          "There is error in Pathfinder server. Please get in touch with your Relationship manager";
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
      case "TPvalidate":
        error =
          "TP policy end date should be greater than OD policy start date";
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
      case "loaddiserror":
        error =
          "The Loading or Discount on Basic OD Rate does not lie between the limits set in the Rate Validation Master , hence the policy is declined";
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
      default:
        // error = 'At present system response is slow, kindly try after sometime. We regret the inconvenience';
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: error });
        break;
    }
  }

  getmsg(err) {
    return err;
  }
}
