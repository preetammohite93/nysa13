import { TestBed } from '@angular/core/testing';

import { CommonMessageService } from './common-message.service';

describe('CommonMessageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommonMessageService = TestBed.get(CommonMessageService);
    expect(service).toBeTruthy();
  });
});
