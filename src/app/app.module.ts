import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { FormsModule } from "@angular/forms";
import { SharedtranslateModule } from "./shared/sharedtranslate/sharedtranslate.module";
import { SharedModule } from "./shared/shared.module";
import { LombardpayWalletModule } from './pages/lombardpay-wallet/lombardpay-wallet.module';
import { TechopsLoginModule } from './pages/techops-login/techops-login.module';
import { TechopModule } from './pages/techop/techop.module';
import { QuoteModule } from './pages/quote/quote.module';
import { PhviewdocumentModule } from './pages/phviewdocument/phviewdocument.module';
import { PhloginModule } from './pages/phlogin/phlogin.module';
import { PhDashboardModule } from './pages/ph-dashboard/ph-dashboard.module'
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader'; 

import { NgxPaginationModule } from 'ngx-pagination';

import { ImgMagnifier } from "ng-img-magnifier";

import { AppComponent } from "./app.component";

import { PaymentComponent } from "./pages/payment/payment.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatDatepickerModule } from "@angular/material/datepicker";
import {
  DateAdapter,
  MatNativeDateModule,
  MAT_DATE_LOCALE,
  MAT_DATE_FORMATS,
} from "@angular/material/core";

import { MatFormFieldModule } from "@angular/material/form-field";
import { MomentDateAdapter } from "@angular/material-moment-adapter";
import { MY_DATE_FORMATS } from "./services/my-date-format";

import { BackButtonDisableModule } from "angular-disable-browser-back-button";
import { Ng2SearchPipeModule } from 'ng2-search-filter';

// import { MatFormFieldModule, MatNativeDateModule,MAT_DATE_LOCALE  } from '@angular/material';

import { MatInputModule } from "@angular/material/input";
import { HttpClient, HttpClientModule } from "@angular/common/http";

// import { BnNgIdleService } from 'bn-ng-idle';
// import { NgxTimerModule } from 'ngx-timer';

import { A11yModule } from "@angular/cdk/a11y";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { PortalModule } from "@angular/cdk/portal";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { CdkStepperModule } from "@angular/cdk/stepper";
import { CdkTableModule } from "@angular/cdk/table";
import { CdkTreeModule } from "@angular/cdk/tree";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatBadgeModule } from "@angular/material/badge";
import { MatBottomSheetModule } from "@angular/material/bottom-sheet";
import { MatButtonModule } from "@angular/material/button";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatCardModule } from "@angular/material/card";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatChipsModule } from "@angular/material/chips";
import { MatStepperModule } from "@angular/material/stepper";
import { MatDialogModule } from "@angular/material/dialog";
import { MatDividerModule } from "@angular/material/divider";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatIconModule } from "@angular/material/icon";
import { MatListModule } from "@angular/material/list";
import { MatMenuModule } from "@angular/material/menu";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatRadioModule } from "@angular/material/radio";
import { MatSelectModule } from "@angular/material/select";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatSliderModule } from "@angular/material/slider";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatSortModule } from "@angular/material/sort";
import { MatTableModule } from "@angular/material/table";
import { MatTabsModule } from "@angular/material/tabs";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatTreeModule } from "@angular/material/tree";

import { PaymentConfirmationComponent } from "./pages/payment-confirmation/payment-confirmation.component";
import { SwapComponent } from "./pages/swap/swap.component";
import { PendingConfirmationComponent } from "./pages/pending-confirmation/pending-confirmation.component";

import { Ng5SliderModule } from "ng5-slider";
import { GoogleAnalyticsService } from "./services/google-analytics.service";
import { RedirectionComponent } from "./pages/redirection/redirection.component";
import { NysaloginComponent } from "./pages/nysalogin/nysalogin.component";
import { AddonCoverComponent } from "./pages/addon-cover/addon-cover.component";
import { PremiumPageComponent } from "./pages/premium-page/premium-page.component";

import { NgOtpInputModule } from "ng-otp-input";
import { AutofocusDirective } from "./directives/autofocus.directive";
import { PlutusPaymentConfirmationComponent } from "./pages/plutus-payment-confirmation/plutus-payment-confirmation.component";
import { MultipaymentComponent } from "./pages/multipayment/multipayment.component";
import { PriceBreakUPComponent } from "./pages/price-break-up/price-break-up.component";
import { CreateBreakInComponent } from "./pages/create-break-in/create-break-in.component";
import { BreakInDashComponent } from "./pages/break-in-dash/break-in-dash.component";
import { CustlinkbreakupComponent } from "./pages/custlinkbreakup/custlinkbreakup.component";
import { LombardpayWalletComponent } from './pages/lombardpay-wallet/lombardpay-wallet.component';
import { TechopComponent } from './pages/techop/techop.component';
import { TechopsLoginComponent } from './pages/techops-login/techops-login.component';

import { PHLoginComponent } from './pages/phlogin/phlogin.component';
import { PhDashboardComponent } from './pages/ph-dashboard/ph-dashboard.component';
import { PhviewdocumentComponent } from './pages/phviewdocument/phviewdocument.component';

import { RenewalDashboardComponent } from './pages/renewal-dashboard/renewal-dashboard.component';
import { RenewalPDFComponent } from './pages/renewal-pdf/renewal-pdf.component';
import { RenewalModifyComponent } from './pages/renewal-modify/renewal-modify.component';
import { NcbPaymentComponent } from './pages/ncb-payment/ncb-payment.component';
import { NcbDashboardComponent } from './pages/ncb-dashboard/ncb-dashboard.component';
import { AllriskCustPaymentComponent } from './pages/allrisk-cust-payment/allrisk-cust-payment.component';


import { NotifierModule, NotifierOptions } from "angular-notifier";
import { LockedBankingIDComponent } from "./pages/locked-banking-id/locked-banking-id.component";
import { MatPaginatorModule } from "@angular/material/paginator";
import { CommonModule } from "@angular/common";
import { CustPaymentComponent } from "./pages/cust-payment/cust-payment.component";

const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: "right",
      distance: 12,
    },
    vertical: {
      position: "top",
      distance: 20,
      gap: 10,
    },
  },
  theme: "material",
  behaviour: {
    autoHide: 5000,
    onClick: "hide",
    onMouseover: "pauseAutoHide",
    showDismissButton: true,
    stacking: 4,
  },
  animations: {
    enabled: true,
    show: {
      preset: "slide",
      speed: 300,
      easing: "ease",
    },
    hide: {
      preset: "fade",
      speed: 300,
      easing: "ease",
      offset: 50,
    },
    shift: {
      speed: 300,
      easing: "ease",
    },
    overlap: 150,
  },
};
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json')
}

@NgModule({
  declarations: [
    AppComponent,
    PaymentComponent,
    PaymentConfirmationComponent,
    SwapComponent,
    PendingConfirmationComponent,
    RedirectionComponent,
    NysaloginComponent,
    AddonCoverComponent,
    PremiumPageComponent,
    AutofocusDirective,
    PlutusPaymentConfirmationComponent,
    MultipaymentComponent,
    PriceBreakUPComponent,
    CreateBreakInComponent,
    BreakInDashComponent,
    NcbDashboardComponent,
    CustlinkbreakupComponent,
    LockedBankingIDComponent,
    CustPaymentComponent,
    RenewalPDFComponent,
    RenewalModifyComponent,
    NcbPaymentComponent,
    AllriskCustPaymentComponent

  ],
  
  imports: [
    SharedModule,
    CommonModule,
    LombardpayWalletModule,
    QuoteModule,
    PhloginModule,
    PhDashboardModule,
    PhviewdocumentModule,
    TechopModule,
    TechopsLoginModule,
    BrowserModule,
    ImgMagnifier,
    AppRoutingModule,
    Ng2SearchPipeModule,
    BrowserAnimationsModule,
    NotifierModule.withConfig(customNotifierOptions),
    BackButtonDisableModule.forRoot({
      preserveScrollPosition: true,
    }),
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    NgOtpInputModule,
    Ng5SliderModule,
    NgxPaginationModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),

    // NgxTimerModule,
    SharedtranslateModule.forRoot(),
    HttpClientModule,
    FormsModule,
    A11yModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    PortalModule,
    ScrollingModule,
  ],
  exports: [],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    { provide: MAT_DATE_LOCALE, useValue: "en-GB" },
    // BnNgIdleService,
    GoogleAnalyticsService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
