import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, } from "@angular/core";
import { Event as NavigationEvent } from "@angular/router";
import { filter } from "rxjs/operators";
import { Router } from "@angular/router";
import { NavigationStart } from "@angular/router";
import { CommonService } from "src/app/services/common.service";
import swal from "sweetalert";
import { commonData } from "src/app/commonData/commonData";
import { Guid } from "guid-typescript";
declare var $: any;
import * as moment from "moment";
import { ApiServiceService } from "src/app/services/api-service.service";
import { SwitchLanguageService } from "src/app/services/switch-language.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"],
})
export class HeaderComponent implements OnInit,AfterViewInit {
  auth: any;
  policy: any;
  product: any;
  paramDataValues: any;
  authtok: any;
  policyType: any;
  productCode: any;
  view: any;
  iPartnerUserId: any;
  dealId: any;
  feedbackpattern =
    "(?=[a-zA-Z0-9#@$?]{8,}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).";
  showusererror: boolean = false;
  // Ui/ux changes by monika
  body: any = {};
  language: any;
  lang = [];
  @ViewChild("langSelect", { static: false }) private langSelect: ElementRef;

  feedbackObj = {
    isNysaIpartner: "N",
    isCommissionBrokerage: "N",
    isSalesTeam: "N",
    isUWTeam: "N",
    isOtherSuggestion: "N",
    userComments: "",
    CorelationID: "",
  };

  corelatinId: any;
  showRenwalHeader = false;

  /**
   * Digital POS Variable
   */
  IsPOSTransaction_flag: boolean = false;
  mobile_view: boolean;

  constructor(
    public router: Router,
    public cs: CommonService,
    public api: ApiServiceService,
    private switchLang: SwitchLanguageService,
  ) {
    this.auth = JSON.parse(localStorage.getItem("AuthToken"));
    this.dealId = JSON.parse(localStorage.getItem("DealID"));

    /**
     * digitalPOS change
     * get values from localStorage for checking if conditions in html file
     * hidding the side nav on mobile screen's for digital pos
     * date :- 29-07-2021
     */
    if (localStorage.getItem("IsPOSTransaction")) {
      this.IsPOSTransaction_flag = JSON.parse(
        localStorage.getItem("IsPOSTransaction")
      );
    }
    if (this.IsPOSTransaction_flag == true) {
      if (screen.width < 768) {
        this.mobile_view = true;
      } else {
        this.mobile_view = false;
      }
    } else {
      this.mobile_view = false;
    }
  }

  ngOnInit() {
    $("#expand").click(function () {
      $("aside").toggleClass("active");
    });
    this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
    let showRenwHeader = JSON.parse(sessionStorage.getItem("type"));
    if(showRenwHeader == 'PDF' || showRenwHeader == 'PAY'){
      this.showRenwalHeader = true;
    }else{
      this.showRenwalHeader = false;
    }
    this.view = this.paramDataValues.iPartnerLogin.view;
    this.authtok = JSON.parse(localStorage.getItem("AuthToken"));
    this.policyType = this.paramDataValues.iPartnerLogin.policy;
    this.productCode = this.paramDataValues.iPartnerLogin.product;
    if (this.productCode == "2312") {
      this.product = "Two Wheeler";
    } else if (this.productCode == "2311") {
      this.product = "Four Wheeler";
    } else if (this.productCode == "2320") {
      this.product = "Two Wheeler Liability";
    } else if (this.productCode == "2319") {
      this.product = "Four Wheeler Liability";
    } else if (this.productCode == commonData.allriskProductCode) {
      this.product = "Risk Electric Bike";
    }
    this.prefferedLanguage(); //ui/ux change by monika
    this.corelatinId = Guid.raw(); 
    const pasteBox = document.getElementById("userfeedback");
    pasteBox.onpaste = (e) => {
      e.preventDefault();
      return false;
    };
  }

  // ui/ux changes by Monika
  ngAfterViewInit() {
    this.langSelect.nativeElement.value = this.language;
  }

  // Go to Dashboard
  goToDashboard() {
    /**
     * digital POS Change
     * Added If conditon for restricting redirection on click of icici-lombard logo
     * date :- 29-07-2021
     */
    if (this.IsPOSTransaction_flag == false) {
      swal({
        text: "Do you really want to exit the screen?",
        dangerMode: true,
        closeOnClickOutside: false,
        buttons: ["No", "Yes"],
      }).then((willDelete) => {
        if (willDelete) {
          this.cs.geToDashboard();
          this.cs.clearLocalStorage();
          this.cs.clearQuoteDate();
          this.cs.clearPropData();
        } else {
          window.location.reload();
        }
      });
    } else {
      console.info("DigitalPOS Transaction");
    }
  }

  goToNysa() {
    this.router.navigateByUrl("quote");
    this.cs.clearLocalStorage();
    this.cs.clearQuoteDate();
    this.cs.clearPropData();
  }

  // Go to Login page
  goToLogin() {
    this.cs.geToLogin();
    localStorage.removeItem("calQuoteReq");
    localStorage.removeItem("calQuoteReq4w");
    localStorage.removeItem("calQuoteRes");
    localStorage.removeItem("calQuoteRes4w");
    localStorage.removeItem("dataforBack");
    localStorage.removeItem("PropDatareq");
    localStorage.removeItem("PropDatares");
    localStorage.removeItem("propdataforBack");
    localStorage.removeItem("orderReq");
    localStorage.removeItem("orderRes");
    localStorage.removeItem("PFReq");
    localStorage.removeItem("PFRes");
    // localStorage.removeItem("EVQuoteReq");
    // localStorage.removeItem("EVQuoteRes");
    // localStorage.removeItem("scpaQuoteReq");
    // localStorage.removeItem("scpaQuoteRes");
    // localStorage.removeItem("scpaPropDatareq");
    // localStorage.removeItem("scpaPropDatares");
    // localStorage.removeItem("EVPropDatareq");
    // localStorage.removeItem("EVPropDatares");
  }

  refreshToken(ev: any) {
    // console.log('selected Code', ev.target.innerText);
    localStorage.removeItem("myNysaPolicy");
    if (ev == "comm") {
      this.cs.geToDashboard();
    } else if (ev == "Endo") {
      let existingtoken = JSON.parse(localStorage.getItem("AuthToken"));
      this.cs
        .get(
          "Token/EndorsementRedirectionWithToken?imID=" + existingtoken.username
        )
        .subscribe((res: any) => {
          this.cs.clearLocalStorage();
          this.cs.clearQuoteDate();
          this.cs.clearPropData();
          let paramData = JSON.parse(localStorage.getItem("paramData"));
          if (paramData.iPartnerLogin.view == "MVIEW") {
            window.location.href =
              res.message + "&view=" + paramData.iPartnerLogin.view;
          } else {
            window.location.href = res.message;
          }
        });
    } else if (ev == "claims") {
      this.router.navigateByUrl("/claims");
    } else if (ev == "renewal") {
      this.router.navigateByUrl("/renewals");
    } else if (ev == "Health") {
      this.cs.goToHealth();
    } else if (ev == "Travel") {
      this.cs.goToTravel();
    } else if (ev == "bitly") {
      // this.cs.getBitLy('Access/IsEnableBitly').then((res:any) => {
      //   if(!res){
      let date = new Date();
      console.log(moment(date).format("DD-MMM-YY hh:mm:ss"));
      let auth =
        this.authtok.username + "|" + moment(date).format("DD-MMM-YY hh:mm:ss");
      let data = this.cs.encryptLikePython(auth);
      // console.log("AUTH", auth, data, encodeURIComponent(data));
      this.cs.goToBitly(data);
      //   }else{
      //     swal({
      //       text: "You don't have access for this product",
      //     })
      //   }
      // })
      // let auth = this.authtok.username + "&" + new Date();
      // let data = this.cs.encryptLikePython(auth)
      // this.cs.goToBitly(data);
    } else {
      this.cs.get("Token/RefreshToken").subscribe(
        (res: any) => {
          if (ev == "4WNew") {
            (this.policy = "NEW"), (this.product = "2311");
          }
          if (ev == "4WRoll") {
            (this.policy = "ROLL"), (this.product = "2311");
          }
          if (ev == "4WUsed") {
            (this.policy = "USED"), (this.product = "2311");
          }
          if (ev == "2WNew") {
            (this.policy = "NEW"), (this.product = "2312");
          }
          if (ev == "2WRoll") {
            (this.policy = "ROLL"), (this.product = "2312");
          }
          if (ev == "4WTPNew") {
            (this.policy = "NEW"), (this.product = "2319");
          }
          if (ev == "4WTPRoll") {
            (this.policy = "ROLL"), (this.product = "2319");
          }
          if (ev == "2WTPNew") {
            (this.policy = "NEW"), (this.product = "2320");
          }
          if (ev == "2WTPRoll") {
            (this.policy = "ROLL"), (this.product = "2320");
          }
          if (ev == "riskEB") {
            (this.policy = "NEW"), (this.product = commonData.allriskProductCode);
          }

          let Username,
            Password,
            Expiry,
            Signature,
            iPartner_Token,
            view,
            isSubagent,
            subAID;
          let existingtoken = JSON.parse(localStorage.getItem("AuthToken"));

          if (!this.cs.isUndefineORNull(res)) {
            //  let body1 = {
            //     expiry: res.expiry,
            //     token: res.token,
            //     username: existingtoken.username,
            //   };
            // //CreateToken + GetApplicationAccess merging code - Sejal
            let body1 = {
              expiry: res.expiry,
              token: res.token,
              username: existingtoken.username,
              applicationAccess: existingtoken.applicationAccess,
            };
            localStorage.setItem("AuthToken", JSON.stringify(body1));
            let paramData = JSON.parse(localStorage.getItem("paramData"));
            paramData.iPartnerLogin.policy = this.policy;
            paramData.iPartnerLogin.product = this.product;
            Username = paramData.Username;
            Password = paramData.Password;
            Expiry = paramData.iPartnerLogin.Expiry;
            Signature = paramData.iPartnerLogin.Signature;
            iPartner_Token = paramData.iPartnerLogin.iPartner_Token;
            view = paramData.iPartnerLogin.view;
            isSubagent = paramData.iPartnerLogin.isSubagent;
            subAID = paramData.iPartnerLogin.subAID;
            let body;
            body = {
              Username: Username,
              Password: Password,
              iPartnerLogin: {
                Expiry: Expiry,
                Signature: Signature,
                iPartner_Token: iPartner_Token,
                product: this.product,
                policy: this.policy,
                view: view,
                // "isSubagent" : null,
                // "subAID" : null,
              },
            };
            if (isSubagent == "Y") {
              body.iPartnerLogin.isSubagent = isSubagent;
              body.iPartnerLogin.subAID = subAID;
            }
            localStorage.setItem("paramData", JSON.stringify(body));
            this.router.navigateByUrl("/quote").then(() => {
              this.cs.clearLocalStorage();
              this.cs.clearQuoteDate();
              this.cs.clearPropData();
              // window.location.reload();
              this.router
                .navigateByUrl("/", { skipLocationChange: true })
                .then(() => this.router.navigate(["/quote"]));
            });
          }
        },
        (err) => {
          this.cs.loaderStatus = false;
          this.cs.geToDashboard();
        }
      );
    }
  }

  gotoLombardPayWallet() {
    // //CreateToken + GetApplicationAccess merging code - Sejal
    if (!this.cs.isUndefineORNull(localStorage.getItem("AuthToken"))) {
      let encryptedTokenaccess = JSON.parse(localStorage.getItem("AuthToken"));
      let Parsedtokenaccess: any = this.api.decrypt(
        encryptedTokenaccess.applicationAccess
      );
      let tokenaccess = JSON.parse(Parsedtokenaccess);
      this.cs.isPlutusEnable = tokenaccess.isEnablePlutus;
      if (
        this.cs.isPlutusEnable == true ||
        this.IsPOSTransaction_flag == true
      ) {
        this.cs.getPaymsToken().then((resp: any) => {
          localStorage.setItem("paymsToken", JSON.stringify(resp.accessToken));
          this.iPartnerUserId = resp.iPartnerUserId;

          var enc = window.btoa(this.dealId);
          var iPartnerUserId = window.btoa(this.iPartnerUserId);
          window.location.href =
            commonData.plutusUI +
            "/lombardpay/wallet/" +
            enc +
            "/" +
            iPartnerUserId;
        });
      } else {
        this.router.navigateByUrl("/lombardpaywallet");
      }
    }

    // this.cs.get1("Access/GetApplicationAccess").then((res: any) => {
    //   this.cs.isPlutusEnable = res.isEnablePlutus;
    //   if (res.isEnablePlutus == true || this.IsPOSTransaction_flag == true) {
    //     this.cs.getPaymsToken().then((resp: any) => {
    //       localStorage.setItem("paymsToken", JSON.stringify(resp.accessToken));
    //       this.iPartnerUserId = resp.iPartnerUserId;

    //       var enc = window.btoa(this.dealId);
    //       var iPartnerUserId = window.btoa(this.iPartnerUserId);
    //       window.location.href =
    //         commonData.plutusUI +
    //         "/lombardpay/wallet/" +
    //         enc +
    //         "/" +
    //         iPartnerUserId;
    //     });
    //   } else {
    //     this.router.navigateByUrl("/lombardpaywallet");
    //   }
    // });
  }

  gotoTravel() {
    this.cs.goToTravel();
  }

  goToHealth() {
    this.cs.goToHealth();
  }

  goToIpartnerDashboard() {
    if (this.view == "MVIEW") {
      // Mview
      swal({
        text: "Do you want to exit from current screen ?",
        dangerMode: true,
        closeOnClickOutside: false,
        buttons: ["Yes", "No"],
      }).then((willDelete) => {
        if (willDelete) {
          console.log("Continue");
        } else {
          console.log("Go Back");
          // this.router.navigateByUrl('/redirection?product=');
          location.href = "redirect1.html";
        }
      });
    } else {
      let type;
      if (this.policyType == "NEW") {
        type = "N";
      } else {
        type = "R";
      }
      // this.cs.geToDashboardapproval(this.productCode,type);
      this.cs.geToDashboard();
      this.cs.clearLocalStorage();
      this.cs.clearQuoteDate();
      this.cs.clearPropData();
    }
  }

  feedbackCheckVal(ev: any) {
    let val = ev.target.id;

    switch (val) {
      case "nysaIpartner":
        if (ev.target.checked == true) {
          this.feedbackObj.isNysaIpartner = "Y";
        } else {
          this.feedbackObj.isNysaIpartner = "N";
        }

        break;
      case "commisionandbrokerage":
        if (ev.target.checked == true) {
          this.feedbackObj.isCommissionBrokerage = "Y";
        } else {
          this.feedbackObj.isCommissionBrokerage = "N";
        }
        break;
      case "salesTeams":
        if (ev.target.checked == true) {
          this.feedbackObj.isSalesTeam = "Y";
        } else {
          this.feedbackObj.isSalesTeam = "N";
        }
        break;
      case "uwTeam":
        if (ev.target.checked == true) {
          this.feedbackObj.isUWTeam = "Y";
        } else {
          this.feedbackObj.isUWTeam = "N";
        }
        break;
      case "otherSuggestion":
        if (ev.target.checked == true) {
          this.feedbackObj.isOtherSuggestion = "Y";
        } else {
          this.feedbackObj.isOtherSuggestion = "N";
        }
        break;
      default:
        break;
    }
  }

  clearfeedback() {
    this.feedbackObj = {
      isNysaIpartner: "N",
      isCommissionBrokerage: "N",
      isSalesTeam: "N",
      isUWTeam: "N",
      isOtherSuggestion: "N",
      userComments: "",
      CorelationID: "",
    };
  }

  submitFeedback() {
    this.feedbackObj.CorelationID = this.corelatinId;
    if (
      this.feedbackObj.isNysaIpartner == "N" &&
      this.feedbackObj.isCommissionBrokerage == "N" &&
      this.feedbackObj.isSalesTeam == "N" &&
      this.feedbackObj.isUWTeam == "N" &&
      this.feedbackObj.isOtherSuggestion == "N"
    ) {
      swal({
        closeOnClickOutside: false,
        text: "Please select atleast one option",
      }).then((value) => {
        $("#feedbackModal").modal("show");
      });
    } else if (
      this.feedbackObj.userComments.length < 4 ||
      this.feedbackObj.userComments.length > 800
    ) {
      swal({
        closeOnClickOutside: false,
        text: "Feedback Message should be minimum 4 to maximum 800 words",
      }).then((value) => {
        $("#feedbackModal").modal("show");
      });
    } else {
      this.cs.loaderStatus = true;
      this.cs.post("Agent/SendFeedback", this.feedbackObj).then((res: any) => {
        if (res.statusCode == 1) {
          swal({
            closeOnClickOutside: false,
            text: "Your feedback is submitted successfully",
          }).then(() => {
            this.feedbackObj = {
              isNysaIpartner: "N",
              isCommissionBrokerage: "N",
              isSalesTeam: "N",
              isUWTeam: "N",
              isOtherSuggestion: "N",
              userComments: "",
              CorelationID: "",
            };
          });
          this.cs.loaderStatus = false;
        } else {
          this.cs.loaderStatus = false;
        }
      });
    }
  }

  // ui/ux changes by --monika
  switchLanguage(val) {
    swal({
      closeOnClickOutside: false,
      text: "Do you want it to save as your preffered language",
      buttons: ["No", "Yes"],
    }).then((value) => {
      if (value == true) {
        this.prefferedLanguage(val);
        this.cs.setLanguage(val);
        this.switchLang.translate.use(val);
      } else {
        this.cs.setLanguage(val);
        this.switchLang.translate.use(val);
      }
    });
  }

  loadLanguage() {
    this.lang = this.switchLang.translate.getLangs();
    const browserLang = this.switchLang.translate.getBrowserLang();
    this.switchLang.translate.use(this.language);
  }
  prefferedLanguage(val?) {
       this.body.IsPrefferedLanguage = true;
    if (val && (val != null || val != "")) {
      this.body.Language = val;
      let encryptedLanguageVal = this.api.encrypt(val);
      localStorage.setItem("UserLanguage", encryptedLanguageVal);
    } else {
      if (localStorage.getItem("UserLanguage")) {
        let languageValue = localStorage.getItem("UserLanguage");
        let decryptedLanguageVal = JSON.parse(this.api.decrypt(languageValue));
        val = decryptedLanguageVal;
      } else {
        val = "English";
        let encryptedLanguageVal = this.api.encrypt(val);
        localStorage.setItem("UserLanguage", encryptedLanguageVal);
      }
    }

    // this.cs.post("Language/SetUserLanguage", this.body).then((res) => {
    //   let newObj = JSON.parse(JSON.stringify(res));
    this.language = val;
    if (
      this.language == "" ||
      this.language == null ||
      this.language == undefined
    ) {
      this.language = "English";
    }
    this.loadLanguage();
    // });
  }
}
