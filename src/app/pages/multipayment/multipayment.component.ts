import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import * as moment from "moment";
import { commonData } from "../../commonData/commonData";
import { CommonService } from "src/app/services/common.service";
import { Guid } from "guid-typescript";
declare var Razorpay: any;
import swal from "sweetalert";
import { ChangeDetectorRef } from "@angular/core";
declare var $: any;

@Component({
  selector: "app-multipayment",
  templateUrl: "./multipayment.component.html",
  styleUrls: ["./multipayment.component.css"],
})
export class MultipaymentComponent implements OnInit {
  multipleProposal: any;
  totalAmount: any;
  proposalNos: any;
  paymentMode: any;
  id: any;
  orderId: any;
  PayKey: any;
  options: any;
  dealID: any;
  startDate: any;
  showpayment: boolean = true;
  showconfirmation: boolean = false;
  isInstPol: boolean;
  PaymentMode: any;
  paylombard: any;
  lombardpaybalance: any;
  userdetails: any;
  showSendLink: boolean = true;
  updatePayment: any;
  // Payment Model
  paymentDetails = {
    Name: "",
    Email: "",
    Contact: "",
    balance: "",
    amount: "",
  };
  PFPayReq: any;
  PFPayRes: any;
  constructor(
    public router: Router,
    public cs: CommonService,
    public activeRoute: ActivatedRoute,
    private changeDetector: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.activeRoute.queryParams.subscribe((params) => {
      const val = params["isInsta"];
      this.isInstPol = JSON.parse(atob(val));
      console.log(this.isInstPol);
    });

    this.cs.getPayAuth().then((resp: any) => {
      localStorage.setItem("PayAuthToken", JSON.stringify(resp.accessToken));
    });
    this.id = Guid.raw();
    this.PayKey = commonData.PayKey;
    this.multipleProposal = JSON.parse(
      localStorage.getItem("multipleProposal")
    );
    this.proposalNos = this.multipleProposal.map((obj) => obj.ProposalNo);

    this.totalAmount = localStorage.getItem("multiTotalAmount");
    this.dealID = JSON.parse(localStorage.getItem("DealID"));
    this.startDate = moment(new Date()).format("YYYY-MM-DD");

    this.cs.getlombardPay().then((resp: any) => {
      localStorage.setItem(
        "LombardPayAuthToken",
        JSON.stringify(resp.accessToken)
      );
      this.showBalance();
      this.userDetails();
    });
  }

  customerData(mode: any) {
    this.paymentMode = mode;
    this.pay();
  }

  // Razor Pay Call
  pay() {
    this.createOrder().then(() => {
      this.payByRazorPay(this.router);
    });
  }

  saveorderReq() {
    let bankType = localStorage.getItem("bankType");
    let body;
    if (bankType == "INT" && this.isInstPol == false) {
      body = {
        CorrelationID: this.id,
        DealID: this.dealID,
        // "PaymentID": paymentid,
        isMappingRequired: true,
        isTaggingRequired: true,
        PaymentMode: this.paymentMode,
        PaymentEntry: {
          onlineDAEntry: {
            CorelationID: this.id,
            DealId: this.dealID,
            MerchantID: commonData.MerchantID,
            TransactionId: commonData.TransactionId,
            PaymentAmount: this.totalAmount,
            InstrumentDate: this.startDate,
            ReceiptDate: this.startDate,

            PayerType: "I",
          },
          cheque: null,
        },
        PaymentTagging: {
          DealID: this.dealID,
          CorrelationID: this.id,

          customerProposal: this.multipleProposal,
        },
        PaymentMapping: {
          DealID: this.dealID,
          CorrelationID: this.id,

          customerProposal: this.multipleProposal,
        },
      };
    } else if (bankType == "INT" && this.isInstPol == true) {
      body = {
        CorrelationID: this.id,
        DealID: this.dealID,

        isMappingRequired: true,
        isTaggingRequired: false,

        PaymentEntry: {
          onlineDAEntry: {
            CorelationID: this.id,
            DealId: this.dealID,

            MerchantID: commonData.MerchantID,
            TransactionId: commonData.TransactionId,
            PaymentAmount: this.totalAmount,
            InstrumentDate: this.startDate,

            ReceiptDate: this.startDate,

            PayerType: "I",
          },
          cheque: null,
        },
        PaymentMapping: {
          DealID: this.dealID,
          CorrelationID: this.id,

          customerProposal: this.multipleProposal,
        },
      };
    } else {
      body = {
        CorrelationID: this.id,
        DealID: this.dealID,

        isMappingRequired: false,
        isTaggingRequired: true,
        PaymentMode: this.paymentMode,

        PaymentEntry: {
          onlineDAEntry: {
            CorelationID: this.id,
            DealId: this.dealID,

            MerchantID: commonData.MerchantID,
            TransactionId: commonData.TransactionId,
            PaymentAmount: this.totalAmount,
            InstrumentDate: this.startDate,

            ReceiptDate: this.startDate,

            PayerType: "I",
          },
          cheque: null,
        },
        PaymentTagging: {
          DealID: this.dealID,
          CorrelationID: this.id,

          customerProposal: this.multipleProposal,
        },
        PaymentMapping: null,
      };
    }

    let str = JSON.stringify(body);
    this.cs
      .post1("Payment/SaveOrderRequestBody", str)
      .then((response: any) => {});
  }

  // Create Order Service
  createOrder(): Promise<any> {
    return new Promise((resolve: any) => {
      this.saveorderReq();
      let body = {
        CorrelationID: this.id, //CorelationID
        amount: this.totalAmount, //Premium Amount
        // "amount":  this.cs.encrypt(this.totalAmount,commonData.aesnysaKey),
        identifier: "ipartner_policy_" + this.proposalNos, //Proposal Number
      };
      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs.post1("Payment/CreateOrder", str).then((response: any) => {
        if (response.status == "created") {
          this.orderId = response.id;
          localStorage.setItem("orderReq", JSON.stringify(body));
          localStorage.setItem("orderRes", JSON.stringify(response));
          this.cs.loaderStatus = false;
          resolve();
        } else {
          resolve();
          this.cs.loaderStatus = false;
        }
      });
    });
  }

  // RazorPay Call
  payByRazorPay(route: any) {
    var desc = "ICICI Lombard - Motor Insurance";
    let self = this;
    this.options = {
      description: desc,
      image: "https://www.icicilombard.com/mobile/mclaim/images/favicon.ico",
      currency: "INR",
      key: this.PayKey,
      order_id: this.orderId,
      method: {
        netbanking: {
          order: ["ICIC", "HDFC", "SBIN", "UTIB", "IDFB", "IBKL"],
        },
      },
      prefill: {
        // email: this.custEmail,
        // contact: this.custMobile,
        // name: this.custName,
        method: this.paymentMode,
      },
      theme: {
        color: "#E04844",
        hide_topbar: true,
      },
      handler: function (response: any) {
        self.createPFPaymentAsync(response, self, route);
      },
      modal: {
        ondismiss: function () {
          this.cs.loaderStatus = false;
        },
      },
    };
    var rzp1 = new Razorpay(this.options);
    rzp1.open();
  }

  // PFPayment
  createPFPaymentAsync(res: any, _self: any, route: any): Promise<any> {
    let paymentid = JSON.parse(JSON.stringify(res.razorpay_payment_id));
    return new Promise((resolve: any) => {
      let authCode = res.razorpay_payment_id.split("_");
      let bankType = localStorage.getItem("bankType");
      let body;
      if (bankType == "INT" && this.isInstPol == false) {
        body = {
          CorrelationID: this.id,
          DealID: this.dealID,
          // "PaymentID": paymentid,
          isMappingRequired: true,
          isTaggingRequired: true,
          PaymentMode: this.paymentMode,
          RazorPayResponse: {
            razorpay_order_id: res.razorpay_order_id,
            razorpay_payment_id: res.razorpay_payment_id,
            razorpay_signature: res.razorpay_signature,
          },
          PaymentEntry: {
            onlineDAEntry: {
              CorelationID: this.id,
              DealId: this.dealID,
              AuthCode: authCode[1],
              MerchantID: commonData.MerchantID,
              TransactionId: commonData.TransactionId,
              PaymentAmount: this.totalAmount,
              InstrumentDate: this.startDate,
              // "CustomerID":null,
              ReceiptDate: this.startDate,
              // "PaymentOption":null,
              // "InstrumentType":null,
              // "ReferenceNumber":null,
              PayerType: "I",
            },
            cheque: null,
          },
          PaymentTagging: {
            DealID: this.dealID,
            CorrelationID: this.id,
            // "PaymentID": null,
            customerProposal: this.multipleProposal,
          },
          PaymentMapping: {
            DealID: this.dealID,
            CorrelationID: this.id,
            // "PaymentID": null,
            customerProposal: this.multipleProposal,
          },
        };
      } else if (bankType == "INT" && this.isInstPol == true) {
        body = {
          CorrelationID: this.id,
          DealID: this.dealID,
          // "PaymentID": paymentid,
          isMappingRequired: true,
          isTaggingRequired: false,
          RazorPayResponse: {
            razorpay_order_id: res.razorpay_order_id,
            razorpay_payment_id: res.razorpay_payment_id,
            razorpay_signature: res.razorpay_signature,
          },
          PaymentEntry: {
            onlineDAEntry: {
              CorelationID: this.id,
              DealId: this.dealID,
              AuthCode: authCode[1],
              MerchantID: commonData.MerchantID,
              TransactionId: commonData.TransactionId,
              PaymentAmount: this.totalAmount,
              InstrumentDate: this.startDate,
              // "CustomerID":null,
              ReceiptDate: this.startDate,
              // "PaymentOption":null,
              // "InstrumentType":null,
              // "ReferenceNumber":null,
              PayerType: "I",
            },
            cheque: null,
          },
          PaymentMapping: {
            DealID: this.dealID,
            CorrelationID: this.id,
            // "PaymentID": null,
            customerProposal: this.multipleProposal,
          },
        };
      } else {
        body = {
          CorrelationID: this.id,
          DealID: this.dealID,
          // "PaymentID": paymentid,
          isMappingRequired: false,
          isTaggingRequired: true,
          PaymentMode: this.paymentMode,
          RazorPayResponse: {
            razorpay_order_id: res.razorpay_order_id,
            razorpay_payment_id: res.razorpay_payment_id,
            razorpay_signature: res.razorpay_signature,
          },
          PaymentEntry: {
            onlineDAEntry: {
              CorelationID: this.id,
              DealId: this.dealID,
              AuthCode: authCode[1],
              MerchantID: commonData.MerchantID,
              TransactionId: commonData.TransactionId,
              PaymentAmount: this.totalAmount,
              InstrumentDate: this.startDate,
              // "CustomerID":null,
              ReceiptDate: this.startDate,
              // "PaymentOption":null,
              // "InstrumentType":null,
              // "ReferenceNumber":null,
              PayerType: "I",
            },
            cheque: null,
          },
          PaymentTagging: {
            DealID: this.dealID,
            CorrelationID: this.id,
            // "PaymentID": null,
            customerProposal: this.multipleProposal,
          },
          PaymentMapping: null,
        };
      }

      let str = JSON.stringify(body);
      let loader = document.getElementById("loaderdiv") as HTMLInputElement;
      loader.style.display = "block";
      _self.cs
        .post1("Payment/PFPaymentsyncNew", str)
        .then((response: any) => {
          if (response == "Request Sent") {
            localStorage.setItem("PFReq", JSON.stringify(body));
            localStorage.setItem("PFRes", JSON.stringify(response));
            localStorage.setItem("transactionId", authCode[1]);
            // swal({
            //   closeOnClickOutside: false,
            //   text: 'Congratulations ! You payment was successful. Your Transaction id is ' + authCode[1] + " Kindly check proposal numbers in My Nysa Policy " + this.proposalNos
            // }).then((result: any) => { this.goBack() });

            loader.style.display = "none";
            this.cs.pointingChange();
            // Local URL
            //  location.href = "http://localhost:4200/#/payment-confirmation";
            // Sanity URL
            //  location.href = "https://nysa-uat.insurancearticlez.com/nysaui/#/payment-confirmation"
            // Production URL
            //  location.href = "https://ipartnerms.icicilombard.com/nysa/#/payment-confirmation"
          } else {
            swal({
              closeOnClickOutside: false,
              title: "Error!",
              text: "Payment was not initiated. Kindly try again",
            });
            if (!this.isInstPol) {
              let req = {
                CorrelationID: this.id,
                ProposalNo: this.multipleProposal,
                ReqBody: str,
                // "ReqFrom": "PAYMENT_TAG",
                ReqURL: commonData.baseURL1 + "Payment/PFPaymentAsync",
                ErrorMsg: response,
                // "IPartneruserid": this.auth.username,
              };
              let str1 = JSON.stringify(req);
              this.cs
                .postpaymentreq("PaymsPayment/AddPaymsRequestDetails", str1)
                .then((res: any) => {});
            }
          }
        })
        .catch((err: any) => {
          loader.style.display = "none";
          if (!this.isInstPol) {
            let req = {
              CorrelationID: this.id,
              ProposalNo: this.multipleProposal,
              ReqBody: str,
              // "ReqFrom": "PAYMENT_TAG",
              ReqURL: commonData.baseURL1 + "Payment/PFPaymentAsync",
              ErrorMsg: err,
              // "IPartneruserid": this.auth.username,
            };
            let str1 = JSON.stringify(req);
            this.cs
              .postpaymentreq("PaymsPayment/AddPaymsRequestDetails", str1)
              .then((res: any) => {});
          }
          resolve();
        });
    });
  }

  goBack() {
    this.cs.quotepointing();
    // Local URL
    // location.href = "http://localhost:4200/#/quote";
    // Sanity URL
    // location.href = "https://nysa-uat.insurancearticlez.com/nysaui/#/quote"
    // Production URL
    // location.href = "https://ipartnerms.icicilombard.com/nysa/#/quote"
    this.cs.clearLocalStorage();
    this.cs.clearQuoteDate();
    this.cs.clearPropData();
    localStorage.removeItem("multipleProposal");
    localStorage.removeItem("multiTotalAmount");
    localStorage.removeItem("allriskmultipleProposal");
    localStorage.removeItem("allriskmultiTotalAmount");
  }

  // Lombardpay development

  showBalance() {
    return new Promise((resolve: any) => {
      this.cs.get2("LombardPay/GetCustomerBalance").then((response: any) => {
        if (response.statusType == "SUCCESS") {
          this.lombardpaybalance = response.balance;
          this.changeDetector.detectChanges();
        } else {
          this.lombardpaybalance = "0";
        }
      });
    });
  }

  userDetails() {
    this.cs.get2("LombardPay/GetCustomerDetails").then((res: any) => {
      this.userdetails = res;
    });
  }

  addmoney() {
    this.cs.getlombardPay().then((resp: any) => {
      localStorage.setItem(
        "LombardPayAuthToken",
        JSON.stringify(resp.accessToken)
      );
      this.CreateWalletOrder().then(() => {
        this.userDetails();
      });
    });
  }

  CreateWalletOrder(): Promise<any> {
    return new Promise((resolve: any) => {
      let body = {
        amount: JSON.parse(this.paymentDetails.amount),
        identifier: "wallet order test",
      };
      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs
        .post2("LombardPay/CreateWalletOrder", str)
        .then((response: any) => {
          if (response.statusType == "Success") {
            this.orderId = response.razorOrderID;
            this.LombardRazorPay(this.router);
            this.cs.loaderStatus = false;
            resolve();
          } else {
            $("#myModal").modal("show");
            resolve();
            this.cs.loaderStatus = false;
          }
        });
    });
  }

  // RazorPay Call
  LombardRazorPay(route: any) {
    var desc = "ICICI Lombard - Motor Insurance";
    let self = this;
    this.options = {
      description: desc,
      image: "https://www.icicilombard.com/mobile/mclaim/images/favicon.ico",
      currency: "INR",
      key: commonData.LombardKey,
      order_id: this.orderId,
      method: {
        netbanking: {
          order: ["ICIC", "HDFC", "SBIN", "UTIB", "IDFB", "IBKL"],
        },
      },
      prefill: {
        email: this.userdetails.email,
        contact: this.userdetails.contact,
        name: this.userdetails.name,
        method: "netbanking",
      },
      theme: {
        color: "#E04844",
        hide_topbar: true,
      },
      handler: function (response: any) {
        self.loadBalance(response, self, route);
      },
      modal: {
        ondismiss: function () {
          this.cs.loaderStatus = false;
        },
      },
    };
    var rzp1 = new Razorpay(this.options);
    rzp1.open();
  }

  loadBalance(res: any, _self: any, route: any) {
    return new Promise((resolve: any) => {
      let body = {
        razorpay_order_id: res.razorpay_order_id,
        razorpay_payment_id: res.razorpay_payment_id,
        razorpay_signature: res.razorpay_signature,
      };
      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs
        .post2("LombardPay/TransferToWallet", str)
        .then((response: any) => {
          if (response.statusType == "SUCCESS") {
            this.cs.getlombardPay().then((resp: any) => {
              localStorage.setItem(
                "LombardPayAuthToken",
                JSON.stringify(resp.accessToken)
              );
              this.showBalance();
            });
            // this.showBalance().then(() => {

            // });
            // this.orderId1 = response.id;
            // this.cs.loaderStatus = false;
            this.showBalance();
            resolve();
          } else {
            this.showBalance();
            this.cs.loaderStatus = false;
            swal({
              closeOnClickOutside: false,
              text: res.errorText,
            });
            resolve();
          }
        })
        .catch((err: any) => {
          this.cs.loaderStatus = false;
          swal({
            closeOnClickOutside: false,
            text: err,
          });
        });
    });
  }

  payWalletAmount() {
    // (<any>window).ga('send', 'event', {
    //   eventCategory: 'Lombard Pay' ,
    //   eventLabel: "Lombard Pay Clicked" + '' + this.product + '' + this.policyType,
    //   eventAction: 'Lombard Pay Clicked',
    //   eventValue: 10
    // });

    this.cs.getlombardPay().then((resp: any) => {
      localStorage.setItem(
        "LombardPayAuthToken",
        JSON.stringify(resp.accessToken)
      );
      // this.checkBankingid().then(() => {
      this.PayLombardPay();
      // });
    });
  }

  dataEntryPayment() {
    let bankType = localStorage.getItem("bankType");
    let body;
    if (bankType == "INT" && this.isInstPol == false) {
      body = {
        isMappingRequired: true,
        isTaggingRequired: true,
        PaymentMode: "Lombardpay",
        CorrelationId: this.id,
        DealId: this.dealID,
        PaymentEntry: {
          onlineDAEntry: {
            CorrelationId: this.id,
            DealId: this.dealID,
            AuthCode: "",
            // "CustomerID": this.propRes.generalInformation.customerId,
            MerchantID: commonData.lombardMerchant,
            TransactionId: commonData.lombardTransactionId,
            PaymentAmount: String(this.totalAmount),
            InstrumentDate: this.startDate,
            ReceiptDate: this.startDate,
            PayerType: "I",
          },
        },
        PaymentTagging: {
          DealID: this.dealID,
          CorrelationID: this.id,
          customerProposal: this.multipleProposal,
        },
        PaymentMapping: {
          DealID: this.dealID,
          CorrelationID: this.id,
          customerProposal: this.multipleProposal,
        },
      };
    } else if (bankType == "INT" && this.isInstPol == true) {
      body = {
        isMappingRequired: true,
        isTaggingRequired: false,
        CorrelationId: this.id,
        DealId: this.dealID,
        PaymentEntry: {
          onlineDAEntry: {
            CorrelationId: this.id,
            DealId: this.dealID,
            AuthCode: "",
            // "CustomerID": this.propRes.generalInformation.customerId,
            MerchantID: commonData.lombardMerchant,
            TransactionId: commonData.lombardTransactionId,
            PaymentAmount: String(this.totalAmount),
            InstrumentDate: this.startDate,
            ReceiptDate: this.startDate,
            PayerType: "I",
          },
        },
        PaymentMapping: {
          DealID: this.dealID,
          CorrelationID: this.id,
          customerProposal: this.multipleProposal,
        },
      };
    } else {
      body = {
        isMappingRequired: false,
        isTaggingRequired: true,
        PaymentMode: "Lombardpay",
        CorrelationId: this.id,
        DealId: this.dealID,
        PaymentEntry: {
          onlineDAEntry: {
            CorrelationId: this.id,
            DealId: this.dealID,
            AuthCode: "",
            // "CustomerID": this.propRes.generalInformation.customerId,
            MerchantID: commonData.lombardMerchant,
            TransactionId: commonData.lombardTransactionId,
            PaymentAmount: String(this.totalAmount),
            InstrumentDate: this.startDate,
            ReceiptDate: this.startDate,
            PayerType: "I",
          },
        },
        PaymentTagging: {
          DealID: this.dealID,
          CorrelationID: this.id,
          // "PaymentID": null,
          customerProposal: this.multipleProposal,
        },
        PaymentMapping: null,
      };
    }
    if (this.isInstPol) {
      let str = JSON.stringify(body);
      let req = {
        CorrelationID: this.id,
        ProposalNo: this.proposalNos[0],
        ReqBody: str,
        ReqURL: "",
        ErrorMsg: "Insta/Lombard Pay from multi payment",
      };
      let str1 = JSON.stringify(req);
      this.cs
        .postpaymentreq("PaymsPayment/AddPaymsRequestDetails", str1)
        .then((res: any) => {});
    }
  }

  PayLombardPay() {
    this.saveorderReq();
    // this.dataEntryPayment();
    let props = this.proposalNos.join(",");

    if (this.lombardpaybalance < JSON.parse(this.totalAmount)) {
      swal({
        closeOnClickOutside: false,
        text: "Your Lombard Pay wallet balance is low. Kindly add money",
      });
    } else {
      let body = {
        amount: JSON.parse(this.totalAmount),
        description: props,
      };
      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs
        .post2("LombardPay/PayFromWallet", str)
        .then((response: any) => {
          this.PaymentMode = "3";
          this.paylombard = response;
          this.cs.loaderStatus = false;
          this.createLombardPFPayment(response);
          // if (!this.isInstPol){
          //   this.dataEntryPayment();          }
        })
        .catch((err: any) => {
          swal({ text: err.error }).then(() => {
            this.router.navigateByUrl("quote");
          });
          this.cs.loaderStatus = false;
        });
    }
  }

  // LombardPayPayment
  createLombardPFPayment(res: any): Promise<any> {
    return new Promise((resolve: any) => {
      let authCode = res.razorPaymentID.split("_");
      let bankType = localStorage.getItem("bankType");
      // this.IsInstaDone = false;

      let body;
      if (bankType == "INT" && this.isInstPol == false) {
        body = {
          isMappingRequired: true,
          isTaggingRequired: true,
          PaymentMode: "Lombardpay",
          CorrelationId: this.id,
          DealId: this.dealID,
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.id,
              DealId: this.dealID,
              AuthCode: authCode[1],
              // "CustomerID": this.propRes.generalInformation.customerId,
              MerchantID: commonData.lombardMerchant,
              TransactionId: commonData.lombardTransactionId,
              PaymentAmount: String(this.totalAmount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
              PayerType: "I",
            },
          },
          PaymentTagging: {
            DealID: this.dealID,
            CorrelationID: this.id,
            customerProposal: this.multipleProposal,
          },
          PaymentMapping: {
            DealID: this.dealID,
            CorrelationID: this.id,
            customerProposal: this.multipleProposal,
          },
        };
      } else if (bankType == "INT" && this.isInstPol == true) {
        body = {
          isMappingRequired: true,
          isTaggingRequired: false,
          CorrelationId: this.id,
          DealId: this.dealID,
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.id,
              DealId: this.dealID,
              AuthCode: authCode[1],
              // "CustomerID": this.propRes.generalInformation.customerId,
              MerchantID: commonData.lombardMerchant,
              TransactionId: commonData.lombardTransactionId,
              PaymentAmount: String(this.totalAmount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
              PayerType: "I",
            },
          },
          PaymentMapping: {
            DealID: this.dealID,
            CorrelationID: this.id,
            customerProposal: this.multipleProposal,
          },
        };
      } else {
        body = {
          isMappingRequired: false,
          isTaggingRequired: true,
          PaymentMode: "Lombardpay",
          CorrelationId: this.id,
          DealId: this.dealID,
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.id,
              DealId: this.dealID,
              AuthCode: authCode[1],
              // "CustomerID": this.propRes.generalInformation.customerId,
              MerchantID: commonData.lombardMerchant,
              TransactionId: commonData.lombardTransactionId,
              PaymentAmount: String(this.totalAmount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
              PayerType: "I",
            },
          },
          PaymentTagging: {
            DealID: this.dealID,
            CorrelationID: this.id,
            // "PaymentID": null,
            customerProposal: this.multipleProposal,
          },
          PaymentMapping: null,
        };
      }

      let str = JSON.stringify(body);
      let loader = document.getElementById("loaderdiv") as HTMLInputElement;
      loader.style.display = "block";
      // Payment/PFPayment_LomPay
      this.cs
        .post1("Payment/PFPaymentsyncNew", str)
        .then((response: any) => {
          localStorage.setItem("PFReq", JSON.stringify(body));
          localStorage.setItem("PFRes", JSON.stringify(response));
          localStorage.setItem("transactionId", authCode[1]);
          // this.updatePaymentStatus().then(() => {
          //   this.updatePolicyStatus();
          //  });
          // if (response.statusMessage == 'Success') {
          //   if (response.paymentEntryResponse.status == 'Failed') {
          //     swal({
          //       closeOnClickOutside: false,
          //       title: "Error!",
          //       text: response.paymentEntryResponse.errorText
          //     });
          //   } else {
          //     if (response.paymentTagResponse.paymentTagResponseList[0].status == 'Failed' || response.paymentTagResponse.paymentTagResponseList[0].status == 'FAILED')
          //     {
          //       swal({
          //         closeOnClickOutside: false,
          //         title: "Error!",
          //         text: response.paymentTagResponse.paymentTagResponseList[0].errorText
          //       }).then(()=> {
          //       });
          //     } else {
          //       // localStorage.setItem('newProposalNumber', JSON.stringify(this.propRes.generalInformation.proposalNumber))
          //       // localStorage.setItem('newPolicyNumber', JSON.stringify(response.paymentTagResponse.paymentTagResponseList[0].policyNo));
          //       // localStorage.setItem('instaPayment', JSON.stringify(false));

          //       // localStorage.setItem('PFReq', JSON.stringify(body));
          //       // localStorage.setItem('PFRes',JSON.stringify(response));
          //       // swal({
          //       //   closeOnClickOutside: false,
          //       //   text: 'Congratulations ! You payment was successful. Your Transaction id is ' + authCode[1] + " Kindly check proposal numbers in My Nysa Policy " + this.proposalNos
          //       // }).then((result: any) => { this.goBack() });

          //       loader.style.display = 'none';

          //       // {
          //         // Local URL
          //         //  location.href = "http://localhost:4200/#/payment-confirmation";
          //         // Sanity URL
          //         location.href = "https://nysa-uat.insurancearticlez.com/nysaui/#/payment-confirmation"
          //         // Production URL
          //         // location.href = "https://ipartnerms.icicilombard.com/nysa/#/payment-confirmation"
          //       // }
          //       resolve();

          //     }
          //   }
          // } else {
          //   // this.payLater();
          //   swal({
          //     closeOnClickOutside: false,
          //     title: "Error!",
          //     text: 'Payment Unsuccessful. Please Try again'
          //   });
          // }
          loader.style.display = "none";

          this.cs.pointingChange();

          // Local URL
          //  location.href = "http://localhost:4200/#/payment-confirmation";
          // Sanity URL
          // location.href = "https://nysa-uat.insurancearticlez.com/nysaui/#/payment-confirmation"
          // Production URL
          // location.href = "https://ipartnerms.icicilombard.com/nysa/#/payment-confirmation"
          // Prajakta
          // this.updatePaymentStatus().then(() => { this.updatePolicyStatus(); });
          // this.router.navigateByUrl('/payment-confirmation');
        })
        .catch((err: any) => {
          loader.style.display = "none";
          swal({
            closeOnClickOutside: false,
            title: "Error!",
            text: err,
          });
          resolve();
        });
    });
  }

  updatePaymentStatus(): Promise<any> {
    return new Promise((resolve: any) => {
      this.PFPayReq = JSON.parse(localStorage.getItem("PFReq"));
      this.PFPayRes = JSON.parse(localStorage.getItem("PFRes"));
      let body = {
        policyNo:
          this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].policyNo,
        coverNoteNo:
          this.PFPayRes.paymentTagResponse.paymentTagResponseList[0]
            .coverNoteNo,
        proposalNo: this.multipleProposal, //to be changed
        dealID: this.dealID,
        customerID: null, //to be changed
        pfPaymentID:
          this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].paymentID,
        paymentEntryErrorID: this.PFPayRes.paymentEntryResponse.error_ID,
        paymentEntryErrorText: this.PFPayRes.paymentEntryResponse.errorText,
        paymentEntryStatus: this.PFPayRes.paymentEntryResponse.status,
        paymentTagErrorID:
          this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].error_ID,
        paymentTagErrorText:
          this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].errorText,
        paymentTagStatus:
          this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].status,
        message: this.PFPayRes.message,
        PaymentMode: this.PaymentMode,
        statusMessage: this.PFPayRes.statusMessage,
        paymsRequestID: String(this.PFPayRes.PAYMS_RequestID),
        paymentRS: JSON.stringify(this.PFPayRes), // here pass full response string which is coming from Mihir api
        corelationID: this.PFPayRes.correlationId, // pass correlation from payment api response
      };
      let str = JSON.stringify(body);
      this.cs.post("Payment/UpdatePaymentDetails", str).then((res: any) => {
        this.updatePayment = res;
        resolve();
      });
    });
  }

  // MobileNo Validation
  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
