import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { CommonService } from "src/app/services/common.service";
import { ActivatedRoute, Router } from "@angular/router";
import swal from "sweetalert";
import * as moment from "moment";
declare var $: any;
@Component({
  selector: "app-plutus-payment-confirmation",
  templateUrl: "./plutus-payment-confirmation.component.html",
  styleUrls: ["./plutus-payment-confirmation.component.css"],
})
export class PlutusPaymentConfirmationComponent implements OnInit {
  quoteReq: any;
  quoteRes: any;
  propReq: any;
  propRes: any;
  newPolicyNumber: any;
  newProposalNumber: any;
  discount: any;
  finalamount: any;
  ordReq: any;
  ordRes: any;
  PFPayReq: any;
  PFPayRes: any;
  totalTax: any;
  pdfResponse: any;
  auth: any;
  paramDataValues: any;
  authtok: any;
  policyType: any;
  productCode: any;
  type: any;
  product: any;
  finalPremium: any;
  PolicyStartDate: any;
  PolicyEndDate: any;
  policySubType: any;
  feedBackResponse: any;
  addPolicyDetails: any;
  updatePaymentDetails: any;
  podetails: any;
  paydetails: any;
  timeTaken: any;
  starRatingFeed: any;
  timeseconds: any;
  checkInsta: any;
  SubProductType: any;
  updatePayment: any;
  PaymentMode: any;
  IsInstaDone: any;
  IsPayLinkSent: string;
  PayLinkReciever: any;
  CustomerPayLink: any;
  isInstaFlag: any;
  instaDetails: any;
  isMultiProp: any;
  proposals: any;
  proposalNo: any;

  constructor(
    public router: Router,
    public cs: CommonService,
    private location: Location,
    private activatedRoute: ActivatedRoute
  ) {
    let dateStart = JSON.parse(localStorage.getItem("AppStartTime"));
    this.activatedRoute.queryParams.subscribe((params) => {
      this.isMultiProp = params["isMulti"];
      this.proposalNo = params["proposalNo"];
    });

    let date2 = new Date();

    let timeDiff = date2.getTime() - dateStart;
    this.convertTime(timeDiff);
  }

  ngOnInit() {
    $("#smileys input").on("click", function () {
      $("#result").html($(this).val());
    });

    $("aside").removeClass("active");

    window.scrollTo(0, 0);

    (<any>window).ga("send", "event", {
      eventCategory: "Payment Confirmation",
      eventLabel: "" + this.product + "" + this.policyType,
      eventAction: "Policy issued",
      eventValue: 10,
    });

    this.starRatingFeed = "5";
    if (this.isMultiProp == "0") {
      // localStorage.removeItem("dataforBack");
      this.isInstaFlag = localStorage.getItem("bankType");
      this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
      this.authtok = JSON.parse(localStorage.getItem("AuthToken"));
      this.policyType = this.paramDataValues.iPartnerLogin.policy;
      this.productCode = this.paramDataValues.iPartnerLogin.product;
      if (this.policyType == "NEW") {
        this.type = "New";
      } else {
        this.type = "Rollover";
      }
      if (this.productCode == "2312") {
        this.product = "Two Wheeler";
      } else {
        this.product = "Four Wheeler";
      }

      this.newPolicyNumber = JSON.parse(
        localStorage.getItem("newPolicyNumber")
      );

      // if (JSON.parse(localStorage.getItem('calQuoteReq'))) {
      //   this.quoteReq = JSON.parse(localStorage.getItem('calQuoteReq'));
      //   this.quoteRes = JSON.parse(localStorage.getItem('calQuoteRes'));
      // } else {
      //   this.quoteReq = JSON.parse(localStorage.getItem('calQuoteTPReq'));
      //   this.quoteRes = JSON.parse(localStorage.getItem('calQuoteTPRes'));
      // }

      // this.propReq = JSON.parse(localStorage.getItem('PropDatareq'));
      // this.propRes = JSON.parse(localStorage.getItem('PropDatares'));
      // this.finalPremium = Math.round(this.propRes.finalPremium);
    } else {
      this.proposals = JSON.parse(localStorage.getItem("multipleProposal"));
      this.finalPremium = localStorage.getItem("multiTotalAmount");
    }

    // if (this.isInstaFlag == 'INT') {
    //   this.PaymentMode = '2';
    //   this.getInstaPolicyDetails().then(() =>{
    //     if ((this.instaDetails.coverNoteNo != null || this.instaDetails.coverNoteNo != undefined || this.instaDetails.coverNoteNo != '')) {
    //       this.updateInstaPaymentStatus().then(()=>{
    //         this.updateInstaPolicyStatus();
    //       })
    //     }
    //   })
    // }

    this.getProposalData().then((res: any) => {
      this.PlutusPaymentPaymsToken();
      // this.getInstaPolicyDetails();
      this.starRating("0");
    });
  }

  // Go to Dashboard
  goToDashboard() {
    this.starRating("0");
    this.cs.geToDashboard();
  }

  goBack() {
    if (this.isMultiProp == "0") {
      this.starRating("0");
    }
    this.router.navigateByUrl("quote");
    this.cs.clearLocalStorage();
    this.cs.clearQuoteDate();
    this.cs.clearPropData();
    localStorage.setItem("myNysaPolicy", "true");
  }

  starRating(ev: any) {
    this.starRatingFeed = ev;
    let breakinFlag: any = localStorage.getItem("breakinFlag");
    if (breakinFlag == "true") {
      let breakinData: any = JSON.parse(localStorage.getItem("breakinData"));
      this.policySubType = breakinData.policySubType;
    } else {
      if (this.productCode == "2312") {
        this.policySubType = 1;
      } else if (this.productCode == "2311") {
        this.policySubType = 2;
      } else if (this.productCode == "2320") {
        this.policySubType = 9;
      } else if (this.productCode == "2319") {
        this.policySubType = 10;
      }
    }

    let body = {
      PolicySubType: JSON.stringify(this.policySubType), // mandatory
      ProposalNumber: this.propRes.generalInformation.proposalNumber, // mandatory
      StarRating: JSON.parse(this.starRatingFeed), // mandatory
      PolicyCreatedIn: this.timeTaken,
      Feedback: "", // non mandatory , do not show on screen
      PolicyCreatedInSecond: JSON.stringify(this.timeseconds),
    };
    let str = JSON.stringify(body);
    this.cs.post("Feedback/GetUserFeedback", str).then((res: any) => {
      this.feedBackResponse = res;

      (<any>window).ga("send", "event", {
        eventCategory: "Policy Punched",
        eventLabel:
          "" + this.product + "" + this.policyType + "" + this.timeseconds,
        eventAction: "Policy Punched",
        eventValue: 10,
      });
    });
  }

  //time convert
  convertTime(timeDiff) {
    //   var msec = timeDiff;
    //   this.timeseconds = Math.round(msec / 1000);
    //   var hh = Math.floor(msec / 1000 / 60 / 60);
    //   msec -= hh * 1000 * 60 * 60;;
    //   var mm = Math.floor(msec / 1000 / 60);
    //   msec -= mm * 1000 * 60;
    //   var ss = Math.floor(msec / 1000);
    //   msec -= ss * 1000;
    //   if(hh == 0) {
    //     this.timeTaken = mm + ' Minutes ' + ss + ' Seconds ';
    //   } else {
    //     this.timeTaken = hh + ' Hours ' + mm + ' Minutes ' + ss + ' Seconds '; }
    //   let clock = setInterval(() => {
    //     clearInterval(clock)
    //     clock = null
    //     if(mm >= 5) { document.getElementById('timeTaken').style.color = 'red'; } else
    //     if(mm < 2) { document.getElementById('timeTaken').style.color = 'green'; } else
    //     { document.getElementById('timeTaken').style.color = 'orange'; }
    // }, 200)
  }

  // Download PDF Policy
  showPolicy() {
    this.cs.loaderStatus = true;
    this.cs
      .get(
        "PolicySchedule/GeneratePolicySchedule?policyNo=" +
          this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].policyNo
      )
      .subscribe(
        (res) => {
          this.pdfResponse = res;
          if (
            this.pdfResponse != null &&
            this.pdfResponse != undefined &&
            this.pdfResponse.status == "SUCCESS"
          ) {
            var fileName =
              this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].policyNo
                .split("/")
                .join("_") + ".pdf";
            this.cs.save(
              "data:application/pdf;base64," + this.pdfResponse.buffer,
              fileName
            );
            this.cs.loaderStatus = false;
          } else {
            swal({
              text: "There seems to be an issue with the system, Kindly try after sometime. Regret the inconvenience",
            });
          }
          this.cs.loaderStatus = false;
        },
        (err) => {
          swal({
            text: err.error.ExceptionMessage,
          });
          this.cs.loaderStatus = false;
          document.getElementById("myNav4").style.height = "0%";
        }
      );
  }

  // Update Insta Policy Data to database
  updateInstaPolicyStatus() {
    this.totalTax = Math.round(this.propRes.totalTax);
    if (this.productCode == "2320" || this.productCode == "2319") {
      this.propRes.specialDiscount = "0";
      this.propRes.finalPremium = JSON.stringify(this.propRes.finalPremium);
      this.propRes.policyType = 1;
    }
    let policysubType = this.propRes.policySubType;
    let breakinFlag: any = localStorage.getItem("breakinFlag");
    if (breakinFlag == "true") {
      let breakinData: any = JSON.parse(localStorage.getItem("breakinData"));
      this.SubProductType = breakinData.policySubType;
    } else {
      if (this.productCode == "2312") {
        this.SubProductType = "1";
      } else if (this.productCode == "2311") {
        this.SubProductType = "2";
      } else if (this.productCode == "2320") {
        this.SubProductType = "9";
      } else if (this.productCode == "2319") {
        this.SubProductType = "10";
      }
    }
    let body;
    body = {
      DealID: this.propReq.DealId,
      IsInstaDone: this.IsInstaDone,
      PolicyType: "1", //String
      PolicySubType: this.SubProductType, //String
      ProposalNumber: this.propRes.generalInformation.proposalNumber,
      PolicyNumber: this.instaDetails.policyNo,
      PolicyStartDate: this.propReq.PolicyStartDate,
      PolicyEndDate: this.propReq.PolicyEndDate,
      Status: this.propRes.statusID,
      BasicPremium: this.propRes.totalLiabilityPremium,
      Discount: parseFloat(this.propRes.specialDiscount), //String
      ServiceTax: this.totalTax,
      TotalPremium: parseFloat(this.propRes.finalPremium), //String
      AddressID: "",
      Isactive: "1",
      PaymentID: JSON.stringify(this.updatePayment.paymentID),
      SalesTax: "",
      Surcharge: "",
      TotalTax: this.totalTax, //String
      TransFor: this.propRes.generalInformation.transactionType,
      TaxRate: "",
      CustomerType: this.propReq.CustomerType,
      PreviousPolicyNumber: null,
      PFPolicyNo: this.instaDetails.policyNo,
      PFProposalNo: this.instaDetails.proposalNo,
      PFCovernoteNo: this.instaDetails.coverNoteNo,
      PFCustomerID: this.propRes.generalInformation.customerId,
      CustomerName: this.propReq.CustomerDetails.CustomerName,
      PFPaymentID: null,
      SumInsured: null,
      Planname: null,
      PolicyTenure: this.propRes.generalInformation.tenure,
      PolicyTPTenure: this.propRes.generalInformation.tpTenure,
      PolicyPACoverTenure: JSON.stringify(this.propReq.PACoverTenure),
      UTGSTAmount: "0",
      TotalGSTAmount: "0",
      CGSTAmountRate: null,
      SGSTAmountRate: null,
      IGSTAmountRate: null,
      UTGSTAmountRate: null,
      TOTALGSTAmountRate: null,
      TransOn: "2020-03-18",
      CreatedBy: null,
      CreatedOn: "2020-03-18",
      ModifiedBy: null,
      ModifiedOn: "2020-03-18",
      CorelationID: this.propRes.correlationId,
      IsPayLinkSent: this.IsPayLinkSent,
      PayLinkReciever: this.PayLinkReciever,
      CustomerPayLink: this.CustomerPayLink,
      // "IsStandaloneOD" : null,
      // "IsSubagent":null,
      // "SubagentIpartnerUserID":null
    };
    if (this.propReq.Tenure == "1" && this.propReq.TPTenure == "0") {
      body.IsStandaloneOD = true;
    }
    if (
      this.paramDataValues.iPartnerLogin.isSubagent != null ||
      this.paramDataValues.iPartnerLogin.isSubagent != undefined
    ) {
      body.IsSubagent = true;
      body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
    }
    let str = JSON.stringify(body);
    this.cs.post("Policy/AddPolicyDetails", body).then((res: any) => {});
  }

  updateInstaPaymentStatus(): Promise<any> {
    return new Promise((resolve: any) => {
      let body = {
        policyNo: this.instaDetails.policyNo,
        coverNoteNo: this.instaDetails.coverNoteNo,
        proposalNo: this.propRes.generalInformation.proposalNumber,
        dealID: this.propReq.DealId,
        customerID: this.propRes.generalInformation.customerId,
        pfPaymentID: null,
        paymentEntryErrorID: null,
        paymentEntryErrorText: null,
        paymentEntryStatus: null,
        paymentTagErrorID: null,
        paymentTagErrorText: this.instaDetails.errorText,
        paymentTagStatus: "",
        message: this.instaDetails.errorText,
        PaymentMode: this.PaymentMode,
        statusMessage: this.instaDetails.errorText,
        paymsRequestID: null,
        paymentRS: JSON.stringify(this.instaDetails), // here pass full response string which is coming from Mihir api
        corelationID: this.propRes.correlationId, // pass correlation from payment api response
      };
      let str = JSON.stringify(body);
      this.cs.post("Payment/UpdatePaymentDetails", str).then((res: any) => {
        this.updatePayment = res;
        this.updateInstaPolicyStatus();
        resolve();
      });
    });
  }

  getInstaPolicyDetails(): Promise<any> {
    return new Promise((resolve: any) => {
      let body = {
        ProposalNo: this.proposalNo
          ? this.proposalNo
          : this.propRes.generalInformation.proposalNumber,
      };

      let str = JSON.stringify(body);
      this.cs.postPayms("Payment/GetInstaDetails", str).then((res: any) => {
        if (res.statusMessage == "Success") {
          this.instaDetails = res.paymentTagResponse.paymentTagResponseList[0];
          if (res.IsInsta == "1") {
            this.PaymentMode = "2";
            this.IsInstaDone = true;
            // this.updateInstaPaymentStatus().then(() => {
            //   // this.updateInstaPolicyStatus();
            // });
            resolve();
          } else {
            this.IsInstaDone = false;
            resolve();
          }
        } else {
          swal({
            text: res.result.paymentEntryResponse.errorText,
          });
          resolve();
        }
      });
    });
  }

  getProposalData(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs
        .get1("Proposal/getProposalDetails?proposalNo=" + this.proposalNo)
        .then((res: any) => {
          this.propReq = JSON.parse(res.proposalRQ);
          this.propRes = JSON.parse(res.proposalRS);
          this.finalPremium = Math.round(this.propRes.finalPremium);
          this.newProposalNumber = this.propRes.proposalNumber;
          if (res.isInsta) {
            this.isInstaFlag == "INT";
            this.PaymentMode = "2";
            // this.getInstaPolicyDetails().then(() =>{
            //   if ((this.instaDetails.coverNoteNo != null || this.instaDetails.coverNoteNo != undefined || this.instaDetails.coverNoteNo != '')) {
            // this.updateInstaPaymentStatus().then(() => {
            //   // this.updateInstaPolicyStatus();
            // });
            // }
            // })
          } else {
            this.isInstaFlag == "MAT";
          }
          console.log("Proposal Req", this.propReq);
          console.log("Proposal Res", this.propRes.generalInformation);
          resolve();
        });
    });
  }

  /**
   * fetching the paymsToken from api before redirecting to plutus payment page
   * Author :- Sumit
   * date :- 21-01-2021
   */
  PlutusPaymentPaymsToken() {
    this.cs.getPaymsToken().then((resp: any) => {
      localStorage.setItem("paymsToken", JSON.stringify(resp.accessToken));
      this.getInstaPolicyDetails();
    });
  }
}
