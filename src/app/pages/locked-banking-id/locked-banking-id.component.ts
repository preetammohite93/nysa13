import { Component, OnInit } from "@angular/core";
import * as moment from "moment";
import { ApiServiceService } from "src/app/services/api-service.service";
import { CommonService } from "src/app/services/common.service";

@Component({
  selector: "app-locked-banking-id",
  templateUrl: "./locked-banking-id.component.html",
  styleUrls: ["./locked-banking-id.component.css"],
})
export class LockedBankingIDComponent implements OnInit {
  pendingObj: any = {};
  memmaxDOB: any;
  spinloader: boolean = false;
  successMsg: boolean = false;
  showTable: boolean = false;
  pendingCasesList: any;
  pendingCasesList_proto: any;
  searchTerm: any;
  productCode = "2311";
  agentId: any;
  dealDetails: any;
  page: any;
  constructor(public cs: CommonService, public apiService: ApiServiceService) {}

  ngOnInit() {
    let auth = JSON.parse(localStorage.getItem("AuthToken"));
    this.dealDetails = JSON.parse(localStorage.getItem("DealID"));
    this.agentId = auth.username;
    this.memmaxDOB = new Date();
    this.memmaxDOB.setDate(this.memmaxDOB.getDate() + 0);
    let startDate = moment(new Date()).format("YYYY-MM-DD");
    this.pendingObj.FromDate = startDate;
    this.pendingObj.ToDate = startDate;
  }

  getDealData() {
    console.log(this.productCode);
    this.apiService
      .getDeal(this.agentId, this.productCode)
      .subscribe((res: any) => {
        this.dealDetails = res.dealID;
      });
  }

  getPendigCases() {
    console.log(this.productCode, this.dealDetails);
    let body = {
      FromDate: moment(this.pendingObj.FromDate).format("DD/MM/YYYY"),
      ToDate: moment(this.pendingObj.ToDate).format("DD/MM/YYYY"),
      ProductCode: this.productCode,
      DealNo: this.dealDetails,
    };

    let str = JSON.stringify(body);
    this.spinloader = true;

    this.cs
      .post("/BankingIDLock/GetBankingIDLockProposalList", str)
      .then((res: any) => {
        if (res.status == "SUCCESS") {
          this.showTable = false;
          this.spinloader = false;
          this.pendingCasesList_proto = res.proposalDetails;
          this.pendingCasesList = this.pendingCasesList_proto;
          // this.getFilter(this.filterCase);
        } else {
          this.spinloader = false;
          this.showTable = true;
        }
      })
      .catch((err: any) => {
        this.spinloader = false;
      });
  }
}
