import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { CommonService } from "src/app/services/common.service";
import swal from "sweetalert";
import { commonData } from "../../commonData/commonData";
import jwt_decode from "jwt-decode";
import { ApiServiceService } from "src/app/services/api-service.service";
import sizeof from 'object-sizeof'
@Component({
  selector: "app-swap",
  templateUrl: "./swap.component.html",
  styleUrls: ["./swap.component.css"],
})
export class SwapComponent implements OnInit {
  expiry: any;
  signature: any;
  iPartner_Token: any;
  policy: any;
  product: any;
  producttype: any;
  view: any;
  isSubagent: any;
  subAID: any;
  iPartnerUserID: any;
  manulist: any = {};

  /**
   * digitalPOS variables
   */
  IsPOSTransaction: boolean = false;
  PosmobileNumber: any;
  authResponse:any;


  constructor(
    public router: Router,
    public cs: CommonService,
    public activeRoute: ActivatedRoute,
    public api: ApiServiceService
  ) { }

  ngOnInit() {
    this.cs.clearLocalStorage();
    this.cs.clearPropData();
    this.cs.clearQuoteDate();
    localStorage.removeItem('scpaData');
    this.getParams().then(() => {
      if (this.product == "2312") {
        this.producttype = "Two Wheeler";
      } else if (this.product == "2320") {
        this.producttype = "Two Wheeler Liability";
      } else if (this.product == "2311") {
        this.producttype = "Four Wheeler";
      } else if (this.product == "2319") {
        this.producttype = "Four Wheeler Liability";
      } else {
        this.producttype = "";
      }
      if (this.view == "MVIEW") {
        // Mview
        swal({
          text: "Dear User, If you wish to opt for any deviation flow. Please click on Return button",
          dangerMode: true,
          closeOnClickOutside: false,
          buttons: ["Return", "Proceed"],
        }).then((willDelete) => {
          if (willDelete) {
            console.log("Continue");
          } else {
            console.log("Go Back");
            // this.router.navigateByUrl('/redirection?product=');
            location.href = "redirect.html";
          }
        });
      } else {
        console.log("WebView");
      }

      if (this.producttype == "") {
        swal({ text: "Invalid User" }).then(() => {
          this.cs.geToLogin();
        });
      } else {
        this.authentication();
      }
    });
    this.getParamsData();
  }

  // Get Parameters from redirection
  getParams(): Promise<any> {
    return new Promise((resolve: any) => {
      this.activeRoute.queryParams.forEach((params) => {
        this.expiry = params.expiry;
        this.signature = params.signature;
        this.iPartner_Token = params.iPartner_Token;
        this.product = params.product;
        this.policy = params.policy;
        this.view = params.view;
        this.isSubagent = params.isSubagent;
        this.subAID = params.subAID;

        /**
         * Digital POS Change
         * Created LocalStorage For Adding Checks
         * author :- digitalPOS
         * date :- 18-06-2021
         */
        if (params.hasOwnProperty("channelName")) {
          if (
            params.channelName != null &&
            params.channelName != undefined &&
            params.channelName != ""
          ) {
            this.IsPOSTransaction = true;
            localStorage.setItem(
              "IsPOSTransaction",
              JSON.stringify(this.IsPOSTransaction)
            );
            this.getDigitalPOSToken(); //digitalPOS Change :- Calling getDigitalPOSToken function for getting the pos token
            if (params.hasOwnProperty("LandingPage")) {
              if (localStorage.getItem("landingPage")) {
                localStorage.removeItem("landingPage");
              }
              localStorage.setItem(
                "landingPage",
                JSON.stringify(params.LandingPage)
              );
            } else {
              if (localStorage.getItem("landingPage")) {
                localStorage.removeItem("landingPage");
              }
            }
          } else {
            this.IsPOSTransaction = false;
            localStorage.setItem(
              "IsPOSTransaction",
              JSON.stringify(this.IsPOSTransaction)
            );
          }
        } else {
          this.IsPOSTransaction = false;
          localStorage.setItem(
            "IsPOSTransaction",
            JSON.stringify(this.IsPOSTransaction)
          );
        }

        resolve();
      });
    });
  }

  getParamsData() {
    let body;
    body = {
      Username: commonData.Username,
      Password: commonData.Password,
      iPartnerLogin: {
        Expiry: this.expiry,
        Signature: this.signature,
        iPartner_Token: this.iPartner_Token,
        product: this.product,
        policy: this.policy,
        view: this.view,
      },
    };
    if (this.isSubagent == "Y") {
      body.iPartnerLogin.isSubagent = this.isSubagent;
      body.iPartnerLogin.subAID = atob(this.subAID);
    }
    localStorage.setItem("paramData", JSON.stringify(body));
  }

  // authentication Service
  authentication() {
    if (
      this.cs.isUndefineORNull(this.expiry) &&
      this.cs.isUndefineORNull(this.signature) &&
      this.cs.isUndefineORNull(this.iPartner_Token)
    ) {
      swal({
        text: "You are unauthorized",
      });
    } else {
      let body = {
        Username: commonData.Username,
        Password: commonData.Password,
        iPartnerLogin: {
          Expiry: this.expiry,
          Signature: this.signature,
          iPartner_Token: this.iPartner_Token,
        },
      };

      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      // this.cs.getAuth('Token/CreateToken', str).then((res: any) => {
      this.cs.getAuth("Token/CreateToken", str).subscribe(
        (res: any) => {
          // let authToken = res.token;
          let authToken = this.authResponse = res;
          if(this.cs.isUndefineORNull(this.authResponse.applicationAccess.scpaDeal)){
            let body1 = {
              isSCPAAccess: false,
              scpaDeal: ''
            }
            localStorage.setItem('scpaData', JSON.stringify(body1))
          }else{
            let body1 = {
              isSCPAAccess: true,
              scpaDeal: this.authResponse.applicationAccess.scpaDeal
            }
            localStorage.setItem('scpaData', JSON.stringify(body1))
          }

          var decoded:any = jwt_decode(res.token);
          this.iPartnerUserID = decoded.sub;
          if (!this.cs.isUndefineORNull(authToken)) {
            localStorage.setItem('Nil_AuthToken', JSON.stringify(authToken));
            // //CreateToken + GetApplicationAccess merging code - Sejal
            let authBody = {
              expiry: authToken.expiry,
              token: authToken.token,
              username: authToken.username,
              applicationAccess: this.api.encrypt(authToken.applicationAccess),
            };
            localStorage.setItem("AuthToken", JSON.stringify(authBody));
            // localStorage.setItem("AuthToken", JSON.stringify(authToken));
            sessionStorage.setItem("token", JSON.stringify(authToken));
            this.cs.loaderStatus = false;

            /**
             * digitalPOS Change
             * calling the getPOSMobileNo function for fetching mobile no of pos agent
             */
            if (this.IsPOSTransaction == true) {
              this.getPOSMobileNo();
            }

            this.getManufactureNModelList();
            this.getManufactureNModelListAllRisk();
            // For Renewal Changes
            if (this.policy == 'RENEW') {
              this.router.navigateByUrl("/renewals");
              localStorage.setItem('ProductCode',JSON.stringify(this.product))
            } else {
              // Need to check for prod
              this.router.navigateByUrl("/quote");
            }

          } else {
            this.cs.loaderStatus = false;
            swal({
              text: "You are unauthorized",
            });
          }
          // localStorage.setItem('AuthToken', JSON.stringify(authToken));
        },
        (err) => {
          this.cs.loaderStatus = false;
          swal({ closeOnClickOutside: false, text: "Internal Server Error" });
        }
      );
      // .catch((err) => {
      //   this.cs.loaderStatus = false;
      //   swal({

      //     text: "Kindly try after sometime"
      //   });
      // })
    }
  }

  /**
   * digitalPOS Change
   * Get The Token For Digital Pos API's
   */
  getDigitalPOSToken() {
    let body = {
      Username: "W5u5hpCJNoNoQveeVqx8ef1f3aqMQjmC0pMPyGtq8bo=",
      Password: "W5u5hpCJNoNoQveeVqx8eWbeO9*byte*+X1KPhqfWAAOD2DU=",
    };

    let str = JSON.stringify(body);
    this.cs.loaderStatus = true;
    this.cs.getPOSToken("/digital-pos/token", str).then(
      (res: any) => {
        localStorage.setItem("digitalPosAuthToken", JSON.stringify(res));
      },
      (err) => {
        this.cs.loaderStatus = false;
        swal({ closeOnClickOutside: false, text: "Internal Server Error" });
      }
    );
  }

  /**
   * digitalPOS Change
   * getting the mobile no for digital pos agent
   */
  getPOSMobileNo() {
    this.cs.GetPOSMobile().then((res: any) => {
      this.PosmobileNumber = res.mobileNumber;
      localStorage.setItem("POSmobileNumber", JSON.stringify(res.mobileNumber));
      this.getDigitalPOSPaymsToken(res.mobileNumber);
    });
  }

  getDigitalPOSPaymsToken(POSmobileNumber) {
    this.cs.encryptCustomerData(POSmobileNumber).then((res: any) => {
      this.cs.getPOSPaymsToken(res).then((res: any) => {
        localStorage.setItem("accessTokenPOS", JSON.stringify(res.accessToken));
      });
    });
  }

  getManufactureNModelList() {
    if (localStorage.getItem("ManufactureModelList")) {
    } else {
      this.cs.getManufactureNModelListFull().then((resp: any) => {
        let manumod_4w = resp.makeNModelOptimiseRS.filter((option) => {
          return option.vehicleclasscode == "45";
        });
        console.info(sizeof(manumod_4w));
        let ManufactureModelList_4w: any =
          this.cs.encryptLikePythonForRTO(manumod_4w);
        localStorage.setItem(
          "ManufactureModelList_4w",
          ManufactureModelList_4w
        );
        console.info(sizeof(ManufactureModelList_4w));

        let manumod_2w = resp.makeNModelOptimiseRS.filter((option) => {
          return option.vehicleclasscode == "37";
        });
        console.info(sizeof(manumod_2w));
        let ManufactureModelList_2w: any =
          this.cs.encryptLikePythonForRTO(manumod_2w);
        localStorage.setItem(
          "ManufactureModelList_2w",
          ManufactureModelList_2w
        );
        console.info(sizeof(ManufactureModelList_2w));
      });
    }
  }

  getManufactureNModelListAllRisk() {
    if (localStorage.getItem("ManufactureModelList")) {
    } else {
      this.cs.getManufactureNModelListAllRisk().then((resp: any) => {
        let manumod_allRisk = resp;
        let ManufactureModelList_AllRisk: any =
          this.cs.encryptLikePythonForRTO(manumod_allRisk);
        localStorage.setItem(
          "ManufactureModelList_AllRisk",
          ManufactureModelList_AllRisk
        );
      });
    }
  }
}
