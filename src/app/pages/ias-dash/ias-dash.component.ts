import { Component, DoCheck, OnInit } from "@angular/core";
import { ApiServiceService } from "src/app/services/api-service.service";
import { CommonService } from "src/app/services/common.service";
import * as moment from "moment";
import { Router } from "@angular/router";
import { commonData } from "../../commonData/commonData";
import swal from "sweetalert";
import { CommonMessageService } from "src/app/services/common-message.service";

@Component({
  selector: "app-ias-dash",
  templateUrl: "./ias-dash.component.html",
  styleUrls: ["./ias-dash.component.css"],
})
export class IasDashComponent implements OnInit, DoCheck {
  dealDetails: any;
  agentId: any;
  memmaxDOB: any;
  pendingObj: any = {};
  IASCasesList_proto: any;
  IASCasesData: any;
  IASCasesDate = [{ index: "", value: "" }];
  searchTerm: any;
  page: any;
  spinloader: boolean = false;
  successMsg: boolean = false;
  showTable: boolean = false;
  startDate: any;
  startDate1: any;
  iasIntRes: any;
  saveProposalStatus: any;
  dataForCustLink:any;
  moberrorFlag: boolean = false;
  IsPayLinkSent:any;
  PayLinkReciever:any;
  isSMSSent:any;
  CustomerPayLink:any;
  updatePayment:any;
  custLink:any;
  paramDataValues:any;
  dealDetailsfromIM: any;
  mobNumberPattern = "^(0/91)?[6-9][0-9]{9}$";
  sendDetails = {
    Email: "",
    mobileno: "",
  };
  authtoken:any;
  productCode:any;
  IsPOSTransaction_flag = false;
  constructor(
    public cs: CommonService,
    public apiService: ApiServiceService,
    public router: Router,
    public msgService: CommonMessageService,
    public api: ApiServiceService
  ) {}

  ngOnInit() {
    this.authtoken = JSON.parse(localStorage.getItem("AuthToken"));
    this.dealDetails = JSON.parse(localStorage.getItem("DealID"));
    if (localStorage.getItem("IsPOSTransaction")) {
      this.IsPOSTransaction_flag = JSON.parse(
        localStorage.getItem("IsPOSTransaction")
      );
    }
    // this.startDate = JSON.parse(localStorage.getItem("startDate"));
    this.startDate = moment(new Date()).format("DD/MM/YYYY");
    this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
    this.agentId = this.authtoken.username;
    this.startDate1 = moment(this.startDate, "DD/MM/YYYY").format("MM/DD/YYYY");
    // console.log("Start Date", this.startDate, this.startDate1);
    if (localStorage.getItem("IASData1")) {
      let IASData = JSON.parse(localStorage.getItem("IASData1"));
      this.bindIASCases(IASData);
    } else {
      // this.getIASCases();   //GetIASList api call change - Sejal
    }
  }

  ngDoCheck() {
    let auth = JSON.parse(localStorage.getItem("AuthToken"));
    this.dealDetails = JSON.parse(localStorage.getItem("DealID"));
    // this.startDate = JSON.parse(localStorage.getItem("startDate"));
    this.startDate = moment(new Date()).format("DD/MM/YYYY");
    this.agentId = auth.username;
    this.startDate1 = moment(this.startDate, "DD/MM/YYYY").format("MM/DD/YYYY");
    // console.log("Start Date", this.startDate, this.startDate1);
    if (localStorage.getItem("IASData1")) {
      let IASData = JSON.parse(localStorage.getItem("IASData1"));
      this.bindIASCases(IASData);
    } else {
      // this.getIASCases();
    }
  }

  mobileValidate() {
    let isNumberValidate = this.cs.mobileValidation(this.sendDetails.mobileno);
    if (isNumberValidate && this.sendDetails.mobileno.length == 10) {
      this.sendDetails.mobileno = this.sendDetails.mobileno;
      this.moberrorFlag = false;
    } else {
      // this.sendDetails.mobileno = "";
      this.moberrorFlag = true;
    }
  }

  getDataForLink(data:any){
    console.log(data);
    this.dataForCustLink = data;
  }

  convertIAS1(data: any, type: any){
    if(data.policY_SUB_TYPE == '2'){
      this.productCode = "2311";
    }else{
      this.productCode = "2312";
    }
    if(this.IsPOSTransaction_flag == true){
      this.convertIAS(data, type);
    }else{
      this.getDeal().then(() => {
        this.getDealDetails(this.dealDetails.dealID).then(() => {
          this.convertIAS(data, type);
        });      
      });
    }
    
  }

  getDeal():Promise<any>{
    return new Promise((resolve:any) => {
      this.api
      .getDeal(this.authtoken.username, this.productCode)
      .subscribe((res: any) => {
        this.dealDetails = res;
        //localStorage.setItem("DealID", JSON.stringify(this.dealDetails));
        let dealid = this.dealDetails.dealID;
        localStorage.setItem("DealID", JSON.stringify(dealid));
        resolve();
      });
    });      
  }

  getDealDetails(dealID:any):Promise<any>{
    return new Promise((resolve:any) => {
      this.api.getDealDetails(dealID).subscribe((res: any) => {
        if (res.status == "FAILED") {
          resolve();
        } else {
          this.dealDetailsfromIM = res;
          resolve();
        }
      });
    });    
  }

  sendPaymentLinkforBoth() {
    console.log(this.dataForCustLink, JSON.parse(this.dataForCustLink.bizRequest))
    if(typeof(JSON.parse(this.dataForCustLink.bizRequest)) == 'string' || typeof(JSON.parse(this.dataForCustLink.bizResponse)) == 'string'){
      this.sendPaymentLinkRenew();
    }else{
      this.sendPaymentLink();
    }
  }

  sendPaymentLinkRenew() {
    let propRes:any; let propReq: any;
    let propReq1 = JSON.parse(this.dataForCustLink.bizRequest);
    let propRes1 = JSON.parse(this.dataForCustLink.bizResponse);
    propReq = JSON.parse(propReq1);
    propRes = JSON.parse(propRes1);
    if(this.dataForCustLink.policY_SUB_TYPE == '2'){
      this.productCode = "2311";
    }else{
      this.productCode = "2312";
    }
    console.log(propRes, propReq);
    let regNO = propRes.reqPacket ? propRes.reqPacket.ProposalDetails.RegistrationNumber : propReq.ProposalDetails.RegistrationNumber;
    let polNO = propReq.PolicyNumber ? propReq.PolicyNumber : propReq.policyNo;
    let body = {
      propNo: propRes.generalInformation.proposalNumber,
      policyNo: polNO,
      phoneNo: this.sendDetails.mobileno,
      emailId: this.sendDetails.Email,
      premium: propRes.finalPremium,
      regNo: regNO,
    };
    let str = JSON.stringify(body);
    this.cs.postForRenewals("sendPaymentLink", str).then((res: any) => {
      console.log(res);
      if (res.emailStatus) {
        swal({ text: "Payment link has been sent successfully." });
      } else {
        swal({ text: "Failed to send link." });
      }
    });
  }

  sendPaymentLink() {
    let propRes = JSON.parse(this.dataForCustLink.bizResponse);
    let propReq = JSON.parse(this.dataForCustLink.bizRequest);
    if(this.dataForCustLink.policY_SUB_TYPE == '2'){
      this.productCode = "2311";
    }else{
      this.productCode = "2312";
    }
    let body = {
      CorrelationID: propReq.CorrelationId,
      ProposalNo: propRes.generalInformation.proposalNumber,
      EmailID: this.cs.encrypt(this.sendDetails.Email, commonData.aesnysaKey),
      MobileNo: this.cs.encrypt(this.sendDetails.mobileno, commonData.aesnysaKey),
    };
    let url = "CustomerPayment/getLink";
    let str = JSON.stringify(body);
    let status = "N";
    let smsStatus = "N";
    this.cs.loaderStatus = true;

    this.cs.post(url, str).then((res: any) => {
      const emailStatus = res.isLinkSent ? true : false;
      const mobileStatus = res.isSMSSent ? true : false;
      const checkBoth = emailStatus && mobileStatus ? true : false;
      this.paymentStatusDetails(status, smsStatus, res, this.sendDetails.Email);

      let msg;
      let maskedEmail = this.sendDetails.Email.replace(
        /(.{2})(.*)(?=@)/,
        function (gp1, gp2, gp3) {
          for (let i = 0; i < gp3.length; i++) {
            gp2 += "X";
          }
          return gp2;
        }
      );
      let maskedMobile =
      this.sendDetails.mobileno.slice(0, 2) + this.sendDetails.mobileno.slice(2).replace(/.(?=...)/g, "X");
      if (checkBoth) {
        msg = `Payment link has been sent to ${maskedEmail} and ${maskedMobile}.`;
        status = "Y";
        smsStatus = "Y";
      } else {
        if (emailStatus) {
          msg = `Payment link has been sent to ${maskedEmail}.`;
          status = "Y";
          smsStatus = "N";
        } else if (mobileStatus) {
          msg = `Payment link has been sent to ${maskedMobile}.`;
          status = "N";
          smsStatus = "Y";
        } else if (!checkBoth) {
          msg = `Failed to send payment link to ${maskedEmail} and ${maskedMobile}.`;
          status = "N";
          smsStatus = "N";
        }
      }
      swal({ text: msg }).then(() => {
        this.cs.loaderStatus = false;
      });
    });
  }

  paymentStatusDetails(emailStatus, smsStatus, res, email) {
    this.IsPayLinkSent = emailStatus;
    this.PayLinkReciever = email;
    this.isSMSSent = smsStatus;
    this.CustomerPayLink = res.paymentLink;
    this.custLink = res;
    this.updatePaymentStatuscustlink().then(() => {
      this.updatePolicyStatusCustLink();
    });
  }

  // Update Policy Payment Data to database
  updatePaymentStatuscustlink(): Promise<any> {
    return new Promise((resolve: any) => {
      let propRes = JSON.parse(this.dataForCustLink.bizResponse);
      let propReq = JSON.parse(this.dataForCustLink.bizRequest);
      let body = {
        policyNo: "",
        coverNoteNo: "",
        proposalNo: propRes.generalInformation.proposalNumber,
        dealID: this.cs.isUndefineORNull(propReq.DealId) ? this.dealDetails.dealID : propReq.DealId, //this.dataForCustLink.deaL_ID,
        customerID: propRes.generalInformation.customerId,
        pfPaymentID: "",
        paymentEntryErrorID: "",
        paymentEntryErrorText: "",
        paymentEntryStatus: "",
        paymentTagErrorID: "",
        paymentTagErrorText: "",
        paymentTagStatus: "",
        message: "",
        PaymentMode: '4',
        statusMessage: "",
        paymsRequestID: "",
        paymentRS: "", // here pass full response string which is coming from Mihir api
        corelationID: propReq.CorrelationId, // pass correlation from payment api response
      };
      let str = JSON.stringify(body);
      this.cs.post("Payment/UpdatePaymentDetails", str).then((res: any) => {
        this.updatePayment = res;
        resolve();
      });
    });
  }

  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    console.log(event.target.value.length);
    let length = event.target.value.length;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  updatePolicyStatusCustLink() {
    // this.totalTax = Math.round(this.propRes.totalTax); 
    let SubProductType = this.dataForCustLink.policY_SUB_TYPE;
    let propRes = JSON.parse(this.dataForCustLink.bizResponse);
    let propReq = JSON.parse(this.dataForCustLink.bizRequest);
    let totalTax = propRes.totalTax
    console.log(propRes);
    console.log(propReq);
      
    
    let body;
    body = {
      DealID: this.cs.isUndefineORNull(propReq.DealId) ? this.dealDetails.dealID : propReq.DealId,
      PolicyType: "1", //String
      PolicySubType: SubProductType, //String
      ProposalNumber: propRes.generalInformation.proposalNumber,//this.dataForCustLink.proposaL_NUMBER,
      PolicyNumber: "",
      PolicyStartDate: propReq.PolicyStartDate, //moment(this.dataForCustLink.starT_DATE, "MM/DD/YYYY").format("YYYY-MM-DD"),
      PolicyEndDate: propReq.PolicyEndDate, //moment(this.dataForCustLink.enD_DATE, "MM/DD/YYYY").format("YYYY-MM-DD"),
      Status: propRes.StatusID,
      BasicPremium: propRes.totalLiabilityPremium,
      Discount: parseFloat(propRes.specialDiscount), //String
      ServiceTax: totalTax,
      TotalPremium: parseFloat(propRes.finalPremium), //String
      AddressID: "",
      Isactive: "1",
      PaymentID: JSON.stringify(this.updatePayment.paymentID),
      SalesTax: "",
      Surcharge: "",
      TotalTax: totalTax, //String
      TransFor: propRes.generalInformation.transactionType,
      TaxRate: "",
      CustomerType: propReq.CustomerType,
      PreviousPolicyNumber: null,
      PFPolicyNo: "",
      PFProposalNo: "",
      PFCovernoteNo: "",
      PFCustomerID: propRes.generalInformation.customerId,
      CustomerName: propReq.CustomerDetails.CustomerName,
      PFPaymentID: "",
      SumInsured: null,
      Planname: null,
      PolicyTenure: propRes.generalInformation.tenure,
      PolicyTPTenure: propRes.generalInformation.tpTenure,
      PolicyPACoverTenure: JSON.stringify(propReq.PACoverTenure),
      UTGSTAmount: "0",
      TotalGSTAmount: "0",
      CGSTAmountRate: null,
      SGSTAmountRate: null,
      IGSTAmountRate: null,
      UTGSTAmountRate: null,
      TOTALGSTAmountRate: null,
      TransOn: "2020-03-18",
      CreatedBy: null,
      CreatedOn: "2020-03-18",
      ModifiedBy: null,
      ModifiedOn: "2020-03-18",
      CorelationID: propReq.CorrelationId,
      IsPayLinkSent: this.IsPayLinkSent,
      PayLinkReciever: this.PayLinkReciever,
      CustomerPayLink: this.CustomerPayLink,
      smsreciver: this.sendDetails.mobileno,
      isSMSSent: this.isSMSSent,
    };
      if (propReq.Tenure == "1" && propReq.TPTenure == "0") {
        body.IsStandaloneOD = true;
      }
    

    if (
      this.paramDataValues.iPartnerLogin.isSubagent != null ||
      this.paramDataValues.iPartnerLogin.isSubagent != undefined
    ) {
      body.IsSubagent = true;
      body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
    }
    let str = JSON.stringify(body);
    this.cs.post("Policy/AddPolicyDetails", body).then((res: any) => { });
  }

  bindIASCases(data: any) {
    if (data.statusType == "Success") {
      // console.log(data.items[3].transOn.substring(0, 10));
      // console.log(data.items[2].transOn.substring(0, 10));
      this.showTable = false;
      this.spinloader = false;
      this.IASCasesList_proto = data.items;
      this.IASCasesData = this.IASCasesList_proto;
      console.log(this.IASCasesData);
    } else {
      this.spinloader = false;
      this.showTable = true;
    }
  }

  getIASCases() {
    console.log("HIIIIIIIIIIIIIIIIII");
    this.cs.get("IAS/GetIASList").subscribe((res: any) => {
      console.log(res);
      if (res.statusType == "Success") {
        this.showTable = false;
        this.spinloader = false;
        this.IASCasesList_proto = res.items;
        console.log(this.IASCasesList_proto);
        this.IASCasesData = this.IASCasesList_proto;
        console.log(this.IASCasesData.items);
      } else {
        this.spinloader = false;
        this.showTable = true;
      }
    });
  }

  convertIAS(data: any, type: any) {
    localStorage.setItem("bankType", data.bankType);
    console.log("HI",this.dealDetailsfromIM);
    
    let isInsta;
    let propReq;
    let propRes;
    if(this.IsPOSTransaction_flag == false){
    if (this.dealDetailsfromIM.bankType == "INT") {
      isInsta = 1;
    } else {
      isInsta = 0;
    }
  }else{
    isInsta = 0;
  }
    console.log("IAS Data", data, type);
    console.log("IAS Data", typeof(JSON.parse(data.bizRequest)), typeof(JSON.parse(data.bizResponse)));
    if(typeof(JSON.parse(data.bizRequest)) == 'string' || typeof(JSON.parse(data.bizResponse)) == 'string'){
      let propReq1 = JSON.parse(data.bizRequest);
      let propRes1 = JSON.parse(data.bizResponse);
      propReq = JSON.parse(propReq1);
      propRes = JSON.parse(propRes1);
    }else{
      propReq = JSON.parse(data.bizRequest);
      propRes = JSON.parse(data.bizResponse);
    }
    console.log("IAS Data", typeof(propReq), propRes);
    console.log("IAS Data", propReq, propRes);
    let isIAS = true;
    localStorage.setItem("isIASFlag", JSON.stringify(isIAS));
    localStorage.setItem("IASDataProp", JSON.stringify(data));
    if(typeof(JSON.parse(data.bizRequest)) == 'string' || typeof(JSON.parse(data.bizResponse)) == 'string'){
      console.log(propReq);
      console.log(propRes);
      if (type == "Recalculate") {
        localStorage.setItem("renewBreakInData", JSON.stringify(data));
        this.router.navigateByUrl("renewalmodifyproposal");
      }else if(propReq.BusinessType == "Roll Over"){
        this.cs.redirectToPlutus(propReq, propRes, isInsta, 0, false, 0);
      }else {
        this.cs.redirectToPlutusIAS(propReq, propRes, isInsta, 0, false, 0);
      }
    }else{
      localStorage.setItem("PropDatareq", JSON.stringify(propReq));
      localStorage.setItem("calQuoteReq", JSON.stringify(propReq));
      localStorage.setItem("PropDatares", JSON.stringify(propRes));
      localStorage.setItem("calQuoteRes", JSON.stringify(propRes));
      if (type == "Recalculate") {
        this.router.navigateByUrl("proposal");
      }else if(propReq.BusinessType == "Roll Over"){
        this.cs.redirectToPlutus(propReq, propRes, isInsta, 0, false, 0);
      }else {
        this.cs.redirectToPlutusIAS(propReq, propRes, isInsta, 0, false, 0);
      }
    }
    
  }
  
  rePushIAS(data: any) {
    console.log(data);
    let body = JSON.parse(data.odIasRQ);
    this.cs.postILSBIAS("/Genus/IASInteractionID", body).subscribe(
      (res: any) => {
        console.log(res);
        if (res.status == "Success") {
          this.iasIntRes = res;
          this.cs.loaderStatus = false;
          this.updateDeviationID(data);
        } else {
          this.router.navigateByUrl("quote");
          this.cs.loaderStatus = false;
        }
      },
      (err) => {
        // swal({ closeOnClickOutside: false, text: err });
        let errMsg = this.msgService.errorMsg(err)
        swal({ closeOnClickOutside: false, text: errMsg });
        this.cs.loaderStatus = false;
        this.router.navigateByUrl("quote");
      }
    );
  }

  updateDeviationID(data: any) {
    console.log(data);
    let body = JSON.parse(data.odIasRQ);
    console.log(body);
    let req = {
      SRT: body.SRT,
      OldIASID: data.odIasID,
      NewIASID: this.iasIntRes.srRequestNo,
    };
    let str = JSON.stringify(req);
    this.cs.post("IAS/IASRepush", str).then((res: any) => {
      this.getIASCases();
    });
  }

  saveofflineProposal(data: any): Promise<any> {
    return new Promise((resolve: any) => {
      let URL: any;
      URL = "Proposal/fw/SaveProposal";
      let propRes = JSON.parse(data.bizResponse);
      let propReq = JSON.parse(data.bizRequest);
      let odRQ = JSON.parse(data.odIasRQ);
      let idvRQ = JSON.parse(data.idvIasRQ);
      let bankType = localStorage.getItem("bankType");
      let body = {
        proposalRQ: propReq,
        proposalRS: propRes,
        bankType: bankType,
        IasRQ: {
          OdIasID:
            propRes.isQuoteDeviation && data.idvIasSrt != "IDV Deviation "
              ? this.iasIntRes.srRequestNo
              : "",
          OdIasSrt:
            propRes.isQuoteDeviation && data.idvIasSrt != "IDV Deviation "
              ? data.odIasSrt
              : "",
          OdIasSubSrt:
            propRes.isQuoteDeviation && data.idvIasSrt != "IDV Deviation "
              ? data.odIasSubSrt
              : "",
          IdvIasID:
            propRes.isQuoteDeviation && data.idvIasSrt == "IDV Deviation "
              ? this.iasIntRes.srRequestNo
              : "",
          IdvIasSrt:
            propRes.isQuoteDeviation && data.idvIasSrt == "IDV Deviation "
              ? data.idvIasSrt
              : "",
          IdvIasSubSrt:
            propRes.isQuoteDeviation && data.idvIasSrt == "IDV Deviation "
              ? data.idvIasSubSrt
              : "",
          OdIasRQ: odRQ,
          OdIasRS:
            data.idvIasSrt != "IDV Deviation"
              ? JSON.stringify(this.iasIntRes)
              : "",
          IdvIasRQ: idvRQ,
          IdvIasRS:
            data.idvIasSrt == "IDV Deviation"
              ? JSON.stringify(this.iasIntRes)
              : "",
        },
      };

      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs
        .post(URL, str)
        .then((res: any) => {
          if (res.status == "Success") {
            this.saveProposalStatus = res.status;
            this.getIASCases();
          } else {
            let errorbody = {
              RequestJson: JSON.stringify(body),
              ResponseJson: JSON.stringify(res),
              ServiceURL: URL,
              CorrelationID: data.corelationID,
            };
            this.api.adderrorlogs(errorbody);
          }

          this.cs.loaderStatus = false;
          resolve();
        })
        .catch((err: any) => {
          // swal({ text: err });
          let errMsg = this.msgService.errorMsg(err)
          swal({ closeOnClickOutside: false, text: errMsg });
          this.cs.loaderStatus = false;
        });
    });
  }
}
