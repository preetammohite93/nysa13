import { LombardpayWalletComponent } from "./lombardpay-wallet.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { LombardpayWalletRoutingModule } from "./lombardpay-wallet-routing.module";
import { SharedModule } from "src/app/shared/shared.module";
import { SharedtranslateModule } from "src/app/shared/sharedtranslate/sharedtranslate.module";

@NgModule({
  declarations: [LombardpayWalletComponent],
  imports: [
    CommonModule,
    LombardpayWalletRoutingModule,
    SharedModule,
    SharedtranslateModule,
  ],
})
export class LombardpayWalletModule {}
