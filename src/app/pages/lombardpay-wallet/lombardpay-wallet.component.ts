import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import * as moment from "moment";
import { commonData } from "../../commonData/commonData";
// import { ApiService } from 'src/app/services/api.service';
import { CommonService } from "src/app/services/common.service";
// import Swal from 'sweetalert2';
import swal from "sweetalert";
declare var $: any;

declare var Razorpay: any;

@Component({
  selector: "app-lombardpay-wallet",
  templateUrl: "./lombardpay-wallet.component.html",
  styleUrls: ["./lombardpay-wallet.component.css"],
})
export class LombardpayWalletComponent implements OnInit {
  addMoney: any = {};
  transId: any;
  orderdata: any;
  options: any;
  razorOrderData: any;
  paymentMode: any;
  custMobile: any;
  custEmail: any;
  bankData: any;
  showInstaBlock: boolean = false;
  second_time_function_calling: boolean = false;
  difference: number;
  dealType: string;
  userFlag: any;
  proposalNo: any;
  quoteId: any;
  seqID: any;
  PremiumAmont: any;
  transactionType: any;

  /**
   * Variables For Wallet Pay
   */
  IsInstaDone: boolean;
  propRes: any;
  lombardpaybalance: any;
  orderId: any;
  policyType: any;
  product: any;
  finalamount: any;
  paylombard: any;
  propReq: any;
  startDate: any;
  createWalletFlag: boolean = false;
  creditList: any = [];
  debitList: any = [];
  showPaymentModes: boolean = false;
  isShowDivIf: boolean = false;
  currentDate: any;
  month: any;
  monthDisplay: any;
  year: any;
  dealId: any;
  postObj: any = {};
  iPartnerUserId: any;
  // Payment Model
  paymentDetails = {
    Name: "",
    Email: "",
    Contact: "",
    balance: "",
    amount: "",
  };
  paramDataValues: any;
  productCode: any;
  type: string;
  userdetails: any;
  isName: any;
  isEmail: any;
  isContact: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private cs: CommonService,
    private changeDetector: ChangeDetectorRef
  ) {}

  ngOnInit() {
    if (!this.cs.isUndefineORNull(localStorage.getItem("orderdataPlutus"))) {
      this.orderdata = JSON.parse(localStorage.getItem("orderdataPlutus"));
      this.userFlag = this.orderdata.UserFlag;
      this.proposalNo = this.orderdata.ProposalNo;
      this.quoteId = this.orderdata.QuoteID;
      this.custMobile = this.orderdata.ContactNo;
      this.custEmail = this.orderdata.Email;
      this.PremiumAmont = JSON.parse(this.orderdata.Amount);

      this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));

      this.productCode = this.paramDataValues.iPartnerLogin.product;
      // if(this.productCode == '2312' || this.productCode == '2320') { this.classCode = "37" }
      // else if(this.productCode == '2311' || this.productCode == '2319') { this.classCode = "45" }
      if (this.policyType == "NEW") {
        this.type = "New";
      } else {
        this.type = "Rollover";
      }
      if (this.productCode == "2312") {
        this.product = "Two Wheeler";
      } else if (this.productCode == "2311") {
        this.product = "Four Wheeler";
      } else if (this.productCode == "2320") {
        this.product = "Two Wheeler Liability";
      } else if (this.productCode == "2319") {
        this.product = "Four Wheeler Liability";
      }
    }

    console.info("Testing purpose");
    this.currentDate = moment();
    this.month = moment(this.currentDate).format("MM");
    this.monthDisplay = moment(this.currentDate).format("MMMM");
    this.year = moment(this.currentDate).format("YYYY");
    this.transactionType = "CREDIT";

    if (!this.cs.isUndefineORNull(localStorage.getItem("seqId"))) {
      this.seqID = localStorage.getItem("seqId");
    }

    // if (this.iPartnerUserId != null && this.iPartnerUserId != undefined) {
    this.cs.getlombardPay().then((resp: any) => {
      localStorage.setItem(
        "LombardPayAuthToken",
        JSON.stringify(resp.accessToken)
      );
      // this.getDealDetails();
      this.showBalance();
      this.userDetails();
    });
  }

  openPaymentMode() {
    let reg = /^[1-9][0-9]*$/;
    let flag = reg.test(this.paymentDetails.amount);
    if (this.paymentDetails.amount && flag == true) {
      this.isShowDivIf = true;
    } else {
      this.isShowDivIf = false;
    }
  }

  //   addmoney()
  // {
  //   this.cs.getlombardPay().then((resp: any) => {
  //     localStorage.setItem('LombardPayAuthToken', JSON.stringify(resp.accessToken));
  //     this.CreateWalletOrder().then(() => {
  //       this.userDetails();
  //     });
  //   });

  // }

  userDetails() {
    this.cs.get2("LombardPay/GetCustomerDetails").then((res: any) => {
      this.userdetails = res;
    });
  }

  showBalance() {
    this.cs.getlombardPay().then((resp: any) => {
      localStorage.setItem(
        "LombardPayAuthToken",
        JSON.stringify(resp.accessToken)
      );
      return new Promise((resolve: any) => {
        this.cs.get2("LombardPay/GetCustomerBalance").then((response: any) => {
          if (response.statusType == "SUCCESS") {
            this.lombardpaybalance = response.balance;
            this.changeDetector.detectChanges();
          } else {
            this.lombardpaybalance = "0";
          }
        });
      });
    });
  }

  sendAgentData() {
    if (this.cs.isUndefineORNull(this.paymentDetails.Name)) {
      // this.isName = false;
      $("#myModal").modal("show");
    } else if (this.cs.isUndefineORNull(this.paymentDetails.Email)) {
      // this.isEmail = false;
      $("#myModal").modal("show");
    } else if (this.cs.isUndefineORNull(this.paymentDetails.Contact)) {
      // this.isContact = false;
      $("#myModal").modal("show");
    } else {
      let body = {
        Name: this.paymentDetails.Name,
        Email: this.paymentDetails.Email,
        Contact: this.paymentDetails.Contact,
      };
      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs.post2("LombardPay/CreateCustomer", str).then((response: any) => {
        this.cs.loaderStatus = false;
        $("#myModalmoney").modal("show");
      });
    }
  }

  //Create order
  CreateWalletOrder(paymentMode: any): Promise<any> {
    // return new Promise((resolve: any) => {

    //   let amt;
    //   if (!this.paymentDetails.amount) {
    //     amt = 0;
    //   } else {
    //     amt = JSON.parse(this.paymentDetails.amount)
    //   }
    //   let postObj = {
    //     "amount": amt,
    //     "identifier": "wallet order test"
    //   }
    //   // let str = JSON.stringify(body);
    //   this.cs.loaderStatus = true;
    //   this.apiService.createWalletOrderData(postObj).subscribe((res: any) => {
    //     if (res.statusType == 'Success') {
    //       this.orderId = res.razorOrderID;
    //       this.LombardRazorPay(this.router);
    //       this.cs.loaderStatus = false;
    //       resolve();
    //     } else {
    //       //$("#myModal").modal('show');
    //       this.cs.loaderStatus = false;
    //       resolve();
    //     }
    //   });
    // });

    return new Promise((resolve: any) => {
      let amt;
      if (!this.paymentDetails.amount) {
        amt = 0;
      } else {
        amt = JSON.parse(this.paymentDetails.amount);
      }
      let body = {
        amount: amt,
        identifier: "wallet order test",
      };
      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs
        .post2("LombardPay/CreateWalletOrder", str)
        .then((response: any) => {
          if (response.statusType == "Success") {
            this.orderId = response.razorOrderID;
            this.LombardRazorPay(this.router, paymentMode);
            this.cs.loaderStatus = false;
            resolve();
          } else {
            $("#myModal").modal("show");
            resolve();
            this.cs.loaderStatus = false;
          }
        });
    });
  }

  changeDate(ev: any) {
    if (ev == "back") {
      this.currentDate = moment(this.currentDate).subtract(1, "month");
      this.month = moment(this.currentDate).format("MM");
      this.monthDisplay = moment(this.currentDate).format("MMMM");
      this.year = moment(this.currentDate).format("YYYY");
      this.cs.getlombardPay().then((resp: any) => {
        localStorage.setItem(
          "LombardPayAuthToken",
          JSON.stringify(resp.accessToken)
        );
        this.viewTransactions();
      });
    } else if (ev == "next") {
      this.currentDate = moment(this.currentDate).add(1, "month");
      this.month = moment(this.currentDate).format("MM");
      this.monthDisplay = moment(this.currentDate).format("MMMM");
      this.year = moment(this.currentDate).format("YYYY");
      this.cs.getlombardPay().then((resp: any) => {
        localStorage.setItem(
          "LombardPayAuthToken",
          JSON.stringify(resp.accessToken)
        );
        this.viewTransactions();
      });
    } else {
      console.log("something went wrong");
    }
  }

  // RazorPay Call
  LombardRazorPay(route: any, mode: any) {
    // var desc = "ICICI Lombard - Motor Insurance";
    // let self = this;
    // this.options = {
    //   "description": desc,
    //   "image": 'https://www.icicilombard.com/mobile/mclaim/images/favicon.ico',
    //   "currency": 'INR',
    //   "key": environment.Lombardpay_key,
    //   "order_id": this.orderId,
    //   "method": {
    //     "netbanking": {
    //       "order": ["ICIC", "HDFC", "SBIN", "UTIB", "IDFB", "IBKL"]
    //     }
    //   },
    //   "prefill": {
    //     email: this.custEmail,
    //     contact: this.custMobile,
    //     name: "",
    //     method: "netbanking"
    //   },
    //   "theme": {
    //     "color": '#E04844',
    //     "hide_topbar": true
    //   },
    //   "handler": function (response: any) {
    //     self.loadBalance(response, self, route)
    //   }
    // };
    // var rzp1 = new Razorpay(this.options);
    // rzp1.open();

    this.cs.getlombardPay();
    var desc = "ICICI Lombard - Motor Insurance";
    let self = this;
    this.options = {
      description: desc,
      image: "https://www.icicilombard.com/mobile/mclaim/images/favicon.ico",
      currency: "INR",
      key: commonData.LombardKey,
      order_id: this.orderId,
      method: {
        netbanking: {
          order: ["ICIC", "HDFC", "SBIN", "UTIB", "IDFB", "IBKL"],
        },
      },
      prefill: {
        email: this.userdetails.email,
        contact: this.userdetails.contact,
        name: this.userdetails.name,
        method: mode,
      },
      theme: {
        color: "#E04844",
        hide_topbar: true,
      },
      handler: function (response: any) {
        self.loadBalance(response, self, route);
      },
      modal: {
        ondismiss: function () {
          this.cs.loaderStatus = false;
        },
      },
    };
    var rzp1 = new Razorpay(this.options);
    rzp1.open();
  }

  //Load balance
  loadBalance(res: any, _self: any, route: any) {
    // return new Promise((resolve: any) => {
    //   let postObj = {
    //     "razorpay_order_id": res.razorpay_order_id,
    //     "razorpay_payment_id": res.razorpay_payment_id,
    //     "razorpay_signature": res.razorpay_signature
    //   }
    //   this.cs.loaderStatus = true;
    //   this.apiService.loadWalletBalance(postObj).subscribe((res: any) => {
    //     this.paymentDetails.amount = undefined;
    //     this.isShowDivIf = false;
    //     if (res.statusType == 'SUCCESS') {
    //       this.showBalance();
    //       this.cs.loaderStatus = false;
    //       resolve();
    //     } else {
    //       resolve();
    //       this.cs.loaderStatus = false;
    //     }
    //   });
    // });

    return new Promise((resolve: any) => {
      this.cs.getlombardPay().then((resp: any) => {
        localStorage.setItem(
          "LombardPayAuthToken",
          JSON.stringify(resp.accessToken)
        );
        let body = {
          razorpay_order_id: res.razorpay_order_id,
          razorpay_payment_id: res.razorpay_payment_id,
          razorpay_signature: res.razorpay_signature,
        };
        let str = JSON.stringify(body);
        this.cs.loaderStatus = true;
        this.cs
          .post2("LombardPay/TransferToWallet", str)
          .then((response: any) => {
            if (response.statusType == "SUCCESS") {
              this.paymentDetails.amount = "";
              this.isShowDivIf = false;
              this.showBalance();
              this.changeDetector.detectChanges();
              resolve();
            } else {
              this.showBalance();
              resolve();
              this.cs.loaderStatus = false;
            }
          });
      });
    });
  }

  /**
   * viewTransactions Code
   */

  trans(ev: any) {
    if (ev == "received-tab") {
      this.transactionType = "CREDIT";
    } else if (ev == "utilised-tab") {
      this.transactionType = "DEBIT";
    }
    this.viewTransactions();
  }

  // MobileNo Validation
  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  viewTransactions() {
    let TransactionType = "NETBANKING";
    this.cs.loaderStatus = true;
    return new Promise((resolve: any) => {
      this.cs
        .get2(
          "LombardPay/GetCustomerStatement?TransactionType=" +
            this.transactionType +
            "&month=" +
            this.month +
            "&year=" +
            this.year
        )
        .then((res: any) => {
          // this.apiService.getTrans('LombardPay/GetCustomerStatement?TransactionType=' + TransactionType + '&month=' + this.month + '&year=' + this.year).subscribe((res: any) => {

          if (res.statusType == "SUCCESS") {
            this.creditList = [];
            this.debitList = [];
            res.items.forEach((element) => {
              if (element.credit != 0) {
                this.creditList.push(element);
              }
              if (element.debit != 0) {
                this.debitList.push(element);
              }
            });
            this.cs.loaderStatus = false;
            resolve();
          } else {
            this.creditList = [];
            this.debitList = [];
            this.cs.loaderStatus = false;
            resolve();
          }
        })
        .catch((err: any) => {
          resolve();
          this.cs.loaderStatus = false;
        });
    });
  }
}
