import { Component, OnInit, OnChanges, DoCheck, Input } from "@angular/core";
import swal from "sweetalert";
import * as moment from "moment";
import { commonData } from "../../commonData/commonData";
import { CommonService } from "src/app/services/common.service";
import { Router, ActivatedRoute } from "@angular/router";
import { PaymentURL } from "src/environments/env";
import { ApiServiceService } from "src/app/services/api-service.service";
import { Observable, Subscription, interval } from "rxjs";
declare var Razorpay: any;
declare var $: any;

import { SendMessageService } from "src/app/services/send-message.service";
import { CommonMessageService } from "src/app/services/common-message.service";

@Component({
  selector: "app-pending-payment",
  templateUrl: "./pending-payment.component.html",
  styleUrls: ["./pending-payment.component.css"],
})
export class PendingPaymentComponent implements OnInit, DoCheck {
  @Input() nysaPolicydata;

  nysaPolicy: any;
  custid: any;
  pdfResponse: any;
  policyno: any;
  policyStartDate: any;
  policyEndDate: any;
  authtok: any;
  norecordflag: any;
  ProductType: any;
  term: any;
  bankingidData: any;
  corelationID: any;
  totalPremium: any;
  dealid: any;
  proposalNo: any;
  customerid: any;
  orderId: any;
  options: any;
  dealID: any;
  startDate: any;
  todayDate: any;
  diffDays: any;
  showRecal: any;
  PFPayReq: any;
  PFPayRes: any;
  payButton: boolean = false;
  proposalRQ: any;
  proposalRS: any;
  totalTax: any;
  pendingproData: any;
  policyType: any;
  productCode: any;
  paramDataValues: any;
  type: any;
  product: any;
  page: any;
  PayKey: any;
  proposalReq: any;
  proposalRes: any;
  custemail: any;
  custmob: any;
  custname: any;
  memmaxDOB: any;
  instatype: any;
  bankType: any;
  isResendLink: boolean = false;
  showbutton: boolean = false;
  refreshflag: boolean = false;
  showcheckButton: boolean = false;
  customerEmail: any;
  custMobile: any;
  mobNumberPattern = "^(0/91)?[6-9][0-9]{9}$";
  resendData: any;
  spinloaderpolicy: boolean = false;
  spinloaderrecal: boolean = false;
  loaderByPolicy: any;
  pendingpayment = {
    FromDate: "",
    ToDate: "",
    subType: "",
  };

  scpaRQ: any;
  scpaRS: any;
  isAllRisk = false;
  bundleID: any;
  scpaProposalNo: any;

  private updateSubscription: Subscription;

  singleData = [];
  proposalData = [];
  multitotalPremium = [];
  totalsum: number;
  nysapolicyCount: any;
  subtype: any;
  allData = [];

  /**
   * Digital pos variable
   */
  IsPOSTransaction_flag: boolean = false;
  isSendLinkForInstaPolicy = false;
  watchMe: any;
  PaymentMode: any;
  instaDataForSendLink: any;
  IsPayLinkSent: any;
  PayLinkReciever: any;
  isSMSSent: any;
  CustomerPayLink: any;
  custLink: any;
  propRes: any;
  propReq: any;
  updatePayment: any;
  isRenewalPolicy = false;
  isMobileValid = true;
  isEmailValid = false;

  constructor(
    public router: Router,
    public cs: CommonService,
    public activeRoute: ActivatedRoute,
    public api: ApiServiceService,
    public msgService: CommonMessageService,
    private messageService: SendMessageService
  ) {
    /**
     * digitalPOS change
     * get values from localStorage for checking if conditions in html file
     * date :- 29-07-2021
     */
    if (localStorage.getItem("IsPOSTransaction")) {
      this.IsPOSTransaction_flag = JSON.parse(
        localStorage.getItem("IsPOSTransaction")
      );
    }
  }

  ngOnChanges() {
    if (this.nysaPolicydata == true) {
      this.nysaPolicy = "";
    } else {
      this.nysaPolicy = "";
    }
  }

  ngOnInit() {
    this.PayKey = commonData.PayKey;

    this.memmaxDOB = new Date();
    this.memmaxDOB.setDate(this.memmaxDOB.getDate() + 0);

    this.bankType = localStorage.getItem("bankType");
    this.instatype = false;
    this.ProductType = "1";
    localStorage.removeItem("addPolicyDetails");
    localStorage.removeItem("updatePaymentDetails");
    localStorage.removeItem("multipleProposal");
    localStorage.removeItem("multiTotalAmount");
    localStorage.removeItem("allriskmultipleProposal");
    localStorage.removeItem("allriskmultiTotalAmount");

    // this.createPFPayment = this.createPFPayment.bind(this);
    // this.payByRazorPay = this.payByRazorPay.bind(this);
    this.startDate = moment(new Date()).format("YYYY-MM-DD");
    this.todayDate = moment(new Date()).format("YYYY-MM-DD");
    let startDate = moment(new Date()).format("YYYY-MM-DD");
    this.pendingpayment.FromDate = startDate;
    this.pendingpayment.ToDate = startDate;
    let accessFor = JSON.parse(localStorage.getItem("AccessFor"));
    if (!this.cs.isUndefineORNull(accessFor)) {
      this.cs.isPlutusEnable = accessFor.isEnablePlutus;
    } else {
      this.cs.isPlutusEnable = false;
    }
    this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
    this.policyType = this.paramDataValues.iPartnerLogin.policy;
    this.productCode = this.paramDataValues.iPartnerLogin.product;
    if (this.policyType == "NEW") {
      this.type = "New";
    } else {
      this.type = "Rollover";
    }
    if (this.productCode == "2312") {
      this.product = "Two Wheeler";
    } else if (this.productCode == commonData.allriskProductCode) {
      this.product = "Risk Electric Bike"
    } else {
      this.product = "Four Wheeler";
    }

    this.updateSubscription = interval(300000).subscribe((val) => {
      if (
        this.nysaPolicy == "" ||
        this.nysaPolicy == undefined ||
        this.nysaPolicy == null
      ) {
        this.refreshflag = false;
      } else {
        this.refreshflag = true;
        this.getpolicies();
      }
    });

    // this.updateSubscription = interval(3000).subscribe(
    //     (val) => { this.getpolicies() });

    // Creating a bundle ID 
    // let newBundleID:any;
    // newBundleID = Guid.create();
    // this.bundleID = newBundleID.value;
  }

  ngDoCheck() {
    this.PayKey = commonData.PayKey;

    this.memmaxDOB = new Date();
    this.memmaxDOB.setDate(this.memmaxDOB.getDate() + 0);

    this.bankType = localStorage.getItem("bankType");
    // this.instatype = false;
    // this.ProductType = "1";

    // this.createPFPayment = this.createPFPayment.bind(this);
    // this.payByRazorPay = this.payByRazorPay.bind(this);

    this.startDate = moment(new Date()).format("YYYY-MM-DD");
    this.todayDate = moment(new Date()).format("YYYY-MM-DD");
    let startDate = moment(new Date()).format("YYYY-MM-DD");
    // this.pendingpayment.FromDate = startDate;
    // this.pendingpayment.ToDate = startDate;

    this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
    this.policyType = this.paramDataValues.iPartnerLogin.policy;
    this.productCode = this.paramDataValues.iPartnerLogin.product;
    // if(this.policyType == "NEW") { this.type = 'New'; } else { this.type = 'Rollover' }
    // if(this.productCode == "2312") { this.product = 'Two Wheeler'; } else { this.product = 'Four Wheeler' }
  }

  PayInsta(data: any, isAdvPID: any) {
    localStorage.setItem("InstaData", JSON.stringify(data));
    localStorage.setItem("isPendingPayment", "true");
    let propRes1 = JSON.parse(data.proposalRS);
    if(typeof(propRes1) == 'object'){
      this.propRes = JSON.parse(data.proposalRS);
      this.propReq = JSON.parse(data.proposalRQ);
      this.isRenewalPolicy = false;
    }else{
      this.isRenewalPolicy = true;
      let RQ = JSON.parse(data.proposalRQ);
      let RQ1 = JSON.parse(RQ);
      let RS = JSON.parse(data.proposalRS);
      let RS1 = JSON.parse(RS);
      this.propReq = RQ1;
      this.propRes = RS1;
    }
    this.propReq.id = data.id;
    this.messageService.sendproposalreqData(this.propReq);
    this.messageService.sendproposalresData(this.propRes);

    if (data.emailID != "" && data.emailID != null) {
      localStorage.setItem("DisablecustLink", "true");
    }
    if (data.isInstaPayment == true) {
      // //CreateToken + GetApplicationAccess merging code - Sejal
      if (!this.cs.isUndefineORNull(localStorage.getItem("AuthToken"))) {
        let encryptedTokenaccess = JSON.parse(
          localStorage.getItem("AuthToken")
        );
        let Parsedtokenaccess: any = this.api.decrypt(
          encryptedTokenaccess.applicationAccess
        );
        let tokenaccess = JSON.parse(Parsedtokenaccess);
        this.cs.isPlutusEnable = tokenaccess.isEnablePlutus;
        if (
          this.cs.isPlutusEnable == true ||
          this.IsPOSTransaction_flag == true
        ) {
          if (isAdvPID == 1) {
            this.cs.redirectToPlutus(this.propReq, this.propRes, 1, 0, false, isAdvPID);
          } else {
            this.cs.redirectToPlutus(this.propReq, this.propRes, 1, 1, false, isAdvPID);
          }
        } else {
        }
      }
    }
  }

  sendPayInstaLink(data: any) {
    console.log(data);
    this.isSendLinkForInstaPolicy = true;
    this.instaDataForSendLink = data;
    this.watchMe = "fly";
    console.log(data);
    if (data.productCode == "2312" || data.productCode == "2320") {
      this.product = "Two Wheeler";
    } else if (data.productCode == "2311" || data.productCode == "2319") {
      this.product = "Four Wheeler";
    } else {
      this.product = "Risk Electric Bike";
    }
  }

  sendLinkForInsta() {
    let custEmail, custMobile;
    let propRes1 = JSON.parse(this.instaDataForSendLink.proposalRS);
    if(typeof(propRes1) == 'object'){
      this.propRes = JSON.parse(this.instaDataForSendLink.proposalRS);
      this.propReq = JSON.parse(this.instaDataForSendLink.proposalRQ);
      this.isRenewalPolicy = false;
    }else{
      this.isRenewalPolicy = true;
      let RQ = JSON.parse(this.instaDataForSendLink.proposalRQ);
      let RQ1 = JSON.parse(RQ);
      let RS = JSON.parse(this.instaDataForSendLink.proposalRS);
      let RS1 = JSON.parse(RS);
      this.propReq = RQ1;
      this.propRes = RS1;
    }
    (<any>window).ga("send", "event", {
      eventCategory: "Customer Link",
      eventLabel:
        "Customer Link Clicked" + "" + this.product + "" + this.policyType,
      eventAction: "Customer Link Clicked",
      eventValue: 10,
    });

    custEmail = this.customerEmail;
    custMobile = this.custMobile;

    let regemailtest =
      /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(
        this.customerEmail
      );

    if (
      this.customerEmail == "" ||
      this.customerEmail == undefined ||
      this.customerEmail == null ||
      !regemailtest
    ) {
      this.cs.loaderStatus = false;
      swal({
        closeOnClickOutside: false,
        text: "Insert Email Id",
      }).then(() => {
      });
    } else if (
      this.custMobile == "" ||
      this.custMobile == undefined ||
      this.custMobile == null
    ) {
      this.cs.loaderStatus = false;
      swal({
        closeOnClickOutside: false,
        text: "Insert Mobile Number",
      }).then(() => {
      });
    } else {
      if(this.isRenewalPolicy){
        this.sendPaymentLinkForRenewal();
        this.PaymentMode = "4";
        localStorage.setItem("senCustLink", "true");
      }else{
        this.getCustomerLink(custEmail, custMobile);
        this.PaymentMode = "4";
        localStorage.setItem("senCustLink", "true");
      }
      
    }
  }

  getCustomerLink(email: any, mobile: any) {
    
    console.log('Proposal Response', typeof(this.propRes), typeof(this.propReq));
    let body = {
      CorrelationID: this.propRes.correlationId,
      ProposalNo: this.propRes.generalInformation.proposalNumber,
      EmailID: this.cs.encrypt(email, commonData.aesnysaKey),
      MobileNo: this.cs.encrypt(mobile, commonData.aesnysaKey),
    };
    let url = "CustomerPayment/getLink";
    let str = JSON.stringify(body);
    let status = "N";
    let smsStatus = "N";
    this.cs.loaderStatus = true;

    this.cs.post(url, str).then((res: any) => {
      const emailStatus = res.isLinkSent ? true : false;
      const mobileStatus = res.isSMSSent ? true : false;
      const checkBoth = emailStatus && mobileStatus ? true : false;
      this.paymentStatusDetails(status, smsStatus, res, email);
      let msg;
      let maskedEmail = email.replace(
        /(.{2})(.*)(?=@)/,
        function (gp1, gp2, gp3) {
          for (let i = 0; i < gp3.length; i++) {
            gp2 += "X";
          }
          return gp2;
        }
      );
      let maskedMobile =
        mobile.slice(0, 2) + mobile.slice(2).replace(/.(?=...)/g, "X");
      if (checkBoth) {
        msg = `Payment link has been sent to ${maskedEmail} and ${maskedMobile}.`;
        status = "Y";
        smsStatus = "Y";
      } else {
        if (emailStatus) {
          msg = `Payment link has been sent to ${maskedEmail}.`;
          status = "Y";
          smsStatus = "N";
        } else if (mobileStatus) {
          msg = `Payment link has been sent to ${maskedMobile}.`;
          status = "N";
          smsStatus = "Y";
        } else if (!checkBoth) {
          msg = `Failed to send payment link to ${maskedEmail} and ${maskedMobile}.`;
          status = "N";
          smsStatus = "N";
        }
      }
      swal({ text: msg }).then(() => {
        this.cs.loaderStatus = false;
      });
    });
  }

  paymentStatusDetails(emailStatus, smsStatus, res, email) {
    this.IsPayLinkSent = emailStatus;
    this.PayLinkReciever = email;
    this.isSMSSent = smsStatus;
    this.CustomerPayLink = res.paymentLink;
    this.custLink = res;

    this.updatePaymentStatuscustlink().then(() => {
      this.updatePolicyStatusCustLink();
    });

    this.cs.showProposal = false;
  }

  // Update Policy Payment Data to database
  updatePaymentStatuscustlink(): Promise<any> {
    return new Promise((resolve: any) => {
      let body;
      body = {
        policyNo: "",
        coverNoteNo: "",
        //proposalNo: this.propRes.generalInformation.proposalNumber,
        dealID: this.propReq.DealId,
        // customerId: this.propRes.generalInformation.customerId,
        pfPaymentID: "",
        paymentEntryErrorID: "",
        paymentEntryErrorText: "",
        paymentEntryStatus: "",
        paymentTagErrorID: "",
        paymentTagErrorText: "",
        paymentTagStatus: "",
        message: "",
        PaymentMode: this.PaymentMode,
        statusMessage: "",
        paymsRequestID: "",
        paymentRS: "", // here pass full response string which is coming from Mihir api
        corelationID: this.propRes.correlationId, // pass correlation from payment api response
      };


      body.proposalNo = this.propRes.generalInformation.proposalNumber;
      body.customerId = this.propRes.generalInformation.customerId;

      let str = JSON.stringify(body);
      this.cs.post("Payment/UpdatePaymentDetails", str).then((res: any) => {
        this.updatePayment = res;
        resolve();
      });
    });
  }

  updatePolicyStatusCustLink() {
    console.info("PropRes", this.propRes);
    console.info("PropReq", this.propReq);
    let SubProductType = this.instaDataForSendLink.policySubType;
    this.totalTax = Math.round(this.propRes.totalTax);
    if (this.productCode == "2320" || this.productCode == "2319") {
      this.propRes.specialDiscount = "0";
      this.propRes.finalPremium = JSON.stringify(this.propRes.finalPremium);
      this.propRes.policyType = 1;
    }

    let body;
    body = {
      DealID: this.propReq.DealId,
      PolicyType: "1", //String
      PolicySubType: SubProductType, //String
      // ProposalNumber: this.propRes.generalInformation.proposalNumber,
      PolicyNumber: "",
      PolicyStartDate: this.propReq.PolicyStartDate,
      PolicyEndDate: this.propReq.PolicyEndDate,
      Status: this.propRes.statusID,
      // BasicPremium: this.propRes.totalLiabilityPremium,
      // Discount: parseFloat(this.propRes.specialDiscount), //String
      ServiceTax: this.totalTax,
      TotalPremium: parseFloat(this.propRes.finalPremium), //String
      AddressID: "",
      Isactive: "1",
      PaymentID: JSON.stringify(this.updatePayment.paymentID),
      SalesTax: "",
      Surcharge: "",
      TotalTax: this.totalTax, //String
      // TransFor: this.propRes.generalInformation.transactionType,
      TaxRate: "",
      CustomerType: this.propReq.CustomerType,
      PreviousPolicyNumber: null,
      PFPolicyNo: "",
      PFProposalNo: "",
      PFCovernoteNo: "",
      // PFCustomerID: this.propRes.generalInformation.customerId,
      CustomerName: this.propReq.CustomerDetails.CustomerName,
      PFPaymentID: "",
      SumInsured: null,
      Planname: null,
      // PolicyTenure: this.propRes.generalInformation.tenure,
      // PolicyTPTenure: this.propRes.generalInformation.tpTenure,
      PolicyPACoverTenure: JSON.stringify(this.propReq.PACoverTenure),
      UTGSTAmount: "0",
      TotalGSTAmount: "0",
      CGSTAmountRate: null,
      SGSTAmountRate: null,
      IGSTAmountRate: null,
      UTGSTAmountRate: null,
      TOTALGSTAmountRate: null,
      TransOn: "2020-03-18",
      CreatedBy: null,
      CreatedOn: "2020-03-18",
      ModifiedBy: null,
      ModifiedOn: "2020-03-18",
      CorelationID: this.propRes.correlationId,
      IsPayLinkSent: this.IsPayLinkSent,
      PayLinkReciever: this.PayLinkReciever,
      CustomerPayLink: this.CustomerPayLink,
      smsreciver: this.custMobile,
      isSMSSent: this.isSMSSent,
    };


    body.ProposalNumber = this.propRes.generalInformation.proposalNumber;
    body.BasicPremium = this.propRes.totalLiabilityPremium;
    body.TransFor = this.propRes.generalInformation.transactionType;
    body.PFCustomerID = this.propRes.generalInformation.customerId;
    body.PolicyTenure = this.propRes.generalInformation.tenure;
    body.PolicyTPTenure = this.propRes.generalInformation.tpTenure;
    body.Discount = parseFloat(this.propRes.specialDiscount);


    if (this.propReq.Tenure == "1" && this.propReq.TPTenure == "0") {
      body.IsStandaloneOD = true;
    }

    if (
      this.paramDataValues.iPartnerLogin.isSubagent != null ||
      this.paramDataValues.iPartnerLogin.isSubagent != undefined
    ) {
      body.IsSubagent = true;
      body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
    }
    let str = JSON.stringify(body);
    this.cs.post("Policy/AddPolicyDetails", body).then((res: any) => { });
  }

  sendPaymentLinkForRenewal() {
    console.log(this.propRes, this.propReq);
    let body = {
      propNo: this.propRes.generalInformation.proposalNumber,
      policyNo: this.propReq.PolicyNumber,
      phoneNo: this.custMobile,
      emailId: this.customerEmail,
      premium: this.propRes.finalPremium,
      regNo: this.propReq.ProposalDetails.RegistrationNumber,
    };
    let str = JSON.stringify(body);
    this.cs.postForRenewals("sendPaymentLink", str).then((res: any) => {
      console.log(res);
      if (res.emailStatus) {
        swal({ text: "Payment link has been sent successfully." });
      } else {
        swal({ text: "Failed to send link." });
      }
    });
  }

  closeLink() {
    // this.paymentModeFlag = true;
    this.isSendLinkForInstaPolicy = false;
    this.watchMe = "fade";
  }

  Payamount(data) {
    localStorage.setItem("isPendingPayment", "true");
    localStorage.setItem("PendingData", JSON.stringify(data));
    let propReq, propRes;
    if (this.ProductType == "11") {
      localStorage.setItem("isAllRisk", "true");
      propReq = JSON.parse(data.proposalRQ); //allrisk
      propRes = JSON.parse(data.proposalRS); //allrisk

      propReq.id = data.id;  //allrisk
      propReq.policyID = data.policyID;  //allrisk
    } else {
      propReq = JSON.parse(data.proposalRQ);
      propRes = JSON.parse(data.proposalRS);

      propReq.id = data.id;
      propReq.policyID = data.policyID;
    }


    this.messageService.sendproposalreqData(propReq);
    this.messageService.sendproposalresData(propRes);
    localStorage.setItem("DisablecustLink", "true");
    // if(data.paymentMode != "Insta"){

    // //CreateToken + GetApplicationAccess merging code - Sejal
    if (!this.cs.isUndefineORNull(localStorage.getItem("AuthToken"))) {
      let encryptedTokenaccess = JSON.parse(localStorage.getItem("AuthToken"));
      let Parsedtokenaccess: any = this.api.decrypt(
        encryptedTokenaccess.applicationAccess
      );
      let tokenaccess = JSON.parse(Parsedtokenaccess);
      // this.cs.isPlutusEnable = tokenaccess.isEnablePlutus;
      this.cs.isPlutusEnable = true;
      if (
        this.cs.isPlutusEnable == true ||
        this.IsPOSTransaction_flag == true
      ) {
        this.cs.redirectToPlutus(propReq, propRes, 0, 0, false, 0);
      } else {
        // this.router.navigateByUrl("payment");
      }
    }

    // this.cs.get1("Access/GetApplicationAccess").then((resp: any) => {
    //   this.cs.isPlutusEnable = resp.isEnablePlutus;
    //   if (resp.isEnablePlutus == true || this.IsPOSTransaction_flag == true) {
    //     this.cs.redirectToPlutus(propReq, propRes, 0, false, 0);
    //   } else {
    //     this.router.navigateByUrl("payment");
    //   }
    // });
  }

  checkSendLinkData(data) {
    this.customerEmail = "";
    this.custMobile = "";

    $("#sendLinkModal1").appendTo("body");
    $("#sendLinkModal1").modal("show");

    // if (this.ProductType == "11") {
    //   this.customerEmail = data.allRisk.emailID;
    //   this.custMobile = data.allRisk.mobileNo;
    // } else {
    this.customerEmail = data.emailID;
    this.custMobile = data.mobileNo;
    // }

    this.resendData = data;
  }

  sendLink() {
    let regemailtest =
      /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(
        this.customerEmail
      );
    if (this.ProductType == "11") {
      if ( //allrisk
        this.resendData.emailID == "" ||
        this.resendData.emailID == undefined ||
        this.resendData.emailID == null ||
        !regemailtest
      ) {
        this.cs.loaderStatus = false;
        swal({
          closeOnClickOutside: false,
          text: "Insert valid Email Id",
        }).then(() => {
          $("#sendLinkModal1").modal("show");
        });
      } else if (//allrisk
        this.resendData.mobileNo == "" ||
        this.resendData.mobileNo == undefined ||
        this.resendData.mobileNo == null
      ) {
        this.cs.loaderStatus = false;
        swal({
          closeOnClickOutside: false,
          text: "Insert Mobile Number",
        }).then(() => {
          $("#sendLinkModal1").modal("show");
        });
      } else {
        this.resend1(this.resendData, this.customerEmail, this.custMobile);
      }
    } else {
      if (
        this.resendData.emailID == "" ||
        this.resendData.emailID == undefined ||
        this.resendData.emailID == null ||
        !regemailtest
      ) {
        this.cs.loaderStatus = false;
        swal({
          closeOnClickOutside: false,
          text: "Insert valid Email Id",
        }).then(() => {
          $("#sendLinkModal1").modal("show");
        });
      } else if (
        this.resendData.mobileNo == "" ||
        this.resendData.mobileNo == undefined ||
        this.resendData.mobileNo == null
      ) {
        this.cs.loaderStatus = false;
        swal({
          closeOnClickOutside: false,
          text: "Insert Mobile Number",
        }).then(() => {
          $("#sendLinkModal1").modal("show");
        });
      } else {
        this.resend(this.resendData, this.customerEmail, this.custMobile);
      }
    }

  }

  closeModal(modalName) {
    $("#" + modalName).modal("hide");
  }

  resend(data, email, mobile) {
    // let mobile = btoa(JSON.parse(data.proposalRQ).CustomerDetails.MobileNumber);
    // let email = btoa(data.emailID);
    this.cs.loaderStatus = true;
    // let mobile = data.mobileNo;
    // let email = data.emailID;
    let body = {
      CorrelationID: data.corelationID,
      EmailID: this.cs.encrypt(email, commonData.aesnysaKey),
      MobileNo: this.cs.encrypt(mobile, commonData.aesnysaKey),
      ProposalNo: data.policyNo,
      IsResentLink: true,
    };
    let str = JSON.stringify(body);
    this.cs.post("CustomerPayment/getLink", str).then((res: any) => {
      const emailStatus = res.isLinkSent ? true : false;
      const mobileStatus = res.isSMSSent ? true : false;
      const checkBoth = emailStatus && mobileStatus ? true : false;
      let msg = "Something went wrong, please try again later.";
      let maskedEmail = email.replace(
        /(.{2})(.*)(?=@)/,
        function (gp1, gp2, gp3) {
          for (let i = 0; i < gp3.length; i++) {
            gp2 += "X";
          }
          return gp2;
        }
      );
      let maskedMobile =
        mobile.slice(0, 2) + mobile.slice(2).replace(/.(?=...)/g, "X");
      if (checkBoth) {
        msg = `Payment link has been re-sent to ${maskedEmail} and ${maskedMobile}.`;
      } else {
        if (emailStatus) {
          msg = `Payment link has been re-sent to ${maskedEmail}.`;
        } else if (mobileStatus) {
          msg = `Payment link has been re-sent to ${maskedMobile}.`;
        } else if (!checkBoth) {
          msg = `Failed to re-send the payment link to ${maskedEmail} and ${maskedMobile}.`;
        }
      }

      swal({ text: msg }).then(() => {
        this.getpolicies();
      });
      this.cs.loaderStatus = false;
      // if(res.isLinkSent == true ){
      //   swal({  text: 'Link has been sent successfully' })
      // } else if(res.paymentLink != "" && res.paymentLink != null) {
      //   swal({
      //   text: 'Link was not sent to mentioned email id. Please share below customer payment link: ' + '\n'  + res.paymentLink })
      // }
    });
  }

  resend1(data, email, mobile) {
    // let mobile = btoa(JSON.parse(data.proposalRQ).CustomerDetails.MobileNumber);
    // let email = btoa(data.emailID);
    this.cs.loaderStatus = true;
    // let mobile = data.mobileNo;
    // let email = data.emailID;
    let body = {
      CorrelationID: data.corelationID,  //allrisk
      EmailID: this.cs.encrypt(email, commonData.aesnysaKey),
      MobileNo: this.cs.encrypt(mobile, commonData.aesnysaKey),
      ProposalNo: data.policyNo,   //allrisk
      IsResentLink: true,
      BundleID: data.bundleId,
    };
    let str = JSON.stringify(body);
    this.cs.post("CustomerPayment/getLink", str).then((res: any) => {
      const emailStatus = res.isLinkSent ? true : false;
      const mobileStatus = res.isSMSSent ? true : false;
      const checkBoth = emailStatus && mobileStatus ? true : false;
      let msg = "Something went wrong, please try again later.";
      let maskedEmail = email.replace(
        /(.{2})(.*)(?=@)/,
        function (gp1, gp2, gp3) {
          for (let i = 0; i < gp3.length; i++) {
            gp2 += "X";
          }
          return gp2;
        }
      );
      let maskedMobile =
        mobile.slice(0, 2) + mobile.slice(2).replace(/.(?=...)/g, "X");
      if (checkBoth) {
        msg = `Payment link has been re-sent to ${maskedEmail} and ${maskedMobile}.`;
      } else {
        if (emailStatus) {
          msg = `Payment link has been re-sent to ${maskedEmail}.`;
        } else if (mobileStatus) {
          msg = `Payment link has been re-sent to ${maskedMobile}.`;
        } else if (!checkBoth) {
          msg = `Failed to re-send the payment link to ${maskedEmail} and ${maskedMobile}.`;
        }
      }

      swal({ text: msg }).then(() => {
        this.getpolicies();
      });
      this.cs.loaderStatus = false;
      // if(res.isLinkSent == true ){
      //   swal({  text: 'Link has been sent successfully' })
      // } else if(res.paymentLink != "" && res.paymentLink != null) {
      //   swal({
      //   text: 'Link was not sent to mentioned email id. Please share below customer payment link: ' + '\n'  + res.paymentLink })
      // }
    });
  }

  recalculate(data, ev: any) {
    let URL, parsedrequest;

    this.pendingproData = data;

    // if ( data.policySubType == "11" || this.ProductType == "11") {
    //   this.loaderByPolicy = data.allRisk.policyNo;
    //   parsedrequest = JSON.parse(data.allRisk.proposalRQ);
    // } else {
    this.loaderByPolicy = data.policyNo;
    parsedrequest = JSON.parse(data.proposalRQ);
    // }
    let policyStartDate = moment(new Date()).format("YYYY-MM-DD");
    if (
      (data.policySubType == "1" ||
        this.ProductType == "1" ||
        data.policySubType == "9" ||
        this.ProductType == "9") &&
      parsedrequest.BusinessType == "New Business"
    ) {
      this.policyEndDate = moment(policyStartDate)
        .add(5, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    } else if (
      (data.policySubType == "2" ||
        this.ProductType == "2" ||
        data.policySubType == "10" ||
        this.ProductType == "10") &&
      parsedrequest.BusinessType == "New Business"
    ) {
      this.policyEndDate = moment(policyStartDate)
        .add(3, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    } else {
      this.policyEndDate = moment(policyStartDate)
        .add(1, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    }
    if (
      data.policySubType == "2" ||
      data.policySubType == "1" ||
      this.ProductType == "1" ||
      this.ProductType == "2" ||
      data.policySubType == "9" ||
      this.ProductType == "9" ||
      data.policySubType == "10" ||
      this.ProductType == "10"
    ) {
      this.proposalRQ = JSON.parse(data.proposalRQ);
    }

    if (data.policySubType == "11" || this.ProductType == "11") {
      if (data.scpa != null) {
        this.proposalRQ = JSON.parse(data.proposalRQ);
        this.scpaRQ = JSON.parse(data.scpa.proposalRQ);
        this.scpaRQ.PolicyStartDate = policyStartDate;
        this.scpaRQ.PolicyEndDate = this.policyEndDate;
        localStorage.setItem("scpaPropDatareq", this.scpaRQ);
        localStorage.setItem("IsSCPA", "true");
      } else {
        this.proposalRQ = JSON.parse(data.proposalRQ);  //allrisk
        localStorage.setItem("IsSCPA", "false");
      }

    }
    // else if(data.policySubType == "12" || this.ProductType == "12" || data.policySubType == "13" || this.ProductType == "13"){
    if (data.isStandaloneOD === true) {
      let obj1 = JSON.parse(data.proposalRQ);
      let obj2 = JSON.parse(data.standAloneODRQ);
      this.proposalRQ = Object.assign(obj1, obj2);
    }

    if (data.policySubType != "11" && this.ProductType != "11") {
      if (this.cs.isUndefineORNull(parsedrequest.IsPACoverWaiver)) {
        this.proposalRQ.IsPACoverWaiver = "false";
      } else {
        this.proposalRQ.IsPACoverWaiver = parsedrequest.IsPACoverWaiver;
      }

      this.proposalRQ.OldReferenceNo = data.policyNo;
    } else {
      this.proposalRQ.OldReferenceNo = data.policyNo;
    }

    if (this.cs.isUndefineORNull(parsedrequest.softCopyFlag)) {
      this.proposalRQ.softCopyFlag = "No";
    } else {
      this.proposalRQ.softCopyFlag = parsedrequest.softCopyFlag;
    }

    this.proposalRQ.OldReferenceNo = data.policyNo;
    this.proposalRQ.PolicyStartDate = policyStartDate;
    this.proposalRQ.PolicyEndDate = this.policyEndDate;
    if (
      data.policySubType == "1" ||
      data.policySubType == "2" ||
      data.policySubType == "12" ||
      data.policySubType == "13"
    )
      if (
        this.proposalRQ.BusinessType == "Roll Over" &&
        this.proposalRQ.PreviousPolicyDetails == null
      ) {
        this.proposalRQ.isNoPrevInsurance = true;
      }
    if (
      data.policySubType == "2" ||
      data.policySubType == "13" ||
      this.ProductType == "2" ||
      this.ProductType == "13"
    ) {
      if (this.proposalRQ.IsEMIProtect == false) {
        this.proposalRQ.EMIAmount = "0";
      }
    }

    this.proposalReq = this.proposalRQ;
    if (
      data.policySubType == "1" ||
      data.policySubType == "12" ||
      this.ProductType == "1" ||
      this.ProductType == "12"
    ) {
      URL = "/proposal/twowheelercreateproposal";
    }
    if (
      data.policySubType == "2" ||
      data.policySubType == "13" ||
      this.ProductType == "2" ||
      this.ProductType == "13"
    ) {
      if (this.cs.isUndefineORNull(this.proposalRQ.OtherDiscount)) {
        this.proposalRQ.OtherDiscount = "0";
      } else {
        this.proposalRQ.OtherDiscount = this.proposalRQ.OtherDiscount;
      }
      URL = "/proposal/privatecarcreateproposal";
    }
    if (data.policySubType == "9" || this.ProductType == "9") {
      URL = "/proposal/twowheelertp";
    }
    if (data.policySubType == "10" || this.ProductType == "10") {
      URL = "/proposal/pvtcartp";
    }
    if (data.policysubType == "11" || this.ProductType == "11") {
      URL = "/zerotat/Proposal/Create";
    }

    if (data.policysubType == "11" || this.ProductType == "11") {
      this.bundleID = data.bundleId;
      console.info(this.proposalRQ);
      this.createAllRiskBizToken().then(() => {
        this.spinloaderrecal = true;
        this.cs.allRiskEBpostBiz(URL, this.proposalRQ).subscribe((res: any) => {
          this.proposalRes = res;
          if (res.status == "Success" || res.status == "SUCCESS") {
            this.saveofflineProposalAllRisk().then(() => {
              if (this.cs.isUndefineORNull(data.scpa)) {
                this.getpolicies();
              }
            });
            this.spinloaderrecal = false;

            if (!this.cs.isUndefineORNull(data.scpa)) {
              this.scpaRQ = JSON.parse(data.scpa.proposalRQ);
              this.scpaRQ.OldReferenceNo = data.scpa.policyNo;
              this.scpaRQ.PolicyStartDate = policyStartDate;
              this.scpaRQ.PolicyEndDate = this.policyEndDate;
              this.cs.allRiskEBpostBiz(URL, this.scpaRQ).subscribe((res: any) => {
                this.spinloaderrecal = true;
                this.scpaRS = res;
                if (res.status == "Success" || res.status == "SUCCESS") {
                  this.scpaRQ = JSON.parse(data.scpa.proposalRQ);
                  this.scpaRQ.OldReferenceNo = data.scpa.policyNo;
                  this.scpaRQ.PolicyStartDate = policyStartDate;
                  this.scpaRQ.PolicyEndDate = this.policyEndDate;
                  this.scpaRS = res;
                  this.saveofflineProposalSCPA().then(() => {
                    this.getpolicies();
                  });
                  this.spinloaderrecal = false;
                }
                else {
                  this.spinloaderrecal = false;
                  swal({ text: "Cannot modify the proposal1. Reason: " + res.message });
                  let errormsg = res.message;
                  // this.api.handleerrors(errormsg);
                  let errMsg = this.msgService.errorMsg(errormsg)
                  swal({ closeOnClickOutside: false, text: errMsg })
                  let errorbody = {
                    RequestJson: JSON.stringify(this.proposalRQ),
                    ResponseJson: JSON.stringify(res),
                    ServiceURL: URL,
                    CorrelationID: data.scpa.proposalRQ.correlationId,
                  };
                  this.api.adderrorlogs(errorbody);
                }
              });
            }
          } else {
            this.spinloaderrecal = false;
            swal({ text: "Cannot modify the proposal. Reason: " + res.message });
            let errormsg = res.message;
            this.api.handleerrors(errormsg);
            let errorbody = {
              RequestJson: JSON.stringify(this.proposalRQ),
              ResponseJson: JSON.stringify(res),
              ServiceURL: URL,
              CorrelationID: data.proposalRQ.correlationId, //allrisk
            };
            this.api.adderrorlogs(errorbody);
          }
        });
      });
    } else {
      this.spinloaderrecal = true;
      this.cs.postBiz(URL, this.proposalRQ).subscribe((res: any) => {
        this.proposalRes = res;
        if (res.status == "Success") {
          if (
            data.policySubType == "9" ||
            this.ProductType == "9" ||
            data.policySubType == "10" ||
            this.ProductType == "10"
          ) {
            this.offlineSaveProposal().then(() => {
              this.getpolicies();
            });
          } else {
            this.saveofflineProposal().then(() => {
              this.getpolicies();
            });
          }

          this.spinloaderrecal = false;
        } else {
          this.spinloaderrecal = false;
          swal({ text: "Cannot modify the proposal. Reason: " + res.message });
          let errormsg = res.message;
          this.api.handleerrors(errormsg);
          let errorbody;
          errorbody = {
            RequestJson: JSON.stringify(this.proposalRQ),
            ResponseJson: JSON.stringify(res),
            ServiceURL: URL,
            CorrelationID: data.proposalRQ.correlationId,
          };
          this.api.adderrorlogs(errorbody);
        }
      });
    }
  }

  createAllRiskBizToken(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.get("middleware/token?Scope=14").subscribe((res: any) => {
        localStorage.setItem("allRiskbizToken", JSON.stringify(res));
        resolve();
      });
    }).catch((err: any) => {
      swal({ text: err });
      this.cs.loaderStatus = false;
    });
  }

  offlineSaveProposal(): Promise<any> {
    return new Promise((resolve: any) => {
      let body;
      body = {
        STATUS: 2,
        PROPOSAL_NUMBER: this.proposalRes.generalInformation.proposalNumber,
        BASIC_PREMIUM: "0",
        SERVICE_TAX: "0",
        bankType: this.pendingproData.bankType,
        TOTAL_PREMIUM: JSON.stringify(this.proposalRes.finalPremium),
        TOTAL_TAX: JSON.stringify(this.proposalRes.totalTax),
        PACKAGE_PREMIUM: "0",
        START_DATE: this.proposalReq.PolicyStartDate,
        END_DATE: this.proposalReq.PolicyEndDate,
        OldReferenceNo: this.proposalRQ.OldReferenceNo,
        TOTAL_LIABILITY_PREMIUM: JSON.stringify(
          this.proposalRes.totalLiabilityPremium
        ),
        MODIFIED_BY: "",
        CUSTOMER_ID: this.proposalRes.generalInformation.customerId,
        CORELATION_ID: this.pendingproData.corelationID,
        PROPOSAL_RS: JSON.stringify(this.proposalRes),
        PROPOSAL_RQ: JSON.stringify(this.proposalReq),
        QUOTE_ID: "",
        CustDetails: {
          CustomerName: this.proposalReq.CustomerDetails.CustomerName,
          CustomerType: this.proposalReq.CustomerDetails.CustomerType, //string
          DateOfBirth: "01/01/1999", //propReq.CustomerDetails.DateOfBirth,//string
          PinCode: this.proposalReq.CustomerDetails.PinCode, //string
          PANCardNo: this.proposalReq.CustomerDetails.PANCardNo, //string
          Email: this.proposalReq.CustomerDetails.Email, //string
          MobileNumber: this.proposalReq.CustomerDetails.MobileNumber, //string
          AddressLine1: this.proposalReq.CustomerDetails.AddressLine1, //string
          CountryCode: this.proposalReq.CustomerDetails.CountryCode, //string
          StateCode: this.proposalReq.CustomerDetails.StateCode, //string
          CityCode: this.proposalReq.CustomerDetails.CityCode, //string
          MobileISD: "91", //string
          Gender: this.proposalReq.CustomerDetails.Gender, //string
          AadharNumber: this.proposalReq.CustomerDetails.AadharNumber, //string
          GSTDetails: this.proposalReq.CustomerDetails.GSTDetails,
        },
      };

      if (
        this.paramDataValues.iPartnerLogin.isSubagent != null ||
        this.paramDataValues.iPartnerLogin.isSubagent != undefined
      ) {
        body.IsSubagent = true;
        body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
      }

      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs
        .post("proposal/offlinesave/TP", str)
        .then((res: any) => {
          // this.saveFlagTP = res.isRecordInserted;
          resolve();
          this.cs.loaderStatus = false;
        })
        .catch((err: any) => {
          swal({
            text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
          });
        });
    });
  }

  saveofflineProposal(): Promise<any> {
    return new Promise((resolve: any) => {
      let body, URL;
      body = {
        OdIasID: "",
        OdIasSrt: "",
        OdIasSubSrt: "",
        IdvIasID: "",
        IdvIasSrt: "",
        IdvIasSubSrt: "",
        proposalRQ: this.proposalReq,
        proposalRS: this.proposalRes,
        bankType: this.pendingproData.bankType,
        IsSubagent: this.pendingproData.isSubAgent,
        SubagentIpartnerUserID: this.pendingproData.subagentIpartnerUserID,
      };

      // if (this.ProductType == "11" && this.scpaRQ != null) {
      //   body.proposalRQ = this.scpaRQ;
      //   body.proposalRS = this.scpaRS;
      //   body.BundleID = this.bundleID;
      // } else {
      //   body.proposalRQ = this.proposalReq;
      //   body.proposalRS = this.proposalRes;
      //   body.BundleID = this.bundleID;
      // }

      if (
        this.pendingproData.policySubType == "1" ||
        this.pendingproData.policySubType == "12" ||
        this.ProductType == "1" ||
        this.ProductType == "12"
      ) {
        URL = "Proposal/tw/SaveProposal";
      }
      if (
        this.pendingproData.policySubType == "2" ||
        this.pendingproData.policySubType == "13" ||
        this.ProductType == "2" ||
        this.ProductType == "13"
      ) {
        URL = "Proposal/fw/SaveProposal";
      }
      // if (
      //   this.pendingproData.policySubType == "11" ||
      //   this.ProductType == "11" 
      // ) {
      //   URL = "Proposal/twev/SaveProposal";
      // }
      let str = JSON.stringify(body);
      this.cs.post(URL, str).then((res: any) => {
        resolve();
      });
    });
  }

  saveofflineProposalAllRisk(): Promise<any> {
    return new Promise((resolve: any) => {
      let body, URL;
      body = {
        OdIasID: "",
        OdIasSrt: "",
        OdIasSubSrt: "",
        IdvIasID: "",
        IdvIasSrt: "",
        IdvIasSubSrt: "",
        proposalRQ: this.proposalReq,
        proposalRS: this.proposalRes,
        BundleID: this.bundleID,
        bankType: this.pendingproData.bankType, //allrisk
        IsSubagent: this.pendingproData.isSubAgent, //allrisk
        SubagentIpartnerUserID: this.pendingproData.subagentIpartnerUserID,  //allrisk
      };

      URL = "Proposal/twev/SaveProposal";
      let str = JSON.stringify(body);
      this.cs.post(URL, str).then((res: any) => {
        resolve();
      });
    });
  }

  saveofflineProposalSCPA(): Promise<any> {
    return new Promise((resolve: any) => {
      let body, URL;
      body = {
        OdIasID: "",
        OdIasSrt: "",
        OdIasSubSrt: "",
        IdvIasID: "",
        IdvIasSrt: "",
        IdvIasSubSrt: "",
        proposalRQ: this.scpaRQ,
        proposalRS: this.scpaRS,
        BundleID: this.bundleID,
        bankType: this.pendingproData.scpa.bankType,
        IsSubagent: this.pendingproData.scpa.isSubAgent,
        SubagentIpartnerUserID: this.pendingproData.scpa.subagentIpartnerUserID,
      };

      URL = "Proposal/twev/SaveProposal";
      let str = JSON.stringify(body);
      this.cs.post(URL, str).then((res: any) => {
        resolve();
      });
    });
  }

  gotoMultiPayment(isAdvPID: any) {
    // //CreateToken + GetApplicationAccess merging code - Sejal
    if (!this.cs.isUndefineORNull(localStorage.getItem("AuthToken"))) {
      let encryptedTokenaccess = JSON.parse(localStorage.getItem("AuthToken"));
      let Parsedtokenaccess: any = this.api.decrypt(
        encryptedTokenaccess.applicationAccess
      );
      let tokenaccess = JSON.parse(Parsedtokenaccess);
      // this.cs.isPlutusEnable = tokenaccess.isEnablePlutus;
      this.cs.isPlutusEnable = true;
      if (
        this.cs.isPlutusEnable == true ||
        this.IsPOSTransaction_flag == true
      ) {
        if (isAdvPID) {
          if (this.instatype) {
            this.cs.redirectToPlutus(null, null, 1, 1, true, isAdvPID);
          } else {
            this.cs.redirectToPlutus(null, null, 0, 0, true, isAdvPID);
          }
        } else {
          if (this.instatype) {
            this.cs.redirectToPlutus(null, null, 1, 1, true, isAdvPID);
          } else {
            this.cs.redirectToPlutus(null, null, 0, 0, true, isAdvPID);
          }
        }
      } else {
        // this.router.navigateByUrl('multipayment',);
        let flag = btoa(this.instatype);
        this.router.navigate(["multipayment"], {
          queryParams: { isInsta: flag },
        });
      }
    }

    // this.cs.get1("Access/GetApplicationAccess").then((resp: any) => {
    //   this.cs.isPlutusEnable = resp.isEnablePlutus;
    //   if (resp.isEnablePlutus == true || this.IsPOSTransaction_flag == true) {
    //     if (isAdvPID) {
    //       if (this.instatype) {
    //         this.cs.redirectToPlutus(null, null, 1, true, isAdvPID);
    //       } else {
    //         this.cs.redirectToPlutus(null, null, 0, true, isAdvPID);
    //       }
    //     } else {
    //       if (this.instatype) {
    //         this.cs.redirectToPlutus(null, null, 1, true, isAdvPID);
    //       } else {
    //         this.cs.redirectToPlutus(null, null, 0, true, isAdvPID);
    //       }
    //     }
    //   } else {
    //     // this.router.navigateByUrl('multipayment',);
    //     let flag = btoa(this.instatype);
    //     this.router.navigate(["multipayment"], {
    //       queryParams: { isInsta: flag },
    //     });
    //   }
    // });
  }

  // Get Deal
  getDeal(): Promise<void> {
    return new Promise((resolve) => {
      this.authtok = JSON.parse(localStorage.getItem("AuthToken"));
      this.api
        .getDeal(this.authtok.username, this.productCode)
        .subscribe((res: any) => {
          this.dealid = res.dealID;
          localStorage.setItem("DealID", JSON.stringify(this.dealid));
          resolve();
        });
    });
  }

  validate() {
    /**
     * digitalPOS Change
     * call getDeal function only for nysa not for POS
     * But Calling getPayAuth for both nysa and pos
     * date :- 29-07-2021
     */
    if (this.IsPOSTransaction_flag == true) {
      this.cs.getPayAuth().then((resp: any) => {
        this.payButton = true;
        localStorage.setItem("PayAuthToken", JSON.stringify(resp.accessToken));
      });
    } else {
      // GetDealFromIM? changes - Sejal - 21-01-2022
      if (this.cs.isUndefineORNull(JSON.parse(localStorage.getItem("DealID")))) {
        this.getDeal().then(() => {
          this.cs.getPayAuth().then((resp: any) => {
            this.payButton = true;
            localStorage.setItem(
              "PayAuthToken",
              JSON.stringify(resp.accessToken)
            );
          });
        });
      } else {
        this.cs.getPayAuth().then((resp: any) => {
          this.payButton = true;
          localStorage.setItem(
            "PayAuthToken",
            JSON.stringify(resp.accessToken)
          );
        });
      }
      // this.getDeal().then(() => {
      //   this.cs.getPayAuth().then((resp: any) => {
      //     this.payButton = true;
      //     localStorage.setItem(
      //       "PayAuthToken",
      //       JSON.stringify(resp.accessToken)
      //     );
      //   });
      // });
    }
    if (
      this.pendingpayment.FromDate == null ||
      this.pendingpayment.FromDate == undefined ||
      this.pendingpayment.FromDate == ""
    ) {
      swal({ title: "Alert!", text: "Kindly insert from date" });
    } else if (
      this.pendingpayment.ToDate == null ||
      this.pendingpayment.ToDate == undefined ||
      this.pendingpayment.ToDate == ""
    ) {
      swal({ title: "Alert!", text: "Kindly insert to date" });
    } else {
      this.getpolicies();
    }
  }

  vehicletype(ev: any) {
    this.showbutton = false;
    if (ev.target.id == "penselectVehicle_all") {
      this.ProductType = "0";
      this.nysaPolicy = "";
    } else if (ev.target.id == "penselectVehicle_twoWheeler") {
      this.ProductType = "1";
      this.nysaPolicy = "";
      this.productCode = "2312";
    } else if (ev.target.id == "penselectVehicle_fourWheeler") {
      this.ProductType = "2";
      this.nysaPolicy = "";
      this.productCode = "2311";
    } else if (ev.target.id == "penselectVehicle_twoWheelerTP") {
      this.ProductType = "9";
      this.nysaPolicy = "";
      this.productCode = "2320";
    } else if (ev.target.id == "penselectVehicle_fourWheelerTP") {
      this.ProductType = "10";
      this.nysaPolicy = "";
      this.productCode = "2319";
    } else if (ev.target.id == "penselectVehicle_twoWheelerOD") {
      this.ProductType = "12";
      this.nysaPolicy = "";
      this.productCode = "2312";
    } else if (ev.target.id == "penselectVehicle_fourWheelerOD") {
      this.ProductType = "13";
      this.nysaPolicy = "";
      this.productCode = "2311";
    } else if (ev.target.id == "penselectVehicle_allriskElectricBike") {
      this.ProductType = "11";   // All Risk Electric Bike - Sejal - 5-4-2022
      this.nysaPolicy = "";
      this.productCode = commonData.allriskProductCode;
    }
  }

  redirectTo() {
    this.cs.goTopending();
  }

  insta(ev: any) {
    this.instatype = JSON.parse(ev.target.value);
    this.nysaPolicy = "";
  }

  // getPolicies
  getpolicies() {
    let from: any;
    let to: any;
    from = moment(this.pendingpayment.FromDate);
    to = moment(this.pendingpayment.ToDate);
    let diff = to.diff(from, "days");
    this.diffDays = diff;
    this.showcheckButton = false;
    this.showbutton = false;
    if (diff < "0") {
      swal({ title: "Alert!", text: "To Date cannot be less than from date" });
    } else {
      if (this.refreshflag == false) {
        this.spinloaderpolicy = true;
      }
      // this.cs.loaderStatus = true;
      let body;
      body = {
        FromDate: moment(this.pendingpayment.FromDate).format("MM/DD/YYYY"), //"4/12/2020",
        ToDate: moment(this.pendingpayment.ToDate)
          .add(1, "days")
          .format("MM/DD/YYYY"), // "4/14/2020",
        ProductType: "1",
        SubProductType: this.ProductType,
        IsInstaPayment: this.instatype,
      };
      if (
        this.paramDataValues.iPartnerLogin.isSubagent != null ||
        this.paramDataValues.iPartnerLogin.isSubagent != undefined
      ) {
        body.IsSubagent = true;
        body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
      }
      let str = JSON.stringify(body);
      this.cs.post("Payment/PendingPayment", str).then((res: any) => {
        this.multitotalPremium = [];
        this.proposalData = [];
        this.singleData = [];
        this.showcheckButton = false;
        if (res.status == null) {
          this.norecordflag = true;
        } else {
          this.norecordflag = false;
          // if (!this.cs.isUndefineORNull(res.allRiskDetials)){
          //   this.isAllRisk = true;
          //   console.log("All Risk", res.allRiskDetials);
          //   this.nysaPolicy = res.allRiskDetials;
          //   this.nysapolicyCount = res.allRiskDetials.length;
          // } else {
          //   this.isAllRisk = false;
          this.nysaPolicy = res.policyDetails;
          this.nysapolicyCount = res.policyDetails.length;
          // }

        }
        this.spinloaderpolicy = false;
        for (let i = 0; i < this.nysaPolicy.length; i++) {

          if (this.nysaPolicy[i].isAllRisk == true && !this.cs.isUndefineORNull(this.nysaPolicy[i].bundleId)) {
            this.isAllRisk = true;
          } else {
            this.isAllRisk = false;
          }

          let currentDate;
          currentDate = moment().format("YYYY-MM-DD");
          let policystartdate;
          //console.info(this.nysaPolicy[i], this.nysaPolicy[i].allRisk.policyStartDate);
          // if (this.ProductType == "11" && this.isAllRisk == true) {
          //   let date1 = moment(
          //     this.nysaPolicy[i].allRisk.policyStartDate,
          //     "DD/MM/YYYY"
          //   ).format("YYYY-MM-DD");
          //   policystartdate = moment(currentDate).diff(date1, "days");
          // } else {
          let date1 = moment(
            this.nysaPolicy[i].policyStartDate,
            "DD/MM/YYYY"
          ).format("YYYY-MM-DD");
          policystartdate = moment(currentDate).diff(date1, "days");
          // }
          if (
            policystartdate <= 0 &&
            this.nysaPolicy[i].emailID != "" &&
            this.nysaPolicy[i].emailID != null
          ) {
            this.nysaPolicy[i].isResendLink = true;
          } else {
            this.nysaPolicy[i].isResendLink = false;
          }
          if (policystartdate <= 0) {
            this.nysaPolicy[i].isPayButtonVis = true;
          } else {
            this.nysaPolicy[i].isPayButtonVis = false;
          }

          if (policystartdate <= 0) {
            this.nysaPolicy[i].showRecal = false;
          } else {
            this.nysaPolicy[i].showRecal = true;
          }
        }
      });
    }
  }

  // Go to Dashboard
  goToDashboard() {
    this.cs.geToDashboard();
  }

  // Check Banking ID
  checkBankingid(): Promise<any> {
    this.dealID = JSON.parse(localStorage.getItem("DealID"));
    return new Promise((resolve: any) => {
      let body = { DealID: this.dealID };
      let str = JSON.stringify(body);
      this.cs
        .post("Payment/FetchDealStatusDetails", str)
        .then((res: any) => {
          if (res.status == "SUCCESS") {
            this.bankingidData = res;
            resolve();
          } else {
            resolve();
            swal({
              title: "Alert!",
              text: "You cannot go further as no data found for your deal id",
            }).then(() => {
              this.goToDashboard();
            });
          }
        })
        .catch((err: any) => {
          resolve();
          swal({ title: "Alert!", text: err });
        });
    });
  }

  getData(ProposalNo, CustomerID, totalPremium, ev: any, singledata) {
    console.log(singledata);
    this.isSendLinkForInstaPolicy = false;
    let checked = ev.target.checked;
    let scpafinalamount, finalamount;
    if (checked) {
      if ((this.ProductType == "11" || this.ProductType == "0") && !this.cs.isUndefineORNull(singledata.scpa)) {
        this.scpaProposalNo = singledata.scpa.policyNo;
        scpafinalamount = singledata.scpa.totalPremium;
        this.singleData.push(singledata);
        this.proposalData.push({
          CustomerID: CustomerID,
          ProposalNo: ProposalNo,
        });

        this.proposalData.push({
          CustomerID: CustomerID,
          ProposalNo: this.scpaProposalNo,
        });

        this.multitotalPremium.push({ totalPremium: JSON.parse(totalPremium) });
        this.multitotalPremium.push({ totalPremium: JSON.parse(scpafinalamount) });

        if (this.multitotalPremium.length >= 2) {
          this.showbutton = true;
        } else {
          this.showbutton = false;
        }
        this.totalsum = this.multitotalPremium
          .map((a) => a.totalPremium)
          .reduce(function (a, b) {
            return a + b;
          });

        // if (finalamount == null) {
        //   finalamount = totalPremium + scpafinalamount;
        // }
        localStorage.setItem("allriskmultipleProposal", JSON.stringify(this.proposalData));
        localStorage.setItem("allriskmultiTotalAmount", JSON.stringify(this.totalsum));
      } else {
        this.singleData.push(singledata);
        this.proposalData.push({
          CustomerID: CustomerID,
          ProposalNo: ProposalNo,
        });

        this.multitotalPremium.push({ totalPremium: JSON.parse(totalPremium) });

        if (this.multitotalPremium.length >= 2) {
          this.showbutton = true;
        } else {
          this.showbutton = false;
        }
        this.totalsum = this.multitotalPremium
          .map((a) => a.totalPremium)
          .reduce(function (a, b) {
            return a + b;
          });
      }

    } else {
      let isSCPA = localStorage.getItem("IsSCPA");
      if ((this.ProductType == "11" || this.ProductType == "0") &&
        this.proposalData.length == 2 &&
        isSCPA == "true"
      ) {
        let index = this.proposalData.findIndex(
          (list) => list.ProposalNo == ProposalNo
        );
        this.proposalData.splice(index, 2);
        let index1 = this.multitotalPremium.findIndex(
          (list) => list.totalPremium == totalPremium
        );
        this.multitotalPremium.splice(index1, 2);
        let index2 = this.singleData.findIndex(
          (list) => list.totalPremium == totalPremium
        );
        this.singleData.splice(index2, 2);
        if (this.multitotalPremium.length > 0) {
          this.totalsum = this.multitotalPremium
            .map((a) => a.totalPremium)
            .reduce(function (a, b) {
              return a + b;
            });
        } else {
          this.totalsum = 0;
        }
      } else if ((this.ProductType == "11" || this.ProductType == "0") &&
        this.proposalData.length > 2
      ) {
        let index = this.proposalData.findIndex(
          (list) => list.ProposalNo == ProposalNo
        );
        console.log(this.proposalData, this.proposalData[index].CustomerID);
        let dataLength = this.proposalData.filter(obj => obj.CustomerID === this.proposalData[index].CustomerID).length;
        console.log(dataLength);
        this.proposalData.splice(index, dataLength);
        console.log(this.proposalData);
        console.log(totalPremium);
        let index1 = this.multitotalPremium.findIndex(
          (list) => list.totalPremium == totalPremium
        );
        this.multitotalPremium.splice(index, dataLength);
        let index2 = this.singleData.findIndex(
          (list) => list.totalPremium == totalPremium
        );
        this.singleData.splice(index2, 1);
        if (this.multitotalPremium.length > 0) {
          this.totalsum = this.multitotalPremium
            .map((a) => a.totalPremium)
            .reduce(function (a, b) {
              return a + b;
            });
        } else {
          this.totalsum = 0;
        }
      } else {
        let index = this.proposalData.findIndex(
          (list) => list.ProposalNo == ProposalNo
        );
        this.proposalData.splice(index, 1);
        let index1 = this.multitotalPremium.findIndex(
          (list) => list.totalPremium == totalPremium
        );
        this.multitotalPremium.splice(index1, 1);
        let index2 = this.singleData.findIndex(
          (list) => list.totalPremium == totalPremium
        );
        this.singleData.splice(index2, 1);
        if (this.multitotalPremium.length > 0) {
          this.totalsum = this.multitotalPremium
            .map((a) => a.totalPremium)
            .reduce(function (a, b) {
              return a + b;
            });
        } else {
          this.totalsum = 0;
        }

        if (this.multitotalPremium.length >= 2) {
          this.showbutton = true;
        } else {
          this.showbutton = false;
        }
      }


      if (this.multitotalPremium.length >= 2) {
        this.showbutton = true;
      } else {
        this.showbutton = false;
      }
    }

    if (this.singleData.length == 1) {
      this.showcheckButton = true;
      this.commonPaybutton(this.singleData[0]);
    } else {
      this.showcheckButton = false;
    }
    if(this.proposalData == []){
      this.isSendLinkForInstaPolicy = false;
    }
    localStorage.setItem("multipleProposal", JSON.stringify(this.proposalData));
    localStorage.setItem("multiTotalAmount", JSON.stringify(this.totalsum));
  }

  commonPaybutton(data: any) {
    let currentDate;
    currentDate = moment().format("YYYY-MM-DD");
    let date1, policystartdate;
    // if (this.ProductType == "11") {
    //   date1 = moment(data.allRisk.policyStartDate, "DD/MM/YYYY").format("YYYY-MM-DD");
    //   policystartdate = moment(currentDate).diff(date1, "days");
    // } else {
    date1 = moment(data.policyStartDate, "DD/MM/YYYY").format("YYYY-MM-DD");
    policystartdate = moment(currentDate).diff(date1, "days");
    // }

    if (policystartdate <= 0 && data.emailID != "" && data.emailID != null) {
      this.singleData[0].isResendLink = true;
    }
    // else if (policystartdate <= 0 && data.allRisk.emailID != "" && data.allRisk.emailID != null) {
    //   this.singleData[0].isResendLink = true;
    // } 
    else {
      this.singleData[0].isResendLink = false;
    }
    if (policystartdate <= 0) {
      this.singleData[0].isPayButtonVis = true;
    } else {
      this.singleData[0].isPayButtonVis = false;
    }

    if (policystartdate <= 0) {
      this.singleData[0].showRecal = false;
    } else {
      this.singleData[0].showRecal = true;
    }
  }

  validateMobile(){
    let isNumberValidate = this.cs.mobileValidation(this.custMobile);
    if (isNumberValidate && this.custMobile.length == 10) {
      this.custMobile = this.custMobile;
      this.isMobileValid = true;
    } else {
      this.isMobileValid = false;
    }
  }

  // MobileNo Validation
  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
