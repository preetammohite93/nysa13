import { DOCUMENT } from "@angular/common";
import { Component, Inject, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ApiServiceService } from "src/app/services/api-service.service";
import { CommonService } from "src/app/services/common.service";
import swal from "sweetalert";
import { commonData } from "src/app/commonData/commonData";

@Component({
  selector: "app-price-break-up",
  templateUrl: "./price-break-up.component.html",
  styleUrls: ["./price-break-up.component.css"],
})
export class PriceBreakUPComponent implements OnInit {
  correlationID: any;
  policy_Subtype: any;
  calculatedQuote: any;
  quoteResp: any;
  quoteReq: any;
  dealid: any;
  bankType: any;
  dealDetailsfromIM: any;
  stateCD: any;
  productCode: string;
  ODandTPAmount: any;
  premiumWithoutDiscount: any;
  totalDiscount: any;
  isRSAPlan: any;

  // Sejal - 9-5-2022 - All Risk Electric Bike
  evPremium: any;
  evBasePremium: any;
  evGST: any;
  scpaPremium: any;
  scpaBasePremium: any;
  scpaGST: any;
  totalTax: any;
  finalPremium: any;
  scpaReq: any;
  scpaRes: any;
  saveAllRiskData: any;
  bundleID: any;
  isSCPA: any;
  allRisk_productCode:any;

  constructor(
    public activeRoute: ActivatedRoute,
    public cs: CommonService,
    public api: ApiServiceService,
    @Inject(DOCUMENT) private document
  ) {
    this.cs.loaderStatus = true;
  }

  ngOnInit() {
    this.document.body.style.padding = "10px 10px";
    this.cs.loaderStatus = true;
    this.allRisk_productCode = commonData.allriskProductCode;
    this.activeRoute.queryParams.forEach((params) => {
      this.correlationID = params.CorrelationID;
      this.bundleID = params.BundleId;
      if (!this.cs.isUndefineORNull(this.bundleID)) {
        this.getAllRiskDetails();
      } else {
        this.getPriceBreakUP();
      }
    });
  }

  getPriceBreakUP() {
    this.cs
      .getBreakUPDetails(
        "customer/GetPremiumBreakupDetails?correlationID=" + this.correlationID
      )
      .then((res: any) => {
        if (res.status == "Failed") {
          swal({ text: res.message });
        } else {
          this.quoteReq = JSON.parse(res.quoteRQ);
          this.calculatedQuote = JSON.parse(res.quoteRS);
          this.dealid = this.quoteReq.DealId;
          this.policy_Subtype = this.calculatedQuote.PolicySubType;
          // if (this.policy_Subtype != "11") {
            this.ODandTPAmount =
            this.calculatedQuote.riskDetails.basicOD +
            this.calculatedQuote.riskDetails.basicTP;
          this.policy_Subtype = this.calculatedQuote.PolicySubType;
          let antiTheftDiscount = this.calculatedQuote.riskDetails
            .antiTheftDiscount
            ? this.calculatedQuote.riskDetails.antiTheftDiscount
            : 0;
          let automobileAssociationDiscount = this.calculatedQuote.riskDetails
            .automobileAssociationDiscount
            ? this.calculatedQuote.riskDetails.automobileAssociationDiscount
            : 0;
          let handicappedDiscount = this.calculatedQuote.riskDetails
            .handicappedDiscount
            ? this.calculatedQuote.riskDetails.handicappedDiscount
            : 0;
          let bonusDiscount = this.calculatedQuote.riskDetails.bonusDiscount
            ? this.calculatedQuote.riskDetails.bonusDiscount
            : 0;
          let voluntaryDiscount = this.calculatedQuote.riskDetails
            .voluntaryDiscount
            ? this.calculatedQuote.riskDetails.voluntaryDiscount
            : 0;
          let tppD_Discount = this.calculatedQuote.riskDetails.tppD_Discount
            ? this.calculatedQuote.riskDetails.tppD_Discount
            : 0;
          this.totalDiscount =
            antiTheftDiscount +
            automobileAssociationDiscount +
            handicappedDiscount +
            bonusDiscount +
            voluntaryDiscount +
            tppD_Discount;
          // this.totalDiscount = this.calculatedQuote.riskDetails.antiTheftDiscount + this.calculatedQuote.riskDetails.automobileAssociationDiscount + this.calculatedQuote.riskDetails.handicappedDiscount + this.calculatedQuote.riskDetails.bonusDiscount + this.calculatedQuote.riskDetails.voluntaryDiscount + this.calculatedQuote.riskDetails.tppD_Discount;
          // } else {
          //   this.getDealDetails();
          // }
          
          let pkgPrem = this.calculatedQuote.packagePremium
            ? this.calculatedQuote.packagePremium
            : this.calculatedQuote.totalLiabilityPremium;
          this.premiumWithoutDiscount = pkgPrem + this.totalDiscount;
          if (this.policy_Subtype == "1" || this.policy_Subtype == "12") {
            this.productCode = "2312";
          } else if (
            this.policy_Subtype == "2" ||
            this.policy_Subtype == "13"
          ) {
            this.productCode = "2311";
          } else if (this.policy_Subtype == "9") {
            this.productCode = "2320";
          } else if (this.policy_Subtype == "10") {
            this.productCode = "2319";
          } 
          // else if (this.policy_Subtype == "11") {
          //   this.productCode = commonData.allriskProductCode;
          // }
          this.quoteResp = JSON.parse(res.quoteRS);
          console.info(this.quoteResp);
          if (this.cs.isUndefineORNull(this.quoteReq.RSAPlanName)) {
            this.isRSAPlan = false;
          } else {
            this.isRSAPlan = true;
          }

          // if (this.productCode == commonData.allriskProductCode) {
          //   this.evPremium = this.quoteResp.TotalPremium;
          //   this.evBasePremium = this.quoteResp.BasicPremium;
          //   this.evGST = this.quoteResp.TotalTax;

          //   if (res.scpaRQ != null && res.scpaRS != null) {
          //     this.scpaReq = res.scpaRQ;
          //     this.scpaRes = res.scpaRS;
          //     this.scpaPremium = this.scpaRes.TotalPremium;
          //     this.scpaBasePremium = this.scpaRes.BasicPremium;
          //     this.scpaGST = this.scpaRes.TotalTax;
              
          //     this.totalTax = this.evGST + this.scpaGST;
          //     this.finalPremium = this.evPremium + this.scpaPremium;
          //   } else {
          //     this.totalTax = this.evGST;
          //     this.finalPremium = this.evPremium;
          //   }
          // }
        }
        this.cs.loaderStatus = false;
      })
      .catch((err: any) => {
        this.cs.loaderStatus = false;
      });
  }

  //Get Deal Details from deal
  getDealDetails() {
    this.api.getDealDetails(this.dealid).subscribe((res: any) => {
      if (res.status == "FAILED") {
        swal({ text: "Deal Details not found" });
      } else {
        this.dealDetailsfromIM = res;
        this.stateCD = this.dealDetailsfromIM.stateCD;
        this.bankType = this.dealDetailsfromIM.bankType;
        localStorage.setItem("bankType", this.bankType);
      }
    });
  }

  get getODSubTotal() {
    let total = 0;
    let count;
    if (this.productCode == "2312") {
      count = 8;
    } else if (this.productCode == "2311") {
      count = 14;
    }
    for (let i = 0; i < count; i++) {
      let data = String(document.getElementById(`odSub-${i + 1}`).innerHTML);
      total += Number(data.replace(/\D/g, ""));
    }
    return total;
  }

  get getTpTotal() {
    let total = 0;
    for (let i = 0; i < 3; i++) {
      let data = String(document.getElementById(`tpSub-${i + 1}`).innerHTML);
      total += Number(data.replace(/\D/g, ""));
    }
    return total;
  }

  get getliabTpTotal() {
    let total = 0;
    for (let i = 0; i < 4; i++) {
      let data = String(
        document.getElementById(`liabtpSub-${i + 1}`).innerHTML
      );
      total += Number(data.replace(/\D/g, ""));
    }
    return total;
  }

  //  get getdiscountTotal(){
  //   let total = 0;
  //   for( let i = 0; i < 5; i++) {
  //     let data = String(document.getElementById(`disc-${i+1}`).innerHTML);
  //     total += (Number(data.replace(/\D/g, "")));
  //   }
  //   this.premiumWithoutDiscount = this.calculatedQuote.packagePremium + total;
  //   return total;
  //  }

  getAllRiskDetails() {
    this.cs.getAllRiskData("SaveQuote/GetAllRiskDetails?bundleId=" + this.bundleID).subscribe((res: any) => {
      if (res.status == "Failed") {
        swal({ text: res.message });
      } else {
        this.cs.loaderStatus = false;
        // this.dontShowPage = true;

        this.saveAllRiskData = res.saveQuoteDetails;
        console.info(this.saveAllRiskData);
        this.dealDetailsfromIM = res.dealData;

        for (let entry of this.saveAllRiskData) {

          this.quoteReq = JSON.parse(entry.quotE_RQ);
          console.info(this.quoteReq);
          this.calculatedQuote = JSON.parse(entry.quotE_RS);
          this.dealid = this.quoteReq.DealId;
          this.policy_Subtype = this.calculatedQuote.PolicySubType;
          
          if (this.policy_Subtype == "11") {
            this.productCode = commonData.allriskProductCode;
          }

          this.quoteResp = JSON.parse(entry.quotE_RS);
          console.info(this.quoteResp);

          let pkgPrem = this.calculatedQuote.packagePremium
            ? this.calculatedQuote.packagePremium
            : this.calculatedQuote.totalLiabilityPremium;
          this.premiumWithoutDiscount = pkgPrem + this.totalDiscount;

          this.evPremium = parseInt(entry.totaL_PREMIUM);
          this.evBasePremium = parseInt(entry.totaL_PREMIUM) - parseInt(entry.totaL_TAX);
          this.evGST = parseInt(entry.totaL_TAX);
          if (entry.iS_ALL_RISK == false) {
            this.isSCPA = true;
            this.scpaReq = entry.quotE_RQ;
            this.scpaRes = entry.quotE_RS;
            this.scpaPremium = parseInt(entry.totaL_PREMIUM);
            this.scpaBasePremium = parseInt(entry.totaL_PREMIUM) - parseInt(entry.totaL_TAX);  //parseInt(entry.totaL_PREMIUM);
            this.scpaGST = parseInt(entry.totaL_TAX);
          
            this.totalTax = this.evGST + this.scpaGST;
            // this.finalPremium = this.evPremium + this.scpaPremium;
          } else {
            // this.totalTax = this.evGST;
            // this.finalPremium = this.evPremium;
          }  
          this.totalTax = this.evGST + this.scpaGST; 
          this.finalPremium = this.evPremium + this.scpaPremium;      
        }
        // this.getDealDetails();
        this.cs.loaderStatus = false;
      }
    })
  }
}
