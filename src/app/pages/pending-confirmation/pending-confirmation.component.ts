import { Component, OnInit } from "@angular/core";
import { CommonService } from "src/app/services/common.service";
import { Router } from "@angular/router";
import swal from "sweetalert";
import * as moment from "moment";

@Component({
  selector: "app-pending-confirmation",
  templateUrl: "./pending-confirmation.component.html",
  styleUrls: ["./pending-confirmation.component.css"],
})
export class PendingConfirmationComponent implements OnInit {
  PFPayReq: any;
  PFPayRes: any;
  pdfResponse: any;

  constructor(public router: Router, public cs: CommonService) {}

  ngOnInit() {
    this.PFPayReq = JSON.parse(localStorage.getItem("PFReq"));
    this.PFPayRes = JSON.parse(localStorage.getItem("PFRes"));
  }

  // Go to Dashboard
  goToDashboard() {
    this.cs.geToDashboard();
  }

  // Download PDF Policy
  showPolicy() {
    this.cs.loaderStatus = true;
    this.cs
      .get(
        "PolicySchedule/GeneratePolicySchedule?policyNo=" +
          this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].policyNo
      )
      .subscribe(
        (res) => {
          this.pdfResponse = res;
          if (
            this.pdfResponse != null &&
            this.pdfResponse != undefined &&
            this.pdfResponse.status == "SUCCESS"
          ) {
            var fileName =
              this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].policyNo
                .split("/")
                .join("_") + ".pdf";
            this.cs.save(
              "data:application/pdf;base64," + this.pdfResponse.buffer,
              fileName
            );
            this.cs.loaderStatus = false;
          } else {
            swal({
              title: "Alert!",
              text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
            });
          }
          this.cs.loaderStatus = false;
        },
        (err) => {
          swal({
            title: "Alert!",
            text: err.error.ExceptionMessage,
          });
          this.cs.loaderStatus = false;
          document.getElementById("myNav4").style.height = "0%";
        }
      );
  }
}
