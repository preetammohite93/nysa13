import { Component, OnInit } from "@angular/core";
import swal from "sweetalert";
import * as moment from "moment";
import { commonData } from "../../commonData/commonData";
import { CommonService } from "src/app/services/common.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-failed-payment",
  templateUrl: "./failed-payment.component.html",
  styleUrls: ["./failed-payment.component.css"],
})
export class FailedPaymentComponent implements OnInit {
  nysaPolicy: any;
  custid: any;
  pdfResponse: any;
  policyno: any;
  policyStartDate: any;
  policyEndDate: any;
  norecordflag: any;
  ProductType: any;
  term: any;
  failedDetail: any;
  dealID: any;
  PFPaymentTagging: any;
  bankingidData: any;
  RePaymentTagging: any;
  page: any;
  memmaxDOB: any;

  nysapolicyDetails = {
    FromDate: "",
    ToDate: "",
  };

  constructor(public router: Router, public cs: CommonService) {}

  ngOnInit() {
    this.ProductType = "1";
    this.memmaxDOB = new Date();
    this.memmaxDOB.setDate(this.memmaxDOB.getDate() + 0);

    this.dealID = JSON.parse(localStorage.getItem("DealID"));
  }

  validate() {
    this.checkBankingid().then(() => {
      this.cs.getPayAuth().then((resp: any) => {
        // this.payButton = true;
        localStorage.setItem("PayAuthToken", JSON.stringify(resp.accessToken));
      });
    });

    if (
      this.nysapolicyDetails.FromDate == null ||
      this.nysapolicyDetails.FromDate == undefined ||
      this.nysapolicyDetails.FromDate == ""
    ) {
      swal({ title: "Alert!", text: "Kindly insert from date" });
    } else if (
      this.nysapolicyDetails.ToDate == null ||
      this.nysapolicyDetails.ToDate == undefined ||
      this.nysapolicyDetails.ToDate == ""
    ) {
      swal({ title: "Alert!", text: "Kindly insert to date" });
    } else {
      this.getpolicies();
    }
  }

  vehicletype(ev: any) {
    if (ev.target.id == "failedselectVehicle_twoWheeler") {
      this.ProductType = "1";
    } else if (ev.target.id == "failedselectVehicle_fourWheeler") {
      this.ProductType = "2";
    } else if (ev.target.id == "failedselectVehicle_twoWheelerTP") {
      this.ProductType = "9";
    } else if (ev.target.id == "failedselectVehicle_fourWheelerTP") {
      this.ProductType = "10";
    }
  }

  // getPolicies
  getpolicies() {
    let from: any;
    let to: any;
    from = moment(this.nysapolicyDetails.FromDate);
    to = moment(this.nysapolicyDetails.ToDate);
    let diff = to.diff(from, "days");
    if (diff <= "0") {
      swal({
        title: "Alert!",
        text: "From Date & To Date cannot be same & To date cannot be less than from date",
      });
    } else {
      this.cs.loaderStatus = true;
      let body = {
        FromDate: moment(this.nysapolicyDetails.FromDate).format("MM/DD/YYYY"), //"4/12/2020",
        ToDate: moment(this.nysapolicyDetails.ToDate).format("MM/DD/YYYY"), // "4/14/2020",
        ProductType: "1",
        SubProductType: this.ProductType,
      };
      let str = JSON.stringify(body);
      this.cs.post("Payment/FailedPayment", str).then((res: any) => {
        if (res.status == null) {
          this.norecordflag = true;
        } else {
          this.norecordflag = false;
          this.nysaPolicy = res.policyDetails;
        }
        this.cs.loaderStatus = false;
      });
    }
  }

  fetch(ev: any) {
    this.policyno = ev.policyNo;
    this.failedDetail = ev;
  }

  // Check Banking ID
  checkBankingid(): Promise<any> {
    this.dealID = JSON.parse(localStorage.getItem("DealID"));
    return new Promise((resolve: any) => {
      let body = { DealID: this.dealID };
      let str = JSON.stringify(body);
      this.cs
        .post("Payment/FetchDealStatusDetails", str)
        .then((res: any) => {
          if (res.status == "SUCCESS") {
            this.bankingidData = res;
            resolve();
          } else {
            resolve();
            swal({
              title: "Alert!",
              text: "You cannot go further as no data found for your deal id",
            }).then(() => {
              this.goToDashboard();
            });
          }
        })
        .catch((err: any) => {
          resolve();
          swal({ title: "Alert!", text: err });
        });
    });
  }

  // Go to Dashboard
  goToDashboard() {
    this.cs.geToDashboard();
  }

  payment() {
    this.PFPayment().then(() => {
      this.RePayment();
    });
  }

  PFPayment() {
    return new Promise((resolve: any) => {
      let body = {
        isMappingRequired: false,
        isTaggingRequired: true,
        CorrelationId: this.failedDetail.corelationID,
        DealId: this.dealID,
        PaymentTagging: {
          PaymentID: this.failedDetail.pfPaymentID,
          customerProposal: [
            {
              CustomerID: this.failedDetail.pfCustomerID,
              ProposalNo: this.failedDetail.policyNo,
            },
          ],
        },
      };
      let str = JSON.stringify(body);
      this.cs.post1("Payment/PFPaymentTagging", str).then((res: any) => {
        this.PFPaymentTagging = res;
        resolve();
      });
    });
  }

  RePayment() {
    let body = {
      PaymentID: this.failedDetail.id,
      PolicyNo:
        this.PFPaymentTagging.paymentTagResponse.paymentTagResponseList[0]
          .policyNo,
      CoverNoteNo:
        this.PFPaymentTagging.paymentTagResponse.paymentTagResponseList[0]
          .coverNoteNo,
      Status:
        this.PFPaymentTagging.paymentTagResponse.paymentTagResponseList[0]
          .status,
      ErrorText:
        this.PFPaymentTagging.paymentTagResponse.paymentTagResponseList[0]
          .errorText,
    };
    let str = JSON.stringify(body);
    this.cs.post("Payment/RePaymentTagging", str).then((res: any) => {
      this.RePaymentTagging = res;
    });
  }
}
