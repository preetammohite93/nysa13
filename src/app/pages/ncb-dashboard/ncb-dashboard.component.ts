import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { CommonService } from 'src/app/services/common.service';
import swal from "sweetalert";


@Component({
  selector: 'app-ncb-dashboard',
  templateUrl: './ncb-dashboard.component.html',
  styleUrls: ['./ncb-dashboard.component.css']
})
export class NcbDashboardComponent implements OnInit {
  
  pendingPayment: any;
  IsPayLinkSent: string;
  PayLinkReciever: any;
  isSMSSent: any;
  CustomerPayLink: any;
  ncbLink: any;
  dealStartDate: any;
  dealEndDate: any;
  noCompleteRecord: any;
  noPendingRecord: any;
  pdfResp: any
  spinloaderpolicy = false;
  correlationId: any;
  completePayment: any;
  event: any;
  authtok: any;
  productCode: any;
  dealDetails: any;
  dealid: any;
  dealDetailsfromIM: any;
  stateCD: any;
  bankType: any;
  policy: any;
  product: any;
  term: any;
  policyNo: any;
  customerId: any
  isPending = true;
  moberrorFlag: boolean = false;
  selectedPendingData: any;
  selectedCompleteData: any;
  endoPolicyNo: any;
  sendLinkLoader: boolean = false;
  mobNumberPattern = "^(0/91)?[6-9][0-9]{9}$";
  sendDetails = {
    Email: "",
    mobileno: "",
  };
  ncbPendingPage:any;
  ncbCompletePage:any;
  // showMe:boolean=true;
  // hideme:boolean=false
  constructor(private cs: CommonService,private router: Router, public activeRoute: ActivatedRoute,public api: ApiServiceService,) { }

  ngOnInit() {
    this.authtok = JSON.parse(localStorage.getItem("AuthToken"));
    this.createPayToken();
    this.getDeal().then(()=>{
      this.getDealDeatils().then(()=>{
        this.dealDetailsfromIM = JSON.parse(localStorage.getItem("dealDetailsfromIMCode"));
        this.ncbPendingData();
      })
    })   
  }
  // ngAfterContentInit() {
  //  this.ncbPendingData();
  // }       
  // Number only valid
  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    console.log(event.target.value.length);
    let length = event.target.value.length;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  //Mobile validation
  mobileValidate() {
    let isNumberValidate = this.cs.mobileValidation(this.sendDetails.mobileno);
    if (isNumberValidate && this.sendDetails.mobileno.length == 10) {
      // this.sendDetails.mobileno = this.sendDetails.mobileno;
      this.moberrorFlag = false;
    } else {
      // this.sendDetails.mobileno = "";
      this.moberrorFlag = true;
    }
  }

  createToken() {
    let body = {
      username: "iwqIU6Z/t8o=",
      password: "VYxkVlge5rq0WHkO7bucmQ==",
    };

    let str = JSON.stringify(body);
    this.cs.createNcbToken("token", str).then((res: any) => {
      console.log(res);
      localStorage.setItem("ncbAuthToken", JSON.stringify(res));
    });
  }

  createPayToken() {
    let body = {
      Username: "mario",
      Password: "secret",
    };

    let str = JSON.stringify(body);
    this.cs.createNcbPayToken("Token", str).then((res: any) => {
      console.log(res);
    })

  }

  getData(ev: any) {
    if (ev == 'Pending') {
      this.ncbPendingData();
      this.isPending = true;
    } else if (ev == 'Completed') {
      this.ncbCompletedData();
      this.isPending = false;
    } else {

    }
  }
  
  getDeal(): Promise<void> {
    return new Promise((resolve) => {
      this.api
        .getDeal(this.authtok.username, '2311')
        .subscribe((res: any) => {
          this.dealDetails = res;
          this.dealid = this.dealDetails.dealID;
          localStorage.setItem("DealID", JSON.stringify(this.dealid));
          resolve();
        });
    });
  } 

  getDealDeatils():Promise<any>{
    return new Promise((resolve: any) =>{
      this.api.getDealDetails(this.dealid).subscribe((res: any) => {
        if (res.status == "FAILED") {
          swal({ text: "Deal Details not found" });
          let errorbody = {
            RequestJson: this.dealid,
            ResponseJson: JSON.stringify(res),
            ServiceURL: "Deal/GetDealDetailsFromDeal?dealID=" + this.dealid,
            CorrelationID: this.dealid,
          };
          this.api.adderrorlogs(errorbody);
          resolve();
        } else {
          this.dealDetailsfromIM = res;
          this.stateCD = this.dealDetailsfromIM.stateCD;
          this.bankType = this.dealDetailsfromIM.bankType;
          localStorage.setItem("bankType", this.bankType);
          localStorage.setItem("dealDetailsfromIMCode", this.dealDetailsfromIM.intermediaryCode);
          localStorage.setItem("dealStartDate", this.dealDetailsfromIM.startDate);
          localStorage.setItem("dealEndDate", this.dealDetailsfromIM.endDate);
          resolve();
       
        }
      });
    })
  }

  ncbPendingData(): Promise<any> {
    return new Promise((resolve: any) => {
      // this.showMe = true;
      let fromDate = "01-01-22";
      let toDate = moment(new Date()).format("MM-DD-YY");
      this.dealDetailsfromIM = JSON.parse(localStorage.getItem("dealDetailsfromIMCode"));
      // this.dealStartDate = JSON.parse(localStorage.getItem("dealStartDate"));
      // this.dealEndDate = JSON.parse(localStorage.getItem("dealEndDate"));
      let body = {
        intermediate_code: JSON.stringify(this.dealDetailsfromIM),//"201244152088", //"IM CODE-007",
        fromDate: fromDate, //"10-FEB-22",
        toDate: toDate//"13-FEB-22"
      }
      let strBody = JSON.stringify(body);
      this.cs.postForNcbDashboard("NCBPendingPaymentDetails", strBody).then((res: any) => {
        console.log(res);
        this.pendingPayment = res.ncbPendingDetails;
        localStorage.setItem("pendingPayment", JSON.stringify(res));
        // this.event = ev.target.value;
        if (res.message == "No Data Found" && this.cs.isUndefineORNull(res.ncbPendingDetails)) {
          this.noPendingRecord = true;
        } else {
          this.noPendingRecord = false;

        }
        resolve();
      })
    })
  }

  ncbCompletedData(): Promise<any> {
    return new Promise((resolve: any) => {
      let fromDate = "01-01-22";
      let toDate = moment(new Date()).format("MM-DD-YY");
      this.dealDetailsfromIM = JSON.parse(localStorage.getItem("dealDetailsfromIMCode"));
      // this.showMe = true;
      let body = {
        intermediate_code: JSON.stringify(this.dealDetailsfromIM), //"201244152088", 
        fromDate:fromDate,
        toDate: toDate
      }
      let strBody = JSON.stringify(body);
      this.cs.postForNcbDashboard("NCBCompletePaymentDetails", strBody).then((res: any) => {
        console.log(res);
        this.completePayment = res.ncbCompleteDetails;
        localStorage.setItem("completePayment", JSON.stringify(res));
        // this.event = ev.target.value;
        if (res.message == "No Data Found" && this.cs.isUndefineORNull(res.ncbCompleteDetails)) {
          this.noCompleteRecord = true;
        } else {
          this.noCompleteRecord = false;
        }
        resolve();
      })
    })
  }

  sendLinkData(data:any){
    console.log('ncbdata',data);
    this.selectedPendingData = data;
    this.sendDetails.mobileno = this.selectedPendingData.contact_no;
    // this.sendDetails.Email = this.selectedPendingData.email_id;
  }
  
  
  
  sendPaymentLink(): Promise<any> {
    return new Promise((resolve: any)=>{
    this.sendLinkLoader = true;
    let splitPolicy = this.selectedPendingData.ilpolicy_no.split('/');
    console.log('ncbpolicy',splitPolicy);
    let ilPolicyno = splitPolicy[0]
      let body = {
        "policyNumber" : this.selectedPendingData.ilpolicy_no,
        // "product_code": ilPolicyno == '3001' ? 2 : 1,
        "mobile_no": this.sendDetails.mobileno,
        "email_id": this.sendDetails.Email,
        "ncb_per": JSON.stringify(this.selectedPendingData.ncb_per),
        "ncb_amount": JSON.stringify(this.selectedPendingData.ncb_to_be_recovered),
        "Correlation_Id":this.selectedPendingData.correlation_Id
      }
      let strBody = JSON.stringify(body);
      let status = "N";
      let smsStatus = "N";
      this.cs.postForNcbLinkDashboard("NCBRecovery", strBody).then((res: any) => {
        console.log(res);
        if (res.status == "Failed" && res.message == "Amount Mismatch") {
          this.sendLinkLoader = false;
          swal({
            text: "Amount has been Mismatch. Please contact your Relationship Manager",
          });
          resolve();
        } else if (res.status == "Failed" && res.message == "Proposal not created") {
          this.sendLinkLoader = false;
          swal({
            text: "Invalid data",
          });
          resolve();
        }
        else {
          this.sendLinkLoader = false;
          const emailStatus = res.isLinkSent ? true : false;
          const mobileStatus = res.isSMSSent ? true : false;
          const checkBoth = emailStatus && mobileStatus ? true : false;
          let msg;
          let maskedEmail = this.sendDetails.Email.replace(
            /(.{2})(.*)(?=@)/,
            function (gp1, gp2, gp3) {
              for (let i = 0; i < gp3.length; i++) {
                gp2 += "X";
              }
              return gp2;
            }
          );
          let maskedMobile =
            this.sendDetails.mobileno.slice(0, 2) + this.sendDetails.mobileno.slice(2).replace(/.(?=...)/g, "X");
          if (checkBoth) {
            msg = `Payment link has been sent to ${maskedEmail} and ${maskedMobile}.`;
            status = "Y";
            smsStatus = "Y";
            // this.router.navigateByUrl("/ncbPayment");
          } else {
            if (emailStatus) {
              msg = `Payment link has been sent to ${maskedEmail}.`;
              status = "Y";
              smsStatus = "N";
              // this.router.navigateByUrl("/ncbPayment");
            } else if (mobileStatus) {
              msg = `Payment link has been sent to ${maskedMobile}.`;
              status = "N";
              smsStatus = "Y";
              // this.router.navigateByUrl("/ncbPayment");
            } else if (!checkBoth) {
              msg = `Failed to send payment link to ${maskedEmail} and ${maskedMobile}.`;
              status = "N";
              smsStatus = "N";
            }
          }
          swal({ text: msg }).then(() => {
            this.cs.loaderStatus = false;
          });
          resolve();
        }
      });
    })
  }
  
 
  FetchNcbPdf(data:any){
    this.selectedCompleteData = data;
    // this.policyNo = this.selectedCompleteData.ilpolicy_no;
    this.endoPolicyNo = this.selectedCompleteData.endorsement_no;
    this.customerId = this.selectedCompleteData.cust_id;
    this.correlationId = this.selectedCompleteData.correlation_id;
    this.downloadPolicy();
  }


  downloadPolicy() {
    this.spinloaderpolicy = true;
    this.cs
      .get(
        "PolicySchedule/GeneratePolicySchedule?policyNo=" +
        this.endoPolicyNo +
          "&correlationID=" +
          this.correlationId +
          "&customerId=" +
          this.customerId
      )
      .subscribe(
        (res) => {
          this.pdfResp = res;
          if (!this.cs.isUndefineORNull(this.pdfResp) && this.pdfResp.status == "SUCCESS") {
            var fileName = this.endoPolicyNo.split("/").join("_") + ".pdf";
            this.cs.save(
              "data:application/pdf;base64," + this.pdfResp.buffer,
              fileName
            );
            // this.cs.loaderStatus = false;
            this.spinloaderpolicy = false;
          } else {
            swal({
              text: "Try fetching Policy pdf from My policies dashboard after sometime",
            });
            let errorbody = {
              RequestJson: this.endoPolicyNo,
              ResponseJson: JSON.stringify(res),
              ServiceURL:
                "PolicySchedule/GeneratePolicySchedule?policyNo=" +
                this.endoPolicyNo,
              CorrelationID: this.correlationId,
            };
            this.api.adderrorlogs(errorbody);
          }
          this.spinloaderpolicy = false;
          // this.cs.loaderStatus = false;
        },
        (err) => {
          swal({
            text: "Try fetching Policy pdf from My policies dashboard after sometime",
          });
          // this.cs.loaderStatus = false;
          this.spinloaderpolicy = false;
          document.getElementById("myNav4").style.height = "0%";
        }
      );
  }
  
  }
  
  

  
