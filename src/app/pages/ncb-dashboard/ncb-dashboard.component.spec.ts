import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NcbDashboardComponent } from './ncb-dashboard.component';

describe('NcbDashboardComponent', () => {
  let component: NcbDashboardComponent;
  let fixture: ComponentFixture<NcbDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NcbDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NcbDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
