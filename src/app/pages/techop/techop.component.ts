import { Component, OnInit } from "@angular/core";
import * as moment from "moment";
import { CommonService } from "src/app/services/common.service";
import swal from "sweetalert";

@Component({
  selector: "app-techop",
  templateUrl: "./techop.component.html",
  styleUrls: ["./techop.component.css"],
})
export class TechopComponent implements OnInit {
  techopObj = {
    FromDate: "",
    ToDate: "",
  };
  techOpsList: any;
  diffDays: any;
  page: any;
  term: any;
  memmaxDOB: any;
  norecordflag: any;
  isIMIDEntered: boolean = false;
  imidDiv: boolean = true;
  authUser: any;
  imid: any;
  constructor(private cs: CommonService) {
    let date = new Date();
    this.techopObj.FromDate = date.toString();
    this.techopObj.ToDate = date.toString();

    this.getTechOpsList();
  }

  ngOnInit() {
    // this.getTechOpsList();
    this.cs.getPayAuth().then((resp: any) => {
      localStorage.setItem("PayAuthToken", JSON.stringify(resp.accessToken));
    });

    this.memmaxDOB = new Date();
    this.memmaxDOB.setDate(this.memmaxDOB.getDate() + 0);
    if (localStorage.getItem("techopsUser")) {
      this.imid = localStorage.getItem("techopsUser");
    }
  }

  getTechOpsList() {
    let body = {
      FromDate: moment(this.techopObj.FromDate).format("DD-MMMM-YYYY"),
      ToDate: moment(this.techopObj.ToDate).format("DD-MMMM-YYYY"),
    };

    let str = JSON.stringify(body);
    this.cs.loaderStatus = true;

    this.cs
      .postOps("TechOps/Dashboard", str)
      .then((res: any) => {
        if (res.status == "Success") {
          this.term = "";
          this.cs.loaderStatus = false;
          this.techOpsList = res.dashboardRecords;
          //  this.techopObj.FromDate = '';
          //  this.techopObj.ToDate = '';
        } else {
          this.cs.loaderStatus = false;
        }
      })
      .catch((err: any) => {
        this.cs.loaderStatus = false;
      });
  }

  getList() {
    let from: any;
    let to: any;
    from = moment(this.techopObj.FromDate);
    to = moment(this.techopObj.ToDate);
    let diff = to.diff(from, "days");
    this.diffDays = diff;

    if (this.diffDays >= 0) {
      this.getTechOpsList();
    } else {
      swal({ title: "Alert!", text: "To Date cannot be less than from date" });
    }
  }

  reattemptCase(data) {
    let paymentId;

    if (data.pF_PAYMENTID == null) {
      paymentId = 0;
    } else {
      paymentId = JSON.parse(data.pF_PAYMENTID);
    }

    let body = {
      CorelationID: data.correlationID,
      DealID: data.deaL_ID,
      PaymentID: paymentId,
      ProposalNo: data.proposaL_NUMBER,
      CustomerID: data.customeR_ID,
      TransactionID: data.transactionID,
      isMappingRequired: data.ismappingsuccess,
      isTaggingRequired: data.istaggingsuccess,
      EmployeeId: this.imid,
    };

    //   let body = {
    //     "CorrelationID": "d2fe8c5e-11f0-6690-646d-4cbb333ec773",
    //     "DealID": "DEAL-3001-0000006",
    //     "PaymentID": 1052306181,
    //     "ProposalNo": "1206763823",
    //     "CustomerID": "102025889457",
    //     "isMappingRequired": false,
    //     "isTaggingRequired": false
    // }

    let str = JSON.stringify(body);
    this.cs.loaderStatus = true;

    this.cs
      .post1("Payment/ReattemptPolicy", str)
      .then((res: any) => {
        if (res.statusMessage == "Success") {
          this.cs.loaderStatus = false;
          // this.techOpsList = res.dashboardRecords;
          let policy =
            res.paymentTagResponse.paymentTagResponseList[0].policyNo;
          let coverNote =
            res.paymentTagResponse.paymentTagResponseList[0].coverNoteNo;
          swal({
            title: "Alert!",
            text:
              "PaymentTagging and PaymentMapping is success  and covernote is : " +
              coverNote +
              "and policy number is : " +
              policy,
          }).then((res) => {
            this.getTechOpsList();
          });
        } else {
          this.cs.loaderStatus = false;
        }
      })
      .catch((err: any) => {
        this.cs.loaderStatus = false;
      });
  }

  //  getIMID(){
  //    if (this.imid.length < 3 || parseInt(this.imid) == 0) {
  //     swal({
  //       title: "Alert!",
  //       text: 'Kindly enter propoer employee id'
  //     })
  //    } else {
  //     this.isIMIDEntered = true;
  //     this.imidDiv = false;
  //     this.authUser = this.imid;

  //    }

  //  }

  goToLogin() {
    localStorage.removeItem("techopsUser");
    this.cs.goToTechOpsLogin();
  }
}
