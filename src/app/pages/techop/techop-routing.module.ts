import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TechopComponent } from "./techop.component";

const routes: Routes = [
  {
    path: "",
    component: TechopComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TechopRoutingModule {}
