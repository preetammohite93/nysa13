import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { TechopRoutingModule } from "./techop-routing.module";
import { TechopComponent } from "./techop.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [TechopComponent],
  imports: [CommonModule, TechopRoutingModule, SharedModule],
})
export class TechopModule {}
