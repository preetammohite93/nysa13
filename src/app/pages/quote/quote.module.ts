import { MatSelectModule } from "@angular/material/select";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { QuoteComponent } from "./quote.component";
import { QuoteRoutingModule } from "./quote-routing.module";
import { SharedModule } from "src/app/shared/shared.module";
import { SharedtranslateModule } from "src/app/shared/sharedtranslate/sharedtranslate.module";
import { RenewalDashboardComponent } from 'src/app/pages/renewal-dashboard/renewal-dashboard.component'
// import {
//   MatAutocompleteModule,
//   MatDatepickerModule,
//   MatExpansionModule,
//   MatIconModule,
// } from "@angular/material";
import { FormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    QuoteRoutingModule,
    SharedModule,
    SharedtranslateModule,

    FormsModule,
  ],
  exports: [QuoteComponent],
  declarations: [QuoteComponent, RenewalDashboardComponent],
  providers: [],
})
export class QuoteModule {}
