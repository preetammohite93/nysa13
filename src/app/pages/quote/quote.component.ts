import {
  Component,
  Input,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef,
  AfterContentInit,
} from "@angular/core";
import { commonData } from "../../commonData/commonData";
import { CommonService } from "src/app/services/common.service";
import { ApiServiceService } from "src/app/services/api-service.service";
import { Router } from "@angular/router";
import swal from "sweetalert";
import * as moment from "moment";
import { Guid } from "guid-typescript";
import "rxjs/add/operator/timeout";
declare var $: any;
import { Options } from "ng5-slider";
import { parse } from "zipson";
import { CommonMessageService } from "src/app/services/common-message.service";

declare var jspdf: any;
declare var html2canvas: any;

@Component({
  selector: "app-quote",
  templateUrl: "./quote.component.html",
  styleUrls: ["./quote.component.css"],
  encapsulation: ViewEncapsulation.None,
})
export class QuoteComponent implements OnInit, AfterContentInit {
  public responseCache = new Map();
  sortedClaimDetails = [];
  isRecalculate1 = false;
  failedCalculatedQuote: any;
  nysaPolicydata: boolean = false;
  nysaRenewaldata: boolean = false;
  moberrorFlag: boolean = false;
  reCalDealerDiscount: any;
  previousPolicyValue: boolean = false;
  additional: boolean = false;
  isFocus: boolean = true;
  bFlag: any;
  claimdiv: boolean = true;
  calculatedQuote: any;
  manumod: any;
  rtoList: any;
  totalDiscount: any;
  stateMaster: any;
  exshowroomprice: any;
  makecode: any;
  modelcode: any;
  years = [];
  premiumWithoutDiscount: any;
  manuflag: boolean = false;
  showGetQuoteBtn: boolean = false;
  viewBreakUpText:string 
  rtoDetail: any;
  rtoCode: any;
  RTOname: any;
  dealDetails: any;
  dealid: any;
  dealDetailsfromIM: any;
  radioValue: any;
  paramDataValues: any;
  insurerlist: any;
  policyType: any;
  productCode: any;
  authtok: any;
  classCode: any;
  manuinput: any;
  registrationType: any = "INDIVIDUAL";
  ncbValue: any;
  companyCode: any;
  claimNCB: boolean = true;
  claimNCBT: boolean = true;
  previousPolicyTen: any;
  ClaimOnPreviousPolicy: boolean = false;
  vehicledesc: any;
  modelName: any;
  exShowroomPrice: any;
  type: any;
  product: any;
  manuname: any;
  quoteReq: any;
  quoteReqrem: any;
  model: any;
  datalistname: any;
  regDatecheck: any;
  rtoLength: any;
  rollpolicyType: any = "Comprehensive";
  nysaPolicy: any;
  memmaxDOB: any;
  policyEndDate: any;
  cubiccapacity: any;
  rtoName: any;
  ManumodelName: any;
  selectData: any;
  rtoinputvalue: any;
  TransFor: any;
  checkdate: any;
  PolicyTenure: any;
  MotorSubType: any;
  stateCD: any;
  statecode: any;
  IsInterstate: any;
  dateStart;
  manufacname: any;
  getVehicleFlag: boolean = false;
  memminDOB: any;
  manumodelbind: any;
  additionalcoverflag: any;
  paidDriver: boolean = false;
  paidEmployee: boolean = false;
  showAddon: boolean = false;
  showAddon1: boolean = false;
  quickquote: boolean = true;
  totalPremium: any;
  totalPremiumTP: any;
  totalPremiumTP2: any;
  totalPremiumTP3: any;
  submitQuote: boolean = false;
  isRecalculate: boolean = false;
  disabled: any;
  disabled1: any;
  disabled3: any;
  disabled4: any;
  cpaTenure: any;
  showValues: boolean = false;
  showaccessories: any;
  PaidDrivervalue: any;
  PaidEmployeevalue: any;
  voluntaryDeductableValue: any;
  isncbProtect: any;
  isRSAPlan: boolean;
  isGarageCashCover: boolean;
  isZeroDepPlanName: boolean;
  saveQuote: any;
  colSize2: any;
  zeroDepPlans: any;
  rsaPlans: any;
  ncbProtectPlans: any;
  garageCashPlans: any;
  panelOpenState: any = true;
  panelOpenStateAddon: any;
  garagecashVal: any;
  // PaidEmployee: any;
  isKeyProtectPlanName: boolean;
  isLossOfPersonalBelonging: boolean;
  PreviousPolicyDetails: any = {};
  idv: any;
  lowerLimit: any;
  upperLimit: any;
  modeldesc: any;
  name: any;
  TPPDLimit: any;
  showIDV: boolean = false;
  colSize: any;
  isValidDrivingLicense: boolean = false;
  colSize1: any;
  voluntaryDedData: any;
  showPrev: boolean = false;
  seatingcapacity: any;
  isTransfer: boolean = true;
  TodayDate: any;
  liablitySubTotal: any;
  claimAdd: any;
  voluntaryDiscount: any;
  bankType: any;
  polmminDOB: any;
  // polmmaxDOB: any;
  value: number = 200;
  rsaplanname: any;
  showsaod: boolean = false;
  vehicleAge: any;
  systemQC: boolean = false;
  panelOpenStatePremium: any;
  panelOpenStateProposal: boolean = false;
  planName: any;
  premid1: any;
  premid2: any;
  premid3: any;
  fillprev = false;
  IsAutomobileAssocnFlag: any;
  premres1: any;
  premres2: any;
  premres3: any;
  quoteResp: any;
  quoteRequest: any;
  policySubType: any;
  antitheftDiscount: any;
  premreq1: any;
  premreq2: any;
  premreq3: any;
  quoteRequ: any;
  IsAntiTheftDisc: boolean = false;
  IsHandicapDisc: boolean = false;
  freezeManuModel: boolean = false;
  IsSelfInspection: boolean = true;
  failedquote: boolean = false;
  prevtenure = 1;
  getVehicleDetailsClicked: boolean = false;
  countaddon = 0;
  showmessage: boolean = false;
  openQuoteSection: boolean = false;
  showdiscinput: boolean = false;
  sendpdf: boolean = false;
  isRegnocorrect: boolean = false;
  twoletteralpha: boolean = false;
  rtoerror: boolean = false;
  manuerror: boolean = false;
  spinloader: boolean = false;
  showQuotebutton: boolean = false;
  panelOpenIIBGrid: boolean = false;
  showIIBData = false;
  options: Options = {
    floor: 100,
    ceil: 300,
  };
  proposalPanelOpened = false;
  myBreakinPolicyTab: boolean = false;
  policyEnddateDiffFlag: boolean = false;
  subLocation: any;
  sublocations: any;
  // refreshBreadcrumb:boolean = false;
  public headerData: number = 0;
  public placeholder: string = "Enter the Country Name";
  public historyHeading: string = "Recently selected";
  public keyword = "name";
  mobNumberPattern = "^(0/91)?[6-9][0-9]{9}$";

  breakinDetails = {
    inspectionMode: "",
    subLocation: "",
  };

  breakintype = {
    breakin: "selfinspection",
    breakincustom: "selfinspection1",
    isPreApprovedBreakIN: false,
    breakINID: "",
  };
  // Model Form
  quoteDetails = {
    ncbChecked: "",
    claim: "claimyes",

    rtO_LOCATION_DESC: "",
    RegistrationNumber: "",
    FirstRegistrationDate: "",
    GSTToState: "",
    PolicyStartDate: "",
    Tenure: "",
    TPTenure: "",
    vehiclemodel: "",
    ManufacturingYear: "",
    PACoverTenure: "",

    // SAOD TAGS
    TPStartDate: "",
    TPEndDate: "",
    TPPolicyNo: "",
    TPInsurerName: "",

    // Rollover Tags
    PreviousPolicyDetails: false,
    PreviousPolicyStartDate: "",
    PreviousPolicyEndDate: "",
    PreviousPolicyType: "",
    BonusOnPreviousPolicy: "",
    PreviousPolicyNumber: "",
    ClaimOnPreviousPolicy: "",
    TotalNoOfODClaims: "0",
    NoOfClaimsOnPreviousPolicy: "",
    PreviousInsurerName: "",
    PreviousVehicleSaleDate: "",
    PreviousPolicyTenure: "",
    claims: "claimyes",

    // Addons
    IsExtensionCountry: false,
    ExtensionCountryName: "",
    IsPACoverUnnamedPassenger: false,
    SIPACoverUnnamedPassenger: "100000",
    RSAPlanName: "",
    ZeroDepPlanName: "",
    IsLegalLiabilityToPaidDriver: false,
    NoOfDriver: "0",
    IsLegalLiabilityToPaidEmployee: false,
    NoOfEmployee: "1",
    IsConsumables: false,
    IsVoluntaryDeductible: false,
    VoluntaryDeductiblePlanName: "",
    isNCBProtect: false,
    ncbProtectPlanName: "Plan1",
    isGarageCash: false,
    garageCashPlanName: "",
    IsRTIApplicableflag: false,
    IsVehicleHaveLPG: false,
    IsVehicleHaveCNG: false,
    IsVehicleHaveCNGLPG: "CNG",
    SIVehicleHaveLPG_CNG: "1000",
    OtherDiscount: 0,

    //4w package addons
    KeyProtectPlan: "",
    LossOfPersonalBelongingPlanName: "",
    IsEngineProtectPlus: false,
    IsTyreProtect: false,

    IsEMIProtect: false,
    EMIAmount: "",
    NoofEMI: 1,
    TimeExcessindays: 7,
    PolicyEndDate: "",

    //All Risk Electric Vehicle - Sejall - 28-02-2022
    batterysaver: "",
    PurchaseDate: "",
    UniqueIDnumber: "",
    InvoiceNumber: "",
    Manufacturer: "",
    model: "",
    TransactionType: "",
    SumInsuredIDV: 0,
    evTenure: "",
  };

  sendDetails = {
    Email: "",
    mobileno: "",
  };
  saodBundleID:any;
  public id;
  keyProtectPlans: any;
  lossOfPersonalBelongingPlans: any;
  policy: any;
  shownew: any;
  dataVal: any;
  mandatorycover: number;
  failerrormsg: any;
  premiumloader: boolean = true;
  quoteservice: any;
  basicbreak: any;
  ODandTPAmount: any;
  errorRTO: boolean;
  manerror: boolean;
  isBreakInDash = false;
  isexpanded: boolean = false;
  deviceInfo = null;

  isRegNumberValid = true;
  isRegNumberTextValid = true;
  isCityValidate = true;
  isRTOValidate = true;
  isManValidate = true;
  isModelValidate = true;
  isSiteDown = false;
  getrtoButtonclicked: boolean = false;
  claimsmade: string;
  otherdisccheck: any;
  showqcmsg: boolean = false;
  showElement: boolean = false;
  radiocheck: any;
  locationDetails: any;
  today = new Date();
  todaysDataTime = "";
  /**
   * Digital POS variables
   */
  IsPOSTransaction_flag: boolean = false;
  btn_disable_flag: boolean;
  localAuth: any;
  POSmobileNumber: any;
  IIBData: any;
  claimCount = 0;

  // Deviation Changes
  isDeviationTriggered: boolean = false;
  deviationMsg: string;
  isIDVChangeDev = false;
  isOtherDiscDev = false;
  isIDVChangeMSG = false;
  //Crawler Changes
  isCrawlerEnable = false;
  otherDiscount: any;
  isEnableCarInfoNewService = false;
  isRTOFreeze = false;
  showPYP: boolean = false;
  step = 0;
  spinloader1 = false;
  backupPreviousInsurerList: any = [];
  ManufactureModelList: any = []; // Array for Manufacture/Model List

  //All Risk electric bike - Sejall - 28-02-2022
  isBatterySaverValid = true;
  isUniqueNumberValid = true;
  isInvoiceValid = true;
  isPurchaseDateValid = false;
  isStateValid = false;
  allriskEBModel: any;
  EBVehicleModel: any;
  modelname: any;
  ebmanulist: any;
  EBManufacturerList: any;
  evSumInsured: any;
  rate = 2.25; //in percent
  isSCPA = true;
  scpaPremium: any;  // scpa premium
  scpaBasePremium: any; // scpa basic premium
  evTotalPremium: any; // ev premium
  evBasePremium: any; // ev basic premium
  evGST: any;
  scpaGST: any;
  totalGST: any;
  isValidSCPA = true; // false if corporate customer type
  sendCustPaymentLinkFlag: boolean = false;
  watchMe: any;
  evPolicyTenure: any;
  scpaSumInsured = 1500000;
  scpacalculatedQuote: any;
  scpafailedCalculatedQuote: any;
  scpafailedquote = false;
  policyno: any;
  customerId: any;
  spinloaderpolicy: boolean = false;
  pdfResponse: any;
  isPremiumBeakup = false;
  bundleID: any;
  allriskManufacturer: any;


  oldSaveQuoteRequ: any;
  oldSaveRespRequest: any;
  isSCPARequired = false;
  scpaResponse:any;
  allRisk_productCode:any;
  showSCPAinNysa = false;
  allriskmanumod: any = {};
  kilowatt : any;
  constructor(
    public router: Router,
    public cs: CommonService,
    public api: ApiServiceService,
    public msgService: CommonMessageService,
    private changeDetector: ChangeDetectorRef
  ) {
    this.dateStart = new Date(); //or New Date()
    localStorage.setItem(
      "AppStartTime",
      JSON.stringify(this.dateStart.getTime())
    );
    this.id = Guid.raw();

    this.cs
      .getVahaanToken()
      .then((resp: any) => {
        localStorage.setItem("AuthTokenRedis", JSON.stringify(resp));
      })
      .catch((err: any) => {
        swal({
          text: "There is some issue with system.",
          closeOnClickOutside: false,
        });
      });

    /**
     * Digital POS change
     * get values from localStorage for checking if conditions in html file
     * author :- digitalPOS
     * date :- 15-06-2021
     */
    if (localStorage.getItem("IsPOSTransaction")) {
      this.IsPOSTransaction_flag = JSON.parse(
        localStorage.getItem("IsPOSTransaction")
      );
    }

    /**
     * Get the decrypted Manufacture/Model List
     * Author :- Sumit
     * date :- 31-01-2022
     */
    if (
      localStorage.getItem("ManufactureModelList_4w") ||
      localStorage.getItem("ManufactureModelList_2w") || 
      localStorage.getItem("ManufactureModelList_AllRisk")
    ) {
      if (this.productCode == "2312" || this.productCode == "2320") {
        let encryptedrtoList = localStorage.getItem("ManufactureModelList_2w");
        let decryptedrtoList =
          this.cs.decryptLikePythonForRTO(encryptedrtoList);
        const parsed = parse(decryptedrtoList);
        this.ManufactureModelList = parsed;
        this.classCode = "37";
      } else if (this.productCode == "2311" || this.productCode == "2319") {
        let encryptedrtoList = localStorage.getItem("ManufactureModelList_4w");
        let decryptedrtoList =
          this.cs.decryptLikePythonForRTO(encryptedrtoList);
        const parsed = parse(decryptedrtoList);
        this.ManufactureModelList = parsed;
        this.classCode = "45";
      } else if (this.productCode == commonData.allriskProductCode) {
        let encryptedrtoList = localStorage.getItem("ManufactureModelList_AllRisk");
        let decryptedrtoList =
          this.cs.decryptLikePythonForRTO(encryptedrtoList);
        const parsed = parse(decryptedrtoList);
        this.ManufactureModelList = parsed;
        //this.classCode = "45";

        // let encryptedmanulist = localStorage.getItem("ManufactureListAllRisk");
        // let decryptedmanulist = this.cs.decryptLikePythonForRTO(encryptedmanulist);
        // const parsed1 = parse(decryptedrtoList);
        // this.allriskmanumod = localStorage.getItem("ManufactureListAllRisk");
      }
    } else {
      // swal({ text: "Issue in manufacture and model", closeOnClickOutside: false });
    }
  }

  ngOnInit() {
    localStorage.removeItem("DisablecustLink");
    localStorage.removeItem("breakinData");
    localStorage.removeItem("PreBreakINData");
    localStorage.removeItem("IDVTriggred");
    localStorage.removeItem("ODTriggred");
    localStorage.removeItem("dashboardData");
    localStorage.removeItem("ProductCode");
    localStorage.removeItem("SAODflag");
    localStorage.removeItem("customiseFlag");
    localStorage.removeItem("fromDate");
    localStorage.removeItem("toDate");
    localStorage.removeItem("dashboardBodyIndex");
    localStorage.setItem('isRenewalProposalModify', 'false');
    localStorage.removeItem("allriskmultipleProposal");
    localStorage.removeItem("allriskmultiTotalAmount");
    localStorage.removeItem("isSCPARequired");
    localStorage.removeItem("resSAODSCPA");
    localStorage.removeItem("reqSAODSCPA");
    localStorage.removeItem("reqPropSAODSCPA");
    localStorage.removeItem("SCPAToken");
    let regNo = JSON.parse(localStorage.getItem("regNumber"));
    if(!this.cs.isUndefineORNull(regNo)){
      this.quoteDetails.RegistrationNumber = regNo;
      this.calledVahaanAPI();
    }
    
    this.getStateData();
    // Crawler Data
    this.checkCrawler();
    // this.cs.createIASToken();   // Create IAS token on Raise Interaction - Sejal
    // this.getRTOtest();
    // this.showspecificmessage();
    this.planName = "plan1";

    $(document).ready(function () {
      $('[data-toggle="tooltip"]').tooltip();
    });
    localStorage.removeItem("breakinFlag");
    localStorage.removeItem("isIASFlag");
    localStorage.removeItem("IASDataProp");
    localStorage.removeItem("instaPayment");
    localStorage.removeItem("InstaData");
    localStorage.removeItem("savedQuotes");
    localStorage.removeItem("saveQuoteData");
    localStorage.removeItem("oldreference");

    window.scrollTo(0, 0);

    let cdate = new Date();
    cdate.setHours(cdate.getHours() - 2);

    let localAuth = JSON.parse(localStorage.getItem("AuthToken"));
    if (!this.cs.isUndefineORNull(localAuth)) {
      if (this.cs.checkTokenValidity()) {
        this.cs.geToLogin();
      } else {
      }
    } else {
    }

    /**
     * digitalPOS Change
     */
    this.localAuth = JSON.parse(localStorage.getItem("AuthToken"));

    this.quoteDetails.PreviousPolicyType = "Comprehensive Package";
    // this.isBreakInDash = JSON.parse(localStorage.getItem('fromBreakInDash'));
    // let myBreakinMenu = document.getElementById('myBreakinMenu') as HTMLInputElement;
    // if(this.isBreakInDash){
    //   myBreakinMenu.classList.add('active');
    // }else{
    //   // myBreakinMenu.classList.remove('active');
    // }

    this.cpaTenure = 0;
    this.disabled = true;
    this.disabled1 = false;
    this.ncbValue = "0";
    // this.quoteDetails.BonusOnPreviousPolicy = "0";
    this.quoteDetails.TotalNoOfODClaims = "0";

    if (JSON.parse(localStorage.getItem("calQuoteReq"))) {
      this.quoteReq = JSON.parse(localStorage.getItem("calQuoteReq"));
      this.quoteReqrem = JSON.parse(localStorage.getItem("dataforBack"));
      // let checkBox = document.getElementById('PrePoDetail') as HTMLInputElement;
      // if(!this.cs.isUndefineORNull(this.quoteReq.PreviousPolicyDetails)){
      //   checkBox.checked = true;
      // }else{
      //   checkBox.checked = false;
      // }

    } else if (JSON.parse(localStorage.getItem("calQuoteTPRes"))) {
      this.quoteReq = JSON.parse(localStorage.getItem("calQuoteTPReq"));
      this.quoteReqrem = JSON.parse(localStorage.getItem("dataforBack"));
      this.cubiccapacity = this.quoteReq.VehicleCC;
    }

    if (this.quoteReq != null && this.quoteReqrem != null) {
      this.backData();
    } else {
      console.log("No Data Found");
    }

    this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
    this.authtok = JSON.parse(localStorage.getItem("AuthToken"));
    this.policyType = this.paramDataValues.iPartnerLogin.policy;
    this.productCode = this.paramDataValues.iPartnerLogin.product;
    this.allRisk_productCode = commonData.allriskProductCode;
    if (this.policyType == "NEW") {
      this.type = "New";
      this.colSize = 6;
    } else {
      this.type = "Rollover";
      this.colSize = 2;
    }
    if (this.policyType == "NEW") {
      this.type = "New";
      this.colSize1 = 6;
    } else {
      this.type = "Rollover";
      this.colSize1 = 4;
    }
    if (this.policyType == "NEW") {
      this.type = "New";
      this.colSize2 = 6;
    } else {
      this.type = "Rollover";
      this.colSize2 = 3;
    }
    if (this.productCode == "2312") {
      this.product = "Two Wheeler";
    } else if (this.productCode == "2311") {
      this.product = "Four Wheeler";
    } else if (this.productCode == "2320") {
      this.product = "Two Wheeler Liability";
    } else if (this.productCode == "2319") {
      this.product = "Four Wheeler Liability";
    } else if (this.productCode == commonData.allriskProductCode) {
      this.product = "Risk Electric Bike";
      if (this.policyType == "NEW") {
        this.quoteDetails.TransactionType = "New";
      } else {
        this.quoteDetails.TransactionType = "Rollover";
      }
    }
    if (
      this.policyType == "NEW" &&
      JSON.parse(localStorage.getItem("calQuoteReq")) == undefined &&
      JSON.parse(localStorage.getItem("calQuoteTPRes")) == undefined
    ) {
      this.quoteDetails.RegistrationNumber = "NEW";
    }
    if (this.policyType == "NEW") {
      this.quoteDetails.RegistrationNumber = "NEW";
      this.openQuoteSection = true;
    }

    if (this.productCode == "2312" || this.productCode == "2320") {
      this.classCode = "37";
    } else if (this.productCode == "2311" || this.productCode == "2319") {
      this.classCode = "45";
    }

    if (this.productCode == "2312") {
      this.quoteDetails.VoluntaryDeductiblePlanName = "3000";
      this.quoteDetails.RSAPlanName = "TW-299";
      this.quoteDetails.ZeroDepPlanName = commonData.ZD;
    }
    if (this.productCode == "2311") {
      this.quoteDetails.LossOfPersonalBelongingPlanName = "PLAN A";
      this.quoteDetails.KeyProtectPlan = commonData.KeyProtectPlan;
      this.quoteDetails.garageCashPlanName = "MBCON";
      this.quoteDetails.VoluntaryDeductiblePlanName = "3500";
      this.quoteDetails.RSAPlanName = "RSA-Standard";
      this.quoteDetails.ZeroDepPlanName = "ZD";
    }
    this.claimAdd = "claimyes";
    if (this.productCode == "2311" || this.productCode == "2319") {
      this.TPPDLimit = 750000;
    } else {
      this.TPPDLimit = 100000;
    }
    if (this.productCode == commonData.allriskProductCode) {
      this.lowerLimit = 100;
      this.upperLimit = 150000;
      localStorage.setItem("IsSCPA", this.isSCPA.toString());
      // Creating a bundle ID 
      let newBundleID: any;
      newBundleID = Guid.create();
      this.bundleID = newBundleID.value;
      //console.info("BUNDLE ID", this.bundleID.value);
    }

    (<any>window).ga("send", "event", {
      eventCategory: "Product",
      eventLabel: "" + this.product,
      eventAction: "Product Selected",
      eventValue: 10,
    });

    (<any>window).ga("send", "event", {
      eventCategory: "Policy Type",
      eventLabel: "" + this.policyType,
      eventAction: "Policy Type",
      eventValue: 10,
    });

    this.createBizToken();

    this.setTab();

    if (this.IsPOSTransaction_flag == true) {
      let landingPage = JSON.parse(localStorage.getItem("landingPage"));
      this.cs.digitalPOSUserTabChange(landingPage);
    }

    if (this.productCode == "2320" || this.productCode == "2319") {
      this.polmminDOB = new Date();
      this.polmminDOB.setDate(this.polmminDOB.getDate() + 1);
    } else {
      this.polmminDOB = new Date();
      this.polmminDOB.setDate(this.polmminDOB.getDate());
    }

    if (
      (this.productCode == "2320" || this.productCode == "2319") &&
      this.policyType == "NEW"
    ) {
      this.memmaxDOB = new Date();
      this.memmaxDOB.setDate(this.memmaxDOB.getDate() + 30);
      this.memminDOB = new Date();
      this.memminDOB.setDate(this.memminDOB.getDate() - 179);
    } else {
      // Maxdate logic
      this.memmaxDOB = new Date();
      this.memmaxDOB.setDate(this.memmaxDOB.getDate() + 0);
    }

    if (
      this.policyType != "NEW" &&
      this.productCode != "2320" &&
      this.productCode != "2319" &&
      this.productCode != commonData.allriskProductCode
    ) {
      this.getInsurerName();
    }

    this.quoteDetails.PACoverTenure = "1";
    if (this.policyType == "NEW") {
      if (this.productCode == "2312") {
        this.quoteDetails.Tenure = "1";
        this.quoteDetails.TPTenure = "5";
      } else {
        this.quoteDetails.Tenure = "1";
        this.quoteDetails.TPTenure = "3";
      }
      let currentYear = new Date().getFullYear();
      let startYear = currentYear - 1;
      for (let i = startYear; i <= currentYear; i++) {
        this.years.push(startYear.toString());
        startYear++;
      }
    } else {
      this.quoteDetails.Tenure = "1";
      this.quoteDetails.TPTenure = "1";
      let currentYear = new Date().getFullYear();
      let startYear = currentYear - 24;
      for (let i = startYear; i <= currentYear; i++) {
        this.years.push(startYear.toString());
        startYear++;
      }
    }

    // this.cs.encrypt('test@gmail.com',commonData.aesSecretKey);

    let startDate = moment(new Date()).format("YYYY-MM-DD");
    this.TodayDate = moment(startDate).format("DD-MM-YYYY");

    if (this.productCode == "2320" || this.productCode == "2319") {
      let startDate = moment(new Date()).add(1, "days").format("YYYY-MM-DD");
      this.quoteDetails.PolicyStartDate = startDate;
    } else if (
      (this.productCode == "2312" || this.productCode == "2311") &&
      this.policyType != "NEW"
    ) {
      let startDate = moment(new Date()).add(1, "days").format("YYYY-MM-DD");
      this.quoteDetails.PolicyStartDate = startDate;
    } else {
      let startDate = moment(new Date()).format("YYYY-MM-DD");
      this.TodayDate = moment(startDate).format("DD-MM-YYYY");
      if (this.quoteReqrem != undefined || this.quoteReqrem != null) {
        this.quoteDetails.PolicyStartDate = this.quoteReq.PolicyStartDate;
      } else {
        this.quoteDetails.PolicyStartDate = startDate;
      }
    }

    if (this.policyType == "NEW") {
      this.quoteDetails.FirstRegistrationDate = startDate;
      let currentyear = moment(this.quoteDetails.PolicyStartDate).format(
        "YYYY"
      );
      this.quoteDetails.ManufacturingYear = currentyear;
    }
    let currentyear = moment(this.quoteDetails.PolicyStartDate).format("YYYY");
    let leapcheck = moment(currentyear).isLeapYear();
    if (this.productCode == "2320" && this.policyType == "NEW") {
      this.policyEndDate = moment(this.quoteDetails.PolicyStartDate)
        .add(5, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    } else if (this.productCode == "2319" && this.policyType == "NEW") {
      this.policyEndDate = moment(this.quoteDetails.PolicyStartDate)
        .add(3, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    } else if (this.productCode == "2312") {
      this.quoteDetails.PolicyEndDate = moment(this.quoteDetails.PolicyStartDate)
        .add(1, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
      this.policyEndDate = this.quoteDetails.PolicyEndDate;
    } else if (this.productCode == commonData.allriskProductCode) {
      this.quoteDetails.PolicyEndDate = moment(this.quoteDetails.PolicyStartDate)
        .add(1, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    } else {
      this.quoteDetails.PolicyEndDate = moment(this.quoteDetails.PolicyStartDate)
        .add(1, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
      this.policyEndDate = this.quoteDetails.PolicyEndDate;
    }

    if (localStorage.getItem("getVehiclDetalsClicked")) {
      this.getVehicleDetailsClicked = JSON.parse(
        localStorage.getItem("getVehiclDetalsClicked")
      );
    }
    console.clear();

    // for prod
    // this.cs.getIpAddress().subscribe((data) => {
    //   this.cs.getIpdetails(data).then(() => {
    //     this.cs.getlocationdetails().subscribe((res) => {
    //       this.cs.loaderStatus = false;
    //       this.locationDetails = res;
    //       this.todaysDataTime = formatDate(
    //         this.today,
    //         "dd-MM-yyyy hh:mm:ss a",
    //         "en-US",
    //         "+0530"
    //       );
    //       /**
    //        * digitalPOS Change :- restricting the api call for POS transaction
    //        */
    // if (this.IsPOSTransaction_flag == false) {
    //   this.getUserMachineDetails();
    // }
    //     });
    //   });
    // });

    /**
     * Digital POS change
     * call getDeal and getDealDetails function only for nysa not for POS after timemout
     * author :- digitalPOS
     * date :- 29-07-2021
     */
    if (this.IsPOSTransaction_flag == true) {
      setTimeout(() => {
        this.getPOSDeal_ID();
        this.getDigitalPOSPaymsToken();
      }, 1000);
    } else {
      this.getDeal()
        .then(() => {
          if (this.dealid == null) {
            swal({ text: "Deal not found", closeOnClickOutside: false });
          }
        })
        .then(() => {
          this.getDealDetails();
        });
    }

    let scpaData = JSON.parse(localStorage.getItem('scpaData'));
    this.showSCPAinNysa = scpaData.isSCPAAccess;
  }

  ngAfterContentInit() {
    console.log("ContentINIt");
    let regNo = JSON.parse(localStorage.getItem("regNumber"));
    if(!this.cs.isUndefineORNull(regNo)){
      this.quoteDetails.RegistrationNumber = regNo;
      this.calledVahaanAPI();
    }
  }

  // Check crawler enable or not
  checkCrawler() {
    // //CreateToken + GetApplicationAccess merging code - Sejal
    if (!this.cs.isUndefineORNull(localStorage.getItem("AuthToken"))) {
      let encryptedTokenaccess = JSON.parse(localStorage.getItem("AuthToken"));
      let Parsedtokenaccess: any = this.api.decrypt(
        encryptedTokenaccess.applicationAccess
      );
      let tokenaccess = JSON.parse(Parsedtokenaccess);
      console.log("tokenaccess", tokenaccess, tokenaccess.isEnableCrwaler)
      this.isCrawlerEnable = tokenaccess.isEnableCrwaler;
      // this.isCrawlerEnable = true;
      this.isEnableCarInfoNewService = tokenaccess.isEnableCarInfoNewService;
      this.cs.isPlutusEnable = tokenaccess.isEnablePlutus;
      this.isSiteDown = tokenaccess.isSiteDown;
    }
    // this.cs.get("Access/GetApplicationAccess").subscribe((res: any) => {
    //   console.log("Response", res);
    //   localStorage.setItem("AccessFor", JSON.stringify(res));
    //   this.isCrawlerEnable = res.isEnableCrwaler;
    //   this.isEnableCarInfoNewService = res.isEnableCarInfoNewService;
    //   this.cs.isPlutusEnable = res.isEnablePlutus;
    //   this.isSiteDown = res.isSiteDown;
    // });
  }

  getDigitalPOSPaymsToken() {
    this.POSmobileNumber = JSON.parse(localStorage.getItem("POSmobileNumber"));
    this.cs.encryptCustomerData(this.POSmobileNumber).then((res: any) => {
      this.cs.getPOSPaymsToken(res).then((res: any) => {
        localStorage.setItem("accessTokenPOS", JSON.stringify(res.accessToken));
      });
    });
  }

  /**
   * Digital POS change
   * call getDeal and getDealDetails function only for nysa not for POS
   * author :- digitalPOS
   * date :- 29-07-2021
   */
  getPOSDeal_ID() {
    if (this.IsPOSTransaction_flag == true) {
      this.POSmobileNumber = JSON.parse(
        localStorage.getItem("POSmobileNumber")
      );
      this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
      this.productCode = this.paramDataValues.iPartnerLogin.product;
      let PosDealObj = {
        MobileNumber: JSON.parse(localStorage.getItem("POSmobileNumber")),
        SubProductCode: this.productCode,
      };
      this.cs.GetPOSDealId(JSON.stringify(PosDealObj)).then((res: any) => {
        if (res.isSuccess == true) {
          this.dealid = res.dealID;
          localStorage.setItem("DealID", JSON.stringify(res.dealID));
        } else {
          swal({ text: "Deal not found" });
        }
      });
    } else {
    }
  }

  getUserMachineDetails() {
    let body = {
      IP: this.locationDetails.ip,
      IPtype: this.locationDetails.type,
      browser: this.cs.myBrowser(),
      browserVersion: this.cs.getBrowserVersion(),
      isp: this.locationDetails.isp,
      org: this.locationDetails.org,
      latitude: this.locationDetails.latitude,
      longitude: this.locationDetails.longitude,
      continent: this.locationDetails.continent,
      country: this.locationDetails.country,
      region: this.locationDetails.region,
      city: this.locationDetails.city,
      currency_code: this.locationDetails.currency_code,
      currency_rates: this.locationDetails.currency_rates,
      timezone: this.locationDetails.timezone,
      timezone_gmt: this.locationDetails.timezone_gmt,
      country_capital: this.locationDetails.country_capital,
      currentDateTime: this.todaysDataTime,
      dealId: this.dealid,
      bankID: this.dealDetailsfromIM.bankID,
      bankType: this.dealDetailsfromIM.bankType,
      dealStartDate: this.dealDetailsfromIM.startDate,
      dealEndDate: this.dealDetailsfromIM.endDate,
      imid: this.dealDetailsfromIM.imid,
      vertical: this.dealDetailsfromIM.fullVerticalDesc,
      role: this.dealDetailsfromIM.role,
    };
    let str = JSON.stringify(body);
    // console.log(str);
  }

  beforePanelClosed(panel) {
    panel.isExpanded = false;
  }
  beforePanelOpened(panel) {
    panel.isExpanded = true;
  }

  // showspecificmessage(){
  //   const todaydate = moment().format('MMMM D YYYY, HH:mm');
  //   const nextdate = moment().format('MMMM 24 2021, 07:59');
  //   if(todaydate < nextdate){
  //       this.showmessage = true;
  //     }
  // }

  setTab() {
    let fromNysaPolicy = localStorage.getItem("myNysaPolicy");
    // let myBreakinPolicy = localStorage.getItem('myBreakinPolicy');
    if (fromNysaPolicy == "true") {
      $("#qQuoteMenu").removeClass("active");
      $("#penPayMenu").removeClass("active");
      $("#saveQoteMenu").removeClass("active");
      $("#myBreakinMenu").removeClass("active");
      $("#qcCasesMenu").removeClass("active");
      $("#nysaPloMenu").addClass("active");

      $(".tab-content #qQote").removeClass("active show");
      $(".tab-content #nysaPlo").addClass("active show");
      $(".tab-content #penPay").removeClass("active show");
      $(".tab-content #saveQote").removeClass("active show");
      $(".tab-content #myBreakin").removeClass("active show");
      $(".tab-content #qcCasesMenu").removeClass("active show");
    } else if (this.myBreakinPolicyTab == true) {
      $("#qQuoteMenu").removeClass("active");
      $("#penPayMenu").removeClass("active");
      $("#saveQoteMenu").removeClass("active");
      $("#myBreakinMenu").addClass("active");
      $("#nysaPloMenu").removeClass("active");
      $("#qcCasesMenu").removeClass("active");

      $(".tab-content #qQote").removeClass("active show");
      $(".tab-content #nysaPlo").removeClass("active show");
      $(".tab-content #penPay").removeClass("active show");
      $(".tab-content #saveQote").removeClass("active show");
      $(".tab-content #myBreakin").addClass("active show");
      $(".tab-content #qcCasesMenu").removeClass("active show");
    }
  }

  getChangedEvent(ev: any) {
    //check plutus enabled or not
    this.cs.get("PaymsPayment/IsPaymentModuleNew").subscribe((res: any) => {
      if (res == false) {
        this.myBreakinPolicyTab = ev;
        this.setTab();
        this.panelOpenStateProposal = false;
        this.panelOpenStatePremium = false;
        this.panelOpenState = true;
      } else {
        this.panelOpenStateProposal = false;
        this.panelOpenStatePremium = false;
        this.panelOpenState = true;
        this.showAddon = false;
      }
    });
  }

  refreshToken(ev: any, ev1: any): Promise<any> {
    return new Promise((resolve: any) => {
      this.api.refreshToken().subscribe(
        (res: any) => {
          if (ev == "new" && this.productCode == "2311") {
            this.policy = "NEW";
          }
          if (ev == "roll" && this.productCode == "2311") {
            this.policy = "ROLL";
          }
          if (ev == "used" && this.productCode == "2311") {
            this.policy = "USED";
          }
          if (ev == "new" && this.productCode == "2312") {
            this.policy = "NEW";
          }
          if (ev == "roll" && this.productCode == "2312") {
            this.policy = "ROLL";
          }
          if (ev == "new" && this.productCode == "2320") {
            this.policy = "NEW";
          }
          if (ev == "roll" && this.productCode == "2320") {
            this.policy = "ROLL";
          }
          if (ev == "new" && this.productCode == "2319") {
            this.policy = "NEW";
          }
          if (ev == "roll" && this.productCode == "2319") {
            this.policy = "ROLL";
          }
          if (ev == "New" && this.productCode == commonData.allriskProductCode) {
            this.policy = "NEW";
          }
          if (ev == "Rollover" && this.productCode == commonData.allriskProductCode) {
            this.policy = "ROLL";
          }

          let Username,
            Password,
            Expiry,
            Signature,
            iPartner_Token,
            view,
            isSubagent,
            subAID;
          let existingtoken = JSON.parse(localStorage.getItem("AuthToken"));

          if (!this.cs.isUndefineORNull(res)) {
            // let body1 = {
            //   expiry: res.expiry,
            //   token: res.token,
            //   username: existingtoken.username,
            // };
            // //CreateToken + GetApplicationAccess merging code - Sejal
            let body1 = {
              expiry: res.expiry,
              token: res.token,
              username: existingtoken.username,
              applicationAccess: existingtoken.applicationAccess,
            };
            localStorage.setItem("AuthToken", JSON.stringify(body1));
            let paramData = JSON.parse(localStorage.getItem("paramData"));
            paramData.iPartnerLogin.policy = this.policy;
            paramData.iPartnerLogin.product = this.product;
            Username = paramData.Username;
            Password = paramData.Password;
            Expiry = paramData.iPartnerLogin.Expiry;
            Signature = paramData.iPartnerLogin.Signature;
            iPartner_Token = paramData.iPartnerLogin.iPartner_Token;
            view = paramData.iPartnerLogin.view;
            isSubagent = paramData.iPartnerLogin.isSubagent;
            subAID = paramData.iPartnerLogin.subAID;
            let body;
            body = {
              Username: Username,
              Password: Password,
              iPartnerLogin: {
                Expiry: Expiry,
                Signature: Signature,
                iPartner_Token: iPartner_Token,
                product: this.productCode,
                policy: this.policy,
                view: view
              },
            };
            if (isSubagent == "Y") {
              body.iPartnerLogin.isSubagent = isSubagent;
              body.iPartnerLogin.subAID = subAID;
            }
            localStorage.setItem("paramData", JSON.stringify(body));
            if (ev1 == "RTO") {
              this.router.navigate(["/quote"]);
            } else {
              this.router
                .navigateByUrl("/", { skipLocationChange: true })
                .then(() => this.router.navigate(["/quote"]));
            }
            this.cs.data$.next({
              flag: true,
            });
          }
          resolve();
        },
        (err) => {
          this.cs.loaderStatus = false;
        }
      );
    });
  }

  // Restore Data for back button
  backData() {
    this.backRTO(this.quoteReq.RTOLocationCode);
    this.quoteDetails.RegistrationNumber = this.quoteReq.RegistrationNumber;
    this.getInsurerName();
    this.openQuoteSection = true;
    this.quoteDetails.GSTToState = this.quoteReq.GSTToState;
    this.quoteDetails.FirstRegistrationDate =
      this.quoteReq.FirstRegistrationDate;
    this.quoteDetails.PolicyStartDate = this.quoteReq.PolicyStartDate;
    this.quoteDetails.ManufacturingYear = this.quoteReq.ManufacturingYear;
    this.quoteDetails.PACoverTenure = this.quoteReq.PACoverTenure;
    let checkBox = document.getElementById('PrePoDetail') as HTMLInputElement;
    console.log(this.quoteReq, checkBox);
    if (!this.cs.isUndefineORNull(checkBox)) {
      if (!this.cs.isUndefineORNull(this.quoteReq.PreviousPolicyDetails)) {
        checkBox.checked = true;
      } else {
        checkBox.checked = false;
      }
    }

    this.backManuModel();
    if (
      (this.quoteReqrem.productCode == "2311" ||
        this.quoteReqrem.productCode == "2312") &&
      this.quoteReqrem.policyType == "NEW"
    ) {
      if (this.quoteReq.PreviousPolicyDetails != undefined) {
        this.showPrev = true;
        this.quoteDetails.ncbChecked = "true";
        this.quoteDetails.PreviousPolicyEndDate =
          this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate;
        this.quoteDetails.PreviousVehicleSaleDate =
          this.quoteReq.PreviousPolicyDetails.PreviousVehicleSaleDate;
        this.quoteDetails.PreviousInsurerName =
          this.quoteReqrem.PreviousInsurerName;
        this.quoteDetails.PreviousPolicyNumber =
          this.quoteReq.PreviousPolicyDetails.PreviousPolicyNumber;

        let prevPolStartDate = moment(this.quoteDetails.PreviousPolicyEndDate)
          .subtract(1, "years")
          .add(1, "days")
          .format("YYYY-MM-DD");
        this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
        let prevStartDate = moment(
          this.quoteDetails.PreviousPolicyStartDate
        ).format("DD-MMM-YYYY");
        let prevEndDate = moment(
          this.quoteDetails.PreviousPolicyEndDate
        ).format("DD-MMM-YYYY");
        this.api
          .calculatePrevTenure(prevStartDate, prevEndDate)
          .subscribe((res: any) => {
            this.previousPolicyTen = res;
            this.quoteDetails.PreviousPolicyTenure = this.previousPolicyTen;
          });
      } else {
        this.showPrev = false;
        this.quoteDetails.ncbChecked = "";
        this.claimAdd == "claimno";
      }
    }

    if (this.policyType != "NEW") {
      if (this.quoteReq.PreviousPolicyDetails != undefined) {
        this.quoteDetails.PreviousPolicyEndDate =
          this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate;
      }
      if (this.quoteReqrem.showsaod == true) {
        console.log("SAOD", this.quoteReq);
        this.showsaod = true;
        this.isSCPARequired = true;
        this.quoteDetails.TPTenure = this.quoteReq.TPTenure;
        this.quoteDetails.Tenure = this.quoteReq.Tenure;
        this.quoteDetails.PreviousPolicyType = "Bundled Package Policy";
        this.quoteDetails.TPStartDate = this.quoteReq.TPStartDate;
        this.quoteDetails.TPEndDate = this.quoteReq.TPEndDate;
        this.quoteDetails.TPPolicyNo = this.quoteReq.TPPolicyNo;
      }
    }
    this.manufacname = localStorage.getItem("ManufactureName");
  }

  backRTO(ev: any) {
    this.api
      .getRTOLocation(ev, this.quoteReqrem.classCode)
      .subscribe((res: any) => {
        this.rtoName = res;
        this.quoteDetails.rtO_LOCATION_DESC = this.rtoName;
        this.rtoCode = ev;
      });
  }

  backManuModel() {
    this.api
      .getModelName(this.quoteReq.VehicleModelCode, this.quoteReqrem.classCode)
      .subscribe((res: any) => {
        this.ManumodelName = res.vehicleName;
        this.quoteDetails.vehiclemodel = this.ManumodelName;
        this.model = this.ManumodelName;
        this.cubiccapacity = res.cubicCapacity;
        this.seatingcapacity = this.quoteReqrem.seatingcapacity;
        this.makecode = this.quoteReq.VehicleMakeCode;
        this.modelcode = this.quoteReq.VehicleModelCode;
        this.modelName = this.quoteReqrem.manufacturer;
        this.findexshowroom();
        if (
          this.quoteDetails.RegistrationNumber != "" &&
          this.quoteDetails.rtO_LOCATION_DESC != "" &&
          this.quoteDetails.vehiclemodel != "" &&
          this.quoteDetails.FirstRegistrationDate != "" &&
          this.quoteDetails.PolicyStartDate != "" &&
          this.quoteDetails.GSTToState != ""
        ) {
          this.showIDV = true;
        }
        this.manufacname = localStorage.getItem("ManufactureName");
      });
  }

  bindyear(ev: any) {
    if (this.productCode == commonData.allriskProductCode) {
      if (typeof this.quoteDetails.PurchaseDate == "object") {
        this.quoteDetails.PurchaseDate = moment(ev._d).format(
          "YYYY-MM-DD"
        );
      }
      this.vehicleAge = moment().diff(
        this.quoteDetails.PurchaseDate,
        "years"
      );
      this.showAddon = false;
      this.systemQC = false;
      this.panelOpenStateProposal = false;
      this.showAddon1 = false;
      if (this.policyType == "NEW") {
        let regisdate: any;
        regisdate = moment(this.quoteDetails.PurchaseDate);
        let date1 = moment(new Date());
        let diff = date1.diff(regisdate, "days");
        if (diff > 365) {
          swal({
            text: "Registration of Vehicle provided cannot be more than one year prior to working date for new vehicles",
          }).then(() => {
            let startDate = moment(new Date()).format("YYYY-MM-DD");
            this.quoteDetails.PurchaseDate = startDate;
            let currentyear = moment(this.quoteDetails.PolicyStartDate).format(
              "YYYY"
            );
            this.quoteDetails.ManufacturingYear = currentyear;
          });
        }
        // if (
        //   this.quoteDetails.RegistrationNumber != "" &&
        //   this.quoteDetails.rtO_LOCATION_DESC != "" &&
        //   this.quoteDetails.vehiclemodel != "" &&
        //   this.quoteDetails.FirstRegistrationDate != ""
        // ) {
        //   setTimeout(() => {
        //     this.finddataforquote().then(() => {
        //       this.findexshowroom();
        //     });
        //   }, 1000);
        // }
      } else {
        let regisdate: any;
        regisdate = moment(this.quoteDetails.PurchaseDate);
        let date1 = moment(new Date());
        let diff = date1.diff(regisdate, "days");
        if (diff == 0) {
          swal({
            text: "Registration date cannot be same as today’s date",
          }).then(() => {
            this.quoteDetails.PurchaseDate = "";
          });
        }
        // if (
        //   this.quoteDetails.RegistrationNumber != "" &&
        //   this.quoteDetails.rtO_LOCATION_DESC != "" &&
        //   this.quoteDetails.vehiclemodel != "" &&
        //   this.quoteDetails.FirstRegistrationDate != ""
        // ) {
        //   setTimeout(() => {
        //     this.finddataforquote().then(() => {
        //       this.findexshowroom();
        //     });
        //   }, 1000);
        // }
      }
      let year = moment(ev).format("YYYY");
      this.quoteDetails.ManufacturingYear = year;
    } else {
      if (typeof this.quoteDetails.FirstRegistrationDate == "object") {
        this.quoteDetails.FirstRegistrationDate = moment(ev._d).format(
          "YYYY-MM-DD"
        );
      }
      this.vehicleAge = moment().diff(
        this.quoteDetails.FirstRegistrationDate,
        "years"
      );
      this.showAddon = false;
      this.systemQC = false;
      this.panelOpenStateProposal = false;
      this.showAddon1 = false;
      if (this.policyType == "NEW") {
        let regisdate: any;
        regisdate = moment(this.quoteDetails.FirstRegistrationDate);
        let date1 = moment(new Date());
        let diff = date1.diff(regisdate, "days");
        if (diff > 365) {
          swal({
            text: "Registration of Vehicle provided cannot be more than one year prior to working date for new vehicles",
          }).then(() => {
            let startDate = moment(new Date()).format("YYYY-MM-DD");
            this.quoteDetails.FirstRegistrationDate = startDate;
            let currentyear = moment(this.quoteDetails.PolicyStartDate).format(
              "YYYY"
            );
            this.quoteDetails.ManufacturingYear = currentyear;
          });
        }
        if (
          this.quoteDetails.RegistrationNumber != "" &&
          this.quoteDetails.rtO_LOCATION_DESC != "" &&
          this.quoteDetails.vehiclemodel != "" &&
          this.quoteDetails.FirstRegistrationDate != ""
        ) {
          setTimeout(() => {
            this.finddataforquote().then(() => {
              this.findexshowroom();
            });
          }, 1000);
        }
      } else {
        let regisdate: any;
        regisdate = moment(this.quoteDetails.FirstRegistrationDate);
        let date1 = moment(new Date());
        let diff = date1.diff(regisdate, "days");
        if (diff == 0) {
          swal({
            text: "Registration date cannot be same as today’s date",
          }).then(() => {
            this.quoteDetails.FirstRegistrationDate = "";
          });
        }
        if (
          this.quoteDetails.RegistrationNumber != "" &&
          this.quoteDetails.rtO_LOCATION_DESC != "" &&
          this.quoteDetails.vehiclemodel != "" &&
          this.quoteDetails.FirstRegistrationDate != ""
        ) {
          setTimeout(() => {
            this.finddataforquote().then(() => {
              this.findexshowroom();
            });
          }, 1000);
        }
      }
      let year = moment(ev).format("YYYY");
      this.quoteDetails.ManufacturingYear = year;
    }
  }

  // Add Previous Detail hide/show flag
  click(ev: any) {
    let data = JSON.parse(localStorage.getItem('calQuoteReq'));
    // localStorage.setItem('PreviousData', JSON.stringify(data));
    if (
      this.quoteDetails.TotalNoOfODClaims != "0" &&
      !this.cs.isUndefineORNull(this.quoteDetails.TotalNoOfODClaims)
    ) {
      this.quoteDetails.claims = "claimyes";
      this.ClaimOnPreviousPolicy = true;
    } else {
      this.quoteDetails.claims = "claimno";
      this.ClaimOnPreviousPolicy = false;
    }
    this.getrtoButtonclicked = false;
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    
    if (ev.target.checked) {
      this.quoteDetails.PreviousPolicyDetails = true;
      this.fillprev = true;
      this.previousPolicyValue = true;
      this.quoteDetails.NoOfClaimsOnPreviousPolicy = "0";
      this.quoteDetails.PreviousPolicyType = "Comprehensive Package";
      this.showPYP = true;
      if (!this.cs.isUndefineORNull(data)) {
        data.isNoPrevInsurance = !ev.target.checked;
        data = JSON.parse(localStorage.getItem('PreviousData'));
        localStorage.setItem('calQuoteReq', JSON.stringify(data));
      }
    } else {
      this.fillprev = false;
      this.quoteDetails.PreviousPolicyDetails = false;
      this.previousPolicyValue = false;
      this.showPYP = false;
      if (!this.cs.isUndefineORNull(data)) {
        data.isNoPrevInsurance = !ev.target.checked;
        localStorage.setItem('calQuoteReq', JSON.stringify(data));
      }
    }
    if (this.quoteDetails.PreviousPolicyDetails == true) {
      // 14-07-2022
      if(!this.cs.isUndefineORNull(this.rtoDetail)){
        this.quoteDetails.PreviousPolicyEndDate = moment(
          this.rtoDetail.insuranceUpto,
          "DD-MMM-YYYY"
        ).format("YYYY-MM-DD");
        this.getPrevEndDate(this.quoteDetails.PreviousPolicyEndDate);
      }      
    } else if (this.quoteDetails.PreviousPolicyDetails == false) {
      let startDate = moment(new Date()).add(1, "days").format("YYYY-MM-DD");
      // this.TodayDate = moment(startDate).format('DD-MM-YYYY');
      this.quoteDetails.PolicyStartDate = startDate;
      if (this.productCode == "2320" && this.policyType == "NEW") {
        this.policyEndDate = moment(this.quoteDetails.PolicyStartDate)
          .add(5, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      } else if (this.productCode == "2319" && this.policyType == "NEW") {
        this.policyEndDate = moment(this.quoteDetails.PolicyStartDate)
          .add(3, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      } else {
        this.policyEndDate = moment(this.quoteDetails.PolicyStartDate)
          .add(1, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      }
    }
    this.quoteDetails.IsExtensionCountry = false;
    // this.IsSelfInspection = false;
    this.isZeroDepPlanName = false;
    this.isRSAPlan = false;
    this.quoteDetails.IsRTIApplicableflag = false;
    this.quoteDetails.IsConsumables = false;
  }

  // Add Previous Detail hide/show flag
  clickclaim(ev: any) {
    console.log(
      ev.target.htmlFor,
      ev.target.innerHTML,
      ev.target.id,
      ev.target.defaultValue
    );
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    if (ev.target.htmlFor == "claimno" || ev.target.defaultValue == "claimno") {
      this.claimNCB = true;
      this.ClaimOnPreviousPolicy = false;
      this.quoteDetails.TotalNoOfODClaims = "0";
      this.quoteDetails.NoOfClaimsOnPreviousPolicy = "0";
    } else {
      this.claimNCB = false;
      this.quoteDetails.NoOfClaimsOnPreviousPolicy = "1";
      this.quoteDetails.TotalNoOfODClaims = "1";
      this.ClaimOnPreviousPolicy = true;
    }
  }

  clickclaimNCB(ev: any) {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.claimAdd = ev.target.value;
    if (
      ev.target.value == "claimno" ||
      this.cs.isUndefineORNull(ev.target.value)
    ) {
      this.isTransfer = true;
      this.claimNCBT = true;
      this.ClaimOnPreviousPolicy = false;
      this.quoteDetails.TotalNoOfODClaims = "0";
    } else {
      this.claimNCBT = false;
      this.isTransfer = false;
      this.quoteDetails.NoOfClaimsOnPreviousPolicy = "1";
      this.ClaimOnPreviousPolicy = true;
      this.quoteDetails.TotalNoOfODClaims = "1";
    }
  }

  // Additional cover hide/show flag
  clickAdditional(ev: any) {
    if (ev.target.checked) {
      this.additional = true;
    } else {
      this.additional = false;
    }
  }

  changeNoofClaims(ev: any) {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;
    if (!this.claimNCB) {
      this.quoteDetails.NoOfClaimsOnPreviousPolicy = this.quoteDetails
        .TotalNoOfODClaims
        ? this.quoteDetails.TotalNoOfODClaims
        : "1";
      this.quoteDetails.TotalNoOfODClaims = this.quoteDetails.TotalNoOfODClaims
        ? this.quoteDetails.TotalNoOfODClaims
        : "1";
      this.ClaimOnPreviousPolicy = true;
    } else {
      this.quoteDetails.NoOfClaimsOnPreviousPolicy = "0";
      this.quoteDetails.TotalNoOfODClaims = "0";
      this.ClaimOnPreviousPolicy = false;
    }
  }

  // OD Logic for Tenures
  ODType(ev: any) {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    if (ev.target.id == "1od") {
      this.quoteDetails.Tenure = "1";
      this.quoteDetails.TPTenure = "5";
    } else if (ev.target.id == "5od") {
      this.quoteDetails.Tenure = "5";
      this.quoteDetails.TPTenure = "5";
    }
  }

  ODType4w(ev: any) {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    if (ev.target.id == "4w1od") {
      this.quoteDetails.Tenure = "1";
      this.quoteDetails.TPTenure = "3";
    } else if (ev.target.id == "4w3od") {
      this.quoteDetails.Tenure = "3";
      this.quoteDetails.TPTenure = "3";
    }
  }
  // Prev Policy Type
  prevPolType(ev: any) {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    if (ev.target.id == "comprehensive") {
      this.rollpolicyType = "Comprehensive";
      this.quoteDetails.PreviousPolicyType = "Comprehensive Package";
      this.claimNCB = true;
      this.ClaimOnPreviousPolicy = false;
      this.quoteDetails.TotalNoOfODClaims = "0";
    } else {
      this.rollpolicyType = "Bundled";
      this.quoteDetails.PreviousPolicyType = "TP";
      this.claimNCB = false;
      this.quoteDetails.NoOfClaimsOnPreviousPolicy = "0";
      this.ClaimOnPreviousPolicy = true;
    }
  }
  // Previous Policy Date

  getPrevStartDate(ev: any) {
    let prevPolEndDate = moment(this.quoteDetails.PreviousPolicyStartDate)
      .add(1, "years")
      .subtract(1, "days")
      .format("YYYY-MM-DD");
    this.quoteDetails.PreviousPolicyEndDate = prevPolEndDate;
    let prevStartDate = moment(
      this.quoteDetails.PreviousPolicyStartDate
    ).format("DD-MMM-YYYY");
    let prevEndDate = moment(this.quoteDetails.PreviousPolicyEndDate).format(
      "DD-MMM-YYYY"
    );
    this.api
      .calculatePrevTenure(prevStartDate, prevEndDate)
      .subscribe((res: any) => {
        this.previousPolicyTen = res;
        this.quoteDetails.PreviousPolicyTenure = this.previousPolicyTen;
      });
  }

  changeprev(ev: any) {
    this.prevtenure = ev;
    if (this.prevtenure == 1) {
      let prevPolStartDate = moment(this.quoteDetails.PreviousPolicyEndDate)
        .subtract(1, "years")
        .add(1, "days")
        .format("YYYY-MM-DD");
      this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
    } else if (this.prevtenure == 2) {
      let prevPolStartDate = moment(this.quoteDetails.PreviousPolicyEndDate)
        .subtract(2, "years")
        .add(1, "days")
        .format("YYYY-MM-DD");
      this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
    } else if (this.prevtenure == 3) {
      let prevPolStartDate = moment(this.quoteDetails.PreviousPolicyEndDate)
        .subtract(3, "years")
        .add(1, "days")
        .format("YYYY-MM-DD");
      this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
    }
    // let prevStartDate = moment(this.quoteDetails.PreviousPolicyStartDate).format('DD-MMM-YYYY');
    let prevStartDate = moment(
      this.quoteDetails.PreviousPolicyStartDate
    ).format("YYYY-MM-DD");
    let prevEndDate = moment(this.quoteDetails.PreviousPolicyEndDate).format(
      "YYYY-MM-DD"
    );
    this.cs
      .get(
        "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
        prevStartDate +
        "&prevEndDate=" +
        prevEndDate
      )
      .subscribe((res: any) => {
        this.previousPolicyTen = res;
        // this.quoteDetails.PreviousPolicyTenure = this.previousPolicyTen;
      });
  }

  // All Risk Electric Bike - Sejal
  getPolicyEndDate(ev: any) {
    this.quoteDetails.PolicyEndDate = moment(this.quoteDetails.PolicyStartDate)
      .add(1, "years")
      .subtract(1, "days")
      .format("YYYY-MM-DD");
  }

  getPrevEndDate(ev: any) {
    this.isZeroDepPlanName = false;
    this.isRSAPlan = false;
    this.quoteDetails.IsRTIApplicableflag = false;
    this.quoteDetails.IsConsumables = false;
    // let policystartdate: any = moment(this.quoteDetails.PreviousPolicyEndDate)
    //   .add(1, "days")
    //   .format("YYYY-MM-DD");
    let startDate: any = moment(new Date());
    let policyenddate: any = moment(
      new Date(this.quoteDetails.PreviousPolicyEndDate)
    );
    let diff = startDate.diff(policyenddate, "days");
    if (diff <= 0) {
      let policystartdate: any = moment(this.quoteDetails.PreviousPolicyEndDate)
        .add(1, "days")
        .format("YYYY-MM-DD");
      this.quoteDetails.PolicyStartDate = policystartdate;
      this.claimdiv = true;
      this.policyEndDate = moment(this.quoteDetails.PolicyStartDate)
        .add(1, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    } else if (diff > 0) {
      console.log(this.productCode, moment(new Date()).add(1, "days").format("YYYY-MM-DD"));
      let todaydate: any;
      if (this.productCode == 2319 || this.productCode == 2320) {
        todaydate = moment(new Date()).add(1, "days").format("YYYY-MM-DD");
        this.quoteDetails.PolicyStartDate = todaydate;
      } else {
        todaydate = moment(new Date()).format("YYYY-MM-DD");
        this.quoteDetails.PolicyStartDate = todaydate;
      }
      this.policyEndDate = moment(this.quoteDetails.PolicyStartDate)
        .add(1, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
      if (diff > 90) {
        this.quoteDetails.claims = "claimno";
        this.claimNCB = true;
        this.quoteDetails.NoOfClaimsOnPreviousPolicy = "0";
        this.ClaimOnPreviousPolicy = false;
      } else {
        this.claimdiv = true;
      }
    } else {
      this.changeprev(1);
    }

    if (this.prevtenure == 1) {
      this.changeprev(1);
    }
  }

  getTPEndDate(ev: any) {
    let TPpolicystartdate: any = moment(this.quoteDetails.TPEndDate)
      .add(1, "days")
      .format("YYYY-MM-DD");
    let startDate: any = moment(new Date());
    let policyenddate: any = moment(new Date(this.quoteDetails.TPEndDate));
    this.policyEndDate = moment(this.quoteDetails.TPStartDate)
      .add(1, "years")
      .subtract(1, "days")
      .format("YYYY-MM-DD");
  }

  // RegistrationType
  regType(ev: any) {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;
    if (ev.target.id == "individual") {
      this.registrationType = "INDIVIDUAL";
      if (this.productCode == commonData.allriskProductCode) {
        this.isValidSCPA = true;
        this.isSCPA = true;
      }
    } else if (ev.target.id == "corporate") {
      this.registrationType = "CORPORATE";
      if (this.productCode == commonData.allriskProductCode) {
        this.isValidSCPA = false;
        this.isSCPA = false;
      }
      
    }
    else if (ev.target.id == "partnership") {
      this.registrationType = "INDIVIDUAL";     
      if (this.productCode == commonData.allriskProductCode) {
        this.isValidSCPA = true;
        this.isSCPA = true;
      }
    }
  }

  // Tenure for rollover
  rollTenure(ev: any) {
    this.removeaddon();
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;
    if (ev.target.id == "1") {
      this.quoteDetails.Tenure = "1";
      this.quoteDetails.TPTenure = "1";
    } else if (ev.target.id == "2") {
      this.quoteDetails.Tenure = "2";
      this.quoteDetails.TPTenure = "2";
    } else if (ev.target.id == "3") {
      this.quoteDetails.Tenure = "3";
      this.quoteDetails.TPTenure = "3";
    }
  }

  // CPA Tenure 2W
  getCPA(ev: any) {
    this.removeaddon();
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;
    // Sejal - 25-02-2022 - CPA Tenure changes
    if (ev.target.value == "1year") {
      this.quoteDetails.PACoverTenure = "1";
    } else if (ev.target.value == "2year") {
      this.quoteDetails.PACoverTenure = "2";
    } else if (ev.target.value == "3year") {
      this.quoteDetails.PACoverTenure = "3";
    } else if (ev.target.value == "4year") {
      this.quoteDetails.PACoverTenure = "4";
    } else if (ev.target.value == "5year") {
      this.quoteDetails.PACoverTenure = "5";
    }
  }

  // CPA Tenure 4W
  getCPA4W(ev: any) {
    this.removeaddon();
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;
    // Sejal - 25-02-2022 - CPA Tenure changes
    if (ev.target.value == "1year4w") {
      this.quoteDetails.PACoverTenure = "1";
    } else if (ev.target.value == "2year4w") {
      this.quoteDetails.PACoverTenure = "2";
    } else if (ev.target.value == "3year4w") {
      this.quoteDetails.PACoverTenure = "3";
    }
  }

  // Get Deal
  getDeal(): Promise<void> {
    return new Promise((resolve) => {
      this.api
        .getDeal(this.authtok.username, this.productCode)
        .subscribe((res: any) => {
          this.dealDetails = res;
          this.dealid = this.dealDetails.dealID;
          localStorage.setItem("DealID", JSON.stringify(this.dealid));
          resolve();
        });
    });
  }

  //Get Deal Details from deal
  getDealDetails() {
    this.api.getDealDetails(this.dealid).subscribe((res: any) => {
      if (res.status == "FAILED") {
        swal({ text: "Deal Details not found" });
        let errorbody = {
          RequestJson: this.dealid,
          ResponseJson: JSON.stringify(res),
          ServiceURL: "Deal/GetDealDetailsFromDeal?dealID=" + this.dealid,
          CorrelationID: this.dealid,
        };
        this.api.adderrorlogs(errorbody);
      } else {
        this.dealDetailsfromIM = res;
        this.stateCD = this.dealDetailsfromIM.stateCD;
        this.bankType = this.dealDetailsfromIM.bankType;
        localStorage.setItem("bankType", this.bankType);
        localStorage.setItem("dealDetailsfromIMCode", this.dealDetailsfromIM.intermediaryCode);
        localStorage.setItem("dealStartDate", this.dealDetailsfromIM.startDate);
        localStorage.setItem("dealEndDate", this.dealDetailsfromIM.endDate);
      }
    });
  }

  // GetVehicleModel
  getVehicleModel() {
    if (this.rtoDetail.pf_model_code != null && this.manumod) {
      this.cs.loaderStatus = false;
      this.model = this.manumod.find(
        (c) => c.vehiclemodelcode == this.rtoDetail.pf_model_code
      ).vehiclemodel;
      this.quoteDetails.vehiclemodel = this.model;
    } else {
      console.log("Model not available");
    }
  }

  decCheck(ev: any) {
    if (ev.target.checked == false) {
      ev.target.checked = true;
    }
  }

  closeModal() {
    let modal = document.getElementById("premium_breakup") as HTMLInputElement;
    $("#premium_breakup").modal("hide");
    console.log("ABC", modal);
    modal.style.display = "none";
  }

  openModal() {
    console.log("recalculate", this.isRecalculate, this.isRecalculate1);
    $("#premium_breakup").modal({ backdrop: "static", keyboard: false });
    this.premid1 = document.getElementById("prem1");
    this.premid2 = document.getElementById("prem2");
    this.premid3 = document.getElementById("prem3");
    if (this.planName == "plan1" && !this.isRecalculate1) {
      console.log(this.calculatedQuote, this.quoteResp);
      // this.calculatedQuote = this.premres1;
      let antiTheftDiscount = this.calculatedQuote.riskDetails.antiTheftDiscount
        ? this.calculatedQuote.riskDetails.antiTheftDiscount
        : 0;
      let automobileAssociationDiscount = this.calculatedQuote.riskDetails
        .automobileAssociationDiscount
        ? this.calculatedQuote.riskDetails.automobileAssociationDiscount
        : 0;
      let handicappedDiscount = this.calculatedQuote.riskDetails
        .handicappedDiscount
        ? this.calculatedQuote.riskDetails.handicappedDiscount
        : 0;
      let bonusDiscount = this.calculatedQuote.riskDetails.bonusDiscount
        ? this.calculatedQuote.riskDetails.bonusDiscount
        : 0;
      let voluntaryDiscount = this.calculatedQuote.riskDetails.voluntaryDiscount
        ? this.calculatedQuote.riskDetails.voluntaryDiscount
        : 0;
      let tppD_Discount = this.calculatedQuote.riskDetails.tppD_Discount
        ? this.calculatedQuote.riskDetails.tppD_Discount
        : 0;
      this.totalDiscount =
        antiTheftDiscount +
        automobileAssociationDiscount +
        handicappedDiscount +
        bonusDiscount +
        voluntaryDiscount +
        tppD_Discount;
      // this.totalDiscount = this.calculatedQuote.riskDetails.antiTheftDiscount + this.calculatedQuote.riskDetails.automobileAssociationDiscount + this.calculatedQuote.riskDetails.handicappedDiscount + this.calculatedQuote.riskDetails.bonusDiscount + this.calculatedQuote.riskDetails.voluntaryDiscount + this.calculatedQuote.riskDetails.tppD_Discount;
      let pkgPrem = this.calculatedQuote.packagePremium
        ? this.calculatedQuote.packagePremium
        : this.calculatedQuote.totalLiabilityPremium;
      this.premiumWithoutDiscount = pkgPrem + this.totalDiscount;
      this.saveofflineQuot3prem();
      $("#premium_breakup").modal("show");
    } else if (this.planName == "plan2" && !this.isRecalculate1) {
      this.calculatedQuote = this.premres2;
      let antiTheftDiscount = this.calculatedQuote.riskDetails.antiTheftDiscount
        ? this.calculatedQuote.riskDetails.antiTheftDiscount
        : 0;
      let automobileAssociationDiscount = this.calculatedQuote.riskDetails
        .automobileAssociationDiscount
        ? this.calculatedQuote.riskDetails.automobileAssociationDiscount
        : 0;
      let handicappedDiscount = this.calculatedQuote.riskDetails
        .handicappedDiscount
        ? this.calculatedQuote.riskDetails.handicappedDiscount
        : 0;
      let bonusDiscount = this.calculatedQuote.riskDetails.bonusDiscount
        ? this.calculatedQuote.riskDetails.bonusDiscount
        : 0;
      let voluntaryDiscount = this.calculatedQuote.riskDetails.voluntaryDiscount
        ? this.calculatedQuote.riskDetails.voluntaryDiscount
        : 0;
      let tppD_Discount = this.calculatedQuote.riskDetails.tppD_Discount
        ? this.calculatedQuote.riskDetails.tppD_Discount
        : 0;
      this.totalDiscount =
        antiTheftDiscount +
        automobileAssociationDiscount +
        handicappedDiscount +
        bonusDiscount +
        voluntaryDiscount +
        tppD_Discount;
      // this.totalDiscount = this.calculatedQuote.riskDetails.antiTheftDiscount + this.calculatedQuote.riskDetails.automobileAssociationDiscount + this.calculatedQuote.riskDetails.handicappedDiscount + this.calculatedQuote.riskDetails.bonusDiscount + this.calculatedQuote.riskDetails.voluntaryDiscount + this.calculatedQuote.riskDetails.tppD_Discount;
      let pkgPrem = this.calculatedQuote.packagePremium
        ? this.calculatedQuote.packagePremium
        : this.calculatedQuote.totalLiabilityPremium;
      this.premiumWithoutDiscount = pkgPrem + this.totalDiscount;
      this.saveofflineQuot3prem();
      $("#premium_breakup").modal("show");
    } else if (this.planName == "plan3" && !this.isRecalculate1) {
      this.calculatedQuote = this.premres3;
      let antiTheftDiscount = this.calculatedQuote.riskDetails.antiTheftDiscount
        ? this.calculatedQuote.riskDetails.antiTheftDiscount
        : 0;
      let automobileAssociationDiscount = this.calculatedQuote.riskDetails
        .automobileAssociationDiscount
        ? this.calculatedQuote.riskDetails.automobileAssociationDiscount
        : 0;
      let handicappedDiscount = this.calculatedQuote.riskDetails
        .handicappedDiscount
        ? this.calculatedQuote.riskDetails.handicappedDiscount
        : 0;
      let bonusDiscount = this.calculatedQuote.riskDetails.bonusDiscount
        ? this.calculatedQuote.riskDetails.bonusDiscount
        : 0;
      let voluntaryDiscount = this.calculatedQuote.riskDetails.voluntaryDiscount
        ? this.calculatedQuote.riskDetails.voluntaryDiscount
        : 0;
      let tppD_Discount = this.calculatedQuote.riskDetails.tppD_Discount
        ? this.calculatedQuote.riskDetails.tppD_Discount
        : 0;
      this.totalDiscount =
        antiTheftDiscount +
        automobileAssociationDiscount +
        handicappedDiscount +
        bonusDiscount +
        voluntaryDiscount +
        tppD_Discount;
      // this.totalDiscount = this.calculatedQuote.riskDetails.antiTheftDiscount + this.calculatedQuote.riskDetails.automobileAssociationDiscount + this.calculatedQuote.riskDetails.handicappedDiscount + this.calculatedQuote.riskDetails.bonusDiscount + this.calculatedQuote.riskDetails.voluntaryDiscount + this.calculatedQuote.riskDetails.tppD_Discount;
      let pkgPrem = this.calculatedQuote.packagePremium
        ? this.calculatedQuote.packagePremium
        : this.calculatedQuote.totalLiabilityPremium;
      this.premiumWithoutDiscount = pkgPrem + this.totalDiscount;
      this.saveofflineQuot3prem();
      $("#premium_breakup").modal("show");
    } else {
      // this.calculatedQuote = this.premres3;
      this.quoteRequ = "";
      let antiTheftDiscount = this.calculatedQuote.riskDetails.antiTheftDiscount
        ? this.calculatedQuote.riskDetails.antiTheftDiscount
        : 0;
      let automobileAssociationDiscount = this.calculatedQuote.riskDetails
        .automobileAssociationDiscount
        ? this.calculatedQuote.riskDetails.automobileAssociationDiscount
        : 0;
      let handicappedDiscount = this.calculatedQuote.riskDetails
        .handicappedDiscount
        ? this.calculatedQuote.riskDetails.handicappedDiscount
        : 0;
      let bonusDiscount = this.calculatedQuote.riskDetails.bonusDiscount
        ? this.calculatedQuote.riskDetails.bonusDiscount
        : 0;
      let voluntaryDiscount = this.calculatedQuote.riskDetails.voluntaryDiscount
        ? this.calculatedQuote.riskDetails.voluntaryDiscount
        : 0;
      let tppD_Discount = this.calculatedQuote.riskDetails.tppD_Discount
        ? this.calculatedQuote.riskDetails.tppD_Discount
        : 0;
      this.totalDiscount =
        antiTheftDiscount +
        automobileAssociationDiscount +
        handicappedDiscount +
        bonusDiscount +
        voluntaryDiscount +
        tppD_Discount;
      // this.totalDiscount = this.calculatedQuote.riskDetails.antiTheftDiscount + this.calculatedQuote.riskDetails.automobileAssociationDiscount + this.calculatedQuote.riskDetails.handicappedDiscount + this.calculatedQuote.riskDetails.bonusDiscount + this.calculatedQuote.riskDetails.voluntaryDiscount + this.calculatedQuote.riskDetails.tppD_Discount;
      let pkgPrem = this.calculatedQuote.packagePremium
        ? this.calculatedQuote.packagePremium
        : this.calculatedQuote.totalLiabilityPremium;
      this.premiumWithoutDiscount = pkgPrem + this.totalDiscount;
      this.saveofflineQuot3prem();
      $("#premium_breakup").modal("show");
    }
  }

  openModal1() {
    this.savecustomizequote().then(() => {
      $("#premium_breakup").modal({ backdrop: "static", keyboard: false });
    });
  }

  openPremiumModal() {
    this.allriskSaveOfflineQuote();
    if (this.isSCPA == true && this.isValidSCPA == true) {
      this.scpaSaveOfflineQuote();
    }
    $("#premium_breakup").modal("show");
  }

  getRTONew() {
    swal({
      closeOnClickOutside: false,
      text: "Automated data not fetched, kindly continue with manual data entry",
    });
  }

  clearquoteform() {
    let startDate = moment(new Date()).format("YYYY-MM-DD");
    this.quoteDetails.rtO_LOCATION_DESC = "";
    this.quoteDetails.vehiclemodel = "";
    this.showIDV = false;
    if (this.policyType == "NEW") {
      this.quoteDetails.FirstRegistrationDate = startDate;
      let currentyear = moment(this.quoteDetails.PolicyStartDate).format(
        "YYYY"
      );
      this.quoteDetails.ManufacturingYear = currentyear;
    } else {
      this.quoteDetails.FirstRegistrationDate = "";
    }

    if (this.policyType == "NEW") {
      this.quoteDetails.FirstRegistrationDate = startDate;
      let currentyear = moment(this.quoteDetails.PolicyStartDate).format(
        "YYYY"
      );
      this.quoteDetails.ManufacturingYear = currentyear;
    }
  }

  calledVahaanAPI() {
    //15032022
    this.cs.clearPropData();
    localStorage.removeItem('regNumber');
    this.cs
      .getVahaanToken()
      .then((resp: any) => {
        localStorage.setItem("AuthTokenRedis", JSON.stringify(resp));
        console.log(this.productCode);
        if (this.productCode == "2311" || this.productCode == "2319") {
          this.getRTO4w();
        } else {
          this.getRTO();
        }
      })
      .catch((err: any) => {
        swal({
          text: "There is some issue with system.",
          closeOnClickOutside: false,
        });
      });
  }

  // Get RTO Details 2w
  getRTO() {
    this.isRecalculate1 = false;
    this.saod(this.showsaod);
    this.openQuoteSection = true;
    this.clearquoteform();
    this.quoteDetails.IsExtensionCountry = false;
    // this.IsSelfInspection = false;
    this.isZeroDepPlanName = false;
    this.isRSAPlan = false;
    this.quoteDetails.IsRTIApplicableflag = false;
    this.quoteDetails.IsConsumables = false;
    this.getVehicleDetailsClicked = true;
    this.getrtoButtonclicked = true;
    if (this.quoteDetails.RegistrationNumber != "") {
      let regNum = this.quoteDetails.RegistrationNumber;
      // this.cs.loaderStatus = true;
      this.spinloader = true;
      /**
       * Digital POS change
       * call getDeal function only for nysa not for POS
       * date :- 29-07-2021
       */
      if (this.IsPOSTransaction_flag == true) {
        console.info("Quote-deal-details-change");
        this.POSmobileNumber = JSON.parse(
          localStorage.getItem("POSmobileNumber")
        );
        let PosDealObj = {
          MobileNumber: this.POSmobileNumber,
          SubProductCode: this.productCode,
        };
        this.cs.GetPOSDealId(JSON.stringify(PosDealObj)).then((res: any) => {
          if (res.isSuccess == true) {
            this.dealid = res.dealID;
            localStorage.setItem("DealID", JSON.stringify(res.dealID));
          } else {
            swal({ text: "Deal not found" });
          }
        });
      } else {
        // GetDealFromIM? changes - Sejal - 21-01-2022
        if (this.cs.isUndefineORNull(localStorage.getItem("DealID"))) {
          this.getDeal();
        }
      }

      (<any>window).ga("send", "event", {
        eventCategory: "Car Info Event - 2 Wheeler",
        eventLabel: regNum + "" + this.product + "" + this.policyType,
        eventAction: "Car Info Clicked",
        eventValue: 10,
      });

      if (
        (this.productCode == "2312" || this.productCode == "2320") &&
        this.isEnableCarInfoNewService
      ) {
        let body = {
          LoginId: "xyz",
          VehRegNo: regNum,
          Token:
            "WTJGeWFXNW1iMTlwWTJsamFWOXdjbVZ0YVhWdFgyRndhVjh5TURJd1gyUnljM05yY0E9PQ==",
          Key: "$!c!c!_2020",
        };
        let str = JSON.stringify(body);
        let GUID = Guid.raw();
        let key1 = GUID.split("-");
        let key2 =
          key1[0] + "-" + key1[1] + "-" + key1[2] + "-" + key1[3] + "-";
        // let encrypted1 = str+'|'+JSON.stringify(key2);
        let encrypted1 = str;
        let encrypted2 = this.cs.encryptUsingGUID(encrypted1, key2);
        let encrypted3 = encrypted2 + "|" + key2;
        let encrypted4 = this.cs.encryptLikePython(encrypted3);
        let body1 = {
          encryptedData: encrypted4,
        };
        let str1 = JSON.stringify(body1);
        console.log("Encrypted Final", encrypted4);
        // commented for IIB Change 
        // this.api.getRTODetailsForVahaan(str1).subscribe(
        this.api.getRTODetailsForVahaanNew(str1).subscribe(
          (res: any) => {
            this.ncbValue = "0";
            if (res.StatusType == "SUCCESS") {
              localStorage.removeItem("calQuoteReq");
              localStorage.removeItem("PreviousData");
              localStorage.removeItem("calQuoteRes");
              this.showPYP = true;
              if (res.IsSuccess) {
                if (!this.cs.isUndefineORNull(res.insuranceUpto)) {
                  this.quoteDetails.PreviousPolicyEndDate = moment(
                    res.insuranceUpto,
                    "DD-MMM-YYYY"
                  ).format("YYYY-MM-DD");
                  this.getPrevEndDate(this.quoteDetails.PreviousPolicyEndDate);
                }

                if (res.IsClaimMade == 'Y') {
                  if (res.ClaimDetails.length > 1) {
                    this.sortedClaimDetails = this.cs.sortByDOL(res.ClaimDetails);
                    console.log('Sorted', this.sortedClaimDetails);
                  } else {
                    this.sortedClaimDetails = res.ClaimDetails;
                  }
                  let DOL = moment(this.sortedClaimDetails[0].DateOfLoss).format("YYYY-MM-DD");
                  console.log('For IIB', this.ncbValue);
                  // let PYPEndDate = new Date(res.insuranceUpto);

                  var D1 = new Date(this.quoteDetails.PreviousPolicyStartDate);
                  var D2 = new Date(this.quoteDetails.PreviousPolicyEndDate);
                  var D3 = new Date(DOL);
                  if (
                    D3.getTime() <= D2.getTime() &&
                    D3.getTime() >= D1.getTime()
                  ) {
                    this.claimNCB = false;
                    this.ClaimOnPreviousPolicy = true;
                    this.quoteDetails.claims = "claimyes";
                    this.showIIBData = true;
                    this.IIBData = res.ClaimDetails;
                    this.quoteDetails.TotalNoOfODClaims = JSON.stringify(
                      res.TotalClaims
                    );
                    this.claimCount = res.TotalClaims;
                  } else {
                    this.claimNCB = true;
                    this.ClaimOnPreviousPolicy = false;
                    this.quoteDetails.claims = "claimno";
                    this.quoteDetails.TotalNoOfODClaims = "0";
                    this.showIIBData = false;
                    this.claimCount = 0;
                    let Time = new Date(this.quoteDetails.PolicyStartDate).getTime() - new Date(this.quoteDetails.PreviousPolicyEndDate).getTime();
                    let daysDiff = Time / (1000 * 3600 * 24);
                    console.log('Days diff', daysDiff);
                    if (daysDiff > 90) {
                      this.ncbValue = '0';
                    } else {
                      if (this.sortedClaimDetails[0].ClaimType == 'OD') {
                        this.ncbValue = this.cs.NCBLogic(DOL, this.quoteDetails.PolicyStartDate, this.sortedClaimDetails[0].ClaimType);
                      } else {
                        this.ncbValue = this.cs.NCBLogicOnDate(this.quoteDetails.PolicyStartDate, this.sortedClaimDetails[0].ClaimIntimationDate);
                      }
                    }
                  }
                } else {
                  this.claimNCB = true;
                  this.ClaimOnPreviousPolicy = false;
                  this.quoteDetails.claims = "claimno";
                  this.quoteDetails.TotalNoOfODClaims = "0";
                  this.showIIBData = false;
                  this.ncbValue = this.ncbValue;
                  this.claimCount = 0;
                }
              } else {
                this.claimNCB = true;
                this.ClaimOnPreviousPolicy = false;
                this.quoteDetails.claims = "claimno";
                this.quoteDetails.TotalNoOfODClaims = "0";
                this.showIIBData = false;
                this.ncbValue = this.ncbValue;
                this.claimCount = 0;
              }
              // this.api.getiibServices(regNum).subscribe((res: any) => {
              //   // this.quoteDetails.PreviousPolicyNumber = res.policyNumber;
              //   if (res.isClaimMade == "Y") {
              //     let index = res.totalClaims - 1;
              //     console.log(
              //       "ABC",
              //       this.quoteDetails.PreviousPolicyStartDate,
              //       this.sortedClaimDetails[0].dateOfLoss
              //     );
              //     console.log(
              //       "ABC1",
              //       this.quoteDetails.PreviousPolicyStartDate,
              //       this.quoteDetails.PreviousPolicyEndDate,
              //       moment(this.sortedClaimDetails[0].dateOfLoss).format(
              //         "YYYY-MM-DD"
              //       )
              //     );
              //     let dateCheck = moment(
              //       this.sortedClaimDetails[0].dateOfLoss
              //     ).format("YYYY-MM-DD");
              //     // let DOL = moment(this.sortedClaimDetails[0].dateOfLoss).format("YYYY-MM-DD");
              //     // let NCBValue = this.cs.NCBLogic(DOL, this.quoteDetails.PolicyStartDate, this.sortedClaimDetails[0].claimType);
              //     // console.log('New NCB Logic', NCBValue)
              //     var D1 = new Date(this.quoteDetails.PreviousPolicyStartDate);
              //     var D2 = new Date(this.quoteDetails.PreviousPolicyEndDate);
              //     var D3 = new Date(dateCheck);
              //     if (
              //       D3.getTime() <= D2.getTime() &&
              //       D3.getTime() >= D1.getTime()
              //     ) {
              //       this.claimNCB = false;
              //       this.ClaimOnPreviousPolicy = true;
              //       this.quoteDetails.claims = "claimyes";
              //       // this.quoteDetails.TotalNoOfODClaims = "1";
              //       this.showIIBData = true;
              //       this.IIBData = res.claimDetails;
              //       this.quoteDetails.TotalNoOfODClaims = JSON.stringify(
              //         res.totalClaims
              //       );
              //       this.claimCount = res.totalClaims;
              //     } else {
              //       this.claimNCB = true;
              //       this.ClaimOnPreviousPolicy = false;
              //       this.quoteDetails.claims = "claimno";
              //       this.quoteDetails.TotalNoOfODClaims = "0";
              //       this.showIIBData = false;
              //       this.ncbValue = "20";
              //       this.claimCount = 0;
              //     }
              //   } else {
              //     this.claimNCB = true;
              //     this.ClaimOnPreviousPolicy = false;
              //     this.quoteDetails.claims = "claimno";
              //     this.quoteDetails.TotalNoOfODClaims = "0";
              //     // NEED To check
              //     this.ncbValue = this.ncbValue;
              //     this.showIIBData = false;
              //     this.claimCount = 0;
              //     this.quoteDetails.TotalNoOfODClaims = JSON.stringify(
              //       res.totalClaims
              //     );
              //   }
              // });
              this.quoteDetails.PreviousPolicyNumber =
                res.previousInsurerPolicyNo;
              if (res.vh_class_desc == "Two Wheeler") {
                (<any>window).ga("send", "event", {
                  eventCategory: "Car Info Event - 2 Wheeler Success",
                  eventLabel:
                    "" + regNum + "" + this.product + "" + this.policyType,
                  eventAction: "Car Info Success",
                  eventValue: 10,
                });
                
                this.rtoDetail = res;

                localStorage.setItem("rtoDetail", JSON.stringify(res));
                this.RTOname = "";
                let regDate = moment(
                  this.rtoDetail.regn_dt,
                  "DD-MMM-YYYY"
                ).format("YYYY-MM-DD");
                this.quoteDetails.FirstRegistrationDate = regDate;
                this.vehicleAge = moment().diff(
                  this.quoteDetails.FirstRegistrationDate,
                  "years"
                );
                this.bindyear(regDate);
                this.quoteDetails.rtO_LOCATION_DESC = "";
                this.quoteDetails.rtO_LOCATION_DESC = this.rtoDetail.rto_name;

                // this.cs
                //   .get("PrevPolicy/GetPreviousInsurer")
                //   .subscribe((res: any) => {
                //     let compname;
                //     compname = res.find(
                //       (c) => c.shortName == this.rtoDetail.previousInsurerCode
                //     ).companyName;
                //     if(this.cs.isUndefineORNull(compname)){
                //       this.quoteDetails.PreviousInsurerName = 'OTHER';
                //     }else{
                //       this.quoteDetails.PreviousInsurerName = compname;
                //     }
                //   });

                /**
                 * Get Previous InsurerName api change
                 * Author :- Sumit
                 * date :- 12-01-2022
                 */
                let previousInsurer: any = this.cs.PreviousInsurerList;
                let compname;
                if (previousInsurer != undefined && previousInsurer != null) {
                  if (
                    this.rtoDetail.previousInsurerCode != "NA" &&
                    !this.cs.isUndefineORNull(
                      this.rtoDetail.previousInsurerCode
                    )
                  ) {
                    compname = previousInsurer.find(
                      (c) => c.shortName == this.rtoDetail.previousInsurerCode
                    );
                    console.log(compname);
                    if (this.cs.isUndefineORNull(compname)) {
                      this.quoteDetails.PreviousInsurerName = "OTHER";
                    } else {
                      this.quoteDetails.PreviousInsurerName =
                        compname.companyName;
                    }
                  } else {
                    compname = "OTHER";
                  }
                } else {
                  this.quoteDetails.PreviousInsurerName = "OTHER";
                }

                this.quoteDetails.PreviousPolicyNumber =
                  res.previousInsurerPolicyNo;
                if (
                  !this.cs.isUndefineORNull(res.previousInsurerPolicyNo) &&
                  !this.cs.isUndefineORNull(res.previousInsurerCode) &&
                  !this.cs.isUndefineORNull(res.previousInsurer) &&
                  (!this.cs.isUndefineORNull(res.insuranceUpto) ||
                    res.insuranceUpto != "NA")
                ) {
                  this.quoteDetails.PreviousPolicyDetails = true;
                } else {
                  this.showPYP = false;
                }
                if (
                  !this.cs.isUndefineORNull(res.insuranceUpto) ||
                  res.insuranceUpto != "NA"
                ) {
                  this.quoteDetails.PreviousPolicyEndDate = moment(
                    res.insuranceUpto,
                    "DD-MMM-YYYY"
                  ).format("YYYY-MM-DD");
                } else {
                  this.showPYP = false;
                }
                if (this.productCode == "2312" || this.productCode == "2311") {
                  this.getPrevEndDate(this.quoteDetails.PreviousPolicyEndDate);
                }

                if (this.rtoDetail.pf_model_code != null) {
                  this.manuinput = this.rtoDetail.pf_manufactur;
                  this.modelcode = this.rtoDetail.pf_model_code;
                  this.rtoCode = this.rtoDetail.rto_cd;

                  this.api
                    .getRTOLocation(this.rtoCode, this.classCode)
                    .subscribe((res: any) => {
                      if (this.cs.isUndefineORNull(res)) {
                        this.freezeRTOCall();
                      } else {
                        this.quoteDetails.rtO_LOCATION_DESC = res;
                        let data = res.split("-");
                        if (data[0] == "TAMILNADU") {
                          this.quoteDetails.GSTToState = "TAMIL NADU";
                        } else {
                          this.quoteDetails.GSTToState = data[0];
                        }
                        this.isRTOFreeze = true;
                      }
                    });

                  this.api
                    .getModelName(this.modelcode, this.classCode)
                    .subscribe((res: any) => {
                      if (res.seatingCapacity != null) {
                        this.quoteDetails.vehiclemodel = res.vehicleName;
                        this.modelName = res.manufacturer;
                        this.makecode = res.makeCode;
                        this.cubiccapacity = res.cubicCapacity;
                        this.seatingcapacity = res.seatingCapacity;

                        if (
                          this.productCode == "2320" ||
                          this.productCode == "2312"
                        ) {
                          if (
                            this.rtoDetail.rto_cd != "" &&
                            this.rtoDetail.pf_model_code != "" &&
                            this.rtoDetail.rto_cd != "NA"
                          ) {
                            if (
                              this.rtoDetail.pf_model_code != null &&
                              this.seatingcapacity != null
                            ) {
                              this.modelcode = this.rtoDetail.pf_model_code;

                              if (this.showsaod == false) {
                                this.findexshowroom().then(() => {
                                  // this.cs.loaderStatus = false;
                                  this.calculatequote().then(() => {
                                    if (this.bFlag == true) {
                                      // this.cs.loaderStatus = false;
                                      this.spinloader = false;
                                      this.showAddon = false;
                                    } else {
                                      this.systemQC = true;
                                    }
                                  });
                                });
                              } else {
                                this.findexshowroom();
                                // this.cs.loaderStatus = false;
                                this.spinloader = false;
                              }

                              localStorage.setItem(
                                "ManufactureName",
                                this.rtoDetail.fla_maker_desc
                              );
                              this.getVehicleFlag = true;
                            } else {
                              // this.cs.loaderStatus = false;
                              this.spinloader = false;
                            }
                          } else {
                            // this.cs.loaderStatus = false;
                            this.spinloader = false;
                            // swal({
                            //   closeOnClickOutside: false,
                            //   text: 'Data not fetched from Carinfo, continue with manual data entry',
                            // });
                            // this.freezeRTOCall();
                            this.showElement = true;
                            setTimeout(() => {
                              this.showElement = false; // Here, value is updated
                            }, 5000);
                            // this.quoteDetails.rtO_LOCATION_DESC = "";
                            this.quoteDetails.vehiclemodel = "";
                            this.showIDV = false;
                          }
                        }
                      } else {
                        // this.cs.loaderStatus = false;
                        this.spinloader = false;

                        // swal({
                        //   closeOnClickOutside: false,
                        //   text: 'Data not fetched from Carinfo, continue with manual data entry',
                        // });

                        this.showElement = true;
                        setTimeout(() => {
                          this.showElement = false; // Here, value is updated
                        }, 5000);
                      }
                    });
                } else {
                  this.showElement = true;
                  this.spinloader = false;
                  this.freezeRTOCall();
                  // swal({
                  //   closeOnClickOutside: false,
                  //   text: 'Data not fetched from Carinfo, continue with manual data entry',
                  // });

                  setTimeout(() => {
                    this.showElement = false; // Here, value is updated
                  }, 5000);
                  // }

                  // this.cs.loaderStatus = false;
                  this.spinloader = false;
                  // this.quoteDetails.rtO_LOCATION_DESC = "";
                }
              } else if (res.vh_class_desc == "Private Car") {
                // this.cs.loaderStatus = false;
                this.spinloader = false;

                if (this.productCode == "2312") {
                  swal({
                    text: "Entered registration number is of 4 wheeler. Do you want to switch to Private Car ?",
                    dangerMode: true,
                    closeOnClickOutside: false,
                    buttons: ["Yes", "No"],
                  }).then((willDelete) => {
                    if (willDelete) {
                      console.log("Stay");
                      this.freezeRTOCall();
                    } else {
                      this.productCode = "2311";
                      this.getDeal();
                      this.classCode = "45";
                      this.TPPDLimit = 750000;
                      this.refreshToken("roll", "RTO").then(() => {
                        regNum = this.quoteDetails.RegistrationNumber;
                        this.getRTO4w();
                      });
                    }
                  });
                } else if (this.productCode == "2320") {
                  swal({
                    text: "You have entered Private Car registration number. Do you want to switch to Private Car TP?",
                    dangerMode: true,
                    closeOnClickOutside: false,
                    buttons: ["Yes", "No"],
                  }).then((willDelete) => {
                    if (willDelete) {
                      console.log("Stay");
                      this.freezeRTOCall();
                    } else {
                      this.productCode = "2319";
                      this.getDeal();
                      this.classCode = "45";
                      this.TPPDLimit = 750000;
                      this.refreshToken("roll", "RTO").then(() => {
                        regNum = this.quoteDetails.RegistrationNumber;
                        this.getRTO4w();
                      });
                    }
                  });
                }
              } else {
                this.cs.loaderStatus = false;
                this.spinloader = false;
                this.quoteDetails.RegistrationNumber = "";
                // this.freezeRTOCall();
                swal({
                  closeOnClickOutside: false,
                  text: "The registration number entered is of a commercial vehicle. Login to ipartner for a commercial vehicle Quote & policy booking",
                });

                this.showElement = true;
                setTimeout(() => {
                  this.showElement = false; // Here, value is updated
                }, 5000);
              }
            } else {
              localStorage.removeItem("calQuoteReq");
              localStorage.removeItem("calQuoteRes");
              this.showPYP = false;
              console.log("Rahuls API call");
              this.cs.loaderStatus = false;
              this.spinloader = false;
              this.showElement = true;
              this.freezeRTOCall();
              setTimeout(() => {
                this.showElement = false; // Here, value is updated
              }, 5000);
            }
          },
          (err) => {
            this.ncbValue = "0";
            this.cs.loaderStatus = false;
            this.spinloader = false;
            console.log("Rahuls API call");
            this.freezeRTOCall();
            this.showPYP = false; // PYP display issue - Sejal - 2-1-2022
            localStorage.removeItem("calQuoteReq");
            localStorage.removeItem("calQuoteRes");
            if (err.name == "TimeoutError") {
              // swal({
              //   closeOnClickOutside: false,
              //   text: 'Data not fetched from Carinfo, continue with manual data entry',
              // });
              this.showElement = true;
              setTimeout(() => {
                this.showElement = false; // Here, value is updated
              }, 5000);
            }
          }
        );
      } else {
        this.ncbValue = "0";
        this.api.getRTODetails(regNum).subscribe(
          (res: any) => {
            if (res.statusType == "SUCCESS") {
              if (res.IsSuccess) {
                if (!this.cs.isUndefineORNull(res.insuranceUpto)) {
                  this.quoteDetails.PreviousPolicyEndDate = moment(
                    res.insuranceUpto,
                    "DD-MMM-YYYY"
                  ).format("YYYY-MM-DD");
                  this.getPrevEndDate(this.quoteDetails.PreviousPolicyEndDate);
                }
                if (res.IsClaimMade == 'Y') {
                  if (res.ClaimDetails.length > 1) {
                    this.sortedClaimDetails = this.cs.sortByDOL(res.ClaimDetails);
                    console.log('Sorted', this.sortedClaimDetails);
                  } else {
                    this.sortedClaimDetails = res.ClaimDetails;
                  }
                  let DOL = moment(this.sortedClaimDetails[0].DateOfLoss).format("YYYY-MM-DD");
                  console.log(this.sortedClaimDetails[0])
                  console.log('For IIB', this.ncbValue);
                  var D1 = new Date(this.quoteDetails.PreviousPolicyStartDate);
                  var D2 = new Date(this.quoteDetails.PreviousPolicyEndDate);
                  var D3 = new Date(DOL);
                  if (
                    D3.getTime() <= D2.getTime() &&
                    D3.getTime() >= D1.getTime()
                  ) {
                    this.claimNCB = false;
                    this.ClaimOnPreviousPolicy = true;
                    this.quoteDetails.claims = "claimyes";
                    this.showIIBData = true;
                    this.IIBData = res.ClaimDetails;
                    this.quoteDetails.TotalNoOfODClaims = JSON.stringify(
                      res.TotalClaims
                    );
                    this.claimCount = res.TotalClaims;
                  } else {
                    this.claimNCB = true;
                    this.ClaimOnPreviousPolicy = false;
                    this.quoteDetails.claims = "claimno";
                    this.quoteDetails.TotalNoOfODClaims = "0";
                    this.showIIBData = false;
                    this.claimCount = 0;
                    let Time = new Date(this.quoteDetails.PolicyStartDate).getTime() - new Date(this.quoteDetails.PreviousPolicyEndDate).getTime();
                    let daysDiff = Time / (1000 * 3600 * 24);
                    console.log('Days diff', daysDiff);
                    if (daysDiff > 90) {
                      this.ncbValue = '0';
                    } else {
                      if (this.sortedClaimDetails[0].ClaimType == 'OD') {
                        this.ncbValue = this.cs.NCBLogic(DOL, this.quoteDetails.PolicyStartDate, this.sortedClaimDetails[0].ClaimType);
                      } else {
                        this.ncbValue = this.cs.NCBLogicOnDate(this.quoteDetails.PolicyStartDate, this.sortedClaimDetails[0].ClaimIntimationDate);
                      }
                    }
                  }
                } else {
                  this.claimNCB = true;
                  this.ClaimOnPreviousPolicy = false;
                  this.quoteDetails.claims = "claimno";
                  this.quoteDetails.TotalNoOfODClaims = "0";
                  this.showIIBData = false;
                  this.ncbValue = this.ncbValue;
                  this.claimCount = 0;
                }
              } else {
                this.claimNCB = true;
                this.ClaimOnPreviousPolicy = false;
                this.quoteDetails.claims = "claimno";
                this.quoteDetails.TotalNoOfODClaims = "0";
                this.showIIBData = false;
                this.ncbValue = this.ncbValue;
                this.claimCount = 0;
              }
              // this.api.getiibServices(regNum).subscribe((res: any) => {
              //   // this.quoteDetails.PreviousPolicyNumber = res.policyNumber;
              //   if (res.isClaimMade == "Y") {
              //     let index = res.totalClaims - 1;
              //     console.log(
              //       "ABC",
              //       this.quoteDetails.PreviousPolicyStartDate,
              //       this.sortedClaimDetails[0].dateOfLoss
              //     );
              //     console.log(
              //       "ABC1",
              //       this.quoteDetails.PreviousPolicyStartDate,
              //       this.quoteDetails.PreviousPolicyEndDate,
              //       moment(this.sortedClaimDetails[0].dateOfLoss).format(
              //         "YYYY-MM-DD"
              //       )
              //     );
              //     let dateCheck = moment(
              //       this.sortedClaimDetails[0].dateOfLoss
              //     ).format("YYYY-MM-DD");
              //     // let DOL = moment(this.sortedClaimDetails[0].dateOfLoss).format("YYYY-MM-DD");
              //     // let NCBValue = this.cs.NCBLogic(DOL, this.quoteDetails.PolicyStartDate, this.sortedClaimDetails[0].claimType);
              //     // console.log('New NCB Logic', NCBValue)
              //     var D1 = new Date(this.quoteDetails.PreviousPolicyStartDate);
              //     var D2 = new Date(this.quoteDetails.PreviousPolicyEndDate);
              //     var D3 = new Date(dateCheck);
              //     if (
              //       D3.getTime() <= D2.getTime() &&
              //       D3.getTime() >= D1.getTime()
              //     ) {
              //       this.claimNCB = false;
              //       this.ClaimOnPreviousPolicy = true;
              //       this.quoteDetails.claims = "claimyes";
              //       this.showIIBData = true;
              //       this.IIBData = res.claimDetails;
              //       this.quoteDetails.TotalNoOfODClaims = JSON.stringify(
              //         res.totalClaims
              //       );
              //       this.claimCount = res.totalClaims;
              //     } else {
              //       this.claimNCB = true;
              //       this.ClaimOnPreviousPolicy = false;
              //       this.quoteDetails.claims = "claimno";
              //       this.showIIBData = false;
              //       this.ncbValue = "20";
              //       this.claimCount = 0;
              //       this.quoteDetails.TotalNoOfODClaims = "0";
              //     }
              //   } else {
              //     this.claimNCB = true;
              //     this.ClaimOnPreviousPolicy = false;
              //     this.quoteDetails.claims = "claimno";
              //     this.quoteDetails.TotalNoOfODClaims = "0";
              //     this.showIIBData = false;
              //     // NEED to check
              //     this.ncbValue = this.ncbValue;
              //     this.claimCount = 0;
              //   }
              // });
              this.quoteDetails.PreviousPolicyNumber =
                res.previousInsurerPolicyNo;
              if (res.vh_class_desc == "Two Wheeler") {
                (<any>window).ga("send", "event", {
                  eventCategory: "Car Info Event - 2 Wheeler Success",
                  eventLabel:
                    "" + regNum + "" + this.product + "" + this.policyType,
                  eventAction: "Car Info Success",
                  eventValue: 10,
                });

                this.rtoDetail = res;

                localStorage.setItem("rtoDetail", JSON.stringify(res));
                this.RTOname = "";
                let regDate = moment(
                  this.rtoDetail.regn_dt,
                  "DD-MMM-YYYY"
                ).format("YYYY-MM-DD");
                this.quoteDetails.FirstRegistrationDate = regDate;
                this.vehicleAge = moment().diff(
                  this.quoteDetails.FirstRegistrationDate,
                  "years"
                );
                this.bindyear(regDate);
                this.quoteDetails.rtO_LOCATION_DESC = "";
                this.quoteDetails.rtO_LOCATION_DESC = this.rtoDetail.rto_name;

                // this.cs
                //   .get("PrevPolicy/GetPreviousInsurer")
                //   .subscribe((res: any) => {
                //     let compname;
                //     compname = res.find(
                //       (c) => c.shortName == this.rtoDetail.previousInsurerCode
                //     ).companyName;
                //     if(this.cs.isUndefineORNull(compname)){
                //       this.quoteDetails.PreviousInsurerName = 'OTHER';
                //     }else{
                //       this.quoteDetails.PreviousInsurerName = compname;
                //     }
                //   });

                /**
                 * Get Previous InsurerName api change
                 * Author :- Sumit
                 * date :- 12-01-2022
                 */
                let previousInsurer: any = this.cs.PreviousInsurerList;;
                let compname;
                if (previousInsurer != undefined && previousInsurer != null) {
                  if (
                    this.rtoDetail.previousInsurerCode != "NA" &&
                    !this.cs.isUndefineORNull(
                      this.rtoDetail.previousInsurerCode
                    )
                  ) {
                    compname = previousInsurer.find(
                      (c) => c.shortName == this.rtoDetail.previousInsurerCode
                    );
                    console.log(compname);
                    if (this.cs.isUndefineORNull(compname)) {
                      this.quoteDetails.PreviousInsurerName = "OTHER";
                    } else {
                      this.quoteDetails.PreviousInsurerName =
                        compname.companyName;
                    }
                  } else {
                    compname = "OTHER";
                  }
                } else {
                  this.quoteDetails.PreviousInsurerName = "OTHER";
                }
                this.quoteDetails.PreviousPolicyNumber =
                  res.previousInsurerPolicyNo;
                if (
                  !this.cs.isUndefineORNull(res.previousInsurerPolicyNo) &&
                  !this.cs.isUndefineORNull(res.previousInsurerCode) &&
                  !this.cs.isUndefineORNull(res.previousInsurer) &&
                  (!this.cs.isUndefineORNull(res.insuranceUpto) ||
                    res.insuranceUpto != "NA")
                ) {
                  this.quoteDetails.PreviousPolicyDetails = true;
                } else {
                  this.showPYP = false;
                }
                if (
                  !this.cs.isUndefineORNull(res.insuranceUpto) ||
                  res.insuranceUpto != "NA"
                ) {
                  this.quoteDetails.PreviousPolicyEndDate = moment(
                    res.insuranceUpto,
                    "DD-MMM-YYYY"
                  ).format("YYYY-MM-DD");
                } else {
                  this.showPYP = false;
                }
                if (this.productCode == "2312" || this.productCode == "2311") {
                  this.getPrevEndDate(this.quoteDetails.PreviousPolicyEndDate);
                }

                if (this.rtoDetail.pf_model_code != null) {
                  this.manuinput = this.rtoDetail.pf_manufactur;
                  this.modelcode = this.rtoDetail.pf_model_code;
                  this.rtoCode = this.rtoDetail.rto_cd;

                  this.api
                    .getRTOLocation(this.rtoCode, this.classCode)
                    .subscribe((res: any) => {
                      if (this.cs.isUndefineORNull(res)) {
                        this.freezeRTOCall();
                      } else {
                        this.quoteDetails.rtO_LOCATION_DESC = res;
                        let data = res.split("-");
                        if (data[0] == "TAMILNADU") {
                          this.quoteDetails.GSTToState = "TAMIL NADU";
                        } else {
                          this.quoteDetails.GSTToState = data[0];
                        }
                        this.isRTOFreeze = true;
                      }
                    });

                  this.api
                    .getModelName(this.modelcode, this.classCode)
                    .subscribe((res: any) => {
                      if (res.seatingCapacity != null) {
                        this.quoteDetails.vehiclemodel = res.vehicleName;
                        this.modelName = res.manufacturer;
                        this.makecode = res.makeCode;
                        this.cubiccapacity = res.cubicCapacity;
                        this.seatingcapacity = res.seatingCapacity;

                        if (
                          this.productCode == "2320" ||
                          this.productCode == "2312"
                        ) {
                          if (
                            this.rtoDetail.rto_cd != "" &&
                            this.rtoDetail.pf_model_code != "" &&
                            this.rtoDetail.rto_cd != "NA"
                          ) {
                            if (
                              this.rtoDetail.pf_model_code != null &&
                              this.seatingcapacity != null
                            ) {
                              this.modelcode = this.rtoDetail.pf_model_code;

                              if (this.showsaod == false) {
                                this.findexshowroom().then(() => {
                                  // this.cs.loaderStatus = false;
                                  this.calculatequote().then(() => {
                                    if (this.bFlag == true) {
                                      // this.cs.loaderStatus = false;
                                      this.spinloader = false;
                                      this.showAddon = false;
                                    } else {
                                      this.systemQC = true;
                                    }
                                  });
                                });
                              } else {
                                this.findexshowroom();
                                // this.cs.loaderStatus = false;
                                this.spinloader = false;
                              }

                              localStorage.setItem(
                                "ManufactureName",
                                this.rtoDetail.fla_maker_desc
                              );
                              this.getVehicleFlag = true;
                            } else {
                              // this.cs.loaderStatus = false;
                              this.spinloader = false;
                            }
                          } else {
                            // this.cs.loaderStatus = false;
                            this.spinloader = false;
                            // swal({
                            //   closeOnClickOutside: false,
                            //   text: 'Data not fetched from Carinfo, continue with manual data entry',
                            // });
                            // this.freezeRTOCall();
                            this.showElement = true;
                            setTimeout(() => {
                              this.showElement = false; // Here, value is updated
                            }, 5000);
                            // this.quoteDetails.rtO_LOCATION_DESC = "";
                            this.quoteDetails.vehiclemodel = "";
                            this.showIDV = false;
                          }
                        }
                      } else {
                        // this.cs.loaderStatus = false;
                        this.spinloader = false;

                        // swal({
                        //   closeOnClickOutside: false,
                        //   text: 'Data not fetched from Carinfo, continue with manual data entry',
                        // });

                        this.showElement = true;
                        setTimeout(() => {
                          this.showElement = false; // Here, value is updated
                        }, 5000);
                      }
                    });
                } else {
                  this.showElement = true;
                  this.spinloader = false;
                  this.freezeRTOCall();
                  // swal({
                  //   closeOnClickOutside: false,
                  //   text: 'Data not fetched from Carinfo, continue with manual data entry',
                  // });

                  setTimeout(() => {
                    this.showElement = false; // Here, value is updated
                  }, 5000);
                  // }

                  // this.cs.loaderStatus = false;
                  this.spinloader = false;
                  // this.quoteDetails.rtO_LOCATION_DESC = "";
                }
              } else if (res.vh_class_desc == "Private Car") {
                // this.cs.loaderStatus = false;
                this.spinloader = false;

                if (this.productCode == "2312") {
                  swal({
                    text: "Entered registration number is of 4 wheeler. Do you want to switch to Private Car ?",
                    dangerMode: true,
                    closeOnClickOutside: false,
                    buttons: ["Yes", "No"],
                  }).then((willDelete) => {
                    if (willDelete) {
                      console.log("Stay");
                      this.freezeRTOCall();
                    } else {
                      this.productCode = "2311";
                      this.getDeal();
                      this.classCode = "45";
                      this.TPPDLimit = 750000;
                      this.refreshToken("roll", "RTO").then(() => {
                        regNum = this.quoteDetails.RegistrationNumber;
                        this.getRTO4w();
                      });
                    }
                  });
                } else if (this.productCode == "2320") {
                  swal({
                    text: "You have entered Private Car registration number. Do you want to switch to Private Car TP?",
                    dangerMode: true,
                    closeOnClickOutside: false,
                    buttons: ["Yes", "No"],
                  }).then((willDelete) => {
                    if (willDelete) {
                      console.log("Stay");
                      this.freezeRTOCall();
                    } else {
                      this.productCode = "2319";
                      this.getDeal();
                      this.classCode = "45";
                      this.TPPDLimit = 750000;
                      this.refreshToken("roll", "RTO").then(() => {
                        regNum = this.quoteDetails.RegistrationNumber;
                        this.getRTO4w();
                      });
                    }
                  });
                }
              } else {
                this.cs.loaderStatus = false;
                this.spinloader = false;
                // this.freezeRTOCall();
                this.quoteDetails.RegistrationNumber = "";
                swal({
                  closeOnClickOutside: false,
                  text: "The registration number entered is of a commercial vehicle. Login to ipartner for a commercial vehicle Quote & policy booking",
                });

                this.showElement = true;
                setTimeout(() => {
                  this.showElement = false; // Here, value is updated
                }, 5000);
              }
            } else {
              if (res.IsSuccess) {
                if (!this.cs.isUndefineORNull(res.insuranceUpto)) {
                  this.quoteDetails.PreviousPolicyEndDate = moment(
                    res.insuranceUpto,
                    "DD-MMM-YYYY"
                  ).format("YYYY-MM-DD");
                  this.getPrevEndDate(this.quoteDetails.PreviousPolicyEndDate);
                }
                if (res.IsClaimMade == 'Y') {
                  if (res.ClaimDetails.length > 1) {
                    this.sortedClaimDetails = this.cs.sortByDOL(res.ClaimDetails);
                    console.log('Sorted', this.sortedClaimDetails);
                  } else {
                    this.sortedClaimDetails = res.ClaimDetails;
                  }
                  let DOL = moment(this.sortedClaimDetails[0].DateOfLoss).format("YYYY-MM-DD");
                  console.log(this.sortedClaimDetails[0])
                  console.log('For IIB', this.ncbValue);
                  var D1 = new Date(this.quoteDetails.PreviousPolicyStartDate);
                  var D2 = new Date(this.quoteDetails.PreviousPolicyEndDate);
                  var D3 = new Date(DOL);
                  if (
                    D3.getTime() <= D2.getTime() &&
                    D3.getTime() >= D1.getTime()
                  ) {
                    this.claimNCB = false;
                    this.ClaimOnPreviousPolicy = true;
                    this.quoteDetails.claims = "claimyes";
                    this.showIIBData = true;
                    this.IIBData = res.ClaimDetails;
                    this.quoteDetails.TotalNoOfODClaims = JSON.stringify(
                      res.TotalClaims
                    );
                    this.claimCount = res.TotalClaims;
                  } else {
                    this.claimNCB = true;
                    this.ClaimOnPreviousPolicy = false;
                    this.quoteDetails.claims = "claimno";
                    this.quoteDetails.TotalNoOfODClaims = "0";
                    this.showIIBData = false;
                    this.claimCount = 0;
                    let Time = new Date(this.quoteDetails.PolicyStartDate).getTime() - new Date(this.quoteDetails.PreviousPolicyEndDate).getTime();
                    let daysDiff = Time / (1000 * 3600 * 24);
                    console.log('Days diff', daysDiff);
                    if (daysDiff > 90) {
                      this.ncbValue = '0';
                    } else {
                      if (this.sortedClaimDetails[0].ClaimType == 'OD') {
                        this.ncbValue = this.cs.NCBLogic(DOL, this.quoteDetails.PolicyStartDate, this.sortedClaimDetails[0].ClaimType);
                      } else {
                        this.ncbValue = this.cs.NCBLogicOnDate(this.quoteDetails.PolicyStartDate, this.sortedClaimDetails[0].ClaimIntimationDate);
                      }
                    }
                  }
                } else {
                  this.claimNCB = true;
                  this.ClaimOnPreviousPolicy = false;
                  this.quoteDetails.claims = "claimno";
                  this.quoteDetails.TotalNoOfODClaims = "0";
                  this.showIIBData = false;
                  this.ncbValue = this.ncbValue;
                  this.claimCount = 0;
                }
              } else {
                this.claimNCB = true;
                this.ClaimOnPreviousPolicy = false;
                this.quoteDetails.claims = "claimno";
                this.quoteDetails.TotalNoOfODClaims = "0";
                this.showIIBData = false;
                this.ncbValue = this.ncbValue;
                this.claimCount = 0;
              }
              // this.cs.loaderStatus = false;
              console.log("Rahuls API call");
              this.spinloader = false;
              this.freezeRTOCall();

              // swal({
              //   closeOnClickOutside: false,
              //   text: 'Data not fetched from Carinfo, continue with manual data entry',
              // });

              this.showElement = true;
              setTimeout(() => {
                this.showElement = false; // Here, value is updated
              }, 5000);

              // this.cs.loaderStatus = false;
              this.spinloader = false;
            }
          },
          (err) => {
            // this.cs.loaderStatus = false;
            console.log("Rahuls API call");
            this.spinloader = false;
            this.freezeRTOCall();
            if (err.name == "TimeoutError") {
              // swal({
              //   closeOnClickOutside: false,
              //   text: 'Data not fetched from Carinfo, continue with manual data entry',
              // });

              // this.cs.showNotification( 'error', 'Data not fetched from Carinfo, continue with manual data entry')

              this.showElement = true;
              setTimeout(() => {
                this.showElement = false; // Here, value is updated
              }, 5000);
            }
          }
        );
      }
    } else {
      this.ncbValue = "0";
      swal({
        text: "Please enter registration number",
      });
    }
  }

  // Get RTO Details 4w
  getRTO4w() {
    
    // localStorage.removeItem('calQuoteReq');
    // localStorage.removeItem('ODAmount');
    this.quoteDetails.OtherDiscount = 0;
    localStorage.setItem('ODAmount', JSON.stringify(this.quoteDetails.OtherDiscount));
    this.isRecalculate1 = false;
    this.saod(this.showsaod);
    this.openQuoteSection = true;
    this.clearquoteform();
    this.quoteDetails.IsExtensionCountry = false;
    // this.IsSelfInspection = false;
    this.isZeroDepPlanName = false;
    this.isRSAPlan = false;
    this.quoteDetails.IsRTIApplicableflag = false;
    this.quoteDetails.IsConsumables = false;
    let regNum = this.quoteDetails.RegistrationNumber;
    this.getVehicleDetailsClicked = true;
    this.getrtoButtonclicked = true;
    // this.cs.loaderStatus = true;
    this.spinloader = true;
    
    if (this.quoteDetails.RegistrationNumber != "") {
     
      /**
       * Digital POS change
       * call getDeal function only for nysa not for POS
       * date :- 15-06-2021
       */
      if (this.IsPOSTransaction_flag == true) {
        console.info("Quote-deal-details-change");
        this.POSmobileNumber = JSON.parse(
          localStorage.getItem("POSmobileNumber")
        );
        let PosDealObj = {
          MobileNumber: this.POSmobileNumber,
          SubProductCode: this.productCode,
        };
        this.cs.GetPOSDealId(JSON.stringify(PosDealObj)).then((res: any) => {
          if (res.isSuccess == true) {
            this.dealid = res.dealID;
            localStorage.setItem("DealID", JSON.stringify(res.dealID));
          } else {
            swal({ text: "Deal not found" });
          }
        });
      } else {
        // GetDealFromIM? changes - Sejal - 21-01-2022
        if (this.cs.isUndefineORNull(localStorage.getItem("DealID"))) {
          this.getDeal();
        }
      }

      (<any>window).ga("send", "event", {
        eventCategory: "Car Info 4Wheeler",
        eventLabel: "" + regNum + "" + this.product + "" + this.policyType,
        eventAction: "Car Info Clicked",
        eventValue: 10,
      });
      // New Changes for Vahaan API
      if (
        (this.productCode == "2311" || this.productCode == "2319") &&
        this.isEnableCarInfoNewService
      ) {
        
        let body = {
          LoginId: "xyz",
          VehRegNo: regNum,
          Token:
            "WTJGeWFXNW1iMTlwWTJsamFWOXdjbVZ0YVhWdFgyRndhVjh5TURJd1gyUnljM05yY0E9PQ==",
          Key: "$!c!c!_2020",
        };
        let str = JSON.stringify(body);
        let GUID = Guid.raw();
        let key1 = GUID.split("-");
        let key2 =
          key1[0] + "-" + key1[1] + "-" + key1[2] + "-" + key1[3] + "-";
        // let encrypted1 = str+'|'+JSON.stringify(key2);
        let encrypted1 = str;
        let encrypted2 = this.cs.encryptUsingGUID(encrypted1, key2);
        let encrypted3 = encrypted2 + "|" + key2;
        let encrypted4 = this.cs.encryptLikePython(encrypted3);
        let body1 = {
          encryptedData: encrypted4,
        };
        let str1 = JSON.stringify(body1);
        console.log("Encrypted Final", encrypted4);
        // commented for IIB Change 
        // this.api.getRTODetailsForVahaan(str1).subscribe(
        this.api.getRTODetailsForVahaanNew(str1).subscribe(
          (res: any) => {
            this.ncbValue = "0";
            if (res.StatusType == "SUCCESS") {
              localStorage.removeItem("calQuoteReq");
              localStorage.removeItem("PreviousData");
              localStorage.removeItem("calQuoteRes");
              this.showPYP = true;
              
              if (res.IsSuccess) {
                
                if (!this.cs.isUndefineORNull(res.insuranceUpto)) {
                  this.quoteDetails.PreviousPolicyEndDate = moment(
                    res.insuranceUpto,
                    "DD-MMM-YYYY"
                  ).format("YYYY-MM-DD");
                  this.getPrevEndDate(this.quoteDetails.PreviousPolicyEndDate);
                }
                if (res.IsClaimMade == 'Y') {
                  if (res.ClaimDetails.length > 1) {
                    this.sortedClaimDetails = this.cs.sortByDOL(res.ClaimDetails);
                    console.log('Sorted', this.sortedClaimDetails);
                  } else {
                    this.sortedClaimDetails = res.ClaimDetails;
                  }
                  let DOL = moment(this.sortedClaimDetails[0].DateOfLoss).format("YYYY-MM-DD");
                  console.log(this.sortedClaimDetails[0])
                  console.log('For IIB', this.ncbValue);
                  var D1 = new Date(this.quoteDetails.PreviousPolicyStartDate);
                  var D2 = new Date(this.quoteDetails.PreviousPolicyEndDate);
                  var D3 = new Date(DOL);
                  console.log(D1, D2, this.quoteDetails.PreviousPolicyStartDate, this.quoteDetails.PreviousPolicyEndDate, D3);
                  if (
                    D3.getTime() <= D2.getTime() &&
                    D3.getTime() >= D1.getTime()
                  ) {
                    this.claimNCB = false;
                    this.ClaimOnPreviousPolicy = true;
                    this.quoteDetails.claims = "claimyes";
                    this.showIIBData = true;
                    this.IIBData = res.ClaimDetails;
                    this.quoteDetails.TotalNoOfODClaims = JSON.stringify(
                      res.TotalClaims
                    );
                    this.claimCount = res.TotalClaims;
                  } else {
                    this.claimNCB = true;
                    this.ClaimOnPreviousPolicy = false;
                    this.quoteDetails.claims = "claimno";
                    this.quoteDetails.TotalNoOfODClaims = "0";
                    this.showIIBData = false;
                    this.claimCount = 0;
                    let Time = new Date(this.quoteDetails.PolicyStartDate).getTime() - new Date(this.quoteDetails.PreviousPolicyEndDate).getTime();
                    let daysDiff = Time / (1000 * 3600 * 24);
                    console.log('Days diff', daysDiff);
                    if (daysDiff > 90) {
                      this.ncbValue = '0';
                    } else {
                      if (this.sortedClaimDetails[0].ClaimType == 'OD') {
                        this.ncbValue = this.cs.NCBLogic(DOL, this.quoteDetails.PolicyStartDate, this.sortedClaimDetails[0].ClaimType);
                      } else {
                        this.ncbValue = this.cs.NCBLogicOnDate(this.quoteDetails.PolicyStartDate, this.sortedClaimDetails[0].ClaimIntimationDate);
                      }
                    }
                  }
                } else {
                  this.claimNCB = true;
                  this.ClaimOnPreviousPolicy = false;
                  this.quoteDetails.claims = "claimno";
                  this.quoteDetails.TotalNoOfODClaims = "0";
                  this.showIIBData = false;
                  this.ncbValue = this.ncbValue;
                  this.claimCount = 0;
                }
              } else {
                this.claimNCB = true;
                this.ClaimOnPreviousPolicy = false;
                this.quoteDetails.claims = "claimno";
                this.quoteDetails.TotalNoOfODClaims = "0";
                this.showIIBData = false;
                this.ncbValue = this.ncbValue;
                this.claimCount = 0;
              }
              // this.api.getiibServices(regNum).subscribe((res: any) => {
              //   // this.quoteDetails.PreviousPolicyNumber = res.policyNumber;
              //   if (res.isClaimMade == "Y") {
              //     let index = res.totalClaims - 1;
              //     console.log(
              //       "ABC",
              //       this.quoteDetails.PreviousPolicyStartDate,
              //       this.sortedClaimDetails[0].dateOfLoss
              //     );
              //     console.log(
              //       "ABC1",
              //       this.quoteDetails.PreviousPolicyStartDate,
              //       this.quoteDetails.PreviousPolicyEndDate,
              //       moment(this.sortedClaimDetails[0].dateOfLoss).format(
              //         "YYYY-MM-DD"
              //       )
              //     );
              //     let dateCheck = moment(
              //       this.sortedClaimDetails[0].dateOfLoss
              //     ).format("YYYY-MM-DD");
              //     // let DOL = moment(this.sortedClaimDetails[0].dateOfLoss).format("YYYY-MM-DD");
              //     // let NCBValue = this.cs.NCBLogic(DOL, this.quoteDetails.PolicyStartDate, this.sortedClaimDetails[0].claimType);
              //     // console.log('New NCB Logic', NCBValue)
              //     var D1 = new Date(this.quoteDetails.PreviousPolicyStartDate);
              //     var D2 = new Date(this.quoteDetails.PreviousPolicyEndDate);
              //     var D3 = new Date(dateCheck);
              //     if (
              //       D3.getTime() <= D2.getTime() &&
              //       D3.getTime() >= D1.getTime()
              //     ) {
              //       this.claimNCB = false;
              //       this.ClaimOnPreviousPolicy = true;
              //       this.quoteDetails.claims = "claimyes";
              //       this.showIIBData = true;
              //       this.IIBData = res.claimDetails;
              //       this.quoteDetails.TotalNoOfODClaims = JSON.stringify(
              //         res.totalClaims
              //       );
              //       this.claimCount = res.totalClaims;
              //     } else {
              //       this.claimNCB = true;
              //       this.ClaimOnPreviousPolicy = false;
              //       this.quoteDetails.claims = "claimno";
              //       this.quoteDetails.TotalNoOfODClaims = "0";
              //       this.showIIBData = false;
              //       this.ncbValue = "20";
              //       this.claimCount = 0;
              //     }
              //   } else {
              //     this.claimNCB = true;
              //     this.ClaimOnPreviousPolicy = false;
              //     this.quoteDetails.claims = "claimno";
              //     this.quoteDetails.TotalNoOfODClaims = "0";
              //     this.showIIBData = false;
              //     this.ncbValue = this.ncbValue;
              //     this.claimCount = 0;
              //   }
              // });
              this.quoteDetails.PreviousPolicyNumber =
                res.previousInsurerPolicyNo;
              if (res.vh_class_desc == "Private Car") {
                
                this.rtoDetail = res;
                this.RTOname = "";
                let regDate = moment(
                  this.rtoDetail.regn_dt,
                  "DD-MMM-YYYY"
                ).format("YYYY-MM-DD");
                this.quoteDetails.FirstRegistrationDate = regDate;
                this.bindyear(regDate);
                this.quoteDetails.rtO_LOCATION_DESC = this.rtoDetail.rto_name;
                // this.cs
                //   .get("PrevPolicy/GetPreviousInsurer")
                //   .subscribe((res: any) => {
                //     console.log(res);
                //     let compname;
                //     // companyCode
                //     compname = res.find(
                //       (c) => c.shortName == this.rtoDetail.previousInsurerCode
                //     ).companyName;
                //     console.log(compname);
                //     if(this.cs.isUndefineORNull(compname)){
                //       this.quoteDetails.PreviousInsurerName = 'OTHER';
                //     }else{
                //       this.quoteDetails.PreviousInsurerName = compname;
                //     }
                //   });
                /**
                 * Get Previous InsurerName api change
                 * Author :- Sumit
                 * date :- 12-01-2022
                 */
                let previousInsurer: any = this.cs.PreviousInsurerList;;
                let compname;
                if (previousInsurer != undefined && previousInsurer != null) {
                  if (
                    this.rtoDetail.previousInsurerCode != "NA" &&
                    !this.cs.isUndefineORNull(
                      this.rtoDetail.previousInsurerCode
                    )
                  ) {
                    compname = previousInsurer.find(
                      (c) => c.shortName == this.rtoDetail.previousInsurerCode
                    );
                    console.log(compname);
                    if (this.cs.isUndefineORNull(compname)) {
                      this.quoteDetails.PreviousInsurerName = "OTHER";
                    } else {
                      this.quoteDetails.PreviousInsurerName =
                        compname.companyName;
                    }
                  } else {
                    compname = "OTHER";
                  }
                } else {
                  this.quoteDetails.PreviousInsurerName = "OTHER";
                }
                this.quoteDetails.PreviousPolicyNumber =
                  res.previousInsurerPolicyNo;
                if (
                  !this.cs.isUndefineORNull(res.previousInsurerPolicyNo) &&
                  !this.cs.isUndefineORNull(res.previousInsurerCode) &&
                  !this.cs.isUndefineORNull(res.previousInsurer) &&
                  (!this.cs.isUndefineORNull(res.insuranceUpto) ||
                    res.insuranceUpto != "NA")
                ) {
                  this.quoteDetails.PreviousPolicyDetails = true;
                } else {
                  this.showPYP = false;
                }
                if (
                  !this.cs.isUndefineORNull(res.insuranceUpto) ||
                  res.insuranceUpto != "NA"
                ) {
                  this.quoteDetails.PreviousPolicyEndDate = moment(
                    res.insuranceUpto,
                    "DD-MMM-YYYY"
                  ).format("YYYY-MM-DD");
                } else {
                  this.showPYP = false;
                }

                if (this.productCode == "2312" || this.productCode == "2311") {
                  this.getPrevEndDate(this.quoteDetails.PreviousPolicyEndDate);
                }

                if (this.rtoDetail.pf_model_code != null) {
                  this.rtoCode = this.rtoDetail.pf_rtocode;
                  this.manuinput = this.rtoDetail.pf_manufactur;
                  this.modelcode = this.rtoDetail.pf_model_code;
                  this.api
                    .getRTOLocation(this.rtoCode, this.classCode)
                    .subscribe((res: any) => {
                      if (this.cs.isUndefineORNull(res)) {
                        console.log("Rahuls API call");
                        this.freezeRTOCall();
                      } else {
                        this.quoteDetails.rtO_LOCATION_DESC = res;
                        let data = res.split("-");
                        if (data[0] == "TAMILNADU") {
                          this.quoteDetails.GSTToState = "TAMIL NADU";
                        } else {
                          this.quoteDetails.GSTToState = data[0];
                        }
                        this.isRTOFreeze = true;
                      }
                    });
                  this.api
                    .getModelName(this.modelcode, this.classCode)
                    .subscribe((res: any) => {
                      if (res.seatingCapacity != null) {
                        this.quoteDetails.vehiclemodel = res.vehicleName;
                        this.modelName = res.manufacturer;
                        this.makecode = res.makeCode;
                        this.cubiccapacity = res.cubicCapacity;
                        this.quoteDetails.vehiclemodel = res.vehicleName;
                        this.seatingcapacity = res.seatingCapacity;
                        if (
                          this.productCode == "2319" ||
                          this.productCode == "2311"
                        ) {
                          if (
                            this.rtoDetail.rto_cd != "" &&
                            this.rtoDetail.pf_model_code != "" &&
                            this.rtoDetail.rto_cd != "NA"
                          ) {
                            if (this.rtoDetail.pf_model_code != null) {
                              this.rtoCode = this.rtoDetail.rto_cd;
                              this.modelcode = this.rtoDetail.pf_model_code;
                              this.showIDV = true;
                              if (this.showsaod == false) {
                                this.findexshowroom().then(() => {
                                  this.calculatequote().then(() => {
                                    if (this.bFlag == true) {
                                      this.cs.loaderStatus = false;
                                      this.spinloader = false;
                                      this.showAddon = false;
                                    } else {
                                      this.systemQC = true;
                                    }
                                  });
                                });
                              } else {
                                this.findexshowroom();
                                this.cs.loaderStatus = false;
                                this.spinloader = false;
                              }
                            } else {
                              this.cs.loaderStatus = false;
                              this.spinloader = false;
                            }
                          } else {
                            this.cs.loaderStatus = false;
                            this.spinloader = false;
                            this.freezeRTOCall();
                            // swal({
                            //   closeOnClickOutside: false,
                            //   text: 'Data not fetched from Carinfo, continue with manual data entry',
                            // });

                            this.showElement = true;
                            setTimeout(() => {
                              this.showElement = false; // Here, value is updated
                            }, 5000);

                            this.cs.loaderStatus = false;
                            this.spinloader = false;
                            // this.quoteDetails.rtO_LOCATION_DESC = "";
                            this.quoteDetails.vehiclemodel = "";
                            this.showIDV = false;
                          }
                        }
                      } else {
                        // this.freezeRTOCall();
                        // swal({
                        //   closeOnClickOutside: false,
                        //   text: 'Data not fetched from Carinfo, continue with manual data entry',
                        // });

                        this.showElement = true;
                        setTimeout(() => {
                          this.showElement = false; // Here, value is updated
                        }, 5000);

                        this.cs.loaderStatus = false;
                        this.spinloader = false;
                        // this.quoteDetails.rtO_LOCATION_DESC = "";
                        this.quoteDetails.vehiclemodel = "";
                        this.showIDV = false;
                      }
                    });
                } else {
                  this.cs.loaderStatus = false;
                  this.spinloader = false;
                  this.showElement = true;
                  this.freezeRTOCall();
                  // this.quoteDetails.rtO_LOCATION_DESC = "";
                  setTimeout(() => {
                    this.showElement = false; // Here, value is updated
                  }, 5000);

                  // swal({
                  //   closeOnClickOutside: false,
                  //   text: 'Data not fetched from Carinfo, continue with manual data entry',
                  // });
                }
                localStorage.setItem("rtoDetail", JSON.stringify(res));
              } else if (res.vh_class_desc == "Two Wheeler") {
                this.cs.loaderStatus = false;
                this.spinloader = false;
                if (this.productCode == "2311") {
                  swal({
                    text: "Entered registration number is of Two-Wheeler wheeler. Do you want to switch to Two-Wheeler ?",
                    dangerMode: true,
                    closeOnClickOutside: false,
                    buttons: ["Yes", "No"],
                  }).then((willDelete) => {
                    if (willDelete) {
                      console.log("Stay");
                      this.freezeRTOCall();
                    } else {
                      this.productCode = "2312";
                      this.getDeal();
                      this.classCode = "37";
                      this.TPPDLimit = 100000;
                      this.refreshToken("roll", "RTO").then(() => {
                        regNum = this.quoteDetails.RegistrationNumber;
                        this.getRTO();
                      });
                    }
                  });
                } else if (this.productCode == "2319") {
                  swal({
                    text: "Entered registration number is of Two-Wheeler TP. Do you want to switch to Two-Wheeler TP ?",
                    dangerMode: true,
                    closeOnClickOutside: false,
                    buttons: ["Yes", "No"],
                  }).then((willDelete) => {
                    if (willDelete) {
                      console.log("Stay");
                      this.freezeRTOCall();
                    } else {
                      this.productCode = "2320";
                      this.getDeal();
                      this.classCode = "37";
                      this.TPPDLimit = 100000;
                      this.refreshToken("roll", "RTO").then(() => {
                        regNum = this.quoteDetails.RegistrationNumber;
                        this.getRTO();
                      });
                    }
                  });
                }
              } else {
                this.cs.loaderStatus = false;
                this.spinloader = false;
                this.quoteDetails.RegistrationNumber = "";
                // this.freezeRTOCall();
                swal({
                  closeOnClickOutside: false,
                  text: "The registration number entered is of a commercial vehicle. Login to ipartner for a commercial vehicle Quote & policy booking",
                });

                this.showElement = true;
                setTimeout(() => {
                  this.showElement = false; // Here, value is updated
                }, 5000);
              }
            } else {
              
              if (res.IsSuccess) {
                
                if (!this.cs.isUndefineORNull(res.insuranceUpto)) {
                  this.quoteDetails.PreviousPolicyEndDate = moment(
                    res.insuranceUpto,
                    "DD-MMM-YYYY"
                  ).format("YYYY-MM-DD");
                  this.getPrevEndDate(this.quoteDetails.PreviousPolicyEndDate);
                }
                if (res.IsClaimMade == 'Y') {
                  if (res.ClaimDetails.length > 1) {
                    this.sortedClaimDetails = this.cs.sortByDOL(res.ClaimDetails);
                    console.log('Sorted', this.sortedClaimDetails);
                  } else {
                    this.sortedClaimDetails = res.ClaimDetails;
                  }
                  let DOL = moment(this.sortedClaimDetails[0].DateOfLoss).format("YYYY-MM-DD");
                  console.log(this.sortedClaimDetails[0])
                  console.log('For IIB', this.ncbValue);
                  var D1 = new Date(this.quoteDetails.PreviousPolicyStartDate);
                  var D2 = new Date(this.quoteDetails.PreviousPolicyEndDate);
                  var D3 = new Date(DOL);
                  if (
                    D3.getTime() <= D2.getTime() &&
                    D3.getTime() >= D1.getTime()
                  ) {
                    this.claimNCB = false;
                    this.ClaimOnPreviousPolicy = true;
                    this.quoteDetails.claims = "claimyes";
                    this.showIIBData = true;
                    this.IIBData = res.ClaimDetails;
                    this.quoteDetails.TotalNoOfODClaims = JSON.stringify(
                      res.TotalClaims
                    );
                    this.claimCount = res.TotalClaims;
                  } else {
                    this.claimNCB = true;
                    this.ClaimOnPreviousPolicy = false;
                    this.quoteDetails.claims = "claimno";
                    this.quoteDetails.TotalNoOfODClaims = "0";
                    this.showIIBData = false;
                    this.claimCount = 0;
                    let Time = new Date(this.quoteDetails.PolicyStartDate).getTime() - new Date(this.quoteDetails.PreviousPolicyEndDate).getTime();
                    let daysDiff = Time / (1000 * 3600 * 24);
                    console.log('Days diff', daysDiff);
                    if (daysDiff > 90) {
                      this.ncbValue = '0';
                    } else {
                      if (this.sortedClaimDetails[0].ClaimType == 'OD') {
                        this.ncbValue = this.cs.NCBLogic(DOL, this.quoteDetails.PolicyStartDate, this.sortedClaimDetails[0].ClaimType);
                      } else {
                        this.ncbValue = this.cs.NCBLogicOnDate(this.quoteDetails.PolicyStartDate, this.sortedClaimDetails[0].ClaimIntimationDate);
                      }
                    }
                  }
                } else {
                  this.claimNCB = true;
                  this.ClaimOnPreviousPolicy = false;
                  this.quoteDetails.claims = "claimno";
                  this.quoteDetails.TotalNoOfODClaims = "0";
                  this.showIIBData = false;
                  this.ncbValue = this.ncbValue;
                  this.claimCount = 0;
                }
              } else {
                this.claimNCB = true;
                this.ClaimOnPreviousPolicy = false;
                this.quoteDetails.claims = "claimno";
                this.quoteDetails.TotalNoOfODClaims = "0";
                this.showIIBData = false;
                this.ncbValue = this.ncbValue;
                this.claimCount = 0;
              }
              localStorage.removeItem("calQuoteReq");
              localStorage.removeItem("calQuoteRes");
              this.showPYP = false; // PYP display issue - Sejal - 2-1-2022
              console.log("Rahuls API call");
              this.cs.loaderStatus = false;
              this.spinloader = false;
              this.showElement = true;
              this.freezeRTOCall();
              setTimeout(() => {
                this.showElement = false; // Here, value is updated
              }, 5000);
            }
          },
          (err) => {
            this.ncbValue = "0";
            localStorage.removeItem("calQuoteReq");
            localStorage.removeItem("calQuoteRes");
            this.showPYP = false; // PYP display issue - Sejal - 2-1-2022
            this.cs.loaderStatus = false;
            this.spinloader = false;
            console.log("Rahuls API call");
            this.freezeRTOCall();

            if (err.name == "TimeoutError") {
              // swal({
              //   closeOnClickOutside: false,
              //   text: 'Data not fetched from Carinfo, continue with manual data entry',
              // });
              this.showElement = true;
              setTimeout(() => {
                this.showElement = false; // Here, value is updated
              }, 5000);
            }
          }
        );
      } else {
        this.api.getRTODetails(regNum).subscribe(
          (res: any) => {
            this.ncbValue = "0";
            
            if (res.statusType == "SUCCESS") {
              if (res.IsSuccess) {
                if (!this.cs.isUndefineORNull(res.insuranceUpto)) {
                  this.quoteDetails.PreviousPolicyEndDate = moment(
                    res.insuranceUpto,
                    "DD-MMM-YYYY"
                  ).format("YYYY-MM-DD");
                  this.getPrevEndDate(this.quoteDetails.PreviousPolicyEndDate);
                }
                if (res.IsClaimMade == 'Y') {
                  if (res.ClaimDetails.length > 1) {
                    this.sortedClaimDetails = this.cs.sortByDOL(res.ClaimDetails);
                    console.log('Sorted', this.sortedClaimDetails);
                  } else {
                    this.sortedClaimDetails = res.ClaimDetails;
                  }
                  let DOL = moment(this.sortedClaimDetails[0].DateOfLoss).format("YYYY-MM-DD");
                  console.log('For IIB', this.ncbValue);
                  var D1 = new Date(this.quoteDetails.PreviousPolicyStartDate);
                  var D2 = new Date(this.quoteDetails.PreviousPolicyEndDate);
                  var D3 = new Date(DOL);
                  if (
                    D3.getTime() <= D2.getTime() &&
                    D3.getTime() >= D1.getTime()
                  ) {
                    this.claimNCB = false;
                    this.ClaimOnPreviousPolicy = true;
                    this.quoteDetails.claims = "claimyes";
                    this.showIIBData = true;
                    this.IIBData = res.claimDetails;
                    this.quoteDetails.TotalNoOfODClaims = JSON.stringify(
                      res.TotalClaims
                    );
                    this.claimCount = res.TotalClaims;
                  } else {
                    this.claimNCB = true;
                    this.ClaimOnPreviousPolicy = false;
                    this.quoteDetails.claims = "claimno";
                    this.quoteDetails.TotalNoOfODClaims = "0";
                    this.showIIBData = false;
                    this.claimCount = 0;
                    let Time = new Date(this.quoteDetails.PolicyStartDate).getTime() - new Date(this.quoteDetails.PreviousPolicyEndDate).getTime();
                    let daysDiff = Time / (1000 * 3600 * 24);
                    console.log('Days diff', daysDiff);
                    if (daysDiff > 90) {
                      this.ncbValue = '0';
                    } else {
                      if (this.sortedClaimDetails[0].ClaimType == 'OD') {
                        this.ncbValue = this.cs.NCBLogic(DOL, this.quoteDetails.PolicyStartDate, this.sortedClaimDetails[0].ClaimType);
                      } else {
                        this.ncbValue = this.cs.NCBLogicOnDate(this.quoteDetails.PolicyStartDate, this.sortedClaimDetails[0].ClaimIntimationDate);
                      }
                    }
                  }
                } else {
                  this.claimNCB = true;
                  this.ClaimOnPreviousPolicy = false;
                  this.quoteDetails.claims = "claimno";
                  this.quoteDetails.TotalNoOfODClaims = "0";
                  this.showIIBData = false;
                  this.ncbValue = this.ncbValue;
                  this.claimCount = 0;
                }
              } else {
                this.claimNCB = true;
                this.ClaimOnPreviousPolicy = false;
                this.quoteDetails.claims = "claimno";
                this.quoteDetails.TotalNoOfODClaims = "0";
                this.showIIBData = false;
                this.ncbValue = this.ncbValue;
                this.claimCount = 0;
              }
              // this.api.getiibServices(regNum).subscribe((res: any) => {
              //   // this.quoteDetails.PreviousPolicyNumber = res.policyNumber;
              //   if (res.isClaimMade == "Y") {
              //     let index = res.totalClaims - 1;
              //     console.log(
              //       "ABC",
              //       this.quoteDetails.PreviousPolicyStartDate,
              //       this.sortedClaimDetails[0].dateOfLoss
              //     );
              //     console.log(
              //       "ABC1",
              //       this.quoteDetails.PreviousPolicyStartDate,
              //       this.quoteDetails.PreviousPolicyEndDate,
              //       moment(this.sortedClaimDetails[0].dateOfLoss).format(
              //         "YYYY-MM-DD"
              //       )
              //     );
              //     let dateCheck = moment(
              //       this.sortedClaimDetails[0].dateOfLoss
              //     ).format("YYYY-MM-DD");

              //     var D1 = new Date(this.quoteDetails.PreviousPolicyStartDate);
              //     var D2 = new Date(this.quoteDetails.PreviousPolicyEndDate);
              //     var D3 = new Date(dateCheck);
              //     if (
              //       D3.getTime() <= D2.getTime() &&
              //       D3.getTime() >= D1.getTime()
              //     ) {
              //       this.claimNCB = false;
              //       this.ClaimOnPreviousPolicy = true;
              //       this.quoteDetails.claims = "claimyes";
              //       this.showIIBData = true;
              //       this.IIBData = res.claimDetails;
              //       this.quoteDetails.TotalNoOfODClaims = JSON.stringify(
              //         res.totalClaims
              //       );
              //       this.claimCount = res.totalClaims;
              //     } else {
              //       this.claimNCB = true;
              //       this.ClaimOnPreviousPolicy = false;
              //       this.quoteDetails.claims = "claimno";
              //       this.quoteDetails.TotalNoOfODClaims = "0";
              //       this.showIIBData = false;
              //       this.ncbValue = "20";
              //       this.claimCount = 0;
              //     }
              //   } else {
              //     this.claimNCB = true;
              //     this.ClaimOnPreviousPolicy = false;
              //     this.quoteDetails.claims = "claimno";
              //     this.quoteDetails.TotalNoOfODClaims = "0";
              //     this.showIIBData = false;
              //     this.ncbValue = this.ncbValue;
              //     this.claimCount = 0;
              //   }
              // });
              this.quoteDetails.PreviousPolicyNumber =
                res.previousInsurerPolicyNo;
              if (res.vh_class_desc == "Private Car") {
                this.rtoDetail = res;
                this.RTOname = "";
                let regDate = moment(
                  this.rtoDetail.regn_dt,
                  "DD-MMM-YYYY"
                ).format("YYYY-MM-DD");
                this.quoteDetails.FirstRegistrationDate = regDate;
                this.bindyear(regDate);
                this.quoteDetails.rtO_LOCATION_DESC = this.rtoDetail.rto_name;
                // this.cs
                //   .get("PrevPolicy/GetPreviousInsurer")
                //   .subscribe((res: any) => {
                //     let compname;
                //     compname = res.find(
                //       (c) => c.shortName == this.rtoDetail.previousInsurerCode
                //     ).companyName;
                //     if(this.cs.isUndefineORNull(compname)){
                //       this.quoteDetails.PreviousInsurerName = 'OTHER';
                //     }else{
                //       this.quoteDetails.PreviousInsurerName = compname;
                //     }
                //   });
                /**
                 * Get Previous InsurerName api change
                 * Author :- Sumit
                 * date :- 12-01-2022
                 */
                let previousInsurer: any = this.cs.PreviousInsurerList;;
                let compname;
                if (previousInsurer != undefined && previousInsurer != null) {
                  if (
                    this.rtoDetail.previousInsurerCode != "NA" &&
                    !this.cs.isUndefineORNull(
                      this.rtoDetail.previousInsurerCode
                    )
                  ) {
                    compname = previousInsurer.find(
                      (c) => c.shortName == this.rtoDetail.previousInsurerCode
                    );
                    console.log(compname);
                    if (this.cs.isUndefineORNull(compname)) {
                      this.quoteDetails.PreviousInsurerName = "OTHER";
                    } else {
                      this.quoteDetails.PreviousInsurerName =
                        compname.companyName;
                    }
                  } else {
                    compname = "OTHER";
                  }
                } else {
                  this.quoteDetails.PreviousInsurerName = "OTHER";
                }
                this.quoteDetails.PreviousPolicyNumber =
                  res.previousInsurerPolicyNo;
                if (
                  !this.cs.isUndefineORNull(res.previousInsurerPolicyNo) &&
                  !this.cs.isUndefineORNull(res.previousInsurerCode) &&
                  !this.cs.isUndefineORNull(res.previousInsurer) &&
                  (!this.cs.isUndefineORNull(res.insuranceUpto) ||
                    res.insuranceUpto != "NA")
                ) {
                  this.quoteDetails.PreviousPolicyDetails = true;
                } else {
                  this.showPYP = false;
                }
                if (
                  !this.cs.isUndefineORNull(res.insuranceUpto) ||
                  res.insuranceUpto != "NA"
                ) {
                  this.quoteDetails.PreviousPolicyEndDate = moment(
                    res.insuranceUpto,
                    "DD-MMM-YYYY"
                  ).format("YYYY-MM-DD");
                } else {
                  this.showPYP = false;
                }
                if (this.productCode == "2312" || this.productCode == "2311") {
                  this.getPrevEndDate(this.quoteDetails.PreviousPolicyEndDate);
                }

                if (this.rtoDetail.pf_model_code != null) {
                  this.rtoCode = this.rtoDetail.pf_rtocode;
                  this.manuinput = this.rtoDetail.pf_manufactur;
                  this.modelcode = this.rtoDetail.pf_model_code;
                  this.api
                    .getRTOLocation(this.rtoCode, this.classCode)
                    .subscribe((res: any) => {
                      //Preetam
                      console.log("Preetam 1", res);
                      if (this.cs.isUndefineORNull(res)) {
                        this.spinloader = false;
                        this.freezeRTOCall();
                      } else {
                        this.quoteDetails.rtO_LOCATION_DESC = res;
                        console.log("WE", this.quoteDetails.rtO_LOCATION_DESC);
                        let data = res.split("-");
                        if (data[0] == "TAMILNADU") {
                          this.quoteDetails.GSTToState = "TAMIL NADU";
                        } else {
                          this.quoteDetails.GSTToState = data[0];
                        }
                        this.isRTOFreeze = true;
                      }
                    });
                  this.api
                    .getModelName(this.modelcode, this.classCode)
                    .subscribe((res: any) => {
                      if (res.seatingCapacity != null) {
                        this.quoteDetails.vehiclemodel = res.vehicleName;
                        this.modelName = res.manufacturer;
                        this.makecode = res.makeCode;
                        this.cubiccapacity = res.cubicCapacity;
                        this.quoteDetails.vehiclemodel = res.vehicleName;
                        this.seatingcapacity = res.seatingCapacity;
                        if (
                          this.productCode == "2319" ||
                          this.productCode == "2311"
                        ) {
                          if (
                            this.rtoDetail.rto_cd != "" &&
                            this.rtoDetail.pf_model_code != "" &&
                            this.rtoDetail.rto_cd != "NA"
                          ) {
                            if (this.rtoDetail.pf_model_code != null) {
                              this.rtoCode = this.rtoDetail.rto_cd;
                              this.modelcode = this.rtoDetail.pf_model_code;
                              this.showIDV = true;
                              if (this.showsaod == false) {
                                this.findexshowroom().then(() => {
                                  this.calculatequote().then(() => {
                                    if (this.bFlag == true) {
                                      this.cs.loaderStatus = false;
                                      this.spinloader = false;
                                      this.showAddon = false;
                                    } else {
                                      this.systemQC = true;
                                    }
                                  });
                                });
                              } else {
                                this.findexshowroom();
                                this.cs.loaderStatus = false;
                                this.spinloader = false;
                              }
                            } else {
                              this.cs.loaderStatus = false;
                              this.spinloader = false;
                            }
                          } else {
                            // swal({
                            //   closeOnClickOutside: false,
                            //   text: 'Data not fetched from Carinfo, continue with manual data entry',
                            // });
                            this.cs.loaderStatus = false;
                            this.spinloader = false;
                            this.freezeRTOCall();
                            this.showElement = true;
                            setTimeout(() => {
                              this.showElement = false; // Here, value is updated
                            }, 5000);
                            // this.quoteDetails.rtO_LOCATION_DESC = "";
                            this.quoteDetails.vehiclemodel = "";
                            this.showIDV = false;
                          }
                        }
                      } else {
                        // swal({
                        //   closeOnClickOutside: false,
                        //   text: 'Data not fetched from Carinfo, continue with manual data entry',
                        // });
                        // this.freezeRTOCall();
                        this.showElement = true;
                        setTimeout(() => {
                          this.showElement = false; // Here, value is updated
                        }, 5000);

                        this.cs.loaderStatus = false;
                        this.spinloader = false;
                        // this.quoteDetails.rtO_LOCATION_DESC = "";
                        this.quoteDetails.vehiclemodel = "";
                        this.showIDV = false;
                      }
                    });
                } else {
                  this.cs.loaderStatus = false;
                  this.spinloader = false;
                  // this.quoteDetails.rtO_LOCATION_DESC = "";
                  // this.freezeRTOCall();
                  this.showElement = true;
                  setTimeout(() => {
                    this.showElement = false; // Here, value is updated
                  }, 5000);

                  // swal({
                  //   closeOnClickOutside: false,
                  //   text: 'Data not fetched from Carinfo, continue with manual data entry',
                  // });
                }
                localStorage.setItem("rtoDetail", JSON.stringify(res));
              } else if (res.vh_class_desc == "Two Wheeler") {
                this.cs.loaderStatus = false;
                this.spinloader = false;
                if (this.productCode == "2311") {
                  swal({
                    text: "Entered registration number is of Two-Wheeler wheeler. Do you want to switch to Two-Wheeler ?",
                    dangerMode: true,
                    closeOnClickOutside: false,
                    buttons: ["Yes", "No"],
                  }).then((willDelete) => {
                    if (willDelete) {
                      console.log("Stay");
                      this.freezeRTOCall();
                    } else {
                      this.productCode = "2312";
                      this.getDeal();
                      this.classCode = "37";
                      this.TPPDLimit = 100000;
                      this.refreshToken("roll", "RTO").then(() => {
                        regNum = this.quoteDetails.RegistrationNumber;
                        this.getRTO();
                      });
                    }
                  });
                } else if (this.productCode == "2319") {
                  swal({
                    text: "Entered registration number is of Two-Wheeler TP. Do you want to switch to Two-Wheeler TP ?",
                    dangerMode: true,
                    closeOnClickOutside: false,
                    buttons: ["Yes", "No"],
                  }).then((willDelete) => {
                    if (willDelete) {
                      console.log("Stay");
                      this.freezeRTOCall();
                    } else {
                      this.productCode = "2320";
                      this.getDeal();
                      this.classCode = "37";
                      this.TPPDLimit = 100000;
                      this.refreshToken("roll", "RTO").then(() => {
                        regNum = this.quoteDetails.RegistrationNumber;
                        this.getRTO();
                      });
                    }
                  });
                }
              } else {
                this.cs.loaderStatus = false;
                this.spinloader = false;
                this.quoteDetails.RegistrationNumber = "";
                // this.freezeRTOCall();
                swal({
                  closeOnClickOutside: false,
                  text: "The registration number entered is of a commercial vehicle. Login to ipartner for a commercial vehicle Quote & policy booking",
                });

                this.showElement = true;
                setTimeout(() => {
                  this.showElement = false; // Here, value is updated
                }, 5000);
              }
            } else {
              console.log("Rahuls API call");
              this.cs.loaderStatus = false;
              this.spinloader = false;
              this.freezeRTOCall();
              this.showElement = true;
              setTimeout(() => {
                this.showElement = false; // Here, value is updated
              }, 5000);
            }
          },
          (err) => {
            this.ncbValue = "0";
            this.cs.loaderStatus = false;
            this.spinloader = false;
            console.log("Rahuls API call");
            this.freezeRTOCall();
            if (err.name == "TimeoutError") {
              // swal({
              //   closeOnClickOutside: false,
              //   text: 'Data not fetched from Carinfo, continue with manual data entry',
              // });
              this.showElement = true;
              setTimeout(() => {
                this.showElement = false; // Here, value is updated
              }, 5000);
            }
          }
        );
      }
    } else {
      this.ncbValue = "0";
      swal({
        text: "Please enter registration number",
      });
    }
  }

  // getRedisQuoteExShowroom(polStartDate:any, firRegDate:any):Promise<any>{
  //   return new Promise((resolve:any) => {
  //   let body = {
  //   "manufacturercode": this.makecode,
  //   "BusinessType": BusinessType,
  //   "rtolocationcode": this.rtoCode,
  //   "DeliveryOrRegistrationDate": firRegDate,
  //   "PolicyStartDate": polStartDate,
  //   "DealID":  this.dealid,
  //   "vehiclemodelcode": this.modelcode,
  //   "productcode" : this.productCode,
  //   "correlationId": ""
  //   }
  //   let str = JSON.stringify(body);
  //   this.cs.getRedisExShowroom('getExshowroomPrice',str).subscribe(
  //     (res: any) => {
  //       this.isIDVChangeMSG = false;
  //       this.isDeviationTriggered = false;
  //       console.log("Exshowroom", res);
  //       if (res.status == "Failed") {
  //         swal({
  //           text: "Ex-Showroom Price not available for this Model. Please contact your Relationship Manager",
  //         });
  //         this.cs.loaderStatus = false;
  //         this.showIDV = false;
  //         let errorbody = {
  //           RequestJson: str,
  //           ResponseJson: JSON.stringify(res),
  //           ServiceURL: 'https://nysa-ups-engine.insurancearticlez.com/getExshowroomPrice',
  //         };
  //         this.api.adderrorlogs(errorbody);
  //         resolve();
  //       } else {
  //         this.exShowroomPrice = res;
  //         localStorage.setItem('exShowData', JSON.stringify(res));
  //         this.exshowroomprice = this.exShowroomPrice.exshowRoomPrice;
  //         this.idv = this.exShowroomPrice.idv;
  //         this.lowerLimit = this.exShowroomPrice.lowerlimit;
  //         this.upperLimit = this.exShowroomPrice.upperlimit;
  //         this.idvChange();
  //         if (this.idv != null) {
  //           this.showIDV = true;
  //         }
  //         resolve();
  //       }
  //     });
  //   })
  // }

  getRedisQuoteExShowroom(polStartDate: any, firRegDate: any): Promise<any> {
    return new Promise((resolve: any) => {
      this.id = Guid.raw();
      let body = {
        DealID: this.dealid,
        correlationId: this.id,
        DeliveryOrRegistrationDate: firRegDate,
        BusinessType: this.policyType == "NEW" ? "New Business" : "Roll Over",
        manufacturercode: this.makecode,
        vehiclemodelcode: this.modelcode,
        rtolocationcode: this.rtoCode,
        PolicyStartDate: polStartDate,
      };
      let str = JSON.stringify(body);
      this.cs
        .postExBiz("/generic/getvehicledetails", str)
        .subscribe((res: any) => {
          this.showGetQuoteBtn = true
          this.isIDVChangeMSG = false;
          this.isDeviationTriggered = false;
          console.log("Exshowroom", res);
          if (!res.status) {
            swal({
              text: "Ex-Showroom Price not available for this Model. Please contact your Relationship Manager",
            });
            this.cs.loaderStatus = false;
            this.spinloader = false;
            this.showIDV = false;
            let errorbody = {
              RequestJson: str,
              ResponseJson: JSON.stringify(res),
              ServiceURL: "/generic/getvehicledetails",
            };
            this.api.adderrorlogs(errorbody);
            resolve();
          } else {
            this.exShowroomPrice = res;
            localStorage.setItem("exShowData", JSON.stringify(res));
            if (this.productCode == '2311' || this.productCode == '2319') {
              this.exshowroomprice = this.exShowroomPrice.vehiclesellingprice;
              // let idv = parseInt(this.exshowroomprice) - (this.exshowroomprice * res.idv)
              let idv =
                res.vehiclesellingprice - res.vehiclesellingprice * res.idvdepreciationpercent;
              console.log("IDV", idv, Math.floor(idv));
              this.idv = Math.floor(idv);
              this.lowerLimit = this.exShowroomPrice.minidv;
              this.upperLimit = this.exShowroomPrice.maxidv;
              this.idvChange();
              if (this.idv != null) {
                this.showIDV = true;
              }
            } else {
              this.exshowroomprice = this.exShowroomPrice.maximumprice;
              // let idv = parseInt(this.exshowroomprice) - (this.exshowroomprice * res.idv)
              let idv =
                res.maximumprice - res.maximumprice * res.idvdepreciationpercent;
              console.log("IDV", idv, Math.floor(idv));
              this.idv = Math.floor(idv);
              this.lowerLimit = this.exShowroomPrice.minidv;
              this.upperLimit = this.exShowroomPrice.maxidv;
              this.idvChange();
              if (this.idv != null) {
                this.showIDV = true;
              }
            }

            resolve();
          }
        });
    });
  }

  freezeRTOCall() {
    let code = this.quoteDetails.RegistrationNumber.substring(0, 4);
    console.log(code, this.classCode);
    this.spinloader = false;
    this.api.freezeRTOData(code, this.classCode).subscribe((res: any) => {
      if (res.status == "Success") {
        this.rtoList = res;
        this.isRTOFreeze = true;
        this.quoteDetails.rtO_LOCATION_DESC = res.rtoName;
        this.rtoCode = res.rtoCode;
        if (this.cs.isUndefineORNull(res.state)) {
          let data = res.split("-");
          if (data[0] == "TAMILNADU") {
            this.quoteDetails.GSTToState = "TAMIL NADU";
          } else {
            this.quoteDetails.GSTToState = data[0];
          }
        } else {
          this.quoteDetails.GSTToState = res.state;
        }
        this.spinloader = false;
      } else {
        this.quoteDetails.rtO_LOCATION_DESC = "";
        this.isRTOFreeze = false;
        this.spinloader = false;
      }
    });
  }

  // Validation
  validate() {
    this.isRecalculate1 = false;
    this.isDeviationTriggered = false;
    localStorage.removeItem("ODAmount");
    // Sejal - 22-02-2022 - CPA Tenure new requirement for 2WTP & 4WTP NEW
    // if (this.productCode == "2319" || this.productCode == "2320") {
    //   this.quoteDetails.PACoverTenure = "1";
    // }
    if (this.cs.isUndefineORNull(this.quoteDetails.PACoverTenure)) {
      this.quoteDetails.PACoverTenure = "1";
    } else {
      this.quoteDetails.PACoverTenure = this.quoteDetails.PACoverTenure;
    }
    if (
      this.quoteDetails.rtO_LOCATION_DESC == null ||
      this.quoteDetails.rtO_LOCATION_DESC == undefined ||
      this.quoteDetails.rtO_LOCATION_DESC == ""
    ) {
      this.isRTOValidate = false;
    }
    let regYear = moment(this.quoteDetails.FirstRegistrationDate).format(
      "YYYY"
    );
    this.checkdate = moment(this.quoteDetails.PolicyStartDate).isSameOrAfter(
      this.quoteDetails.FirstRegistrationDate,
      "days"
    );
    if (
      this.quoteDetails.RegistrationNumber == null ||
      this.quoteDetails.RegistrationNumber == undefined ||
      this.quoteDetails.RegistrationNumber == ""
    ) {
      swal({ text: "Kindly insert registration Number" });
    } else if (
      this.quoteDetails.RegistrationNumber.length < 5 &&
      this.quoteDetails.RegistrationNumber != "NEW"
    ) {
      this.isRegNumberValid = false;
    } else if (
      this.quoteDetails.RegistrationNumber.length < 5 &&
      this.quoteDetails.RegistrationNumber == "NEW" &&
      (this.policyType == "ROLL" || this.policyType == "USED")
    ) {
      this.isRegNumberValid = false;
    } else if (
      this.quoteDetails.rtO_LOCATION_DESC == null ||
      this.quoteDetails.rtO_LOCATION_DESC == undefined ||
      this.quoteDetails.rtO_LOCATION_DESC == ""
    ) {
      this.isRegNumberValid = true;
      this.isCityValidate = false;
    } else if (this.rtoList != undefined && this.rtoList.length == 0) {
      this.isRegNumberValid = true;
      this.isRTOValidate = false;
      this.isCityValidate = true;
      swal({ text: "Select correct RTO location" });
    } else if (
      this.quoteDetails.vehiclemodel == null ||
      this.quoteDetails.vehiclemodel == undefined ||
      this.quoteDetails.vehiclemodel == ""
    ) {
      this.isRegNumberValid = true;
      this.isRTOValidate = true;
      this.isCityValidate = true;
      this.isManValidate = false;
    } else if (this.manumod != undefined && this.manumod.length == 0) {
      this.isRegNumberValid = true;
      this.isRTOValidate = true;
      this.isModelValidate = false;
      this.isCityValidate = true;
      this.isManValidate = true;
      swal({ text: "Select correct manufacturer model" });
    } else if (
      this.quoteDetails.FirstRegistrationDate == null ||
      this.quoteDetails.FirstRegistrationDate == undefined ||
      this.quoteDetails.FirstRegistrationDate == ""
    ) {
      this.isRegNumberValid = true;
      this.isRTOValidate = true;
      this.isModelValidate = true;
      this.isCityValidate = true;
      this.isManValidate = true;
      swal({ text: "Kindly select first registration date" });
    } else if (
      this.quoteDetails.PolicyStartDate == null ||
      this.quoteDetails.PolicyStartDate == undefined ||
      this.quoteDetails.PolicyStartDate == ""
    ) {
      swal({ text: "Kindly select policy start date" });
    } else if (this.checkdate == false) {
      swal({
        text: "Policy Start Date can not be less than First Registration Date",
      });
    } else if (
      this.quoteDetails.GSTToState == null ||
      this.quoteDetails.GSTToState == undefined ||
      this.quoteDetails.GSTToState == ""
    ) {
      swal({ text: "Kindly select state" });
    } else if (
      this.quoteDetails.ManufacturingYear == null ||
      this.quoteDetails.ManufacturingYear == undefined ||
      this.quoteDetails.ManufacturingYear == ""
    ) {
      swal({ text: "Kindly select manufacturing year" });
    } else if (
      this.quoteDetails.PACoverTenure == null ||
      this.quoteDetails.PACoverTenure == undefined ||
      this.quoteDetails.PACoverTenure == ""
    ) {
      swal({ text: "Kindly select CPA Tenure" });
    } else if (
      this.registrationType == null ||
      this.registrationType == undefined ||
      this.registrationType == ""
    ) {
      swal({ text: "Kindly select registration type" });
    } else if (this.quoteDetails.ManufacturingYear > regYear) {
      swal({
        text: "Year of Manufacture can not be greater then registration year",
      });
    } else if (this.policyType == "NEW" && this.showPrev == true) {
      if (
        this.quoteDetails.PreviousPolicyEndDate == null ||
        this.quoteDetails.PreviousPolicyEndDate == undefined ||
        this.quoteDetails.PreviousPolicyEndDate == ""
      ) {
        swal({ text: "Previous policy end date is not entered" });
      } else if (
        this.quoteDetails.PreviousVehicleSaleDate == null ||
        this.quoteDetails.PreviousVehicleSaleDate == undefined ||
        this.quoteDetails.PreviousVehicleSaleDate == ""
      ) {
        swal({ text: "Select previous vehicle sale date" });
      } else if (
        this.claimNCB == false &&
        (this.quoteDetails.TotalNoOfODClaims == null ||
          this.quoteDetails.TotalNoOfODClaims == undefined ||
          this.quoteDetails.TotalNoOfODClaims == "")
      ) {
        swal({ text: "Enter number of claims intimated on previous policy" });
      } else if (
        this.claimNCB == true &&
        this.cs.isUndefineORNull(this.ncbValue)
      ) {
        swal({ text: "Kindly select NCB on Previous Policy" });
      } else {
        /**
         * digitalPOS Change For 5000000 limit
         * date :- 29-07-2021
         */
        if (this.IsPOSTransaction_flag == true) {
          if (this.exshowroomprice >= 5000000) {
            this.btn_disable_flag = true;
            swal({
              closeOnClickOutside: false,
              text: "Unable to process transaction as Sum Insured value is more than 50 Lacs.",
            });
          } else {
            this.finddataforquote().then(() => {
              this.calculatequote();
            });
          }
        } else {
          this.finddataforquote().then(() => {
            this.calculatequote();
          });
        }
      }
    } else if (
      (this.policyType == "ROLL" || this.policyType == "USED") &&
      this.productCode != "2320" &&
      this.productCode != "2319"
    ) {
      if (
        this.quoteDetails.Tenure == null ||
        this.quoteDetails.Tenure == undefined ||
        this.quoteDetails.Tenure == ""
      ) {
        swal({ text: "Kindly select Tenure" });
      } else if (
        this.showsaod == true &&
        (this.quoteDetails.PreviousPolicyEndDate == null ||
          this.quoteDetails.PreviousPolicyEndDate == undefined ||
          this.quoteDetails.PreviousPolicyEndDate == "")
      ) {
        swal({ text: "Previous policy end date is not entered" });
      } else if (
        this.showsaod == true &&
        (this.quoteDetails.PreviousInsurerName == null ||
          this.quoteDetails.PreviousInsurerName == undefined ||
          this.quoteDetails.PreviousInsurerName == "")
      ) {
        swal({ text: "Kindly select previous policy Insurer Name" });
      } else if (
        this.showsaod == true &&
        (this.quoteDetails.PreviousPolicyNumber == null ||
          this.quoteDetails.PreviousPolicyNumber == undefined ||
          this.quoteDetails.PreviousPolicyNumber == "")
      ) {
        swal({ text: "Previous policy number is not entered" });
      } else if (
        this.showsaod == true &&
        this.claimNCB == false &&
        (this.quoteDetails.TotalNoOfODClaims == null ||
          this.quoteDetails.TotalNoOfODClaims == undefined ||
          this.quoteDetails.TotalNoOfODClaims == "")
      ) {
        swal({ text: "Kindly insert previous policy total no of claims" });
      } else if (
        this.showsaod == true &&
        this.claimNCB == true &&
        this.cs.isUndefineORNull(this.ncbValue)
      ) {
        swal({ text: "Kindly select NCB on Previous Policy" });
      } else if (
        this.showsaod == true &&
        (this.quoteDetails.TPStartDate == null ||
          this.quoteDetails.TPStartDate == undefined ||
          this.quoteDetails.TPStartDate == "")
      ) {
        swal({ text: "Select third party start date" });
      } else if (
        this.showsaod == true &&
        (this.quoteDetails.TPEndDate == null ||
          this.quoteDetails.TPEndDate == undefined ||
          this.quoteDetails.TPEndDate == "")
      ) {
        swal({ text: "Select third party end date" });
      } else if (
        this.showsaod == true &&
        (this.quoteDetails.TPInsurerName == null ||
          this.quoteDetails.TPInsurerName == undefined ||
          this.quoteDetails.TPInsurerName == "")
      ) {
        swal({ text: "Select third party insurer name" });
      } else if (
        this.showsaod == true &&
        (this.quoteDetails.TPPolicyNo == null ||
          this.quoteDetails.TPPolicyNo == undefined ||
          this.quoteDetails.TPPolicyNo == "")
      ) {
        swal({ text: "Insert third party policy number" });
      } else if (this.quoteDetails.PreviousPolicyDetails == true) {
        if (
          this.quoteDetails.PreviousPolicyEndDate == null ||
          this.quoteDetails.PreviousPolicyEndDate == undefined ||
          this.quoteDetails.PreviousPolicyEndDate == ""
        ) {
          swal({ text: "Kindly select previous policy End date" });
        } else if (
          this.claimNCB == false &&
          (this.quoteDetails.TotalNoOfODClaims == null ||
            this.quoteDetails.TotalNoOfODClaims == undefined ||
            this.quoteDetails.TotalNoOfODClaims == "")
        ) {
          swal({ text: "Kindly insert previous policy total no of claims" });
        } else if (
          this.claimNCB == true &&
          this.cs.isUndefineORNull(this.ncbValue)
        ) {
          swal({ text: "Kindly select NCB on Previous Policy" });
        } else {
          /**
           * digitalPOS Change For 5000000 limit
           * date :- 29-07-2021
           */
          if (this.IsPOSTransaction_flag == true) {
            if (this.exshowroomprice >= 5000000) {
              this.btn_disable_flag = true;
              swal({
                closeOnClickOutside: false,
                text: "Unable to process transaction as Sum Insured value is more than 50 Lacs.",
              });
            } else {
              this.finddataforquote().then(() => {
                this.calculatequote();
              });
            }
          } else {
            this.finddataforquote().then(() => {
              this.calculatequote();
            });
          }
        }
      } else {
        /**
         * digitalPOS Change For 5000000 limit
         * date :- 16-07-2021
         */
        if (this.IsPOSTransaction_flag == true) {
          if (this.exshowroomprice >= 5000000) {
            this.btn_disable_flag = true;
            swal({
              closeOnClickOutside: false,
              text: "Unable to process transaction as Sum Insured value is more than 50 Lacs.",
            });
          } else {
            this.finddataforquote().then(() => {
              this.calculatequote();
            });
          }
        } else {
          this.finddataforquote().then(() => {
            this.calculatequote();
          });
        }
      }
    } else {
      /**
       * digitalPOS Change For 5000000 limit
       * date :- 29-07-2021
       */
      if (this.IsPOSTransaction_flag == true) {
        if (this.exshowroomprice >= 5000000) {
          this.btn_disable_flag = true;
          swal({
            closeOnClickOutside: false,
            text: "Unable to process transaction as Sum Insured value is more than 50 Lacs.",
          });
        } else {
          this.finddataforquote().then(() => {
            this.calculatequote();
          });
        }
      } else {
        this.finddataforquote().then(() => {
          this.calculatequote();
        });
      }
    }
  }

  // All Risk Electric Bike - Sejal - 25-02-2022
  validate1() {
    //Sejal - 28-02-2022
    //console.info(this.quoteDetails);
    // Sejal - 09-03-2022
    let EVquoteFormData: any;
    EVquoteFormData = {
      TransactionType: this.quoteDetails.TransactionType,
      BatteryNumber: this.quoteDetails.batterysaver,
      ManufacturingYear: this.quoteDetails.ManufacturingYear,
      CustomerType: this.registrationType,
      PurchaseDate: this.quoteDetails.PurchaseDate,
      Manufacturer: this.quoteDetails.Manufacturer,
      Model: this.quoteDetails.model,
      PolicyStartDate: moment(this.quoteDetails.PolicyStartDate).format("YYYY-MM-DD"),
      PolicyEndDate: moment(this.quoteDetails.PolicyEndDate).format("YYYY-MM-DD"),
      CustomerState: this.quoteDetails.GSTToState,
      SumInsuredIDV: this.quoteDetails.SumInsuredIDV,
      UniqueIDnumber: this.quoteDetails.UniqueIDnumber,
      InvoiceNumber: this.quoteDetails.InvoiceNumber
    }

    // this.allriskEBModel = [
    //   { id: 1, text: "INSPIRER" },
    //   { id: 2, text: "JAUNTY" },
    //   { id: 3, text: "SPIN LED" }
    // ]

    // localStorage.setItem("EVQuoteReq", JSON.stringify(EVquoteFormData));

    if (
      this.quoteDetails.batterysaver == null ||
      this.quoteDetails.batterysaver == undefined ||
      this.quoteDetails.batterysaver == ""
    ) {
      swal({ text: "Kindly insert Battery Saver Number" });
    } else if (
      this.quoteDetails.batterysaver.length > 50 ||
      this.quoteDetails.batterysaver === "00000000000" ||
      this.isBatterySaverValid == false
    ) {
      swal({ text: "Kindly insert valid Battery Saver Number" });
    } else if (
      this.quoteDetails.ManufacturingYear == null ||
      this.quoteDetails.ManufacturingYear == undefined ||
      this.quoteDetails.ManufacturingYear == ""
    ) {
      this.isBatterySaverValid = true;
      swal({ text: "Kindly select manufacturing year" });
    } else if (
      this.quoteDetails.PurchaseDate == null ||
      this.quoteDetails.PurchaseDate == undefined ||
      this.quoteDetails.PurchaseDate == ""
    ) {
      this.isBatterySaverValid = true;
      swal({ text: "Kindly select purchase date" });
    } else if (
      this.quoteDetails.Manufacturer == null ||
      this.quoteDetails.Manufacturer == undefined ||
      this.quoteDetails.Manufacturer == ""
    ) {
      this.isBatterySaverValid = true;
      this.isPurchaseDateValid = true;
      this.isManValidate = false;
      swal({ text: "Kindly select Manufacturer" });
    } else if (
      this.allriskEBModel.some((c: any) => c.make === this.quoteDetails.Manufacturer) == false
    ) {
      console.log(this.quoteDetails.Manufacturer);
      swal({ text: "Kindly select Manufacturer from dropdown list" });
    }else if (
      this.quoteDetails.model == null ||
      this.quoteDetails.model == undefined ||
      this.quoteDetails.model == ""
    ) {
      this.isBatterySaverValid = true;
      this.isPurchaseDateValid = true;
      this.isManValidate = true;
      swal({ text: "Kindly select Model" });
    } else if (
      this.allriskEBModel.some((c: any) => c.model === this.quoteDetails.model) == false
    ) {
      swal({ text: "Kindly select Model from dropdown list" });
    } else if (
      this.quoteDetails.PolicyStartDate == null ||
      this.quoteDetails.PolicyStartDate == undefined ||
      this.quoteDetails.PolicyStartDate == ""
    ) {
      this.isBatterySaverValid = true;
      this.isPurchaseDateValid = true;
      this.isManValidate = true;
      this.isModelValidate = true;
      swal({ text: "Kindly select policy start date" });
    } else if (this.checkdate == false) {
      this.isBatterySaverValid = true;
      this.isPurchaseDateValid = true;
      this.isManValidate = true;
      this.isModelValidate = true;
      swal({
        text: "Policy Start Date can not be less than Purchase Date",
      });
    } else if (
      this.quoteDetails.PolicyEndDate == null ||
      this.quoteDetails.PolicyEndDate == undefined ||
      this.quoteDetails.PolicyEndDate == ""
    ) {
      this.isBatterySaverValid = true;
      this.isPurchaseDateValid = true;
      this.isManValidate = true;
      this.isModelValidate = true;
      swal({ text: "Policy end date is not entered" });
    } else if (
      this.quoteDetails.GSTToState == null ||
      this.quoteDetails.GSTToState == undefined ||
      this.quoteDetails.GSTToState == ""
    ) {
      swal({ text: "Kindly select state" });
    } else if (
      this.quoteDetails.UniqueIDnumber == null ||
      this.quoteDetails.UniqueIDnumber == undefined ||
      this.quoteDetails.UniqueIDnumber == ""
    ) {
      this.isBatterySaverValid = true;
      this.isPurchaseDateValid = true;
      this.isManValidate = true;
      this.isModelValidate = true;
      this.isStateValid = true;
      swal({ text: "Kindly insert Unique Identification Number" });
    } else if (
      this.quoteDetails.UniqueIDnumber.length <= 0 ||
      this.quoteDetails.UniqueIDnumber === "00000000000000000" ||
      this.isUniqueNumberValid == false
    ) {
      swal({ text: "Kindly insert valid Unique Identification Number" });
    } else if (
      this.quoteDetails.InvoiceNumber == null ||
      this.quoteDetails.InvoiceNumber == undefined ||
      this.quoteDetails.InvoiceNumber == ""
    ) {
      this.isBatterySaverValid = true;
      this.isPurchaseDateValid = true;
      this.isManValidate = true;
      this.isModelValidate = true;
      this.isStateValid = true;
      this.isUniqueNumberValid = true;
      swal({ text: "Kindly insert Invoice Number" });
    } else if (
      this.quoteDetails.InvoiceNumber.length <= 0 ||
      this.quoteDetails.InvoiceNumber === "00000000" ||
      this.isInvoiceValid == false
    ) {
      swal({ text: "Kindly insert valid Invoice Number" });
    } else {
      this.isBatterySaverValid = true;
      this.isPurchaseDateValid = true;
      this.isManValidate = true;
      this.isModelValidate = true;
      this.isStateValid = true;
      this.isUniqueNumberValid = true;
      this.isInvoiceValid = true;

      if (this.registrationType == "INDIVIDUAL") {
        this.isValidSCPA = true;
        this.isSCPA = true;
      } else {
        this.isValidSCPA = false;
        this.isSCPA = false;
      }
      localStorage.setItem("IsSCPA", this.isSCPA.toString());

      if (this.isSCPA == true && this.isValidSCPA == true) {
        this.calculateAllriskEBquote().then(() => {
          this.calculateSCPA();
        })
      } else {
        this.calculateAllriskEBquote();
      }
    }

    // if (this.isRecalculate1) {
    //   this.panelOpenStateProposal = false;
    //   this.setStep(2);
    // } else {
    //   this.panelOpenStateProposal = false;
    //   this.setStep(1);
    // }

    //this.calculatequote();
  }

  findexshowroom(): Promise<any> {
    if (
      this.quoteDetails.rtO_LOCATION_DESC != null &&
      this.quoteDetails.FirstRegistrationDate != null &&
      this.quoteDetails.PolicyStartDate != null &&
      this.quoteDetails.vehiclemodel != null
    ) {
      if (this.productCode == "2312" || this.productCode == "2320") {
        this.modelName = this.modelName.replace("&", "%26");
        let firstReg = moment(this.quoteDetails.FirstRegistrationDate).format(
          "YYYY-MM-DD"
        );
        let polstar = moment(this.quoteDetails.PolicyStartDate).format(
          "YYYY-MM-DD"
        );
        return new Promise((resolve: any) => {
          // Amit Changes
          this.cs
            .getExshowroomToken()
            .then((resp: any) => {
              console.log(resp);
              localStorage.setItem("exShowroomToken", JSON.stringify(resp));
              this.getRedisQuoteExShowroom(polstar, firstReg).then(() => {
                resolve();
              });
            })
            .catch((err: any) => {
              swal({
                text: "There is some issue with system.",
                closeOnClickOutside: false,
              });
            });
        });
      } else if (this.productCode == "2311" || this.productCode == "2319") {
        this.modelName = this.modelName.replace("&", "%26");
        let firstReg = moment(this.quoteDetails.FirstRegistrationDate).format(
          "YYYY-MM-DD"
        );
        let polstar = moment(this.quoteDetails.PolicyStartDate).format(
          "YYYY-MM-DD"
        );
        return new Promise((resolve: any) => {
          // Amit Changes
          this.cs
            .getExshowroomToken()
            .then((resp: any) => {
              console.log(resp);
              localStorage.setItem("exShowroomToken", JSON.stringify(resp));
              this.getRedisQuoteExShowroom(polstar, firstReg).then(() => {
                resolve();
              });
            })
            .catch((err: any) => {
              swal({
                text: "There is some issue with system.",
                closeOnClickOutside: false,
              });
            });
        });
      }
    }
  }

  inspectionType(ev: any) {
    this.quoteDetails.IsExtensionCountry = false;
    this.IsSelfInspection = false;
    this.isZeroDepPlanName = false;
    this.isRSAPlan = false;
    this.quoteDetails.IsRTIApplicableflag = false;
    this.quoteDetails.IsConsumables = false;
    this.breakinDetails.inspectionMode = ev.target.value;
    if (ev.target.value == "selfinspection") {
      this.IsSelfInspection = true;
      this.breakintype.isPreApprovedBreakIN = false;
      this.calculatequote();
    } else if (ev.target.value == "ilinspection") {
      this.IsSelfInspection = false;
      this.breakintype.isPreApprovedBreakIN = false;
      this.calculatequote();
    } else {
      this.IsSelfInspection = false;
      this.breakintype.isPreApprovedBreakIN = true;
    }
  }

  // Calculate quote
  calculatequote(): Promise<void> {
    return new Promise((resolve) => {
      this.showAddon1 = false;
      let rtoCode = "";
      if (this.showsaod) {
        let compname;
        compname = this.cs.PreviousInsurerList.find((c: any) => c.companyName == this.quoteDetails.PreviousInsurerName);
        rtoCode = compname.companyCode
      }
      let dataforBack = {
        rtO_LOCATION_DESC: this.quoteDetails.rtO_LOCATION_DESC,
        vehiclemodel: this.quoteDetails.vehiclemodel,
        classCode: this.classCode,
        manufacturer: this.modelName,
        modeldesc: this.modeldesc,
        name: this.name,
        // For SAOD
        // PreviousInsurerName: this.showsaod ?  rtoCode : this.quoteDetails.PreviousInsurerName,
        PreviousInsurerName: this.quoteDetails.PreviousInsurerName,
        TPEndDate: this.quoteDetails.TPEndDate,
        TPPolicyNo: this.quoteDetails.TPPolicyNo,
        TPStartDate: this.quoteDetails.TPStartDate,
        TPInsurerName: this.quoteDetails.TPInsurerName,
        PreviousPolicyNumber: this.quoteDetails.PreviousPolicyNumber,
        productCode: this.productCode,
        policyType: this.policyType,
        seatingcapacity: this.seatingcapacity,
        showsaod: this.showsaod,
      };
      localStorage.setItem("dataforBack", JSON.stringify(dataforBack));
      // 15032022
      // this.cs.clearPropData();

      if (this.rtoDetail && this.getrtoButtonclicked == true) {
        if (
          this.quoteDetails.PreviousPolicyNumber != "" &&
          this.quoteDetails.PreviousPolicyEndDate != ""
          // && this.rtoDetail.previousInsurerCode != "NA" &&
          // !this.cs.isUndefineORNull(this.rtoDetail.previousInsurerCode)
        ) {
          this.quoteDetails.PreviousPolicyDetails = true;
        } else {
          this.quoteDetails.PreviousPolicyDetails = false;
        }
      }

      if (this.ClaimOnPreviousPolicy == true) {
        this.claimsmade = "Yes";
      } else if (this.ClaimOnPreviousPolicy == false) {
        this.claimsmade = "No";
      }

      if (this.productCode == "2320" || this.productCode == "2319") {
        this.statecode = this.stateMaster.find(
          (c) => c.statE_NAME == this.quoteDetails.GSTToState
        ).statE_ID;

        if (
          this.stateCD == 62 &&
          this.statecode == 62 &&
          this.statecode == this.stateCD
        ) {
          this.IsInterstate = "K";
        } else if (
          this.stateCD != 62 &&
          this.statecode != 62 &&
          this.statecode == this.stateCD
        ) {
          this.IsInterstate = "N";
        } else {
          this.IsInterstate = "Y";
        }
      }

      this.totalPremium = undefined;
      this.totalPremiumTP2 = undefined;
      this.totalPremiumTP3 = undefined;
      this.calculatedQuote = undefined;

      this.showQuotebutton = true;
      this.createBizToken().then(() => {
        this.showAddon = false;
        if (this.showAddon == false) {
          this.calculatedQuote = undefined;
          this.premres1 = undefined;
          this.premres2 = undefined;
          this.premres3 = undefined;
          this.totalPremiumTP = undefined;
          this.totalPremiumTP2 = undefined;
          this.totalPremiumTP3 = undefined;
          this.premreq1 = undefined;
          this.premreq2 = undefined;
          this.premreq3 = undefined;
          if (this.productCode == "2320" || this.productCode == "2319") {
            this.PaidDrivervalue = false;
            this.quoteDetails.IsLegalLiabilityToPaidDriver = false;
            this.quoteDetails.IsExtensionCountry = false;
          } else if (this.productCode == "2311" || this.productCode == "2312") {
            this.showdiscinput = false;
            this.quoteDetails.IsExtensionCountry = false;
            this.isZeroDepPlanName = false;
            this.isRSAPlan = false;
            this.quoteDetails.IsRTIApplicableflag = false;
            this.disableaddonscheck();
          }
        }

        if (this.productCode == "2312") {
          /**
           * digitalPOS Change For 5000000 limit
           * date :- 28-01-2022
           */
          if (
            this.exShowroomPrice != undefined &&
            this.exShowroomPrice != null
          ) {
            if (
              this.IsPOSTransaction_flag == true &&
              this.exShowroomPrice.maximumprice >= 5000000
            ) {
              swal({
                closeOnClickOutside: false,
                text: "Unable to process transaction as Sum Insured value is more than 50 Lacs.",
              });
              this.showAddon = false;
              this.spinloader = false;
              this.setStep(0);
            } else {
              this.calculate2Wquote();
            }
          }
        } else if (this.productCode == "2311") {
          // IsConsumables Changes
          /**
           * digitalPOS Change For 5000000 limit
           * date :- 28-01-2022
           */
          if (
            this.exShowroomPrice != undefined &&
            this.exShowroomPrice != null
          ) {
            if (
              this.IsPOSTransaction_flag == true &&
              this.exShowroomPrice.maximumprice >= 5000000
            ) {
              swal({
                closeOnClickOutside: false,
                text: "Unable to process transaction as Sum Insured value is more than 50 Lacs.",
              });
              this.showAddon = false;
              this.spinloader = false;
              this.setStep(0);
            } else {
              this.quoteDetails.IsConsumables = false;
              this.calculate4Wquote();
            }
          }
        } else if (this.productCode == "2320") {
          this.findexshowroom().then(() => {
            /**
             * digitalPOS Change For 5000000 limit
             * date :- 28-01-2022
             */
            if (
              this.exShowroomPrice != undefined &&
              this.exShowroomPrice != null
            ) {
              if (
                this.IsPOSTransaction_flag == true &&
                this.exShowroomPrice.maximumprice >= 5000000
              ) {
                swal({
                  closeOnClickOutside: false,
                  text: "Unable to process transaction as Sum Insured value is more than 50 Lacs.",
                });
                this.showAddon = false;
                this.spinloader = false;
                this.setStep(0);
              } else {
                if (
                  this.isCrawlerEnable &&
                  (this.policyType == "NEW" || this.policyType == "ROLL")
                ) {
                  this.calculateCrawTP();
                } else {
                  this.calculateTP();
                }
              }
            }
          });
        } else if (this.productCode == "2319") {
          this.findexshowroom().then(() => {
            /**
             * digitalPOS Change For 5000000 limit
             * date :- 28-01-2022
             */
            if (
              this.exShowroomPrice != undefined &&
              this.exShowroomPrice != null
            ) {
              if (
                this.IsPOSTransaction_flag == true &&
                this.exShowroomPrice.maximumprice >= 5000000
              ) {
                swal({
                  closeOnClickOutside: false,
                  text: "Unable to process transaction as Sum Insured value is more than 50 Lacs.",
                });
                this.showAddon = false;
                this.spinloader = false;
                this.setStep(0);
              } else {
                // Sejal - 25-02-2022 - Commenting as getting error in 2WTP New CPA Tenure
                if (
                  this.isCrawlerEnable &&
                  (this.policyType == "NEW" || this.policyType == "ROLL")
                ) {
                  this.calculateCrawTP();
                } else {
                  this.calculateTP();
                  // this.createCommonQuote();
                }
              }
            }
          });
        }
      });
      this.vehicleAge = moment().diff(
        this.quoteDetails.FirstRegistrationDate,
        "years"
      );
      resolve();
    });
  }

  changeState(ev: any) {
    console.log(ev);
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;
  }
  changeYear(ev: any) {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;
  }
  changePrevDate(ev: any) {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;
  }
  changePrevEndDate(ev: any) {
    console.log(
      "HIIIIIIIIII",
      this.quoteDetails.PreviousPolicyEndDate,
      ev._d,
      typeof this.quoteDetails.PreviousPolicyEndDate
    );

    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;
    if (typeof this.quoteDetails.PreviousPolicyEndDate == "object") {
      this.quoteDetails.PreviousPolicyEndDate = moment(ev._d).format(
        "YYYY-MM-DD"
      );
    }
    // this.quoteDetails.PreviousPolicyEndDate = ev.target.value;
  }

  changeEndDate(ev: any) {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.quoteDetails.PolicyEndDate = ev.target.value;
  }

  changePrevTenure(ev: any) {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;
  }
  abc(ev: any) {
    console.log('Event', ev);
  }
  changeInsurer(ev: any) {
    console.log('Insurer Name', ev);
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;
  }
  changePolicyNumber(ev: any) {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;
  }

  checkLeap(ev: any) {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;

    if (this.productCode == commonData.allriskProductCode) {
      this.checkdate = moment(this.quoteDetails.PolicyStartDate).isSameOrAfter(
        this.quoteDetails.PurchaseDate,
        "days"
      );
      if (this.checkdate == false) {
        swal({
          text: "Policy Start Date can not be less than Purchase Date",
        });
      }
    } else {
      this.checkdate = moment(this.quoteDetails.PolicyStartDate).isSameOrAfter(
        this.quoteDetails.FirstRegistrationDate,
        "days"
      );
      if (this.checkdate == false) {
        swal({
          text: "Policy Start Date can not be less than First Registration Date",
        });
      }
    }


    let currentyear = moment(this.quoteDetails.PolicyStartDate).format("YYYY");
    let leapcheck = moment(currentyear).isLeapYear();
    if (this.productCode == "2320" && this.policyType == "NEW") {
      this.policyEndDate = moment(this.quoteDetails.PolicyStartDate)
        .add(5, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    } else if (this.productCode == "2319" && this.policyType == "NEW") {
      this.policyEndDate = moment(this.quoteDetails.PolicyStartDate)
        .add(3, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    } else {
      this.policyEndDate = moment(this.quoteDetails.PolicyStartDate)
        .add(1, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    }
    this.findexshowroom();
  }

  idvChange() {
    this.value = this.exShowroomPrice.minimumprice;
    this.options = {
      floor: this.exShowroomPrice.minidv,
      ceil: this.exShowroomPrice.maxidv,
    };
  }

  idvmodal() {
    $("#sirangeinput").focus(function () {
      $("#myModal").modal({ show: true });
    });
  }

  getIDVinput(ev: any) {
    let y = JSON.parse(ev.target.value);
    let min = JSON.parse(this.lowerLimit);
    let max = JSON.parse(this.upperLimit);
    if (y < min || y > max) {
      this.isIDVChangeDev = true;
      localStorage.setItem("IDVTriggred", JSON.stringify(this.isIDVChangeDev));
      if (
        (this.productCode == "2311" || this.productCode == "2312") &&
        this.policyType != "NEW"
      ) {
        this.isIDVChangeMSG = true;
      } else {
        swal({
          text: "You have exceeded min & max limit of IDV. You need to get in touch with your RM to book policy offline",
          dangerMode: true,
          closeOnClickOutside: false,
        });
      }

      let firstReg = moment(this.quoteDetails.FirstRegistrationDate).format(
        "YYYY-MM-DD"
      );
      let polstar = moment(this.quoteDetails.PolicyStartDate).format(
        "YYYY-MM-DD"
      );

      let vehicleAge = this.cs.getVehicleAge(this.quoteDetails.FirstRegistrationDate, this.quoteDetails.PolicyStartDate);
      let depreciationRate = this.exShowroomPrice.idvdepreciationpercent;
      let requiredIDV = y;
      let newDepreciationRate = 1 - depreciationRate;
      console.log('Vehicle Age', vehicleAge, requiredIDV, newDepreciationRate);
      let newExshowRoom = Math.round(parseInt(requiredIDV) / newDepreciationRate);
      console.log('exshowroomprice', newExshowRoom);
      // this.exShowroomPrice = newExshowRoom;
      this.exshowroomprice = newExshowRoom;
      this.idv = y;
    } else {

      this.isIDVChangeDev = false;
      localStorage.setItem("IDVTriggred", JSON.stringify(this.isIDVChangeDev));

      this.isIDVChangeMSG = false;
      let firstReg = moment(this.quoteDetails.FirstRegistrationDate).format(
        "YYYY-MM-DD"
      );
      let polstar = moment(this.quoteDetails.PolicyStartDate).format(
        "YYYY-MM-DD"
      );

      let vehicleAge = this.cs.getVehicleAge(this.quoteDetails.FirstRegistrationDate, this.quoteDetails.PolicyStartDate);
      let depreciationRate = this.exShowroomPrice.idvdepreciationpercent;
      let requiredIDV = y;
      let newDepreciationRate = 1 - depreciationRate;
      console.log('Vehicle Age', vehicleAge, requiredIDV, newDepreciationRate);
      let newExshowRoom = Math.round(parseInt(requiredIDV) / newDepreciationRate);
      console.log('exshowroomprice', newExshowRoom);
      this.exshowroomprice = newExshowRoom;
      this.idv = y;
      this.quoteDetails.IsExtensionCountry = false;
      this.isZeroDepPlanName = false;
      this.isRSAPlan = false;
      this.quoteDetails.IsRTIApplicableflag = false;
      this.quoteDetails.IsConsumables = false;
    }
  }

  getIDV(ev: any) {
    let x = document.getElementById("sirange") as HTMLInputElement;
    let y = x.value;
    console.log(document.getElementById("sirangevalue"), y);
    document.getElementById("sirangevalue").innerHTML = y;
    console.log(y);
    let min = JSON.parse(this.lowerLimit);
    let max = JSON.parse(this.upperLimit);
    if (y > min || y < max) {
      this.isIDVChangeMSG = false;
    }
    let firstReg = moment(this.quoteDetails.FirstRegistrationDate).format(
      "YYYY-MM-DD"
    );
    let polstar = moment(this.quoteDetails.PolicyStartDate).format(
      "YYYY-MM-DD"
    );

    let vehicleAge = this.cs.getVehicleAge(this.quoteDetails.FirstRegistrationDate, this.quoteDetails.PolicyStartDate);
    let depreciationRate = this.exShowroomPrice.idvdepreciationpercent;
    console.log(depreciationRate);
    let requiredIDV = y;
    let newDepreciationRate = 1 - depreciationRate;

    console.log('Vehicle Age', vehicleAge, requiredIDV, newDepreciationRate);
    let newExshowRoom = Math.round(parseInt(requiredIDV) / newDepreciationRate);
    console.log('exshowroomprice', newExshowRoom);
    // this.exShowroomPrice = newExshowRoom;
    this.exshowroomprice = newExshowRoom;
    this.idv = y;
    // this.cs
    //   .get(
    //     "ManufactureNModel/CalculateExshowRoomPriceFromIDV?rtocode=" +
    //     this.rtoCode +
    //     "&requiredIDV=" +
    //     y +
    //     "&firstRegDate=" +
    //     firstReg +
    //     "&policyStartDate=" +
    //     polstar +
    //     "&productCode=" +
    //     this.productCode
    //   )
    //   .subscribe((res: any) => {
    //     this.exShowroomPrice = res;
    //     this.exshowroomprice = this.exShowroomPrice;
    //     this.idv = y;
    //   });
  }

  getEVSuminsured(ev: any) {
    // if (this.quoteDetails.model == "JAUNTY") {
    //   this.idv = 65000;
    // } else if (this.quoteDetails.model == "INSPIRER") {
    //   this.idv = 50000;
    // } else if (this.quoteDetails.model == "SPIN LED") {
    //   this.idv = 40000;
    // }
    console.log(this.quoteDetails.model, ev, this.allriskmanumod);

    if (this.cs.isUndefineORNull(this.quoteDetails.model) || ev.length < 2) {
      this.idv = "";
      this.quoteDetails.SumInsuredIDV = 0;
    } else {
      let data1 = this.allriskmanumod.filter((option) => {
        console.log(option, this.quoteDetails.model, ev);
        return (
          option.model.includes(this.quoteDetails.model) && option.model.length == this.quoteDetails.model.length
        );
      });
      console.log(data1);

      localStorage.setItem("SelectedEVModel", JSON.stringify(data1));
  
      this.idv = data1[0].price;
      this.evSumInsured = this.idv * this.rate;
      this.quoteDetails.SumInsuredIDV = this.idv;
      let batteryVolt1 = data1[0].battery_Kv.split("-");
      let batteryVolt2 = data1[0].battery_Kv.split("/");
      if (batteryVolt1.length > 1) {
        this.kilowatt = batteryVolt1[0];
      } else {
        this.kilowatt = batteryVolt2[0];
      }
      // this.kilowatt = data1[0].battery_Kv;
    }

    
  }

  getEVIDV(ev: any) {
    let x = document.getElementById("sirange") as HTMLInputElement;
    let y = x.value;
    console.log(document.getElementById("sirangevalue"), y);

    // this.lowerLimit = 100;
    // this.upperLimit = 150000;
    let min = this.lowerLimit;
    let max = this.upperLimit;

    if (y >= min && y <= max) {
      this.isIDVChangeMSG = false;
      this.idv = y;
    }
    else {
      this.isIDVChangeMSG = true;
    }

    // if ( this.isIDVChangeMSG == false && this.idv != null ) {
    //   if(this.quoteDetails.model == "JAUNTY") {
    //     this.idv = 65000;
    //   } else if (this.quoteDetails.model == "INSPIRER") {
    //     this.idv = 50000;
    //   } else if (this.quoteDetails.model == "SPIN LED") {
    //     this.idv = 40000;
    //   }
    // } else {
    //   this.idv = y;
    // }

    this.evSumInsured = this.idv * this.rate;
    this.quoteDetails.SumInsuredIDV = this.idv;
  }

  getEVIDVinput(ev: any) {
    let x = document.getElementById("sirange") as HTMLInputElement;
    let y = parseInt(x.value);
    let modelValue = parseInt(ev.target.value);
    // this.lowerLimit = 100;
    // this.upperLimit = 150000;
    let min = this.lowerLimit;
    let max = this.upperLimit;
    console.info("YYY", y);
    console.info("Model Value", modelValue);

    if (modelValue != y) {
      y = modelValue;
      if (y >= min && y <= max) {
        this.isIDVChangeMSG = false;
      }
      else {
        this.isIDVChangeMSG = true;
      }
    } else {
      if (y >= min && y <= max) {
        this.isIDVChangeMSG = false;
      }
      else {
        this.isIDVChangeMSG = true;
      }
    }

    // if(this.quoteDetails.model == "JAUNTY") {
    //   this.idv = 65000;
    // } else if (this.quoteDetails.model == "INSPIRER") {
    //   this.idv = 50000;
    // } else if (this.quoteDetails.model == "SPIN LED") {
    //   this.idv = 40000;
    // }
    this.idv = y;

    this.evSumInsured = this.idv * this.rate;
    this.quoteDetails.SumInsuredIDV = this.idv;
  }

  // Quote inputs
  quoteInputs() {
    if (this.policyType == "NEW") {
      this.TransFor = "N";
    } else {
      this.TransFor = "R";
    }
    if (this.productCode == "2320" && this.policyType == "NEW") {
      this.PolicyTenure = 5;
    } else if (this.productCode == "2319" && this.policyType == "NEW") {
      this.PolicyTenure = 3;
    } else {
      this.PolicyTenure = 1;
    }
    if (this.productCode == "2320") {
      this.MotorSubType = 1;
    } else if (this.productCode == "2319") {
      this.MotorSubType = 2;
    }

    if (this.productCode == "2320" || this.productCode == "2319") {
      this.statecode = this.stateMaster.find(
        (c) => c.statE_NAME == this.quoteDetails.GSTToState
      ).statE_ID;
      if (
        this.stateCD == 62 &&
        this.statecode == 62 &&
        this.statecode == this.stateCD
      ) {
        this.IsInterstate = "K";
      } else if (
        this.stateCD != 62 &&
        this.statecode != 62 &&
        this.statecode == this.stateCD
      ) {
        this.IsInterstate = "N";
      } else {
        this.IsInterstate = "Y";
      }
    }

    let body = {
      MotorSubType: this.MotorSubType,
      VehicleCC: parseInt(this.cubiccapacity),
      PolicyTenure: this.PolicyTenure, //No Change
      CPATenure: 1,
      TransFor: this.TransFor,
      IsInterstate: this.IsInterstate,
      PolicyStartDate: moment(this.quoteDetails.PolicyStartDate).format(
        "YYYY-MM-DD"
      ),
      PolicyEndDate: this.policyEndDate,
      DealID: this.dealid,
      CustomerType: this.registrationType,
      RegistrationNumber: this.quoteDetails.RegistrationNumber,
      ExShowRoomPrice: parseInt(this.exshowroomprice),
      manufacturer: this.modelName,
      VehicleModelCode: this.modelcode,
      VehicleMakeCode: this.makecode,
      vehiclemodel: this.quoteDetails.vehiclemodel,
      GSTToState: this.quoteDetails.GSTToState,
      RTOLocationCode: parseInt(this.rtoCode),
      rtoLocation: this.quoteDetails.rtO_LOCATION_DESC,
      ManufacturingYear: parseInt(this.quoteDetails.ManufacturingYear),
      DeliveryOrRegistrationDate: moment(
        this.quoteDetails.FirstRegistrationDate
      ).format("YYYY-MM-DD"),
      FirstRegistrationDate: moment(
        this.quoteDetails.FirstRegistrationDate
      ).format("YYYY-MM-DD"),
    };
  }

  // Calculate 2w quote
  calculate2Wquote() {
    let button = document.getElementById("get_quote") as HTMLInputElement;
    console.log("Button", button);
    this.id = Guid.raw();
    this.vehicleAge = moment().diff(
      this.quoteDetails.FirstRegistrationDate,
      "years"
    );
    let body: any;
    let PreviousPolicyDetails: any;
    // this.isDeviationTriggered = false;
    console.log(this.isRecalculate, this.isRecalculate1, this.quoteDetails);
    if (this.isRecalculate1) {
      this.panelOpenStateProposal = false;
      this.setStep(2);
    } else {
      this.panelOpenStateProposal = false;
      this.setStep(1);
    }
    let quoteRequest = JSON.parse(localStorage.getItem("calQuoteReq"));

    if (!this.cs.isUndefineORNull(quoteRequest)) {
      if (this.quoteDetails.PolicyStartDate != quoteRequest.PolicyStartDate) {
        quoteRequest.PolicyStartDate = this.quoteDetails.PolicyStartDate;
        if (!this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyEndDate)) {
          quoteRequest.PreviousPolicyDetails.PreviousPolicyEndDate =
            this.quoteDetails.PreviousPolicyEndDate;
          //Changes for future date
          quoteRequest.PreviousPolicyDetails.PreviousPolicyStartDate = moment(
            this.quoteDetails.PreviousPolicyEndDate
          )
            .subtract(1, "years")
            .add(1, "days")
            .format("YYYY-MM-DD");
        }
      }

      console.log('For SAOD', this.showsaod, this.quoteDetails);
      if (this.showsaod) {
        quoteRequest.TPPolicyNo = this.quoteDetails.TPPolicyNo;
        quoteRequest.TPInsurerName = this.quoteDetails.TPInsurerName;
        quoteRequest.PreviousPolicyDetails.PreviousPolicyType = 'Bundled Package Policy';
        quoteRequest.TPTenure = '0';
        quoteRequest.Tenure = '1';
      } else {
        // quoteRequest.PreviousPolicyDetails.PreviousPolicyType = 'Comprehensive Package';
        quoteRequest.TPTenure = "5";  // Sejal - 27-4-2022 - CPA Tenure Changes
        quoteRequest.Tenure = "1";
      }

      if (!this.cs.isUndefineORNull(quoteRequest.PreviousPolicyDetails)) {
        console.log(this.quoteDetails.PreviousPolicyEndDate, quoteRequest.PreviousPolicyDetails.PreviousPolicyEndDate);
        if (
          this.quoteDetails.PreviousPolicyEndDate !=
          quoteRequest.PreviousPolicyDetails.PreviousPolicyEndDate
        ) {
          quoteRequest.PreviousPolicyDetails.PreviousPolicyEndDate =
            this.quoteDetails.PreviousPolicyEndDate;
          quoteRequest.PreviousPolicyDetails.PreviousPolicyStartDate = moment(
            this.quoteDetails.PreviousPolicyEndDate
          )
            .subtract(1, "years")
            .add(1, "days")
            .format("YYYY-MM-DD");
        }
      }
      if (
        this.quoteDetails.FirstRegistrationDate !=
        quoteRequest.FirstRegistrationDate
      ) {
        quoteRequest.FirstRegistrationDate =
          this.quoteDetails.FirstRegistrationDate;
      }
      // Sejal - 27-4-2022 - CPA Tenure Changes
      if (this.quoteDetails.PACoverTenure != quoteRequest.PACoverTenure) {
        quoteRequest.PACoverTenure = this.quoteDetails.PACoverTenure;
      }
    }
    if (
      !this.cs.isUndefineORNull(quoteRequest) &&
      !this.isRecalculate1 &&
      this.quoteDetails.OtherDiscount == 0 &&
      !quoteRequest.isNoPrevInsurance
    ) {
      let noOfClaims = this.quoteDetails.NoOfClaimsOnPreviousPolicy;
      this.quoteRequ = this.quoteDetails = quoteRequest;
      this.showPYP = quoteRequest.isNoPrevInsurance;
      if (
        typeof this.quoteDetails.PreviousPolicyDetails != "boolean" &&
        !this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyDetails)
      ) {
        PreviousPolicyDetails = this.quoteDetails.PreviousPolicyDetails
          ? this.quoteDetails.PreviousPolicyDetails
          : "";
        if (this.claimNCB) {
          this.ClaimOnPreviousPolicy = false;
          PreviousPolicyDetails = this.quoteDetails.PreviousPolicyDetails;
          PreviousPolicyDetails.TotalNoOfODClaims = "0";
          this.quoteDetails.TotalNoOfODClaims =
            PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy = "0";
          PreviousPolicyDetails.ClaimOnPreviousPolicy = false;
          this.quoteDetails.PreviousPolicyDetails = PreviousPolicyDetails;
          this.quoteRequ = this.quoteDetails = this.quoteDetails;
          this.PreviousPolicyDetails = this.quoteDetails.PreviousPolicyDetails;
        } else {
          this.ClaimOnPreviousPolicy = true;
          PreviousPolicyDetails = this.quoteDetails.PreviousPolicyDetails;
          PreviousPolicyDetails.TotalNoOfODClaims = noOfClaims
            ? noOfClaims
            : "1";
          this.quoteDetails.TotalNoOfODClaims =
            PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy = noOfClaims
              ? noOfClaims
              : "1";
          PreviousPolicyDetails.ClaimOnPreviousPolicy = true;
          this.quoteDetails.PreviousPolicyDetails = PreviousPolicyDetails;
          this.quoteRequ = this.quoteDetails = this.quoteDetails;
          this.PreviousPolicyDetails = this.quoteDetails.PreviousPolicyDetails;
        }
      }
    } else {
      let discount = JSON.parse(localStorage.getItem("ODAmount"));
      if (this.cs.isUndefineORNull(discount)) {
        // localStorage.removeItem("ODAmount");
        this.quoteDetails.OtherDiscount = 0;
        localStorage.setItem('ODAmount', JSON.stringify(this.quoteDetails.OtherDiscount));
      }
      this.quoteRequ = this.quoteDetails = this.quoteDetails;
      if (
        typeof this.quoteDetails.PreviousPolicyDetails != "boolean" &&
        !this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyDetails)
      ) {
        PreviousPolicyDetails = this.PreviousPolicyDetails =
          this.quoteDetails.PreviousPolicyDetails;
      }
    }
    let dataForBind = JSON.parse(localStorage.getItem("dataforBack"));
    this.quoteDetails.rtO_LOCATION_DESC = dataForBind.rtO_LOCATION_DESC;
    this.quoteDetails.vehiclemodel = dataForBind.vehiclemodel;
    this.quoteDetails.PreviousPolicyEndDate =
      typeof this.quoteDetails.PreviousPolicyDetails != "boolean" &&
        !this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyDetails)
        ? PreviousPolicyDetails.PreviousPolicyEndDate
        : this.quoteDetails.PreviousPolicyEndDate;
    this.quoteDetails.claims = this.ClaimOnPreviousPolicy
      ? "claimyes"
      : "claimno";
    console.log('Data For Bind', dataForBind)
    this.quoteDetails.PreviousInsurerName = !this.cs.isUndefineORNull(
      dataForBind.PreviousInsurerName
    )
      ? dataForBind.PreviousInsurerName
      : "OTHER";
    if (dataForBind.showsaod) {

      this.quoteDetails.TPStartDate = dataForBind.TPStartDate;
      this.quoteDetails.TPEndDate = dataForBind.TPEndDate;
      this.quoteDetails.TPPolicyNo = dataForBind.TPPolicyNo;
      this.quoteDetails.PreviousPolicyNumber = dataForBind.PreviousPolicyNumber;
      this.quoteDetails.PreviousInsurerName = dataForBind.PreviousInsurerName;
    }
    if (this.policyType != "NEW" && this.showsaod == true) {
      this.quoteDetails.TPTenure = "0";
      this.quoteDetails.Tenure = "1";
      this.quoteDetails.PreviousPolicyType = "Bundled Package Policy";
    } else if (this.showsaod == true && this.quoteReqrem.showsaod == true) {
      this.quoteDetails.TPTenure = "0";
      this.quoteDetails.Tenure = "1";
      this.quoteDetails.PreviousPolicyType = "Bundled Package Policy";
    }
    // 2w new Request Body
    body = {
      CorrelationId: this.id,
      RegistrationNumber: this.quoteDetails.RegistrationNumber,
      ExShowRoomPrice: parseInt(this.exshowroomprice),
      PACoverTenure: this.quoteDetails.PACoverTenure,
      IsLegalLiabilityToPaidEmployee:
        this.quoteDetails.IsLegalLiabilityToPaidEmployee,
      NoOfEmployee: this.quoteDetails.NoOfEmployee,
      IsPACoverUnnamedPassenger: this.quoteDetails.IsPACoverUnnamedPassenger,
      IsProposal: false,
      Tenure: this.quoteDetails.Tenure,
      TPTenure: this.quoteDetails.TPTenure,
      PolicyStartDate: moment(this.quoteDetails.PolicyStartDate).format(
        "YYYY-MM-DD"
      ), //YYY-MM-DD
      PolicyEndDate: this.policyEndDate, //YYY-MM-DD
      DealId: this.dealid, // "DL-3005/1485414"
      VehicleModelCode: this.modelcode,
      VehicleMakeCode: this.makecode,
      RTOLocationCode: parseInt(this.rtoCode),
      ManufacturingYear: parseInt(this.quoteDetails.ManufacturingYear),
      DeliveryOrRegistrationDate: moment(
        this.quoteDetails.FirstRegistrationDate
      ).format("YYYY-MM-DD"), //YYY-MM-DD
      FirstRegistrationDate: moment(
        this.quoteDetails.FirstRegistrationDate
      ).format("YYYY-MM-DD"), //YYY-MM-DD
      BusinessType: "New Business",
      CustomerType: this.registrationType,
      IsValidDrivingLicense: this.isValidDrivingLicense,
      GSTToState: this.quoteDetails.GSTToState,
      IsConsumables: this.quoteDetails.IsConsumables,
      IsExtensionCountry: this.quoteDetails.IsExtensionCountry,
      ExtensionCountryName: this.quoteDetails.ExtensionCountryName,
      IsRTIApplicableflag: this.quoteDetails.IsRTIApplicableflag,
      TPPDLimit: this.TPPDLimit,
      IsVoluntaryDeductible: this.quoteDetails.IsVoluntaryDeductible,
      isGarageCash: this.quoteDetails.isGarageCash,
      isNCBProtect: this.quoteDetails.isNCBProtect,
      ChannelSource: "IAGENT",
    };

    if (this.showsaod == true) {
      body.TPStartDate = moment(this.quoteDetails.TPStartDate).format(
        "YYYY-MM-DD"
      );
      body.TPEndDate = moment(this.quoteDetails.TPEndDate).format("YYYY-MM-DD");
      body.TPPolicyNo = this.quoteDetails.TPPolicyNo;
      body.TPInsurerName = this.quoteDetails.TPInsurerName;
    }

    if (this.isZeroDepPlanName == true) {
      let zerodep = this.quoteDetails.ZeroDepPlanName;
      body.ZeroDepPlanName = zerodep;
    }

    if (this.quoteDetails.isGarageCash == true) {
      body.garageCashPlanName = this.quoteDetails.garageCashPlanName;
    }

    if (this.IsAntiTheftDisc == true) {
      body.IsAntiTheftDisc = this.IsAntiTheftDisc;
    }

    if (this.IsAutomobileAssocnFlag == true) {
      body.IsAutomobileAssocnFlag = this.IsAutomobileAssocnFlag;
    }

    if (this.quoteDetails.isNCBProtect == true) {
      body.ncbProtectPlanName = this.quoteDetails.ncbProtectPlanName;
    }

    if (
      this.isRSAPlan == true &&
      !this.cs.isUndefineORNull(this.quoteDetails.RSAPlanName)
    ) {
      body.RSAPlanName = this.quoteDetails.RSAPlanName;
    }
    // Preetam
    if (this.quoteDetails.OtherDiscount != 0) {
      let OD = this.otherDiscount
        ? this.otherDiscount
        : this.quoteDetails.OtherDiscount;
      console.log(OD, typeof OD);
      let OD1 = Number(OD) / 100;
      // console.log(OD1, typeof(OD1), OD1.toString());
      body.OtherDiscount = OD1.toString();
      // body.OtherDiscount = OD;
      if (Number(this.quoteDetails.OtherDiscount) > 0) {
        this.isOtherDiscDev = true;
        localStorage.setItem("ODTriggred", JSON.stringify(this.isOtherDiscDev));
      } else {
        this.isOtherDiscDev = false;
        localStorage.setItem("ODTriggred", JSON.stringify(this.isOtherDiscDev));
      }
    } else {
      body.OtherDiscount = "0";
      this.isOtherDiscDev = false;
      localStorage.setItem("ODTriggred", JSON.stringify(this.isOtherDiscDev));
    }

    if (this.quoteDetails.IsVoluntaryDeductible == true) {
      body.VoluntaryDeductiblePlanName =
        this.quoteDetails.VoluntaryDeductiblePlanName;
    }

    if (this.quoteDetails.IsPACoverUnnamedPassenger == true) {
      let PACovervalue =
        JSON.parse(this.quoteDetails.SIPACoverUnnamedPassenger) *
        JSON.parse(this.seatingcapacity);
      body.SIPACoverUnnamedPassenger = PACovervalue;
    }

    if (this.policyType == "ROLL") {
      body.BusinessType = "Roll Over";
      body.NomineeDetails = null;
      body.financierDetails = null;
      body.CustomerDetails = null;
      body.SPDetails = null;

      if (this.quoteDetails.TotalNoOfODClaims == "0") {
        this.ClaimOnPreviousPolicy = false;
      }

      if (
        this.quoteDetails.PreviousPolicyDetails == true ||
        this.showsaod == true
      ) {
        if (
          this.systemQC == true &&
          this.ncbValue == 0 &&
          this.ClaimOnPreviousPolicy == false
        ) {
          let PPED = this.quoteDetails.PreviousPolicyEndDate
            ? this.quoteDetails.PreviousPolicyEndDate
            : this.PreviousPolicyDetails.PreviousPolicyEndDate;
          if (PPED == "Invalid date") {
            body.IsQCByPass = false;
            body.isNoPrevInsurance = true;
            this.showPYP = true;
          } else {
            body.IsQCByPass = true;
            body.isNoPrevInsurance = false;
            this.showPYP = false;
            let prevPolStartDate = moment(PPED)
              .subtract(1, "years")
              .add(1, "days")
              .format("YYYY-MM-DD");
            this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
            let prevStartDate = moment(
              this.quoteDetails.PreviousPolicyStartDate
            ).format("YYYY-MM-DD");
            let prevEndDate = moment(PPED).format("YYYY-MM-DD");
            this.quoteDetails.PreviousPolicyEndDate = prevEndDate;
            this.cs
              .get(
                "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
                prevStartDate +
                "&prevEndDate=" +
                prevEndDate
              )
              .subscribe((res: any) => {
                this.previousPolicyTen = res;
                this.quoteDetails.PreviousPolicyTenure = this.previousPolicyTen;
              });

            this.PreviousPolicyDetails.PreviousPolicyType = this.quoteDetails
              .PreviousPolicyType
              ? this.quoteDetails.PreviousPolicyType
              : "Comprehensive Package";
            this.PreviousPolicyDetails.PreviousPolicyStartDate = moment(
              this.quoteDetails.PreviousPolicyStartDate
            ).format("YYYY-MM-DD");
            this.PreviousPolicyDetails.PreviousPolicyEndDate = moment(
              this.quoteDetails.PreviousPolicyEndDate
            ).format("YYYY-MM-DD");
            this.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
            this.PreviousPolicyDetails.PreviousPolicyNumber =
              !this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyNumber) ? this.quoteDetails.PreviousPolicyNumber : null;
            this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
              this.ClaimOnPreviousPolicy;
            this.PreviousPolicyDetails.TotalNoOfODClaims =
              this.quoteDetails.TotalNoOfODClaims;
            this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
              this.quoteDetails.TotalNoOfODClaims;
            this.PreviousPolicyDetails.PreviousInsurerName = this.companyCode;
            this.PreviousPolicyDetails.PreviousVehicleSaleDate = this
              .PreviousPolicyDetails.PreviousVehicleSaleDate
              ? this.PreviousPolicyDetails.PreviousVehicleSaleDate
              : "0001-01-01T00:00:00";
            this.quoteDetails.PreviousVehicleSaleDate =
              this.PreviousPolicyDetails.PreviousVehicleSaleDate;
            this.PreviousPolicyDetails.PreviousPolicyTenure =
              this.previousPolicyTen;

            body.PreviousPolicyDetails = this.PreviousPolicyDetails
              ? this.PreviousPolicyDetails
              : PreviousPolicyDetails;
          }

          this.getInsurerName();
          let compName = this.insurerlist.find(
            (c) => c.companyName == this.quoteDetails.PreviousInsurerName
          );

          if (this.cs.isUndefineORNull(compName)) {
            this.companyCode = "OTHER";
          } else {
            this.companyCode = compName.companyCode;
          }
        } else {
          console.log(this.PreviousPolicyDetails);
          let PPED = this.quoteDetails.PreviousPolicyEndDate
            ? this.quoteDetails.PreviousPolicyEndDate
            : this.PreviousPolicyDetails.PreviousPolicyEndDate;
          if (PPED == "Invalid date") {
            body.IsQCByPass = false;
            body.isNoPrevInsurance = true;
            this.showPYP = true;
          } else {
            body.isNoPrevInsurance = false;
            this.showPYP = false;
            body.IsQCByPass = true;
            let prevPolStartDate = moment(PPED)
              .subtract(1, "years")
              .add(1, "days")
              .format("YYYY-MM-DD");
            this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
            let prevStartDate = moment(
              this.quoteDetails.PreviousPolicyStartDate
            ).format("YYYY-MM-DD");
            let prevEndDate = moment(PPED).format("YYYY-MM-DD");
            this.quoteDetails.PreviousPolicyEndDate = prevEndDate;
            this.cs
              .get(
                "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
                prevStartDate +
                "&prevEndDate=" +
                prevEndDate
              )
              .subscribe((res: any) => {
                this.previousPolicyTen = res;
                this.quoteDetails.PreviousPolicyTenure = this.previousPolicyTen;
              });

            this.PreviousPolicyDetails.PreviousPolicyType = this.quoteDetails
              .PreviousPolicyType
              ? this.quoteDetails.PreviousPolicyType
              : "Comprehensive Package";
            this.PreviousPolicyDetails.PreviousPolicyStartDate = moment(
              this.quoteDetails.PreviousPolicyStartDate
            ).format("YYYY-MM-DD");
            this.PreviousPolicyDetails.PreviousPolicyEndDate = moment(
              this.quoteDetails.PreviousPolicyEndDate
            ).format("YYYY-MM-DD");
            this.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
            this.PreviousPolicyDetails.PreviousPolicyNumber =
              !this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyNumber) ? this.quoteDetails.PreviousPolicyNumber : null;
            this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
              this.ClaimOnPreviousPolicy;
            this.PreviousPolicyDetails.TotalNoOfODClaims =
              this.quoteDetails.TotalNoOfODClaims;
            this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
              this.quoteDetails.TotalNoOfODClaims;
            this.PreviousPolicyDetails.PreviousInsurerName = this.companyCode;
            this.PreviousPolicyDetails.PreviousVehicleSaleDate = this
              .PreviousPolicyDetails.PreviousVehicleSaleDate
              ? this.PreviousPolicyDetails.PreviousVehicleSaleDate
              : "0001-01-01T00:00:00";
            this.quoteDetails.PreviousVehicleSaleDate =
              this.PreviousPolicyDetails.PreviousVehicleSaleDate;
            this.PreviousPolicyDetails.PreviousPolicyTenure =
              this.previousPolicyTen;

            body.PreviousPolicyDetails = this.PreviousPolicyDetails
              ? this.PreviousPolicyDetails
              : PreviousPolicyDetails;
          }
          if (this.showsaod == true) {
            this.getInsurerName();
            let compName = this.insurerlist.find(
              (c) => c.companyName == this.quoteDetails.PreviousInsurerName
            );
            if (this.cs.isUndefineORNull(compName)) {
              this.companyCode = "OTHER";
            } else {
              this.companyCode = compName.companyCode;
            }
          }
        }
      } else {
        console.log(this.PreviousPolicyDetails, PreviousPolicyDetails);
        if (this.quoteDetails.PreviousPolicyEndDate == "Invalid date" || this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyEndDate) || this.quoteDetails.PreviousPolicyDetails == false) {
          // if(this.quoteDetails.PreviousPolicyEndDate == "Invalid date" || this.cs.isUndefineORNull(this.PreviousPolicyDetails.PreviousPolicyNumber)){
          body.isNoPrevInsurance = true;
          this.showPYP = true;
          body.IsQCByPass = false;
          // }else{
          // body.isNoPrevInsurance = false;
          // this.showPYP = false;
          // body.IsQCByPass = true;
          // body.PreviousPolicyDetails = this.PreviousPolicyDetails;
          // body.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
          // }
        } else {
          body.isNoPrevInsurance = false;
          body.IsQCByPass = true;
          body.PreviousPolicyDetails = this.PreviousPolicyDetails;
          body.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
          this.showPYP = false;
        }
        // body.IsQCByPass = false;
        // body.isNoPrevInsurance = true;
      }
      body.IsSelfInspection = this.IsSelfInspection;
    } else {
      body.BusinessType = "New Business";
      if (this.showPrev == true) {
        if (this.ncbValue == 0) {
          body.IsTransferOfNCB = false;
          if (this.showsaod == true) {
            this.getInsurerName();
            //Preetam Changes
            let compName = this.insurerlist.find(
              (c) => c.companyName == this.quoteDetails.PreviousInsurerName
            );
            // this.companyCode = this.insurerlist.find(
            //   (c) => c.companyName == this.quoteDetails.PreviousInsurerName
            // ).companyCode;
            if (this.cs.isUndefineORNull(compName)) {
              this.companyCode = "OTHER";
            } else {
              this.companyCode = compName.companyCode;
            }
          }
          let prevPolStartDate = moment(this.quoteDetails.PreviousPolicyEndDate)
            .subtract(1, "years")
            .add(1, "days")
            .format("YYYY-MM-DD");
          this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
          let prevStartDate = moment(
            this.quoteDetails.PreviousPolicyStartDate
          ).format("YYYY-MM-DD");
          let prevEndDate = moment(
            this.quoteDetails.PreviousPolicyEndDate
          ).format("YYYY-MM-DD");
          this.cs
            .get(
              "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
              prevStartDate +
              "&prevEndDate=" +
              prevEndDate
            )
            .subscribe((res: any) => {
              this.previousPolicyTen = res;
              this.quoteDetails.PreviousPolicyTenure = this.previousPolicyTen;
            });

          this.PreviousPolicyDetails.PreviousPolicyType = this.quoteDetails
            .PreviousPolicyType
            ? this.quoteDetails.PreviousPolicyType
            : "Comprehensive Package";
          this.PreviousPolicyDetails.PreviousPolicyStartDate = prevStartDate;
          this.PreviousPolicyDetails.PreviousPolicyEndDate = prevEndDate;
          this.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
          if (this.showsaod == true) {
            this.PreviousPolicyDetails.PreviousPolicyNumber =
              this.quoteDetails.PreviousPolicyNumber;
          }
          this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
            this.ClaimOnPreviousPolicy;
          this.PreviousPolicyDetails.TotalNoOfODClaims =
            this.quoteDetails.TotalNoOfODClaims;
          this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
            this.quoteDetails.TotalNoOfODClaims;
          if (this.showsaod == true) {
            this.PreviousPolicyDetails.PreviousInsurerName = this.companyCode;
          }
          this.PreviousPolicyDetails.PreviousVehicleSaleDate = this
            .PreviousPolicyDetails.PreviousVehicleSaleDate
            ? this.PreviousPolicyDetails.PreviousVehicleSaleDate
            : moment(this.quoteDetails.PreviousVehicleSaleDate).format(
              "YYYY-MM-DD"
            );
          this.quoteDetails.PreviousVehicleSaleDate =
            this.PreviousPolicyDetails.PreviousVehicleSaleDate;
          this.PreviousPolicyDetails.PreviousPolicyTenure =
            this.previousPolicyTen;
          body.PreviousPolicyDetails = this.PreviousPolicyDetails
            ? this.PreviousPolicyDetails
            : PreviousPolicyDetails;
        } else {
          if (this.showsaod == true) {
            this.getInsurerName();
            let compName = this.insurerlist.find(
              (c) => c.companyName == this.quoteDetails.PreviousInsurerName
            );
            // this.companyCode = this.insurerlist.find(
            //   (c) => c.companyName == this.quoteDetails.PreviousInsurerName
            // ).companyCode;
            if (this.cs.isUndefineORNull(compName)) {
              this.companyCode = "OTHER";
            } else {
              this.companyCode = compName.companyCode;
            }
          }
          if (
            this.ncbValue != undefined ||
            this.ncbValue != null ||
            this.claimAdd != "claimyes"
          ) {
            body.TransferOfNCBPercent = parseInt(this.ncbValue);
            body.IsTransferOfNCB = this.isTransfer;
          }

          let prevPolStartDate = moment(this.quoteDetails.PreviousPolicyEndDate)
            .subtract(1, "years")
            .add(1, "days")
            .format("YYYY-MM-DD");
          this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
          let prevStartDate = moment(
            this.quoteDetails.PreviousPolicyStartDate
          ).format("YYYY-MM-DD");
          let prevEndDate = moment(
            this.quoteDetails.PreviousPolicyEndDate
          ).format("YYYY-MM-DD");
          this.cs
            .get(
              "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
              prevStartDate +
              "&prevEndDate=" +
              prevEndDate
            )
            .subscribe((res: any) => {
              this.previousPolicyTen = res;
              this.quoteDetails.PreviousPolicyTenure = this.previousPolicyTen;
            });

          this.PreviousPolicyDetails.PreviousPolicyType = this.quoteDetails
            .PreviousPolicyType
            ? this.quoteDetails.PreviousPolicyType
            : "Comprehensive Package";
          this.PreviousPolicyDetails.PreviousPolicyStartDate = prevStartDate;
          this.PreviousPolicyDetails.PreviousPolicyEndDate = prevEndDate;
          this.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
          if (this.showsaod == true) {
            this.PreviousPolicyDetails.PreviousPolicyNumber =
              this.quoteDetails.PreviousPolicyNumber;
          }
          this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
            this.ClaimOnPreviousPolicy;
          this.PreviousPolicyDetails.TotalNoOfODClaims =
            this.quoteDetails.TotalNoOfODClaims;
          this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
            this.quoteDetails.TotalNoOfODClaims;
          if (this.showsaod == true) {
            this.PreviousPolicyDetails.PreviousInsurerName = this.companyCode;
          }
          this.PreviousPolicyDetails.PreviousVehicleSaleDate = this
            .PreviousPolicyDetails.PreviousVehicleSaleDate
            ? this.PreviousPolicyDetails.PreviousVehicleSaleDate
            : moment(this.quoteDetails.PreviousVehicleSaleDate).format(
              "YYYY-MM-DD"
            );
          this.quoteDetails.PreviousVehicleSaleDate =
            this.PreviousPolicyDetails.PreviousVehicleSaleDate;
          this.PreviousPolicyDetails.PreviousPolicyTenure =
            this.previousPolicyTen;
          body.PreviousPolicyDetails = this.PreviousPolicyDetails
            ? this.PreviousPolicyDetails
            : PreviousPolicyDetails;
        }
      } else {
        body.IsTransferOfNCB = false;
      }
    }
    this.quoteRequ = body;
    let str = JSON.stringify(body);
    let data = JSON.parse(localStorage.getItem('PreviousData'));
    if (this.cs.isUndefineORNull(data)) {
      localStorage.setItem("PreviousData", str);
    }
    localStorage.setItem("calQuoteReq", str);

    if (this.isRecalculate == true) {
      this.cs.loaderStatus = true;
      this.panelOpenStateAddon = true;
    } else {
      this.panelOpenStateAddon = false;
    }

    this.showAddon = true;
    this.quickquote = false;
    this.panelOpenState = false;
    this.panelOpenStatePremium = true;
    this.panelOpenStateProposal = false;
    this.spinloader1 = true;
    this.quoteservice = this.cs
      .postBiz("/proposal/twowheelercalculatepremium", str)
      .subscribe(
        (res: any) => {
          this.spinloader1 = false;
          localStorage.setItem("calQuoteRes", JSON.stringify(res));
          localStorage.removeItem('regNumber');
          this.totalPremium = this.totalPremiumTP = res.finalPremium;
          if (this.isRecalculate1) {
            this.reCalDealerDiscount = res;
          }
          if (res.status == "Failed" || res.status == null) {
            this.freezeManuModel = false;
            let errormsg = res.message;
            this.failedCalculatedQuote = res;
            // Common MSG
            // this.api.handleerrors(errormsg);
            let errMsg = this.msgService.errorMsg(errormsg)
            swal({ closeOnClickOutside: false, text: errMsg });
            let errorbody = {
              RequestJson: JSON.stringify(body),
              ResponseJson: JSON.stringify(res),
              ServiceURL:
                commonData.bizURL + "/proposal/twowheelercalculatepremium",
              CorrelationID: this.id,
            };
            this.api.adderrorlogs(errorbody);
            this.failedquote = true;
            // this.totalPremiumTP3 = '0'
            // this.calculatedQuote = res;
            // this.PremiumCalculation();
            this.cs.loaderStatus = false;
            this.spinloader = false;
            if (this.isRecalculate1) {
              this.panelOpenStateProposal = false;
              this.setStep(2);
            } else {
              this.panelOpenStateProposal = false;
              this.setStep(1);
            }
            // if (this.panelOpenStateAddon == false) {
            //   this.defaultPlan();
            //   if (this.totalPremiumTP3 == undefined) {
            //     let prem3 = document.getElementById(
            //       "prem3"
            //     ) as HTMLInputElement;
            //     prem3.style.display = "none";
            //   }
            // }
          } else {
            this.failedquote = false;
            this.bFlag = res.breakingFlag;
            if (this.showsaod == true && this.showSCPAinNysa) {
              let newBundleID: any;
              newBundleID = Guid.create();
              this.saodBundleID = newBundleID.value;
              this.quoteForSCPA();
            }
            if (this.isRecalculate1) {
              // this.quoteResp = res;
              this.quoteResp = this.calculatedQuote = res;
              let antiTheftDiscount = this.calculatedQuote.riskDetails
                .antiTheftDiscount
                ? this.calculatedQuote.riskDetails.antiTheftDiscount
                : 0;
              let automobileAssociationDiscount = this.calculatedQuote
                .riskDetails.automobileAssociationDiscount
                ? this.calculatedQuote.riskDetails.automobileAssociationDiscount
                : 0;
              let handicappedDiscount = this.calculatedQuote.riskDetails
                .handicappedDiscount
                ? this.calculatedQuote.riskDetails.handicappedDiscount
                : 0;
              let bonusDiscount = this.calculatedQuote.riskDetails.bonusDiscount
                ? this.calculatedQuote.riskDetails.bonusDiscount
                : 0;
              let voluntaryDiscount = this.calculatedQuote.riskDetails
                .voluntaryDiscount
                ? this.calculatedQuote.riskDetails.voluntaryDiscount
                : 0;
              let tppD_Discount = this.calculatedQuote.riskDetails.tppD_Discount
                ? this.calculatedQuote.riskDetails.tppD_Discount
                : 0;
              this.totalDiscount =
                antiTheftDiscount +
                automobileAssociationDiscount +
                handicappedDiscount +
                bonusDiscount +
                voluntaryDiscount +
                tppD_Discount;
              let pkgPrem = this.calculatedQuote.packagePremium
                ? this.calculatedQuote.packagePremium
                : this.calculatedQuote.totalLiabilityPremium;
              this.premiumWithoutDiscount = pkgPrem + this.totalDiscount;
            }
            if (res.isQuoteDeviation == true) {
              this.isOtherDiscDev = false;
              if (this.policyType == "NEW") {
                swal({
                  text:
                    "This case would go into deviation.Reason: " +
                    res.deviationMessage +
                    " Would you like to continue?",
                  dangerMode: true,
                  closeOnClickOutside: false,
                  buttons: ["Yes", "No"],
                }).then((willDelete) => {
                  if (willDelete) {
                    console.log("Stay");
                  } else {
                    let type;
                    if (this.policyType == "NEW") {
                      type = "N";
                    } else {
                      type = "R";
                    }
                    this.cs.geToDashboardapproval(this.productCode, type);
                  }
                });
                this.cs.loaderStatus = false;
                this.spinloader = false;
              } else {
                this.cs.loaderStatus = false;
                this.spinloader = false;
                this.isDeviationTriggered = true;
                // console.log(this.isIDVChangeDev)
                this.deviationMsg =
                  "This case would go into deviation. Reason: " +
                  res.deviationMessage;
                // if(this.isIDVChangeDev){
                //   this.deviationMsg = 'The case has triggered deviation as the maximum and minimum limit has exceeded, exshowroom deviation is at -90%';
                // }else{
                //   this.deviationMsg = 'The quote has triggered deviation for OD discount as limit has exceeded';
                // }
              }
            } else {
              if (
                this.quoteDetails.PreviousPolicyDetails == true &&
                body.IsQCByPass == true &&
                this.ncbValue == 0
              ) {
                if (
                  !this.totalPremiumTP &&
                  this.ClaimOnPreviousPolicy == false
                ) {
                  this.showqcmsg = true;
                  // swal({ text: 'Congratulations, your policy will not be triggered for QC. Proceed for payment to get your instant policy' });
                }
              }

              if (this.isRecalculate1) {
                this.setStep(2);
              } else {
                this.setStep(1);
              }
              this.isDeviationTriggered = false;
              this.isOtherDiscDev = false;
              this.quoteDetails.OtherDiscount = 0;
              localStorage.setItem('ODAmount', JSON.stringify(this.quoteDetails.OtherDiscount));
            }
            this.bFlag = res.breakingFlag;
            if (res.breakingFlag == true) {
              this.cs.loaderStatus = false;
              this.spinloader = false;
              if (
                this.quoteDetails.PreviousPolicyDetails == false &&
                this.policyType == "ROLL" &&
                this.showsaod == false
              ) {
                this.quoteRequest = body;
                this.quoteResp = this.calculatedQuote = res;
                if (
                  res.riskDetails.breakinLoadingAmount &&
                  res.riskDetails.breakinLoadingAmount != 0
                ) {
                  this.basicbreak =
                    res.riskDetails.basicOD +
                    res.riskDetails.breakinLoadingAmount;
                  this.ODandTPAmount =
                    this.basicbreak + res.riskDetails.basicTP;
                } else {
                  this.basicbreak = res.riskDetails.basicOD;
                  this.ODandTPAmount =
                    this.basicbreak + res.riskDetails.basicTP;
                }
                if (this.calculatedQuote.riskDetails.voluntaryDiscount < 1) {
                  this.voluntaryDiscount = "1";
                } else if (
                  this.calculatedQuote.riskDetails.voluntaryDiscount > 1
                ) {
                  this.voluntaryDiscount = Math.round(
                    this.calculatedQuote.riskDetails.voluntaryDiscount
                  );
                }
                this.antitheftDiscount = Math.round(
                  this.calculatedQuote.riskDetails.antiTheftDiscount
                );
                this.cs.loaderStatus = false;
                this.spinloader = false;
                //Changes for one premium
                // this.PremiumCalculation();
                this.showAddon = true;
                this.quickquote = false;
                // if(!this.totalPremiumTP){
                this.panelOpenState = false;
                this.panelOpenStateAddon = true;
                // this.defaultPlan();
                // }
              } else {
                this.showAddon = true;
                this.quoteRequest = body;
                this.quoteResp = this.calculatedQuote = res;
                if (
                  res.riskDetails.breakinLoadingAmount &&
                  res.riskDetails.breakinLoadingAmount != 0
                ) {
                  this.basicbreak =
                    res.riskDetails.basicOD +
                    res.riskDetails.breakinLoadingAmount;
                  this.ODandTPAmount =
                    this.basicbreak + res.riskDetails.basicTP;
                } else {
                  this.basicbreak = res.riskDetails.basicOD;
                  this.ODandTPAmount =
                    this.basicbreak + res.riskDetails.basicTP;
                }
                if (this.calculatedQuote.riskDetails.voluntaryDiscount < 1) {
                  this.voluntaryDiscount = "1";
                } else if (
                  this.calculatedQuote.riskDetails.voluntaryDiscount > 1
                ) {
                  this.voluntaryDiscount = Math.round(
                    this.calculatedQuote.riskDetails.voluntaryDiscount
                  );
                }
                this.cs.loaderStatus = false;
                this.spinloader = false;
                //Changes for one premium
                // this.PremiumCalculation();

                this.quickquote = false;
                if (!this.totalPremiumTP) {
                  this.panelOpenState = false;
                  this.panelOpenStateAddon = true;
                }
                // this.defaultPlan();
              }

              if (res.isQuoteDeviation == true) {
                this.isDeviationTriggered = true;
              } else {
                this.isDeviationTriggered = false;
              }
              if (this.isRecalculate1) {
                // this.panelOpenStateProposal = true;
                this.setStep(2);
              } else {
                // this.panelOpenStateProposal = false;
                this.setStep(1);
              }
            } else {
              this.quoteRequest = body;
              this.quoteResp = this.calculatedQuote = res;
              this.basicbreak = res.riskDetails.basicOD;
              this.ODandTPAmount =
                res.riskDetails.basicOD + res.riskDetails.basicTP;
              this.cs.loaderStatus = false;
              this.spinloader = false;
              if (this.calculatedQuote.riskDetails.voluntaryDiscount < 1) {
                this.voluntaryDiscount = "1";
              } else if (
                this.calculatedQuote.riskDetails.voluntaryDiscount > 1
              ) {
                this.voluntaryDiscount = Math.round(
                  this.calculatedQuote.riskDetails.voluntaryDiscount
                );
              }
              (<any>window).ga("send", "event", {
                eventCategory: "Make Model",
                eventLabel:
                  res.generalInformation.manufacturerName +
                  "-" +
                  res.generalInformation.vehicleModel +
                  "" +
                  this.product +
                  "" +
                  this.policyType,
                eventAction: "Make Model Selected",
                eventValue: 10,
              });
              // this.defaultPlan();
              if (this.isRecalculate1) {
                // this.panelOpenStateProposal = true;
                this.setStep(2);
              } else {
                // this.panelOpenStateProposal = false;
                this.setStep(1);
              }
            }
          }

          localStorage.setItem("getVehiclDetalsClicked", "false");
        },
        (err: any) => {
          console.log("Error", err);
          this.setStep(0);
          this.totalPremiumTP = "Server Issue";
          // swal({ text: "There is some issue with data. Please try manually." });
          this.spinloader1 = false;
          this.failedquote = true;
          this.cs.loaderStatus = false;
          // this.setStep(0);
        }
      );
  }

  // All Risk Electric Bike - Sejal - 01-03-2022
  calculateAllriskEBquote(): Promise<any> {
    return new Promise((resolve: any) => {
      let button = document.getElementById("get_quote") as HTMLInputElement;
      let body: any;
      //let EVquoteFormData: any;
      this.id = Guid.raw();

      this.calculateAllRiskPolicyTenure(
        moment(this.quoteDetails.PolicyStartDate).format("YYYY-MM-DD"),
        moment(this.quoteDetails.PolicyEndDate).format("YYYY-MM-DD")
      );

      body = {
        BatteryNumber: this.quoteDetails.batterysaver,          // Included on 4-4-2022
        Model: this.quoteDetails.model,                         // Included on 4-4-2022
        CustomerState: this.quoteDetails.GSTToState,            // Included on 4-4-2022
        ManuFacturer: this.quoteDetails.Manufacturer,           // Included on 4-4-2022
        UniqueIDNo: this.quoteDetails.UniqueIDnumber,           // Included on 4-4-2022
        InvoiceNo: this.quoteDetails.InvoiceNumber,             // Included on 4-4-2022
        TransactionType: this.quoteDetails.TransactionType,     // Included on 4-4-2022
        ManufacturingYear: this.quoteDetails.ManufacturingYear, // Included on 4-4-2022
        CustomerType: this.registrationType,                    // Included on 4-4-2022
        PurchaseDate: this.quoteDetails.PurchaseDate,           // Included on 4-4-2022
        SumInsuredIDV: typeof (this.quoteDetails.SumInsuredIDV) == 'number' ? this.quoteDetails.SumInsuredIDV : parseInt(this.quoteDetails.SumInsuredIDV), //this.quoteDetails.SumInsuredIDV,         // Included on 4-4-2022
        PolicyStartDate: moment(this.quoteDetails.PolicyStartDate).format("YYYY-MM-DD"),	//"2022-02-08T00:00:00",
        PolicyEndDate: moment(this.quoteDetails.PolicyEndDate).format("YYYY-MM-DD"),		//"2023-02-07T00:00:00",
        policyTenure: this.evPolicyTenure, //parseInt(this.quoteDetails.Tenure),	//1,
        preExistingIllness: false,
        //Loading: 0,
        //Discount: 0,
        InwardDate: moment(this.quoteDetails.PolicyStartDate).format("YYYY-MM-DD"),		//"2022-02-08T00:00:00",
        // InsuredDOB: "1998-02-27T00:00:00",
        //UserRole: "COPS PLUS",
        //ApprovalRequired: false,
        StampDutyChargable: false,
        ProductDetails: {
          ProductCode: this.productCode,			                                    	//"50083",
          PlanDetails: [{
            PlanCode: commonData.allriskPlanCode,
            RiskSIComponentDetails: [{
              RiskSIComponent: "Electric Vehicle",			//"Electric Vehicle",
              CoverDetails: [{
                CoverCode: null,
                CoverName: "All risk EV",	//"All risk EV",
                CoverSI: this.quoteDetails.SumInsuredIDV.toString(),  //this.evSumInsured,  //"5000000",
                Column1: null,
                Column2: null,
                Column3: null,
                Column4: null,
                Column5: null
              }
              ]
            }
            ]
          }
          ]
        },
        TaxDetails: {
          GstServiceDetails: [{
            ILGICStateName: this.quoteDetails.GSTToState,		//"MAHARASHTRA",
            PartyStateName: this.quoteDetails.GSTToState,		//"MAHARASHTRA",
            //GrossAmount: 0,
            //TaxExempted: false
          }
          ]
        },
        DealId: this.dealid,					//"DL-4001/E/1365912",
        isRegisteredCustomer: null,
        CorrelationId: this.id							//"{{$guid}}"
      }

      // if(this.quoteDetails.TransactionType == "New"){
      //   body.BusinessType = "New";
      // } else {
      //   body.BusinessType = "Roll Over";
      // }
      if (this.registrationType == "INDIVIDUAL") {
        this.isValidSCPA = true;
        this.isSCPA = true;
      } else {
        this.isValidSCPA = false;
        this.isSCPA = false;
      }

      this.quoteRequ = body;
      let str = JSON.stringify(body);
      // localStorage.setItem("calQuoteReq", str);
      localStorage.setItem("EVQuoteReq", str);

      this.createAllRiskBizToken().then(() => {
        this.cs.loaderStatus = true;
        this.spinloader = true;
        // Sejal - 09-03-2022
        this.quoteservice = this.cs
          .allRiskEBpostBiz("/zerotat/quote/create", str)
          .subscribe(
            (res: any) => {
              this.spinloader1 = false;
              localStorage.setItem("EVQuoteRes", JSON.stringify(res));
              //this.totalPremium = res.finalPremium;
              if (res.statusMessage == "Failed" || res.statusMessage == null) {
                console.log("Failed");
                this.failedquote = true;
                this.failedCalculatedQuote = res;
                this.evBasePremium = Math.round(res.basicPremium);
                this.evTotalPremium = this.totalPremiumTP = Math.round(res.totalPremium);
                this.totalPremium = this.evTotalPremium;
                this.freezeManuModel = false;
                this.showAddon = false;
                let errormsg = res.message;
                this.api.handleerrors(errormsg);
                this.failerrormsg = errormsg;
                let errorbody = {
                  RequestJson: JSON.stringify(body),
                  ResponseJson: JSON.stringify(res),
                  ServiceURL:
                    commonData.bizURL + "/zerotat/proposal/create",
                  CorrelationID: this.id,
                };
                this.api.adderrorlogs(errorbody);
                this.cs.loaderStatus = false;
                this.spinloader = false;
                if (this.isRecalculate1) {
                  this.panelOpenStateProposal = false;
                  this.setStep(2);
                } else {
                  this.panelOpenStateProposal = false;
                  this.setStep(1);
                }
                resolve();
              } else {
                this.failedquote = false;
                this.showAddon = true;
                // if (this.isRecalculate1) {
                //   // this.quoteResp = res;
                this.quoteResp = this.calculatedQuote = res;
                // }
                this.cs.loaderStatus = false;
                this.spinloader = false;
                //Changes for one premium
                // this.PremiumCalculation();
                if (this.isValidSCPA == false || this.isSCPA == false || this.scpaPremium == null) {
                  this.scpaBasePremium = 0;
                  this.scpaGST = 0;
                  this.scpaPremium = 0;
                }
                this.evBasePremium = Math.round(res.basicPremium);
                this.evGST = Math.round(res.totalTax);
                this.totalGST = Math.round(res.totalTax) + this.scpaGST;
                this.evTotalPremium = this.totalPremiumTP = Math.round(res.totalPremium);
                this.totalPremium = this.evTotalPremium + this.scpaPremium;
                this.showAddon = true;
                this.quickquote = false;
                if (!this.totalPremiumTP) {
                  this.panelOpenState = false;
                  this.panelOpenStateAddon = true;
                }
                if (this.isRecalculate1) {
                  // this.panelOpenStateProposal = true;
                  this.setStep(2);
                } else {
                  // this.panelOpenStateProposal = false;
                  this.setStep(1);
                }
                resolve();
              }
            },
            (err: any) => {
              console.log("Error", err);
              this.totalPremiumTP = "Server Issue";
              // swal({
              //   text: "There is some issue with data. Please try manually.",
              // });
              this.spinloader1 = false;
              this.failedquote = true;
              this.cs.loaderStatus = false;
              this.setStep(0);
              resolve();
            }
          );
      });
    });
  }

  allriskSaveOfflineQuote() {
    let body: any;
    let quoteReq: any;
    let quoteRes: any;

    this.quoteRequ = quoteReq = JSON.parse(localStorage.getItem("EVQuoteReq"));
    console.log(quoteReq.SumInsuredIDV, typeof (quoteReq.SumInsuredIDV));

    if (typeof (quoteReq.SumInsuredIDV) == 'number') {
      quoteReq.SumInsuredIDV = quoteReq.SumInsuredIDV;
    } else {
      quoteReq.SumInsuredIDV = parseInt(quoteReq.SumInsuredIDV);
    }

    quoteRes = JSON.parse(localStorage.getItem("EVQuoteRes"));
    localStorage.setItem("BundleID", this.bundleID);

    body = {
      BundleID: this.bundleID,
      quoteRQ: quoteReq,
      quoteRS: quoteRes,
      IsDigitalPOS: this.IsPOSTransaction_flag, // Added digitalPOS parameter with value true or false based on value in localStorage
      isAllRisk: true,
    };

    if (
      this.paramDataValues.iPartnerLogin.isSubagent != null ||
      this.paramDataValues.iPartnerLogin.isSubagent != undefined
    ) {
      body.IsSubagent = true;
      body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
    }

    let str = JSON.stringify(body);
    this.cs.post("Quote/twev/SaveQuote", str).then((res: any) => {
      this.saveQuote = res;

      if (this.showAddon1 && !this.submitQuote) {
        this.setStep(2);
      } else {
        this.setStep(3);
      }
    });

  }

  scpaSaveOfflineQuote() {
    let body: any;
    let quoteReq: any;
    let quoteRes: any;

    // if (this.isSCPA == true && this.isValidSCPA == true) {
    this.quoteRequ = quoteReq = JSON.parse(localStorage.getItem("scpaQuoteReq"));

    if (typeof (quoteReq.SumInsuredIDV) == 'number') {
      quoteReq.SumInsuredIDV = quoteReq.SumInsuredIDV;
    } else {
      quoteReq.SumInsuredIDV = parseInt(quoteReq.SumInsuredIDV);
    }

    quoteRes = JSON.parse(localStorage.getItem("scpaQuoteRes"));

    body = {
      BundleID: this.bundleID,
      quoteRQ: quoteReq,
      quoteRS: quoteRes,
      IsDigitalPOS: this.IsPOSTransaction_flag, // Added digitalPOS parameter with value true or false based on value in localStorage
      isAllRisk: false,
    };
    // }


    if (
      this.paramDataValues.iPartnerLogin.isSubagent != null ||
      this.paramDataValues.iPartnerLogin.isSubagent != undefined
    ) {
      body.IsSubagent = true;
      body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
    }

    let str = JSON.stringify(body);
    this.cs.post("Quote/twev/SaveQuote", str).then((res: any) => {
      this.saveQuote = res;

      if (this.showAddon1 && !this.submitQuote) {
        this.setStep(2);
      } else {
        this.setStep(3);
      }
    });

  }

  calculateSCPA() {
    let body: any;
    //let EVquoteFormData: any;
    this.id = Guid.raw();

    this.calculateAllRiskPolicyTenure(
      moment(this.quoteDetails.PolicyStartDate).format("YYYY-MM-DD"),
      moment(this.quoteDetails.PolicyEndDate).format("YYYY-MM-DD")
    );

    body = {
      BatteryNumber: this.quoteDetails.batterysaver,          // Included on 4-4-2022
      Model: this.quoteDetails.model,                         // Included on 4-4-2022
      CustomerState: this.quoteDetails.GSTToState,            // Included on 4-4-2022
      ManuFacturer: this.quoteDetails.Manufacturer,           // Included on 4-4-2022
      UniqueIDNo: this.quoteDetails.UniqueIDnumber,           // Included on 4-4-2022
      InvoiceNo: this.quoteDetails.InvoiceNumber,             // Included on 4-4-2022
      TransactionType: this.quoteDetails.TransactionType,     // Included on 4-4-2022
      ManufacturingYear: this.quoteDetails.ManufacturingYear, // Included on 4-4-2022
      CustomerType: this.registrationType,                    // Included on 4-4-2022
      PurchaseDate: this.quoteDetails.PurchaseDate,           // Included on 4-4-2022
      SumInsuredIDV: typeof (this.quoteDetails.SumInsuredIDV) == 'number' ? this.quoteDetails.SumInsuredIDV : parseInt(this.quoteDetails.SumInsuredIDV),         // Included on 4-4-2022
      PolicyStartDate: moment(this.quoteDetails.PolicyStartDate).format("YYYY-MM-DD"),	//"2022-02-08T00:00:00",
      PolicyEndDate: moment(this.quoteDetails.PolicyEndDate).format("YYYY-MM-DD"),		//"2023-02-07T00:00:00",
      policyTenure: this.evPolicyTenure, //parseInt(this.quoteDetails.Tenure),	//1,
      preExistingIllness: false,
      //Loading: 0,
      //Discount: 0,
      InwardDate: moment(this.quoteDetails.PolicyStartDate).format("YYYY-MM-DD"),		//"2022-02-08T00:00:00",
      // InsuredDOB: "1998-02-27T00:00:00",
      //UserRole: "COPS PLUS",
      //ApprovalRequired: false,
      StampDutyChargable: false,
      ProductDetails: {
        ProductCode: commonData.scpaProductCode,			                                    	//this.productCode,
        PlanDetails: [{
          PlanCode: commonData.scpaPlancode,
          RiskSIComponentDetails: [{
            RiskSIComponent: "Person",
            CoverDetails: [{
              CoverCode: null,
              CoverName: "Compulsory Personal Accident Owner Driver Cover",
              CoverSI: this.scpaSumInsured.toString(),
              Column1: null,
              Column2: null,
              Column3: null,
              Column4: null,
              Column5: null
            }
            ]
          }
          ]
        }
        ]
      },
      TaxDetails: {
        GstServiceDetails: [{
          ILGICStateName: this.quoteDetails.GSTToState,		//"MAHARASHTRA",
          PartyStateName: this.quoteDetails.GSTToState,		//"MAHARASHTRA",
          //GrossAmount: 0,
          //TaxExempted: false
        }
        ]
      },
      DealId: this.dealid,					//"DL-4001/E/1365912",
      isRegisteredCustomer: null,
      CorrelationId: this.id							//"{{$guid}}"
    }

    this.quoteRequ = body;
    let str = JSON.stringify(body);
    // localStorage.setItem("calQuoteReq", str);
    localStorage.setItem("scpaQuoteReq", str);

    this.createAllRiskBizToken().then(() => {
      this.cs.loaderStatus = true;
      this.spinloader = true;
      // Sejal - 09-03-2022
      this.quoteservice = this.cs
        .allRiskEBpostBiz("/zerotat/quote/create", str)
        .subscribe(
          (res: any) => {
            this.spinloader1 = false;
            localStorage.setItem("scpaQuoteRes", JSON.stringify(res));
            this.scpaPremium = Math.round(res.totalPremium);
            if (res.statusMessage == "Failed" || res.statusMessage == null) {
              console.log("Failed");
              this.scpafailedquote = true;
              this.scpafailedCalculatedQuote = res;
              this.totalPremium = this.evTotalPremium;
              this.freezeManuModel = false;
              this.showAddon = false;
              let errormsg = res.message;
              this.api.handleerrors(errormsg);
              this.failerrormsg = errormsg;
              let errorbody = {
                RequestJson: JSON.stringify(body),
                ResponseJson: JSON.stringify(res),
                ServiceURL:
                  commonData.bizURL + "/zerotat/proposal/create",
                CorrelationID: this.id,
              };
              this.api.adderrorlogs(errorbody);
              this.cs.loaderStatus = false;
              this.spinloader = false;
              if (this.isRecalculate1) {
                this.panelOpenStateProposal = false;
                this.setStep(1);
              } else {
                this.panelOpenStateProposal = false;
                this.setStep(1);
              }
            } else {
              this.scpafailedquote = false;
              this.showAddon = true;
              this.cs.loaderStatus = false;
              this.spinloader = false;
              this.quickquote = false;
              this.scpacalculatedQuote = res;
              this.scpaBasePremium = Math.round(res.basicPremium);
              this.scpaGST = Math.round(res.totalTax);
              this.totalPremium = this.evTotalPremium + this.scpaPremium;
              this.totalGST = this.scpaGST + this.evGST;
              if (!this.totalPremium) {
                this.panelOpenState = false;
                this.panelOpenStateAddon = true;
              }
              // if (this.isRecalculate1) {
              //   // this.panelOpenStateProposal = true;
              //   this.setStep(2);
              // } else {
              //   // this.panelOpenStateProposal = false;
              //   this.setStep(1);
              // }
            }
          },
          (err: any) => {
            console.log("Error", err);
            this.totalPremiumTP = "Server Issue";
            // swal({
            //   text: "There is some issue with data. Please try manually.",
            // });
            this.spinloader1 = false;
            this.failedquote = true;
            this.cs.loaderStatus = false;
            this.setStep(0);
          }
        );
    });

  }

  calculateAllRiskPolicyTenure(StartDate, EndDate): Promise<any> {
    return new Promise((resolve: any) => {
      console.info(StartDate, EndDate);
      let fd = new Date(StartDate);
      let td = new Date(EndDate);
      // let difference = (td.getTime() - fd.getTime()) / (1000 * 3600 * 365.25 * 24);
      let difference = Math.round((Date.UTC(td.getFullYear(), td.getMonth(), td.getDate()) - Date.UTC(fd.getFullYear(), fd.getMonth(), fd.getDate())) / (1000 * 60 * 60 * 24 * 365.25));
      this.evPolicyTenure = difference;
      this.quoteDetails.evTenure = this.evPolicyTenure;
      console.info(difference);
      console.info(this.evPolicyTenure);
      //this.proposalDetails.PreviousPolicyTenure = this.previousPolicyTen;  
    })
  }

  paown(ev: any) {
    if (this.isValidDrivingLicense == true) {
      let x = document.getElementById("txtreg") as HTMLInputElement;
      x.checked = false;
      ev.target.checked = false;
    }
  }

  premiumPanel(ev) {
    console.log("premium", ev.target.outerText);
    /**
     * Added the condition for retriving the PYP flag value
     * Author :- Sumit
     * date :- 03-02-2022
     */
    if (
      this.quoteDetails.PreviousPolicyDetails != null &&
      this.quoteDetails.PreviousPolicyDetails != undefined
    ) {
      if (typeof this.quoteDetails.PreviousPolicyDetails == "boolean") {
        this.showPYP = this.quoteDetails.PreviousPolicyDetails;
      } else {
        this.showPYP = true;
      }
    }

    if (
      ev.target.outerText == "add" ||
      ev.target.outerText == "Premium Plans"
    ) {
      this.showAddon1 = false;
    }
    if (this.proposalPanelOpened == true) {
      this.panelOpenStateProposal = true;
      this.isexpanded = false;
    }
  }

  togglePropPanel(ev) {
    this.isexpanded = !this.isexpanded;
    this.panelOpenStateProposal = true;
  }
  noValid(ev: any) {
    if (ev.target.checked == true) {
      this.isValidDrivingLicense = true;
      let x = document.getElementById("paowner") as HTMLInputElement;
      x.checked = false;
    } else {
      this.isValidDrivingLicense = false;
      let x = document.getElementById("paowner") as HTMLInputElement;
      x.checked = true;
    }
  }

  handleAddOns(ev: any) {
    let addOnVal = ev.target.id;
    let Radiochecked = ev.target.checked;
    // let Radiochecked = ev1;
    this.radiocheck = Radiochecked;
    this.submitQuote = false;
    this.isRecalculate = false;
    this.isRecalculate1 = false;
    this.countaddon = 1;
    // let optionValue = ev.target.value;
    let vehicle;
    if (this.productCode == "2311") {
      vehicle = "4wheeler";
    }

    if (this.productCode == "2312") {
      vehicle = "2wheeler";
    }

    switch (addOnVal) {
      case "geographicalExt":
        if (Radiochecked == true) {
          this.isRecalculate = true;
          this.quoteDetails.IsExtensionCountry = true;
          this.quoteDetails.ExtensionCountryName =
            "Bhutan, Bangladesh, Nepal, Pakistan, Maldives, SriLanka";
        } else {
          this.isRecalculate = true;
          this.quoteDetails.IsExtensionCountry = false;
          this.quoteDetails.ExtensionCountryName = "";
        }
        break;
      case "UnnamedPassenger":
        if (Radiochecked == true) {
          this.isRecalculate = true;
          this.showValues = true;
          this.quoteDetails.IsPACoverUnnamedPassenger = true;
        } else {
          this.isRecalculate = true;
          this.showValues = false;
          this.quoteDetails.IsPACoverUnnamedPassenger = false;
        }
        break;
      case "paForUnnamedPassanger":
        this.isRecalculate = true;
        this.quoteDetails.IsPACoverUnnamedPassenger = true;
        break;
      case "tppd":
        this.isRecalculate = true;
        // this.quoteDetails.IsPACoverUnnamedPassenger = true;
        break;
      case "paidDriver":
        if (Radiochecked == true) {
          this.isRecalculate = true;
          this.PaidDrivervalue = true;
          this.quoteDetails.IsLegalLiabilityToPaidDriver = true;
          this.quoteDetails.NoOfDriver = this.quoteDetails.NoOfDriver
            ? this.quoteDetails.NoOfDriver
            : "1";
        } else {
          this.isRecalculate = true;
          this.PaidDrivervalue = false;
          this.quoteDetails.IsLegalLiabilityToPaidDriver = false;
          this.quoteDetails.NoOfDriver = "0";
        }
        break;
      case "NoOfDriver":
        this.isRecalculate = true;
        this.quoteDetails.IsLegalLiabilityToPaidDriver = true;
        break;
      case "paidEmployee":
        if (Radiochecked == true) {
          this.isRecalculate = true;
          this.PaidEmployeevalue = true;
          this.quoteDetails.IsLegalLiabilityToPaidEmployee = true;
          this.quoteDetails.NoOfEmployee = this.quoteDetails.NoOfEmployee
            ? this.quoteDetails.NoOfEmployee
            : "1";
        } else {
          this.isRecalculate = true;
          this.PaidEmployeevalue = false;
          this.quoteDetails.IsLegalLiabilityToPaidEmployee = false;
          this.quoteDetails.NoOfEmployee = "0";
        }
        break;
      case "NoOfEmployee":
        this.isRecalculate = true;
        this.quoteDetails.IsLegalLiabilityToPaidEmployee = true;
        break;
      case "Accessories":
        if (Radiochecked == true) {
          this.isRecalculate = true;
          if (this.quoteDetails.IsVehicleHaveCNGLPG == "CNG") {
            this.isRecalculate = true;
            this.quoteDetails.IsVehicleHaveCNG = true;
            this.quoteDetails.IsVehicleHaveLPG = false;
          } else if (this.quoteDetails.IsVehicleHaveCNGLPG == "LPG") {
            this.isRecalculate = true;
            this.quoteDetails.IsVehicleHaveLPG = true;
            this.quoteDetails.IsVehicleHaveCNG = false;
          } else {
            this.isRecalculate = true;
            this.quoteDetails.IsVehicleHaveCNG = false;
            this.quoteDetails.IsVehicleHaveLPG = false;
          }
        } else {
          this.isRecalculate = true;
          this.quoteDetails.IsVehicleHaveCNG = false;
          this.quoteDetails.IsVehicleHaveLPG = false;
        }
        break;
      case "accessories":
        if (this.quoteDetails.IsVehicleHaveCNGLPG == "CNG") {
          this.isRecalculate = true;
          this.quoteDetails.IsVehicleHaveCNG = true;
          this.quoteDetails.IsVehicleHaveLPG = false;
        } else if (this.quoteDetails.IsVehicleHaveCNGLPG == "LPG") {
          this.isRecalculate = true;
          this.quoteDetails.IsVehicleHaveLPG = true;
          this.quoteDetails.IsVehicleHaveCNG = false;
        } else {
          this.isRecalculate = true;
          this.quoteDetails.IsVehicleHaveCNG = false;
          this.quoteDetails.IsVehicleHaveLPG = false;
        }
        break;
      case "LPG_CNG":
        if (this.quoteDetails.SIVehicleHaveLPG_CNG == "0") {
          swal({
            text: "Enter the IDV / Capital Sum Insured as applicable against CNG/LPG",
          });
        } else {
          if (this.quoteDetails.IsVehicleHaveCNGLPG == "CNG") {
            this.isRecalculate = true;
            this.quoteDetails.IsVehicleHaveCNG = true;
            this.quoteDetails.IsVehicleHaveLPG = false;
          } else if (this.quoteDetails.IsVehicleHaveCNGLPG == "LPG") {
            this.isRecalculate = true;
            this.quoteDetails.IsVehicleHaveLPG = true;
            this.quoteDetails.IsVehicleHaveCNG = false;
          } else {
            this.isRecalculate = true;
            this.quoteDetails.IsVehicleHaveCNG = false;
            this.quoteDetails.IsVehicleHaveLPG = false;
          }
        }
        break;
      case "voluntaryDeductable":
        if (Radiochecked == true) {
          this.voluntaryDeductableValue = true;
          this.isRecalculate = true;
          this.quoteDetails.IsVoluntaryDeductible = true;
          this.voluntaryDeductableVal(vehicle);
        } else {
          this.isRecalculate = true;
          this.voluntaryDeductableValue = false;
          this.quoteDetails.IsVoluntaryDeductible = false;
        }
        break;
      case "volDed":
        this.isRecalculate = true;
        break;
      case "other":
        if (Radiochecked == true) {
          this.isRecalculate = true;
        } else {
          this.isRecalculate = true;
        }
        break;
      case "otherDiscount":
        this.isRecalculate = true;
        this.otherDiscount = this.quoteDetails.OtherDiscount;
        if (
          this.cs.isUndefineORNull(JSON.parse(localStorage.getItem("ODAmount")))
        ) {
          if (Number(this.quoteDetails.OtherDiscount) > 0) {
            this.isOtherDiscDev = true;
            localStorage.setItem(
              "ODAmount",
              JSON.stringify(this.quoteDetails.OtherDiscount)
            );
            localStorage.setItem(
              "ODTriggred",
              JSON.stringify(this.isOtherDiscDev)
            );
          } else {
            this.isOtherDiscDev = false;
            localStorage.setItem(
              "ODTriggred",
              JSON.stringify(this.isOtherDiscDev)
            );
            localStorage.removeItem("ODAmount");
          }
        } else {
          if (JSON.parse(localStorage.getItem("ODAmount")) > 0) {
            this.isOtherDiscDev = true;
            localStorage.removeItem("ODAmount");
            localStorage.setItem(
              "ODTriggred",
              JSON.stringify(this.isOtherDiscDev)
            );
          } else {
            this.isOtherDiscDev = false;
            localStorage.setItem(
              "ODTriggred",
              JSON.stringify(this.isOtherDiscDev)
            );
            localStorage.removeItem("ODAmount");
          }
        }
        break;
      case "NCBProtection":
        if (Radiochecked == true) {
          this.isncbProtect = true;
          this.isRecalculate = true;
          this.quoteDetails.isNCBProtect = true;
          this.getaddOnsData("ncbprotection", vehicle);
        } else {
          this.isRecalculate = true;
          this.isncbProtect = false;
          this.quoteDetails.isNCBProtect = false;
        }
        break;
      case "ncb":
        this.isRecalculate = true;
        break;
      case "RSAPlanName":
        if (Radiochecked == true) {
          this.isRSAPlan = true;
          this.isRecalculate = true;
          this.getRSAData();
        } else {
          this.isRecalculate = true;
          this.isRSAPlan = false;
        }
        break;
      case "rsa":
        this.isRecalculate = true;
        break;
      case "garageCashCover":
        if (Radiochecked == true) {
          this.isGarageCashCover = true;
          this.isRecalculate = true;
          this.quoteDetails.isGarageCash = true;
          this.getgarageCashData();
        } else {
          this.isRecalculate = true;
          this.isGarageCashCover = false;
          this.quoteDetails.isGarageCash = false;
        }
        break;
      case "garage":
        this.isRecalculate = true;
        break;
      case "ZeroDepPlanName":
        console.log('For ZD', this.vehicleAge, this.productCode)
        if (Radiochecked == true) {
          this.isZeroDepPlanName = true;
          this.isRecalculate = true;
          this.getZeroDepData();
          // if(this.vehicleAge > 5){
          //   let keyProtect = document.getElementById('keyProtect') as HTMLInputElement;
          //   let enginProtectPlus = document.getElementById('enginProtectPlus') as HTMLInputElement;
          //   keyProtect.checked = true;
          //   enginProtectPlus.checked = true;
          //   this.handleAddOns('keyProtect', true);
          //   this.handleAddOns('enginProtectPlus', true);
          // }
        } else {
          this.isRecalculate = true;
          this.isZeroDepPlanName = false;
        }
        break;
      case "zerodep":
        console.log('For ZD', this.vehicleAge)
        this.isZeroDepPlanName = true;
        this.isRecalculate = true;
        // if(this.vehicleAge > 5){
        //   let keyProtect = document.getElementById('keyProtect') as HTMLInputElement;
        //   let enginProtectPlus = document.getElementById('enginProtectPlus') as HTMLInputElement;
        //   keyProtect.checked = true;
        //   enginProtectPlus.checked = true;
        //   this.handleAddOns('keyProtect', true);
        //   this.handleAddOns('enginProtectPlus', true);
        // }
        break;
      case "noValidLicense":
        this.isRecalculate = true;
        break;
      case "perAccedentCover":
        this.isRecalculate = true;
        break;
      case "returnOfInvoice":
        if (Radiochecked == true) {
          this.isRecalculate = true;
          this.quoteDetails.IsRTIApplicableflag = true;
        } else {
          this.isRecalculate = true;
          this.quoteDetails.IsRTIApplicableflag = false;
        }
        break;
      case "Consumables":
        if (Radiochecked == true) {
          this.isRecalculate = true;
          this.quoteDetails.IsConsumables = true;
        } else {
          this.isRecalculate = true;
          this.quoteDetails.IsConsumables = false;
        }
        break;
      case "keyProtect":
        if (Radiochecked == true) {
          this.isRecalculate = true;
          this.isKeyProtectPlanName = true;
          this.getKeyProtectData();
        } else {
          this.isRecalculate = true;
          this.isKeyProtectPlanName = false;
          // if(this.vehicleAge > 5 && (this.isZeroDepPlanName && this.quoteDetails.IsEngineProtectPlus)){
          //   this.isRecalculate = true;
          //   this.isZeroDepPlanName = true;
          // }else{
          //   let zd = document.getElementById('ZeroDepPlanName') as HTMLInputElement;
          //   zd.checked = false;
          //   swal({ text: "Kindly select either Key Protect or Engine Protect" });
          //   this.isRecalculate = true;
          //   this.isZeroDepPlanName = false;            
          //   // this.handleAddOns('ZeroDepPlanName', false);
          // }
        }
        break;
      case "antitheft":
        if (Radiochecked == true) {
          this.isRecalculate = true;
          this.IsAntiTheftDisc = true;
        } else {
          this.isRecalculate = true;
          this.IsAntiTheftDisc = false;
        }
      case "selfinspection1":
        this.IsSelfInspection = true;
        this.isRecalculate = true;
        break;
      case "ilinspection1":
        this.IsSelfInspection = false;
        this.isRecalculate = true;
        break;
      case "automobile":
        if (Radiochecked == true) {
          this.isRecalculate = true;
          this.IsAutomobileAssocnFlag = true;
        } else {
          this.isRecalculate = true;
          this.IsAutomobileAssocnFlag = false;
        }
        break;
      case "handicap":
        if (Radiochecked == true) {
          this.isRecalculate = true;
          this.IsHandicapDisc = true;
        } else {
          this.isRecalculate = true;
          this.IsHandicapDisc = false;
        }
        break;
      case "KeyProtectPlan":
        this.isRecalculate = true;
        // if(this.vehicleAge > 5 && (this.isZeroDepPlanName && this.quoteDetails.IsEngineProtectPlus)){
        //   this.isRecalculate = true;
        //   this.isZeroDepPlanName = true;
        // }else{
        //   let zd = document.getElementById('ZeroDepPlanName') as HTMLInputElement;
        //   zd.checked = false;
        //   swal({ text: "Kindly select either Key Protect or Engine Protect" });
        //   this.isRecalculate = true;
        //   this.isZeroDepPlanName = false;
        //   // this.handleAddOns('ZeroDepPlanName', false);          
        // }
        break;
      case "lossOfPersonalBelongings":
        if (Radiochecked == true) {
          this.isRecalculate = true;
          this.isLossOfPersonalBelonging = true;
          this.getlossofPerData();
        } else {
          this.isRecalculate = true;
          this.isLossOfPersonalBelonging = false;
        }
        break;
      case "LossOfPersonalBelongingPlanName":
        this.isRecalculate = true;
        this.isLossOfPersonalBelonging = true;
        break;
      case "enginProtectPlus":
        if (Radiochecked == true) {
          this.isRecalculate = true;
          this.quoteDetails.IsEngineProtectPlus = true;
        } else {
          this.isRecalculate = true;
          this.quoteDetails.IsEngineProtectPlus = false;
          // if(this.vehicleAge > 5 && (this.isZeroDepPlanName && this.isKeyProtectPlanName)){
          //   this.isRecalculate = true;
          //   this.isZeroDepPlanName = true;
          // }else{
          //   let zd = document.getElementById('ZeroDepPlanName') as HTMLInputElement;
          //   zd.checked = false;
          //   swal({ text: "Kindly select either Key Protect or Engine Protect" });
          //   this.isRecalculate = true;
          //   this.isZeroDepPlanName = false;
          //   // this.handleAddOns('ZeroDepPlanName', false);

          // }
        }
        break;

      case "tyreProtect":
        if (Radiochecked == true) {
          this.isRecalculate = true;
          this.quoteDetails.IsTyreProtect = true;
        } else {
          this.isRecalculate = true;
          this.quoteDetails.IsTyreProtect = false;
        }
        break;

      case "emiProtect":
        if (Radiochecked == true) {
          $("#emiprotectModal").modal("show");
          this.isRecalculate = true;
          this.quoteDetails.IsEMIProtect = true;
        } else {
          this.isRecalculate = true;
          this.quoteDetails.IsEMIProtect = false;
        }
        break;

      default:
        this.isRecalculate = false;
        break;
    }
  }

  EmiProtect() {
    let body = {
      EMIAmount: this.quoteDetails.EMIAmount,
      NoofEMI: this.quoteDetails.NoofEMI,
      TimeExcessindays: this.quoteDetails.TimeExcessindays,
    };
    let str = JSON.stringify(body);
  }

  editPYPDetails() {
    // if(this.productCode == '2311' || this.productCode == '2312'){
    this.quoteDetails.IsExtensionCountry = false;
    this.IsSelfInspection = false;
    this.isZeroDepPlanName = false;
    this.isRSAPlan = false;
    this.quoteDetails.IsRTIApplicableflag = false;
    this.quoteDetails.IsConsumables = false;
    // }
    this.panelOpenState = true;
    this.panelOpenStateAddon = false;
    this.panelOpenStatePremium = false;
    this.panelOpenStateProposal = false;
    this.quoteDetails.PreviousPolicyDetails = true;
    this.previousPolicyValue = true;
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;
    if (this.quoteDetails.PreviousPolicyDetails == true) {
      this.quoteDetails.PreviousPolicyEndDate = moment(
        this.rtoDetail.insuranceUpto,
        "DD-MMM-YYYY"
      ).format("YYYY-MM-DD");
      this.getPrevEndDate(this.quoteDetails.PreviousPolicyEndDate);
    }
    // else if(this.quoteDetails.PreviousPolicyDetails == false) {
    //   let startDate = moment(new Date()).add(1, 'days').format('YYYY-MM-DD');
    //   // this.TodayDate = moment(startDate).format('DD-MM-YYYY');
    //   this.quoteDetails.PolicyStartDate = startDate;
    // }
  }

  editNoPYPDetails() {
    this.panelOpenState = true;
    this.panelOpenStateAddon = false;
    this.panelOpenStatePremium = false;
    this.panelOpenStateProposal = false;
    this.quoteDetails.PreviousPolicyDetails = false;
    this.previousPolicyValue = false;
    this.showAddon = false;
    this.systemQC = false;
    this.showAddon1 = false;
    // if(this.quoteDetails.PreviousPolicyDetails == true){
    //   this.quoteDetails.PreviousPolicyEndDate = moment(this.rtoDetail.insuranceUpto,'DD-MMM-YYYY').format('YYYY-MM-DD');
    //   this.getPrevEndDate(this.quoteDetails.PreviousPolicyEndDate);
    // } else
    if (this.quoteDetails.PreviousPolicyDetails == false) {
      let startDate = moment(new Date()).add(1, "days").format("YYYY-MM-DD");
      // this.TodayDate = moment(startDate).format('DD-MM-YYYY');
      this.quoteDetails.PolicyStartDate = startDate;
      if (this.productCode == "2320" && this.policyType == "NEW") {
        this.policyEndDate = moment(this.quoteDetails.PolicyStartDate)
          .add(5, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      } else if (this.productCode == "2319" && this.policyType == "NEW") {
        this.policyEndDate = moment(this.quoteDetails.PolicyStartDate)
          .add(3, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      } else {
        this.policyEndDate = moment(this.quoteDetails.PolicyStartDate)
          .add(1, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      }
    }
  }

  calculateCrawTP() {
    let body, URL, PreviousPolicyDetails;
    let quoteData = this.quoteDetails;
    this.id = Guid.raw();
    this.vehicleAge = moment().diff(
      this.quoteDetails.FirstRegistrationDate,
      "years"
    );
    let quoteRequest = JSON.parse(localStorage.getItem("calQuoteReq"));
    if (!this.cs.isUndefineORNull(quoteRequest)) {
      if (this.quoteDetails.PolicyStartDate != quoteRequest.PolicyStartDate) {
        quoteRequest.PolicyStartDate = this.quoteDetails.PolicyStartDate;
        // quoteRequest.PreviousPolicyDetails.PreviousPolicyEndDate = this.quoteDetails.PreviousPolicyEndDate;
      }
      if (
        this.quoteDetails.FirstRegistrationDate !=
        quoteRequest.FirstRegistrationDate
      ) {
        quoteRequest.FirstRegistrationDate =
          this.quoteDetails.FirstRegistrationDate;
      }

      // Sejal - 27-4-2022 - CPA Tenure Changes
      if (this.quoteDetails.PACoverTenure != quoteRequest.PACoverTenure) {
        quoteRequest.PACoverTenure = this.quoteDetails.PACoverTenure;
      }
    }
    if (!this.cs.isUndefineORNull(quoteRequest) && !this.isRecalculate1) {
      this.quoteRequ = this.quoteDetails = quoteRequest;
    } else {
      this.quoteRequ = this.quoteDetails = this.quoteDetails;
    }
    let dataForBind = JSON.parse(localStorage.getItem("dataforBack"));
    this.quoteDetails.rtO_LOCATION_DESC = dataForBind.rtO_LOCATION_DESC;
    this.quoteDetails.vehiclemodel = dataForBind.vehiclemodel;
    this.quoteDetails.PreviousInsurerName = !this.cs.isUndefineORNull(
      dataForBind.PreviousInsurerName
    )
      ? dataForBind.PreviousInsurerName
      : "OTHER";

    body = {
      ChannelSource: "IAGENT",
      CorrelationId: this.id,
      RegistrationNumber: this.quoteDetails.RegistrationNumber,
      VehicleMakeCode: this.makecode,
      VehicleModelCode: this.modelcode,
      ExShowRoomPrice: parseInt(this.exshowroomprice),
      RTOLocationCode: parseInt(this.rtoCode),
      DealId: this.dealid,
      TPPDLimit: this.TPPDLimit,
      PolicyStartDate: moment(this.quoteDetails.PolicyStartDate).format(
        "YYYY-MM-DD"
      ),
      PolicyEndDate: this.policyEndDate,
      GSTToState: this.quoteDetails.GSTToState,
      CustomerType: this.registrationType,
      ManufacturingYear: parseInt(this.quoteDetails.ManufacturingYear),
      DeliveryOrRegistrationDate: moment(
        this.quoteDetails.FirstRegistrationDate
      ).format("YYYY-MM-DD"),
      FirstRegistrationDate: moment(
        this.quoteDetails.FirstRegistrationDate
      ).format("YYYY-MM-DD"),
      PreviousPolicyDetails: null,
      IsExtensionCountry: this.quoteDetails.IsExtensionCountry,
      ExtensionCountryName: this.quoteDetails.ExtensionCountryName,
      IsPACoverUnnamedPassenger: this.quoteDetails.IsPACoverUnnamedPassenger,
      IsVehicleHaveLPG: this.quoteDetails.IsVehicleHaveLPG,
      IsVehicleHaveCNG: this.quoteDetails.IsVehicleHaveCNG,
      SIVehicleHaveLPG_CNG: this.quoteDetails.SIVehicleHaveLPG_CNG,
      IsLegalLiabilityToPaidDriver:
        this.quoteDetails.IsLegalLiabilityToPaidDriver,
      NoOfDriver: this.quoteDetails.NoOfDriver,
      IsLegalLiabilityToPaidEmployee:
        this.quoteDetails.IsLegalLiabilityToPaidEmployee,
      NoOfEmployee: this.quoteDetails.NoOfEmployee,
      SeatingCapacity: parseInt(this.seatingcapacity),
      isNoPrevInsurance: true
    };

    if (
      this.quoteReq == null &&
      (this.seatingcapacity == null ||
        this.seatingcapacity == undefined ||
        this.seatingcapacity == "")
    ) {
      this.seatingcapacity = this.manumod.find(
        (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
      ).seatingcapacity;
    }

    if (this.quoteDetails.IsPACoverUnnamedPassenger == true) {
      let PACovervalue =
        JSON.parse(this.quoteDetails.SIPACoverUnnamedPassenger) *
        JSON.parse(this.seatingcapacity);
      body.SIPACoverUnnamedPassenger = PACovervalue;
    }

    // let PreviousPolicyDetails;
    if (this.policyType == "ROLL") {
      body.isNoPrevInsurance = true;
      this.showPYP = true;
    }

    if (this.policyType == "NEW") {
      body.BusinessType = "New Business";
    } else {
      body.BusinessType = "Roll Over";
    }

    body.IsPACoverWaiver = true;
    // Sejal - 22-02-2022 - CPA Tenure new requirement for 2WTP & 4WTP NEW
    if (this.policyType == "NEW") {
      body.PACoverTenure = parseInt(this.quoteDetails.PACoverTenure);  // Sejal - 27-4-2022 - CPA Tenure Changes
      body.IsPACoverWaiver = false;     // Sejal - 23-02-2022 - CPA Tenure
    } else {
      body.PACoverTenure = this.cpaTenure;
      if (this.cpaTenure == 0) {
        body.PACoverTenure = 0;
        body.IsPACoverWaiver = true;
      } else {
        body.PACoverTenure = 1;
        body.IsPACoverWaiver = false;
      }
    }

    // Crawler Changes
    if (this.productCode == "2320" && this.policyType == "NEW") {
      URL = "/twowheelertp"; //tpcrawler endpoint change--monika
    }
    if (this.productCode == "2319" && this.policyType == "NEW") {
      URL = "/privatecartp"; //tpcrawler endpoint change--monika
    }
    if (this.productCode == "2320" && this.policyType != "NEW") {
      URL = "/twowheelertp"; //tpcrawler endpoint change--monika
    }
    if (this.productCode == "2319" && this.policyType != "NEW") {
      URL = "/privatecartp"; //tpcrawler endpoint change--monika
    }

    // Crawler Changes
    this.showAddon = true;
    this.quickquote = false;
    this.panelOpenState = false;
    this.panelOpenStateAddon = true;
    this.quoteRequ = body;
    let str = JSON.stringify(body);
    this.spinloader1 = true;
    this.cs.loaderStatus = true;
    localStorage.setItem("calQuoteReq", str);
    this.quoteservice = this.cs.postCrawlerBiz(URL, str).subscribe(
      (res: any) => {
        this.spinloader1 = false;
        localStorage.setItem("calQuoteRes", JSON.stringify(res));
        localStorage.removeItem('regNumber');
        this.totalPremium = this.totalPremiumTP = res.finalPremium;
        if (res.status == "Success") {
          if (res.message == "0") {
            this.calculateTPNewForCrawler(body);
            this.cs.loaderStatus = false;
            // this.calculatedQuote = res;
            this.quoteRequest = body;
            this.spinloader = false;
            // this.PremiumCalculation();
          } else {
            // this.totalPremium = this.totalPremiumTP = res.finalPremium;
            this.openQuoteSection = true;
            this.spinloader = false;
            this.quoteResp = this.calculatedQuote = res;
            this.quoteRequest = body;
            //Changes for one premium
            // this.PremiumCalculation();
            this.failedquote = false;
            // Crawler Changes
            this.showAddon = true;
            this.quickquote = false;
            if (!this.totalPremiumTP) {
              this.panelOpenState = false;
              this.panelOpenStateAddon = true;
            }
            if (this.isRecalculate1) {
              // this.panelOpenStateProposal = true;
              this.setStep(2);
            } else {
              // this.panelOpenStateProposal = false;
              this.setStep(1);
            }

            this.cs.loaderStatus = false;
            (<any>window).ga("send", "event", {
              eventCategory: "Make Model",
              eventLabel:
                res.generalInformation.manufacturerName +
                "-" +
                res.generalInformation.vehicleModel +
                "" +
                this.product +
                "" +
                this.policyType,
              eventAction: "Make Model Selected",
              eventValue: 10,
            });

          }
        } else {
          if (res.message == "0") {
            this.calculateTPNewForCrawler(body);
            this.cs.loaderStatus = false;
            // this.failedquote = true;
            this.openQuoteSection = true;
            this.spinloader = false;
          } else {
            this.cs.loaderStatus = false;
            this.failedquote = true;
            this.openQuoteSection = true;
            this.spinloader = false;
            // this.freezeManuModel = false;
            // let errormsg = res.message;
            // Common MSG
            // this.api.handleerrors(errormsg);
            // if(!this.cs.isUndefineORNull(errormsg)){
            //   let errMsg = this.msgService.errorMsg(errormsg)
            //   swal({ closeOnClickOutside: false, text: errMsg });
            // }
            this.calculateTPNewForCrawler(body);
            this.defaultPlan();
            // this.calculatedQuote = null;
            this.cs.loaderStatus = false;
            if (this.isRecalculate1) {
              this.panelOpenStateProposal = false;
              this.setStep(2);
            } else {
              this.panelOpenStateProposal = false;
              this.setStep(1);
            }
          }
        }
      },
      (err: any) => {
        console.log("Error", err);
        this.totalPremiumTP = "Server Issue";
        // swal({ text: "There is some issue with data. Please try manually." });
        this.spinloader1 = false;
        // this.failedquote = true;
        this.cs.loaderStatus = false;
        this.calculateTPNewForCrawler(body);
        // this.setStep(0);
      }
    );
  }

  calculateTPNewForCrawler(body: any) {
    let URL;
    this.id = Guid.raw();
    this.vehicleAge = moment().diff(
      this.quoteDetails.FirstRegistrationDate,
      "years"
    );

    if (this.productCode == "2320") {
      URL = "/quote/twowheelertp";
    }
    if (this.productCode == "2319") {
      URL = "/quote/pvtcartp";
    }

    let str = JSON.stringify(body);
    this.cs.loaderStatus = true;
    this.spinloader1 = true;
    this.quoteservice = this.cs.postBiz(URL, str).subscribe(
      (res: any) => {
        this.spinloader1 = false;
        localStorage.setItem("calQuoteRes", JSON.stringify(res));
        localStorage.removeItem('regNumber');
        this.totalPremium = this.totalPremiumTP = res.finalPremium;
        if (res.status == "Success") {
          // this.totalPremium = this.totalPremiumTP = res.finalPremium;
          this.openQuoteSection = true;
          this.failedquote = false;
          this.spinloader = false;
          this.quoteResp = this.calculatedQuote = res;
          this.quoteRequest = body;
          //Changes for one premium
          // this.PremiumCalculation();
          this.panelOpenState = false;
          this.panelOpenStateAddon = true;
          this.showAddon = true;
          this.quickquote = false;
          this.cs.loaderStatus = false;
          if (this.isRecalculate1) {
            // this.panelOpenStateProposal = true;
            this.setStep(2);
          } else {
            // this.panelOpenStateProposal = false;
            this.setStep(1);
          }
          (<any>window).ga("send", "event", {
            eventCategory: "Make Model",
            eventLabel:
              res.generalInformation.manufacturerName +
              "-" +
              res.generalInformation.vehicleModel +
              "" +
              this.product +
              "" +
              this.policyType,
            eventAction: "Make Model Selected",
            eventValue: 10,
          });
        } else {
          this.cs.loaderStatus = false;
          this.failedquote = true;
          this.openQuoteSection = true;
          this.spinloader = false;
          // this.calculatedQuote = res;
          // this.PremiumCalculation();
          this.totalPremiumTP3 = "0";
          // this.freezeManuModel = false;
          let errormsg = res.message;
          // Common MSG
          // this.api.handleerrors(errormsg);
          let errMsg = this.msgService.errorMsg(errormsg)
          swal({ closeOnClickOutside: false, text: errMsg });
          let errorbody = {
            RequestJson: JSON.stringify(body),
            ResponseJson: JSON.stringify(res),
            ServiceURL: commonData.bizURL + URL,
            CorrelationID: this.id,
          };
          this.api.adderrorlogs(errorbody);
          // this.defaultPlan();
          if (this.isRecalculate1) {
            this.panelOpenStateProposal = false;
            this.setStep(2);
          } else {
            this.panelOpenStateProposal = false;
            this.setStep(1);
          }
          this.cs.loaderStatus = false;
        }
      },
      (err) => {
        this.spinloader1 = false;
        this.cs.loaderStatus = false;
      }
    );
    // if(this.isRecalculate){
    //   this.setStep(2);
    // }else{
    //   this.setStep(1);
    // }
  }

  // Calculate TP Product 2.0
  calculateTP() {
    let body, URL;
    // let quoteData = this.quoteDetails;
    this.id = Guid.raw();
    this.vehicleAge = moment().diff(
      this.quoteDetails.FirstRegistrationDate,
      "years"
    );
    console.log(this.isRecalculate, this.isRecalculate1, this.quoteDetails);
    let quoteRequest = JSON.parse(localStorage.getItem("calQuoteReq"));
    if (!this.cs.isUndefineORNull(quoteRequest)) {
      if (this.quoteDetails.PolicyStartDate != quoteRequest.PolicyStartDate) {
        quoteRequest.PolicyStartDate = this.quoteDetails.PolicyStartDate;
        // quoteRequest.PreviousPolicyDetails.PreviousPolicyEndDate = this.quoteDetails.PreviousPolicyEndDate;
      }
      if (
        this.quoteDetails.FirstRegistrationDate !=
        quoteRequest.FirstRegistrationDate
      ) {
        quoteRequest.FirstRegistrationDate =
          this.quoteDetails.FirstRegistrationDate;
      }
      // Sejal - 27-4-2022 - CPA Tenure Changes
      if (this.quoteDetails.PACoverTenure != quoteRequest.PACoverTenure) {
        quoteRequest.PACoverTenure = this.quoteDetails.PACoverTenure;
      }
    }
    if (!this.cs.isUndefineORNull(quoteRequest) && !this.isRecalculate1) {
      this.quoteRequ = this.quoteDetails = quoteRequest;
    } else {
      this.quoteRequ = this.quoteDetails = this.quoteDetails;
    }

    let dataForBind = JSON.parse(localStorage.getItem("dataforBack"));
    this.quoteDetails.rtO_LOCATION_DESC = dataForBind.rtO_LOCATION_DESC;
    this.quoteDetails.vehiclemodel = dataForBind.vehiclemodel;
    this.quoteDetails.PreviousInsurerName = !this.cs.isUndefineORNull(
      dataForBind.PreviousInsurerName
    )
      ? dataForBind.PreviousInsurerName
      : "OTHER";
    body = {
      ChannelSource: "IAGENT",
      CorrelationId: this.id,
      RegistrationNumber: this.quoteDetails.RegistrationNumber,
      VehicleMakeCode: this.makecode,
      VehicleModelCode: this.modelcode,
      ExShowRoomPrice: parseInt(this.exshowroomprice),
      RTOLocationCode: parseInt(this.rtoCode),
      DealId: this.dealid,
      TPPDLimit: this.TPPDLimit,
      PolicyStartDate: moment(this.quoteDetails.PolicyStartDate).format(
        "YYYY-MM-DD"
      ),
      PolicyEndDate: this.policyEndDate,
      GSTToState: this.quoteDetails.GSTToState,
      CustomerType: this.registrationType,
      ManufacturingYear: parseInt(this.quoteDetails.ManufacturingYear),
      DeliveryOrRegistrationDate: moment(
        this.quoteDetails.FirstRegistrationDate
      ).format("YYYY-MM-DD"),
      FirstRegistrationDate: moment(
        this.quoteDetails.FirstRegistrationDate
      ).format("YYYY-MM-DD"),
      PreviousPolicyDetails: null,
      IsExtensionCountry: this.quoteDetails.IsExtensionCountry,
      ExtensionCountryName: this.quoteDetails.ExtensionCountryName,
      IsPACoverUnnamedPassenger: this.quoteDetails.IsPACoverUnnamedPassenger,
      IsVehicleHaveLPG: this.quoteDetails.IsVehicleHaveLPG,
      IsVehicleHaveCNG: this.quoteDetails.IsVehicleHaveCNG,
      SIVehicleHaveLPG_CNG: this.quoteDetails.SIVehicleHaveLPG_CNG,
      IsLegalLiabilityToPaidDriver:
        this.quoteDetails.IsLegalLiabilityToPaidDriver,
      NoOfDriver: this.quoteDetails.NoOfDriver,
      IsLegalLiabilityToPaidEmployee:
        this.quoteDetails.IsLegalLiabilityToPaidEmployee,
      NoOfEmployee: this.quoteDetails.NoOfEmployee,
    };

    if (
      this.quoteReq == null &&
      (this.seatingcapacity == null ||
        this.seatingcapacity == undefined ||
        this.seatingcapacity == "")
    ) {
      this.seatingcapacity = this.manumod.find(
        (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
      ).seatingcapacity;
    }

    console.log(this.quoteDetails.IsPACoverUnnamedPassenger, this.quoteDetails.SIPACoverUnnamedPassenger, this.seatingcapacity)

    if (this.quoteDetails.IsPACoverUnnamedPassenger == true) {
      let PACovervalue =
        parseInt(this.quoteDetails.SIPACoverUnnamedPassenger) *
        parseInt(this.seatingcapacity);
      body.SIPACoverUnnamedPassenger = PACovervalue;
    }

    // let PreviousPolicyDetails;
    if (this.policyType == "ROLL") {
      // PreviousPolicyDetails ={
      //   "previousPolicyStartDate": "2017-12-05",
      //   "previousPolicyEndDate":  "2018-12-04",
      //   "ClaimOnPreviousPolicy": 1,
      //   "PreviousPolicyType": "Comprehensive Package",
      //   "PreviousInsurerName": "GIC",
      //   "PreviousPolicyNumber": "123ABC",
      // }
      // body.PreviousPolicyDetails = PreviousPolicyDetails;
      body.isNoPrevInsurance = true;
      this.showPYP = true;
    }

    if (this.policyType == "NEW") {
      body.BusinessType = "New Business";
    } else {
      body.BusinessType = "Roll Over";
    }

    body.IsPACoverWaiver = true;
    // Sejal - 22-02-2022 - CPA Tenure new requirement for 2WTP & 4WTP NEW
    if (this.policyType == "NEW") {
      console.info("PA Cover Tenure TP", this.quoteDetails.PACoverTenure);
      body.PACoverTenure = parseInt(this.quoteDetails.PACoverTenure);    // Sejal - 27-4-2022 - CPA Tenure Changes
      body.IsPACoverWaiver = false;   // Sejal - 23-02-2022 - CPA Tenure
    } else {
      body.PACoverTenure = this.cpaTenure;
      if (this.cpaTenure == 0) {
        body.PACoverTenure = 0;
        body.IsPACoverWaiver = true;
      } else {
        body.PACoverTenure = 1;
        body.IsPACoverWaiver = false;
      }
    }

    if (this.productCode == "2320") {
      URL = "/quote/twowheelertp";
    }
    if (this.productCode == "2319") {
      URL = "/quote/pvtcartp";
    }

    // if (this.productCode == '2320') { URL = '/TwoWheelerTP'; }
    // if (this.productCode == '2319') { URL = '/PvtCarTP'; }
    this.quoteRequ = body;
    let str = JSON.stringify(body);
    localStorage.setItem("calQuoteReq", str);
    this.cs.loaderStatus = true;
    this.spinloader1 = false;
    // this.showAddon = true; this.quickquote = false;
    // this.panelOpenState = false; this.panelOpenStateAddon = true;
    // this.quoteservice = this.cs.postCrawlerBiz(URL, str).subscribe((res: any) => {
    this.quoteservice = this.cs.postBiz(URL, str).subscribe(
      (res: any) => {
        this.totalPremium = this.totalPremiumTP = res.finalPremium;
        this.spinloader1 = false;
        if (res.status == "Success") {
          localStorage.setItem("calQuoteRes", JSON.stringify(res));
          localStorage.removeItem('regNumber');
          // this.totalPremium = this.totalPremiumTP = res.finalPremium;
          this.openQuoteSection = true;
          this.spinloader = false;
          this.quoteResp = this.calculatedQuote = res;
          this.quoteRequest = body;
          this.failedquote = false;
          //Changes for one premium
          // this.PremiumCalculation();
          // this.showAddon = true; this.quickquote = false;
          // if (!this.totalPremiumTP) {
          //   this.panelOpenState = false; this.panelOpenStateAddon = true;
          // }

          this.panelOpenState = false;
          this.panelOpenStateAddon = true;
          this.showAddon = true;
          this.quickquote = false;

          this.cs.loaderStatus = false;
          if (this.isRecalculate1) {
            // this.panelOpenStateProposal = true;
            this.setStep(2);
          } else {
            // this.panelOpenStateProposal = false;
            this.setStep(1);
          }
          (<any>window).ga("send", "event", {
            eventCategory: "Make Model",
            eventLabel:
              res.generalInformation.manufacturerName +
              "-" +
              res.generalInformation.vehicleModel +
              "" +
              this.product +
              "" +
              this.policyType,
            eventAction: "Make Model Selected",
            eventValue: 10,
          });
        } else {
          this.cs.loaderStatus = false;
          this.failedquote = true;
          this.openQuoteSection = true;
          this.spinloader = false;
          // this.freezeManuModel = false;
          // this.calculatedQuote = res;
          // this.PremiumCalculation();
          this.totalPremiumTP3 = "0";
          let errormsg = res.message;
          // Common MSG
          // this.api.handleerrors(errormsg);
          let errMsg = this.msgService.errorMsg(errormsg)
          swal({ closeOnClickOutside: false, text: errMsg });
          let errorbody = {
            RequestJson: JSON.stringify(body),
            ResponseJson: JSON.stringify(res),
            ServiceURL: commonData.bizURL + URL,
            CorrelationID: this.id,
          };
          this.api.adderrorlogs(errorbody);
          this.defaultPlan();
          this.setStep(0);
          this.cs.loaderStatus = false;
        }
      },
      (err: any) => {
        console.log("Error", err);
        this.totalPremiumTP = "Server Issue";
        // swal({ text: "There is some issue with data. Please try manually." });
        this.spinloader1 = false;
        this.failedquote = true;
        this.cs.loaderStatus = false;
        this.setStep(0);
      }
    );
    // .catch((err: any) => {
    //   this.cs.loaderStatus = false;
    //   // swal({  text: 'At present system response is slow, kindly try after sometime. We regret the inconvenience'
    //   // })
    // });
    // if(this.isRecalculate){
    //   this.setStep(2);
    // }else{
    //   this.setStep(1);
    // }
  }

  rollback() {
    this.quoteDetails.OtherDiscount = 0;
    localStorage.setItem('ODAmount', JSON.stringify(this.quoteDetails.OtherDiscount));
    let req = JSON.parse(localStorage.getItem("calQuoteReq"));
    if (req.OtherDiscount != 0) {
      this.quoteResp = this.calculatedQuote = JSON.parse(
        localStorage.getItem("calQuoteResAfter")
      );
    } else {
      this.quoteResp = this.calculatedQuote = JSON.parse(
        localStorage.getItem("calQuoteRes")
      );
    }
  }

  // Calculate 4w quote
  calculate4Wquote() {
    let body: any;
    let PreviousPolicyDetails: any;
    console.log(
      this.isRecalculate,
      this.isRecalculate1,
      this.quoteDetails,
      this.ncbValue
    );
    if (this.isRecalculate1) {
      this.panelOpenStateProposal = false;
      this.setStep(2);
    } else {
      this.panelOpenStateProposal = false;
      this.setStep(1);
    }
    // this.isDeviationTriggered = false;
    let quoteRequest = JSON.parse(localStorage.getItem("calQuoteReq"));
    // let PreviousData = JSON.parse(localStorage.getItem("PreviousData"));
    if (!this.cs.isUndefineORNull(quoteRequest)) {
      if (this.quoteDetails.PolicyStartDate != quoteRequest.PolicyStartDate) {
        quoteRequest.PolicyStartDate = this.quoteDetails.PolicyStartDate;
        if (!this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyEndDate)) {
          quoteRequest.PreviousPolicyDetails.PreviousPolicyEndDate =
            this.quoteDetails.PreviousPolicyEndDate;
          //Changes for future date
          quoteRequest.PreviousPolicyDetails.PreviousPolicyStartDate = moment(
            this.quoteDetails.PreviousPolicyEndDate
          )
            .subtract(1, "years")
            .add(1, "days")
            .format("YYYY-MM-DD");
        }
      }

      console.log('For SAOD', this.showsaod, this.quoteDetails);
      if (this.showsaod) {
        quoteRequest.TPPolicyNo = this.quoteDetails.TPPolicyNo;
        quoteRequest.TPInsurerName = this.quoteDetails.TPInsurerName;
        quoteRequest.PreviousPolicyDetails.PreviousPolicyType = 'Bundled Package Policy';
        quoteRequest.TPTenure = '0';
        quoteRequest.Tenure = '1';
      } else {
        quoteRequest.TPTenure = '3';
        quoteRequest.Tenure = '1';
        // quoteRequest.PreviousPolicyDetails.PreviousPolicyType = 'Comprehensive Package';
      }

      if (!this.cs.isUndefineORNull(quoteRequest.PreviousPolicyDetails)) {
        if (
          this.quoteDetails.PreviousPolicyEndDate !=
          quoteRequest.PreviousPolicyDetails.PreviousPolicyEndDate
        ) {
          quoteRequest.PreviousPolicyDetails.PreviousPolicyEndDate =
            this.quoteDetails.PreviousPolicyEndDate;
          quoteRequest.PreviousPolicyDetails.PreviousPolicyStartDate = moment(
            this.quoteDetails.PreviousPolicyEndDate
          )
            .subtract(1, "years")
            .add(1, "days")
            .format("YYYY-MM-DD");
        }
      }

      // Sejal - 27-4-2022 - CPA Tenure Changes
      if (this.quoteDetails.PACoverTenure != quoteRequest.PACoverTenure) {
        quoteRequest.PACoverTenure = this.quoteDetails.PACoverTenure;
      }

      if (
        this.quoteDetails.FirstRegistrationDate !=
        quoteRequest.FirstRegistrationDate
      ) {
        quoteRequest.FirstRegistrationDate =
          this.quoteDetails.FirstRegistrationDate;
      }
    }
    if (
      !this.cs.isUndefineORNull(quoteRequest) &&
      !this.isRecalculate1 &&
      this.quoteDetails.OtherDiscount == 0 &&
      !quoteRequest.isNoPrevInsurance
    ) {
      let noOfClaims = this.quoteDetails.NoOfClaimsOnPreviousPolicy;
      this.quoteRequ = this.quoteDetails = quoteRequest;
      this.showPYP = quoteRequest.isNoPrevInsurance;
      if (
        typeof this.quoteDetails.PreviousPolicyDetails != "boolean" &&
        !this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyDetails)
      ) {
        PreviousPolicyDetails = this.quoteDetails.PreviousPolicyDetails;
        if (this.claimNCB) {
          this.ClaimOnPreviousPolicy = false;
          PreviousPolicyDetails = this.quoteDetails.PreviousPolicyDetails;
          PreviousPolicyDetails.TotalNoOfODClaims = "0";
          this.quoteDetails.TotalNoOfODClaims =
            PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy = "0";
          PreviousPolicyDetails.ClaimOnPreviousPolicy = false;
          this.quoteDetails.PreviousPolicyDetails = PreviousPolicyDetails;
          this.quoteRequ = this.quoteDetails = this.quoteDetails;
          this.PreviousPolicyDetails = this.quoteDetails.PreviousPolicyDetails;
        } else {
          this.ClaimOnPreviousPolicy = true;
          PreviousPolicyDetails = this.quoteDetails.PreviousPolicyDetails;
          PreviousPolicyDetails.TotalNoOfODClaims = noOfClaims
            ? noOfClaims
            : "1";
          this.quoteDetails.TotalNoOfODClaims =
            PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy = noOfClaims
              ? noOfClaims
              : "1";
          PreviousPolicyDetails.ClaimOnPreviousPolicy = true;
          this.quoteDetails.PreviousPolicyDetails = PreviousPolicyDetails;
          this.quoteRequ = this.quoteDetails = this.quoteDetails;
          this.PreviousPolicyDetails = this.quoteDetails.PreviousPolicyDetails;
        }
      } else {
        this.quoteRequ = this.quoteDetails = this.quoteDetails;
      }
    } else {
      let discount = JSON.parse(localStorage.getItem("ODAmount"));
      if (this.cs.isUndefineORNull(discount) && !this.isDeviationTriggered) {
        // localStorage.removeItem("ODAmount");
        this.quoteDetails.OtherDiscount = 0;
        localStorage.setItem('ODAmount', JSON.stringify(this.quoteDetails.OtherDiscount));
      }
      this.quoteRequ = this.quoteDetails = this.quoteDetails;
      if (
        typeof this.quoteDetails.PreviousPolicyDetails != "boolean" &&
        !this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyDetails)
      ) {
        PreviousPolicyDetails = this.PreviousPolicyDetails =
          this.quoteDetails.PreviousPolicyDetails;
      }
    }

    let dataForBind = JSON.parse(localStorage.getItem("dataforBack"));
    this.quoteDetails.rtO_LOCATION_DESC = dataForBind.rtO_LOCATION_DESC;
    this.quoteDetails.vehiclemodel = dataForBind.vehiclemodel;
    console.log(this.quoteDetails);
    this.quoteDetails.PreviousPolicyEndDate =
      typeof this.quoteDetails.PreviousPolicyDetails != "boolean" &&
        !this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyDetails)
        ? PreviousPolicyDetails.PreviousPolicyEndDate
        : this.quoteDetails.PreviousPolicyEndDate;
    this.quoteDetails.claims = this.ClaimOnPreviousPolicy
      ? "claimyes"
      : "claimno";
    this.quoteDetails.PreviousInsurerName = !this.cs.isUndefineORNull(
      dataForBind.PreviousInsurerName
    )
      ? dataForBind.PreviousInsurerName
      : "OTHER";
    if (dataForBind.showsaod) {
      this.quoteDetails.TPStartDate = dataForBind.TPStartDate;
      this.quoteDetails.TPEndDate = dataForBind.TPEndDate;
      this.quoteDetails.TPPolicyNo = dataForBind.TPPolicyNo;
      this.quoteDetails.PreviousPolicyNumber = dataForBind.PreviousPolicyNumber;
    }
    if (this.policyType != "NEW" && this.showsaod == true) {
      this.quoteDetails.TPTenure = "0";
      this.quoteDetails.Tenure = "1";
      this.quoteDetails.PreviousPolicyType = "Bundled Package Policy";
    } else if (this.showsaod == true && this.quoteReqrem.showsaod == true) {
      this.quoteDetails.TPTenure = "0";
      this.quoteDetails.Tenure = "1";
      this.quoteDetails.PreviousPolicyType = "Bundled Package Policy";
    }
    this.id = Guid.raw();
    console.log("EX-data", this.exshowroomprice);
    this.vehicleAge = moment().diff(
      this.quoteDetails.FirstRegistrationDate,
      "years"
    );
    if (this.policyType != "NEW" && this.showsaod == true) {
      this.quoteDetails.TPTenure = "0";
      this.quoteDetails.Tenure = "1";
      this.quoteDetails.PreviousPolicyType = "Bundled Package Policy";
    } else if (this.showsaod == true && this.quoteReqrem.showsaod == true) {
      this.quoteDetails.TPTenure = "0";
      this.quoteDetails.Tenure = "1";
      this.quoteDetails.PreviousPolicyType = "Bundled Package Policy";
    }

    // 4w new Request Body
    body = {
      CorrelationId: this.id,
      CustomerType: this.registrationType,
      RegistrationNumber: this.quoteDetails.RegistrationNumber,
      DealId: this.dealid,
      // DealId: "DL-3001/12325238",
      PolicyStartDate: moment(this.quoteDetails.PolicyStartDate).format(
        "YYYY-MM-DD"
      ), //YYY-MM-DD ,
      PolicyEndDate: this.policyEndDate, //YYY-MM-DD,
      // PolicyEndDate: this.PreviousPolicyDetails.PreviousPolicyEndDate ? this.PreviousPolicyDetails.PreviousPolicyEndDate : this.policyEndDate,
      VehicleMakeCode: this.makecode,
      VehicleModelCode: this.modelcode,
      Tenure: this.quoteDetails.Tenure,
      TPTenure: this.quoteDetails.TPTenure,
      PACoverTenure: this.quoteDetails.PACoverTenure,
      isPACoverWaiver: "false",
      RTOLocationCode: this.rtoCode,
      ManufacturingYear: this.quoteDetails.ManufacturingYear,
      DeliveryOrRegistrationDate: moment(
        this.quoteDetails.FirstRegistrationDate
      ).format("YYYY-MM-DD"), //YYY-MM-DD,
      FirstRegistrationDate: moment(
        this.quoteDetails.FirstRegistrationDate
      ).format("YYYY-MM-DD"), //YYY-MM-DD
      ExShowRoomPrice: parseInt(this.exshowroomprice),
      GSTToState: this.quoteDetails.GSTToState,
      isValidDrivingLicense: this.isValidDrivingLicense,
      IsLegalLiabilityToPaidEmployee:
        this.quoteDetails.IsLegalLiabilityToPaidEmployee,
      NoOfEmployee: this.quoteDetails.NoOfEmployee,
      IsPACoverUnnamedPassenger: this.quoteDetails.IsPACoverUnnamedPassenger,
      IsEngineProtectPlus: this.quoteDetails.IsEngineProtectPlus,
      IsConsumables: this.quoteDetails.IsConsumables,
      IsExtensionCountry: this.quoteDetails.IsExtensionCountry,
      ExtensionCountryName: this.quoteDetails.ExtensionCountryName,
      IsRTIApplicableflag: this.quoteDetails.IsRTIApplicableflag,
      IsVoluntaryDeductible: this.quoteDetails.IsVoluntaryDeductible,
      isGarageCash: this.quoteDetails.isGarageCash,
      isNCBProtect: this.quoteDetails.isNCBProtect,
      ChannelSource: "IAGENT",
      IsTyreProtect: this.quoteDetails.IsTyreProtect,
      TPPDLimit: this.TPPDLimit,
      IsLegalLiabilityToPaidDriver: true,
      NoOfDriver: "1",
      IsVehicleHaveLPG: this.quoteDetails.IsVehicleHaveLPG,
      IsVehicleHaveCNG: this.quoteDetails.IsVehicleHaveCNG,
      SIVehicleHaveLPG_CNG: this.quoteDetails.SIVehicleHaveLPG_CNG,
    };

    if (this.showsaod == true) {
      body.TPStartDate = moment(this.quoteDetails.TPStartDate).format(
        "YYYY-MM-DD"
      );
      body.TPEndDate = moment(this.quoteDetails.TPEndDate).format("YYYY-MM-DD");
      body.TPPolicyNo = this.quoteDetails.TPPolicyNo;
      body.TPInsurerName = this.quoteDetails.TPInsurerName;
    }

    if (this.quoteDetails.IsEMIProtect == true) {
      body.IsEMIProtect = true;
      body.EMIAmount = this.quoteDetails.EMIAmount;
      body.NoofEMI = this.quoteDetails.NoofEMI;
      body.TimeExcessindays = this.quoteDetails.TimeExcessindays;
    }

    if (this.isLossOfPersonalBelonging == true) {
      body.LossOfPersonalBelongingPlanName =
        this.quoteDetails.LossOfPersonalBelongingPlanName;
    }
    if (this.isKeyProtectPlanName == true) {
      body.KeyProtectPlan = this.quoteDetails.KeyProtectPlan;
    }
    if (
      this.isRSAPlan == true &&
      !this.cs.isUndefineORNull(this.quoteDetails.RSAPlanName)
    ) {
      body.RSAPlanName = this.quoteDetails.RSAPlanName;
    }

    if (
      this.isZeroDepPlanName == true &&
      !this.cs.isUndefineORNull(this.quoteDetails.ZeroDepPlanName)
    ) {
      body.ZeroDepPlanName = this.quoteDetails.ZeroDepPlanName;
    }

    if (
      this.quoteDetails.isGarageCash == true &&
      !this.cs.isUndefineORNull(this.quoteDetails.garageCashPlanName)
    ) {
      body.garageCashPlanName = this.quoteDetails.garageCashPlanName;
    }

    if (
      this.quoteDetails.isNCBProtect == true &&
      !this.cs.isUndefineORNull(this.quoteDetails.ncbProtectPlanName)
    ) {
      body.ncbProtectPlanName = this.quoteDetails.ncbProtectPlanName;
    }

    if (this.IsAntiTheftDisc == true) {
      body.IsAntiTheftDisc = this.IsAntiTheftDisc;
    }

    if (this.IsAutomobileAssocnFlag == true) {
      body.IsAutomobileAssocnFlag = this.IsAutomobileAssocnFlag;
    }

    if (this.IsHandicapDisc == true) {
      body.IsHandicapDisc = this.IsHandicapDisc;
    }
    console.log(this.quoteDetails.OtherDiscount, this.otherDiscount);
    if (!this.cs.isUndefineORNull(this.quoteDetails.OtherDiscount)) {
      body.OtherDiscount = this.otherDiscount ? this.otherDiscount : this.quoteDetails.OtherDiscount;
      if (this.quoteDetails.OtherDiscount > 0) {
        this.isOtherDiscDev = true;
        localStorage.setItem("ODTriggred", JSON.stringify(this.isOtherDiscDev));
      } else {
        this.isOtherDiscDev = false;
        localStorage.setItem("ODTriggred", JSON.stringify(this.isOtherDiscDev));
      }
    }

    // if (this.showdiscinput == true || this.isRecalculate1) {
    //   body.OtherDiscount = this.otherDiscount ? this.otherDiscount : this.quoteDetails.OtherDiscount;
    //   if(this.quoteDetails.OtherDiscount > 0.1){
    //     this.isOtherDiscDev = true;
    //     localStorage.setItem('ODTriggred', JSON.stringify(this.isOtherDiscDev));
    //   }
    // }

    if (this.quoteDetails.IsVoluntaryDeductible == true) {
      body.VoluntaryDeductiblePlanName =
        this.quoteDetails.VoluntaryDeductiblePlanName;
    }

    if (
      this.quoteReq == null &&
      (this.seatingcapacity == null ||
        this.seatingcapacity == undefined ||
        this.seatingcapacity == "")
    ) {
      this.seatingcapacity = this.manumod.find(
        (c: any) => c.vehiclemodel == this.quoteDetails.vehiclemodel
      ).seatingcapacity;
    }
    if (this.quoteDetails.IsPACoverUnnamedPassenger == true) {
      let PACovervalue =
        JSON.parse(this.quoteDetails.SIPACoverUnnamedPassenger) *
        JSON.parse(this.seatingcapacity);
      body.SIPACoverUnnamedPassenger = PACovervalue;
    }

    if (this.policyType == "ROLL" || this.policyType == "USED") {
      body.BusinessType = this.policyType == "ROLL" ? "Roll Over" : "Used";
      body.NomineeDetails = null;
      body.financierDetails = null;
      body.CustomerDetails = null;
      body.SPDetails = null;

      if (this.quoteDetails.TotalNoOfODClaims == "0") {
        this.ClaimOnPreviousPolicy = false;
      }

      if (this.showsaod == true) {
        this.quoteDetails.TPTenure = "0";
        this.quoteDetails.Tenure = "1";
        this.quoteDetails.PreviousPolicyType = "Bundled Package Policy";
      }

      if (
        this.quoteDetails.PreviousPolicyDetails == true ||
        this.showsaod == true
      ) {
        if (
          this.systemQC == true &&
          this.ncbValue == 0 &&
          this.ClaimOnPreviousPolicy == false
        ) {
          let PPED = this.quoteDetails.PreviousPolicyEndDate
            ? this.quoteDetails.PreviousPolicyEndDate
            : this.PreviousPolicyDetails.PreviousPolicyEndDate;
          if (PPED == "Invalid date") {
            body.IsQCByPass = false;
            body.isNoPrevInsurance = true;
            this.showPYP = true;
          } else {
            body.IsQCByPass = true;
            body.isNoPrevInsurance = false;
            this.showPYP = false;
            let prevPolStartDate = moment(PPED)
              .subtract(1, "years")
              .add(1, "days")
              .format("YYYY-MM-DD");
            this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
            let prevStartDate = moment(
              this.quoteDetails.PreviousPolicyStartDate
            ).format("YYYY-MM-DD");
            let prevEndDate = moment(PPED).format("YYYY-MM-DD");
            this.quoteDetails.PreviousPolicyEndDate = prevEndDate;
            this.cs
              .get(
                "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
                prevStartDate +
                "&prevEndDate=" +
                prevEndDate
              )
              .subscribe((res: any) => {
                this.previousPolicyTen = res;
                this.quoteDetails.PreviousPolicyTenure = this.previousPolicyTen;
              });

            this.PreviousPolicyDetails.PreviousPolicyType = this.quoteDetails
              .PreviousPolicyType
              ? this.quoteDetails.PreviousPolicyType
              : "Comprehensive Package";
            this.PreviousPolicyDetails.PreviousPolicyStartDate = moment(
              this.quoteDetails.PreviousPolicyStartDate
            ).format("YYYY-MM-DD");
            this.PreviousPolicyDetails.PreviousPolicyEndDate = moment(
              this.quoteDetails.PreviousPolicyEndDate
            ).format("YYYY-MM-DD");
            this.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
            this.PreviousPolicyDetails.PreviousPolicyNumber =
              this.quoteDetails.PreviousPolicyNumber;
            this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
              this.ClaimOnPreviousPolicy;
            this.PreviousPolicyDetails.TotalNoOfODClaims =
              this.quoteDetails.TotalNoOfODClaims;
            this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
              this.quoteDetails.TotalNoOfODClaims;
            this.PreviousPolicyDetails.PreviousInsurerName = this.companyCode;
            this.PreviousPolicyDetails.PreviousVehicleSaleDate = this
              .PreviousPolicyDetails.PreviousVehicleSaleDate
              ? this.PreviousPolicyDetails.PreviousVehicleSaleDate
              : "0001-01-01T00:00:00";
            this.quoteDetails.PreviousVehicleSaleDate =
              this.PreviousPolicyDetails.PreviousVehicleSaleDate;
            this.PreviousPolicyDetails.PreviousPolicyTenure =
              this.previousPolicyTen;

            body.PreviousPolicyDetails = this.PreviousPolicyDetails;
          }
          this.getInsurerName();
          let compName = this.insurerlist.find(
            (c) => c.companyName == this.quoteDetails.PreviousInsurerName
          );

          if (this.cs.isUndefineORNull(compName)) {
            this.companyCode = "OTHER";
          } else {
            this.companyCode = compName.companyCode;
          }
        } else {
          console.log(this.PreviousPolicyDetails);
          let PPED = this.quoteDetails.PreviousPolicyEndDate
            ? this.quoteDetails.PreviousPolicyEndDate
            : this.PreviousPolicyDetails.PreviousPolicyEndDate;
          if (PPED == "Invalid date") {
            body.IsQCByPass = false;
            body.isNoPrevInsurance = true;
            this.showPYP = true;
          } else {
            body.isNoPrevInsurance = false;
            this.showPYP = false;
            body.IsQCByPass = true;
            let prevPolStartDate = moment(PPED)
              .subtract(1, "years")
              .add(1, "days")
              .format("YYYY-MM-DD");
            this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
            let prevStartDate = moment(
              this.quoteDetails.PreviousPolicyStartDate
            ).format("YYYY-MM-DD");
            let prevEndDate = moment(PPED).format("YYYY-MM-DD");
            this.quoteDetails.PreviousPolicyEndDate = prevEndDate;
            this.cs
              .get(
                "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
                prevStartDate +
                "&prevEndDate=" +
                prevEndDate
              )
              .subscribe((res: any) => {
                this.previousPolicyTen = res;
                this.quoteDetails.PreviousPolicyTenure = this.previousPolicyTen;
              });

            this.PreviousPolicyDetails.PreviousPolicyType = this.quoteDetails
              .PreviousPolicyType
              ? this.quoteDetails.PreviousPolicyType
              : "Comprehensive Package";
            this.PreviousPolicyDetails.PreviousPolicyStartDate = moment(
              this.quoteDetails.PreviousPolicyStartDate
            ).format("YYYY-MM-DD");
            this.PreviousPolicyDetails.PreviousPolicyEndDate = moment(
              this.quoteDetails.PreviousPolicyEndDate
            ).format("YYYY-MM-DD");
            this.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
            if (this.showsaod == true) {
              this.PreviousPolicyDetails.PreviousPolicyNumber =
                this.quoteDetails.PreviousPolicyNumber;
            }
            this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
              this.ClaimOnPreviousPolicy;
            this.PreviousPolicyDetails.TotalNoOfODClaims =
              this.quoteDetails.TotalNoOfODClaims;
            this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
              this.quoteDetails.TotalNoOfODClaims;
            if (this.showsaod == true) {
              this.PreviousPolicyDetails.PreviousInsurerName = this.companyCode;
            }
            this.PreviousPolicyDetails.PreviousVehicleSaleDate = this
              .PreviousPolicyDetails.PreviousVehicleSaleDate
              ? this.PreviousPolicyDetails.PreviousVehicleSaleDate
              : "0001-01-01T00:00:00";
            this.quoteDetails.PreviousVehicleSaleDate =
              this.PreviousPolicyDetails.PreviousVehicleSaleDate;
            this.PreviousPolicyDetails.PreviousPolicyTenure =
              this.previousPolicyTen;

            body.PreviousPolicyDetails = this.PreviousPolicyDetails;
          }
          if (this.showsaod == true) {
            this.getInsurerName();
            let compName = this.insurerlist.find(
              (c) => c.companyName == this.quoteDetails.PreviousInsurerName
            );

            body.TPStartDate = moment(this.quoteDetails.TPStartDate).format(
              "YYYY-MM-DD"
            );
            console.log(this.quoteDetails);
            body.TPEndDate = moment(this.quoteDetails.TPEndDate).format("YYYY-MM-DD");
            body.TPPolicyNo = this.quoteDetails.TPPolicyNo;
            body.TPInsurerName = this.quoteDetails.TPInsurerName;
            this.PreviousPolicyDetails.PreviousInsurerName = this.companyCode;
            this.PreviousPolicyDetails.PreviousPolicyNumber = this.quoteDetails.PreviousPolicyNumber;
            body.PreviousPolicyDetails = this.PreviousPolicyDetails;
            if (this.cs.isUndefineORNull(compName)) {
              this.companyCode = "OTHER";
            } else {
              this.companyCode = compName.companyCode;
            }
          }
        }
      } else {
        console.log('ABCDEFG', this.quoteDetails.PreviousPolicyDetails, this.PreviousPolicyDetails, PreviousPolicyDetails)
        if (this.quoteDetails.PreviousPolicyEndDate == "Invalid date" || this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyEndDate) || this.quoteDetails.PreviousPolicyDetails == false) {
          // if(this.cs.isUndefineORNull(this.PreviousPolicyDetails)){
          // if(this.quoteDetails.PreviousPolicyEndDate == "Invalid date" || this.cs.isUndefineORNull(this.PreviousPolicyDetails.PreviousPolicyNumber)){
          body.isNoPrevInsurance = true;
          this.showPYP = true;
          body.IsQCByPass = false;
          // }else{
          //   body.isNoPrevInsurance = false;
          // this.showPYP = false;
          // body.IsQCByPass = true;
          // body.PreviousPolicyDetails = this.PreviousPolicyDetails;
          // body.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
          // }
        } else {
          body.isNoPrevInsurance = false;
          this.showPYP = false;
          body.IsQCByPass = true;
          body.PreviousPolicyDetails = this.PreviousPolicyDetails;
          body.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
        }
      }
      body.IsSelfInspection = this.IsSelfInspection;
    } else {
      body.BusinessType = "New Business";
      if (this.showPrev == true) {
        if (this.ncbValue == 0) {
          body.IsTransferOfNCB = false;
          if (this.showsaod == true) {
            this.getInsurerName();
            // this.companyCode = this.insurerlist.find(
            //   (c) => c.companyName == this.quoteDetails.PreviousInsurerName
            // ).companyCode;
            let compName = this.insurerlist.find(
              (c) => c.companyName == this.quoteDetails.PreviousInsurerName
            );
            if (this.cs.isUndefineORNull(compName)) {
              this.companyCode = "OTHER";
            } else {
              this.companyCode = compName.companyCode;
            }
          }
          // this.getInsurerName();
          // this.companyCode = this.insurerlist.find(c => c.companyName == this.quoteDetails.PreviousInsurerName).companyCode;
          let prevPolStartDate = moment(this.quoteDetails.PreviousPolicyEndDate)
            .subtract(1, "years")
            .add(1, "days")
            .format("YYYY-MM-DD");
          this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
          let prevStartDate = moment(
            this.quoteDetails.PreviousPolicyStartDate
          ).format("YYYY-MM-DD");
          let prevEndDate = moment(
            this.quoteDetails.PreviousPolicyEndDate
          ).format("YYYY-MM-DD");
          this.quoteDetails.PreviousPolicyEndDate = prevEndDate;
          this.cs
            .get(
              "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
              prevStartDate +
              "&prevEndDate=" +
              prevEndDate
            )
            .subscribe((res: any) => {
              this.previousPolicyTen = res;
              this.quoteDetails.PreviousPolicyTenure = this.previousPolicyTen;
            });

          this.PreviousPolicyDetails.PreviousPolicyType = this.quoteDetails
            .PreviousPolicyType
            ? this.quoteDetails.PreviousPolicyType
            : "Comprehensive Package";
          this.PreviousPolicyDetails.PreviousPolicyStartDate = prevStartDate;
          this.PreviousPolicyDetails.PreviousPolicyEndDate = prevEndDate;
          this.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
          if (this.showsaod == true) {
            this.PreviousPolicyDetails.PreviousPolicyNumber =
              this.quoteDetails.PreviousPolicyNumber;
          }
          this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
            this.ClaimOnPreviousPolicy;
          this.PreviousPolicyDetails.TotalNoOfODClaims =
            this.quoteDetails.TotalNoOfODClaims;
          this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
            this.quoteDetails.TotalNoOfODClaims;
          if (this.showsaod == true) {
            this.PreviousPolicyDetails.PreviousInsurerName = this.companyCode;
          }
          this.PreviousPolicyDetails.PreviousVehicleSaleDate = this
            .PreviousPolicyDetails.PreviousVehicleSaleDate
            ? this.PreviousPolicyDetails.PreviousVehicleSaleDate
            : moment(this.quoteDetails.PreviousVehicleSaleDate).format(
              "YYYY-MM-DD"
            );
          this.quoteDetails.PreviousVehicleSaleDate =
            this.PreviousPolicyDetails.PreviousVehicleSaleDate;
          this.PreviousPolicyDetails.PreviousPolicyTenure =
            this.previousPolicyTen;
          body.PreviousPolicyDetails = this.PreviousPolicyDetails;
        } else {
          if (this.showsaod == true) {
            this.getInsurerName();
            // this.companyCode = this.insurerlist.find(
            //   (c) => c.companyName == this.quoteDetails.PreviousInsurerName
            // ).companyCode;
            let compName = this.insurerlist.find(
              (c) => c.companyName == this.quoteDetails.PreviousInsurerName
            );
            if (this.cs.isUndefineORNull(compName)) {
              this.companyCode = "OTHER";
            } else {
              this.companyCode = compName.companyCode;
            }
          }
          if (
            !this.cs.isUndefineORNull(this.ncbValue) ||
            this.claimAdd != "claimyes"
          ) {
            body.TransferOfNCBPercent = parseInt(this.ncbValue);
            body.IsTransferOfNCB = this.isTransfer;
          }

          let prevPolStartDate = moment(this.quoteDetails.PreviousPolicyEndDate)
            .subtract(1, "years")
            .add(1, "days")
            .format("YYYY-MM-DD");
          this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
          let prevStartDate = moment(
            this.quoteDetails.PreviousPolicyStartDate
          ).format("YYYY-MM-DD");
          let prevEndDate = moment(
            this.quoteDetails.PreviousPolicyEndDate
          ).format("YYYY-MM-DD");
          this.quoteDetails.PreviousPolicyEndDate = prevEndDate;
          this.cs
            .get(
              "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
              prevStartDate +
              "&prevEndDate=" +
              prevEndDate
            )
            .subscribe((res: any) => {
              this.previousPolicyTen = res;
              this.quoteDetails.PreviousPolicyTenure = this.previousPolicyTen;
            });

          this.PreviousPolicyDetails.PreviousPolicyType = this.quoteDetails
            .PreviousPolicyType
            ? this.quoteDetails.PreviousPolicyType
            : "Comprehensive Package";
          this.PreviousPolicyDetails.PreviousPolicyStartDate = prevStartDate;
          this.PreviousPolicyDetails.PreviousPolicyEndDate = prevEndDate;
          this.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
          if (this.showsaod == true) {
            this.PreviousPolicyDetails.PreviousPolicyNumber =
              this.quoteDetails.PreviousPolicyNumber;
          }
          this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
            this.ClaimOnPreviousPolicy;
          this.PreviousPolicyDetails.TotalNoOfODClaims =
            this.quoteDetails.TotalNoOfODClaims;
          this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
            this.quoteDetails.TotalNoOfODClaims;
          if (this.showsaod == true) {
            this.PreviousPolicyDetails.PreviousInsurerName = this.companyCode;
          }
          this.PreviousPolicyDetails.PreviousVehicleSaleDate = this
            .PreviousPolicyDetails.PreviousVehicleSaleDate
            ? this.PreviousPolicyDetails.PreviousVehicleSaleDate
            : moment(this.quoteDetails.PreviousVehicleSaleDate).format(
              "YYYY-MM-DD"
            );
          this.quoteDetails.PreviousVehicleSaleDate =
            this.PreviousPolicyDetails.PreviousVehicleSaleDate;
          this.PreviousPolicyDetails.PreviousPolicyTenure =
            this.previousPolicyTen;
          body.PreviousPolicyDetails = this.PreviousPolicyDetails;
        }
      } else {
        body.IsTransferOfNCB = false;
      }
    }
    let str = JSON.stringify(body);
    let data = JSON.parse(localStorage.getItem('PreviousData'));
    if (this.cs.isUndefineORNull(data)) {
      localStorage.setItem("PreviousData", str);
    }
    localStorage.setItem("calQuoteReq", str);
    if (this.isRecalculate == true) {
      this.cs.loaderStatus = true;
      this.panelOpenStateAddon = true;
    } else {
      this.panelOpenStateAddon = false;
    }
    // if (!this.totalPremiumTP) { this.cs.loaderStatus = true; }

    this.panelOpenState = false;
    this.showAddon = true;
    this.quickquote = false;
    // this.createCommonQuote(body);
    if (str == this.premreq1) {
      console.log("samereq");
    } else {
      this.quoteRequ = body;
      let str = JSON.stringify(body);
      this.spinloader1 = true;
      this.quoteservice = this.cs
        .postBiz("/proposal/privatecarcalculatepremium", str)
        .subscribe(
          (res: any) => {
            this.spinloader1 = false;
            console.log(
              this.otherDiscount,
              this.quoteDetails.OtherDiscount,
              body.OtherDiscount
            );
            // if(this.cs.isUndefineORNull(body.OtherDiscount)){
            localStorage.setItem("calQuoteRes", JSON.stringify(res));
            localStorage.removeItem('regNumber');
            // }else{
            //   localStorage.setItem("calQuoteResAfter", JSON.stringify(res));
            // }
            console.log(
              this.quoteDetails.claims,
              this.quoteDetails.PreviousPolicyDetails
            );
            this.totalPremium = this.totalPremiumTP = res.finalPremium;
            console.log(this.isRecalculate, this.isRecalculate1);
            if (this.isRecalculate1) {
              this.reCalDealerDiscount = res;
            }
            if (res.status == "Failed" || res.status == null) {
              console.log("Failed");
              this.failedquote = true;
              this.failedCalculatedQuote = res;
              this.totalPremium = this.totalPremiumTP = res.finalPremium;
              this.freezeManuModel = false;
              let errormsg = res.message;
              // Common MSG
              // this.api.handleerrors(errormsg);
              let errMsg = this.msgService.errorMsg(errormsg)
              swal({ closeOnClickOutside: false, text: errMsg });
              this.failerrormsg = errormsg;
              let errorbody = {
                RequestJson: JSON.stringify(body),
                ResponseJson: JSON.stringify(res),
                ServiceURL:
                  commonData.bizURL + "/proposal/privatecarcalculatepremium",
                CorrelationID: this.id,
              };
              this.api.adderrorlogs(errorbody);
              this.cs.loaderStatus = false;
              this.spinloader = false;
              if (this.isRecalculate1) {
                this.panelOpenStateProposal = false;
                this.setStep(2);
              } else {
                this.panelOpenStateProposal = false;
                this.setStep(1);
              }
            } else {
              console.log("Success");
              if (this.showsaod == true && this.showSCPAinNysa) {
                let newBundleID: any;
                newBundleID = Guid.create();
                this.saodBundleID = newBundleID.value;
                this.quoteForSCPA();
              }
              // NEW PDF
              this.bFlag = res.breakingFlag;
              this.failedquote = false;
              if (this.isRecalculate1) {
                // this.quoteResp = res;
                this.quoteResp = this.calculatedQuote = res;
                let antiTheftDiscount = this.calculatedQuote.riskDetails
                  .antiTheftDiscount
                  ? this.calculatedQuote.riskDetails.antiTheftDiscount
                  : 0;
                let automobileAssociationDiscount = this.calculatedQuote
                  .riskDetails.automobileAssociationDiscount
                  ? this.calculatedQuote.riskDetails
                    .automobileAssociationDiscount
                  : 0;
                let handicappedDiscount = this.calculatedQuote.riskDetails
                  .handicappedDiscount
                  ? this.calculatedQuote.riskDetails.handicappedDiscount
                  : 0;
                let bonusDiscount = this.calculatedQuote.riskDetails
                  .bonusDiscount
                  ? this.calculatedQuote.riskDetails.bonusDiscount
                  : 0;
                let voluntaryDiscount = this.calculatedQuote.riskDetails
                  .voluntaryDiscount
                  ? this.calculatedQuote.riskDetails.voluntaryDiscount
                  : 0;
                let tppD_Discount = this.calculatedQuote.riskDetails
                  .tppD_Discount
                  ? this.calculatedQuote.riskDetails.tppD_Discount
                  : 0;
                this.totalDiscount =
                  antiTheftDiscount +
                  automobileAssociationDiscount +
                  handicappedDiscount +
                  bonusDiscount +
                  voluntaryDiscount +
                  tppD_Discount;
                let pkgPrem = this.calculatedQuote.packagePremium
                  ? this.calculatedQuote.packagePremium
                  : this.calculatedQuote.totalLiabilityPremium;
                this.premiumWithoutDiscount = pkgPrem + this.totalDiscount;
              }
              if (res.isQuoteDeviation == true) {
                this.isOtherDiscDev = false;
                if (this.policyType == "NEW") {
                  swal({
                    text:
                      "This case would go into deviation.Reason: " +
                      res.deviationMessage +
                      " Would you like to continue?",
                    dangerMode: true,
                    closeOnClickOutside: false,
                    buttons: ["Yes", "No"],
                  }).then((willDelete) => {
                    if (willDelete) {
                      console.log("Stay");
                    } else {
                      let type;
                      if (this.policyType == "NEW") {
                        type = "N";
                      } else {
                        type = "R";
                      }
                      this.cs.geToDashboardapproval(this.productCode, type);
                    }
                  });
                  this.cs.loaderStatus = false;
                  this.spinloader = false;
                } else {
                  this.cs.loaderStatus = false;
                  this.spinloader = false;
                  this.isDeviationTriggered = true;
                  this.deviationMsg =
                    "This case would go into deviation. Reason: " +
                    res.deviationMessage;
                  // if(this.isIDVChangeDev){
                  //   this.deviationMsg = 'The case has triggered deviation as the maximum and minimum limit has exceeded, exshowroom deviation is at -90%';
                  // }else{
                  //   this.deviationMsg = 'The quote has triggered deviation for OD discount as limit has exceeded';
                  // }
                }
              } else {
                if (
                  this.quoteDetails.PreviousPolicyDetails == true &&
                  body.IsQCByPass == true &&
                  this.ncbValue == 0
                ) {
                  if (
                    !this.totalPremiumTP &&
                    this.ClaimOnPreviousPolicy == false
                  ) {
                    this.showqcmsg = true;
                    // swal({ text: 'Congratulations, your policy will not be triggered for QC. Proceed for payment to get your instant policy' });
                  }
                }
                if (this.isRecalculate1) {
                  // this.panelOpenStateProposal = true;
                  this.setStep(2);
                } else {
                  // this.panelOpenStateProposal = false;
                  this.setStep(1);
                }
                this.isDeviationTriggered = false;
                this.isOtherDiscDev = false;
                this.quoteDetails.OtherDiscount = 0;
                localStorage.setItem('ODAmount', JSON.stringify(this.quoteDetails.OtherDiscount));
              }
              this.bFlag = res.breakingFlag;
              if (res.breakingFlag == true) {
                this.cs.loaderStatus = false;
                this.spinloader = false;
                if (
                  this.quoteDetails.PreviousPolicyDetails == false &&
                  (this.policyType == "ROLL" || this.policyType == "USED") &&
                  this.showsaod == false
                ) {
                  this.showAddon = true;
                  this.quoteResp = this.calculatedQuote = res;
                  if (
                    res.riskDetails.breakinLoadingAmount &&
                    res.riskDetails.breakinLoadingAmount != 0
                  ) {
                    this.basicbreak =
                      res.riskDetails.basicOD +
                      res.riskDetails.breakinLoadingAmount;
                    this.ODandTPAmount =
                      this.basicbreak + res.riskDetails.basicTP;
                  } else {
                    this.basicbreak = res.riskDetails.basicOD;
                    this.ODandTPAmount =
                      this.basicbreak + res.riskDetails.basicTP;
                  }
                  this.quoteRequest = body;
                  if (this.calculatedQuote.riskDetails.voluntaryDiscount < 1) {
                    this.voluntaryDiscount = "1";
                  } else if (
                    this.calculatedQuote.riskDetails.voluntaryDiscount > 1
                  ) {
                    this.voluntaryDiscount = Math.round(
                      this.calculatedQuote.riskDetails.voluntaryDiscount
                    );
                  }
                  this.cs.loaderStatus = false;
                  this.spinloader = false;
                  //Changes for one premium
                  // this.PremiumCalculation();
                  this.showAddon = true;
                  this.quickquote = false;
                  // if (!this.totalPremiumTP) {
                  //   this.panelOpenState = false; this.panelOpenStateAddon = true;
                  // }
                  // this.defaultPlan();
                } else {
                  this.showAddon = true;
                  this.quoteResp = this.calculatedQuote = res;
                  if (
                    res.riskDetails.breakinLoadingAmount &&
                    res.riskDetails.breakinLoadingAmount != 0
                  ) {
                    this.basicbreak =
                      res.riskDetails.basicOD +
                      res.riskDetails.breakinLoadingAmount;
                    this.ODandTPAmount =
                      this.basicbreak + res.riskDetails.basicTP;
                  } else {
                    this.basicbreak = res.riskDetails.basicOD;
                    this.ODandTPAmount =
                      this.basicbreak + res.riskDetails.basicTP;
                  }
                  this.quoteRequest = body;
                  if (this.calculatedQuote.riskDetails.voluntaryDiscount < 1) {
                    this.voluntaryDiscount = "1";
                  } else if (
                    this.calculatedQuote.riskDetails.voluntaryDiscount > 1
                  ) {
                    this.voluntaryDiscount = Math.round(
                      this.calculatedQuote.riskDetails.voluntaryDiscount
                    );
                  }
                  this.cs.loaderStatus = false;
                  this.spinloader = false;
                  //Changes for one premium
                  // this.PremiumCalculation();
                  this.showAddon = true;
                  this.quickquote = false;
                  if (!this.totalPremiumTP) {
                    this.panelOpenState = false;
                    this.panelOpenStateAddon = true;
                  }
                  // this.defaultPlan();
                }
                if (res.isQuoteDeviation == true) {
                  this.isDeviationTriggered = true;
                } else {
                  this.isDeviationTriggered = false;
                }

                if (this.isRecalculate1) {
                  // this.panelOpenStateProposal = true;
                  this.setStep(2);
                } else {
                  // this.panelOpenStateProposal = false;
                  this.setStep(1);
                }
              } else {
                // this.isDeviationTriggered = false;
                this.quoteResp = this.calculatedQuote = res;
                this.basicbreak = res.riskDetails.basicOD;
                this.ODandTPAmount =
                  res.riskDetails.basicOD + res.riskDetails.basicTP;
                this.quoteRequest = body;
                if (this.calculatedQuote.riskDetails.voluntaryDiscount < 1) {
                  this.voluntaryDiscount = "1";
                } else if (
                  this.calculatedQuote.riskDetails.voluntaryDiscount > 1
                ) {
                  this.voluntaryDiscount = Math.round(
                    this.calculatedQuote.riskDetails.voluntaryDiscount
                  );
                }
                this.cs.loaderStatus = false;
                this.spinloader = false;
                //Changes for one premium
                // this.PremiumCalculation();
                this.showAddon = true;
                this.quickquote = false;
                if (!this.totalPremiumTP) {
                  this.panelOpenState = false;
                  this.panelOpenStateAddon = true;
                }
                // this.defaultPlan();
                if (this.isRecalculate1) {
                  // this.panelOpenStateProposal = true;
                  this.setStep(2);
                } else {
                  // this.panelOpenStateProposal = false;
                  this.setStep(1);
                }
              }
            }
            localStorage.setItem("getVehiclDetalsClicked", "false");
          },
          (err: any) => {
            console.log("Error", err);
            this.totalPremiumTP = "Server Issue";
            // swal({
            //   text: "There is some issue with data. Please try manually.",
            // });
            this.spinloader1 = false;
            this.failedquote = true;
            this.cs.loaderStatus = false;
            this.setStep(0);
          }
        );
    }
  }

  // Go to Dashboard
  goToDashboard() {
    this.cs.geToDashboard();
  }

  checkGetVahicleOnRTO(ev, type) {
    if (
      (this.policyType == "ROLL" || this.policyType == "USED") &&
      this.getVehicleDetailsClicked == true
    ) {
      this.onBlurSelectFirstVal(ev, type);
    } else {
      if (this.policyType == "NEW") {
        this.onBlurSelectFirstVal(ev, type);
      } else {
        // swal({ text: 'Please click on Get Vehicle Details to retrieve vehicle data' });
      }
    }
  }

  // Get RTO List
  getRTOList(ev: any) {
    if (ev.target.value.length < 3 || ev.key == "Backspace") {
    } else {
      this.RTOname = ev.target.value;
      this.cs
        .get(
          "RTO/GetRTOList?rtoName=" +
          this.RTOname +
          "&classCode=" +
          this.classCode
        )
        .subscribe((res: any) => {
          this.rtoList = res;
          console.log(res);
          this.keyword = this.rtoList.rtO_LOCATION_DESC;
          this.rtoCode = this.rtoList.rtO_LOCATION_CODE;
          if (res.length == 0) {
            this.rtoerror = true;
          } else {
            this.rtoerror = false;
          }
        });
    }
  }

  onInsurerChange(ev: any, type: any) {
    console.log('BYE', ev.option.value, type)
    if (type == "insurer") {
      let obj = this.insurerlist.find((insList) => insList.companyName === ev.option.value);
      if (obj == undefined || obj == "" || obj == null) {
        this.quoteDetails.PreviousInsurerName = this.insurerlist[0].companyName;
      } else {
        this.quoteDetails.PreviousInsurerName = obj.companyName;
      }
    } else if (type == "insurerTP") {
      let obj = this.insurerlist.find((insList) => insList.companyName === ev.option.value);
      if (obj == undefined || obj == "" || obj == null) {
        this.quoteDetails.TPInsurerName = this.insurerlist[0].companyName;
      } else {
        this.quoteDetails.TPInsurerName = obj.companyName;
      }
      // for PF Changes on sanity
      // if (obj == undefined || obj == "" || obj == null) {
      //   this.quoteDetails.TPInsurerName = this.insurerlist[0].shortName;
      // } else {
      //   this.quoteDetails.TPInsurerName = obj.shortName;
      // }
    }
  }

  onBlurSelectFirstVal(ev, type) {
    if (type == "rtoCity") {
      if (this.rtoList.length == 0) {
        this.errorRTO = true;
        // swal({ text: 'Select correct RTO location' }).then(()=> { document.getElementById('rtodetail').focus();});
        // document.getElementById('rtodetail').focus;
        this.cs.loaderStatus = false;
      } else {
        this.errorRTO = false;
        let obj = this.rtoList.find((rto) => rto.rtO_LOCATION_DESC === ev);
        if (obj == undefined || obj == "" || obj == null) {
          this.quoteDetails.rtO_LOCATION_DESC =
            this.rtoList[0].rtO_LOCATION_DESC;
          console.log("RTO COde", this.rtoList[0]);
          let data = this.quoteDetails.rtO_LOCATION_DESC.split("-");
          if (data[0] == "TAMILNADU") {
            this.quoteDetails.GSTToState = "TAMIL NADU";
          } else {
            this.quoteDetails.GSTToState = data[0];
          }
          this.rtoCode = this.rtoList[0].rtO_LOCATION_CODE;
          this.modelvalue(this.quoteDetails.rtO_LOCATION_DESC);
        }
      }
    } else if (type == "insurer") {
      let obj = this.insurerlist.find((insList) => insList.companyName === ev);
      if (obj == undefined || obj == "" || obj == null) {
        this.quoteDetails.PreviousInsurerName = this.insurerlist[0].companyName;
      }
    } else if (type == "insurerTP") {
      let obj = this.insurerlist.find((insList) => insList.companyName === ev);
      if (obj == undefined || obj == "" || obj == null) {
        this.quoteDetails.TPInsurerName = this.insurerlist[0].companyName;
        //Sanity
        // this.quoteDetails.TPInsurerName = this.insurerlist[0].shortName;
      }
    } else if (type == "manumod") {
      if (this.manumod.length == 0) {
        this.manerror = true;
        // swal({ text: 'Select correct manufacturer model' }).then(()=> { document.getElementById('manufacturer').focus();}); this.cs.loaderStatus = false;
        // document.getElementById('manufacturer').focus();
        this.cs.loaderStatus = false;
      } else {
        this.manerror = false;
        let obj = this.manumod.find((manu) => manu.vehiclemodel === ev);
        if (obj == undefined || obj == "" || obj == null) {
          this.quoteDetails.vehiclemodel = this.manumod[0].vehiclemodel;
          let data = this.quoteDetails.rtO_LOCATION_DESC.split("-");
          if (data[0] == "TAMILNADU") {
            this.quoteDetails.GSTToState = "TAMIL NADU";
          } else {
            this.quoteDetails.GSTToState = data[0];
          }
          this.modelvalue(this.quoteDetails.vehiclemodel);
        } else {
          console.log(obj);
          // Need Exshowroom API
          this.modelcode = obj.vehiclemodelcode;
          this.makecode = obj.vehiclemanufacturecode;
          let firstReg = moment(this.quoteDetails.FirstRegistrationDate).format(
            "YYYY-MM-DD"
          );
          let polstar = moment(this.quoteDetails.PolicyStartDate).format(
            "YYYY-MM-DD"
          );
          this.cs
            .getExshowroomToken()
            .then((resp: any) => {
              console.log(resp);
              localStorage.setItem("exShowroomToken", JSON.stringify(resp));
              this.getRedisQuoteExShowroom(polstar, firstReg).then(() => { });
            })
            .catch((err: any) => {
              swal({
                text: "There is some issue with system.",
                closeOnClickOutside: false,
              });
            });
        }
      }
    }
  }

  rtovaluechange(ev: any) {
    if (ev.length < 10) {
      swal({ text: "Select correct RTO location" }).then(() => {
        document.getElementById("rtodetail").focus();
      });
    } else {
      console.log("abc");
    }
  }

  RegNumChange(ev: any) {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;
  }

  modelvalue(ev: any) {
    this.rtoLength = ev.length;
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.systemQC = false;
    this.panelOpenStateProposal = false;
    this.proposalPanelOpened = false;
    if (
      this.quoteDetails.RegistrationNumber != "" &&
      this.quoteDetails.rtO_LOCATION_DESC != "" &&
      this.quoteDetails.vehiclemodel != "" &&
      this.quoteDetails.FirstRegistrationDate != ""
    ) {
      this.showIDV = true;
      this.findexshowroom();
    } else {
      this.showIDV = false;
    }
    let obj = this.rtoList.find(
      (rto) => rto.rtO_LOCATION_DESC === this.quoteDetails.rtO_LOCATION_DESC
    );
    console.log(obj);
    if (ev == this.quoteDetails.rtO_LOCATION_DESC) {
      let data = this.quoteDetails.rtO_LOCATION_DESC.split("-");
      if (data[0] == "TAMILNADU") {
        this.quoteDetails.GSTToState = "TAMIL NADU";
      } else {
        this.quoteDetails.GSTToState = data[0];
      }
      this.rtoCode = obj.rtO_LOCATION_CODE;
    }
    this.quoteDetails.IsExtensionCountry = false;
    // this.IsSelfInspection = false;
    this.isZeroDepPlanName = false;
    this.isRSAPlan = false;
    this.quoteDetails.IsRTIApplicableflag = false;
    this.quoteDetails.IsConsumables = false;
  }

  validateRTO() {
    this.rtoinputvalue = this.rtoList.find(
      (c) => c.rtO_LOCATION_DESC == this.quoteDetails.rtO_LOCATION_DESC
    );
    if (this.rtoinputvalue == undefined) {
      swal({ text: "Select correct RTO location" }).then(() => {
        document.getElementById("rtodetail").focus();
      });
      this.cs.loaderStatus = false;
    }
  }

  // Find data for quote service
  finddataforquote(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.loaderStatus = true;
      // RTO Code
      console.log(this.quoteDetails.rtO_LOCATION_DESC, this.rtoDetail);
      if (
        this.RTOname != "" &&
        this.rtoName == undefined &&
        this.cs.isUndefineORNull(this.quoteDetails.rtO_LOCATION_DESC)
      ) {
        if (this.rtoList == undefined || this.rtoList == null) {
          swal({ text: "Select correct RTO location" });
        } else {
          this.rtoinputvalue = this.rtoList.find(
            (c) => c.rtO_LOCATION_DESC == this.quoteDetails.rtO_LOCATION_DESC
          );
        }
        if (this.rtoinputvalue == undefined) {
          // swal({
          //   text: 'Select correct RTO location'
          // }).then(()=> { document.getElementById('rtodetail').focus();});
          this.cs.loaderStatus = false;
        }
        let RTOCode = this.rtoList.find(
          (c) => c.rtO_LOCATION_DESC == this.quoteDetails.rtO_LOCATION_DESC
        ).rtO_LOCATION_CODE;
        this.rtoCode = RTOCode;
      } else if (
        this.rtoName == undefined &&
        this.cs.isUndefineORNull(this.quoteDetails.rtO_LOCATION_DESC)
      ) {
        this.rtoCode = this.rtoDetail.rto_cd;
      }
      // Code for modelname desc make code modelcode
      if (
        (this.rtoDetail == "" ||
          this.rtoDetail == null ||
          this.rtoDetail == undefined) &&
        !this.modelName
      ) {
        if (
          this.manumod == undefined ||
          this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ) == undefined
        ) {
          swal({ text: "Select Make & Model from drop down list" });
        } else {
          this.modelName = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).manufacturer;
          this.modeldesc = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).model_desc;
          this.name = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).name;
          this.modelcode = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).vehiclemodelcode;
          this.makecode = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).vehiclemanufacturecode;
          this.vehicledesc = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).model_desc;
          this.cubiccapacity = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).cubiccapacity;
          this.seatingcapacity = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).seatingcapacity;
        }
      } else if (this.manumod != undefined)
        if (
          this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ) == undefined
        ) {
          // if(this.manumod.length == 0){
          //   this.showIDV = false;
          // } else
          // {
          this.cs.loaderStatus = false;
        } else {
          this.modelName = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).manufacturer;
          this.modeldesc = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).model_desc;
          this.name = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).name;
          this.modelcode = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).vehiclemodelcode;
          this.makecode = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).vehiclemanufacturecode;
          this.vehicledesc = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).model_desc;
          this.cubiccapacity = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).cubiccapacity;
          this.seatingcapacity = this.manumod.find(
            (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
          ).seatingcapacity;
        }
      // }

      resolve();
      this.cs.loaderStatus = false;
    });
  }

  // For Other Radio Button
  getlist(ev: any) {
    this.manuflag = false;
    this.radioValue = undefined;
  }

  // ManuFacturer Radio name
  getManuradio(ev: any) {
    this.radioValue = ev.target.value;
    this.getManuModList(this.radioValue);
  }

  onChange(ev: any) {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.showIDV = false;
    this.systemQC = false;
    // this.panelOpenStateProposal = false;

    if (
      this.quoteDetails.RegistrationNumber != "" &&
      this.quoteDetails.rtO_LOCATION_DESC != "" &&
      this.quoteDetails.vehiclemodel != "" &&
      this.quoteDetails.FirstRegistrationDate != ""
    ) {
      setTimeout(() => {
        this.finddataforquote().then(() => {
          this.showIDV = true;
          this.findexshowroom();
        });
      }, 1000);
    } else {
      this.showIDV = false;
    }
  }

  // // Manufacturer-Model For Production
  // getManuModList(ev: any) {
  //   if (ev.target.value.length < 3 || ev.key == "Backspace") {
  //     console.log("less value");
  //   } else {
  //     this.manuname = ev.target.value;
  //     this.cs
  //       .get(
  //         "ManufactureNModel/GetManufactureNModelList?manufactureName=" +
  //           this.manuname +
  //           "&classCode=" +
  //           this.classCode
  //       )
  //       .subscribe((res: any) => {
  //         this.manumod = res;
  //         localStorage.removeItem("rtoDetail");
  //         this.datalistname = ev.target.value;

  //         if (res.length == 0) {
  //           this.manuerror = true;
  //         } else {
  //           this.manuerror = false;
  //         }
  //       });
  //   }
  // }

  /**
   * ManufactureNModel/GetManufactureNModelList api commented and Manufacture Model List is being fetched from localStorage
   * Author :- Sumit
   * date :- 22-01-2022
   */
  // For Sanity
  backup_manumod_list: any = [];
  getManuModList(ev: any) {
    if (this.productCode == "2312" || this.productCode == "2320") {
      this.classCode = "37";
    } else if (this.productCode == "2311" || this.productCode == "2319") {
      this.classCode = "45";
    }

    if (ev.target.value.length < 3 || ev.key == "Backspace") {
      this.manumod = [];
    } else {
      this.manuname = ev.target.value.toLowerCase();
      // 15/03/2022
      // if (localStorage.getItem("rtoDetail")) {
      //   localStorage.removeItem("rtoDetail");
      // }
      this.datalistname = ev.target.value;

      if (
        this.ManufactureModelList != undefined &&
        this.ManufactureModelList != null
      ) {
        if (this.ManufactureModelList.length != 0) {
          this.ManufactureModelList = this.api.manufactureListNew(
            this.productCode
          );
        } else {
          this.ManufactureModelList = this.api.manufactureListNew(
            this.productCode
          );
        }
      } else {
        this.ManufactureModelList = this.api.manufactureListNew(
          this.productCode
        );
      }

      if (ev.target.value.length <= 3) {
        this.manumod = this.ManufactureModelList.filter((option) => {
          return (
            option.vehiclemodel
              .substring(0, 3)
              .toLowerCase()
              .includes(this.manuname) &&
            option.vehicleclasscode == this.classCode
          );
        });
      } else {
        this.manumod = this.ManufactureModelList.filter((option) => {
          return (
            option.vehiclemodel.toLowerCase().includes(this.manuname) &&
            option.vehicleclasscode == this.classCode
          );
        });
      }

      if (this.manumod.length == 0) {
        this.manumod = this.ManufactureModelList.filter((option) => {
          return (
            option.vehiclemodel.toLowerCase().includes(this.manuname) &&
            option.vehicleclasscode == this.classCode
          );
        });
      }

      if (this.manumod.length == 0) {
        this.manuerror = true;
      } else {
        this.manuerror = false;
      }
    }
  }

  // Previous Insurer Name
  getInsurerName() {
    this.insurerlist = this.cs.PreviousInsurerList;;
  }

  getInsurerName1(ev: any) {

    if (ev.key == "Backspace") {
      this.insurerlist = this.cs.PreviousInsurerList;
    } else {
      let PreviousInsurerList = this.cs.PreviousInsurerList;
      const filterValue = ev.target.value.toLowerCase();
      this.insurerlist = PreviousInsurerList.filter((option) => {
        return (
          option.companyCode.toLowerCase().includes(filterValue) ||
          option.companyName.toLowerCase().includes(filterValue)
        )
      });
    }
  }

  // NCB Percent Value
  ncbPerc(ev: any) {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.ncbValue = ev.target.value;
  }

  // State Data
  // getStateData() {
  //   this.cs.get("State/GetStateList").subscribe((res: any) => {
  //     this.stateMaster = res;
  //   });
  // }

  getStateData() {
    /**
     * Author :- Sumit
     * date :- 12-01-2021
     * added the condition for calling an State/GetStateList api
     */
    this.stateMaster = this.cs.StateListData;
    // if (localStorage.getItem("StateList")) {
    //   let demo = localStorage.getItem("StateList");
    //   let decryptedStateList = this.api.decrypt(demo);
    //   this.stateMaster = JSON.parse(decryptedStateList);
    // } else {
    //   this.cs.get("State/GetStateList").subscribe((res: any) => {
    //     this.stateMaster = res;
    //     let encryptedStateList = this.api.encrypt(res);
    //     localStorage.setItem("StateList", encryptedStateList);
    //   });
    // }
  }

  // Number only valid
  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    console.log(event.target.value.length);
    let length = event.target.value.length;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  numberOnlyRange(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && event.length > 8 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  // Tab change event
  tabChange(ev: any) {
    this.clearquoteform();
    /**
     * digitalPOS change
     * call getDeal function only for nysa not for POS
     * date :- 29-07-2021
     */

    if (this.IsPOSTransaction_flag == true) {
    } else {
      // GetDealFromIM? changes - Sejal - 21-01-2022
      if (this.cs.isUndefineORNull(localStorage.getItem("DealID"))) {
        this.getDeal();
      }
    }

    if (ev == "Pending Payment") {
      this.nysaPolicydata = true;
    } else {
      this.nysaPolicydata = false;
    }
    if (ev != "Nysa Policies") {
      localStorage.removeItem("myNysaPolicy");
    }
  }

  // Check registeration number
  regCheck(ev: any) {
    let regTest = /^[a-zA-Z0-9]+$/.test(ev.target.value);
    if (regTest) {
      this.isRegNumberValid = true;
    } else {
      // swal({ text: "Kindly provide correct vehicle registration number"});
      this.isRegNumberValid = false;
      this.quoteDetails.RegistrationNumber = "";
    }
  }

  validatereg(ev: any) {
    let value = ev.target.value;
    let match = "NEW";
    let maxlength = 5;
    let regTest = /^[0-9]+$/.test(ev.target.value);
    let regTest1 = /^[a-zA-Z]+$/.test(ev.target.value);
    let str = /^[0-9]+$/.test(ev.target.value.slice(0, 1));
    let str1 = /^[0-9]+$/.test(ev.target.value.slice(1, 2));
    if (str || str1) {
      this.twoletteralpha = true;
      this.isRegNumberValid = false;
      this.isRegnocorrect = false;
    } else if (value.length < maxlength && value != match) {
      this.isRegNumberValid = false;
      this.isRegnocorrect = false;
      this.twoletteralpha = false;
      //swal({  text: "Kindly provide correct vehicle registration number" }).then(()=> { document.getElementById('txtreg').focus(); });
    } else if (value.length > 2 && (regTest || regTest1)) {
      // this.isRegNumberTextValid = false;
      this.isRegnocorrect = true;
      this.isRegNumberValid = true;
      this.twoletteralpha = false;
      // swal({ text: "Enter registration number with combination of alphabets and numbers" }).then(() => { document.getElementById('txtreg').focus(); });
    } else {
      // this.isRegNumberTextValid = true;
      this.isRegNumberValid = true;
      this.isRegnocorrect = false;
      this.twoletteralpha = false;
    }
  }

  // TP Additional Covers
  createBizToken(): Promise<any> {
    return new Promise((resolve: any) => {

      this.cs.get("middleware/token?Scope=2").subscribe((res: any) => {
        localStorage.setItem("bizToken", JSON.stringify(res));
        this.showQuotebutton = false;
        resolve();
      });

      this.cs.get("middleware/token?Scope=14").subscribe((res: any) => {
        localStorage.setItem("SCPAToken", JSON.stringify(res));
        this.showQuotebutton = false;
        resolve();
      });

      // this.cs.getCrawler("/Tokens").subscribe((res: any) => {
      //   localStorage.setItem("bizTokenCrawler", JSON.stringify(res));
      //   this.showQuotebutton = false;
      //   resolve();
      // })

      this.cs.get("middleware/token?Scope=15").subscribe((res: any) => {
        localStorage.setItem("bizzTalkToken", JSON.stringify(res));
        this.showQuotebutton = false;
        resolve();
      });

      // tp crawler change by monika  
      if (this.isCrawlerEnable == true) {
        this.cs.getCrawlerNewToken("/api/v1/token").then((res: any) => {
          localStorage.setItem("newCrawlerToken", JSON.stringify(res));
          this.showQuotebutton = false;
          resolve();
        });
      }

    }).catch((err: any) => {
      // swal({ text: err });
      let errMsg = this.msgService.errorMsg(err)
      swal({ closeOnClickOutside: false, text: errMsg });
      this.cs.loaderStatus = false;
    });
  }

  createAddonBizToken(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.get("middleware/token?Scope=5").subscribe((res: any) => {
        localStorage.setItem("addOnbizToken", JSON.stringify(res));
        resolve();
      });
    }).catch((err: any) => {
      // swal({ text: err });
      let errMsg = this.msgService.errorMsg(err)
      swal({ closeOnClickOutside: false, text: errMsg });
      this.cs.loaderStatus = false;
    });
  }

  createRSAAddonBizToken(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.get("middleware/token?Scope=4").subscribe((res: any) => {
        localStorage.setItem("addOnbizToken", JSON.stringify(res));
        resolve();
      });
    }).catch((err: any) => {
      // swal({ text: err });
      let errMsg = this.msgService.errorMsg(err)
      swal({ closeOnClickOutside: false, text: errMsg });
      this.cs.loaderStatus = false;
    });
  }

  createGarageCashAddonBizToken(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.get("middleware/token?Scope=3").subscribe((res: any) => {
        localStorage.setItem("addOnbizToken", JSON.stringify(res));
        resolve();
      });
    }).catch((err: any) => {
      // swal({ text: err });
      let errMsg = this.msgService.errorMsg(err)
      swal({ closeOnClickOutside: false, text: errMsg });
      this.cs.loaderStatus = false;
    });
  }

  getAdditionalCover(ev: any) {
    this.additionalcoverflag = ev.target.checked;
  }

  ncb(ev: any) {
    if (ev.target.checked == true) {
      this.showPrev = true;
      this.getInsurerName();
    } else if (ev.target.checked == false) {
      this.showPrev = false;
    }
  }

  editQuote() {
    this.showAddon = false;
    this.panelOpenStateProposal = false;
    this.showAddon1 = false;
    this.quickquote = true;
    this.systemQC = false;
  }

  SubmitQuote(ev: any) {
    if (this.calculatedQuote.breakingFlag) {
      if (this.cs.isUndefineORNull(this.subLocation)) {
        swal({
          closeOnClickOutside: false,
          text: "Kindly select sublocation before continue...",
        });
      } else {
        if (
          ev == "DD" &&
          this.cs.isUndefineORNull(this.quoteDetails.OtherDiscount) &&
          !this.isRecalculate1
        ) {
          swal({
            text: "Discount can't be blank or 0",
          });
          this.isRecalculate = false;
        } else {
          this.isRecalculate1 = this.isRecalculate;
          // localStorage.removeItem('calQuoteRes');
          if (this.isRecalculate) {
            if (this.productCode == "2312") {
              this.calculate2Wquote();
            } else if (this.productCode == "2311") {
              this.calculate4Wquote();
            } else {
              if (
                this.isCrawlerEnable &&
                (this.policyType == "NEW" || this.policyType == "ROLL")
              ) {
                // if(this.isCrawlerEnable && this.policyType == "NEW"){
                this.calculateCrawTP();
              } else {
                this.calculateTP();
              }
            }
            this.submitQuote = false;
            this.isRecalculate = false;
          } else {
            (<any>window).ga("send", "event", {
              eventCategory: "Customize Premium",
              eventLabel: this.product + "" + this.policyType,
              eventAction: "Customize Premium selected",
              eventValue: 10,
            });

            if (this.bFlag == true) {
              this.submitQuote = true;
              if (this.countaddon == 0) {
                // localStorage.setItem("calQuoteReq", JSON.stringify(this.premreq1));
                localStorage.setItem(
                  "calQuoteReq",
                  JSON.stringify(this.quoteRequest)
                );
                localStorage.setItem(
                  "calQuoteRes",
                  JSON.stringify(this.calculatedQuote)
                );
              } else {
                localStorage.setItem(
                  "calQuoteReq",
                  JSON.stringify(this.quoteRequest)
                );
                localStorage.setItem(
                  "calQuoteRes",
                  JSON.stringify(this.calculatedQuote)
                );
              }

              this.cs.loaderStatus = false;
              this.panelOpenStateProposal = true;
              // this.router.navigateByUrl('proposal');
              this.saveofflineQuote();
              if(this.isSCPARequired){
                this.saveofflineSCPAQuote();
              }
            } else {
              if (this.calculatedQuote == null || this.failedquote) {
                swal({
                  text: "Sorry could not fetch premium for addon cover selected. Please uncheck the cover, select again and recalculate premium",
                });
                this.isRecalculate = false;
              } else {
                this.submitQuote = true;
                if (this.countaddon == 0) {
                  // localStorage.setItem("calQuoteReq", JSON.stringify(this.premreq1));
                  localStorage.setItem(
                    "calQuoteReq",
                    JSON.stringify(this.quoteRequest)
                  );
                  localStorage.setItem(
                    "calQuoteRes",
                    JSON.stringify(this.calculatedQuote)
                  );
                } else {
                  localStorage.setItem(
                    "calQuoteReq",
                    JSON.stringify(this.quoteRequest)
                  );
                  localStorage.setItem(
                    "calQuoteRes",
                    JSON.stringify(this.calculatedQuote)
                  );
                }
                this.cs.loaderStatus = false;
                this.proposalPanelOpened = true;
                this.isexpanded = true;
                this.panelOpenStateProposal = true;
                // this.router.navigateByUrl('proposal');
                this.saveofflineQuote();
                if(this.isSCPARequired){
                  this.saveofflineSCPAQuote();
                }
              }
            }
          }
          // this.setStep(2);
          // this.cs.showProposal = true;
        }
      }

    } else {
      if (
        ev == "DD" &&
        this.cs.isUndefineORNull(this.quoteDetails.OtherDiscount) &&
        !this.isRecalculate1
      ) {
        swal({
          text: "Discount can't be blank or 0",
        });
        this.isRecalculate = false;
      } else {
        this.isRecalculate1 = this.isRecalculate;
        // localStorage.removeItem('calQuoteRes');
        if (this.isRecalculate) {
          if (this.productCode == "2312") {
            this.calculate2Wquote();
          } else if (this.productCode == "2311") {
            this.calculate4Wquote();
          } else {
            if (
              this.isCrawlerEnable &&
              (this.policyType == "NEW" || this.policyType == "ROLL")
            ) {
              // if(this.isCrawlerEnable && this.policyType == "NEW"){
              this.calculateCrawTP();
            } else {
              this.calculateTP();
            }
          }
          this.submitQuote = false;
          this.isRecalculate = false;
        } else {
          (<any>window).ga("send", "event", {
            eventCategory: "Customize Premium",
            eventLabel: this.product + "" + this.policyType,
            eventAction: "Customize Premium selected",
            eventValue: 10,
          });

          if (this.bFlag == true) {
            this.submitQuote = true;
            if (this.countaddon == 0) {
              // localStorage.setItem("calQuoteReq", JSON.stringify(this.premreq1));
              localStorage.setItem(
                "calQuoteReq",
                JSON.stringify(this.quoteRequest)
              );
              localStorage.setItem(
                "calQuoteRes",
                JSON.stringify(this.calculatedQuote)
              );
            } else {
              localStorage.setItem(
                "calQuoteReq",
                JSON.stringify(this.quoteRequest)
              );
              localStorage.setItem(
                "calQuoteRes",
                JSON.stringify(this.calculatedQuote)
              );
            }

            this.cs.loaderStatus = false;
            this.panelOpenStateProposal = true;
            // this.router.navigateByUrl('proposal');
            this.saveofflineQuote();
            if(this.isSCPARequired){
              this.saveofflineSCPAQuote();
            }
          } else {
            if (this.calculatedQuote == null || this.failedquote) {
              swal({
                text: "Sorry could not fetch premium for addon cover selected. Please uncheck the cover, select again and recalculate premium",
              });
              this.isRecalculate = false;
            } else {
              this.submitQuote = true;
              if (this.countaddon == 0) {
                // localStorage.setItem("calQuoteReq", JSON.stringify(this.premreq1));
                localStorage.setItem(
                  "calQuoteReq",
                  JSON.stringify(this.quoteRequest)
                );
                localStorage.setItem(
                  "calQuoteRes",
                  JSON.stringify(this.calculatedQuote)
                );
              } else {
                localStorage.setItem(
                  "calQuoteReq",
                  JSON.stringify(this.quoteRequest)
                );
                localStorage.setItem(
                  "calQuoteRes",
                  JSON.stringify(this.calculatedQuote)
                );
              }
              this.cs.loaderStatus = false;
              this.proposalPanelOpened = true;
              this.isexpanded = true;
              this.panelOpenStateProposal = true;
              // this.router.navigateByUrl('proposal');
              this.saveofflineQuote();
              if(this.isSCPARequired){
                this.saveofflineSCPAQuote();
              }
            }
          }
        }
        // this.setStep(2);
        // this.cs.showProposal = true;
      }
    }

  }

  saveofflineQuote() {
    let body: any;
    let URL: any;
    let quoteReq: any;
    let propRes: any;
    this.quoteRequ = quoteReq = JSON.parse(localStorage.getItem("calQuoteReq"));
    this.showAddon = true;
    this.showPYP = quoteReq.isNoPrevInsurance;
    if (
      quoteReq.isNoPrevInsurance &&
      this.cs.isUndefineORNull(quoteReq.PreviousPolicyDetails)
    ) {
      quoteReq.PreviousPolicyDetails = {};
    } else {
      quoteReq.PreviousPolicyDetails = quoteReq.PreviousPolicyDetails;
    }
    // quoteReq.PreviousPolicyDetails = quoteReq.PreviousPolicyDetails;
    propRes = JSON.parse(localStorage.getItem("calQuoteRes"));
    // console.log(this.planName, this.premreq1, this.premreq2, this.premreq3, this.quoteDetails, this.quoteRequest, this.calculatedQuote);
    // let prevPolDetails = this.quoteRequest.PreviousPolicyDetails;
    // if(this.planName == 'plan1' && (this.premres1.finalPremium == this.calculatedQuote.finalPremium)){
    //   quoteReq = this.premreq1;
    //   quoteReq.PreviousPolicyDetails = prevPolDetails;
    //   propRes = this.premres1;
    // }else if(this.planName == 'plan2' && (this.premres2.finalPremium == this.calculatedQuote.finalPremium)){
    //   quoteReq = this.premreq2;
    //   quoteReq.PreviousPolicyDetails = prevPolDetails;
    //   propRes = this.premres2;
    // }else if(this.planName == 'plan3' && (this.premres3.finalPremium == this.calculatedQuote.finalPremium)){
    //   quoteReq = this.premreq3;
    //   quoteReq.PreviousPolicyDetails = prevPolDetails;
    //   propRes = this.premres3;
    // }else{
    //   quoteReq = JSON.parse(localStorage.getItem("calQuoteReq"));
    //   quoteReq.PreviousPolicyDetails = prevPolDetails;
    //   propRes = JSON.parse(localStorage.getItem("calQuoteRes"));
    // }
    // if(typeof(quoteReq.OtherDiscount) == 'number'){
    //   quoteReq.OtherDiscount = JSON.stringify(quoteReq.OtherDiscount)
    // }

    let sortedRes;
    if (!this.cs.isUndefineORNull(propRes.deviationMessage)) {
      sortedRes = propRes.deviationMessage.replace(/%/g, "");
    } else {
      sortedRes = "";
    }
    propRes.deviationMessage = sortedRes;
    quoteReq.OtherDiscount = quoteReq.OtherDiscount == 0 ? "0" : quoteReq.OtherDiscount;
    if(!this.cs.isUndefineORNull(quoteReq)){
    quoteReq.siHaveElectricalAccessories = typeof (quoteReq.siHaveElectricalAccessories) == 'number' ? JSON.stringify(quoteReq.siHaveElectricalAccessories) : quoteReq.siHaveElectricalAccessories;
    quoteReq.siHaveNonElectricalAccessories = typeof (quoteReq.siHaveNonElectricalAccessories) == 'number' ? JSON.stringify(quoteReq.siHaveNonElectricalAccessories) : quoteReq.siHaveNonElectricalAccessories;
    quoteReq.rtoLocationCode = typeof (quoteReq.rtoLocationCode) == 'number' ? JSON.stringify(quoteReq.rtoLocationCode) : quoteReq.rtoLocationCode;
    quoteReq.manufacturingYear = typeof (quoteReq.manufacturingYear) == 'number' ? JSON.stringify(quoteReq.manufacturingYear) : quoteReq.manufacturingYear;
    }
    body = {
      quoteRQ: quoteReq,
      quoteRS: propRes,
      IsDigitalPOS: this.IsPOSTransaction_flag, // Added digitalPOS parameter with value true or false based on value in localStorage
      BundleID: this.saodBundleID
    };

    if (
      this.paramDataValues.iPartnerLogin.isSubagent != null ||
      this.paramDataValues.iPartnerLogin.isSubagent != undefined
    ) {
      body.IsSubagent = true;
      body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
    }

    if (this.showsaod == true) {
      body.IsStandaloneOD = this.showsaod;
      let standaloneODRQ = {
        TPStartDate: this.quoteDetails.TPStartDate,
        TPEndDate: this.quoteDetails.TPEndDate,
        TPPolicyNo: this.quoteDetails.TPPolicyNo,
        TPInsurerName: this.quoteDetails.TPInsurerName,
      };
      body.standaloneODRQ = standaloneODRQ;
    }

    if (this.productCode == "2312") {
      URL = "Quote/tw/SaveQuote";
    }
    if (this.productCode == "2311") {
      URL = "Quote/fw/SaveQuote";
    }
    if (this.productCode == "2320") {
      URL = "Quote/twtp/SaveQuote";
    }
    if (this.productCode == "2319") {
      URL = "Quote/fwtp/SaveQuote";
    }
    let str = JSON.stringify(body);
    this.cs.post(URL, str).then((res: any) => {
      this.saveQuote = res;
      console.log(this.isRecalculate1, this.isRecalculate);
      if (this.showAddon1 && !this.submitQuote) {
        this.setStep(2);
      } else {
        this.setStep(3);
      }
    });
  }

  savecustomizequote() {
    return new Promise((resolve: any) => {
      let body: any;
      let URL: any;
      let propRes: any;
      let quoteReq: any;
      // console.log(this.premres1.finalPremium, this.calculatedQuote.finalPremium);
      this.quoteRequ = quoteReq = JSON.parse(
        localStorage.getItem("calQuoteReq")
      );
      this.showAddon = true;
      this.showPYP = quoteReq.isNoPrevInsurance;
      if (
        quoteReq.isNoPrevInsurance &&
        this.cs.isUndefineORNull(quoteReq.PreviousPolicyDetails)
      ) {
        quoteReq.PreviousPolicyDetails = {};
      } else {
        quoteReq.PreviousPolicyDetails = quoteReq.PreviousPolicyDetails;
      }
      propRes = JSON.parse(localStorage.getItem("calQuoteRes"));
      if(!this.cs.isUndefineORNull(quoteReq)){
        quoteReq.siHaveElectricalAccessories = typeof (quoteReq.siHaveElectricalAccessories) == 'number' ? JSON.stringify(quoteReq.siHaveElectricalAccessories) : quoteReq.siHaveElectricalAccessories;
        quoteReq.siHaveNonElectricalAccessories = typeof (quoteReq.siHaveNonElectricalAccessories) == 'number' ? JSON.stringify(quoteReq.siHaveNonElectricalAccessories) : quoteReq.siHaveNonElectricalAccessories;
        quoteReq.rtoLocationCode = typeof (quoteReq.rtoLocationCode) == 'number' ? JSON.stringify(quoteReq.rtoLocationCode) : quoteReq.rtoLocationCode;
        quoteReq.manufacturingYear = typeof (quoteReq.manufacturingYear) == 'number' ? JSON.stringify(quoteReq.manufacturingYear) : quoteReq.manufacturingYear;
      }
      
      body = {
        quoteRQ: quoteReq,
        quoteRS: propRes,
        IsDigitalPOS: this.IsPOSTransaction_flag, // Added digitalPOS parameter with value true or false based on value in localStorage
      };

      if (
        this.paramDataValues.iPartnerLogin.isSubagent != null ||
        this.paramDataValues.iPartnerLogin.isSubagent != undefined
      ) {
        body.IsSubagent = true;
        body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
      }

      if (this.showsaod == true) {
        body.IsStandaloneOD = this.showsaod;
        let standaloneODRQ = {
          TPStartDate: this.quoteDetails.TPStartDate,
          TPEndDate: this.quoteDetails.TPEndDate,
          TPPolicyNo: this.quoteDetails.TPPolicyNo,
          TPInsurerName: this.quoteDetails.TPInsurerName,
        };
        body.standaloneODRQ = standaloneODRQ;
      }

      if (this.productCode == "2312") {
        URL = "Quote/tw/SaveQuote";
      }
      if (this.productCode == "2311") {
        URL = "Quote/fw/SaveQuote";
      }
      if (this.productCode == "2320") {
        URL = "Quote/twtp/SaveQuote";
      }
      if (this.productCode == "2319") {
        URL = "Quote/fwtp/SaveQuote";
      }
      let str = JSON.stringify(body);
      this.cs.post(URL, str).then((res: any) => {
        this.saveQuote = res;
        console.log(this.isRecalculate1, this.isRecalculate);
        // if(this.showAddon1 && !this.submitQuote){
        //   this.setStep(2);
        // }else{
        //   this.setStep(3);
        // }
        resolve();
      });
    });
  }

  saveofflineQuot3prem() {
    let body, URL;
    let propRes: any;
    let quoteReq: any;
    let data = document.getElementById("premium_breakup") as HTMLInputElement;
    console.log(data);
    this.quoteRequ = quoteReq = JSON.parse(localStorage.getItem("calQuoteReq"));
    this.showAddon = true;
    this.showPYP = quoteReq.isNoPrevInsurance;
    if (
      quoteReq.isNoPrevInsurance &&
      this.cs.isUndefineORNull(quoteReq.PreviousPolicyDetails)
    ) {
      quoteReq.PreviousPolicyDetails = {};
    } else {
      quoteReq.PreviousPolicyDetails = quoteReq.PreviousPolicyDetails;
    }
    propRes = JSON.parse(localStorage.getItem("calQuoteRes"));
    if (typeof this.quoteRequ.OtherDiscount == "number") {
      this.quoteRequ.OtherDiscount = JSON.stringify(
        this.quoteRequ.OtherDiscount
      );
    }

    if(!this.cs.isUndefineORNull(this.quoteReq)){
      if(!this.cs.isUndefineORNull(this.quoteReq.siHaveElectricalAccessories)){
        this.quoteReq.siHaveElectricalAccessories = typeof (this.quoteReq.siHaveElectricalAccessories) == 'number' ? JSON.stringify(this.quoteReq.siHaveElectricalAccessories) : this.quoteReq.siHaveElectricalAccessories;
      }
      if(!this.cs.isUndefineORNull(this.quoteReq.siHaveNonElectricalAccessories)){
        this.quoteReq.siHaveNonElectricalAccessories = typeof (this.quoteReq.siHaveNonElectricalAccessories) == 'number' ? JSON.stringify(quoteReq.siHaveNonElectricalAccessories) : this.quoteReq.siHaveNonElectricalAccessories;
      }
      if(!this.cs.isUndefineORNull(this.quoteReq.rtoLocationCode)){
        this.quoteReq.rtoLocationCode = typeof (this.quoteReq.rtoLocationCode) == 'number' ? JSON.stringify(this.quoteReq.rtoLocationCode) : this.quoteReq.rtoLocationCode;
      }
      if(!this.cs.isUndefineORNull(this.quoteReq.manufacturingYear)){
        this.quoteReq.manufacturingYear = typeof (this.quoteReq.manufacturingYear) == 'number' ? JSON.stringify(this.quoteReq.manufacturingYear) : this.quoteReq.manufacturingYear;
      } 
    }
    // this.quoteReq.siHaveElectricalAccessories = typeof (this.quoteReq.siHaveElectricalAccessories) == 'number' ? JSON.stringify(this.quoteReq.siHaveElectricalAccessories) : this.quoteReq.siHaveElectricalAccessories;
    // this.quoteReq.siHaveNonElectricalAccessories = typeof (this.quoteReq.siHaveNonElectricalAccessories) == 'number' ? JSON.stringify(quoteReq.siHaveNonElectricalAccessories) : this.quoteReq.siHaveNonElectricalAccessories;
    // this.quoteReq.rtoLocationCode = typeof (this.quoteReq.rtoLocationCode) == 'number' ? JSON.stringify(this.quoteReq.rtoLocationCode) : this.quoteReq.rtoLocationCode;
    // this.quoteReq.manufacturingYear = typeof (this.quoteReq.manufacturingYear) == 'number' ? JSON.stringify(this.quoteReq.manufacturingYear) : this.quoteReq.manufacturingYear;
    body = {
      quoteRQ: this.quoteRequ,
      quoteRS: this.quoteResp,
      IsDigitalPOS: this.IsPOSTransaction_flag, // Added digitalPOS parameter with value true or false based on value in localStorage
    };
    if (
      this.paramDataValues.iPartnerLogin.isSubagent != null ||
      this.paramDataValues.iPartnerLogin.isSubagent != undefined
    ) {
      body.IsSubagent = true;
      body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
    }
    if (this.showsaod == true) {
      body.IsStandaloneOD = this.showsaod;
      let standaloneODRQ = {
        TPStartDate: this.quoteDetails.TPStartDate,
        TPEndDate: this.quoteDetails.TPEndDate,
        TPPolicyNo: this.quoteDetails.TPPolicyNo,
        TPInsurerName: this.quoteDetails.TPInsurerName,
      };
      body.standaloneODRQ = standaloneODRQ;
    }
    if (this.productCode == "2312") {
      URL = "Quote/tw/SaveQuote";
    }
    if (this.productCode == "2311") {
      URL = "Quote/fw/SaveQuote";
    }
    if (this.productCode == "2320") {
      URL = "Quote/twtp/SaveQuote";
    }
    if (this.productCode == "2319") {
      URL = "Quote/fwtp/SaveQuote";
    }
    let str = JSON.stringify(body);
    this.cs.post(URL, str).then((res: any) => {
      this.saveQuote = res;
      // if(this.showAddon1 && !this.submitQuote){
      //   this.setStep(2);
      // }else{
      // this.setStep(2);
      // this.panelOpenStateProposal = false;
      // }
    });
  }

  tppd(ev: any) {
    if (ev.target.checked == true) {
      this.TPPDLimit = 6000;
    } else if (ev.target.checked == false) {
      if (this.productCode == "2311" || this.productCode == "2319") {
        this.TPPDLimit = 750000;
      } else if (this.productCode == "2312" || this.productCode == "2320") {
        this.TPPDLimit = 100000;
      }
    }
  }

  recalculateQuote() {
    this.submitQuote = false;
    if (
      this.isCrawlerEnable &&
      (this.policyType == "NEW" || this.policyType == "ROLL")
    ) {
      // if(this.isCrawlerEnable && this.policyType == "NEW"){
      this.calculateCrawTP();
    } else {
      this.calculateTP();
    }
  }

  premiumBasedCoverClick(ev: any) {
    if (ev.target.id == "noValidLicense") {
      this.disabled1 = true;
      this.disabled = false;
    } else {
      this.disabled = true;
      this.disabled1 = false;
    }

    if (ev.target.checked == true) {
      this.cpaTenure = 0;
    } else {
      this.cpaTenure = 1;
      this.disabled1 = false;
      this.disabled = false;
    }
  }

  premiumBasedCoverClickPackage(ev: any) {
    if (ev.target.id == "noValidLicense") {
      this.disabled1 = true;
      this.disabled = false;
    } else {
      this.disabled = true;
      this.disabled1 = false;
    }
  }

  premiumBasedCoverClickEV(ev: any) {
    if (ev.target.checked) {
      if (ev.target.id == "scpaCover") {
        this.isSCPA = true;
        this.calculateSCPA();
        localStorage.setItem("IsSCPA", this.isSCPA.toString());
      }
      else {
        this.isSCPA = false;
        this.totalPremium = this.evTotalPremium;
        this.totalGST = this.evGST;
        localStorage.setItem("IsSCPA", this.isSCPA.toString());
      }
    } else {
      this.isSCPA = false;
      this.totalPremium = this.evTotalPremium;
      this.totalGST = this.evGST;
      localStorage.setItem("IsSCPA", this.isSCPA.toString());
    }
  }

  //======================addon services==============//
  getaddOnsData(coverName, vehicle) {
    this.cs
      .getAddons(
        "MotorMaster/GetCovers?CoverName=" +
        coverName +
        "&ProductName=" +
        vehicle
      )
      .then((res: any) => {
        switch (coverName) {
          case "zerodepreciation":
            this.zeroDepPlans = res;
            break;
          case "roadsideassistance":
            this.rsaPlans = res;
            //  this.rsa = this.rsaPlans.find(c => c.statE_NAME == this.quoteDetails.GSTToState).statE_ID;
            break;
          case "ncbprotection":
            this.ncbProtectPlans = res;
            break;
          case "garagecash":
            this.garageCashPlans = res;
            break;
          case "keyprotect":
            this.keyProtectPlans = res;
            break;
          case "lossofpersonal":
            this.lossOfPersonalBelongingPlans = res;
            break;
          default:
            break;
        }
      });
  }

  getZeroDepData() {
    this.createAddonBizToken().then(() => {
      this.cs
        .getBizAdd(
          "/pfmaster/getzerodepreciationdetails?productcode=" + this.productCode
        )
        .then((res: any) => {
          this.zeroDepPlans = res.zeroDepreciation;
        });
    });
  }

  getRSAData() {
    this.createRSAAddonBizToken().then(() => {
      this.cs
        .getBizAdd(
          "/pfmaster/getroadsideassistance?productcode=" + this.productCode
        )
        .then((res: any) => {
          this.rsaPlans = res.roadSideAssistance;
          this.rsaplanname = Array.from(
            new Set(this.rsaPlans.map((x) => x.txT_PLAN_NAME))
          );
        });
    });
  }

  getgarageCashData() {
    this.createGarageCashAddonBizToken().then(() => {
      this.cs
        .getBizAdd1("/pfmaster/fetchmotorgaragecashdetails")
        .then((res: any) => {
          this.garageCashPlans = res.motorGarageCashDetails;
        });
    });
  }

  getKeyProtectData() {
    this.createGarageCashAddonBizToken().then(() => {
      this.cs
        .getBizAdd1("/pfmaster/fetchmotorkeyprotectdetails")
        .then((res: any) => {
          this.keyProtectPlans = res.motorKeyProtectDetails;
        });
    });
  }

  getlossofPerData() {
    this.createGarageCashAddonBizToken().then(() => {
      this.cs
        .getBizAdd1("/pfmaster/fetchmotorpersonalbelongdetails")
        .then((res: any) => {
          this.lossOfPersonalBelongingPlans = res.motorPersonalBelongModel;
        });
    });
  }

  unnamedPassenger(ev: any) {
    if (ev.target.checked == true) {
      this.showValues = true;
    } else {
      this.showValues = false;
    }
  }
  PaidDriver(ev: any) {
    if (ev.target.checked == true) {
      this.PaidDrivervalue = true;
    } else {
      this.PaidDrivervalue = false;
    }
  }

  PaidEmployee(ev: any) {
    if (ev.target.checked == true) {
      this.PaidEmployeevalue = true;
    } else {
      this.PaidEmployeevalue = false;
    }
  }

  accessories(ev: any) {
    if (ev.target.checked == true) {
      this.showaccessories = true;
    } else {
      this.showaccessories = false;
    }
  }

  voluntaryDeductableVal(vehicle) {
    this.cs
      .getAddons("MotorMaster/Voluntarydeductible?VehicleType=" + vehicle)
      .then((res: any) => {
        this.voluntaryDedData = res;
      });
  }

  saod(ev: any) {
    this.showsaod = ev;
    // let rtoData = JSON.parse(localStorage.getItem('rtoDetail'));
    console.log(this.quoteDetails, this.rtoDetail, this.cs.isUndefineORNull(this.quoteDetails.RegistrationNumber), this.calculatedQuote);
    if (
      // ev == true &&
      !this.cs.isUndefineORNull(this.quoteDetails.RegistrationNumber) &&
      !this.cs.isUndefineORNull(this.calculatedQuote)
    ) {
      let data = JSON.parse(localStorage.getItem('dataforBack'));
      this.openQuoteSection = true;
      // this.showsaod = true;
      this.isSCPARequired = true;
      this.quoteDetails.TPTenure = "0";
      this.quoteDetails.Tenure = "1";
      this.quoteDetails.PreviousPolicyType = "Bundled Package Policy";
      this.quoteDetails.PreviousPolicyNumber = data.PreviousPolicyNumber
    } else {
      this.openQuoteSection = false;
      // this.showsaod = false;
      this.isSCPARequired = false;
      this.quoteDetails.TPTenure = "1";
      this.quoteDetails.Tenure = "1";
      this.quoteDetails.PreviousPolicyType = "Comprehensive Package";
    }
  }

  getPlans(plan) {
    console.log(plan);
  }

  defaultPlan() {
    let clickplan = document.getElementById("prem1");
    clickplan.click();
  }

  // 3 Premium code
  getPlan(plan) {
    this.premiumloader = false;
    this.showAddon1 = false;
    this.planName = plan;
    this.premid1 = document.getElementById("prem1");
    this.premid2 = document.getElementById("prem2");
    this.premid3 = document.getElementById("prem3");
    // this.otherDiscount = 0;
    // this.quoteDetails.OtherDiscount = 0;
    this.isRecalculate1 = false;
    if (this.productCode == "2312" || this.productCode == "2311") {
      if (this.planName == "plan1") {
        this.premid1.classList.add("recomended");
        // this.premid2.classList.remove("recomended");
        // this.premid3.classList.remove("recomended");
        // this.calculatedQuote = this.premres1;
        // this.quoteResp = this.premres1;
        // this.quoteRequ = this.premreq1;
      } else if (this.planName == "plan2") {
        this.premid2.classList.add("recomended");
        this.premid1.classList.remove("recomended");
        this.premid3.classList.remove("recomended");
        this.calculatedQuote = this.premres2;
        this.quoteResp = this.premres2;
        this.quoteRequ = this.premreq2;
      } else {
        this.premid3.classList.add("recomended");
        this.premid1.classList.remove("recomended");
        this.premid2.classList.remove("recomended");
        this.calculatedQuote = this.premres3;
        this.quoteResp = this.premres3;
        this.quoteRequ = this.premreq3;
      }
    }
    if (this.productCode == "2320" || this.productCode == "2319") {
      if (this.planName == "plan1") {
        this.premid1.classList.add("recomended");
        // this.premid2.classList.remove("recomended");
        // this.quoteResp = this.premres1;
        // this.quoteRequ = this.premreq1;
      } else if (this.planName == "plan2") {
        this.premid2.classList.add("recomended");
        this.premid1.classList.remove("recomended");
        this.quoteResp = this.premres2;
        this.quoteRequ = this.premreq2;
      }
    }
  }

  PremiumCalculation() {
    if (this.totalPremiumTP == undefined) {
      this.totalPremiumTP = this.calculatedQuote.finalPremium;
      this.premres1 = this.calculatedQuote;
      this.premreq1 = this.quoteRequest;
      this.quoteResp = this.premres1;
      this.quoteRequ = this.premreq1;
      this.Premium2();
    } else if (this.totalPremiumTP2 == undefined) {
      this.totalPremiumTP2 = this.calculatedQuote.finalPremium;
      this.premres2 = this.calculatedQuote;
      this.premreq2 = this.quoteRequest;
      if (this.productCode == "2312" || this.productCode == "2311") {
        this.Premium3();
      }
    } else if (this.totalPremiumTP3 == undefined) {
      this.totalPremiumTP3 =
        this.calculatedQuote.finalPremium == 0
          ? "0"
          : this.calculatedQuote.finalPremium;
      console.log(
        "12345",
        this.calculatedQuote.finalPremium,
        this.cs.isUndefineORNull(this.totalPremiumTP3)
      );
      if (this.totalPremiumTP3 == this.totalPremiumTP2) {
        this.totalPremiumTP3 = this.totalPremiumTP2;
        let prem3 = document.getElementById("prem3") as HTMLInputElement;
        prem3.style.display = "none";
      }
      this.premres3 = this.calculatedQuote;
      this.premreq3 = this.quoteRequest;
    } else {
      this.totalPremium = this.calculatedQuote.finalPremium;
      console.log(this.quoteRequest);
      localStorage.setItem("calQuoteRes", JSON.stringify(this.calculatedQuote));
    }
  }

  Premium2() {
    if (this.productCode == "2312") {
      // if(this.vehicleAge < 5){
      //   this.isZeroDepPlanName = true
      //   this.quoteDetails.ZeroDepPlanName = commonData.ZD;
      // }
      this.isZeroDepPlanName = false;
      this.isRSAPlan = true;
      this.quoteDetails.RSAPlanName = "TW-299";
      // this.quoteDetails.IsRTIApplicableflag = false;
      this.calculate2Wquote();
    } else if (this.productCode == "2311") {
      // IsConsumables Changes
      this.quoteDetails.IsConsumables = false;
      if (this.vehicleAge < 5) {
        this.isZeroDepPlanName = true;
        this.quoteDetails.ZeroDepPlanName = commonData.fourZD;
      }
      this.isRSAPlan = true;
      this.quoteDetails.RSAPlanName = "RSA-Standard";
      this.quoteDetails.IsRTIApplicableflag = false;
      this.calculate4Wquote();
    } else if (this.productCode == "2320" || this.productCode == "2319") {
      this.PaidDrivervalue = true;
      this.quoteDetails.IsLegalLiabilityToPaidDriver = true;
      if (
        this.isCrawlerEnable &&
        (this.policyType == "NEW" || this.policyType == "ROLL")
      ) {
        // if(this.isCrawlerEnable && this.policyType == "NEW"){
        this.calculateCrawTP();
      } else {
        this.calculateTP();
      }
    }
  }

  Premium3() {
    if (this.productCode == "2312") {
      // this.quoteDetails.IsExtensionCountry = false;
      // this.quoteDetails.ExtensionCountryName = "Bhutan, Bangladesh, Nepal, Pakistan, Maldives, SriLanka";
      if (this.vehicleAge < 5) {
        this.isZeroDepPlanName = true;
        this.quoteDetails.ZeroDepPlanName = commonData.ZD;
      }
      this.isRSAPlan = true;
      this.quoteDetails.RSAPlanName = "TW-299";
      // this.quoteDetails.IsRTIApplicableflag = true;
      this.calculate2Wquote();
    } else if (this.productCode == "2311") {
      this.quoteDetails.IsExtensionCountry = false;
      this.quoteDetails.ExtensionCountryName = null;
      this.quoteDetails.IsConsumables = true;
      if (this.vehicleAge < 5) {
        this.isZeroDepPlanName = true;
        this.quoteDetails.ZeroDepPlanName = commonData.fourZD;
      }
      this.isRSAPlan = true;
      this.quoteDetails.RSAPlanName = "RSA-Standard";
      this.quoteDetails.IsRTIApplicableflag = false;
      this.calculate4Wquote();
    } else if (this.productCode == "2320" || this.productCode == "2319") {
      this.PaidDrivervalue = true;
      this.quoteDetails.IsLegalLiabilityToPaidDriver = true;
      this.quoteDetails.IsExtensionCountry = true;
      this.quoteDetails.ExtensionCountryName =
        "Bhutan, Bangladesh, Nepal, Pakistan, Maldives, SriLanka";
      // Sejal - 25-02-2022 - Commenting as getting error in 2WTP New CPA Tenure
      if (
        this.isCrawlerEnable &&
        (this.policyType == "NEW" || this.policyType == "ROLL")
      ) {
        // if(this.isCrawlerEnable && this.policyType == "NEW"){
        this.calculateCrawTP();
      } else {
        this.calculateTP();
        // this.createCommonQuote();
      }
    }
  }

  sendPlan(ev: any) {
    console.log(ev, this.quoteDetails.OtherDiscount);
    if (this.isOtherDiscDev) {
      if (this.productCode == "2312") {
        this.calculate2Wquote();
      } else {
        this.calculate4Wquote();
      }
    } else {
      if (this.calculatedQuote.breakingFlag) {
        if (this.cs.isUndefineORNull(this.subLocation)) {
          swal({
            closeOnClickOutside: false,
            text: "Kindly select sublocation before continue...",
          });
        } else {
          this.proposalPanelOpened = true;
          this.isexpanded = true;
          this.panelOpenStateProposal = true;
          if (this.productCode != commonData.allriskProductCode) {
            this.saveofflineQuote();
            if(this.isSCPARequired){
              this.saveofflineSCPAQuote();
            }
          } else {
            this.allriskSaveOfflineQuote();
            if (this.isSCPA == true && this.isValidSCPA == true) {
              this.scpaSaveOfflineQuote();
            }
          }
          if (this.showAddon1 && !this.submitQuote) {
            this.setStep(2);
          } else {
            this.setStep(3);
          }
        }
      } else {
        this.proposalPanelOpened = true;
        this.isexpanded = true;
        this.panelOpenStateProposal = true;
        if (this.productCode != commonData.allriskProductCode) {
          this.saveofflineQuote();
          if(this.isSCPARequired){
            this.saveofflineSCPAQuote();
          }
        } else {
          this.allriskSaveOfflineQuote();
          if (this.isSCPA == true && this.isValidSCPA == true) {
            this.scpaSaveOfflineQuote();
          }
        }
        if (this.showAddon1 && !this.submitQuote) {
          this.setStep(2);
        } else {
          this.setStep(3);
        }
      }

    }
    // if (ev == "basic") {
    //   localStorage.setItem("calQuoteRes", JSON.stringify(this.premres1));
    //   localStorage.setItem("calQuoteReq", JSON.stringify(this.premreq1));
    //   this.quoteservice.unsubscribe();
    //   this.cs.loaderStatus = false;
    //   (<any>window).ga("send", "event", {
    //     eventCategory: "Basic Premium",
    //     eventLabel: this.product + "" + this.policyType,
    //     eventAction: "Basic Premium selected",
    //     eventValue: 10,
    //   });
    //   // this.router.navigateByUrl('proposal');
    //   this.proposalPanelOpened = true;
    //   this.isexpanded = true;
    //   this.panelOpenStateProposal = true;
    //   this.saveofflineQuote();
    // } else if (ev == "recomended") {
    //   localStorage.setItem("calQuoteRes", JSON.stringify(this.premres2));
    //   localStorage.setItem("calQuoteReq", JSON.stringify(this.premreq2));
    //   this.quoteservice.unsubscribe();
    //   this.cs.loaderStatus = false;
    //   (<any>window).ga("send", "event", {
    //     eventCategory: "Recommended Premium",
    //     eventLabel: this.product + "" + this.policyType,
    //     eventAction: "Recomended Premium selected",
    //     eventValue: 10,
    //   });
    //   // this.router.navigateByUrl('proposal');
    //   this.panelOpenStateProposal = true;
    //   this.proposalPanelOpened = true;
    //   this.isexpanded = true;
    //   this.saveofflineQuote();
    // } else if (ev == "recomendedplus") {
    //   localStorage.setItem("calQuoteRes", JSON.stringify(this.premres3));
    //   localStorage.setItem("calQuoteReq", JSON.stringify(this.premreq3));
    //   this.cs.loaderStatus = false;
    //   (<any>window).ga("send", "event", {
    //     eventCategory: "Recomended Plus Premium",
    //     eventLabel: this.product + "" + this.policyType,
    //     eventAction: "Recomended Plus Premium selected",
    //     eventValue: 10,
    //   });
    //   this.panelOpenStateProposal = true;
    //   this.proposalPanelOpened = true;
    //   this.isexpanded = true;
    //   // this.router.navigateByUrl('proposal');
    //   this.saveofflineQuote();
    // }
    // this.setStep(2);
  }

  customize() {
    // console.log(JSON.parse(localStorage.getItem("calQuoteReq")));
    //Changes for one premium
    // if (this.totalPremiumTP && this.totalPremiumTP2) {
    if (this.totalPremiumTP) {
      if (this.isRecalculate1 || this.isRecalculate) {
        this.panelOpenState = false;
        this.panelOpenStatePremium = false;
        this.panelOpenStateProposal = false;
        this.panelOpenStateAddon = true;
        this.showAddon1 = true;
        this.totalPremium = this.quoteResp.finalPremium;
        this.saveofflineQuote();
        if(this.isSCPARequired){
          this.saveofflineSCPAQuote();
        }
        this.setStep(2);
      } else {
        this.panelOpenState = false;
        this.panelOpenStatePremium = false;
        this.panelOpenStateProposal = false;
        this.panelOpenStateAddon = true;
        this.countaddon = 0;
        console.log(this.quoteResp, this.calculatedQuote);
        this.calculatedQuote = this.quoteResp;
        console.log(this.quoteResp, this.calculatedQuote);
        this.totalPremium = this.quoteResp.finalPremium;
        // localStorage.setItem("calQuoteReq", JSON.stringify(this.premreq1));
        // localStorage.setItem("calQuoteRes", JSON.stringify(this.premres1));

        // if(this.planName == "plan1"){
        //   localStorage.setItem("calQuoteReq", JSON.stringify(this.premreq1));
        //   localStorage.setItem("calQuoteRes", JSON.stringify(this.premres1));
        //   this.calculatedQuote = this.premres1;
        //   this.totalPremium = this.premres1.finalPremium;
        //   }else if(this.planName == "plan2"){
        //     localStorage.setItem("calQuoteReq", JSON.stringify(this.premreq2));
        //   localStorage.setItem("calQuoteRes", JSON.stringify(this.premres2));
        //   this.calculatedQuote = this.premres2;
        //   this.totalPremium = this.premres2.finalPremium;
        //   }else{
        //   localStorage.setItem("calQuoteReq", JSON.stringify(this.premreq3));
        //   localStorage.setItem("calQuoteRes", JSON.stringify(this.premres3));
        //   this.calculatedQuote = this.premres3;
        //   this.totalPremium = this.premres3.finalPremium;
        //   }
        this.showAddon1 = true;
        this.saveofflineQuote();
        if(this.isSCPARequired){
          this.saveofflineSCPAQuote();
        }
        if (this.productCode == "2311" || this.productCode == "2312") {
          this.quoteDetails.IsExtensionCountry = false;
          this.isZeroDepPlanName = false;
          this.isRSAPlan = false;
          this.quoteDetails.IsRTIApplicableflag = false;
          this.quoteDetails.IsConsumables = false;
        } else if (this.productCode == "2319" || this.productCode == "2320") {
          this.quoteDetails.IsExtensionCountry = false;
          this.quoteDetails.ExtensionCountryName = null;
          this.PaidDrivervalue = false;
          this.quoteDetails.IsLegalLiabilityToPaidDriver = false;
        }
        // this.setStep(2)
      }
    } else {
      this.setStep(1);
      swal({
        closeOnClickOutside: false,
        text: "Kindly wait till we display your plans",
      });
    }
    // this.setStep(3);
  }

  plans(ev: any) {
    console.log(ev.target.id);
  }

  // PDF GEnerate
  //End of code

  capturescreen() {
    let DATA = document.getElementById("premium_breakup");
    html2canvas(DATA).then((canvas) => {
      const FILEURI = canvas.toDataURL("image/png");
      var imgWidth = 210;
      var pageHeight = 295;

      var imgHeight = (canvas.height * imgWidth) / canvas.width;
      var heightLeft = imgHeight;

      var doc = new jspdf("p", "mm");
      var position = 0;

      doc.addImage(FILEURI, "PNG", 0, position, imgWidth, imgHeight);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(FILEURI, "PNG", 0, position, imgWidth, imgHeight);
        heightLeft -= pageHeight;
      }

      // doc.save('Nysa.pdf');
    });
  }

  generateAndSendPDF() {
    // this.capturescreen();
    // this.exportToPdf();
    console.log(this.quoteRequest);
    let Data;
    if (this.productCode == commonData.allriskProductCode) {
      Data = JSON.parse(localStorage.getItem("EVQuoteReq"));
    } else {
      Data = JSON.parse(localStorage.getItem("calQuoteReq"));
    }
    if (this.productCode == "2312") {
      this.policySubType = "1";
    } else if (this.productCode == "2311") {
      this.policySubType = "2";
    } else if (this.productCode == "2319") {
      this.policySubType = "10";
    } else if (this.productCode == "2320") {
      this.policySubType = "9";
    } else if (this.productCode == commonData.allriskProductCode) {
      this.policySubType = "11";    // All Risk Electric Bike
    }
    // Logic to send this BTOA string to backend service
    if (
      this.sendDetails.mobileno == "" ||
      this.sendDetails.mobileno == undefined ||
      this.sendDetails.mobileno == null
    ) {
      this.cs.loaderStatus = false;
      swal({
        closeOnClickOutside: false,
        text: "Insert mobile number",
      });
    } else if (
      this.sendDetails.Email == "" ||
      this.sendDetails.Email == undefined ||
      this.sendDetails.Email == null
    ) {
      this.cs.loaderStatus = false;
      swal({
        closeOnClickOutside: false,
        text: "Insert Email Id",
      });
    } else {
      this.cs.loaderStatus = true;

      let DATA = document.getElementById("premium_breakup");
      html2canvas(DATA).then((canvas) => {
        const FILEURI = canvas.toDataURL("image/png");
        var imgWidth = 210;
        var pageHeight = 295;

        var imgHeight = (canvas.height * imgWidth) / canvas.width;
        var heightLeft = imgHeight;

        var doc = new jspdf("p", "mm", "a4", true);
        var position = 0;

        doc.addImage(
          FILEURI,
          "PNG",
          0,
          position,
          imgWidth,
          imgHeight,
          "",
          "FAST"
        );
        heightLeft -= pageHeight;

        while (heightLeft >= 0) {
          position = heightLeft - imgHeight;
          doc.addPage();
          doc.addImage(
            FILEURI,
            "PNG",
            0,
            position,
            imgWidth,
            imgHeight,
            "",
            "FAST"
          );
          heightLeft -= pageHeight;
        }
        // doc.save('Nysa.pdf');
        const pdfDoc = btoa(doc.output());
        let dataToSend
        dataToSend = {
          files: this.cs.encryptPDF(pdfDoc, commonData.aesnysaKey),
          PolicySubType: this.policySubType,
          EmailTo: this.cs.encrypt(
            this.sendDetails.Email,
            commonData.aesnysaKey
          ),
          MobileNo: this.cs.encrypt(
            this.sendDetails.mobileno,
            commonData.aesnysaKey
          ),
          //TotalPremium: JSON.stringify(this.calculatedQuote.finalPremium),
          VehicleRegNo: this.quoteDetails.RegistrationNumber,
          CorrelationID: Data.CorrelationId, //this.calculatedQuote.correlationId,
        };

        if (this.productCode == commonData.allriskProductCode) {
          dataToSend.TotalPremium = JSON.stringify(this.totalPremium);
          dataToSend.BundleID = this.bundleID;
          this.isPremiumBeakup = true;
        } else {
          dataToSend.TotalPremium = JSON.stringify(this.quoteResp.finalPremium);
        }
        this.cs
          .post("PolicySchedule/SendPremiumBreakUpPdf", dataToSend)
          .then((res: any) => {
            let msg;
            this.cs.loaderStatus = false;
            if (res.status == "Success" && res.is_Email_Sent) {
              msg = "Link has been sent successfully";
              $("#premium_breakup").modal("hide");
              $("#emiProt_modal").modal("hide");
              $("#premium_breakup").modal("show");
              if (this.isPremiumBeakup == true) {
                this.setStep(1);
              }
            } else {
              this.cs.loaderStatus = false;
              $("#emiProt_modal").modal("hide");
              msg = "Failed to send email";
            }
            this.cs.loaderStatus = false;
            swal({ closeOnClickOutside: false, text: msg });
          });

        // return doc;
        // doc.save('Nysa.pdf');
      });

      // var elem = document.getElementById('premiumBreakup');
      // var img = document.getElementById('lomblogo')
      // let doc = this.cs.convertToPdf(elem, img);
    }
  }

  sendCustLink() {
    let customerEmail, custMobile;
    customerEmail = this.sendDetails.Email;
    custMobile = this.sendDetails.mobileno;

    // $('#sendLinkModal').modal('show');
    this.sendCustPaymentLinkFlag = true;
    this.watchMe = "fly";
  }

  closeLink() {
    // this.paymentModeFlag = true;
    this.sendCustPaymentLinkFlag = false;
    this.watchMe = "fade";
  }

  mobileValidate() {
    let isNumberValidate = this.cs.mobileValidation(this.sendDetails.mobileno);
    if (isNumberValidate) {
      this.sendDetails.mobileno = this.sendDetails.mobileno;
      this.moberrorFlag = false;
    } else {
      this.sendDetails.mobileno = "";
      this.moberrorFlag = true;
      // swal({
      //   closeOnClickOutside: false,
      //   text: "Mobile number entered is not valid."
      // });
    }
  }

  generateAndSendPDFthree() {
    let Data = JSON.parse(localStorage.getItem("calQuoteReq"));
    if (this.productCode == "2312") {
      this.policySubType = "1";
    } else if (this.productCode == "2311") {
      this.policySubType = "2";
    } else if (this.productCode == "2319") {
      this.policySubType = "9";
    } else if (this.productCode == "2320") {
      this.policySubType = "10";
    }
    // var doc = new jsPDF('p', 'pt', 'a4');
    this.cs.loaderStatus = true;
    var elem = document.getElementById("premiumBreakup");
    var img = document.getElementById("lomblogo");
    let doc = this.cs.convertToPdf(elem, img);
    // doc.save("table.pdf");
    const pdfDoc = btoa(doc.output());
    let dataToSend = {
      files: pdfDoc,
      PolicySubType: this.policySubType,
      EmailTo: this.sendDetails.Email,
      MobileNo: this.sendDetails.mobileno,
      TotalPremium: JSON.stringify(this.quoteResp.finalPremium),
      VehicleRegNo: this.quoteDetails.RegistrationNumber,
      CorrelationID: Data.CorrelationId, //this.quoteResp.correlationId,
    };
    // Logic to send this BTOA string to backend service
    if (
      this.sendDetails.mobileno == "" ||
      this.sendDetails.mobileno == undefined ||
      this.sendDetails.mobileno == null
    ) {
      swal({
        closeOnClickOutside: false,
        text: "Insert mobile number",
      });
    } else if (
      this.sendDetails.Email == "" ||
      this.sendDetails.Email == undefined ||
      this.sendDetails.Email == null
    ) {
      swal({
        closeOnClickOutside: false,
        text: "Insert Email Id",
      });
    } else {
      this.cs
        .post("PolicySchedule/SendPremiumBreakUpPdf", dataToSend)
        .then((res: any) => {
          let msg;
          this.cs.loaderStatus = false;
          if (res.status == "Success" && res.is_Email_Sent) {
            msg = "Link has been sent successfully";
            $("#premium_breakup").modal("hide");
            $("#emiProt_modal").modal("hide");
            $("#premium_breakup").modal("show");
          } else {
            msg = "Failed to send email";
          }

          swal({ closeOnClickOutside: false, text: msg });
        });
    }
  }

  get getODSubTotal() {
    let total = 0;
    let count;
    if (this.productCode == "2312") {
      count = 8;
    } else if (this.productCode == "2311") {
      count = 14;
    }
    for (let i = 0; i < count; i++) {
      let data = String(document.getElementById(`odSub-${i + 1}`).innerHTML);
      total += Number(data.replace(/\D/g, ""));
    }
    return total;
  }

  get getTpTotal() {
    let total = 0;
    for (let i = 0; i < 3; i++) {
      let data = String(document.getElementById(`tpSub-${i + 1}`).innerHTML);
      total += Number(data.replace(/\D/g, ""));
    }
    return total;
  }

  get getliabTpTotal() {
    let total = 0;
    for (let i = 0; i < 4; i++) {
      let data = String(
        document.getElementById(`liabtpSub-${i + 1}`).innerHTML
      );
      total += Number(data.replace(/\D/g, ""));
    }
    return total;
  }

  // get getdiscountTotal() {
  //   let total = 0;
  //   for (let i = 0; i < 5; i++) {
  //     let data = String(document.getElementById(`disc-${i + 1}`).innerHTML);
  //     total += Number(data.replace(/\D/g, ""));
  //   }
  //   this.premiumWithoutDiscount = this.calculatedQuote.packagePremium + total;
  //   return total;
  // }

  otherdiscount(ev: any) {
    this.otherdisccheck = ev.target.checked;
    if (this.otherdisccheck == true) {
      swal({
        text: "As you have opted for discount, we are redirecting you to iPartner for completion of transaction, approvals , document upload & QC flow. To proceed click on ‘Yes’. To stay on Nysa and get instant policy and bypass QC, click on ‘No’",
        dangerMode: true,
        closeOnClickOutside: false,
        buttons: ["Yes", "No"],
      }).then((willDelete) => {
        if (willDelete) {
          this.otherdisccheck = false;
          console.log("Stay");
        } else {
          let type;
          if (this.policyType == "NEW") {
            type = "N";
          } else {
            type = "R";
          }
          this.cs.geToDashboardapproval(this.productCode, type);
        }
      });
    }
  }

  editmodel() {
    this.freezeManuModel = false;
    this.systemQC = false;
    $("#manufacturer").attr("disabled", false);
    this.quoteDetails.IsExtensionCountry = false;
    this.IsSelfInspection = false;
    this.isZeroDepPlanName = false;
    this.isRSAPlan = false;
    this.quoteDetails.IsRTIApplicableflag = false;
    this.quoteDetails.IsConsumables = false;
  }

  removeaddon() {
    this.quoteDetails.IsExtensionCountry = false;
    this.IsSelfInspection = false;
    this.isZeroDepPlanName = false;
    this.isRSAPlan = false;
    this.quoteDetails.IsRTIApplicableflag = false;
    this.quoteDetails.IsConsumables = false;
  }

  checkBreakINStatus(ev: any) {
    if (this.productCode == "2312") {
      this.policySubType = "1";
    } else if (this.productCode == "2311") {
      this.policySubType = "2";
    } else if (this.productCode == "2319") {
      this.policySubType = "10";
    } else if (this.productCode == "2320") {
      this.policySubType = "9";
    }
    this.cs
      .getURL(
        "BreakIn/GetPreApprovedBreakInStatus?breakinID=" +
        this.breakintype.breakINID +
        "&correlationID=5ffc4341-1453-4212-b795-9010969d6c1c"
      )
      .then((res: any) => {
        if (res.breakInStatus != "Recommended") {
          this.breakintype.breakINID = "";
        } else {
          if (res.policySubType != this.policySubType) {
            this.breakintype.breakINID = "";
          } else {
            localStorage.setItem("PreBreakINData", JSON.stringify(res));
          }
        }
      })
      .catch((err: any) => {
        console.log(err);
      });
  }

  // Discount/Loading
  adddiscountloading(ev: any) {
    if (ev.target.checked == true) {
      this.showdiscinput = true;
      this.quoteDetails.OtherDiscount = 0;
      localStorage.setItem('ODAmount', JSON.stringify(this.quoteDetails.OtherDiscount));
    } else if (ev.target.checked == false) {
      this.showdiscinput = false;
      this.quoteDetails.OtherDiscount = 0;
      localStorage.setItem('ODAmount', JSON.stringify(this.quoteDetails.OtherDiscount));
    }
  }

  disableaddonscheck() {
    this.quoteDetails.IsPACoverUnnamedPassenger = false;
    this.quoteDetails.IsLegalLiabilityToPaidDriver = false;
    this.quoteDetails.NoOfDriver = "0";
    this.quoteDetails.IsLegalLiabilityToPaidEmployee = false;
    this.quoteDetails.NoOfEmployee = "1";
    this.quoteDetails.IsVoluntaryDeductible = false;
    this.quoteDetails.isNCBProtect = false;
    this.quoteDetails.isGarageCash = false;
    this.quoteDetails.IsVehicleHaveLPG = false;
    this.quoteDetails.IsVehicleHaveCNG = false;
    this.radiocheck = false;

    this.PaidDrivervalue = false;
    this.PaidEmployeevalue = false;
    this.voluntaryDeductableValue = false;
    this.isncbProtect = false;
    this.isRSAPlan = false;
    this.isGarageCashCover = false;
    this.isZeroDepPlanName = false;
    this.IsAutomobileAssocnFlag = false;
    this.isLossOfPersonalBelonging = false;
  }

  showData() {
    this.panelOpenIIBGrid = true;
    // this.IIBData =  this.quoteDetails.claimDetails;
  }

  getIASData() {
    // Create IAS token on Raise Interaction - Sejal
    //GetIASList api call change - Sejal
    if (this.cs.isUndefineORNull(localStorage.getItem("iasToken"))) {
      this.cs.createIASToken();
    }
    let startDate = moment(new Date()).format("DD/MM/YYYY");
    localStorage.setItem("startDate", JSON.stringify(startDate));
    this.cs.get("IAS/GetIASList").subscribe((res: any) => {
      localStorage.setItem("IASData1", JSON.stringify(res));
    });
  }

  checkInsurerName(ev: any) {
    console.log('Hello 123', ev, this.quoteDetails.PreviousInsurerName);
  }

  setStep(index: number) {
    if (index == 2) {
      this.cs.showProposal = true;
      // this.checkAddON();
    } else {
      this.cs.showProposal = false;
    }
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  // checkAddON(){
  //   let req = JSON.parse(localStorage.getItem('calQuoteReq'));
  //   let keyProt = document.getElementById('keyProtect') as HTMLInputElement;
  //   let perAcc = document.getElementById('perAccedentCover') as HTMLInputElement;
  //   let noValidLie = document.getElementById('noValidLicense') as HTMLInputElement;
  //   let geo = document.getElementById('geographicalExt') as HTMLInputElement;
  //   let unNamePass = document.getElementById('UnnamedPassenger') as HTMLInputElement;
  //   let unNamePassPA = document.getElementById('paForUnnamedPassanger') as HTMLInputElement;
  //   let acce = document.getElementById('Accessories') as HTMLInputElement;
  //   let paidDriver = document.getElementById('paidDriver') as HTMLInputElement;
  //   let volDeduct = document.getElementById('voluntaryDeductable') as HTMLInputElement;
  //   let ncbProt = document.getElementById('NCBProtection') as HTMLInputElement;
  //   let rsaPlane = document.getElementById('RSAPlanName') as HTMLInputElement;
  //   let garageCash = document.getElementById('garageCashCover') as HTMLInputElement;
  //   let cunsu = document.getElementById('Consumables') as HTMLInputElement;
  //   let enginePro = document.getElementById('enginProtectPlus') as HTMLInputElement;
  //   let tyrePro = document.getElementById('tyreProtect') as HTMLInputElement;
  //   let emiProt = document.getElementById('emiProtect') as HTMLInputElement;
  //   let tppd = document.getElementById('tppd') as HTMLInputElement;
  //   let antithef = document.getElementById('antitheft') as HTMLInputElement;
  //   let rti = document.getElementById('returnOfInvoice') as HTMLInputElement;
  //   let lopb = document.getElementById('lossOfPersonalBelongings') as HTMLInputElement;
  //   let zd = document.getElementById('ZeroDepPlanName') as HTMLInputElement;
  //   let paidEmp = document.getElementById('paidEmployee') as HTMLInputElement;
  //   let paOwner = document.getElementById('paowner') as HTMLInputElement;

  //   if(req.IsConsumables){ cunsu.checked= true; } else { cunsu.checked= false; }
  //   if(req.IsEngineProtectPlus){ enginePro.checked= true; } else { enginePro.checked= false; }
  //   if(req.IsExtensionCountry){ geo.checked= true; } else { geo.checked= false; }
  //   if(req.IsLegalLiabilityToPaidDriver){ paidDriver.checked= true; } else { paidDriver.checked= false; }
  //   if(req.IsLegalLiabilityToPaidEmployee){ paidEmp.checked= true; } else { paidEmp.checked= false; }
  //   if(req.IsPACoverUnnamedPassenger){ paOwner.checked= true; } else { paOwner.checked= false; }
  //   if(req.IsRTIApplicableflag){ rti.checked= true; } else { rti.checked= false; }
  //   if(req.IsTyreProtect){ tyrePro.checked= true; } else { tyrePro.checked= false; }
  //   if(req.IsVoluntaryDeductible){ volDeduct.checked= true; } else { volDeduct.checked= false; }
  //   if(req.KeyProtectPlan){ keyProt.checked= true; } else { keyProt.checked= false; }
  //   if(req.RSAPlanName){ rsaPlane.checked= true; } else { rsaPlane.checked= false; }
  //   if(req.ZeroDepPlanName){ zd.checked= true; } else { zd.checked= false; }
  //   if(req.isGarageCash){ garageCash.checked= true; } else { garageCash.checked= false; }
  //   if(req.isNCBProtect){ ncbProt.checked= true; } else { ncbProt.checked= false; }

  // }

  // Sejal - 08-03-2022
  createCommonQuote() {
    //let button = document.getElementById("get_quote") as HTMLInputElement;
    //this.id = Guid.raw();
    //console.log(reqBody);
    let previousPolicyDetails: any;
    if (this.isRecalculate1) {
      this.panelOpenStateProposal = false;
      this.setStep(2);
    } else {
      this.panelOpenStateProposal = false;
      this.setStep(1);
    }
    // this.isDeviationTriggered = false;
    let quoteRequest = JSON.parse(localStorage.getItem("calQuoteReq4w"));
    console.info(this.quoteDetails.GSTToState);
    let PreviousData = JSON.parse(localStorage.getItem("PreviousData"));

    if (!this.cs.isUndefineORNull(quoteRequest)) {
      if (this.quoteDetails.PolicyStartDate != quoteRequest.policyStartDate) {
        quoteRequest.policyStartDate = this.quoteDetails.PolicyStartDate;
        if (!this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyEndDate)) {
          quoteRequest.previousPolicyDetails.PreviousPolicyEndDate =
            this.quoteDetails.PreviousPolicyEndDate;
          //Changes for future date
          quoteRequest.previousPolicyDetails.PreviousPolicyStartDate = moment(
            this.quoteDetails.PreviousPolicyEndDate
          )
            .subtract(1, "years")
            .add(1, "days")
            .format("YYYY-MM-DD");
        }
      }

      console.log('For SAOD', this.showsaod, this.quoteDetails);
      if (this.showsaod) {
        quoteRequest.tpPolicyNo = this.quoteDetails.TPPolicyNo;
        quoteRequest.tpInsurerName = this.quoteDetails.TPInsurerName;
        quoteRequest.previousPolicyDetails.PreviousPolicyType = 'Bundled Package Policy';
        quoteRequest.tpTenure = '0';
        quoteRequest.tenure = '1';
      } else {
        quoteRequest.tpTenure = '1';
        quoteRequest.tenure = '1';
        // quoteRequest.previousPolicyDetails.PreviousPolicyType = 'Comprehensive Package';
      }

      if (!this.cs.isUndefineORNull(quoteRequest.previousPolicyDetails)) {
        if (
          this.quoteDetails.PreviousPolicyEndDate !=
          quoteRequest.previousPolicyDetails.PreviousPolicyEndDate
        ) {
          quoteRequest.previousPolicyDetails.PreviousPolicyEndDate =
            this.quoteDetails.PreviousPolicyEndDate;
          quoteRequest.previousPolicyDetails.PreviousPolicyStartDate = moment(
            this.quoteDetails.PreviousPolicyEndDate
          )
            .subtract(1, "years")
            .add(1, "days")
            .format("YYYY-MM-DD");
        }
      }


      if (
        this.quoteDetails.FirstRegistrationDate !=
        quoteRequest.firstRegistrationDate
      ) {
        quoteRequest.firstRegistrationDate =
          this.quoteDetails.FirstRegistrationDate;
      }
    }
    if (
      !this.cs.isUndefineORNull(quoteRequest) &&
      !this.isRecalculate1 &&
      this.quoteDetails.OtherDiscount == 0 &&
      !quoteRequest.isNoPrevInsurance
    ) {
      let noOfClaims = this.quoteDetails.NoOfClaimsOnPreviousPolicy;
      // this.quoteRequ = this.quoteDetails = quoteRequest;  //Commented on 25-4-2022 - Giving 400 while again doing Get Quote
      this.quoteRequ = quoteRequest;
      this.showPYP = quoteRequest.isNoPrevInsurance;
      if (
        typeof this.quoteDetails.PreviousPolicyDetails != "boolean" &&
        !this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyDetails)
      ) {
        previousPolicyDetails = this.quoteDetails.PreviousPolicyDetails;
        if (this.claimNCB) {
          this.ClaimOnPreviousPolicy = false;
          previousPolicyDetails = this.quoteDetails.PreviousPolicyDetails;
          previousPolicyDetails.TotalNoOfODClaims = "0";
          this.quoteDetails.TotalNoOfODClaims =
            previousPolicyDetails.NoOfClaimsOnPreviousPolicy = "0";
          previousPolicyDetails.ClaimOnPreviousPolicy = false;
          this.quoteDetails.PreviousPolicyDetails = previousPolicyDetails;
          // this.quoteRequ = this.quoteDetails = this.quoteDetails;   //Commented on 25-4-2022 - Giving 400 while again doing Get Quote
          this.quoteRequ = quoteRequest;
          this.PreviousPolicyDetails = this.quoteDetails.PreviousPolicyDetails;
        } else {
          this.ClaimOnPreviousPolicy = true;
          previousPolicyDetails = this.quoteDetails.PreviousPolicyDetails;
          previousPolicyDetails.TotalNoOfODClaims = noOfClaims
            ? noOfClaims
            : "1";
          this.quoteDetails.TotalNoOfODClaims =
            previousPolicyDetails.NoOfClaimsOnPreviousPolicy = noOfClaims
              ? noOfClaims
              : "1";
          previousPolicyDetails.ClaimOnPreviousPolicy = true;
          this.quoteDetails.PreviousPolicyDetails = previousPolicyDetails;
          this.quoteRequ = this.quoteDetails = this.quoteDetails;
          this.PreviousPolicyDetails = this.quoteDetails.PreviousPolicyDetails;
        }
      } else {
        this.quoteRequ = this.quoteDetails = this.quoteDetails;
      }
    } else {
      let discount = JSON.parse(localStorage.getItem("ODAmount"));
      if (this.cs.isUndefineORNull(discount) && !this.isDeviationTriggered) {
        localStorage.removeItem("ODAmount");
        this.quoteDetails.OtherDiscount = 0;
      }
      this.quoteRequ = this.quoteDetails = this.quoteDetails;
      if (
        typeof this.quoteDetails.PreviousPolicyDetails != "boolean" &&
        !this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyDetails)
      ) {
        previousPolicyDetails = this.PreviousPolicyDetails =
          this.quoteDetails.PreviousPolicyDetails;
      }
    }

    let dataForBind = JSON.parse(localStorage.getItem("dataforBack"));
    this.quoteDetails.rtO_LOCATION_DESC = dataForBind.rtO_LOCATION_DESC;
    this.quoteDetails.vehiclemodel = dataForBind.vehiclemodel;
    console.log(this.quoteDetails);

    // if (this.policyType != "NEW") {
    this.quoteDetails.PreviousPolicyEndDate =
      typeof this.quoteDetails.PreviousPolicyDetails != "boolean" &&
        !this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyDetails)
        ? previousPolicyDetails.PreviousPolicyEndDate
        : this.quoteDetails.PreviousPolicyEndDate;
    this.quoteDetails.claims = this.ClaimOnPreviousPolicy
      ? "claimyes"
      : "claimno";
    this.quoteDetails.PreviousInsurerName = !this.cs.isUndefineORNull(
      dataForBind.PreviousInsurerName
    )
      ? dataForBind.PreviousInsurerName
      : "OTHER";
    // }

    if (dataForBind.showsaod) {
      this.quoteDetails.TPStartDate = dataForBind.TPStartDate;
      this.quoteDetails.TPEndDate = dataForBind.TPEndDate;
      this.quoteDetails.TPPolicyNo = dataForBind.TPPolicyNo;
      this.quoteDetails.PreviousPolicyNumber = dataForBind.PreviousPolicyNumber;
    }
    this.id = Guid.raw();
    console.log("EX-data", this.exshowroomprice);
    this.vehicleAge = moment().diff(
      this.quoteDetails.FirstRegistrationDate,
      "years"
    );
    if (this.policyType != "NEW" && this.showsaod == true) {
      this.quoteDetails.TPTenure = "0";
      this.quoteDetails.Tenure = "1";
      this.quoteDetails.PreviousPolicyType = "Bundled Package Policy";
    } else if (this.showsaod == true && this.quoteReqrem.showsaod == true) {
      this.quoteDetails.TPTenure = "0";
      this.quoteDetails.Tenure = "1";
      this.quoteDetails.PreviousPolicyType = "Bundled Package Policy";
    }

    let body: any;

    console.info(this.quoteDetails.GSTToState);

    body = {
      "ambulanceCoverSumInsured": 0,
      "automobileAssociationNumber": "1000",
      //"businessType": this.policyType == "NEW" ? "New Business" : "Roll Over",    //4W
      "channelSource": "IAGENT",  //4W
      "chassisNumber": "",
      "correlationId": this.id,   //4W
      "customerType": this.registrationType,    //4W
      "dealId": this.dealid,    //4W
      "deliveryOrRegistrationDate": moment(
        this.quoteDetails.FirstRegistrationDate
      ).format("YYYY-MM-DD"), //YYY-MM-DD   //4W
      //"emiAmount": reqBody.EMIAmount,
      "engineNumber": "",
      "exShowRoomPrice": parseInt(this.exshowroomprice),   //4W
      "extensionCountryName": this.quoteDetails.ExtensionCountryName,   //4W
      //"garageCashPlanName": reqBody.garageCashPlanName,
      "firstRegistrationDate": moment(
        this.quoteDetails.FirstRegistrationDate
      ).format("YYYY-MM-DD"), //YYY-MM-DD     //4W   // Added parameter as it exists in all products
      "gstToState": this.quoteDetails.GSTToState,   //4W
      "hospitalCashSumInsured": 0,
      "inclusionOfIMT": false,
      "isAmbulanceCover": false,
      //"isAntiTheftDisc": reqBody.IsAntiTheftDisc,   // Need to check
      "isApprovedNCB": false,
      //"isAutomobileAssocnFlag": this.IsAutomobileAssocnFlag,   // Need to check
      "isBreakinFlag": false,
      "isBreakinWaiver": true,
      //"isConsumables": reqBody.IsConsumables,
      "isCustomerModified": false,
      "isDrivingTuitionsFlag": false,
      //"isEMIProtect": reqBody.IsEMIProtect,
      //"isEngineProtectPlus": reqBody.IsEngineProtectPlus,
      "isExtensionCountry": this.quoteDetails.IsExtensionCountry,   //4W
      "isFiberGlassFuelTank": false,
      "isForeignEmbassy": false,
      //"isGarageCash": reqBody.isGarageCash,
      //"isHandicapDisc": this.IsHandicapDisc,
      "isHaveElectricalAccessories": (this.quoteDetails.IsVehicleHaveCNG || this.quoteDetails.IsVehicleHaveLPG) ? true : false,
      "isHaveNonElectricalAccessories": (this.quoteDetails.IsVehicleHaveCNG || this.quoteDetails.IsVehicleHaveLPG) ? false : true,
      "isHospitalCashCover": false,
      //"isLegalLiabilityToPaidDriver": reqBody.IsLegalLiabilityToPaidDriver,
      "isLegalLiabilityToPaidEmployee": this.quoteDetails.IsLegalLiabilityToPaidEmployee,
      "isLimitedToOwnPremises": false,
      "isMedicalReimbursementCover": false,
      "isMoreThanOneVehicle": false,
      "isNamedPassengerPACover": false,
      //"isNCBProtect": reqBody.isNCBProtect,
      //"isNoPrevInsurance": reqBody.isNoPrevInsurance,  // done on 10-03-2022
      "isPACoverOwnerDriver": false,
      "isPACoverUnnamedPassenger": this.quoteDetails.IsPACoverUnnamedPassenger,   //4W
      //"isPACoverWaiver": reqBody.isPACoverWaiver,
      "isPOSDealId": this.IsPOSTransaction_flag,
      "isPrivateUse": false,
      "isProposal": false,
      "isQuote": true,
      "isRenewal": false,
      //"isRTIApplicableflag": reqBody.IsRTIApplicableflag,
      "isSchoolBus": false,
      "isThreeWheelerDiscount": false,
      "isTransferOfNCB": false,
      //"isTyreProtect": reqBody.IsTyreProtect,
      //"isValidDrivingLicense": reqBody.isValidDrivingLicense,
      //"isVehicleHaveCNG": reqBody.IsVehicleHaveCNG,
      //"isVehicleHaveLPG": reqBody.IsVehicleHaveLPG,
      "isVintageCar": false,
      //"isVoluntaryDeductible": reqBody.IsVoluntaryDeductible,
      //"keyProtectPlan": reqBody.keyProtectPlan,
      //"lossOfPersonalBelongingPlanName": reqBody.LossOfPersonalBelongingPlanName,
      "manufacturingYear": parseInt(this.quoteDetails.ManufacturingYear),   //4w
      "medicalReimbursementSumInsured": 0,
      "noOfCleanerOrConductor": 0,
      //"noOfDriver": reqBody.NoOfDriver,
      //"noofEMI": reqBody.NoofEMI,
      "noOfEmployee": this.quoteDetails.NoOfEmployee,   //4W
      "noOfPassenger": 0,
      //"otherDiscount": this.quoteDetails.OtherDiscount,   //Done on 10-03-2022 in 2w+4w
      "otherLoading": 0,
      //"paCoverTenure": reqBody.PACoverTenure,
      "policyEndDate": this.policyEndDate,      //YYY-MM-DD   //4W
      "policyStartDate": moment(this.quoteDetails.PolicyStartDate).format("YYYY-MM-DD"), //YYY-MM-DD    //4W
      //"previousPolicyDetails": reqBody.PreviousPolicyDetails,   //Done on 10-03-2022
      "proposalRefNo": 0,
      "registrationNumber": this.quoteDetails.RegistrationNumber,   //4W
      //"rsaPlanName": reqBody.RSAPlanName,
      "rtoLocationCode": parseInt(this.rtoCode),  //4w
      "saveAsQuote": false,
      "siBiFuelKit": 0,
      "siHaveElectricalAccessories": 0,
      "siHaveNonElectricalAccessories": 0,
      //"sipaCoverUnnamedPassenger": reqBody.SIPACoverUnnamedPassenger,
      //"tenure": reqBody.Tenure,
      //"timeExcessindays": 0,
      //"tpEndDate": moment(this.quoteDetails.TPEndDate).format("YYYY-MM-DD"),
      //"tpInsurerName": this.quoteDetails.TPInsurerName,
      "tppdLimit": this.TPPDLimit,    //4W
      //"tpPolicyNo": this.quoteDetails.TPPolicyNo,
      //"tpStartDate": moment(this.quoteDetails.TPStartDate).format("YYYY-MM-DD"),
      //"tpTenure": this.quoteDetails.TPTenure,
      //"transferOfNCBPercent": this.ncbValue,    //Done on 10-03-2022
      "vehicleAge": 1.0514,
      "vehiclebodyPrice": 0,
      "vehiclechasisPrice": 0,
      "vehicleMakeCode": this.makecode,     //4W
      "vehicleModelCode": this.modelcode,   //4W
      //"voluntaryDeductiblePlanName": reqBody.VoluntaryDeductiblePlanName,
      //"zeroDepPlanName": reqBody.ZeroDepPlanName
    }

    if (this.policyType == "NEW") {
      body.businessType = "New Business";
    } else if (this.policyType == "ROLL") {
      body.businessType = "Roll Over";
    } else {
      body.businessType = "Used";
    }


    // Sejal - 09-03-2022
    // Only TP parameters - both
    if (this.productCode == '2319' || this.productCode == '2320') {
      body.isLegalLiabilityToPaidDriver = this.quoteDetails.IsLegalLiabilityToPaidDriver;
      body.previousPolicyDetails = null;
      body.noOfDriver = this.quoteDetails.NoOfDriver;

      console.info("SAOD SHOW", this.showsaod);
      if (this.showsaod == true) {
        body.tpStartDate = moment(this.quoteDetails.TPStartDate).format("YYYY-MM-DD");
        body.tpEndDate = moment(this.quoteDetails.TPEndDate).format("YYYY-MM-DD");
        body.tpPolicyNo = this.quoteDetails.TPPolicyNo;
        body.tpInsurerName = this.quoteDetails.TPInsurerName;
      }

      if (this.policyType == "ROLL") {
        body.isNoPrevInsurance = true;
        this.showPYP = true;
        // body.tenure = "1";  //if policytype is not new
        // body.tpTenure = "0";   //if policytype is not new 
      }

      body.isPACoverWaiver = true;
      // Sejal - 22-02-2022 - CPA Tenure new requirement for 2WTP & 4WTP NEW
      if (this.policyType == "NEW") {
        body.paCoverTenure = this.quoteDetails.PACoverTenure;
        body.isPACoverWaiver = false;     // Sejal - 23-02-2022 - CPA Tenure
      } else {
        body.paCoverTenure = this.cpaTenure;
        if (this.cpaTenure == 0) {
          body.paCoverTenure = 0;
          body.isPACoverWaiver = true;
        } else {
          body.paCoverTenure = 1;
          body.isPACoverWaiver = false;
        }
      }
    }
    // Only Package parameters - both
    else if (this.productCode == '2311' || this.productCode == '2312') {
      body.isConsumables = this.quoteDetails.IsConsumables;   //4W
      body.isGarageCash = this.quoteDetails.isGarageCash;     //4W
      body.isNCBProtect = this.quoteDetails.isNCBProtect;     //4W
      body.paCoverTenure = this.quoteDetails.PACoverTenure;   //4W
      body.isRTIApplicableflag = this.quoteDetails.IsRTIApplicableflag;   //4W
      body.isValidDrivingLicense = this.isValidDrivingLicense;    //4W
      body.isVoluntaryDeductible = this.quoteDetails.IsVoluntaryDeductible;   //4W
      body.tenure = this.quoteDetails.Tenure;       //4W
      body.tpTenure = this.quoteDetails.TPTenure;   //4W

      if (this.showsaod == true) {
        body.tpStartDate = moment(this.quoteDetails.TPStartDate).format("YYYY-MM-DD");
        body.tpEndDate = moment(this.quoteDetails.TPEndDate).format("YYYY-MM-DD");
        body.tpPolicyNo = this.quoteDetails.TPPolicyNo;
        body.tpInsurerName = this.quoteDetails.TPInsurerName;
      }

      // Only 4W parameters
      if (this.productCode == '2311') {
        body.isEngineProtectPlus = this.quoteDetails.IsEngineProtectPlus;   //4W
        body.isLegalLiabilityToPaidDriver = true;   //4W
        body.isPACoverWaiver = "false";   //4W
        body.isTyreProtect = this.quoteDetails.IsTyreProtect;   //4W
        body.noOfDriver = "1";    //4W

        if (this.quoteDetails.IsEMIProtect == true) {
          body.isEMIProtect = true;
          body.emiAmount = this.quoteDetails.EMIAmount;
          body.noofEMI = this.quoteDetails.NoofEMI;
          body.timeExcessindays = this.quoteDetails.TimeExcessindays;
        }

        if (this.IsHandicapDisc == true) {
          body.isHandicapDisc = this.IsHandicapDisc;
        }

        if (this.isKeyProtectPlanName == true) {
          body.keyProtectPlan = this.quoteDetails.KeyProtectPlan;
        }

        if (this.isLossOfPersonalBelonging == true) {
          body.lossOfPersonalBelongingPlanName = this.quoteDetails.LossOfPersonalBelongingPlanName;
        }

        if (!this.cs.isUndefineORNull(this.quoteDetails.OtherDiscount)) {
          this.isOtherDiscDev = true;
          body.otherDiscount = this.otherDiscount
            ? this.otherDiscount
            : this.quoteDetails.OtherDiscount;
          localStorage.setItem("ODTriggred", JSON.stringify(this.isOtherDiscDev));
        }
      }
      // Only 2W parameters
      else if (this.productCode == '2312') {
        if (this.quoteDetails.OtherDiscount != 0) {
          let OD = this.otherDiscount
            ? this.otherDiscount
            : this.quoteDetails.OtherDiscount;
          //console.log(OD, typeof OD);
          let OD1 = Number(OD) / 100;
          // console.log(OD1, typeof(OD1), OD1.toString());
          body.otherDiscount = OD1.toString();
          // body.otherDiscount = OD;
          if (Number(this.quoteDetails.OtherDiscount) > 0.1) {
            this.isOtherDiscDev = true;
            localStorage.setItem("ODTriggred", JSON.stringify(this.isOtherDiscDev));
          }
        } else {
          body.otherDiscount = "0";
          this.isOtherDiscDev = false;
          localStorage.setItem("ODTriggred", JSON.stringify(this.isOtherDiscDev));
        }
      }

      if (
        this.isRSAPlan == true &&
        !this.cs.isUndefineORNull(this.quoteDetails.RSAPlanName)
      ) {
        body.rsaPlanName = this.quoteDetails.RSAPlanName;
      }

      if (this.quoteDetails.IsVoluntaryDeductible == true) {
        body.voluntaryDeductiblePlanName =
          this.quoteDetails.VoluntaryDeductiblePlanName;
      }

      if (
        this.isZeroDepPlanName == true &&
        !this.cs.isUndefineORNull(this.quoteDetails.ZeroDepPlanName)
      ) {
        body.zeroDepPlanName = this.quoteDetails.ZeroDepPlanName;
      }

      if (
        this.quoteDetails.isGarageCash == true &&
        !this.cs.isUndefineORNull(this.quoteDetails.garageCashPlanName)
      ) {
        body.garageCashPlanName = this.quoteDetails.garageCashPlanName;
      }

      if (this.IsAntiTheftDisc == true) {
        body.isAntiTheftDisc = this.IsAntiTheftDisc;
      }

      if (this.IsAutomobileAssocnFlag == true) {
        body.isAutomobileAssocnFlag = this.IsAutomobileAssocnFlag;
      }

      // Added parameter as it exists in package products
      if (
        this.quoteDetails.isNCBProtect == true &&
        !this.cs.isUndefineORNull(this.quoteDetails.ncbProtectPlanName)
      ) {
        body.ncbProtectPlanName = this.quoteDetails.ncbProtectPlanName;
      }

      //Previous Policy Details
      if (this.policyType == "ROLL" || this.policyType == "USED") {
        body.businessType = this.policyType == "ROLL" ? "Roll Over" : "Used";
        body.nomineeDetails = null;   //4W
        body.financierDetails = null;   //4W
        body.customerDetails = null;
        body.spDetails = null;    //4W

        if (this.quoteDetails.TotalNoOfODClaims == "0") {
          this.ClaimOnPreviousPolicy = false;
        }

        if (this.productCode == '2311' && this.showsaod == true) {
          this.quoteDetails.TPTenure = "0";
          this.quoteDetails.Tenure = "1";
          this.quoteDetails.PreviousPolicyType = "Bundled Package Policy";
        }

        if (
          this.quoteDetails.PreviousPolicyDetails == true ||
          this.showsaod == true
        ) {
          if (
            this.systemQC == true &&
            this.ncbValue == 0 &&
            this.ClaimOnPreviousPolicy == false
          ) {
            let PPED = this.quoteDetails.PreviousPolicyEndDate
              ? this.quoteDetails.PreviousPolicyEndDate
              : this.PreviousPolicyDetails.PreviousPolicyEndDate;
            if (PPED == "Invalid date") {
              body.isQCByPass = false;
              body.isNoPrevInsurance = true;   //4W
              this.showPYP = true;
            } else {
              body.isQCByPass = true;
              body.isNoPrevInsurance = false;
              this.showPYP = false;
              let prevPolStartDate = moment(PPED)
                .subtract(1, "years")
                .add(1, "days")
                .format("YYYY-MM-DD");
              this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
              let prevStartDate = moment(
                this.quoteDetails.PreviousPolicyStartDate
              ).format("YYYY-MM-DD");
              let prevEndDate = moment(PPED).format("YYYY-MM-DD");
              this.quoteDetails.PreviousPolicyEndDate = prevEndDate;
              this.cs
                .get(
                  "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
                  prevStartDate +
                  "&prevEndDate=" +
                  prevEndDate
                )
                .subscribe((res: any) => {
                  this.previousPolicyTen = res;
                  this.quoteDetails.PreviousPolicyTenure = this.previousPolicyTen;
                });

              this.PreviousPolicyDetails.PreviousPolicyType = this.quoteDetails
                .PreviousPolicyType
                ? this.quoteDetails.PreviousPolicyType
                : "Comprehensive Package";    //4W
              this.PreviousPolicyDetails.PreviousPolicyStartDate = moment(
                this.quoteDetails.PreviousPolicyStartDate
              ).format("YYYY-MM-DD");     //4W
              this.PreviousPolicyDetails.PreviousPolicyEndDate = moment(
                this.quoteDetails.PreviousPolicyEndDate
              ).format("YYYY-MM-DD");      //4W
              this.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;   //4W
              this.PreviousPolicyDetails.PreviousPolicyNumber =
                this.quoteDetails.PreviousPolicyNumber;     //4W
              this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
                this.ClaimOnPreviousPolicy;     //4W
              this.PreviousPolicyDetails.TotalNoOfODClaims =
                this.quoteDetails.TotalNoOfODClaims;      //4W
              this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
                this.quoteDetails.TotalNoOfODClaims;        //4W
              this.PreviousPolicyDetails.PreviousInsurerName = this.companyCode;
              this.PreviousPolicyDetails.PreviousVehicleSaleDate = this
                .PreviousPolicyDetails.PreviousVehicleSaleDate
                ? this.PreviousPolicyDetails.PreviousVehicleSaleDate
                : "0001-01-01T00:00:00";      //4W
              this.quoteDetails.PreviousVehicleSaleDate =
                this.PreviousPolicyDetails.PreviousVehicleSaleDate;
              this.PreviousPolicyDetails.PreviousPolicyTenure =
                this.previousPolicyTen;     //4w

              body.previousPolicyDetails = this.PreviousPolicyDetails;    //4W
            }
            this.getInsurerName();
            let compName = this.insurerlist.find(
              (c) => c.companyName == this.quoteDetails.PreviousInsurerName
            );

            if (this.cs.isUndefineORNull(compName)) {
              this.companyCode = "OTHER";
            } else {
              this.companyCode = compName.companyCode;
            }
          } else {
            console.log(this.PreviousPolicyDetails);
            let PPED = this.quoteDetails.PreviousPolicyEndDate
              ? this.quoteDetails.PreviousPolicyEndDate
              : this.PreviousPolicyDetails.PreviousPolicyEndDate;
            if (PPED == "Invalid date") {
              body.isQCByPass = false;
              body.isNoPrevInsurance = true;
              this.showPYP = true;
            } else {
              body.isNoPrevInsurance = false;
              this.showPYP = false;
              body.isQCByPass = true;
              let prevPolStartDate = moment(PPED)
                .subtract(1, "years")
                .add(1, "days")
                .format("YYYY-MM-DD");
              this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
              let prevStartDate = moment(
                this.quoteDetails.PreviousPolicyStartDate
              ).format("YYYY-MM-DD");
              let prevEndDate = moment(PPED).format("YYYY-MM-DD");
              this.quoteDetails.PreviousPolicyEndDate = prevEndDate;
              this.cs
                .get(
                  "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
                  prevStartDate +
                  "&prevEndDate=" +
                  prevEndDate
                )
                .subscribe((res: any) => {
                  this.previousPolicyTen = res;
                  this.quoteDetails.PreviousPolicyTenure = this.previousPolicyTen;
                });

              this.PreviousPolicyDetails.PreviousPolicyType = this.quoteDetails
                .PreviousPolicyType
                ? this.quoteDetails.PreviousPolicyType
                : "Comprehensive Package";
              this.PreviousPolicyDetails.PreviousPolicyStartDate = moment(
                this.quoteDetails.PreviousPolicyStartDate
              ).format("YYYY-MM-DD");
              this.PreviousPolicyDetails.PreviousPolicyEndDate = moment(
                this.quoteDetails.PreviousPolicyEndDate
              ).format("YYYY-MM-DD");
              this.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
              if (this.showsaod == true) {
                this.PreviousPolicyDetails.PreviousPolicyNumber =
                  this.quoteDetails.PreviousPolicyNumber;
              }
              this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
                this.ClaimOnPreviousPolicy;
              this.PreviousPolicyDetails.TotalNoOfODClaims =
                this.quoteDetails.TotalNoOfODClaims;
              this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
                this.quoteDetails.TotalNoOfODClaims;
              if (this.showsaod == true) {
                this.PreviousPolicyDetails.PreviousInsurerName = this.companyCode;
              }
              this.PreviousPolicyDetails.PreviousVehicleSaleDate = this
                .PreviousPolicyDetails.PreviousVehicleSaleDate
                ? this.PreviousPolicyDetails.PreviousVehicleSaleDate
                : "0001-01-01T00:00:00";
              this.quoteDetails.PreviousVehicleSaleDate =
                this.PreviousPolicyDetails.PreviousVehicleSaleDate;
              this.PreviousPolicyDetails.PreviousPolicyTenure =
                this.previousPolicyTen;

              body.previousPolicyDetails = this.PreviousPolicyDetails;
            }
            if (this.showsaod == true) {
              this.getInsurerName();
              let compName = this.insurerlist.find(
                (c) => c.companyName == this.quoteDetails.PreviousInsurerName
              );

              if (this.cs.isUndefineORNull(compName)) {
                this.companyCode = "OTHER";
              } else {
                this.companyCode = compName.companyCode;
              }
            }
          }
        } else {
          //console.log('ABCDEFG', this.quoteDetails.PreviousPolicyDetails, this.PreviousPolicyDetails, PreviousPolicyDetails)
          if (this.quoteDetails.PreviousPolicyEndDate == "Invalid date" || this.cs.isUndefineORNull(this.quoteDetails.PreviousPolicyEndDate) || this.quoteDetails.PreviousPolicyDetails == false) {
            // if(this.cs.isUndefineORNull(this.PreviousPolicyDetails)){
            // if(this.quoteDetails.PreviousPolicyEndDate == "Invalid date" || this.cs.isUndefineORNull(this.PreviousPolicyDetails.PreviousPolicyNumber)){
            body.isNoPrevInsurance = true;
            this.showPYP = true;
            body.isQCByPass = false;
            // }else{
            //   body.isNoPrevInsurance = false;
            // this.showPYP = false;
            // body.isQCByPass = true;
            // body.previousPolicyDetails = this.PreviousPolicyDetails;
            // body.previousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
            // }
          } else {
            body.isNoPrevInsurance = false;
            this.showPYP = false;
            body.isQCByPass = true;
            body.previousPolicyDetails = this.PreviousPolicyDetails;
            body.previousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
          }
        }
        body.isSelfInspection = this.IsSelfInspection;  //4W
      } else {
        body.BusinessType = "New Business";
        if (this.showPrev == true) {
          if (this.ncbValue == 0) {
            body.IsTransferOfNCB = false;
            if (this.showsaod == true) {
              this.getInsurerName();
              // this.companyCode = this.insurerlist.find(
              //   (c) => c.companyName == this.quoteDetails.PreviousInsurerName
              // ).companyCode;
              let compName = this.insurerlist.find(
                (c) => c.companyName == this.quoteDetails.PreviousInsurerName
              );
              if (this.cs.isUndefineORNull(compName)) {
                this.companyCode = "OTHER";
              } else {
                this.companyCode = compName.companyCode;
              }
            }
            // this.getInsurerName();
            // this.companyCode = this.insurerlist.find(c => c.companyName == this.quoteDetails.PreviousInsurerName).companyCode;
            let prevPolStartDate = moment(this.quoteDetails.PreviousPolicyEndDate)
              .subtract(1, "years")
              .add(1, "days")
              .format("YYYY-MM-DD");
            this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
            let prevStartDate = moment(
              this.quoteDetails.PreviousPolicyStartDate
            ).format("YYYY-MM-DD");
            let prevEndDate = moment(
              this.quoteDetails.PreviousPolicyEndDate
            ).format("YYYY-MM-DD");
            this.quoteDetails.PreviousPolicyEndDate = prevEndDate;
            this.cs
              .get(
                "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
                prevStartDate +
                "&prevEndDate=" +
                prevEndDate
              )
              .subscribe((res: any) => {
                this.previousPolicyTen = res;
                this.quoteDetails.PreviousPolicyTenure = this.previousPolicyTen;
              });

            this.PreviousPolicyDetails.PreviousPolicyType = this.quoteDetails
              .PreviousPolicyType
              ? this.quoteDetails.PreviousPolicyType
              : "Comprehensive Package";
            this.PreviousPolicyDetails.PreviousPolicyStartDate = prevStartDate;
            this.PreviousPolicyDetails.PreviousPolicyEndDate = prevEndDate;
            this.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
            if (this.showsaod == true) {
              this.PreviousPolicyDetails.PreviousPolicyNumber =
                this.quoteDetails.PreviousPolicyNumber;
            }
            this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
              this.ClaimOnPreviousPolicy;
            this.PreviousPolicyDetails.TotalNoOfODClaims =
              this.quoteDetails.TotalNoOfODClaims;
            this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
              this.quoteDetails.TotalNoOfODClaims;
            if (this.showsaod == true) {
              this.PreviousPolicyDetails.PreviousInsurerName = this.companyCode;
            }
            this.PreviousPolicyDetails.PreviousVehicleSaleDate = this
              .PreviousPolicyDetails.PreviousVehicleSaleDate
              ? this.PreviousPolicyDetails.PreviousVehicleSaleDate
              : moment(this.quoteDetails.PreviousVehicleSaleDate).format(
                "YYYY-MM-DD"
              );
            this.quoteDetails.PreviousVehicleSaleDate =
              this.PreviousPolicyDetails.PreviousVehicleSaleDate;
            this.PreviousPolicyDetails.PreviousPolicyTenure =
              this.previousPolicyTen;
            body.previousPolicyDetails = this.PreviousPolicyDetails;
          } else {
            if (this.showsaod == true) {
              this.getInsurerName();
              // this.companyCode = this.insurerlist.find(
              //   (c) => c.companyName == this.quoteDetails.PreviousInsurerName
              // ).companyCode;
              let compName = this.insurerlist.find(
                (c) => c.companyName == this.quoteDetails.PreviousInsurerName
              );
              if (this.cs.isUndefineORNull(compName)) {
                this.companyCode = "OTHER";
              } else {
                this.companyCode = compName.companyCode;
              }
            }
            if (
              !this.cs.isUndefineORNull(this.ncbValue) ||
              this.claimAdd != "claimyes"
            ) {
              body.transferOfNCBPercent = parseInt(this.ncbValue);
              body.IsTransferOfNCB = this.isTransfer;
            }

            let prevPolStartDate = moment(this.quoteDetails.PreviousPolicyEndDate)
              .subtract(1, "years")
              .add(1, "days")
              .format("YYYY-MM-DD");
            this.quoteDetails.PreviousPolicyStartDate = prevPolStartDate;
            let prevStartDate = moment(
              this.quoteDetails.PreviousPolicyStartDate
            ).format("YYYY-MM-DD");
            let prevEndDate = moment(
              this.quoteDetails.PreviousPolicyEndDate
            ).format("YYYY-MM-DD");
            this.quoteDetails.PreviousPolicyEndDate = prevEndDate;
            this.cs
              .get(
                "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
                prevStartDate +
                "&prevEndDate=" +
                prevEndDate
              )
              .subscribe((res: any) => {
                this.previousPolicyTen = res;
                this.quoteDetails.PreviousPolicyTenure = this.previousPolicyTen;
              });

            this.PreviousPolicyDetails.PreviousPolicyType = this.quoteDetails
              .PreviousPolicyType
              ? this.quoteDetails.PreviousPolicyType
              : "Comprehensive Package";
            this.PreviousPolicyDetails.PreviousPolicyStartDate = prevStartDate;
            this.PreviousPolicyDetails.PreviousPolicyEndDate = prevEndDate;
            this.PreviousPolicyDetails.BonusOnPreviousPolicy = this.ncbValue;
            if (this.showsaod == true) {
              this.PreviousPolicyDetails.PreviousPolicyNumber =
                this.quoteDetails.PreviousPolicyNumber;
            }
            this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
              this.ClaimOnPreviousPolicy;
            this.PreviousPolicyDetails.TotalNoOfODClaims =
              this.quoteDetails.TotalNoOfODClaims;
            this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
              this.quoteDetails.TotalNoOfODClaims;
            if (this.showsaod == true) {
              this.PreviousPolicyDetails.PreviousInsurerName = this.companyCode;
            }
            this.PreviousPolicyDetails.PreviousVehicleSaleDate = this
              .PreviousPolicyDetails.PreviousVehicleSaleDate
              ? this.PreviousPolicyDetails.PreviousVehicleSaleDate
              : moment(this.quoteDetails.PreviousVehicleSaleDate).format(
                "YYYY-MM-DD"
              );
            this.quoteDetails.PreviousVehicleSaleDate =
              this.PreviousPolicyDetails.PreviousVehicleSaleDate;
            this.PreviousPolicyDetails.PreviousPolicyTenure =
              this.previousPolicyTen;
            body.previousPolicyDetails = this.PreviousPolicyDetails;
          }
        } else {
          body.IsTransferOfNCB = false;
        }
      }
    }

    // Not 2W Package parameters but rest
    if (this.productCode != '2312') {
      body.isVehicleHaveLPG = this.quoteDetails.IsVehicleHaveLPG;   //4W
      body.isVehicleHaveCNG = this.quoteDetails.IsVehicleHaveCNG;   //4W
      body.siVehicleHaveLPG_CNG = this.quoteDetails.SIVehicleHaveLPG_CNG;   //added parameter as it exists in 4W Package & TP

      if (
        this.quoteReq == null &&
        (this.seatingcapacity == null ||
          this.seatingcapacity == undefined ||
          this.seatingcapacity == "")
      ) {
        this.seatingcapacity = this.manumod.find(
          (c) => c.vehiclemodel == this.quoteDetails.vehiclemodel
        ).seatingcapacity;
      }
    }

    if (this.quoteDetails.IsPACoverUnnamedPassenger == true) {
      let PACovervalue =
        JSON.parse(this.quoteDetails.SIPACoverUnnamedPassenger) *
        JSON.parse(this.seatingcapacity);
      body.sipaCoverUnnamedPassenger = PACovervalue;
    }

    this.oldSaveQuoteRequ = this.oldSaveQuote(body);
    localStorage.setItem("calQuoteReq", JSON.stringify(this.oldSaveQuoteRequ));
    let str = JSON.stringify(body);
    localStorage.setItem("calQuoteReq4w", str);

    this.cs.loaderStatus = true;
    this.spinloader1 = true;

    this.cs.getBizzTalkQuote('/quote/premiumcalculation', str).then((res: any) => {
      console.log(res);
      this.spinloader1 = false;
      this.cs.loaderStatus = false;
      localStorage.setItem("calQuoteRes4w", JSON.stringify(res));

      this.totalPremium = this.totalPremiumTP = res.finalPremium;
      console.log(this.isRecalculate, this.isRecalculate1);
      if (this.isRecalculate1) {
        this.reCalDealerDiscount = res;
      }
      if (res.status == "Failed" || res.status == null) {
        console.log("Failed");
        this.failedquote = true;
        this.failedCalculatedQuote = res;
        this.totalPremium = this.totalPremiumTP = res.finalPremium;
        this.freezeManuModel = false;
        let errormsg = res.message;
        this.api.handleerrors(errormsg);
        this.failerrormsg = errormsg;
        let errorbody = {
          RequestJson: JSON.stringify(body),
          ResponseJson: JSON.stringify(res),
          ServiceURL:
            commonData.bizURL + "/proposal/privatecarcreateproposal",
          CorrelationID: this.id,
        };
        this.api.adderrorlogs(errorbody);
        this.cs.loaderStatus = false;
        this.spinloader = false;
        if (this.isRecalculate1) {
          this.panelOpenStateProposal = false;
          this.setStep(2);
        } else {
          this.panelOpenStateProposal = false;
          this.setStep(1);
        }
      } else {
        this.oldSaveRespRequest = this.oldSaveResponse(res);
        let olddata = JSON.stringify(this.oldSaveRespRequest);
        localStorage.setItem("calQuoteRes", olddata);
        console.log("Success");
        if (this.showsaod == true && this.showSCPAinNysa) {
          let newBundleID: any;
          newBundleID = Guid.create();
          this.saodBundleID = newBundleID.value;
          this.quoteForSCPA();
        }
        // NEW PDF
        this.bFlag = res.breakingFlag;
        this.failedquote = false;
        if (this.isRecalculate1) {
          // this.quoteResp = res;
          this.quoteResp = this.calculatedQuote = res;
          let antiTheftDiscount = this.calculatedQuote.riskDetails
            .antiTheftDiscount
            ? this.calculatedQuote.riskDetails.antiTheftDiscount
            : 0;
          let automobileAssociationDiscount = this.calculatedQuote
            .riskDetails.automobileAssociationDiscount
            ? this.calculatedQuote.riskDetails
              .automobileAssociationDiscount
            : 0;
          let handicappedDiscount = this.calculatedQuote.riskDetails
            .handicappedDiscount
            ? this.calculatedQuote.riskDetails.handicappedDiscount
            : 0;
          let bonusDiscount = this.calculatedQuote.riskDetails
            .bonusDiscount
            ? this.calculatedQuote.riskDetails.bonusDiscount
            : 0;
          let voluntaryDiscount = this.calculatedQuote.riskDetails
            .voluntaryDiscount
            ? this.calculatedQuote.riskDetails.voluntaryDiscount
            : 0;
          let tppD_Discount = this.calculatedQuote.riskDetails
            .tppD_Discount
            ? this.calculatedQuote.riskDetails.tppD_Discount
            : 0;
          this.totalDiscount =
            antiTheftDiscount +
            automobileAssociationDiscount +
            handicappedDiscount +
            bonusDiscount +
            voluntaryDiscount +
            tppD_Discount;
          let pkgPrem = this.calculatedQuote.packagePremium
            ? this.calculatedQuote.packagePremium
            : this.calculatedQuote.totalLiabilityPremium;
          this.premiumWithoutDiscount = pkgPrem + this.totalDiscount;
        }
        if (res.isQuoteDeviation == true) {
          this.isOtherDiscDev = false;
          if (this.policyType == "NEW") {
            swal({
              text:
                "This case would go into deviation.Reason: " +
                res.deviationMessage +
                " Would you like to continue?",
              dangerMode: true,
              closeOnClickOutside: false,
              buttons: ["Yes", "No"],
            }).then((willDelete) => {
              if (willDelete) {
                console.log("Stay");
              } else {
                let type;
                if (this.policyType == "NEW") {
                  type = "N";
                } else {
                  type = "R";
                }
                this.cs.geToDashboardapproval(this.productCode, type);
              }
            });
            this.cs.loaderStatus = false;
            this.spinloader = false;
          } else {
            this.cs.loaderStatus = false;
            this.spinloader = false;
            this.isDeviationTriggered = true;
            this.deviationMsg =
              "This case would go into deviation. Reason: " +
              res.deviationMessage;
            // if(this.isIDVChangeDev){
            //   this.deviationMsg = 'The case has triggered deviation as the maximum and minimum limit has exceeded, exshowroom deviation is at -90%';
            // }else{
            //   this.deviationMsg = 'The quote has triggered deviation for OD discount as limit has exceeded';
            // }
          }
        } else {
          if (
            this.quoteDetails.PreviousPolicyDetails == true &&
            body.isQCByPass == true &&
            this.ncbValue == 0
          ) {
            if (
              !this.totalPremiumTP &&
              this.ClaimOnPreviousPolicy == false
            ) {
              this.showqcmsg = true;
              // swal({ text: 'Congratulations, your policy will not be triggered for QC. Proceed for payment to get your instant policy' });
            }
          }
          if (this.isRecalculate1) {
            // this.panelOpenStateProposal = true;
            this.setStep(2);
          } else {
            // this.panelOpenStateProposal = false;
            this.setStep(1);
          }
          this.isDeviationTriggered = false;
          this.isOtherDiscDev = false;
          this.quoteDetails.OtherDiscount = 0;
        }
        this.bFlag = res.breakingFlag;
        if (res.breakingFlag == true) {
          this.cs.loaderStatus = false;
          this.spinloader = false;
          if (
            this.quoteDetails.PreviousPolicyDetails == false &&
            (this.policyType == "ROLL" || this.policyType == "USED") &&
            this.showsaod == false
          ) {
            this.showAddon = true;
            //this.quoteRequest = body; ////
            this.quoteResp = this.calculatedQuote = res;
            if (
              res.riskDetails.breakinLoadingAmount &&
              res.riskDetails.breakinLoadingAmount != 0
            ) {
              this.basicbreak =
                res.riskDetails.basicOD +
                res.riskDetails.breakinLoadingAmount;
              this.ODandTPAmount =
                this.basicbreak + res.riskDetails.basicTP;
            } else {
              this.basicbreak = res.riskDetails.basicOD;
              this.ODandTPAmount =
                this.basicbreak + res.riskDetails.basicTP;
            }
            this.quoteRequest = body;
            if (this.calculatedQuote.riskDetails.voluntaryDiscount < 1) {
              this.voluntaryDiscount = "1";
            } else if (
              this.calculatedQuote.riskDetails.voluntaryDiscount > 1
            ) {
              this.voluntaryDiscount = Math.round(
                this.calculatedQuote.riskDetails.voluntaryDiscount
              );
            }
            this.cs.loaderStatus = false;
            this.spinloader = false;
            //Changes for one premium
            // this.PremiumCalculation();
            this.showAddon = true;
            this.quickquote = false;
            // if (!this.totalPremiumTP) {
            //   this.panelOpenState = false; 
            //   this.panelOpenStateAddon = true;
            // }
            // this.defaultPlan();
          } else {
            this.showAddon = true;
            this.quoteResp = this.calculatedQuote = res;
            if (
              res.riskDetails.breakinLoadingAmount &&
              res.riskDetails.breakinLoadingAmount != 0
            ) {
              this.basicbreak =
                res.riskDetails.basicOD +
                res.riskDetails.breakinLoadingAmount;
              this.ODandTPAmount =
                this.basicbreak + res.riskDetails.basicTP;
            } else {
              this.basicbreak = res.riskDetails.basicOD;
              this.ODandTPAmount =
                this.basicbreak + res.riskDetails.basicTP;
            }
            this.quoteRequest = body;
            if (this.calculatedQuote.riskDetails.voluntaryDiscount < 1) {
              this.voluntaryDiscount = "1";
            } else if (
              this.calculatedQuote.riskDetails.voluntaryDiscount > 1
            ) {
              this.voluntaryDiscount = Math.round(
                this.calculatedQuote.riskDetails.voluntaryDiscount
              );
            }
            this.cs.loaderStatus = false;
            this.spinloader = false;
            //Changes for one premium
            // this.PremiumCalculation();
            this.showAddon = true;
            this.quickquote = false;
            if (!this.totalPremiumTP) {
              this.panelOpenState = false;
              this.panelOpenStateAddon = true;
            }
            // this.defaultPlan();
          }
          if (res.isQuoteDeviation == true) {
            this.isDeviationTriggered = true;
            this.deviationMsg =
              "This case would go into deviation. Reason: " +
              res.deviationMessage;
          } else {
            this.isDeviationTriggered = false;
          }

          if (this.isRecalculate1) {
            // this.panelOpenStateProposal = true;
            this.setStep(2);
          } else {
            // this.panelOpenStateProposal = false;
            this.setStep(1);
          }
        } else {
          // this.isDeviationTriggered = false;
          this.quoteResp = this.calculatedQuote = res;
          this.basicbreak = res.riskDetails.basicOD;
          this.ODandTPAmount =
            res.riskDetails.basicOD + res.riskDetails.basicTP;
          this.quoteRequest = body;
          if (this.calculatedQuote.riskDetails.voluntaryDiscount < 1) {
            this.voluntaryDiscount = "1";
          } else if (
            this.calculatedQuote.riskDetails.voluntaryDiscount > 1
          ) {
            this.voluntaryDiscount = Math.round(
              this.calculatedQuote.riskDetails.voluntaryDiscount
            );
          }
          this.cs.loaderStatus = false;
          this.spinloader = false;
          //Changes for one premium
          // this.PremiumCalculation();
          this.showAddon = true;
          this.quickquote = false;
          if (!this.totalPremiumTP) {
            this.panelOpenState = false;
            this.panelOpenStateAddon = true;
          }
          // this.defaultPlan();
          if (this.isRecalculate1) {
            // this.panelOpenStateProposal = true;
            this.setStep(2);
          } else {
            // this.panelOpenStateProposal = false;
            this.setStep(1);
          }
        }
      }
      localStorage.setItem("getVehiclDetalsClicked", "false");
    }).catch((err: any) => {
      console.log(err);
      this.totalPremiumTP = "Server Issue";
      this.spinloader1 = false;
      this.failedquote = true;
      this.cs.loaderStatus = false;
      this.setStep(0);
    })
  }

  oldSaveQuote(body: any) {
    console.info(body.gstToState);
    let savequoteRequestBody;
    savequoteRequestBody = {
      "BusinessType": body.businessType,
      "ChannelSource": body.channelSource,
      "CorrelationId": body.correlationId,
      "CustomerDetails": body.customerDetails,
      "CustomerType": body.customerType,
      "DealId": body.dealId,
      "DeliveryOrRegistrationDate": body.deliveryOrRegistrationDate,
      "ExShowRoomPrice": body.exShowRoomPrice,
      "ExtensionCountryName": body.extensionCountryName,
      "financierDetails": body.financierDetails,
      "FirstRegistrationDate": body.firstRegistrationDate,
      "GSTToState": body.gstToState,
      "IsConsumables": body.isConsumables,
      "IsEngineProtectPlus": body.isEngineProtectPlus,
      "IsExtensionCountry": body.isExtensionCountry,
      "isGarageCash": body.isGarageCash,
      "IsLegalLiabilityToPaidDriver": body.isLegalLiabilityToPaidDriver,
      "IsLegalLiabilityToPaidEmployee": body.isLegalLiabilityToPaidEmployee,
      "isNCBProtect": body.isNCBProtect,
      "isNoPrevInsurance": body.isNoPrevInsurance,
      "IsPACoverUnnamedPassenger": body.isPACoverUnnamedPassenger,
      "isPACoverWaiver": "false",
      "IsQCByPass": body.isQCByPass,
      "IsRTIApplicableflag": body.isRTIApplicableflag,
      "IsSelfInspection": body.isSelfInspection,
      "IsTyreProtect": body.isTyreProtect,
      "isValidDrivingLicense": body.isValidDrivingLicense,
      "IsVehicleHaveCNG": body.isVehicleHaveCNG,
      "IsVehicleHaveLPG": body.isVehicleHaveLPG,
      "IsVoluntaryDeductible": body.isVoluntaryDeductible,
      "ManufacturingYear": body.manufacturingYear.toString(),
      "NomineeDetails": body.nomineeDetails,
      "NoOfDriver": body.noOfDriver,
      "NoOfEmployee": body.noOfEmployee,
      "PACoverTenure": body.paCoverTenure,
      "PolicyEndDate": body.policyEndDate,
      "PolicyStartDate": body.policyStartDate,
      "PreviousPolicyDetails": body.previousPolicyDetails,
      "RegistrationNumber": body.registrationNumber,
      "RTOLocationCode": body.rtoLocationCode.toString(),
      "SIVehicleHaveLPG_CNG": body.siVehicleHaveLPG_CNG,
      "SPDetails": body.spDetails,
      "Tenure": body.tenure,
      "TPPDLimit": body.tppdLimit,
      "TPTenure": body.tpTenure,
      "VehicleMakeCode": body.vehicleMakeCode,
      "VehicleModelCode": body.vehicleModelCode
    }

    // Sejal - 29-03-2922 - Quote API Integration

    if (body.tpStartDate && body.tpEndDate) {
      savequoteRequestBody.TPStartDate = body.tpStartDate;
      savequoteRequestBody.TPEndDate = body.tpEndDate;
      savequoteRequestBody.TPPolicyNo = body.tpPolicyNo;
      savequoteRequestBody.TPInsurerName = body.tpInsurerName;
    }

    if (this.quoteDetails.IsEMIProtect == true && body.isEMIProtect == true) {
      savequoteRequestBody.IsEMIProtect = body.isEMIProtect;
      savequoteRequestBody.EMIAmount = body.emiAmountt;
      savequoteRequestBody.NoofEMI = body.noofEMI;
      savequoteRequestBody.TimeExcessindays = body.timeExcessindays;
    }

    if (this.IsHandicapDisc == true) {
      savequoteRequestBody.IsHandicapDisc = body.isHandicapDisc;
    }

    if (this.isKeyProtectPlanName == true) {
      savequoteRequestBody.KeyProtectPlan = body.keyProtectPlan;
    }

    if (this.isLossOfPersonalBelonging == true) {
      savequoteRequestBody.LossOfPersonalBelongingPlanName = body.lossOfPersonalBelongingPlanName;
    }

    if (!this.cs.isUndefineORNull(this.quoteDetails.OtherDiscount)) {
      this.isOtherDiscDev = true;
      savequoteRequestBody.OtherDiscount = body.otherDiscount;
    }

    return savequoteRequestBody;
  }

  oldSaveResponse(body: any) {
    let saveRespBody = {
      "breakingFlag": body.breakingFlag,
      "correlationId": body.correlationId,
      "deviationMessage": body.deviationMessage,
      "finalPremium": body.finalPremium,
      "generalInformation": body.generalInformation,
      "isApprovalRequired": body.isApprovalRequired,
      "isQuoteDeviation": body.isQuoteDeviation,
      "message": body.message,
      "packagePremium": body.packagePremium,
      "proposalStatus": body.proposalStatus,
      "riskDetails": {
        "antiTheftDiscount": body.riskDetails.antiTheftDiscount,
        "automobileAssociationDiscount": body.riskDetails.automobileAssociationDiscount,
        "basicOD": body.riskDetails.basicOD,
        "basicTP": body.riskDetails.basicTP,
        "biFuelKitOD": body.riskDetails.biFuelKitOD,
        "biFuelKitTP": body.riskDetails.biFuelKitTP,
        "bonusDiscount": body.riskDetails.bonusDiscount,
        "breakinLoadingAmount": body.riskDetails.breakinLoadingAmount,
        "consumables": body.riskDetails.consumables,
        "electricalAccessories": body.riskDetails.electricalAccessories,
        "emeCover": body.riskDetails.emeCover,
        "emiProtect": body.riskDetails.emiProtect,
        "employeesOfInsured": body.riskDetails.employeesOfInsured,
        "engineProtect": body.riskDetails.engineProtect,
        "fibreGlassFuelTank": body.riskDetails.fibreGlassFuelTank,
        "garageCash": body.riskDetails.garageCash,
        "geographicalExtensionOD": body.riskDetails.geographicalExtensionOD,
        "geographicalExtensionTP": body.riskDetails.geographicalExtensionTP,
        "handicappedDiscount": body.riskDetails.handicappedDiscount,
        "keyProtect": body.riskDetails.keyProtect,
        "lossOfPersonalBelongings": body.riskDetails.lossOfPersonalBelongings,
        "ncbPercentage": body.riskDetails.ncbPercentage,
        "nonElectricalAccessories": body.riskDetails.nonElectricalAccessories,
        "paCoverForOwnerDriver": body.riskDetails.paCoverForOwnerDriver,
        "paCoverForUnNamedPassenger": body.riskDetails.paCoverForUnNamedPassenger,
        "paCoverWaiver": body.riskDetails.paCoverWaiver,
        "paidDriver": body.riskDetails.paidDriver,
        "returnToInvoice": body.riskDetails.returnToInvoice,
        "roadSideAssistance": body.riskDetails.roadSideAssistance,
        "tppD_Discount": body.riskDetails.tppD_Discount,
        "tyreProtect": body.riskDetails.tyreProtect,
        "voluntaryDiscount": body.riskDetails.voluntaryDiscount,
        "zeroDepreciation": body.riskDetails.zeroDepreciation
      },
      "roadSideAssistanceService": body.roadSideAssistanceService,
      "specialDiscount": body.specialDiscount,
      "status": body.status,
      "totalLiabilityPremium": body.totalLiabilityPremium,
      "totalOwnDamagePremium": body.totalOwnDamagePremium,
      "totalTax": body.totalTax
    }

    saveRespBody.generalInformation.seatingCapacity = body.generalInformation.seatingCapacity.toString();
    saveRespBody.generalInformation.carryingCapacity = body.generalInformation.carryingCapacity.toString();

    return saveRespBody;
  }

  // All Risk Electric Bike

  batterysavervalue(ev: any) {
    let str = /^(?!0+$)/.test(ev.target.value);
    if (ev.target.value.length <= 50 && str == true) {
      //this.chasismsg = "Chassis Number should be atleast 12 digit";
      this.isBatterySaverValid = true;
    } else {
      this.isBatterySaverValid = false;
    }
  }

  uniquenumbervalue(ev: any) {
    let str = /^(?!0+$)/.test(ev.target.value);
    if (ev.target.value.length > 0 && str == true) {
      this.isUniqueNumberValid = true;
    } else {
      this.isUniqueNumberValid = false;
    }
  }

  invoicenumbercheck(ev: any) {
    let str = /^(?!0+$)/.test(ev.target.value);
    if (ev.target.value.length > 0 && str == true) {
      this.isInvoiceValid = true;
    } else {
      this.isInvoiceValid = false;
    }
  }

  // Sejal - 03-03-2022
  getManufacturerList(ev: any) {
    // this.manuname = ev.target.value.toLowerCase();
    // this.ebmanulist = [
    //   { id: 1, text: "AMO MOBILITY SOLUTIONS PRIVATE LIMITED" }
    // ]

    // // Sejal - 03-03-2022 
    // // this.EBManufacturerList = this.ebmanulist.filter((option) => {
    // //   return (
    // //     option.text.toLowerCase().includes(this.manuname)
    // //   );
    // // });
    // this.EBManufacturerList = this.ebmanulist;

    if (ev.target.value.length < 3 || ev.key == "Backspace") {
      this.allriskmanumod = [];
      this.quoteDetails.model = "";
      this.idv = "";
      this.quoteDetails.SumInsuredIDV = 0;
    } else {
      this.manuname = ev.target.value.toLowerCase();
      this.datalistname = ev.target.value;

      if (
        this.ManufactureModelList != undefined &&
        this.ManufactureModelList != null
      ) {
        if (this.ManufactureModelList.length != 0) {
          this.ManufactureModelList = this.api.manufactureListNew(
            this.productCode
          );
        } else {
          this.ManufactureModelList = this.api.manufactureListNew(
            this.productCode
          );
        }
      } else {
        this.ManufactureModelList = this.api.manufactureListNew(
          this.productCode
        );
      }

      // for (let element of this.ManufactureModelList) {
      //   this.manulist.push(element.make);
      // }
      
      // this.EBVehicleModel = "";
      if (ev.target.value.length <= 3) {
        this.allriskmanumod = this.ManufactureModelList.filter((option) => {
          return (
            option.make
              .substring(0, 3)
              .toLowerCase()
              .includes(this.manuname)
          );
        });

        // for (let element of this.manumod) {
        //   this.manulist = element.make;
        // }
      } else {
        this.allriskmanumod = this.ManufactureModelList.filter((option) => {
          return (
            option.make.toLowerCase().includes(this.manuname)
          );
        });

        // for (let element of this.manumod) {
        //   this.manulist = element.make;
        // }
      }

      //console.info(this.allriskmanumod, this.allriskmanumod[0].make);
	let test = [];
      for (let element of this.allriskmanumod) {
        test.push(element.make);
      }
      console.log(test);
      this.allriskManufacturer = [...new Set(test)]; 
      console.log(this.allriskManufacturer);
      // let selectedManu = this.allriskmanumod[0].make;
      // if ((ev.target.value.length < 4 && selectedManu != this.quoteDetails.Manufacturer ) || this.cs.isUndefineORNull(this.quoteDetails.Manufacturer)) {
      //   this.quoteDetails.model = "";
      //   this.idv = "";
      //   this.quoteDetails.SumInsuredIDV = 0;
      // }

      // if (this.cs.isUndefineORNull(this.quoteDetails.Manufacturer)) {
      //   this.quoteDetails.model = "";
      // }

      // if (this.manumod.length == 0) {
      //   this.manumod = this.ManufactureModelList.filter((option) => {
      //     return (
      //       option.make.toLowerCase().includes(this.manuname)
      //     );
      //   });
      // }

      // if (this.manumod.length == 0) {
      //   this.manuerror = true;
      // } else {
      //   this.manuerror = false;
      // }
    }
  }

  // Sejal - 02-03-2022
  getEBModelList(ev: any) {
    this.modelname = ev.target.value.toLowerCase();
    // this.allriskEBModel = [
    //   { id: 1, text: "INSPIRER" },
    //   { id: 2, text: "JAUNTY" },
    //   { id: 3, text: "SPIN LED" }
    // ]
    // ["INSPIRER","JAUNTY","SPIN LED"];
    // Sejal - 03-03-2022 
    this.allriskEBModel = this.allriskmanumod;
    this.EBVehicleModel = this.allriskEBModel.filter((option) => {
      return (
        option.model.toLowerCase().includes(this.modelname)
        //&& option.vehicleclasscode == this.classCode
      );
    });
    
    // localStorage.setItem("SelectedEVModel", JSON.stringify(this.EBVehicleModel));
    // console.info(this.EBVehicleModel);
    // if (this.cs.isUndefineORNull(this.quoteDetails.SumInsuredIDV)) {
      // this.idv = "";
      // this.getEVSuminsured(ev, this.EBVehicleModel);
    // }
  }

  // getModelandIDV(ev: any) {
  //   if (this.cs.isUndefineORNull(this.quoteDetails.model)) {
  //     this.getEBModelList(ev);
  //     this.getEVSuminsured(ev);
  //   } else {
  //     this.getEVSuminsured(ev);
  //   }
  // }

  fetch(ev: any) {
    this.policyno = ev.policyNo;
    this.customerId = ev.pfCustomerID;
    this.showPolicy();
  }

  // Download PDF Policy
  showPolicy() {
    // this.cs.loaderStatus = true;
    this.id = Guid.raw();
    this.spinloaderpolicy = true;
    this.cs
      .get(
        "PolicySchedule/GeneratePolicySchedule?policyNo=" +
        this.policyno +
        "&correlationID=" +
        this.id +
        "&customerId=" +
        this.customerId
      )
      .subscribe(
        (res) => {
          this.pdfResponse = res;
          if (
            this.pdfResponse != null &&
            this.pdfResponse != undefined &&
            this.pdfResponse.status == "SUCCESS"
          ) {
            var fileName = this.policyno.split("/").join("_") + ".pdf";
            this.cs.save(
              "data:application/pdf;base64," + this.pdfResponse.buffer,
              fileName
            );
            // this.cs.loaderStatus = false;
            this.spinloaderpolicy = false;
          } else {
            swal({
              text: "Try fetching Policy pdf from My policies dashboard after sometime",
            });
            let errorbody = {
              RequestJson: this.policyno,
              ResponseJson: JSON.stringify(res),
              ServiceURL:
                "PolicySchedule/GeneratePolicySchedule?policyNo=" +
                this.policyno,
              CorrelationID: this.id,
            };
            this.api.adderrorlogs(errorbody);
          }
          this.spinloaderpolicy = false;
          // this.cs.loaderStatus = false;
        },
        (err) => {
          swal({
            text: "Try fetching Policy pdf from My policies dashboard after sometime",
          });
          // this.cs.loaderStatus = false;
          this.spinloaderpolicy = false;
          document.getElementById("myNav4").style.height = "0%";
        }
      );
  }

  createAllRiskBizToken(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.get("middleware/token?Scope=14").subscribe((res: any) => {
        localStorage.setItem("allRiskbizToken", JSON.stringify(res));
        resolve();
      });
    }).catch((err: any) => {
      swal({ text: err });
      this.cs.loaderStatus = false;
    });
  }

  scpaForSAOD(ev: any){
    if (ev.target.checked && this.showSCPAinNysa) {
      this.isSCPARequired = true;
      localStorage.setItem('isSCPARequired', JSON.stringify(this.isSCPARequired));
      this.quoteForSCPA();
    } else {
      this.isSCPARequired = false;
      this.totalPremiumTP = this.totalPremium; 
      console.log(this.quoteResp, this.calculatedQuote);
      localStorage.setItem('isSCPARequired', JSON.stringify(this.isSCPARequired));
    }
  }

  quoteForSCPA() {
    let scpaData = JSON.parse(localStorage.getItem('scpaData'));
    console.log(this.quoteDetails.PolicyStartDate, this.quoteDetails.PolicyEndDate);
    let id = Guid.raw();
    let body = {
      "PolicyStartDate": moment(this.quoteDetails.PolicyStartDate).format("YYYY-MM-DD"), //"2020-08-13T00:00:00"
      "PolicyEndDate": moment(this.quoteDetails.PolicyEndDate).format("YYYY-MM-DD"), //"2021-08-12T00:00:00",
      "policyTenure": 1,
      "preExistingIllness": false,
      "Loading": 0.0,
      "Discount": this.quoteDetails.OtherDiscount,
      "InwardDate": moment(this.quoteDetails.PolicyStartDate).format("YYYY-MM-DD"), // Need to check,
      // "InsuredDOB": "1991-08-10T00:00:00", // Need to check,
      "StampDutyChargable": false,
      "ProductDetails": {
        "ProductCode": commonData.scpaProductCode,
        "PlanDetails": [
          {
            "PlanCode": commonData.scpaPlancode,
            "RiskSIComponentDetails": [
              {
                "RiskSIComponent": "Person",
                "CoverDetails": [
                  {
                    "CoverCode": null,
                    "CoverName": "Compulsory Personal Accident Owner Driver Cover",
                    "CoverSI": "1500000.0",
                    "Column1": "",
                    "Column2": null,
                    "Column3": null,
                    "Column4": null,
                    "Column5": null
                  }
                ]
              }
            ]
          }
        ]
      },
      "TaxDetails": {
        "GstServiceDetails": [
          {
            "ILGICStateName": this.quoteDetails.GSTToState,		//"MAHARASHTRA", For Production
            "PartyStateName": this.quoteDetails.GSTToState,//"KERALA"
          }
        ]
      },
      "DealId": scpaData.scpaDeal,
      "BusinessType": null,
      "IsRegisteredCustomer": "false",
      "CorrelationId": id
    }

    let str = JSON.stringify(body);
    localStorage.setItem('reqSAODSCPA', str);
    this.cs.postSCPA('/quote/create', str).then((res:any) => {
      console.log("Response", res);
      localStorage.setItem('resSAODSCPA', JSON.stringify(res));
      this.scpaResponse = res;
      this.totalPremiumTP = this.totalPremiumTP + res.totalPremium;
      this.isSCPARequired = true;
      localStorage.setItem('isSCPARequired', JSON.stringify(this.isSCPARequired));
      // this.saveofflineSCPAQuote();
    })
  }

  saveofflineSCPAQuote() {
    let body: any;
    let URL: any;
    let quoteReq: any;
    let quoteRes: any;
    
    quoteReq = JSON.parse(localStorage.getItem("reqSAODSCPA"));
    quoteRes = JSON.parse(localStorage.getItem("resSAODSCPA"));
    localStorage.setItem('bundleIDSAOD', JSON.stringify(this.saodBundleID));
    body = {
      quoteRQ: quoteReq,
      quoteRS: quoteRes,
      IsDigitalPOS: this.IsPOSTransaction_flag, // Added digitalPOS parameter with value true or false based on value in localStorage
      BundleID: this.saodBundleID
    };

    if (
      this.paramDataValues.iPartnerLogin.isSubagent != null ||
      this.paramDataValues.iPartnerLogin.isSubagent != undefined
    ) {
      body.IsSubagent = true;
      body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
    }

    if (this.showsaod == true) {
      body.IsStandaloneOD = this.showsaod;
      let standaloneODRQ = {
        TPStartDate: this.quoteDetails.TPStartDate,
        TPEndDate: this.quoteDetails.TPEndDate,
        TPPolicyNo: this.quoteDetails.TPPolicyNo,
        TPInsurerName: this.quoteDetails.TPInsurerName,
      };
      body.standaloneODRQ = standaloneODRQ;
    }

    // if (this.productCode == "2312") {
      URL = "Quote/twev/SaveQuote";
    // }
    // if (this.productCode == "2311") {
    //   URL = "Quote/fw/SaveQuote";
    // }
    let str = JSON.stringify(body);
    this.cs.post(URL, str).then((res: any) => {
      this.saveQuote = res;
    });
  }

  selectSubLoc() {
    console.log(this.subLocation);
    localStorage.setItem('SubLocation', JSON.stringify(this.subLocation));
  }

  getSubLocationCities(ev?) {
    if (ev.target.value.length < 3 || ev.key == "Backspace") {
    } else {
      let postObj = ev.target.value;
      this.api.getSubLocation(postObj).subscribe((res: any) => {
        this.sublocations = res;
      });
    }
  }
}
