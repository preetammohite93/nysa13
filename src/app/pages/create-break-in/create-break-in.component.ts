import { Component, OnInit } from "@angular/core";
import { ApiServiceService } from "src/app/services/api-service.service";
import { CommonService } from "src/app/services/common.service";
import swal from "sweetalert";
import { Guid } from "guid-typescript";
import { Router } from "@angular/router";

@Component({
  selector: "app-create-break-in",
  templateUrl: "./create-break-in.component.html",
  styleUrls: ["./create-break-in.component.css"],
})
export class CreateBreakInComponent implements OnInit {
  RegistrationNumber: any;
  SelfInspection = "No";
  TypeofTransaction = "New";
  TypeofBreakIN = "Break-in Policy lapse";
  PreviousNCB = "0";
  IsNCBSow = false;
  PreviousPolClaim = "No";
  RTOCity: any;
  Manufacturer: any;
  Model: any;
  ManufacturYear = 2021;
  EngineNo: any;
  ChassisNo: any;
  customerName: any;
  primMobileNo: any;
  altMobileNo: any;
  pincode: any;
  city: any;
  address: any;
  EmpId: any;
  EmpName: any;
  EmpContactNo: any;
  mobNumberPattern = "^(0/91)?[6-9][0-9]{9}$";
  pincodeDetails: any;
  stateName: any;
  authtoken: any;
  dealDetails: any;
  dealDetailsfromIM: any;
  vehicleType: any;
  manufactureName: any;
  RTODetails: any;
  showVehicleType = false;
  VehcleType = "";
  rtoList: any;
  manuModelList: any;
  modelList = [];
  productCode = "2312";
  policySubType = "1";
  classCode = "37";
  sublocation: any;
  sublocations: any;
  modelcode: any;
  uniqueManufacture: any;
  isSiteDown = false;
  IsPOSTransaction_flag: boolean = false;
  posDealID: string;
  deal_Id: string;
  ManufactureModelList: any = [];

  constructor(
    public api: ApiServiceService,
    public cs: CommonService,
    public router: Router
  ) {
    if (localStorage.getItem("IsPOSTransaction")) {
      this.IsPOSTransaction_flag = JSON.parse(
        localStorage.getItem("IsPOSTransaction")
      );
    }   
  }

  ngOnInit() {
    this.authtoken = JSON.parse(localStorage.getItem("AuthToken"));
    let data = JSON.parse(localStorage.getItem("paramData"));
    this.productCode = data.iPartnerLogin.product;
    this.ManufactureModelList = this.api.manufactureListNew(this.productCode);
    this.checkCrawler();
  }

  validatereg(ev: any) {
    let value = ev.target.value;
    let match = "NEW";
    let maxlength = 5;
    let regTest = /^[0-9]+$/.test(ev.target.value);
    let regTest1 = /^[a-zA-Z]+$/.test(ev.target.value);
    if (value.length < maxlength && value != match) {
      swal({ text: "Kindly provide correct vehicle registration number" }).then(
        () => {
          document.getElementById("txtreg").focus();
        }
      );
    } else if (regTest || regTest1) {
      swal({
        text: "Enter registration number with combination of alphabets and numbers",
      }).then(() => {
        document.getElementById("txtreg").focus();
      });
    } else {
      this.getVehicleDetails();
    }
  }

  // Check crawler enable or not
  checkCrawler() {
    // //CreateToken + GetApplicationAccess merging code - Sejal
    if (!this.cs.isUndefineORNull(localStorage.getItem("AuthToken"))) {
      let encryptedTokenaccess = JSON.parse(localStorage.getItem("AuthToken"));
      let Parsedtokenaccess: any = this.api.decrypt(
        encryptedTokenaccess.applicationAccess
      );
      let tokenaccess = JSON.parse(Parsedtokenaccess);
      this.cs.isPlutusEnable = tokenaccess.isEnablePlutus;
      this.isSiteDown = tokenaccess.isSiteDown;
    }

    // this.cs.get("Access/GetApplicationAccess").subscribe((res: any) => {
    //   console.log("Response", res);
    //   localStorage.setItem("AccessFor", JSON.stringify(res));
    //   this.isSiteDown = res.isSiteDown;
    // });
  }

  getVehicleDetails() {
    this.cs.loaderStatus = true;
    this.api.getRTODetails(this.RegistrationNumber).subscribe((res: any) => {
      if (res.statusType == "SUCCESS") {
        this.showVehicleType = false;
        this.RTODetails = res;
        this.RTOCity = res.rto_name;
        this.Manufacturer = res.pf_manufactur;
        this.Model = res.pf_model;
        // res.isNewRTOVendorCall
        let engNO = res.isZoopRTOVendorCall
          ? this.cs.decryptUsingTripleDES(res.eng_no)
          : res.eng_no;
        let chassNO = res.isZoopRTOVendorCall
          ? this.cs.decryptUsingTripleDES(res.chasi_no)
          : res.chasi_no;
        this.EngineNo = res.isZoopRTOVendorCall
          ? engNO.replace("**", "")
          : engNO;
        this.ChassisNo = res.isZoopRTOVendorCall
          ? chassNO.replace("**", "")
          : chassNO;
        this.vehicleType = res.fla_vh_class_desc;
        if (
          this.vehicleType == "Two Wheeler" ||
          this.vehicleType == "TWO WHEELER"
        ) {
          this.productCode = "2312";
          this.policySubType = "1";
          this.classCode = "37";
        } else {
          this.productCode = "2311";
          this.policySubType = "2";
          this.classCode = "45";
        }
        let date = res.regn_dt.split("-");
        this.ManufacturYear = Number(date[2]);
        this.cs.loaderStatus = false;

        /**
         * digitalPOS change
         * call getDeal and getDealDetails function only for nysa not for POS
         * date :- 15-06-2021
         */
        if (this.IsPOSTransaction_flag == true) {
        } else {
          // GetDealFromIM? changes - Sejal - 24-01-2022
          let data;
          if (!this.cs.isUndefineORNull(localStorage.getItem("DealID"))) {
            let demo = JSON.parse(localStorage.getItem("DealID"));
            data = demo.split("/");
          }
          if (!this.cs.isUndefineORNull(localStorage.getItem("DealID"))) {
            if (data[0] == "DL-3005" && res.vh_class_desc == "Private Car") {
              this.productCode = "2311";
              this.getDeal().then((res: any) => {
                this.getDealDetails();
              });
              // swal({ text: "You have entered four wheeler registration number but you have selected two wheeler" }).then(
              //   () => {
              //     document.getElementById("txtreg").focus();
              //   }
              // );
            }else if (data[0] == "DL-3001" && res.vh_class_desc == "Two Wheeler") {
              this.productCode = "2312";
              this.getDeal().then((res: any) => {
                this.getDealDetails();
              });
              // swal({ text: "You have entered two wheeler registration number but you have selected four wheeler" }).then(
              //   () => {
              //     document.getElementById("txtreg").focus();
              //   }
              // );
            }else{
              this.getDeal().then((res: any) => {
                this.getDealDetails();
              });
            }
          } else {
            this.getDeal().then((res: any) => {
              this.getDealDetails();
            });
          }
        }
      } else {
        this.cs.loaderStatus = false;
        this.showVehicleType = true;
        swal({
          closeOnClickOutside: false,
          text: "Data not fetched from Carinfo, continue with manual data entry",
        });
        this.RTOCity = "";
        this.Manufacturer = "";
        this.Model = "";
        this.EngineNo = "";
        this.ChassisNo = "";
      }
    });

    this.api.getiibServices(this.RegistrationNumber).subscribe((res: any) => {
      if (res.isClaimMade == "Y") {
        this.IsNCBSow = false;
        this.PreviousPolClaim = "Yes";
      } else {
        this.IsNCBSow = true;
        this.PreviousPolClaim = "No";
      }
    });
  }

  getSubLocationCity(ev: any) {
    this.api.getSubLocation(ev.target.value).subscribe((res: any) => {
      this.sublocations = res;
    });
  }

  getManualData(ev: any) {
    // GetDealFromIM? changes - Sejal - 01-02-2022
    let data;
    if (!this.cs.isUndefineORNull(localStorage.getItem("DealID"))) {
      let demo = JSON.parse(localStorage.getItem("DealID"));
      data = demo.split("/");
      if (data[0] == "DL-3001" && ev.target.value == "TW") {
        this.productCode = "2312";
        this.policySubType = "1";
        this.classCode = "37";
        this.getDeal().then((res: any) => {
          this.getDealDetails();
        });
      } else if (data[0] == "DL-3005" && ev.target.value == "FW") {
        this.productCode = "2311";
        this.policySubType = "2";
        this.classCode = "45";
        this.getDeal().then((res: any) => {
          this.getDealDetails();
        });
      }
    }

    // if (ev.target.value == 'TW') {
    //   this.productCode = '2312';
    //   this.policySubType = "1";
    //   this.classCode = "37";
    // } else {
    //   this.productCode = '2311';
    //   this.policySubType = "2";
    //   this.classCode = "45";
    // }
  }

  getRTOList(ev: any) {
    if (ev.target.value.length < 3 || ev.key == "Backspace") {
    } else {
      this.RTOCity = ev.target.value;
      this.cs
        .get(
          "RTO/GetRTOList?rtoName=" +
            this.RTOCity +
            "&classCode=" +
            this.classCode
        )
        .subscribe((res: any) => {
          this.rtoList = res;
        });
    }

    /**
     * digitalPOS change
     * callcall getDeal and getDealDetails function only for nysa not for POS
     * date :- 15-06-2021
     */
    if (this.IsPOSTransaction_flag == true) {
    } else {
      // GetDealFromIM? changes - Sejal - 24-01-2022
      if (this.cs.isUndefineORNull(localStorage.getItem("DealID"))) {
        this.getDeal().then((res: any) => {
          this.getDealDetails();
        });
      }
    }
  }

  // // API Independent for production
  // getManuModList(ev: any) {
  //   if (ev.target.value.length < 3 || ev.key == "Backspace") {
  //   } else {
  //     this.manufactureName = ev.target.value;
  //     this.cs
  //       .get(
  //         "ManufactureNModel/GetManufactureNModelList?manufactureName=" +
  //           this.manufactureName +
  //           "&classCode=" +
  //           this.classCode
  //       )
  //       .subscribe((res: any) => {
  //         this.manuModelList = res;
  //         let man = [];
  //         this.manuModelList.forEach((element: any) => {
  //           man.push(element.manufacturer);
  //           this.uniqueManufacture = new Set(man);
  //         });
  //         localStorage.removeItem("rtoDetail");
  //       });
  //   }
  // }

  // API dependent for sanity
  /**
   * ManufactureNModel/GetManufactureNModelList api function commented and Manufacture Model List is being fetched from localStorage
   * Author :- Sumit
   * date :- 22-01-2022
   */
  man: any = [];
  getManuModList(ev: any) {
    if (this.productCode == "2312" || this.productCode == "2320") {
      this.classCode = "37";
    } else if (this.productCode == "2311" || this.productCode == "2319") {
      this.classCode = "45";
    }

    if (ev.target.value.length < 3 || ev.key == "Backspace") {
      this.manuModelList = [];
      this.man = [];
    } else {
      this.manufactureName = ev.target.value;

      if (localStorage.getItem("rtoDetail")) {
        localStorage.removeItem("rtoDetail");
      }

      if (
        this.ManufactureModelList != undefined &&
        this.ManufactureModelList != null
      ) {
        if (this.ManufactureModelList.length != 0) {
          this.ManufactureModelList =
            // this.api.getManufactureModelListFromLocalStorage();
            this.api.manufactureListNew(this.productCode);
        } else {
          this.ManufactureModelList = this.api.manufactureListNew(
            this.productCode
          );
        }
      } else {
        this.ManufactureModelList =
          // this.api.getManufactureModelListFromLocalStorage();
          this.api.manufactureListNew(this.productCode);
      }

      this.manuModelList = this.ManufactureModelList.filter((option) => {
        return (
          option.vehiclemodel.toLowerCase().includes(this.manufactureName) &&
          option.vehicleclasscode == this.classCode
        );
      });

      this.manuModelList.forEach((element: any) => {
        this.man.push(element.manufacturer);
        this.uniqueManufacture = new Set(this.man);
      });
    }
  }

  getModel(model: any) {
    this.modelcode = model.vehiclemodelcode;
    if(this.cs.isUndefineORNull(this.modelcode)){
      swal({
        text: "Kindly select proper manufacture and model",
      });
      this.Model = "";
    }else{
      this.api
      .getModelName(this.modelcode, this.classCode)
      .subscribe((res: any) => {
        this.Model = res.model;
      });
    }    
  }

  validateEng(ev: any) {
    if (!this.cs.isUndefineORNull(ev.target.value)) {
      if (ev.target.value.length < 5) {
        swal({
          text: "Engine Number cannot be less than 5 digit, enter complete engine number",
        });
        this.EngineNo = "";
      } else {
        this.EngineNo = ev.target.value;
      }
    } else {
      swal({ text: "Kindly insert engine Number" });
      this.EngineNo = "";
    }
  }

  transactionChange(ev: any) {
    if (ev.target.value == "Rollover") {
      let length = this.ChassisNo.length;
      if (length < 5) {
        swal({
          text: "Chassis Number cannot be less than 5 digit, enter complete Chassis number",
        });
        this.ChassisNo = "";
      } else {
        this.ChassisNo = this.ChassisNo;
      }
    } else if (ev.target.value == "New" && this.policySubType == "1") {
      if (length < 11) {
        swal({
          text: "Chassis Number cannot be less than 11 digit, enter complete Chassis number",
        });
        this.ChassisNo = "";
      } else {
        this.ChassisNo = this.ChassisNo;
      }
    } else if (ev.target.value == "New" && this.policySubType == "2") {
      if (length < 17) {
        swal({
          text: "Chassis Number cannot be less than 17 digit, enter complete Chassis number",
        });
        this.ChassisNo = "";
      } else {
        this.ChassisNo = this.ChassisNo;
      }
    } else {
      this.ChassisNo = this.ChassisNo;
    }
  }

  validateChassis(ev: any) {
    if (!this.cs.isUndefineORNull(ev.target.value)) {
      if (
        ev.target.value.length < 11 &&
        this.policySubType == "1" &&
        this.TypeofTransaction == "New"
      ) {
        swal({
          text: "Chassis Number cannot be less than 11 digit, enter complete chassis number",
        });
        this.ChassisNo = "";
      } else if (
        ev.target.value.length < 17 &&
        this.policySubType == "2" &&
        this.TypeofTransaction == "New"
      ) {
        swal({
          text: "Chassis Number cannot be less than 17 digit, enter complete chassis number",
        });
        this.ChassisNo = "";
      } else if (
        ev.target.value.length < 5 &&
        this.TypeofTransaction == "Rollover"
      ) {
        swal({
          text: "Chassis Number cannot be less than 17 digit, enter complete chassis number",
        });
        this.ChassisNo = "";
      } else {
        this.ChassisNo = ev.target.value;
      }
    } else {
      swal({ text: "Kindly insert Chassis Number" });
      this.ChassisNo = "";
    }
  }

  showNCB(ev: any) {
    if (ev.target.value == "Yes") {
      this.IsNCBSow = false;
    } else {
      this.IsNCBSow = true;
    }
  }

  mobileValidate(input: any, mobile: any) {
    let isNumberValidate = this.cs.mobileValidation(mobile);
    if (isNumberValidate) {
      if (input == "primMobileNo") {
        this.primMobileNo = mobile;
      } else if (input == "altMobileNo") {
        this.altMobileNo = mobile;
      } else {
        this.EmpContactNo = mobile;
      }
    } else {
      input = "";
      swal({
        closeOnClickOutside: false,
        text: "Mobile number entered is not valid.",
      });
    }
  }

  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  nameValidate(ev: any) {
    if (ev.target.value.length < 3) {
      this.customerName = "";
      swal({ text: "Length can not less than 3" });
    } else {
      this.customerName = this.customerName;
    }
  }

  getPincodeDetails(ev: any) {
    if (ev.target.value.length < 6 || ev.key == "Backspace") {
      this.city = "";
      this.stateName = "";
      swal({ text: "Please enter valid pincode" });
    } else {
      this.cs
        .get("Pincode/GetPincodeList?pinCode=" + ev.target.value)
        .subscribe((res: any) => {
          this.pincodeDetails = res;
          this.stateName = this.pincodeDetails[0].stateName;
          this.city = res[0].cityName;
        });
    }
  }

  getDeal(): Promise<void> {
    return new Promise((resolve) => {
      this.api
        .getDeal(this.authtoken.username, this.productCode)
        .subscribe((res: any) => {
          this.dealDetails = res;
          //localStorage.setItem("DealID", JSON.stringify(this.dealDetails));
          let dealid = this.dealDetails.dealID;
          localStorage.setItem("DealID", JSON.stringify(dealid));
          resolve();
        });
    });
  }

  getDealDetails() {
    this.api.getDealDetails(this.dealDetails.dealID).subscribe((res: any) => {
      if (res.status == "FAILED") {
        swal({ text: "Deal Details not found" });
        this.cs.loaderStatus = false;
      } else {
        this.dealDetailsfromIM = res;
        this.EmpId = this.dealDetailsfromIM.hR_REF_NO;
        this.EmpName = this.dealDetailsfromIM.employeeName;
        this.cs.loaderStatus = false;
      }
    });
  }

  Validate() {
    if (this.cs.isUndefineORNull(this.RegistrationNumber)) {
      swal({ text: "Registration Number can not be blank" });
    } else if (this.cs.isUndefineORNull(this.RTOCity)) {
      swal({ text: "RTO City can not be blank" });
    } else if (this.cs.isUndefineORNull(this.Manufacturer)) {
      swal({ text: "Vehicle Manufacture can not be blank" });
    } else if (this.cs.isUndefineORNull(this.Model)) {
      swal({ text: "Vehicle Model can not be blank" });
    } else if (this.cs.isUndefineORNull(this.EngineNo)) {
      swal({ text: "Engine Number can not be blank" });
    } else if (this.cs.isUndefineORNull(this.ChassisNo)) {
      swal({ text: "Chassis Number can not be blank" });
    } else if (this.cs.isUndefineORNull(this.ManufacturYear)) {
      swal({ text: "Manufacturing Year Number can not be blank" });
    } else if (this.cs.isUndefineORNull(this.PreviousPolClaim)) {
      swal({ text: "Previous Claim can not be blank" });
    } else if (this.cs.isUndefineORNull(this.PreviousNCB)) {
      swal({ text: "Previous NCB can not be blank" });
    } else if (this.cs.isUndefineORNull(this.TypeofBreakIN)) {
      swal({ text: "Type of BreakIN can not be blank" });
    } else if (this.cs.isUndefineORNull(this.SelfInspection)) {
      swal({ text: "Self Inspection can not be blank" });
    } else if (this.cs.isUndefineORNull(this.TypeofTransaction)) {
      swal({ text: "Type of Transaction can not be blank" });
    } else if (this.cs.isUndefineORNull(this.customerName)) {
      swal({ text: "Name can not be blank" });
    } else if (this.cs.isUndefineORNull(this.primMobileNo)) {
      swal({ text: "Prim. Mobile Number can not be blank" });
    } else if (this.cs.isUndefineORNull(this.altMobileNo)) {
      swal({ text: "Alt. Mobile Number can not be blank" });
    } else if (this.cs.isUndefineORNull(this.pincode)) {
      swal({ text: "Pincode can not be blank" });
    } else if (this.cs.isUndefineORNull(this.city)) {
      swal({ text: "City can not be blank" });
    } else if (this.cs.isUndefineORNull(this.address)) {
      swal({ text: "Address can not be blank" });
    } else if (this.cs.isUndefineORNull(this.sublocation)) {
      swal({ text: "Sublocation can not be blank" });
    } else {
      this.createBreakIn();
    }
  }

  createBreakIn() {
    this.cs.loaderStatus = true;
    let correlationId = Guid.raw();
    /**
     * digitalPOS Change
     * Setting the deal id on basis of POS or nysa
     */
    if (this.IsPOSTransaction_flag == true) {
      this.posDealID = JSON.parse(localStorage.getItem("DealID"));
      this.deal_Id = this.posDealID;
    } else {
      this.deal_Id = this.dealDetails.dealID;
    }

    let body = {
      PolicySubType: this.policySubType,
      customerName: this.customerName,
      customerAddress: this.address,
      state: this.stateName,
      city: this.city,
      pincode: Number(this.pincode),
      telephoneNumber: Number(this.altMobileNo),
      mobileNumber: Number(this.primMobileNo),
      typeVehicle: this.policySubType == "1" ? "MOTORCYCLE" : "PRIVATE CAR",
      vehicleMake: this.Manufacturer,
      vehicleModel: this.Model,
      registrationNo: this.RegistrationNumber,
      engineNo: this.EngineNo,
      chassisNo: this.ChassisNo,
      manufactureYear: Number(this.ManufacturYear),
      previousInsurer: null,
      prevYearsPolicyNo: null,
      prevYearsClaimStatus: null,
      prevPolicyNCB: Number(this.PreviousNCB),
      selfInspection: this.SelfInspection,
      subLocation: this.sublocation,
      appointmentDateTime: null,
      inspectionType: this.TypeofTransaction,
      breakInDays: 0,
      subInspectionType: "",
      salesRemarks: "Pre-Approved BreakIN",
      breakInType: this.TypeofBreakIN,
      addOnCoverType: "",
      basicODPremium: 0,
      distributorName: "Emmet",
      dealId: this.deal_Id,
      correlationId: correlationId,
    };

    let str = JSON.stringify(body);
    this.cs.post("BreakIn/CreateBreakIn", str).then((res: any) => {
      if (res.status == "Failed") {
        swal({ text: res.message }).then(() => {
          document.getElementById("txtreg").focus();
        });
      } else {
        swal({
          text:
            "Your BreakIn ID is " +
            res.brkId +
            ". Please check status in My Break IN ",
        }).then(() => {
          this.router.navigateByUrl("quote");
        });
      }
      this.cs.loaderStatus = false;
    });
  }
}
