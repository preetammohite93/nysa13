import { Component, OnInit } from "@angular/core";
import swal from "sweetalert";
import * as moment from "moment";
import { commonData } from "../../commonData/commonData";
import { CommonService } from "src/app/services/common.service";
import { Router, ActivatedRoute } from "@angular/router";
import { PaymentURL } from "src/environments/env";
declare var Razorpay: any;
import { SendMessageService } from "src/app/services/send-message.service";

@Component({
  selector: "app-saved-quotes",
  templateUrl: "./saved-quotes.component.html",
  styleUrls: ["./saved-quotes.component.css"],
})
export class SavedQuotesComponent implements OnInit {
  startDate: any;
  todayDate: any;
  paramDataValues: any;
  policyType: any;
  productCode: any;
  type: any;
  product: any;
  ProductType: any;
  norecordflag: any;
  savedQuote: any;
  policyno: any;
  corelationID: any;
  totalPremium: any;
  proposalNo: any;
  customerid: any;
  bankingidData: any;
  dealID: any;
  orderId: any;
  options: any;
  PFPayReq: any;
  PFPayRes: any;
  proposalRQ: any;
  proposalRS: any;
  totalTax: any;
  term: any;
  payButton: boolean = false;
  page: any;
  PayKey: any;
  proposalReq: any;
  memmaxDOB: any;
  quoteRQ: any;
  memminDOB: any;
  spinloader: boolean = false;
  IsPOSTransaction_flag = false;

  bundleId: any;

  pendingpayment = {
    FromDate: "",
    ToDate: "",
  };

  totalAmount: any;

  constructor(
    public router: Router,
    public cs: CommonService,
    public activeRoute: ActivatedRoute,
    private messageService: SendMessageService
  ) {}

  ngOnInit() {
    this.memmaxDOB = new Date();
    this.memminDOB = new Date();
    this.memmaxDOB.setDate(this.memmaxDOB.getDate() + 0);
    this.memminDOB.setDate(this.memminDOB.getDate() - 6);

    this.PayKey = commonData.PayKey;

    this.ProductType = "1";
    this.startDate = moment(new Date()).format("YYYY-MM-DD");
    this.todayDate = moment(new Date()).format("YYYY-MM-DD");
    let startDate = moment(new Date()).format("YYYY-MM-DD");
    this.pendingpayment.FromDate = startDate;
    this.pendingpayment.ToDate = startDate;

    this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
    this.policyType = this.paramDataValues.iPartnerLogin.policy;
    this.productCode = this.paramDataValues.iPartnerLogin.product;
    if (this.policyType == "NEW") {
      this.type = "New";
    } else {
      this.type = "Rollover";
    }
    if (this.productCode == "2312") {
      this.product = "Two Wheeler";
    } else {
      this.product = "Four Wheeler";
    }

    if (localStorage.getItem("IsPOSTransaction")) {
      this.IsPOSTransaction_flag = JSON.parse(
        localStorage.getItem("IsPOSTransaction")
      );
    }
  }

  vehicletype(ev: any) {
    
    if (ev.target.id == "savedselectVehicle_twoWheeler") {
      this.ProductType = "1";
      this.savedQuote = "";
      this.term = "";
      localStorage.setItem('productType', JSON.stringify(this.ProductType))
    } else if (ev.target.id == "savedselectVehicle_fourWheeler") {
      this.ProductType = "2";
      this.savedQuote = "";
      this.term = "";
      localStorage.setItem('productType', JSON.stringify(this.ProductType))
    } else if (ev.target.id == "savedselectVehicle_twoWheelerTP") {
      this.ProductType = "9";
      this.savedQuote = "";
      localStorage.setItem('productType', JSON.stringify(this.ProductType))
    } else if (ev.target.id == "savedselectVehicle_fourWheelerTP") {
      this.ProductType = "10";
      this.savedQuote = "";
      localStorage.setItem('productType', JSON.stringify(this.ProductType))
    } else if (ev.target.id == "savedselectVehicle_twoWheelerOD") {
      this.ProductType = "12";
      this.savedQuote = "";
      localStorage.setItem('productType', JSON.stringify(this.ProductType))
    } else if (ev.target.id == "savedselectVehicle_fourWheelerOD") {
      this.ProductType = "13";
      this.savedQuote = "";
      localStorage.setItem('productType', JSON.stringify(this.ProductType))
    } else if (ev.target.id == "savedselectVehicle_allriskElectricBike") {
      this.ProductType = "11";   // All Risk Electric Bike - Sejal - 5-4-2022
      this.savedQuote = "";
      localStorage.setItem('productType', JSON.stringify(this.ProductType))
    }
  }

  validate() {
    if (
      this.pendingpayment.FromDate == null ||
      this.pendingpayment.FromDate == undefined ||
      this.pendingpayment.FromDate == ""
    ) {
      swal({ title: "Alert!", text: "Kindly insert from date" });
    } else if (
      this.pendingpayment.ToDate == null ||
      this.pendingpayment.ToDate == undefined ||
      this.pendingpayment.ToDate == ""
    ) {
      swal({ title: "Alert!", text: "Kindly insert to date" });
    } else {
      this.getpolicies();
    }
  }

  fetch(ev: any) {
    this.policyno = ev.policyNo;
    this.corelationID = ev.corelationID;
    this.totalPremium = ev.totalPremium;
    this.proposalNo = ev.policyNo;
    this.customerid = ev.pfCustomerID;
  }

  // getPolicies
  getpolicies() {
    let from: any;
    let to: any;
    from = moment(this.pendingpayment.FromDate);
    to = moment(this.pendingpayment.ToDate);
    let diff = to.diff(from, "days");
    this.spinloader = true;
    let body = {
      FromDate: moment(this.pendingpayment.FromDate).format("DD-MMM-YYYY"), //"4/12/2020",
      ToDate: moment(this.pendingpayment.ToDate).format("DD-MMM-YYYY"), // "4/14/2020",
      ProductType: "1",
      SubProductType: this.ProductType,
    };
    let str = JSON.stringify(body);
    this.cs.post("SaveQuote/GetSaveQuote", str).then((res: any) => {
      if (res.status == null) {
        this.norecordflag = true;
      } else {
        this.norecordflag = false;
        if (!this.cs.isUndefineORNull(res.allRiskDetials)) {
          this.savedQuote = res.allRiskDetials;
          console.log("Note :",res.allRiskDetials);
          // for (let data of this.savedQuote) {
          //   console.log('entry', data);
          //   if (data.scpa != null) {
          //     this.totalAmount = parseInt(data.allRisk.totalPremium) + parseInt(data.scpa.totalPremium);
          //   } else {
          //     this.totalAmount = parseInt(data.allRisk.totalPremium);
          //   }
          // }
        } else {
          this.savedQuote = res.policyDetails;
          console.log("Note Policy Details:",res.allRiskDetials);
        }
      }
      this.spinloader = false;
    });
  }

  convertQuote(data: any) {
    let quoteRes, scpaRQ, scpaRS, scpaQuoteRQ, scpaQuoteRS;
    if (this.productCode == commonData.allriskProductCode || this.ProductType == "11") {
      this.bundleId = data.bundleId;
      localStorage.setItem("BundleID", this.bundleId);
      // let allRiskData = data.allRiskDetails;
      // if (!this.cs.isUndefineORNull(allRiskData) && !this.cs.isUndefineORNull(allRiskData.scpa)) {
      if (!this.cs.isUndefineORNull(data) && !this.cs.isUndefineORNull(data.scpa)) {
        this.proposalRQ = JSON.parse(data.allRisk.proposalRQ);
        this.quoteRQ = JSON.parse(data.allRisk.quoteRQ);
        quoteRes = JSON.parse(data.allRisk.quoteRS);
        scpaRQ = JSON.parse(data.scpa.proposalRQ);
        scpaRS = JSON.parse(data.scpa.proposalRS);
        scpaQuoteRQ = JSON.parse(data.scpa.quoteRQ);
        scpaQuoteRS = JSON.parse(data.scpa.quoteRS);
        localStorage.setItem("scpaPropDatareq", JSON.stringify(scpaRQ));
        localStorage.setItem("scpaPropDatares", JSON.stringify(scpaRS));
        localStorage.setItem("scpaQuoteReq", JSON.stringify(scpaQuoteRQ));
        localStorage.setItem("scpaQuoteRes", JSON.stringify(scpaQuoteRS));
      } else {
        this.proposalRQ = JSON.parse(data.allRisk.proposalRQ);
        this.quoteRQ = JSON.parse(data.allRisk.quoteRQ);
        quoteRes = JSON.parse(data.allRisk.quoteRQ);
      }
    } else {
      this.proposalRQ = JSON.parse(data.proposalRQ);
      this.quoteRQ = JSON.parse(data.quoteRQ);
      quoteRes = JSON.parse(data.quoteRS);
    }
    
    if (
      this.ProductType == "1" ||
      this.ProductType == "12" ||
      this.ProductType == "2" ||
      this.ProductType == "13"
    ) {
      if (
        this.proposalRQ.BusinessType == "Roll Over" &&
        this.proposalRQ.PreviousPolicyDetails == null
      ) {
        this.proposalRQ.isNoPrevInsurance = true;
      } else {
        this.proposalRQ.isNoPrevInsurance = false;
      }

      if (
        this.quoteRQ.BusinessType == "Roll Over" &&
        this.quoteRQ.PreviousPolicyDetails == null
      ) {
        this.quoteRQ.isNoPrevInsurance = true;
      } else {
        this.quoteRQ.isNoPrevInsurance = false;
      }
    }

    if (this.ProductType == "2" || this.ProductType == "13") {
      if (this.proposalRQ.IsEMIProtect == false) {
        this.proposalRQ.EMIAmount = "0";
      }
    }

    if (this.ProductType == "1" || this.ProductType == "12") {
      this.proposalRQ.productCode = "2312";
      data.productCode = "2312";
    } else if (this.ProductType == "2" || this.ProductType == "13") {
      this.proposalRQ.productCode = "2311";
      data.productCode = "2311";
    } else if (this.ProductType == "9") {
      this.proposalRQ.productCode = "2320";
      data.productCode = "2320";
    } else if (this.ProductType == "10") {
      this.proposalRQ.productCode = "2319";
      data.productCode = "2319";
    } else if (this.ProductType == "11") {
      this.proposalRQ.productCode = commonData.allriskProductCode;
      data.productCode = commonData.allriskProductCode;
    }

    this.proposalReq = this.proposalRQ;
    localStorage.setItem("savedQuotes", "true");
    localStorage.setItem("oldreference", JSON.stringify(data.policyNo));
    localStorage.setItem("saveQuoteData", JSON.stringify(data));


    if (this.ProductType == "11" && data.productCode == commonData.allriskProductCode) {
      localStorage.setItem("EVQuoteReq", JSON.stringify(this.quoteRQ));
      localStorage.setItem("EVQuoteRes", JSON.stringify(quoteRes));
      localStorage.setItem("EVPropDatareq", JSON.stringify(this.proposalRQ));
      localStorage.setItem("oldreference", JSON.stringify(data.allRisk.policyNo));
      localStorage.setItem("bankType", data.allRisk.bankType);
    } else {
      localStorage.setItem("oldreference", JSON.stringify(data.policyNo));
      localStorage.setItem("bankType", data.bankType);

      localStorage.setItem("calQuoteReq", JSON.stringify(this.quoteRQ));
      localStorage.setItem("calQuoteRes", JSON.stringify(quoteRes));
      // this.messageService.sendQuotereqData(this.quoteRQ);
      // this.messageService.sendQuoteresData(quoteRes);
      localStorage.setItem("PropDatareq", JSON.stringify(this.proposalRQ));
      // this.messageService.sendproposalreqData(this.proposalRQ);
    }

    this.router.navigateByUrl("proposal");
  }

  // Go to Dashboard
  goToDashboard() {
    this.cs.geToDashboard();
  }
}
