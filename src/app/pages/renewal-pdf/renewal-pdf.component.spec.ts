import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenewalPdfComponent } from './renewal-pdf.component';

describe('RenewalPdfComponent', () => {
  let component: RenewalPdfComponent;
  let fixture: ComponentFixture<RenewalPdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenewalPdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenewalPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
