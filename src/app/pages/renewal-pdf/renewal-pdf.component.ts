import { AfterViewChecked, Component, OnChanges, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from "src/app/services/common.service";
import swal from "sweetalert";
import { commonData } from "src/app/commonData/commonData";

@Component({
  selector: "app-renewal-pdf",
  templateUrl: "./renewal-pdf.component.html",
  styleUrls: ["./renewal-pdf.component.css"],
})
export class RenewalPDFComponent implements OnInit, AfterViewChecked {
  readPropData: any;
  biFuelODTP: any;
  comparePremiumPropNo: any;
  sendPaymentLinkResp: any;
  isBack: boolean = true;
  sendPropPdfResp: any;
  comparePremiumResp: any;
  comparePremiumStatus: any;
  tokenFromSaveRN: any;
  id: any;
  newPremium: any;
  type: any;
  isFW = false;
  policySubType: any;
  showSendLink: boolean = false;
  sendLink: boolean = false;
  moberrorFlag: boolean = false;
  spinloader: boolean = false;
  spinpayloader: boolean = false;
  premiumWithDiscount: any;
  totalDiscount: any;
  geographicalExtension: any;
  productCode = "2312";
  sendDetails = {
    Email: "",
    mobileno: "",
  };

  exshowroomprice: any;
  idv: any;

  constructor(
    public cs: CommonService,
    public router: Router,
    public activeRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    console.log('Show PDf', this.cs.isPdfModal);
    let sendLink1 = JSON.parse(sessionStorage.getItem('showSendLink'));
    this.sendLink = sendLink1;
    console.log('Send Link', sendLink1);
    this.createToken();
    this.activeRoute.queryParams.forEach((params) => {
      this.id = params.id;
      this.type = params.type;
      if (!this.cs.isUndefineORNull(this.type)) {
        sessionStorage.setItem("type", JSON.stringify(this.type));
        sessionStorage.setItem("id", JSON.stringify(this.id));
        sessionStorage.setItem("showSendLink", JSON.stringify('true'));
        this.showSendLink = true;
        this.cs.styleChanges = true;
      } else {
        this.showSendLink = false;
        this.cs.styleChanges = false;
      }
      if (!this.cs.isUndefineORNull(JSON.parse(sessionStorage.getItem("id")))) {
        this.cs.isPdfModal = true;
      } else {
        this.cs.isPdfModal = false;
      }
      let renewalData = JSON.parse(localStorage.getItem('payRenewalDataRes'));
      if (this.cs.isUndefineORNull(renewalData)) {
        console.log('pdf');
        this.readProposal(this.cs.isPdfModal);
      } else {
        this.readPropData = renewalData;
        this.isFW = this.cs.getProductCode(this.readPropData.PolicyNumber);
        console.log('Product', this.isFW);
        this.checkedCovers();
      }
    })
    // this.getParams().then(() => {
    //   if (this.cs.isUndefineORNull(this.id)) {
    //     localStorage.setItem('id', this.id);
    //     localStorage.setItem('showSendLink', 'true');
    //     this.showSendLink = true;
    //     this.cs.styleChanges = true;
    //   } else {
    //     this.showSendLink = false;
    //     this.cs.styleChanges = false;
    //   }
    //   let renewalData = JSON.parse(localStorage.getItem('payRenewalDataRes'));
    //   if(this.cs.isUndefineORNull(renewalData)){
    //     this.readProposal(true);
    //   }else{
    //     this.readPropData = renewalData;
    //     this.checkedCovers();
    //   }

    // });
  }

  // ngOnChanges(){
  //   if(this.cs.isPdfModal){
  //         let renewalData = JSON.parse(localStorage.getItem('payRenewalDataRes'));
  //         if(this.cs.isUndefineORNull(renewalData)){
  //           this.readProposal(true);
  //         }else{
  //           this.readPropData = renewalData;
  //           if(!this.cs.isUndefineORNull(this.readPropData)){
  //             this.checkedCovers();
  //           }else{
  //             this.readPropData = renewalData;

  //         }
  //       }        
  //     }  
  // }



  ngAfterViewChecked() {
    // window.scrollTo(0, 0);
    if (!this.cs.isUndefineORNull(JSON.parse(sessionStorage.getItem("id")))) {
      this.cs.isPdfModal = true;
    } else {
      this.cs.isPdfModal = false;
      let renewalData = JSON.parse(localStorage.getItem('payRenewalDataRes'));
      if (this.cs.isUndefineORNull(renewalData)) {
        this.readProposal(this.cs.isPdfModal);
      } else {
        this.readPropData = renewalData;
        this.isFW = this.cs.getProductCode(this.readPropData.PolicyNumber);
        console.log('Product', this.isFW);
        let antiTheftDiscount = this.readPropData.riskDetails.antiTheftDiscount ? this.readPropData.riskDetails.antiTheftDiscount : 0;
        let bonusDiscount = this.readPropData.riskDetails.bonusDiscount ? this.readPropData.riskDetails.bonusDiscount : 0;
        let voluntaryDiscount = this.readPropData.riskDetails.voluntaryDiscount ? this.readPropData.riskDetails.voluntaryDiscount : 0;
        let tppD_Discount = this.readPropData.riskDetails.tppD_Discount ? this.readPropData.riskDetails.tppD_Discount : 0;
        this.totalDiscount = antiTheftDiscount + bonusDiscount + voluntaryDiscount + tppD_Discount;
        this.premiumWithDiscount = this.readPropData.packagePremium + this.totalDiscount;
        if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.biFuelKitOD) || !this.cs.isUndefineORNull(this.readPropData.riskDetails.biFuelKitTP)) {
          this.biFuelODTP = this.readPropData.riskDetails.biFuelKitOD + this.readPropData.riskDetails.biFuelKitTP;
        } else {
          this.biFuelODTP = 0;
        }
        console.log('readProposal', this.readPropData, this.readPropData.riskDetails.geographicalExtensionOD, this.readPropData.riskDetails.geographicalExtensionTP);

        if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.geographicalExtensionOD) || !this.cs.isUndefineORNull(this.readPropData.riskDetails.geographicalExtensionTP)) {
          this.geographicalExtension = this.readPropData.riskDetails.geographicalExtensionOD + this.readPropData.riskDetails.geographicalExtensionTP;
        } else {
          this.geographicalExtension = 0;
        }
        this.checkedCovers();
      }
    }

  }

  checkedCovers() {
    let zdCover = document.getElementById('zdCover1') as HTMLInputElement;
    let rsaCover = document.getElementById('rsaCover1') as HTMLInputElement;
    let consuCover = document.getElementById('consuCover1') as HTMLInputElement;
    let keyProCover = document.getElementById('keyProCover1') as HTMLInputElement;
    let gCashCover = document.getElementById('gCashCover1') as HTMLInputElement;
    let rtiCover = document.getElementById('rtiCover1') as HTMLInputElement;
    let engProCover = document.getElementById('engProCover') as HTMLInputElement;
    let tyreProCover = document.getElementById('tyreProCover') as HTMLInputElement;
    let emiProCover = document.getElementById('emiProCover') as HTMLInputElement;
    let ncb = document.getElementById('ncb') as HTMLInputElement;
    let biFuelCover = document.getElementById('biFuelCover') as HTMLInputElement;
    // let cngCover = document.getElementById('cngCover') as HTMLInputElement;
    // let lpgCover = document.getElementById('lpgCover') as HTMLInputElement;
    let lopbCover = document.getElementById('lopbCover') as HTMLInputElement;
    let LPECover = document.getElementById('LPECover') as HTMLInputElement;
    let LEPDriver = document.getElementById('LEPDriver') as HTMLInputElement;
    let paUNPCover = document.getElementById('paUNPCover') as HTMLInputElement;
    let VDCover = document.getElementById('VDCover') as HTMLInputElement;
    let TPPD = document.getElementById('TPPD') as HTMLInputElement;
    let antTheft = document.getElementById('antTheft') as HTMLInputElement;
    let geoCover = document.getElementById('geoCover1') as HTMLInputElement;
    // let geoCoverTp = document.getElementById('geoCoverTp') as HTMLInputElement;
    let PACover = document.getElementById('PACover1') as HTMLInputElement;
    console.log(this.readPropData.riskDetails, zdCover);
    if (!this.cs.isUndefineORNull(this.readPropData)) {
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.zeroDepreciation)) {
        zdCover.checked = true;
      } else {
        zdCover.checked = false;
      }
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.roadSideAssistance)) {
        rsaCover.checked = true;
      } else {
        rsaCover.checked = false;
      }
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.consumables)) {
        consuCover.checked = true;
      } else {
        consuCover.checked = false;
      }
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.keyProtect)) {
        keyProCover.checked = true;
      } else {
        keyProCover.checked = false;
      }
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.garageCash)) {
        gCashCover.checked = true;
      } else {
        gCashCover.checked = false;
      }
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.returnToInvoice)) {
        rtiCover.checked = true;
      } else {
        rtiCover.checked = false;
      }
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.engineProtect)) {
        engProCover.checked = true;
      } else {
        engProCover.checked = false;
      }

      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.tyreProtect)) {
        tyreProCover.checked = true;
      } else {
        tyreProCover.checked = false;
      }
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.emiProtect)) {
        emiProCover.checked = true;
      } else {
        emiProCover.checked = false;
      }
      if (!this.cs.isUndefineORNull(this.readPropData.NCBProtectPlanName)) {
        ncb.checked = true;
      } else {
        ncb.checked = false;
      }
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.paCoverForOwnerDriver)) {
        PACover.checked = true;
      } else {
        PACover.checked = false;
      }
      if (this.productCode == '2311' || this.productCode == '2319') {
        if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.biFuelKitTP || this.readPropData.riskDetails.biFuelKitOD)) {
          biFuelCover.checked = true;
        } else {
          biFuelCover.checked = false;
        }
      }
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.lossOfPersonalBelongings)) {
        lopbCover.checked = true;
      } else {
        lopbCover.checked = false;
      }
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.employeesOfInsured)) {
        LPECover.checked = true;
      } else {
        LPECover.checked = false;
      }
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.paidDriver)) {
        LEPDriver.checked = true;
      } else {
        LEPDriver.checked = false;
      }
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.paCoverForUnNamedPassenger)) {
        paUNPCover.checked = true;
      } else {
        paUNPCover.checked = false;
      }
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.tppD_Discount)) {
        TPPD.checked = true;
      } else {
        TPPD.checked = false;
      }
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.voluntaryDiscount)) {
        VDCover.checked = true;
      } else {
        VDCover.checked = false;
      }

      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.antiTheftDiscount)) {
        antTheft.checked = true;
      } else {
        antTheft.checked = false;
      }
      if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.geographicalExtensionOD || this.readPropData.riskDetails.geographicalExtensionTP)) {
        geoCover.checked = true;
      } else {
        geoCover.checked = false;
      }
      // if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.geographicalExtensionTP)) {
      //   geoCoverTp.checked = true;
      // } else {
      //   geoCoverTp.checked = false;
      // }
    } else {

    }

  }

  createToken() {
    let body = {
      username: "iwqIU6Z/t8o=",
      password: "VYxkVlge5rq0WHkO7bucmQ==",
    };

    let str = JSON.stringify(body);
    this.cs.createRenewalsToken("token", str).then((res: any) => {
      console.log(res);
      localStorage.setItem("renewalAuthToken", JSON.stringify(res));
      if (!this.showSendLink) {
        this.readProposal(true);
      } else {
        this.readProposal(false);
      }
    });
  }

  goToDashborad() {
    if (this.cs.isFromRenewalModify) {
      this.router.navigateByUrl("renewalmodifyproposal");
    } else {
      // this.cs.isBack = true;
      this.router.navigateByUrl("renewals");
      // localStorage.setItem("landingPage",'Renewal');
    }
    // this.cs.fromPDF = true;
  }


  // Number only valid
  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    console.log(event.target.value.length);
    let length = event.target.value.length;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  //Mobile validation
  mobileValidate() {
    let isNumberValidate = this.cs.mobileValidation(this.sendDetails.mobileno);
    if (isNumberValidate && this.sendDetails.mobileno.length == 10) {
      this.sendDetails.mobileno = this.sendDetails.mobileno;
      this.moberrorFlag = false;
    } else {
      this.sendDetails.mobileno = "";
      this.moberrorFlag = true;
    }
  }

  getParams(): Promise<any> {
    return new Promise((resolve: any) => {
      this.activeRoute.queryParams.forEach((params) => {
        this.id = params.id;
        this.type = params.type;
        if (!this.cs.isUndefineORNull(this.type)) {
          sessionStorage.setItem("type", JSON.stringify('PDF'));
        }
        resolve();
      });
    });
  }

  sendPaymentLink() {
    let sendPayData = JSON.parse(localStorage.getItem("payRenewalData"));
    let body = {
      propNo: this.readPropData.proposalNumber,//this.readPropData.PROPOSAL_NO,
      policyNo: this.readPropData.PolicyNumber ? this.readPropData.PolicyNumber : this.readPropData.POLICY_NO,
      phoneNo: this.sendDetails.mobileno,
      emailId: this.sendDetails.Email,
      premium: this.readPropData.finalPremium ? this.readPropData.finalPremium : this.readPropData.PREMIUM,
      regNo: this.readPropData.RegistrationNumber ? this.readPropData.RegistrationNumber : this.readPropData.VEH_REG_NO,
    };
    let str = JSON.stringify(body);
    this.cs.postForRenewals("sendPaymentLink", str).then((res: any) => {
      console.log(res);
      this.sendPaymentLinkResp = res;
      // this.isBack = false;
      if (res.emailStatus) {
        swal({ text: " Payment link has been sent successfully." });
      } else {
        swal({ text: "Failed to send link." });
      }
    });
  }

  sendPropPdf() {
    let data = JSON.parse(localStorage.getItem("payRenewalData"));
    // let vehType = data.VEH_TYPE.replace(/\s/g, "");
    let body = {
      propNo: this.readPropData.proposalNumber,
      product: (this.productCode == "2319" || this.productCode == "2320") ? "TP" : "PP",
      vehType: (this.productCode == "2312" || this.productCode == "2320") ? 'TWOWHEELER' : 'PVTCAR',
      policyNo: this.readPropData.PolicyNumber,
      phoneNo: this.sendDetails.mobileno,
      emailId: this.sendDetails.Email,
      premium: this.readPropData.finalPremium,
      regNo: this.readPropData.RegistrationNumber,
    };
    let strBody = JSON.stringify(body);
    this.cs.postForRenewals("sendProposalPDFV2", strBody).then((res: any) => {
      console.log(res);
      this.sendPropPdfResp = res;
      // this.isBack = false;
      if (res.emailStatus) {
        swal({ text: "Proposal Pdf has been sent successfully." });
      } else {
        swal({ text: "Failed to send Pdf." });
      }
    });
  }

  readProposal(isRead: any): Promise<any> {
    return new Promise((resolve: any) => {
      let body: any; let breakinFlag = false; let iasFlag = false; let sendPayData: any;
      this.type = JSON.parse(sessionStorage.getItem("type"));
      breakinFlag = JSON.parse(localStorage.getItem("breakinFlag"));
      iasFlag = JSON.parse(localStorage.getItem("isIASFlag"));
      this.id = JSON.parse(sessionStorage.getItem("id"));
      if (this.type == "PDF" || this.type == 'PAY') {
        body = {
          id: this.id,
        };
      } else {
        if (breakinFlag) {
          let data = JSON.parse(localStorage.getItem("renewBreakInData"));
          let data1 = JSON.parse(data.proposalRQ);
          sendPayData = JSON.parse(data1);
        } else if (iasFlag) {
          let data = JSON.parse(localStorage.getItem("renewBreakInData"));
          let data1 = JSON.parse(data.bizRequest);
          sendPayData = JSON.parse(data1);
        } else {
          sendPayData = JSON.parse(localStorage.getItem("payRenewalData"));
        }

        body = {
          policyNo: sendPayData.POLICY_NO ? sendPayData.POLICY_NO : sendPayData.PolicyNumber,//"3005/51925075/00/B02", //"3005/2425/6999/B00",
        };
      }

      let str = JSON.stringify(body);
      this.cs.postForRenewals("readProposalV2", str).then((res: any) => {
        console.log(3, res);
        this.readPropData = res;
        this.isFW = this.cs.getProductCode(this.readPropData.PolicyNumber);
        console.log('Product', this.isFW);
        localStorage.setItem('payRenewalDataRes', JSON.stringify(res));
        localStorage.setItem('Riskdetails', JSON.stringify(res.riskDetails));
        if (!this.cs.isUndefineORNull(res.IDVDetails)) {
          this.exshowroomprice = res.VehicleDetails.ShowRoomPrice;

          // let depreciationRate = res.IDVDetails.idvdepreciationpercent;
          // let requiredIDV = res.VehicleDetails.DepriciatedIDV;
          // let newDepreciationRate = 1 - depreciationRate;
          // let newExshowRoom = Math.round(parseInt(requiredIDV) / newDepreciationRate);
          // console.log('exshowroomprice', newExshowRoom);
          // this.exshowroomprice = newExshowRoom;

          // let idv = this.exshowroomprice - this.exshowroomprice * res.IDVDetails.idvdepreciationpercent;
          // console.log("IDV", idv, Math.floor(idv));
          // this.idv = Math.floor(idv);
        }
        this.idv = res.VehicleDetails.DepriciatedIDV;
        let antiTheftDiscount = this.readPropData.riskDetails.antiTheftDiscount ? this.readPropData.riskDetails.antiTheftDiscount : 0;
        let bonusDiscount = this.readPropData.riskDetails.bonusDiscount ? this.readPropData.riskDetails.bonusDiscount : 0;
        let voluntaryDiscount = this.readPropData.riskDetails.voluntaryDiscount ? this.readPropData.riskDetails.voluntaryDiscount : 0;
        let tppD_Discount = this.readPropData.riskDetails.tppD_Discount ? this.readPropData.riskDetails.tppD_Discount : 0;
        this.totalDiscount = antiTheftDiscount + bonusDiscount + voluntaryDiscount + tppD_Discount;
        this.premiumWithDiscount = this.readPropData.packagePremium + this.totalDiscount;
        if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.biFuelKitOD) || !this.cs.isUndefineORNull(this.readPropData.riskDetails.biFuelKitTP)) {
          this.biFuelODTP = this.readPropData.riskDetails.biFuelKitOD + this.readPropData.riskDetails.biFuelKitTP;
        } else {
          this.biFuelODTP = 0;
        }
        console.log('readProposal', this.readPropData, this.readPropData.riskDetails.geographicalExtensionOD, this.readPropData.riskDetails.geographicalExtensionTP);

        if (!this.cs.isUndefineORNull(this.readPropData.riskDetails.geographicalExtensionOD) || !this.cs.isUndefineORNull(this.readPropData.riskDetails.geographicalExtensionTP)) {
          this.geographicalExtension = this.readPropData.riskDetails.geographicalExtensionOD + this.readPropData.riskDetails.geographicalExtensionTP;
        } else {
          this.geographicalExtension = 0;
        }
        this.sendDetails.mobileno =
          this.readPropData.CustomerDetails.MobileNumber;
        this.sendDetails.Email = this.readPropData.CustomerDetails.Email;
        console.log(this.readPropData);
        setTimeout(() => {
          console.log("Hello from setTimeout");
          this.checkedCovers();
        }, 500);
        // if (isRead) {
        //   this.router.navigate(["renewalPayment"]);
        // }
        resolve();
      });
    });

  }

  comparePremium(): Promise<any> {
    return new Promise((resolve: any) => {
      let body = {
        policyNo: this.readPropData.PolicyNumber,
        finalPremium: this.readPropData.finalPremium,
      };
      let str = JSON.stringify(body);
      this.cs.postForRenewals("comparePremiumV2", str).then((res: any) => {
        this.comparePremiumResp = res;
        this.comparePremiumPropNo = res.newProposal;
        this.comparePremiumStatus = res.status == "Failed";
        this.spinpayloader = false;
        if (res.status == "Failed" && res.message == "Cannot proceed to payment due to breakin or deviation scenario") {
          swal({
            text: "Cannot proceed to payment due to breakin or deviation scenario."
          })
          // this.saveProposalForPayment().then(() => {
          //   this.PlutusPaymentPaymsToken();
          //   resolve();
          // })
        } else if (res.status == "Failed" && res.message == "Premium mismatch") {
          this.newPremium = res.newPremium;
          let compareMsg1 = "Your old premium amount Rs."
          let compareMsg2 = " has been changed to Rs.";
          let compareMsg3 = ". Do you want to continue ?";
          let finalPremium = this.readPropData.finalPremium;
          swal({
            text: compareMsg1 + '' + finalPremium + '' + compareMsg2 + '' + res.newPremium + '' + compareMsg3,
            dangerMode: true,
            closeOnClickOutside: false,
            buttons: ["Yes", "No"],
          }).then((click) => {
            if (!click) {
              console.log("Continue");
              this.saveProposalForPayment().then(() => {
                this.PlutusPaymentPaymsToken();
                resolve();
              })
            } else {
              console.log("Go Back");
              resolve();
            }
          });
        }
        else if (res.status == "Failed" && res.blazeMessage.includes("Renewed Proposal No")) {
          let splitMsg = res.blazeMessage.split(":");
          let propNo = splitMsg[2];
          let policyMsg = "Your policy has been Renewed. Please find renewed Proposal no: "
          swal({
            text: policyMsg + "" + propNo,
          })
        }
        else if (res.status == "Failed" && res.message == "Premium API returned failure") {
          swal({
            text: res.blazeMessage,
          })
        }
        else {
          this.saveProposalForPayment().then(() => {
            this.PlutusPaymentPaymsToken();
            resolve();
          })

        }
      });
    });
  }

  saveProposalForPayment(): Promise<any> {
    return new Promise((resolve: any) => {
      console.log(this.cs.isPdfModal);
      let data = JSON.parse(localStorage.getItem('payRenewalDataRes'))
      if (this.cs.isUndefineORNull(data)) {
        if (this.productCode == "2312") {
          this.policySubType = "1";
        } else if (this.productCode == "2311") {
          this.policySubType = "2";
        } else if (this.productCode == "2319") {
          this.policySubType = "10";
        } else if (this.productCode == "2320") {
          this.policySubType = "9";
        }
      } else {
        if (data.ProductCode == "2312") {
          this.policySubType = "1";
        } else if (data.ProductCode == "2311") {
          this.policySubType = "2";
        } else if (data.ProductCode == "2319") {
          this.policySubType = "10";
        } else if (data.ProductCode == "2320") {
          this.policySubType = "9";
        }
      }
      let body = {
        PolicyType: 1,
        PolicySubType: parseInt(this.policySubType),
        ProposalNumber: this.comparePremiumPropNo,
        PolicyStartDate: this.comparePremiumResp.internalRequest.ProposalDetails.PolicyStartDate,
        PolicyEndDate: this.comparePremiumResp.internalRequest.ProposalDetails.PolicyEndDate,
        BasicPremium: this.comparePremiumResp.internalResponse.riskDetails.basicOD,
        Discount: this.comparePremiumResp.internalResponse.riskDetails.bonusDiscount,
        ServiceTax: this.comparePremiumResp.internalResponse.totalTax,
        TotalPremium: this.comparePremiumResp.internalResponse.finalPremium,
        Status: this.comparePremiumStatus ? 6 : 4,
        DealID: this.comparePremiumResp.internalRequest.DealID,
        TOTALTAX: this.comparePremiumResp.internalResponse.totalTax,
        TransFor: "Renewal",
        // TRANS_ON_TS: time,
        PolicyTenure: parseInt(this.comparePremiumResp.internalResponse.generalInformation.tenure),
        PolicyTpTenure: parseInt(this.comparePremiumResp.internalResponse.generalInformation.tpTenure),
        PolicyPACoverTenure: !this.cs.isUndefineORNull(this.comparePremiumResp.internalRequest.ProposalDetails.CustomerDetails.PACoverTenure) ? this.comparePremiumResp.internalRequest.ProposalDetails.CustomerDetails.PACoverTenure : 1,
        CustomerID: parseInt(this.comparePremiumResp.internalRequest.CustomerID),
        PFCustomerID: parseInt(this.comparePremiumResp.internalResponse.generalInformation.customerId),
        CustomerName: this.readPropData.CustomerDetails.CustomerName,
        PackagePremium: this.comparePremiumResp.internalResponse.packagePremium,
        TotalLiablityPremium: this.comparePremiumResp.internalResponse.totalLiabilityPremium,
        ProposalRS: JSON.stringify(this.comparePremiumResp.internalResponse),
        CorrelationId: JSON.stringify(this.comparePremiumResp.internalRequest.CorrelationId),
        CustomerType: JSON.stringify(this.comparePremiumResp.internalRequest.ProposalDetails.CustomerDetails.CustomerType),
        ProposalRQ: JSON.stringify(this.comparePremiumResp.internalRequest),
        InspectionID: this.comparePremiumResp.internalRequest.interactionDetails ? this.comparePremiumResp.internalRequest.interactionDetails.inspection_id : 0,
        InspectionStatus: this.comparePremiumResp.internalRequest.interactionDetails ? this.comparePremiumResp.internalRequest.interactionDetails.inspection_status : 0,
        InspectionType: null,
        IsStandaloneOD: 0,
        subLocation: "",
        TPStartDate: this.comparePremiumResp.internalRequest.ProposalDetails.TPStartDate,
        TPEndDate: this.comparePremiumResp.internalRequest.ProposalDetails.TPEndDate,
        TPPolicyNum: "",//this.comparePremiumResp,
        TPInsurerName: this.comparePremiumResp.internalRequest.ProposalDetails.TPInsurerName,
        IsSubAgent: 0,
        SubAgentIpartnerUserID: "",
        IDVIASID: this.comparePremiumResp.internalRequest.interactionDetails ? this.comparePremiumResp.internalRequest.interactionDetails.idv_ias_id : "",
        IDVIASSRT: "",
        IDVIASSUBSRT: "",
        ODIASID: this.comparePremiumResp.internalRequest.interactionDetails ? this.comparePremiumResp.internalRequest.interactionDetails.od_ias_id : "",
        ODIASSRT: "",
        ODIASSUBSRT: "",
        ODIASRQ: "",
        ODIASRS: "",
        IDVIASRQ: "",
        IDVIASRS: "",

      }
      let str = JSON.stringify(body);
      this.cs.postForRenewalsPayment("Proposal/SaveRNProposal", str).then((res: any) => {
        this.tokenFromSaveRN = res.output;
        localStorage.setItem('RenewAuthToken', JSON.stringify(res));
        console.log(res);
        resolve();

      })
    })
  }

  PlutusPaymentPaymsToken() {
    this.cs.getPaymsTokenRenewalPDF().then((resp: any) => {
      localStorage.setItem("paymsToken", JSON.stringify(resp.accessToken));
      this.redirectToPlutusPayment();
    });
  }

  redirectToPlutusPayment() {
    let bankType = this.readPropData.BankType;
    // console.log('banktype',bankType);
    let instaFlag; let userFlag = 0;
    if (bankType == "INT") {
      instaFlag = 1;
    } else {
      instaFlag = 0;
    }

    if (this.type != 'PAY' && this.type != 'PDF') {
      userFlag = 0;
    } else {
      userFlag = 1;
    }

    let email = this.cs.encrypt(
      this.readPropData.CustomerDetails.Email,
      commonData.aesSecretKey
    );
    let phone = this.cs.encrypt(
      this.readPropData.CustomerDetails.MobileNumber,
      commonData.aesSecretKey
    );
    let body = {
      CorrelationID: this.readPropData.CorrelationId,
      Amount: !this.cs.isUndefineORNull(this.newPremium) ? this.newPremium : this.readPropData.finalPremium,
      ProposalNo: this.comparePremiumPropNo,//this.readPropData.proposalNumber,
      DealID: this.readPropData.DealID,
      CustomerID: this.readPropData.CustomerID,
      Email: email,
      ContactNo: phone,
      UserFlag: userFlag,
      IsInsta: instaFlag,
      MultiFlag: 0,
      PidFlag: 0,
      PreInsta: 0,
      notes: {
        PreviousPolicyNo: this.readPropData.PolicyNumber,
        IsRenewal: "1"
      }
    };

    let str = JSON.stringify(body);
    this.cs.postPayms("Redirection/AddPaymentRequest", str).then((res: any) => {
      // console.log(res);
      if (res.Status == "Success" || res.Status == "success") {
        window.location.href = res.URL;
      } else {
        swal({ text: "Try again later!" });
      }
    });
  }

  paymentValidate() {
    this.spinpayloader = true;
    this.type = JSON.parse(sessionStorage.getItem("type"));
    if (this.type == "PDF") {
      this.comparePremium();
    } else {
      this.readProposal(false).then(() => {
        this.comparePremium().then(() => {
          // this.redirectToPlutusPayment();
        });
      });

    }

  }
}
