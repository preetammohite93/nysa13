import {
  Component,
  DoCheck,
  OnChanges,
  OnInit

} from "@angular/core";
import { CommonService } from "src/app/services/common.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ApiServiceService } from "src/app/services/api-service.service";
import * as moment from "moment";
import swal from "sweetalert";
import { commonData } from "src/app/commonData/commonData";
import jwt_decode from "jwt-decode";


@Component({
  selector: "app-renewal-dashboard",
  templateUrl: "./renewal-dashboard.component.html",
  styleUrls: ["./renewal-dashboard.component.css"],
})
export class RenewalDashboardComponent implements OnInit, DoCheck {
  month: any;
  showTable: boolean = false;
  // spinPayloader = false;
  newPremium: any;

  isProductChange = false;
  policySubType: any;
  customiseFlag = false;
  selectedMonth = false;
  FromDate: any;
  ToDate: any;
  term: any;
  norecordflag: any;
  spinloader: boolean = false;
  renewalPage = 1;
  indexForDashData = 0;
  finalDashBoardData = [];
  memmaxDOB: any;
  memminDOB: any;
  renewalData = [];
  dealid: any;
  stateCD: any;
  authtok: any;
  policyEndDate: any;
  // productCode = "2312";
  productCode: any;
  comparePremiumStatus: any;
  isSAOD: boolean = false;
  dealDetails: any;
  paramDataValues: any;
  startDate: any;
  readPropData: any;
  comparePremiumPropNo: any;
  moberrorFlag: boolean = false;
  mobNumberPattern = "^(0/91)?[6-9][0-9]{9}$";

  // Redirection Change
  expiry: any;
  signature: any;
  iPartner_Token: any;
  product: any;
  policy: any;
  iPartnerUserID: any;

  id: any;
  type: any;
  showSendLink: boolean = false;
  comparePremiumResp: any;
  renewalpolicyDetails = {
    FromDate: "",
    ToDate: "",
  };
  sendDetails = {
    Email: "",
    mobileno: "",
  };
  monthList = [
    { Value: 0, Text: 'January', month: '01', start: '01', end: '31' },
    { Value: 1, Text: 'February', month: '02', start: '01', end: '28' },
    { Value: 2, Text: 'March', month: '03', start: '01', end: '31' },
    { Value: 3, Text: 'April', month: '04', start: '01', end: '30' },
    { Value: 4, Text: 'May', month: '05', start: '01', end: '31' },
    { Value: 5, Text: 'June', month: '06', start: '01', end: '30' },
    { Value: 6, Text: 'July', month: '07', start: '01', end: '31' },
    { Value: 7, Text: 'August', month: '08', start: '01', end: '31' },
    { Value: 8, Text: 'September', month: '09', start: '01', end: '30' },
    { Value: 9, Text: 'October', month: '10', start: '01', end: '31' },
    { Value: 10, Text: 'November', month: '11', start: '01', end: '30' },
    { Value: 11, Text: 'December', month: '12', start: '01', end: '31' }
  ];

  selectedMonthList = [];
  filterCount = 0;
  isRenewalPolicy = true;

  constructor(
    private cs: CommonService,
    private router: Router,
    public api: ApiServiceService,
    public activeRoute: ActivatedRoute
  ) { }

  ngOnInit() {



    localStorage.removeItem("breakinFlag");
    localStorage.removeItem("isIASFlag");
    let productName = JSON.parse(localStorage.getItem('ProductCode'));
    let allProductCode = document.getElementById('renew_allProduct') as HTMLInputElement;
    let twoWheelerOD = document.getElementById('renew_twoWheeler') as HTMLInputElement;
    let fourWheelerOD = document.getElementById('renew_fourWheeler') as HTMLInputElement;
    let twoWheelerTP = document.getElementById('renew_twoWheelerTP') as HTMLInputElement;
    let fourWheelerTP = document.getElementById('renew_fourWheelerTP') as HTMLInputElement;
    let twoWheelerSAOD = document.getElementById('renew_twoWheelerOD') as HTMLInputElement;
    let fourWheelerSAOD = document.getElementById('renew_fourWheelerOD') as HTMLInputElement;
    console.log('productname', productName);
    if (this.cs.isUndefineORNull(productName)) {
      this.productCode = 'ALL';
      allProductCode.checked = true;
      twoWheelerOD.checked = false;
      fourWheelerOD.checked = false;
      twoWheelerTP.checked = false;
      fourWheelerTP.checked = false;
      twoWheelerSAOD.checked = false;
      fourWheelerSAOD.checked = false;
      localStorage.setItem('ProductCode', JSON.stringify(this.productCode));
    } else {

      this.productCode = productName;
      if (this.productCode == '2312') {
        allProductCode.checked = false;
        twoWheelerOD.checked = true;
        fourWheelerOD.checked = false;
        twoWheelerTP.checked = false;
        fourWheelerTP.checked = false;
        twoWheelerSAOD.checked = false;
        fourWheelerSAOD.checked = false;
      } else if (this.productCode == '2311') {
        allProductCode.checked = false;
        twoWheelerOD.checked = false;
        fourWheelerOD.checked = true;
        twoWheelerTP.checked = false;
        fourWheelerTP.checked = false;
        twoWheelerSAOD.checked = false;
        fourWheelerSAOD.checked = false;
      } else if (this.productCode == '2320') {
        allProductCode.checked = false;
        twoWheelerOD.checked = false;
        fourWheelerOD.checked = false;
        twoWheelerTP.checked = true;
        fourWheelerTP.checked = false;
        twoWheelerSAOD.checked = false;
        fourWheelerSAOD.checked = false;
      } else if (this.productCode == '2319') {
        allProductCode.checked = false;
        twoWheelerOD.checked = false;
        fourWheelerOD.checked = false;
        twoWheelerTP.checked = false;
        fourWheelerTP.checked = true;
        twoWheelerSAOD.checked = false;
        fourWheelerSAOD.checked = false;
      } else if (this.productCode == '2312') {
        allProductCode.checked = false;
        twoWheelerOD.checked = false;
        fourWheelerOD.checked = false;
        twoWheelerTP.checked = false;
        fourWheelerTP.checked = false;
        twoWheelerSAOD.checked = true;
        fourWheelerSAOD.checked = false;
      } else if (this.productCode == '2311') {
        allProductCode.checked = false;
        twoWheelerOD.checked = false;
        fourWheelerOD.checked = false;
        twoWheelerTP.checked = false;
        fourWheelerTP.checked = false;
        twoWheelerSAOD.checked = false;
        fourWheelerSAOD.checked = true;
      } else {
        allProductCode.checked = true;
        twoWheelerOD.checked = false;
        fourWheelerOD.checked = false;
        twoWheelerTP.checked = false;
        fourWheelerTP.checked = false;
        twoWheelerSAOD.checked = false;
        fourWheelerSAOD.checked = false;
      }
    }

    sessionStorage.setItem("showSendLink", JSON.stringify('false'));
    sessionStorage.removeItem('type');
    sessionStorage.removeItem('id');
    let beforeMonth1, afterMonth1;
    let month1 = new Date().getMonth();
    let beforeMonth = new Date().getMonth() - 3;
    let afterMonth = new Date().getMonth() + 2;
    if (beforeMonth < 0) {
      beforeMonth1 = 12 + beforeMonth;
    } else {
      beforeMonth1 = 12 - beforeMonth;
    }

    for (let i = month1 - 3; i < month1; i++) {
      console.log('month', this.monthList[i].Text);
      this.selectedMonthList.push(this.monthList[i]);
    }

    for (let i = month1; i <= month1 + 2; i++) {
      console.log('month', this.monthList[i].Text);
      this.selectedMonthList.push(this.monthList[i]);
    }

    console.log('month', month1, beforeMonth, beforeMonth1, afterMonth);
    let month = this.monthList[month1].Text;

    if (this.cs.isBack) {
      let data = JSON.parse(localStorage.getItem('month'));
      this.month = data;
    } else {
      this.month = month;
    }
    if (this.cs.isBack || this.cs.isShowBackButton) {
      let custData = JSON.parse(localStorage.getItem('customiseFlag'));
      this.customiseFlag = custData;
    } else {
      this.customiseFlag = this.customiseFlag;
    }

    // this.monthList.forEach(element => {
    //   console.log('element',element);
    //   if ((element.Value >= beforeMonth1) || (element.Value <= afterMonth)) {
    //     this.selectedMonthList.push(element);
    //   }
    // });
    console.log('NewMonthArray', this.selectedMonthList)
    this.getParams().then(() => {
      if (this.cs.isUndefineORNull(this.id)) {
        this.showSendLink = true;
      } else {
        this.showSendLink = false;
      }
    });




    let date = new Date();
    let mySelectedMonth = this.monthList.find((selectedMonth) => selectedMonth.Text == this.month);
    localStorage.setItem('month', JSON.stringify(this.month))
    console.log('Selected', mySelectedMonth);
    let startDate = mySelectedMonth.start;
    let endDate = mySelectedMonth.end;
    let intMonth = mySelectedMonth.month;
    let monthValue = mySelectedMonth.Value;
    let year = date.getFullYear();
    let year1 = date.getFullYear() - 1;
    console.log('PreviousYear', year1, monthValue, mySelectedMonth.Text);
    let fromDate1 = JSON.parse(localStorage.getItem('fromDate'));
    let endDate1 = JSON.parse(localStorage.getItem('toDate'));
    if (this.cs.isUndefineORNull(fromDate1) || this.cs.isUndefineORNull(endDate1)) {
      this.renewalpolicyDetails.FromDate = year + '-' + intMonth + '-' + startDate;
      this.renewalpolicyDetails.ToDate = year + '-' + intMonth + '-' + endDate;
    } else {
      this.renewalpolicyDetails.FromDate = fromDate1;
      this.renewalpolicyDetails.ToDate = endDate1;
    }

    this.createToken();









    let isRenewalProposalModify = JSON.parse(localStorage.getItem("isRenewalProposalModify"));
    console.log(isRenewalProposalModify);
    if (isRenewalProposalModify) {
      this.getRenewalData();
    } else {
      let dashboardData = JSON.parse(localStorage.getItem("dashboardData"));
      if (!this.cs.isUndefineORNull(dashboardData)) {
        this.renewalData = dashboardData;
        this.norecordflag = false;
      } else {
        this.renewalData = [];
        this.norecordflag = true;
      }
    }
  }

  ngDoCheck() {

    let isRenewalProposalModify = JSON.parse(localStorage.getItem("isRenewalProposalModify"));
    console.log(isRenewalProposalModify);

    // console.log("docheck", this.cs.isBack);
    let dashboardData = JSON.parse(localStorage.getItem("dashboardData"));
    if (!this.cs.isUndefineORNull(dashboardData)) {
      this.renewalData = dashboardData;
      this.norecordflag = false;
    } else {
      this.renewalData = [];
      this.norecordflag = true;
    }
  }

  getParams(): Promise<any> {
    return new Promise((resolve: any) => {
      this.activeRoute.queryParams.forEach((params) => {
        this.id = params.id;
        this.type = params.type;
        resolve();
      });
    });
  }

  customise() {
    this.customiseFlag = !this.customiseFlag;
    localStorage.setItem('customiseFlag', JSON.stringify(this.customiseFlag));
  }

  handlePageChange(event: any) {
    this.renewalPage = event;
    console.log(this.renewalData.length, event, this.finalDashBoardData);
    this.indexForDashData = this.renewalData.length;
    if ((this.renewalData.length / 10) == event) {
      console.log('need to Called Dashboard Data');
      this.getRenewalData();
    } else {
      console.log('No Need to Called Dashboard Data');
    }
  }


  pageSearchChange() {
    this.renewalPage = 1;
  }

  advanceSearch() {
    this.renewalData = [];
    let authData = JSON.parse(localStorage.getItem("AuthToken"));
    let body = {
      "policyNo": this.term,
      "imId": authData.username
    }
    let str = JSON.stringify(body);
    this.spinloader = true;
    this.cs.postForRenewals("search", str).then((res: any) => {
      console.log(res);
      if(res.status == 'Failed'){
        swal({
          text: res.message
        })
      }else if(res.status == 'Renewal'){
        this.isRenewalPolicy = true;
        this.renewalData = res.data;
        localStorage.setItem("dashboardData", JSON.stringify(res.data));
      }else if(res.status == 'Rollover'){
        this.isRenewalPolicy = false;
        this.renewalData = res.data;
        this.productCode = res.data[0].PRODUCT_CODE;
        localStorage.setItem("dashboardData", JSON.stringify(res.data));
      }
      this.spinloader = false;
      console.log(this.renewalData);
    })
  }

  getDateFromMonth(val: any) {
    this.isProductChange = true;
    localStorage.setItem('dashboardBodyIndex', JSON.stringify(this.indexForDashData));
    console.log('ABC', val, this.month);
    localStorage.setItem('month', JSON.stringify(this.month))
    let date = new Date();
    let month = date.getMonth() + 1;
    let mySelectedMonth = this.monthList.find((selectedMonth) => selectedMonth.Text == this.month);
    console.log('Selected', mySelectedMonth);
    let startDate = mySelectedMonth.start;
    let endDate = mySelectedMonth.end;
    let intMonth = mySelectedMonth.month;
    let beforeMonth = new Date().getMonth() - 3;
    let year: any;
    // let year1 = date.getFullYear() - 1;
    let diff = (month - parseInt(intMonth) < 0) ? ((month - parseInt(intMonth)) * -1) : month - parseInt(intMonth);
    console.log(diff)
    console.log('Date Validation', diff, intMonth, month);
    if (diff > 3) {
      year = date.getFullYear() - 1;
      this.renewalpolicyDetails.FromDate = year + '-' + intMonth + '-' + startDate;
      this.renewalpolicyDetails.ToDate = year + '-' + intMonth + '-' + endDate;
    } else if ((parseInt(intMonth) + 2) > 12) {
      year = date.getFullYear() + 1;
      this.renewalpolicyDetails.FromDate = year + '-' + intMonth + '-' + startDate;
      this.renewalpolicyDetails.ToDate = year + '-' + intMonth + '-' + endDate;
    } else {
      year = date.getFullYear();
      this.renewalpolicyDetails.FromDate = year + '-' + intMonth + '-' + startDate;
      this.renewalpolicyDetails.ToDate = year + '-' + intMonth + '-' + endDate;
    }
    console.log('Date', startDate, endDate, intMonth, year, this.month, beforeMonth, month);
    console.log('Current Month', month, 'selected Month', intMonth, 'Difference', month - parseInt(intMonth));
    console.log("range", month - 3, month + 2);
    // this.renewalpolicyDetails.FromDate = year + '-' + intMonth + '-' + startDate;
    // this.renewalpolicyDetails.ToDate = year + '-' + intMonth + '-' +endDate;
    console.log('Date Validation', this.renewalpolicyDetails.FromDate, this.renewalpolicyDetails.ToDate);


  }

  fromDate(ev: any) {
    console.log('Date', ev._d);
    localStorage.setItem('fromDate', JSON.stringify(ev._d));
    this.isProductChange = true;
    localStorage.setItem('dashboardBodyIndex', JSON.stringify(this.indexForDashData));
  }

  toDate(ev: any) {
    console.log('Date', ev._d);
    localStorage.setItem('toDate', JSON.stringify(ev._d));
    this.isProductChange = true;
    localStorage.setItem('dashboardBodyIndex', JSON.stringify(this.indexForDashData));
  }

  // Number only valid
  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    console.log(event.target.value.length);
    let length = event.target.value.length;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  //  if(event.code == "Backspace" || event.code == "Delete"){
  //    return false;
  //  }
  //  return true;
  // }

  //Mobile validation
  mobileValidate() {
    let isNumberValidate = this.cs.mobileValidation(this.sendDetails.mobileno);
    if (isNumberValidate && this.sendDetails.mobileno.length == 10) {
      this.sendDetails.mobileno = this.sendDetails.mobileno;
      this.moberrorFlag = false;
    } else {
      // this.sendDetails.mobileno = "";
      this.moberrorFlag = true;
    }
  }

  vehicletype(ev: any) {
    if (ev.target.id == "renew_allProduct") {
      this.productCode = "ALL";
      this.isSAOD = false;
      this.isProductChange = true;
      localStorage.setItem('dashboardBodyIndex', JSON.stringify(this.indexForDashData));
      this.getDeal();
    } else if (ev.target.id == "renew_twoWheeler") {
      this.productCode = "2312";
      this.isSAOD = false;
      this.isProductChange = true;
      localStorage.setItem('dashboardBodyIndex', JSON.stringify(this.indexForDashData));
      this.getDeal();
    } else if (ev.target.id == "renew_fourWheeler") {
      this.productCode = "2311";
      this.isSAOD = false;
      this.isProductChange = true;
      localStorage.setItem('dashboardBodyIndex', JSON.stringify(this.indexForDashData));
      this.getDeal();
    } else if (ev.target.id == "renew_twoWheelerTP") {
      this.productCode = "2320";
      this.isSAOD = false;
      this.isProductChange = true;
      localStorage.setItem('dashboardBodyIndex', JSON.stringify(this.indexForDashData));
      this.getDeal();
    } else if (ev.target.id == "renew_fourWheelerTP") {
      this.productCode = "2319";
      this.isSAOD = false;
      this.isProductChange = true;
      localStorage.setItem('dashboardBodyIndex', JSON.stringify(this.indexForDashData));
      this.getDeal();
    } else if (ev.target.id == "renew_twoWheelerOD") {
      this.productCode = "2312";
      this.isSAOD = true;
      this.isProductChange = true;
      localStorage.setItem('dashboardBodyIndex', JSON.stringify(this.indexForDashData));
      this.getDeal();
    } else if (ev.target.id == "renew_fourWheelerOD") {
      this.productCode = "2311";
      this.isSAOD = true;
      this.isProductChange = true;
      localStorage.setItem('dashboardBodyIndex', JSON.stringify(this.indexForDashData));
      this.getDeal();
    }
    localStorage.setItem('ProductCode', JSON.stringify(this.productCode));

  }

  createToken() {
    let body = {
      username: "iwqIU6Z/t8o=",
      password: "VYxkVlge5rq0WHkO7bucmQ==",
    };

    let str = JSON.stringify(body);
    this.cs.createRenewalsToken("token", str).then((res: any) => {
      console.log(res);
      localStorage.setItem("renewalAuthToken", JSON.stringify(res));
    });
  }

  getDeal(): Promise<any> {
    return new Promise((resolve: any) => {
      this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
      this.authtok = JSON.parse(localStorage.getItem("AuthToken"));
      // this.productCode = "2312" ;
      this.api
        .getDeal(this.authtok.username, this.productCode)
        .subscribe((res: any) => {
          this.dealDetails = res;
          this.dealid = this.dealDetails.dealID;
          localStorage.setItem("DealID", JSON.stringify(this.dealid));
          resolve();
        });
    });
  }


  getRenewalData() {
    if (this.isProductChange) {
      this.renewalData = [];
      localStorage.removeItem("dashboardData");
      this.renewalPage = 1;
      this.indexForDashData = 0;
    }
    let authData = JSON.parse(localStorage.getItem("AuthToken"));
    let index = JSON.parse(localStorage.getItem('dashboardBodyIndex'));
    console.log('For Index', index, this.indexForDashData);
    this.spinloader = true;
    if (this.cs.isUndefineORNull(this.month) && !this.customiseFlag) {
      this.selectedMonth = false;
      swal({
        text: "Kindly Select the Month"
      })
    } else {
      this.selectedMonth = true;
      let body: any;
      body = {
        "imId": authData.username,
        "productCode": this.productCode,
        "dealId": this.dealid,//"DL-3005/1484625",
        "fromDate": moment(this.renewalpolicyDetails.FromDate).format('DD-MM-YYYY'),
        "toDate": moment(this.renewalpolicyDetails.ToDate).format('DD-MM-YYYY'),
        "saod": this.isSAOD,
        "index": this.indexForDashData,
      };
      let str = JSON.stringify(body);
      this.cs.postForRenewals("dashboard", str).then((res: any) => {
        this.isRenewalPolicy = true;
        if (!this.cs.isUndefineORNull(res.message)) {
          localStorage.removeItem("dashboardData");
          this.isProductChange = false;
        } else {
          localStorage.setItem("dashboardData", JSON.stringify(res));
          localStorage.setItem('dashboardBodyIndex', JSON.stringify(this.indexForDashData));
          console.log('dashboraddata', res.length);
          if ((this.cs.isUndefineORNull(res.length)) && res.Message == 'No Data Found') {
            this.spinloader = false;
            this.norecordflag = true;
            this.renewalData = [];
            localStorage.removeItem("dashboardData");
            this.isProductChange = false;
          } else {
            this.spinloader = false;
            this.norecordflag = false;
            this.finalDashBoardData = res;
            if (index == this.indexForDashData) {
              if (this.isProductChange) {
                this.renewalData = res;
                this.isProductChange = false;
              } else {
                this.renewalData = res;
                this.isProductChange = false;
              }
            } else {
              this.renewalData = this.renewalData.concat(res);
              this.isProductChange = false;
            }
            console.log(this.renewalData)
            localStorage.setItem("dashboardData", JSON.stringify(this.renewalData));
          }
        }
      });
    }
  }

  validate() {
    // call one fuction after another function
    this.getDeal().then(() => {
      this.getRenewalData();
    });
  }

  sendPropPdf() {
    let data = JSON.parse(localStorage.getItem("pdfRenewalData"));
    // let vehType = data.VEH_TYPE.replace(/\s/g, "");
    let body = {
      propNo: data.PROPOSAL_NO,
      product: (this.productCode == "2319" || this.productCode == "2320") ? "TP" : "PP",
      vehType: (this.productCode == "2312" || this.productCode == "2320") ? 'TWOWHEELER' : 'PVTCAR',
      policyNo: data.POLICY_NO,
      phoneNo: this.sendDetails.mobileno,
      emailId: this.sendDetails.Email,
      premium: data.PREMIUM,
      regNo: data.VEH_REG_NO,
    };
    let strBody = JSON.stringify(body);
    this.cs.postForRenewals("sendProposalPDFV2", strBody).then((res: any) => {
      console.log(res);
      if (res.emailStatus) {
        swal({ text: "Proposal Pdf has been sent successfully." });
      } else {
        swal({ text: "Failed to send Pdf." });
      }
    });
  }

  pdfRenewalData(pdfData: any) {
    console.log('pdfdata', pdfData);
    this.sendDetails.mobileno = pdfData.MOBILE_NO;
    this.sendDetails.Email = pdfData.EMAIL_ID;
    localStorage.setItem("pdfRenewalData", JSON.stringify(pdfData));
  }

  sendPaymentLink() {
    let sendPayData = JSON.parse(localStorage.getItem("payRenewalData"));
    let body = {
      propNo: sendPayData.PROPOSAL_NO,
      policyNo: sendPayData.POLICY_NO,
      phoneNo: this.sendDetails.mobileno,
      emailId: this.sendDetails.Email,
      premium: sendPayData.PREMIUM,
      regNo: sendPayData.VEH_REG_NO,
    };
    let str = JSON.stringify(body);
    this.cs.postForRenewals("sendPaymentLink", str).then((res: any) => {
      console.log(res);
      if (res.emailStatus) {
        swal({ text: "Payment link has been sent successfully." });
      } else {
        swal({ text: "Failed to send link." });
      }
    });
  }

  paymentLinkPdfData(paymentPdfData: any) {
    console.log(paymentPdfData);
    this.sendDetails.mobileno = paymentPdfData.MOBILE_NO;
    this.sendDetails.Email = paymentPdfData.EMAIL_ID;
    localStorage.setItem("payRenewalData", JSON.stringify(paymentPdfData));
  }

  goToQuote(data: any) {
    console.log('GO to quote', data, data.VEH_TYPE, data.POLICY_TYPE, data.POLICY_NO);
    localStorage.removeItem("regNumber");
    this.api.refreshToken().subscribe(
      (res: any) => {

        let Username,
          Password,
          Expiry,
          Signature,
          iPartner_Token,
          view,
          isSubagent,
          subAID;
        let existingtoken = JSON.parse(localStorage.getItem("AuthToken"));

        if (!this.cs.isUndefineORNull(res)) {
          let body1 = {
            expiry: res.expiry,
            token: res.token,
            username: existingtoken.username,
            applicationAccess: existingtoken.applicationAccess,
          };
          localStorage.setItem("AuthToken", JSON.stringify(body1));
          let paramData = JSON.parse(localStorage.getItem("paramData"));
          paramData.iPartnerLogin.policy = this.policy;
          paramData.iPartnerLogin.product = this.product;
          Username = paramData.Username;
          Password = paramData.Password;
          Expiry = paramData.iPartnerLogin.Expiry;
          Signature = paramData.iPartnerLogin.Signature;
          iPartner_Token = paramData.iPartnerLogin.iPartner_Token;
          view = paramData.iPartnerLogin.view;
          isSubagent = paramData.iPartnerLogin.isSubagent;
          subAID = paramData.iPartnerLogin.subAID;
          let body;
          body = {
            Username: Username,
            Password: Password,
            iPartnerLogin: {
              Expiry: Expiry,
              Signature: Signature,
              iPartner_Token: iPartner_Token,
              product: this.productCode,
              policy: 'ROLL',
              view: view,
            },
          };
          if (isSubagent == "Y") {
            body.iPartnerLogin.isSubagent = isSubagent;
            body.iPartnerLogin.subAID = subAID;
          }
          localStorage.setItem("paramData", JSON.stringify(body));
          localStorage.setItem("regNumber", JSON.stringify(data.VEH_REG_NO));
          this.router
            .navigateByUrl("/", { skipLocationChange: true })
            .then(() => this.router.navigate(["/quote"]));
          this.cs.data$.next({
            flag: true,
          });
        }
      },
      (err) => {
        this.cs.loaderStatus = false;
      }
    );
  }

  // comparePremiumWhileSendLink(data:any): Promise<any>{
  //   return new Promise((resolve: any) => {
  //     // this.spinPayloader = true;
  //     let body = {
  //       policyNo: data.POLICY_NO,
  //       finalPremium: data.PREMIUM,
  //     };
  //     console.log(data.POLICY_NO);
  //     let str = JSON.stringify(body);
  //     this.cs.postForRenewals("comparePremiumV2", str).then((res: any) => {
  //       console.log(res);
  //       this.comparePremiumResp = res;
  //       this.comparePremiumPropNo = res.newProposal;
  //       this.comparePremiumStatus = res.status == "Failed";
  //       if (res.status == "Failed" && res.message == "Cannot proceed to payment due to breakin or deviation scenario") {
  //         swal({
  //           text: "Cannot proceed to payment from here. Kindly go to IAS or Break-in"
  //         })
  //       }else if (res.status == "Failed" && res.message == "Premium mismatch") {
  //         this.newPremium = res.newPremium
  //         let compareMsg1 = "Your old premium amount Rs."
  //         let compareMsg2 = " has been changed to Rs.";
  //         let compareMsg3 = ". Do you want to continue ?";
  //         let finalPremium = this.readPropData.finalPremium;
  //         swal({
  //           text: compareMsg1 + '' + finalPremium + '' + compareMsg2 + '' + res.newPremium + '' + compareMsg3,
  //           dangerMode: true,
  //           closeOnClickOutside: false,
  //           buttons: ["Yes", "No"],
  //         }).then((click) => {
  //           if (!click) {
  //             console.log("Continue");
  //             this.saveProposalForPayment().then(() => {
  //               // this.PlutusPaymentPaymsToken();
  //               resolve();
  //             })
  //           } else {
  //             console.log("Go Back");
  //             resolve();
  //           }
  //         });
  //       }
  //       else if(res.status == "Failed" && res.blazeMessage.includes("Renewed Proposal No")){
  //         let splitMsg = res.blazeMessage.split(":");
  //         let propNo = splitMsg[2];
  //         let policyMsg = "Your policy has been Renewed. Please find renewed Proposal no: "
  //         swal({
  //           text: policyMsg + "" + propNo,
  //         })
  //       }
  //       else if (res.status == "Failed" && res.message == "Premium API returned failure") {
  //         swal({
  //           text: res.blazeMessage,
  //         })
  //       } else {
  //         this.saveProposalForPayment().then(() => {
  //           // this.PlutusPaymentPaymsToken();
  //           resolve();
  //         })

  //       }
  //     });
  //   });
  // }

  // sendPaymentLinkNew() {
  //   let sendPayData = JSON.parse(localStorage.getItem("payRenewalData"));
  //   let body = {
  //     propNo: this.comparePremiumResp.PROPOSAL_NO,
  //     policyNo: this.comparePremiumResp.POLICY_NO,
  //     phoneNo: this.sendDetails.mobileno,
  //     emailId: this.sendDetails.Email,
  //     premium: this.comparePremiumResp.PREMIUM,
  //     regNo: this.comparePremiumResp.VEH_REG_NO,
  //   };
  //   let str = JSON.stringify(body);
  //   this.cs.postForRenewals("sendPaymentLink", str).then((res: any) => {
  //     console.log(res);
  //     if (res.emailStatus) {
  //       swal({ text: "Payment link has been sent successfully." });
  //     } else {
  //       swal({ text: "Failed to send link." });
  //     }
  //   });
  // }

  modifyProposal(modifyData: any) {
    console.log(modifyData);
    localStorage.setItem("renewalData", JSON.stringify(modifyData));
    localStorage.setItem("payRenewalData", JSON.stringify(modifyData));
    this.router.navigateByUrl("renewalmodifyproposal");
  }

  readProposal(data: any, isRead: any): Promise<any> {
    return new Promise((resolve: any) => {
      // this.spinPayloader = true;
      let body = {
        policyNo: data.POLICY_NO,//"3005/51925075/00/B02",//"3005/2425/6999/B00", 
      };
      let str = JSON.stringify(body);
      this.cs.postForRenewals("readProposalV2", str).then((res: any) => {
        console.log(1, res);
        this.readPropData = res;
        this.renewalData = res;
        localStorage.setItem("renewalData", JSON.stringify(data));
        localStorage.setItem("payRenewalData", JSON.stringify(data));
        this.cs.isShowBackButton = isRead;
        if (isRead) {
          this.router.navigate(["renewalPayment"]);
        }
        resolve();
      });
    });
  }

  comparePremium(data:any): Promise<any> {
    return new Promise((resolve: any) => {
      // this.spinPayloader = true;
      let body = {
        policyNo: data.POLICY_NO,//this.readPropData.PolicyNumber,
        finalPremium: data.PREMIUM//this.readPropData.finalPremium,
      };
      console.log(data.POLICY_NO);
      let str = JSON.stringify(body);
      this.cs.postForRenewals("comparePremiumV2", str).then((res: any) => {
        // this.spinPayloader = false;
        console.log(res);
        this.comparePremiumResp = res;
        this.comparePremiumPropNo = res.newProposal;
        this.comparePremiumStatus = res.status == "Failed";
        // console.log('splitMsg',splitTpStartDate);
        if (res.status == "Failed" && res.message == "Cannot proceed to payment due to breakin or deviation scenario") {
          swal({
            text: "Cannot proceed to payment due to breakin or deviation scenario."
          })
        } else if (res.status == "Failed" && res.message == "Premium mismatch") {
          this.newPremium = res.newPremium
          let compareMsg1 = "Your old premium amount Rs."
          let compareMsg2 = " has been changed to Rs.";
          let compareMsg3 = ". Do you want to continue ?";
          let finalPremium = this.readPropData.finalPremium;
          swal({
            text: compareMsg1 + '' + finalPremium + '' + compareMsg2 + '' + res.newPremium + '' + compareMsg3,
            dangerMode: true,
            closeOnClickOutside: false,
            buttons: ["Yes", "No"],
          }).then((click) => {
            if (!click) {
              console.log("Continue");
              this.saveProposalForPayment().then(() => {
                this.PlutusPaymentPaymsToken();
                resolve();
              })
            } else {
              console.log("Go Back");
              resolve();
            }
          });
        }
        else if (res.status == "Failed" && res.blazeMessage.includes("Renewed Proposal No")) {
          let splitMsg = res.blazeMessage.split(":");
          let propNo = splitMsg[2];
          let policyMsg = "Your policy has been Renewed. Please find renewed Proposal no: "
          swal({
            text: policyMsg + "" + propNo,
          })
        }
        else if (res.status == "Failed" && res.message == "Premium API returned failure") {
          swal({
            text: res.blazeMessage,
          })
        } else {
          this.saveProposalForPayment().then(() => {
            this.PlutusPaymentPaymsToken();
            resolve();
          })

        }
      });
    });
  }

  saveProposalForPayment(): Promise<any> {
    return new Promise((resolve: any) => {
      let data = JSON.parse(localStorage.getItem('payRenewalData'))
      if (this.productCode == "2312" || (data.POLICY_TYPE == 'PP' && data.VEH_TYPE == 'TWOWHEELER')) {
        this.policySubType = "1";
      } else if (this.productCode == "2311" || (data.POLICY_TYPE == 'PP' && data.VEH_TYPE == 'PVTCAR')) {
        this.policySubType = "2";
      } else if (this.productCode == "2319" || (data.POLICY_TYPE == 'TP' && data.VEH_TYPE == 'PVTCAR')) {
        this.policySubType = "10";
      } else if (this.productCode == "2320" || (data.POLICY_TYPE == 'TP' && data.VEH_TYPE == 'TWOWHEELER')) {
        this.policySubType = "9";
      }
      let body = {
        PolicyType: 1,
        PolicySubType: parseInt(this.policySubType),
        ProposalNumber: this.comparePremiumPropNo,
        PolicyStartDate: this.comparePremiumResp.internalRequest.ProposalDetails.PolicyStartDate,
        PolicyEndDate: this.comparePremiumResp.internalRequest.ProposalDetails.PolicyEndDate,
        BasicPremium: this.comparePremiumResp.internalResponse.riskDetails.basicOD,
        Discount: this.comparePremiumResp.internalResponse.riskDetails.bonusDiscount,
        ServiceTax: this.comparePremiumResp.internalResponse.totalTax,
        TotalPremium: this.comparePremiumResp.internalResponse.finalPremium,
        Status: this.comparePremiumStatus ? 6 : 4,
        DealID: this.comparePremiumResp.internalRequest.DealID,
        TOTALTAX: this.comparePremiumResp.internalResponse.totalTax,
        TransFor: "Renewal",
        // TRANS_ON_TS: time,
        PolicyTenure: parseInt(this.comparePremiumResp.internalResponse.generalInformation.tenure),
        PolicyTpTenure: parseInt(this.comparePremiumResp.internalResponse.generalInformation.tpTenure),
        PolicyPACoverTenure: !this.cs.isUndefineORNull(this.comparePremiumResp.internalRequest.ProposalDetails.CustomerDetails.PACoverTenure) ? this.comparePremiumResp.internalRequest.ProposalDetails.CustomerDetails.PACoverTenure : 1,
        CustomerID: parseInt(this.comparePremiumResp.internalRequest.CustomerID),
        PFCustomerID: parseInt(this.comparePremiumResp.internalResponse.generalInformation.customerId),
        CustomerName: this.readPropData.CustomerDetails.CustomerName,
        PackagePremium: this.comparePremiumResp.internalResponse.packagePremium,
        TotalLiablityPremium: this.comparePremiumResp.internalResponse.totalLiabilityPremium,
        ProposalRS: JSON.stringify(this.comparePremiumResp.internalResponse),
        CorrelationId: JSON.stringify(this.comparePremiumResp.internalRequest.CorrelationId),
        CustomerType: JSON.stringify(this.comparePremiumResp.internalRequest.ProposalDetails.CustomerDetails.CustomerType),
        ProposalRQ: JSON.stringify(this.comparePremiumResp.internalRequest),
        InspectionID: this.comparePremiumResp.internalRequest.interactionDetails ? this.comparePremiumResp.internalRequest.interactionDetails.inspection_id : 0,
        InspectionStatus: this.comparePremiumResp.internalRequest.interactionDetails ? this.comparePremiumResp.internalRequest.interactionDetails.inspection_status : 0,
        InspectionType: null,
        IsStandaloneOD: 0,
        subLocation: "",
        TPStartDate: this.comparePremiumResp.internalRequest.ProposalDetails.TPStartDate,
        TPEndDate: this.comparePremiumResp.internalRequest.ProposalDetails.TPEndDate,
        TPPolicyNum: "",//this.comparePremiumResp,
        TPInsurerName: this.comparePremiumResp.internalRequest.ProposalDetails.TPInsurerName,
        IsSubAgent: 0,
        SubAgentIpartnerUserID: "",
        IDVIASID: this.comparePremiumResp.internalRequest.interactionDetails ? this.comparePremiumResp.internalRequest.interactionDetails.idv_ias_id : "",
        IDVIASSRT: "",
        IDVIASSUBSRT: "",
        ODIASID: this.comparePremiumResp.internalRequest.interactionDetails ? this.comparePremiumResp.internalRequest.interactionDetails.od_ias_id : "",
        ODIASSRT: "",
        ODIASSUBSRT: "",
        ODIASRQ: "",
        ODIASRS: "",
        IDVIASRQ: "",
        IDVIASRS: "",

      }
      let str = JSON.stringify(body);
      this.cs.postForRenewalsPayment("Proposal/SaveRNProposal", str).then((res: any) => {
        console.log(res);
        resolve();

      })
    })
  }

  PlutusPaymentPaymsToken() {
    this.cs.getPaymsToken().then((resp: any) => {
      localStorage.setItem("paymsToken", JSON.stringify(resp.accessToken));
      this.redirectToPlutusPayment();
    });
  }

  redirectToPlutusPayment() {
    let bankType = this.readPropData.BankType;
    // console.log('banktype',bankType);
    let instaFlag;
    if (bankType == "INT") {
      instaFlag = 1;
    } else {
      instaFlag = 0;
    }

    let email = this.cs.encrypt(
      this.readPropData.CustomerDetails.Email,
      commonData.aesSecretKey
    );
    let phone = this.cs.encrypt(
      this.readPropData.CustomerDetails.MobileNumber,
      commonData.aesSecretKey
    );
    let body = {
      CorrelationID: this.readPropData.CorrelationId,
      Amount: !this.cs.isUndefineORNull(this.newPremium) ? this.newPremium : this.readPropData.finalPremium,
      ProposalNo: this.comparePremiumPropNo,//this.readPropData.proposalNumber,
      DealID: this.readPropData.DealID,
      CustomerID: this.readPropData.CustomerID,
      Email: email, //this.readPropData.CustomerDetails.Email,
      ContactNo: phone,//this.readPropData.CustomerDetails.MobileNumber,
      UserFlag: 0,
      IsInsta: instaFlag,
      MultiFlag: 0,
      PidFlag: 0,
      PreInsta: 0,
      notes: {
        PreviousPolicyNo: this.readPropData.PolicyNumber,
        IsRenewal: "1"
      }
    };

    let str = JSON.stringify(body);
    this.cs.postPayms("Redirection/AddPaymentRequest", str).then((res: any) => {
      // console.log(res);
      if (res.Status == "Success" || res.Status == "success") {
        window.location.href = res.URL;
      } else {
        swal({ text: "Try again later!" });
      }
    });
  }

  paymentValidate(data: any) {
    console.log(data);
    this.readProposal(data, false).then(() => {
      this.comparePremium(data);
    });
  }

}
