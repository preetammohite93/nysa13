import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { commonData } from "src/app/commonData/commonData";
import swal from "sweetalert";
import { ApiServiceService } from 'src/app/services/api-service.service';
import * as moment from 'moment';
// import { ActivatedRoute, Router } from '@angular/router';
// import { Guid } from 'guid-typescript';
// import { ApiServiceService } from 'src/app/services/api-service.service';
// import { CommonService } from 'src/app/services/common.service';
// import swal from "sweetalert";

@Component({
  selector: 'app-ncb-payment',
  templateUrl: './ncb-payment.component.html',
  styleUrls: ['./ncb-payment.component.css']
})
export class NcbPaymentComponent implements OnInit {
  correlationId:any;
  tokenForNCB: any;
  dontShowPage: boolean = true;
  paymentTokan: any
  CustToken: any;
  ncbPaymentToken: any;
  ncbLinkDetails: any;
  dealDetailsfromIM: any;
  bankType: any;
  sendDetails = {
    Email: "",
    mobileno: "",
  };

  constructor(public router: Router,
    public cs: CommonService,
    public activeRoute: ActivatedRoute,public api: ApiServiceService,) { }

  ngOnInit() {
    this.getParams().then(() =>{
        // this.getCustomerDetails().then(() =>{
        //   this.getNcbPaymentTokan();
        // })
        this.getCustomerDetails();
      })
  }


  getParams(): Promise<any> {
    return new Promise((resolve: any) => {
      this.activeRoute.queryParams.forEach((params) => {
        this.CustToken = params.CustToken;
        localStorage.setItem("custToken", this.CustToken);
        resolve();
      });
    });
  }
  
  getCustomerDetails(): Promise<any>{
    return new Promise((resolve:any) =>{
      this.cs.getWithAuth(
        "customer/GetNCBCaseDetails?CustToken=" + this.CustToken 
      ).then((res: any) => {
        this.ncbLinkDetails = res;
        // this.tokenForNCB = res.token;
        localStorage.setItem('NCBToken', JSON.stringify(res.token));
        console.log('ncbLInk',this.ncbLinkDetails);
        resolve();
      }).catch((err: any) => {
        swal({
          title: "Error!",
          text: "Your Payment link has been expired",
        });
        this.dontShowPage = false;
      
      });
    })
    }
//new ncb changes this.cs.getForNcbDashboard
  // getCustomerDetails(): Promise<any>{
  // return new Promise((resolve:any) =>{
  //   this.cs.getForNcbDashboard(
  //     "GetNCBCaseDetails?CustToken=" + this.CustToken 
  //   ).then((res: any) => {
  //     this.ncbLinkDetails = res;
  //     // this.tokenForNCB = res.token;
  //     // localStorage.setItem('NCBToken', JSON.stringify(res));
  //     console.log('ncbLInk',this.ncbLinkDetails);
  //     resolve();
  //   }).catch((err: any) => {
  //     swal({
  //       title: "Error!",
  //       text: "Your Payment link has been expired",
  //     });
  //     this.dontShowPage = false;
    
  //   });
  // })
  // }

  // getNcbPaymentTokan(): Promise<any>{
  //   return new Promise((resolve: any)=>{
  //     this.cs.getForNcbDashboard("CreateNCBPaymentToken?DealId=" + this.ncbLinkDetails.deal_id ).then((res:any)=>{
  //       localStorage.setItem('NCBPayToken', JSON.stringify(res.token));
  //       resolve();
  //     })
  //   })
  // }

  // getNcbPayToken(){
  //     this.cs.getForNcbDashboard("token_for_ncbPayment?DealID=" + "DEAL-3001-0000006").then((res:any)=>{
  //       this.ncbPaymentToken = res;    
  //   })
  // }
  ValidatePayment(){
   let date = moment(new Date()).format("MM-DD-YYYY");
  //  console.log('todaydate',date);
   let proposalDate = moment(this.ncbLinkDetails.proposal_date).format("MM-DD-YYYY");
  //  console.log('todaydate1',proposalDate);
  if( this.ncbLinkDetails.pid != ""){
    swal({text: "Your payment already done."});
  }else{
    if(date != proposalDate){
      swal({text:"Your link was expired, Kindly generate link again."});
    }else{
    this.PlutusPaymentPaymsToken();
   }
  }
}


  PlutusPaymentPaymsToken() {
    this.cs.getPaymsTokenNCBPage().then((resp: any) => {
      localStorage.setItem("paymsToken", JSON.stringify(resp.accessToken));
      this.redirectToPlutusPayment();
    });
  }

  redirectToPlutusPayment(){
    // console.log('banktype',bankType);
    // for user no need insta , smart collect and lombard pay
    // let instaFlag;
    // if (this.bankType == "INT") {
    //   instaFlag = 1;
    // } else {
    //   instaFlag = 0;
    // }

    let email = this.cs.encrypt(
      this.ncbLinkDetails.email_id,
      commonData.aesSecretKey
    );
    let phone = this.cs.encrypt(
      this.ncbLinkDetails.mobile_no,
      commonData.aesSecretKey
    );
    let body = {
      CorrelationID: this.ncbLinkDetails.correlation_id,
      Amount: this.ncbLinkDetails.ncb_amount,
      ProposalNo: this.ncbLinkDetails.proposal_no,//this.readPropData.proposalNumber,
      DealID: this.ncbLinkDetails.deal_id,
      CustomerID: this.ncbLinkDetails.customer_id,
      Email: email, //this.readPropData.CustomerDetails.Email,
      ContactNo: phone,//this.readPropData.CustomerDetails.MobileNumber,
      UserFlag: 1,
      IsInsta: 0,
      MultiFlag: 0,
      PidFlag: 0,
      notes: {
        IsNcb: "true"
      }
    };

    let str = JSON.stringify(body);
    this.cs.postPayms("Redirection/AddPaymentRequest", str).then((res: any) => {
      // console.log(res);
      if (res.Status == "Success" || res.Status == "success") {
        window.location.href = res.URL;
      } else {
        swal({ text: "Try again later!" });
      }
    });
  }
  
}
