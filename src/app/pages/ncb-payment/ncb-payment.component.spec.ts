import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NcbPaymentComponent } from './ncb-payment.component';

describe('NcbPaymentComponent', () => {
  let component: NcbPaymentComponent;
  let fixture: ComponentFixture<NcbPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NcbPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NcbPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
