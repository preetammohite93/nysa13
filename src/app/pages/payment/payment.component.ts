import { Component, OnInit, ChangeDetectorRef, NgZone } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import * as moment from "moment";
import { commonData } from "../../commonData/commonData";
import { CommonService } from "src/app/services/common.service";
import { ApiServiceService } from "src/app/services/api-service.service";
declare var Razorpay: any;
import swal from "sweetalert";
import {
  trigger,
  transition,
  animate,
  keyframes,
  style,
} from "@angular/animations";
import { SendMessageService } from "src/app/services/send-message.service";
import { CommonMessageService } from "src/app/services/common-message.service";
declare var $: any;
declare const Buffer;

@Component({
  selector: "app-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.css"],
  animations: [
    trigger("flyInOut", [
      transition("void => fly", [
        animate(
          500,
          keyframes([
            style({ transform: "translateY(100%)", opacity: 0 }),
            style({ transform: "translateY(0)", opacity: 1 }),
          ])
        ),
      ]),
    ]),
  ],
})
export class PaymentComponent implements OnInit {
  isName: any;
  isEmail: any;
  isContact: any;
  startDate1: any;
  options: any;
  orderId: any;
  calculatedData: any;
  quoteReq: any;
  quoteRes: any;
  propReq: any;
  propRes: any;
  startDate: any;
  todayDate: any;
  finalamount: any;
  payButton: boolean = false;
  paramDataValues: any;
  authtok: any;
  policyType: any;
  productCode: any;
  type: any;
  product: any;
  PFPayReq: any;
  PFPayRes: any;
  totalTax: any;
  bankingidData: any;
  paylaterRes: any;
  updatePayment: any;
  paymentMode: any;
  custName: any;
  custMobile: any;
  custEmail: any;
  PayKey: any;
  customerEmail: any;
  agentflow: boolean = false;
  orderId1: any;
  lombardpaybalance: any;
  paylombard: any;
  custLink: any;
  CustToken: any;
  custLinkDetails: any;
  showSendLink: boolean = true;
  sendLinkstatus: any;
  SubProductType: any;
  timeTaken: any;
  timeseconds: any;
  PaymentMode: any;
  bankType: any;
  IsInstaDone: boolean = false;
  instaData: any;
  instaDealDetails: any;
  difference: any;
  showInstadiv: boolean = false;
  breakinFlag: any;
  dataforBack: any;
  isSMSSent: any;
  showcustbutton: any;
  mobNumberPattern = "^(0/91)?[6-9][0-9]{9}$";
  sendCustPaymentLinkFlag: boolean = false;
  addlombardmoneyFlag: boolean = false;
  paymentModeFlag: boolean = true;
  proposalRes: any;
  watchMe: any;
  // Payment Model
  paymentDetails = {
    Name: "",
    Email: "",
    Contact: "",
    balance: "",
    amount: "",
  };
  customLink = { custEmail: "" };
  starRatingFeed: any;
  policySubType: any;
  feedBackResponse: any;
  IsPayLinkSent: string;
  PayLinkReciever: any;
  CustomerPayLink: any;
  userdetails: any;

  /**
   * Digital POS Variable
   */
  IsPOSTransaction_flag: boolean = false;
  posDealID: string;
  deal_Id: any;
  isSiteDown = false;

  constructor(
    public router: Router,
    public cs: CommonService,
    public activeRoute: ActivatedRoute,
    public api: ApiServiceService,
    private changeDetector: ChangeDetectorRef,
    private messageservice: SendMessageService,
    public msgService: CommonMessageService,
    private zone: NgZone
  ) {
    let dateStart = JSON.parse(localStorage.getItem("AppStartTime"));
    let date2 = new Date();
    let timeDiff = date2.getTime() - dateStart;
    this.convertTime(timeDiff);
  }

  ngOnInit() {
    this.PayKey = commonData.PayKey;
    window.scrollTo(0, 0);
    $("aside").removeClass("active");
    this.instaData = JSON.parse(localStorage.getItem("InstaData"));

    this.bankType = localStorage.getItem("bankType");

    this.dataforBack = JSON.parse(localStorage.getItem("dataforBack"));

    // this.showcustbutton = localStorage.getItem('DisablecustLink');

    localStorage.removeItem("addPolicyDetails");
    localStorage.removeItem("updatePaymentDetails");
    localStorage.removeItem("oldreference");
    // localStorage.removeItem("breakinFlag");
    // let localAuth = JSON.parse(localStorage.getItem('AuthToken'));
    // if (!this.cs.isUndefineORNull(localAuth)) {
    //   if (this.cs.checkTokenValidity()) {
    //     this.cs.geToLogin();
    //   } else {
    //   }
    // } else {
    // }

    // let updateBalance = JSON.parse(localStorage.getItem('LombardBalance'));
    // this.lombardpaybalance = updateBalance;
    if (this.sendLinkstatus != true) {
      this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
      this.authtok = JSON.parse(localStorage.getItem("AuthToken"));
      this.policyType = this.paramDataValues.iPartnerLogin.policy;
      this.productCode = this.paramDataValues.iPartnerLogin.product;
      if (this.policyType == "NEW") {
        this.type = "New";
      } else {
        this.type = "Rollover";
      }
      if (this.productCode == "2312") {
        this.product = "Two Wheeler";
      } else if (this.productCode == "2311") {
        this.product = "Four Wheeler";
      } else if (this.productCode == "2320") {
        this.product = "Two Wheeler Liability";
      } else if (this.productCode == "2319") {
        this.product = "Four Wheeler Liability";
      }
    }

    if (localStorage.getItem("calQuoteReq")) {
      this.quoteReq = JSON.parse(localStorage.getItem("calQuoteReq"));
    } else {
      this.messageservice.ReceieveQuotereqData().subscribe((data: any) => {
        this.propReq = data;
        localStorage.setItem("calQuoteReq", JSON.stringify(data));
      });
    }

    if (localStorage.getItem("calQuoteRes")) {
      // Changes by preetam
      this.quoteRes = JSON.parse(localStorage.getItem("calQuoteRes"));
    } else {
      this.messageservice.ReceieveQuoteresData().subscribe((data: any) => {
        this.propRes = data;
        localStorage.setItem("calQuoteRes", JSON.stringify(data));
      });
    }

    // if (JSON.parse(localStorage.getItem('calQuoteReq'))) {
    //   this.quoteReq = JSON.parse(localStorage.getItem('calQuoteReq'));
    //   this.quoteRes = JSON.parse(localStorage.getItem('calQuoteRes'));
    // } else {
    //   this.quoteReq = JSON.parse(localStorage.getItem('calQuoteTPReq'));
    //   this.quoteRes = JSON.parse(localStorage.getItem('calQuoteTPRes'));
    // }

    if (JSON.parse(localStorage.getItem("calQuoteTPReq"))) {
      this.quoteReq = JSON.parse(localStorage.getItem("calQuoteTPReq"));
      this.quoteRes = JSON.parse(localStorage.getItem("calQuoteTPRes"));
    }

    if (localStorage.getItem("PropDatareq")) {
      this.propReq = JSON.parse(localStorage.getItem("PropDatareq"));
    } else {
      this.messageservice.ReceieveproposalreqData().subscribe((data: any) => {
        this.propReq = data;
        localStorage.setItem("PropDatareq", JSON.stringify(data));
      });
    }

    if (localStorage.getItem("PropDatares")) {
      this.propRes = JSON.parse(localStorage.getItem("PropDatares"));
    } else {
      this.messageservice.ReceieveproposalresData().subscribe((data: any) => {
        this.propRes = data;
        localStorage.setItem("PropDatares", JSON.stringify(data));
      });
    }

    this.finalamount = Math.round(this.propRes.finalPremium);

    this.startDate = moment(new Date()).format("YYYY-MM-DD");
    this.todayDate = moment(new Date()).format("YYYY-MM-DD");

    this.calculatedData = "";
    /**
     * Digital POS change
     * get values from localStorage for checking if conditions in html file
     * author :- digitalPOS
     * date :- 14-06-2021
     */
    if (localStorage.getItem("IsPOSTransaction")) {
      this.IsPOSTransaction_flag = JSON.parse(
        localStorage.getItem("IsPOSTransaction")
      );
      this.showcustbutton = false;
    } else {
      this.showcustbutton = localStorage.getItem("DisablecustLink");
    }

    /**
     * Digital POS change
     * call getDealDetails function only for nysa not for POS
     * author :- digitalPOS
     * date :- 15-06-2021
     */
    if (this.IsPOSTransaction_flag == true) {
      this.posDealID = JSON.parse(localStorage.getItem("DealID"));
    } else {
      this.getDealDetails();
    }

    // this.checkBankingid();

    let savedQuote = localStorage.getItem("savedQuotes");
    if (savedQuote == "true") {
      let savedquotedata = JSON.parse(localStorage.getItem("saveQuoteData"));
      this.productCode = savedquotedata.productCode;
    }

    if (this.sendLinkstatus == true) {
      this.cs.getPayAuth().then((resp: any) => {
        this.payButton = true;
        localStorage.setItem("PayAuthToken", JSON.stringify(resp.accessToken));
        // this.cs.loaderStatus = false;
      });
    } else {
      // this.checkBankingid().then(() => {
      // this.cs.loaderStatus = true;
      this.cs.getPayAuth().then((resp: any) => {
        this.payButton = true;
        localStorage.setItem("PayAuthToken", JSON.stringify(resp.accessToken));
        // this.cs.loaderStatus = false;
      });
      // });
    }

    this.cs.getlombardPay().then((resp: any) => {
      localStorage.setItem(
        "LombardPayAuthToken",
        JSON.stringify(resp.accessToken)
      );
      this.showBalance();
      this.userDetails();
    });

    let breakinFlag = localStorage.getItem("breakinFlag");

    if (breakinFlag == "true") {
      this.breakinInspectionClear();
    }

    if (!this.cs.isUndefineORNull(this.propReq.CustomerDetails.Email)) {
      this.customerEmail = this.propReq.CustomerDetails.Email;
    }

    if (!this.cs.isUndefineORNull(this.propReq.CustomerDetails.MobileNumber)) {
      this.custMobile = this.propReq.CustomerDetails.MobileNumber;
    }

    this.checkCrawler();
  }

  // Check crawler enable or not
  checkCrawler() {
    // //CreateToken + GetApplicationAccess merging code - Sejal
    if (!this.cs.isUndefineORNull(localStorage.getItem("AuthToken"))) {
      let encryptedTokenaccess = JSON.parse(localStorage.getItem("AuthToken"));
      let Parsedtokenaccess: any = this.api.decrypt(
        encryptedTokenaccess.applicationAccess
      );
      let tokenaccess = JSON.parse(Parsedtokenaccess);
      this.cs.isPlutusEnable = tokenaccess.isEnablePlutus;
      this.isSiteDown = tokenaccess.isSiteDown;
    }

    // this.cs.get('Access/GetApplicationAccess').subscribe((res: any) => {
    //   console.log('Response', res);
    //   localStorage.setItem('AccessFor', JSON.stringify(res));
    //   this.isSiteDown = res.isSiteDown;
    // })
  }

  customerData(mode: any) {
    console.log('customer Data', )
    var customerDetails = JSON.parse(localStorage.getItem('CustomerDetails'))
    this.custName = this.propReq.CustomerDetails.CustomerName;
    this.custMobile = customerDetails.CustomerMobile || this.propReq.CustomerDetails.MobileNumber;
    this.custEmail = this.propReq.CustomerDetails.Email;
    this.paymentMode = mode;
    this.PaymentMode = "1";
    this.showInstadiv = false;
    this.pay();
    (<any>window).ga("send", "event", {
      eventCategory: "Payment Mode",
      eventLabel: this.paymentMode + "" + this.product + "" + this.policyType,
      eventAction: "Payment Mode Clicked",
      eventValue: 10,
    });
  }

  //Get Deal Details from deal
  getDealDetails() {
    this.api.getDealDetails(this.propReq.DealId).subscribe((res: any) => {
      if (res.status == "FAILED") {
        // swal({ text: 'Deal Details not found' })
        console.log("Deal not found");
      } else {
        this.bankType = res.bankType;
        localStorage.setItem("bankType", this.bankType);
      }
    });
  }

  payLater() {
    // this.cs.loaderStatus = true;
    this.cs
      .get(
        "Payment/PayLater?proposalNo=" +
          this.propRes.generalInformation.proposalNumber
      )
      .subscribe((res: any) => {
        this.paylaterRes = res;
        swal({
          closeOnClickOutside: false,
          text:
            "Your proposal number is:" +
            this.propRes.generalInformation.proposalNumber +
            " you can pay from the pending payment section later",
        }).then(() => {
          this.router.navigateByUrl("quote");
        });
      });
  }

  // Razor Pay Call
  pay() {
    if (this.sendLinkstatus == true) {
      this.createOrder().then(() => {
        this.payByRazorPay(this.router);
      });
    } else {
      //   if(this.bankingidData.bankingStatus == 'UNLOCKED')
      // {
      this.createOrder().then(() => {
        this.payByRazorPay(this.router);
      });
      // } else {
      //   swal({

      //     text: "Cannot process further as your banking id is locked"
      //   }).then(()=> {
      //     this.goToDashboard();
      //   });
      // }
    }
  }

  checkBankingid(): Promise<any> {
    return new Promise((resolve: any) => {
      /**
       * DigitalPOS Change
       */
      if (this.IsPOSTransaction_flag == true) {
        this.posDealID = JSON.parse(localStorage.getItem("DealID"));
        this.deal_Id = this.posDealID;
      } else {
        this.deal_Id = this.propReq.DealId;
      }
      let body = {
        DealID: this.deal_Id,
        Correlation: this.propRes.correlationId,
      };
      let str = JSON.stringify(body);
      this.cs
        .post("Payment/FetchDealStatusDetails", str)
        .then((res: any) => {
          if (res.bankingStatus == "UNLOCKED" && res.status == "SUCCESS") {
            this.bankingidData = res;
            resolve();
          } else {
            resolve();
            swal({
              closeOnClickOutside: false,
              text: "Cannot go further as banking id is locked",
            });
            // .then(()=> {
            //   this.router.navigateByUrl('quote');
            // });
          }
        })
        .catch((err: any) => {
          resolve();
          swal({
            closeOnClickOutside: false,
            text: err,
          });
        });
    });
  }

  getinstaDetails(): Promise<any> {
    return new Promise((resolve: any) => {
      /**
       * DigitalPOS Change
       */
      if (this.IsPOSTransaction_flag == true) {
        this.posDealID = JSON.parse(localStorage.getItem("DealID"));
        this.deal_Id = this.posDealID;
      } else {
        this.deal_Id = this.propReq.DealId;
      }

      this.cs
        .get("Deal/GetDealStatus?dealID=" + this.deal_Id)
        .subscribe((res: any) => {
          this.instaDealDetails = res;
          resolve();
        });
    });
  }

  breakinInspectionClear() {
    let breakindata = JSON.parse(localStorage.getItem("breakinData"));
    let dealId = JSON.parse(localStorage.getItem("DealID"));
    let startDate = moment(new Date()).format("DD-MM-YYYY");

    let body = {
      InspectionId: JSON.parse(breakindata.inspectionID),
      DealNo: this.propReq.DealId,
      ReferenceDate: startDate, //this.propRes.generalInformation.proposalDate,
      InspectionStatus: "OK",
      CorrelationId: breakindata.corelationID,
      ReferenceNo: JSON.parse(this.propRes.generalInformation.proposalNumber),
    };
    let str = JSON.stringify(body);
    this.cs.createBizToken().then(() => {
      this.cs.postBiz("/breakin/clearinspectionstatus", str).subscribe(
        (res: any) => {},
        (err) => {
          // swal({ closeOnClickOutside: false, text: err });
          let errMsg = this.msgService.errorMsg(err)
          swal({ closeOnClickOutside: false, text: errMsg });
          let errorbody = {
            RequestJson: JSON.stringify(body),
            ResponseJson: JSON.stringify(err),
            ServiceURL:
              commonData.bizURL + "/proposal/twowheelercreateproposal",
            CorrelationID: breakindata.corelationID,
          };
          this.api.adderrorlogs(errorbody);
        }
      );
    });

    // .catch((err: any) => {
    //   swal({
    //     closeOnClickOutside: false,
    //     text: err
    //   });
    // });
  }

  // Create Order Service
  createOrder(): Promise<any> {
    return new Promise((resolve: any) => {
      this.saveorderReq();

      let body = {
        CorrelationID: this.propRes.correlationId, //CorelationID
        // "amount": this.cs.encrypt(JSON.stringify(this.finalamount), commonData.aesnysaKey), //Premium Amount
        amount: JSON.stringify(this.finalamount), //Premium Amount
        identifier:
          "ipartner_policy_" + this.propRes.generalInformation.proposalNumber, //Proposal Number
      };
      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs.post1("Payment/CreateOrder", str).then((response: any) => {
        if (response.status == "created") {
          this.orderId = response.id;
          localStorage.setItem("orderReq", JSON.stringify(body));
          localStorage.setItem("orderRes", JSON.stringify(response));

          this.cs.loaderStatus = false;
          resolve();
        } else {
          resolve();
          this.cs.loaderStatus = false;
        }
      });
    });
  }

  // RazorPay Call
  payByRazorPay(route: any) {
    var desc = "ICICI Lombard - Motor Insurance";
    let self = this;
    this.options = {
      description: desc,
      image: "https://www.icicilombard.com/mobile/mclaim/images/favicon.ico",
      currency: "INR",
      key: this.PayKey,
      order_id: this.orderId,
      method: {
        netbanking: {
          order: ["ICIC", "HDFC", "SBIN", "UTIB", "IDFB", "IBKL"],
        },
      },
      prefill: {
        email: this.custEmail,
        contact: this.custMobile,
        name: this.custName,
        method: this.paymentMode,
      },
      theme: {
        color: "#E04844",
        hide_topbar: true,
      },
      handler: function (response: any) {
        /**
         * digitalPOS Change
         * 21-10-2021
         */
        if (!localStorage.getItem("IsPOSTransaction")) {
          self.getPayAuthTokenPOS(response, self, route);
        } else {
          self.createPFPaymentAsync(response, self, route);
        }
      },
      modal: {
        ondismiss: function () {
          this.cs.loaderStatus = false;
        },
      },
    };
    var rzp1 = new Razorpay(this.options);
    rzp1.open();
  }

  /**
   * getPayAuthToken For Mobile App
   * digitalPOS
   * 21-10-2021
   */
  getPayAuthTokenPOS(res: any, _self: any, route: any) {
    this.cs.getPayAuth().then((resp: any) => {
      console.info(resp.accessToken);
      this.createPFPaymentAsyncPOS(res, _self, route, resp.accessToken);
    });
    // if (localStorage.getItem('PayAuthToken')) {
    //   this.createPFPaymentAsync(res, _self, route)
    // } else {
    //   this.cs.getPayAuth().then((resp: any) => {
    //     this.createPFPaymentAsyncPOS(res, _self, route, resp.accessToken)
    //   });
    // }
  }

  saveorderReq() {
    let bankType = localStorage.getItem("bankType");
    let body;

    /**
     * DigitalPOS Change
     */
    if (this.IsPOSTransaction_flag == true) {
      this.posDealID = JSON.parse(localStorage.getItem("DealID"));
      this.deal_Id = this.posDealID;
    } else {
      this.deal_Id = this.propReq.DealId;
    }

    if (
      bankType == "INT" &&
      (this.instaData == "" ||
        this.instaData == undefined ||
        this.instaData == null)
    ) {
      body = {
        isMappingRequired: true,
        isTaggingRequired: true,
        CorrelationId: this.propRes.correlationId,
        DealId: this.deal_Id,
        PaymentEntry: {
          onlineDAEntry: {
            CorrelationId: this.propRes.correlationId,
            DealId: this.deal_Id,
            CustomerID: this.propRes.generalInformation.customerId,
            MerchantID: commonData.MerchantID,
            TransactionId: commonData.TransactionId,
            PaymentAmount: String(this.finalamount),
            InstrumentDate: this.startDate,
            ReceiptDate: this.startDate,
          },
        },
        PaymentTagging: {
          DealID: this.deal_Id,
          CorrelationID: this.propRes.correlationId,
          customerProposal: [
            {
              CustomerID: this.propRes.generalInformation.customerId,
              ProposalNo: this.propRes.generalInformation.proposalNumber,
            },
          ],
        },
        PaymentMapping: {
          DealID: this.deal_Id,
          CorrelationID: this.propRes.correlationId,
          customerProposal: [
            {
              CustomerID: this.propRes.generalInformation.customerId,
              ProposalNo: this.propRes.generalInformation.proposalNumber,
            },
          ],
        },
      };
    } else if (bankType == "INT" && this.instaData.instaPolicyNo) {
      body = {
        isMappingRequired: true,
        isTaggingRequired: false,
        CorrelationId: this.propRes.correlationId,
        DealId: this.deal_Id,
        PaymentEntry: {
          onlineDAEntry: {
            CorrelationId: this.propRes.correlationId,
            DealId: this.deal_Id,
            CustomerID: this.propRes.generalInformation.customerId,
            MerchantID: commonData.MerchantID,
            TransactionId: commonData.TransactionId,
            PaymentAmount: String(this.finalamount),
            InstrumentDate: this.startDate,
            ReceiptDate: this.startDate,
          },
        },
        PaymentMapping: {
          DealID: this.deal_Id,
          CorrelationID: this.propRes.correlationId,
          customerProposal: [
            {
              CustomerID: this.propRes.generalInformation.customerId,
              ProposalNo: this.propRes.generalInformation.proposalNumber,
            },
          ],
        },
      };
    } else {
      body = {
        isMappingRequired: false,
        isTaggingRequired: true,
        CorrelationId: this.propRes.correlationId,
        DealId: this.deal_Id,
        PaymentEntry: {
          onlineDAEntry: {
            CorrelationId: this.propRes.correlationId,
            DealId: this.deal_Id,
            CustomerID: this.propRes.generalInformation.customerId,
            MerchantID: commonData.MerchantID,
            TransactionId: commonData.TransactionId,
            PaymentAmount: String(this.finalamount),
            InstrumentDate: this.startDate,
            ReceiptDate: this.startDate,
          },
        },
        PaymentTagging: {
          DealID: this.deal_Id,
          CorrelationID: this.propRes.correlationId,
          customerProposal: [
            {
              CustomerID: this.propRes.generalInformation.customerId,
              ProposalNo: this.propRes.generalInformation.proposalNumber,
            },
          ],
        },
        PaymentMapping: null,
      };
    }

    let str = JSON.stringify(body);
    this.cs
      .post1("Payment/SaveOrderRequestBody", str)
      .then((response: any) => {});
  }

  createPFPaymentAsync(res: any, _self: any, route: any): Promise<any> {
    return new Promise((resolve: any) => {
      let authCode = res.razorpay_payment_id.split("_");
      let bankType = localStorage.getItem("bankType");
      let body;

      /**
       * DigitalPOS Change
       */
      if (this.IsPOSTransaction_flag == true) {
        this.posDealID = JSON.parse(localStorage.getItem("DealID"));
        this.deal_Id = this.posDealID;
      } else {
        this.deal_Id = this.propReq.DealId;
      }

      if (
        bankType == "INT" &&
        (this.instaData == "" ||
          this.instaData == undefined ||
          this.instaData == null)
      ) {
        body = {
          isMappingRequired: true,
          isTaggingRequired: true,
          CorrelationId: this.propRes.correlationId,
          DealId: this.deal_Id,
          RazorPayResponse: {
            razorpay_order_id: res.razorpay_order_id,
            razorpay_payment_id: res.razorpay_payment_id,
            razorpay_signature: res.razorpay_signature,
          },
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.propRes.correlationId,
              DealId: this.deal_Id,
              AuthCode: authCode[1],
              CustomerID: this.propRes.generalInformation.customerId,
              MerchantID: commonData.MerchantID,
              TransactionId: commonData.TransactionId,
              PaymentAmount: String(this.finalamount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
            },
          },
          PaymentTagging: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
          PaymentMapping: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
        };
      } else if (bankType == "INT" && this.instaData.instaPolicyNo) {
        body = {
          isMappingRequired: true,
          isTaggingRequired: false,
          CorrelationId: this.propRes.correlationId,
          DealId: this.deal_Id,
          RazorPayResponse: {
            razorpay_order_id: res.razorpay_order_id,
            razorpay_payment_id: res.razorpay_payment_id,
            razorpay_signature: res.razorpay_signature,
          },
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.propRes.correlationId,
              DealId: this.deal_Id,
              AuthCode: authCode[1],
              CustomerID: this.propRes.generalInformation.customerId,
              MerchantID: commonData.MerchantID,
              TransactionId: commonData.TransactionId,
              PaymentAmount: String(this.finalamount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
            },
          },
          PaymentMapping: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
        };
      } else {
        body = {
          isMappingRequired: false,
          isTaggingRequired: true,
          CorrelationId: this.propRes.correlationId,
          DealId: this.deal_Id,
          RazorPayResponse: {
            razorpay_order_id: res.razorpay_order_id,
            razorpay_payment_id: res.razorpay_payment_id,
            razorpay_signature: res.razorpay_signature,
          },
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.propRes.correlationId,
              DealId: this.deal_Id,
              AuthCode: authCode[1],
              CustomerID: this.propRes.generalInformation.customerId,
              MerchantID: commonData.MerchantID,
              TransactionId: commonData.TransactionId,
              PaymentAmount: String(this.finalamount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
            },
          },
          PaymentTagging: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
        };
      }

      let str = JSON.stringify(body);
      let loader = document.getElementById("loaderdiv") as HTMLInputElement;
      loader.style.display = "block";
      _self.cs
        .post1("Payment/PFPaymentAsync", str)
        .then((response: any) => {
          if (response == "Request Sent") {
            localStorage.setItem("PFReq", JSON.stringify(body));
            localStorage.setItem("PFRes", JSON.stringify(response));
            // this.updatePaymentStatus().then(() => { this.updatePolicyStatus(); });
            // location.href = PaymentURL.URL + "payment-confirmation";
            // _self.router.navigateByUrl('/payment-confirmation');

            this.cs.pointingChange();
            // Local URL
            // location.href = "http://localhost:4200/#/payment-confirmation";
            // Sanity URL
            // location.href = "https://nysa-uat.insurancearticlez.com/nysaui/#/payment-confirmation"
            // Production URL
            // location.href = "https://ipartnerms.icicilombard.com/nysa/#/payment-confirmation"
            // location.href = "https://nysa.icicilombard.com/nysa/#/payment-confirmation"

            loader.style.display = "none";
            // this.router.navigateByUrl('/payment-confirmation');
          } else {
            swal({
              closeOnClickOutside: false,
              title: "Error!",
              text: "There seems to be a problem with the payment. Please try again with a new proposal creation.",
            }).then((result: any) => {
              this.cs.quotepointing();

              // Local URL
              // location.href = "http://localhost:4200/#/quote";
              // Sanity URL
              // location.href = "https://nysa-uat.insurancearticlez.com/nysaui/#/quote"
              // Production URL
              // location.href = "https://ipartnerms.icicilombard.com/nysa/#/quote"
              // location.href = "https://nysa.icicilombard.com/nysa/#/quote"
              loader.style.display = "none";
            });
            if (this.cs.isUndefineORNull(this.instaData)) {
              let req = {
                CorrelationID: this.propRes.correlationId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
                ReqBody: str,
                // "ReqFrom": "PAYMENT_TAG",
                ReqURL: commonData.baseURL1 + "Payment/PFPaymentAsync",
                ErrorMsg: "Insta/Lombard Pay from single payment",
              };
              let str1 = JSON.stringify(req);
              this.cs
                .postpaymentreq("PaymsPayment/AddPaymsRequestDetails", str1)
                .then((res: any) => {});
            }
          }
        })
        .catch((err: any) => {
          loader.style.display = "none";
          swal({
            closeOnClickOutside: false,
            title: "Error!",
            text: "There seems to be a problem with the payment. Please try again with a new proposal creation.",
          }).then((result: any) => {
            this.cs.quotepointing();

            // Local URL
            // location.href = "http://localhost:4200/#/quote";
            // Sanity URL
            // location.href = "https://nysa-uat.insurancearticlez.com/nysaui/#/quote"
            // Production URL
            // location.href = "https://ipartnerms.icicilombard.com/nysa/#/quote"
            // location.href = "https://nysa.icicilombard.com/nysa/#/quote"
            loader.style.display = "none";
          });
          if (this.cs.isUndefineORNull(this.instaData)) {
            let req = {
              CorrelationID: this.propRes.correlationId,
              ProposalNo: this.propRes.generalInformation.proposalNumber,
              ReqBody: str,
              // "ReqFrom": "PAYMENT_TAG",
              ReqURL: commonData.baseURL1 + "Payment/PFPaymentAsync",
              ErrorMsg: JSON.stringify(err),
              // "IPartneruserid": this.auth.username,
            };
            let str1 = JSON.stringify(req);
            this.cs
              .postpaymentreq("PaymsPayment/AddPaymsRequestDetails", str1)
              .then((res: any) => {});
          }
          this.cs.loaderStatus = false;
          resolve();
        });
    });
  }

  /**
   * PFAsyncPayment for POS function
   * digitalPOS Change
   * 21-10-2021
   */
  createPFPaymentAsyncPOS(
    res: any,
    _self: any,
    route: any,
    token: any
  ): Promise<any> {
    return new Promise((resolve: any) => {
      let authCode = res.razorpay_payment_id.split("_");
      let bankType = localStorage.getItem("bankType");
      let body;

      /**
       * DigitalPOS Change
       */
      if (this.IsPOSTransaction_flag == true) {
        this.posDealID = JSON.parse(localStorage.getItem("DealID"));
        this.deal_Id = this.posDealID;
      } else {
        this.deal_Id = this.propReq.DealId;
      }

      if (
        bankType == "INT" &&
        (this.instaData == "" ||
          this.instaData == undefined ||
          this.instaData == null)
      ) {
        body = {
          isMappingRequired: true,
          isTaggingRequired: true,
          CorrelationId: this.propRes.correlationId,
          DealId: this.deal_Id,
          RazorPayResponse: {
            razorpay_order_id: res.razorpay_order_id,
            razorpay_payment_id: res.razorpay_payment_id,
            razorpay_signature: res.razorpay_signature,
          },
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.propRes.correlationId,
              DealId: this.deal_Id,
              AuthCode: authCode[1],
              CustomerID: this.propRes.generalInformation.customerId,
              MerchantID: commonData.MerchantID,
              TransactionId: commonData.TransactionId,
              PaymentAmount: String(this.finalamount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
            },
          },
          PaymentTagging: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
          PaymentMapping: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
        };
      } else if (bankType == "INT" && this.instaData.instaPolicyNo) {
        body = {
          isMappingRequired: true,
          isTaggingRequired: false,
          CorrelationId: this.propRes.correlationId,
          DealId: this.deal_Id,
          RazorPayResponse: {
            razorpay_order_id: res.razorpay_order_id,
            razorpay_payment_id: res.razorpay_payment_id,
            razorpay_signature: res.razorpay_signature,
          },
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.propRes.correlationId,
              DealId: this.deal_Id,
              AuthCode: authCode[1],
              CustomerID: this.propRes.generalInformation.customerId,
              MerchantID: commonData.MerchantID,
              TransactionId: commonData.TransactionId,
              PaymentAmount: String(this.finalamount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
            },
          },
          PaymentMapping: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
        };
      } else {
        body = {
          isMappingRequired: false,
          isTaggingRequired: true,
          CorrelationId: this.propRes.correlationId,
          DealId: this.deal_Id,
          RazorPayResponse: {
            razorpay_order_id: res.razorpay_order_id,
            razorpay_payment_id: res.razorpay_payment_id,
            razorpay_signature: res.razorpay_signature,
          },
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.propRes.correlationId,
              DealId: this.deal_Id,
              AuthCode: authCode[1],
              CustomerID: this.propRes.generalInformation.customerId,
              MerchantID: commonData.MerchantID,
              TransactionId: commonData.TransactionId,
              PaymentAmount: String(this.finalamount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
            },
          },
          PaymentTagging: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
        };
      }

      let str = JSON.stringify(body);
      let loader = document.getElementById("loaderdiv") as HTMLInputElement;
      loader.style.display = "block";
      _self.cs
        .postPaymentPOS("Payment/PFPaymentAsync", str, token)
        .then((response: any) => {
          if (response == "Request Sent") {
            localStorage.setItem("PFReq", JSON.stringify(body));
            localStorage.setItem("PFRes", JSON.stringify(response));
            // this.updatePaymentStatus().then(() => { this.updatePolicyStatus(); });
            // location.href = PaymentURL.URL + "payment-confirmation";
            // _self.router.navigateByUrl('/payment-confirmation');

            this.cs.pointingChange();
            // Local URL
            // location.href = "http://localhost:4200/#/payment-confirmation";
            // Sanity URL
            // location.href = "https://nysa-uat.insurancearticlez.com/nysaui/#/payment-confirmation"
            // Production URL
            // location.href = "https://ipartnerms.icicilombard.com/nysa/#/payment-confirmation"
            // location.href = "https://nysa.icicilombard.com/nysa/#/payment-confirmation"

            loader.style.display = "none";
            // this.router.navigateByUrl('/payment-confirmation');
          } else {
            swal({
              closeOnClickOutside: false,
              title: "Error!",
              text: "There seems to be a problem with the payment. Please try again with a new proposal creation.",
            }).then((result: any) => {
              this.cs.quotepointing();

              // Local URL
              // location.href = "http://localhost:4200/#/quote";
              // Sanity URL
              // location.href = "https://nysa-uat.insurancearticlez.com/nysaui/#/quote"
              // Production URL
              // location.href = "https://ipartnerms.icicilombard.com/nysa/#/quote"
              // location.href = "https://nysa.icicilombard.com/nysa/#/quote"
              loader.style.display = "none";
            });
            if (this.cs.isUndefineORNull(this.instaData)) {
              let req = {
                CorrelationID: this.propRes.correlationId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
                ReqBody: str,
                // "ReqFrom": "PAYMENT_TAG",
                ReqURL: commonData.baseURL1 + "Payment/PFPaymentAsync",
                ErrorMsg: response,
                // "IPartneruserid": this.auth.username,
              };
              let str1 = JSON.stringify(req);
              this.cs
                .postpaymentreq("PaymsPayment/AddPaymsRequestDetails", str1)
                .then((res: any) => {});
            }
          }
        })
        .catch((err: any) => {
          loader.style.display = "none";
          swal({
            closeOnClickOutside: false,
            title: "Error!",
            text: "There seems to be a problem with the payment. Please try again with a new proposal creation.",
          }).then((result: any) => {
            this.cs.quotepointing();

            // Local URL
            // location.href = "http://localhost:4200/#/quote";
            // Sanity URL
            // location.href = "https://nysa-uat.insurancearticlez.com/nysaui/#/quote"
            // Production URL
            // location.href = "https://ipartnerms.icicilombard.com/nysa/#/quote"
            // location.href = "https://nysa.icicilombard.com/nysa/#/quote"
            loader.style.display = "none";
          });
          if (this.cs.isUndefineORNull(this.instaData)) {
            let req = {
              CorrelationID: this.propRes.correlationId,
              ProposalNo: this.propRes.generalInformation.proposalNumber,
              ReqBody: str,
              // "ReqFrom": "PAYMENT_TAG",
              ReqURL: commonData.baseURL1 + "Payment/PFPaymentAsync",
              ErrorMsg: JSON.stringify(err),
              // "IPartneruserid": this.auth.username,
            };
            let str1 = JSON.stringify(req);
            this.cs
              .postpaymentreq("PaymsPayment/AddPaymsRequestDetails", str1)
              .then((res: any) => {});
          }
          this.cs.loaderStatus = false;
          resolve();
        });
    });
  }

  // Create PF Payment
  createPFPayment(res: any, _self: any, route: any): Promise<any> {
    return new Promise((resolve: any) => {
      let authCode = res.razorpay_payment_id.split("_");
      let bankType = localStorage.getItem("bankType");
      let body;
      /**
       * DigitalPOS Change
       */
      if (this.IsPOSTransaction_flag == true) {
        this.posDealID = JSON.parse(localStorage.getItem("DealID"));
        this.deal_Id = this.posDealID;
      } else {
        this.deal_Id = this.propReq.DealId;
      }

      this.IsInstaDone = false;
      if (bankType == "INT") {
        body = {
          isMappingRequired: true,
          isTaggingRequired: true,
          CorrelationId: this.propRes.correlationId,
          DealId: this.deal_Id,
          RazorPayResponse: {
            razorpay_order_id: res.razorpay_order_id,
            razorpay_payment_id: res.razorpay_payment_id,
            razorpay_signature: res.razorpay_signature,
          },
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.propRes.correlationId,
              DealId: this.deal_Id,
              AuthCode: authCode[1],
              CustomerID: this.propRes.generalInformation.customerId,
              MerchantID: commonData.MerchantID,
              TransactionId: commonData.TransactionId,
              PaymentAmount: String(this.finalamount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
            },
          },
          PaymentTagging: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
          PaymentMapping: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
        };
      } else {
        body = {
          isMappingRequired: false,
          isTaggingRequired: true,
          CorrelationId: this.propRes.correlationId,
          DealId: this.deal_Id,
          RazorPayResponse: {
            razorpay_order_id: res.razorpay_order_id,
            razorpay_payment_id: res.razorpay_payment_id,
            razorpay_signature: res.razorpay_signature,
          },
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.propRes.correlationId,
              DealId: this.deal_Id,
              AuthCode: authCode[1],
              CustomerID: this.propRes.generalInformation.customerId,
              MerchantID: commonData.MerchantID,
              TransactionId: commonData.TransactionId,
              PaymentAmount: String(this.finalamount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
            },
          },
          PaymentTagging: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
        };
      }

      let str = JSON.stringify(body);
      let loader = document.getElementById("loaderdiv") as HTMLInputElement;
      loader.style.display = "block";
      _self.cs
        .post1("Payment/PFPayment", str)
        .then((response: any) => {
          localStorage.setItem("PFReq", JSON.stringify(body));
          localStorage.setItem("PFRes", JSON.stringify(response));
          this.updatePaymentStatus().then(() => {
            this.updatePolicyStatus();
          });
          if (response.statusMessage == "Success") {
            if (response.paymentEntryResponse.status == "Failed") {
              swal({
                closeOnClickOutside: false,
                title: "Error!",
                text: response.paymentEntryResponse.errorText,
              });
              // this.goToDashboard();
            } else {
              if (
                response.paymentTagResponse.paymentTagResponseList[0].status ==
                  "Failed" ||
                response.paymentTagResponse.paymentTagResponseList[0].status ==
                  "FAILED"
              ) {
                swal({
                  closeOnClickOutside: false,
                  title: "Error!",
                  text: response.paymentTagResponse.paymentTagResponseList[0]
                    .errorText,
                }).then(() => {
                  // this.cs.geToDashboard();
                });
                // this.router.navigateByUrl('quote');
              } else {
                localStorage.setItem(
                  "newProposalNumber",
                  JSON.stringify(this.propRes.generalInformation.proposalNumber)
                );
                localStorage.setItem(
                  "newPolicyNumber",
                  JSON.stringify(
                    response.paymentTagResponse.paymentTagResponseList[0]
                      .policyNo
                  )
                );
                localStorage.setItem("instaPayment", JSON.stringify(false));
                // route.navigateByUrl('payment-confirmation');
                // location.href = PaymentURL.URL + "payment-confirmation";
                resolve();
                // this.router.navigateByUrl('/payment-confirmation');
                if (
                  response.paymentTagResponse.paymentTagResponseList[0]
                    .policyNo == "" ||
                  response.paymentTagResponse.paymentTagResponseList[0]
                    .policyNo == null ||
                  response.paymentTagResponse.paymentTagResponseList[0]
                    .policyNo == undefined
                ) {
                  swal({
                    closeOnClickOutside: false,
                    title: "Error!",
                    text: "Payment Unsuccessful. Please Try again",
                  }).then(() => {
                    this.router.navigateByUrl("quote");
                  });
                } else {
                  this.cs.pointingChange();
                  // location.href = PaymentURL.URL + "payment-confirmation";
                  // Local URL
                  // location.href = "http://localhost:4200/#/payment-confirmation";
                  // Sanity URL
                  // location.href = "https://nysa-uat.insurancearticlez.com/nysaui/#/payment-confirmation"
                  // Production URL
                  // location.href = "https://ipartnerms.icicilombard.com/nysa/#/payment-confirmation"
                  // location.href = "https://nysa.icicilombard.com/nysa/#/payment-confirmation"
                }
              }
            }
          } else {
            // this.payLater();
            swal({
              closeOnClickOutside: false,
              title: "Error!",
              text: "Payment Unsuccessful. Please Try again",
            });
          }
          loader.style.display = "none";
          // this.router.navigateByUrl('/payment-confirmation');
        })
        .catch((err: any) => {
          loader.style.display = "none";
          swal({
            closeOnClickOutside: false,
            title: "Error!",
            text: err,
          });
          resolve();
        });
    });
  }

  validateEmail(emailData: any) {
    var email = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    return email.test(String(emailData).toLowerCase());
  }

  closeLink() {
    this.paymentModeFlag = true;
    this.sendCustPaymentLinkFlag = false;
    this.watchMe = "fade";
  }
  sendCustLink() {
    this.showInstadiv = false;
    this.sendCustPaymentLinkFlag = true;
    this.paymentModeFlag = false;
    this.watchMe = "fly";
  }

  openaddmoney() {
    this.addlombardmoneyFlag = true;
    this.watchMe = "fly";
  }

  closeaddLink() {
    this.addlombardmoneyFlag = false;
    this.watchMe = "fade";
  }

  // pranay
  sendLink() {
  
    let custEmail, custMobile;
    // let validationStatus = this.validateContacts()
    // if (!validationStatus.length) {
    (<any>window).ga("send", "event", {
      eventCategory: "Customer Link",
      eventLabel:
        "Customer Link Clicked" + "" + this.product + "" + this.policyType,
      eventAction: "Customer Link Clicked",
      eventValue: 10,
    });

    //   if (email) {
    //     custEmail = email;
    //   } else {
    //     custEmail = this.customerEmail;
    //   }

    //  if (mobile) {
    //    custMobile = mobile;
    //  } else {
    //    custMobile = this.custMobile;
    //  }
    custEmail = this.customerEmail;
    custMobile = this.custMobile;

    // AES Solution1
    // let custEmail = CryptoJS.AES.encrypt(JSON.stringify(this.customerEmail),'aXBhcnRuZXJOeXNhU2VjcmV0QDA3S2V5').toString();
    // let custMobile = CryptoJS.AES.encrypt(JSON.stringify(this.custMobile), 'aXBhcnRuZXJOeXNhU2VjcmV0QDA3S2V5').toString();

    let regemailtest =
      /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(
        this.customerEmail
      );

    if (
      this.customerEmail == "" ||
      this.customerEmail == undefined ||
      this.customerEmail == null ||
      !regemailtest
    ) {
      this.cs.loaderStatus = false;
      swal({
        closeOnClickOutside: false,
        text: "Insert valid Email Id",
      }).then(() => {
        // $('#sendLinkModal').modal('show');
        this.paymentModeFlag = true;
        this.sendCustPaymentLinkFlag = false;
      });
    } else if (
      this.custMobile == "" ||
      this.custMobile == undefined ||
      this.custMobile == null
    ) {
      this.cs.loaderStatus = false;
      swal({
        closeOnClickOutside: false,
        text: "Insert Mobile Number",
      }).then(() => {
        // $('#sendLinkModal').modal('show');
        this.paymentModeFlag = true;
        this.sendCustPaymentLinkFlag = false;
      });
    } else {
      this.getCustomerLink(custEmail, custMobile);
      this.PaymentMode = "4";
      localStorage.setItem("senCustLink", "true");
    }

    // this.getCustomerLink(this.customerEmail, this.custMobile);

    // } else {
    //   swal({text : validationStatus[0]});
    // }
    // this.cs.loaderStatus = false;
  }

  //pranay
  validateContacts() {
    let isValid;
    let errMsg = [];
    let contacts = [
      {
        type: "Email id",
        value: this.customerEmail,
        regex: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/,
      },
      {
        type: "Mobile number",
        value: this.custMobile,
        regex: /^(\+\d{1,3}[- ]?)?\d{10}$/,
      },
    ];
    contacts.forEach((element) => {
      if (element.value) {
        if (element.regex.test(element.value)) {
          isValid = true;
        } else {
          isValid = false;
          errMsg.push(`Please enter a valid ${element.type}`);
        }
      } else {
        isValid = false;
        errMsg.push(`Please enter you ${element.type}`);
      }
    });
    // if (this.customerEmail) {
    //   if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this.customerEmail)) {
    //     isValid = true;
    //   } else {
    //     isValid = false;
    //     errMsg.push('Please enter a valid email id');
    //   }
    // } else {
    //   isValid = false;
    //   errMsg.push('Please enter you email id');
    // }
    // if (this.custMobile) {
    //   if (/^(\+\d{1,3}[- ]?)?\d{10}$/.test(this.custMobile)) {
    //     isValid = true;
    //   } else {
    //     isValid = false;
    //     errMsg.push('Please enter a valid mobile number');
    //   }
    // } else {
    //   isValid = false;
    //   errMsg.push('Please enter you mobile number');
    // }
    return errMsg;
  }
  closeModal(modalName) {
    $("#" + modalName).modal("hide");
  }

  //time convert
  convertTime(timeDiff) {
    var msec = timeDiff;
    this.timeseconds = Math.round(msec / 1000);

    var hh = Math.floor(msec / 1000 / 60 / 60);
    msec -= hh * 1000 * 60 * 60;
    var mm = Math.floor(msec / 1000 / 60);
    msec -= mm * 1000 * 60;
    var ss = Math.floor(msec / 1000);
    msec -= ss * 1000;

    if (hh == 0) {
      this.timeTaken = mm + " Minutes " + ss + " Seconds ";
    } else {
      this.timeTaken = hh + " Hours " + mm + " Minutes " + ss + " Seconds ";
    }
    let clock = setInterval(() => {
      clearInterval(clock);
      clock = null;
    }, 200);
  }

  starRating(ev: any) {
    // this.updatePaymentStatuscustlink();
    this.starRatingFeed = ev;
    let breakinFlag: any = localStorage.getItem("breakinFlag");
    if (breakinFlag == "true") {
      let breakinData: any = JSON.parse(localStorage.getItem("breakinData"));
      this.policySubType = breakinData.policySubType;
    } else {
      if (this.productCode == "2312") {
        this.policySubType = "1";
      } else if (this.productCode == "2311") {
        this.policySubType = "2";
      } else if (this.productCode == "2320") {
        this.policySubType = "9";
      } else if (this.productCode == "2319") {
        this.policySubType = "10";
      }
    }

    // if(this.productCode == '2312' ) { this.policySubType = 1; }
    // else if(this.productCode == '2311' ) { this.policySubType = 2; }
    // else if(this.productCode == '2320' ) { this.policySubType = 9; }
    // else if(this.productCode == '2319'){this.policySubType = 10; }
    let body = {
      PolicySubType: this.policySubType, // mandatory
      ProposalNumber: this.propRes.generalInformation.proposalNumber, // mandatory
      StarRating: JSON.parse(this.starRatingFeed), // mandatory
      PolicyCreatedIn: this.timeTaken,
      Feedback: "", // non mandatory , do not show on screen
      PolicyCreatedInSecond: JSON.stringify(this.timeseconds),
    };
    let str = JSON.stringify(body);
    this.cs.post("Feedback/GetUserFeedback", str).then((res: any) => {
      this.feedBackResponse = res;
    });
  }

  // Pranay 19-12-2020
  // failedMsg(type) {
  //   let text;
  //   if (type === 'both') {
  //     text = `Sorry! link was not sent to mentioned email and mobile number`;
  //   } else {
  //     text = `Sorry! link was not sent to ${type}`;
  //   }
  //   return text;
  // }
  // successMsg(type, failedType?) {
  //   let text;
  //   if (failedType) {
  //     text = `Link has been sent to your ${type} but failed to send on mentioned ${failedType}`;
  //   } else {
  //     if (type === 'both') {
  //       text = `Link has been sent to mentioned email and mobile number`;
  //     } else {
  //       text = `Link has been sent to ${type}`;
  //     }
  //   }
  //   return text;
  // }
  // Pranay 19-12-2020
  paymentStatusDetails(emailStatus, smsStatus, res, email) {
    this.IsPayLinkSent = emailStatus;
    this.PayLinkReciever = email;
    this.isSMSSent = smsStatus;
    this.CustomerPayLink = res.paymentLink;
    this.custLink = res;
    this.starRating("0");
    this.updatePaymentStatuscustlink().then(() => {
      this.updatePolicyStatusCustLink();
    });
    this.router.navigateByUrl("quote");
    // this.cs.clearLocalStorage();
    // this.cs.clearQuoteDate();
    // this.cs.clearPropData();
  }
  // Pranay 19-12-2020
  getCustomerLink(email: any, mobile: any) {
    console.log('print this')
    let body = {
      CorrelationID: this.propRes.correlationId,
      // "EmailID": email,
      // "MobileNo": mobile,
      EmailID: this.cs.encrypt(email, commonData.aesnysaKey),
      MobileNo: this.cs.encrypt(mobile, commonData.aesnysaKey),
      ProposalNo: this.propRes.generalInformation.proposalNumber,
    };
    let url = "CustomerPayment/getLink";
    let str = JSON.stringify(body);
    let status = "N";
    let smsStatus = "N";
    this.cs.loaderStatus = true;
    this.cs.post(url, str).then((res: any) => {
      const emailStatus = res.isLinkSent ? true : false;
      status = res.isLinkSent ? "Y" : "N";
      const mobileStatus = res.isSMSSent ? true : false;
      status = res.isSMSSent ? "Y" : "N";
      const checkBoth = emailStatus && mobileStatus ? true : false;
      // this.paymentStatusDetails(status, smsStatus, res, email);
      let msg;
      let maskedEmail = email.replace(
        /(.{2})(.*)(?=@)/,
        function (gp1, gp2, gp3) {
          for (let i = 0; i < gp3.length; i++) {
            gp2 += "X";
          }
          return gp2;
        }
      );
      let maskedMobile =
        mobile.slice(0, 2) + mobile.slice(2).replace(/.(?=...)/g, "X");
      if (checkBoth) {
        msg = `Payment link has been sent to ${maskedEmail} and ${maskedMobile}.`;
        // msg = this.successMsg('both');
        status = "Y";
        smsStatus = "Y";
      } else {
        this.cs.loaderStatus = false;
        if (emailStatus) {
          msg = `Payment link has been sent to ${maskedEmail}.`;
          // msg = this.successMsg(email, mobile);
          status = "Y";
          smsStatus = "N";
        } else if (mobileStatus) {
          msg = `Payment link has been sent to ${maskedMobile}.`;
          // msg = this.successMsg(mobile, email);
          status = "N";
          smsStatus = "Y";
        } else if (!checkBoth) {
          msg = `Failed to send payment link to ${maskedEmail} and ${maskedMobile}.`;
          // msg = this.failedMsg('both');
          status = "N";
          smsStatus = "N";
        }
      }
      swal({ text: msg, closeOnClickOutside: false }).then(() => {
        this.paymentStatusDetails(status, smsStatus, res, email);
        this.cs.loaderStatus = false;
      });
    });
    // this.cs.post('CustomerPayment/getLink', str).then((res: any) => {
    //   if(res.isLinkSent == true ){
    //     this.IsPayLinkSent = "Y";
    //     this.PayLinkReciever = this.customerEmail;
    //     this.CustomerPayLink = res.paymentLink;
    //     this.starRating("0");
    //     this.updatePaymentStatuscustlink().then(() => { this.updatePolicyStatusCustLink(); });

    //     this.custLink = res;
    //     swal({ closeOnClickOutside: false, text: 'Link has been sent successfully' }).then(()=> {
    //     this.router.navigateByUrl('quote');
    //     this.cs.clearLocalStorage();
    //     this.cs.clearQuoteDate();
    //     this.cs.clearPropData();
    //     });
    //   } else if(res.paymentLink != "" && res.paymentLink != null) {
    //     swal({ closeOnClickOutside: false,
    //     text: 'Link was not sent to mentioned email id. Please share below customer payment link: ' + '\n'  + res.paymentLink })
    //     this.IsPayLinkSent = "N";
    //     this.PayLinkReciever = this.customerEmail;
    //     this.CustomerPayLink = res.paymentLink;
    //     this.starRating("0");
    //     this.updatePaymentStatuscustlink().then(() => { this.updatePolicyStatusCustLink(); });
    //     this.router.navigateByUrl('quote');
    //     this.cs.clearLocalStorage();
    //     this.cs.clearQuoteDate();
    //     this.cs.clearPropData();
    //   }
    //   else {
    //     swal({ closeOnClickOutside: false, text: 'Sorry! link was not sent to mentioned email id ' })
    //   }
    // })
    // this.cs.loaderStatus = false;
  }

  // Update Policy Payment Data to database
  updatePaymentStatus(): Promise<any> {
    return new Promise((resolve: any) => {
      this.PFPayReq = JSON.parse(localStorage.getItem("PFReq"));
      this.PFPayRes = JSON.parse(localStorage.getItem("PFRes"));

      /**
       * DigitalPOS Change
       */
      if (this.IsPOSTransaction_flag == true) {
        this.posDealID = JSON.parse(localStorage.getItem("DealID"));
        this.deal_Id = this.posDealID;
      } else {
        this.deal_Id = this.propReq.DealId;
      }
      let body = {
        policyNo:
          this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].policyNo,
        coverNoteNo:
          this.PFPayRes.paymentTagResponse.paymentTagResponseList[0]
            .coverNoteNo,
        proposalNo: this.propRes.generalInformation.proposalNumber,
        dealID: this.deal_Id,
        customerID: this.propRes.generalInformation.customerId,
        pfPaymentID:
          this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].paymentID,
        paymentEntryErrorID: this.PFPayRes.paymentEntryResponse.error_ID,
        paymentEntryErrorText: this.PFPayRes.paymentEntryResponse.errorText,
        paymentEntryStatus: this.PFPayRes.paymentEntryResponse.status,
        paymentTagErrorID:
          this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].error_ID,
        paymentTagErrorText:
          this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].errorText,
        paymentTagStatus:
          this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].status,
        message: this.PFPayRes.message,
        PaymentMode: this.PaymentMode,
        statusMessage: this.PFPayRes.statusMessage,
        paymsRequestID: String(this.PFPayRes.PAYMS_RequestID),
        paymentRS: JSON.stringify(this.PFPayRes), // here pass full response string which is coming from Mihir api
        corelationID: this.PFPayRes.correlationId, // pass correlation from payment api response
      };
      let str = JSON.stringify(body);
      // this.updatePolicyStatus();
      this.cs.post("Payment/UpdatePaymentDetails", str).then((res: any) => {
        this.updatePayment = res;
        resolve();
      });
    });
  }

  // Update Policy Payment Data to database
  updatePaymentStatuscustlink(): Promise<any> {
    return new Promise((resolve: any) => {
      /**
       * DigitalPOS Change
       */
      if (this.IsPOSTransaction_flag == true) {
        this.posDealID = JSON.parse(localStorage.getItem("DealID"));
        this.deal_Id = this.posDealID;
      } else {
        this.deal_Id = this.propReq.DealId;
      }
      let body = {
        ID: this.propReq.id,
        policyNo: "",
        coverNoteNo: "",
        proposalNo: this.propRes.generalInformation.proposalNumber,
        dealID: this.deal_Id,
        customerID: this.propRes.generalInformation.customerId,
        pfPaymentID: "",
        paymentEntryErrorID: "",
        paymentEntryErrorText: "",
        paymentEntryStatus: "",
        paymentTagErrorID: "",
        paymentTagErrorText: "",
        paymentTagStatus: "",
        message: "",
        PaymentMode: this.PaymentMode,
        statusMessage: "",
        paymsRequestID: "",
        paymentRS: "", // here pass full response string which is coming from Mihir api
        corelationID: this.propRes.correlationId, // pass correlation from payment api response
      };

      if (localStorage.getItem("isPendingPayment")) {
        // let instaData = JSON.parse(localStorage.getItem('InstaData'));
        body.policyNo = this.instaData.instaPolicyNo;
        body.coverNoteNo = this.instaData.instaCovernoteNo;
      }

      let str = JSON.stringify(body);
      // this.updatePolicyStatus();
      this.cs.post("Payment/UpdatePaymentDetails", str).then((res: any) => {
        this.updatePayment = res;
        resolve();
      });
    });
  }

  // Update Policy Data to database
  updatePolicyStatus() {
    this.PFPayReq = JSON.parse(localStorage.getItem("PFReq"));
    this.PFPayRes = JSON.parse(localStorage.getItem("PFRes"));
    this.proposalRes = JSON.parse(localStorage.getItem("PropDatares"));
    this.totalTax = Math.round(this.propRes.totalTax);
    if (this.productCode == "2320" || this.productCode == "2319") {
      this.propRes.specialDiscount = "0";
      this.propRes.finalPremium = JSON.stringify(this.propRes.finalPremium);
      this.propRes.policyType = 1;
    }
    // if(this.productCode == '2320' ) { this.propRes.policySubType = 9; } else if(this.productCode == '2319'){this.propRes.policySubType = 10; }
    let policysubType = this.propRes.policySubType;
    let breakinFlag: any = localStorage.getItem("breakinFlag");

    if (breakinFlag == "true") {
      let breakinData: any = JSON.parse(localStorage.getItem("breakinData"));
      this.SubProductType = breakinData.policySubType;
    } else {
      if (this.productCode == "2312") {
        this.SubProductType = "1";
      } else if (this.productCode == "2311") {
        this.SubProductType = "2";
      } else if (this.productCode == "2320") {
        this.SubProductType = "9";
      } else if (this.productCode == "2319") {
        this.SubProductType = "10";
      }
    }
    /**
     * DigitalPOS Change
     */
    if (this.IsPOSTransaction_flag == true) {
      this.posDealID = JSON.parse(localStorage.getItem("DealID"));
      this.deal_Id = this.posDealID;
    } else {
      this.deal_Id = this.propReq.DealId;
    }

    let body;
    body = {
      DealID: this.deal_Id,
      IsInstaDone: this.IsInstaDone,
      PolicyType: "1", //String
      PolicySubType: this.SubProductType, //String
      ProposalNumber: this.propRes.generalInformation.proposalNumber,
      PolicyNumber:
        this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].policyNo,
      PolicyStartDate: this.propReq.PolicyStartDate,
      PolicyEndDate: this.propReq.PolicyEndDate,
      Status: this.propRes.statusID,
      BasicPremium: this.propRes.totalLiabilityPremium,
      Discount: parseFloat(this.propRes.specialDiscount), //String
      ServiceTax: this.totalTax,
      TotalPremium: parseFloat(this.propRes.finalPremium), //String
      AddressID: "",
      Isactive: "1",
      PaymentID: JSON.stringify(this.updatePayment.paymentID),
      SalesTax: "",
      Surcharge: "",
      TotalTax: this.totalTax, //String
      TransFor: this.propRes.generalInformation.transactionType,
      TaxRate: "",
      CustomerType: this.propReq.CustomerType,
      CustomerName: this.propReq.CustomerDetails.CustomerName,
      PreviousPolicyNumber: null,
      PFPolicyNo:
        this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].policyNo,
      PFProposalNo:
        this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].proposalNo,
      PFCovernoteNo:
        this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].coverNoteNo,
      PFCustomerID: this.propRes.generalInformation.customerId,
      PFPaymentID:
        this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].paymentID,
      SumInsured: null,
      Planname: null,
      PolicyTenure: this.propRes.generalInformation.tenure,
      PolicyTPTenure: this.propRes.generalInformation.tpTenure,
      PolicyPACoverTenure: JSON.stringify(this.propReq.PACoverTenure),
      UTGSTAmount: "0",
      TotalGSTAmount: "0",
      CGSTAmountRate: null,
      SGSTAmountRate: null,
      IGSTAmountRate: null,
      UTGSTAmountRate: null,
      TOTALGSTAmountRate: null,
      TransOn: "2020-03-18",
      CreatedBy: null,
      CreatedOn: "2020-03-18",
      ModifiedBy: null,
      ModifiedOn: "2020-03-18",
      CorelationID: this.propRes ? "" : this.PFPayRes.correlationId,
      IsPayLinkSent: this.IsPayLinkSent,
      PayLinkReciever: this.PayLinkReciever,
      CustomerPayLink: this.CustomerPayLink,
      // "IsStandaloneOD" : null,
      // "IsSubagent":null,
      // "SubagentIpartnerUserID": null
    };
    if (this.quoteReq.Tenure == "1" && this.quoteReq.TPTenure == "0") {
      body.IsStandaloneOD = true;
    }

    if (
      this.paramDataValues.iPartnerLogin.isSubagent != null ||
      this.paramDataValues.iPartnerLogin.isSubagent != undefined
    ) {
      body.IsSubagent = true;
      body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
    }
    let str = JSON.stringify(body);
    this.cs.post("Policy/AddPolicyDetails", body).then((res: any) => {});
  }

  // Update Insta Policy Data to database
  updateInstaPolicyStatus(): Promise<any> {
    return new Promise((resolve: any) => {
      this.PFPayReq = JSON.parse(localStorage.getItem("PFReq"));
      this.PFPayRes = JSON.parse(localStorage.getItem("PFRes"));
      this.totalTax = Math.round(this.propRes.totalTax);
      if (this.productCode == "2320" || this.productCode == "2319") {
        this.propRes.specialDiscount = "0";
        this.propRes.finalPremium = JSON.stringify(this.propRes.finalPremium);
        this.propRes.policyType = 1;
      }
      // if(this.productCode == '2320' ) { this.propRes.policySubType = 9; } else if(this.productCode == '2319'){this.propRes.policySubType = 10; }
      let policysubType = this.propRes.policySubType;
      let breakinFlag: any = localStorage.getItem("breakinFlag");

      if (breakinFlag == "true") {
        let breakinData: any = JSON.parse(localStorage.getItem("breakinData"));
        this.SubProductType = breakinData.policySubType;
      } else {
        if (this.productCode == "2312") {
          this.SubProductType = "1";
        } else if (this.productCode == "2311") {
          this.SubProductType = "2";
        } else if (this.productCode == "2320") {
          this.SubProductType = "9";
        } else if (this.productCode == "2319") {
          this.SubProductType = "10";
        }
      }

      /**
       * DigitalPOS Change
       */
      if (this.IsPOSTransaction_flag == true) {
        this.posDealID = JSON.parse(localStorage.getItem("DealID"));
        this.deal_Id = this.posDealID;
      } else {
        this.deal_Id = this.propReq.DealId;
      }

      let body;
      body = {
        DealID: this.deal_Id,
        IsInstaDone: this.IsInstaDone,
        PolicyType: "1", //String
        PolicySubType: this.SubProductType, //String
        ProposalNumber: this.propRes.generalInformation.proposalNumber,
        PolicyNumber: this.PFPayRes.pF_PolicyNo,
        PolicyStartDate: this.propReq.PolicyStartDate,
        PolicyEndDate: this.propReq.PolicyEndDate,
        Status: this.propRes.statusID,
        BasicPremium: this.propRes.totalLiabilityPremium,
        Discount: parseFloat(this.propRes.specialDiscount), //String
        ServiceTax: this.totalTax,
        TotalPremium: parseFloat(this.propRes.finalPremium), //String
        AddressID: "",
        Isactive: "1",
        PaymentID: JSON.stringify(this.updatePayment.paymentID),
        SalesTax: "",
        Surcharge: "",
        TotalTax: this.totalTax, //String
        TransFor: this.propRes.generalInformation.transactionType,
        TaxRate: "",
        CustomerType: this.propReq.CustomerType,
        PreviousPolicyNumber: null,
        PFPolicyNo: this.PFPayRes.pF_PolicyNo,
        PFProposalNo: this.PFPayRes.pF_ProposalNo,
        PFCovernoteNo: this.PFPayRes.pF_CoverNoteNo,
        PFCustomerID: this.propRes.generalInformation.customerId,
        CustomerName: this.propReq.CustomerDetails.CustomerName,
        PFPaymentID: null,
        SumInsured: null,
        Planname: null,
        PolicyTenure: this.propRes.generalInformation.tenure,
        PolicyTPTenure: this.propRes.generalInformation.tpTenure,
        PolicyPACoverTenure: JSON.stringify(this.propReq.PACoverTenure),
        UTGSTAmount: "0",
        TotalGSTAmount: "0",
        CGSTAmountRate: null,
        SGSTAmountRate: null,
        IGSTAmountRate: null,
        UTGSTAmountRate: null,
        TOTALGSTAmountRate: null,
        TransOn: "2020-03-18",
        CreatedBy: null,
        CreatedOn: "2020-03-18",
        ModifiedBy: null,
        ModifiedOn: "2020-03-18",
        CorelationID: this.propRes.correlationId,
        IsPayLinkSent: this.IsPayLinkSent,
        PayLinkReciever: this.PayLinkReciever,
        CustomerPayLink: this.CustomerPayLink,
        // "IsStandaloneOD" : null,
        // "IsSubagent":null,
        // "SubagentIpartnerUserID":null
      };

      if (localStorage.getItem("isPendingPayment")) {
        body.policyID = this.propReq.policyID;
      }

      if (
        this.propRes.generalInformation.tenure == "1" &&
        this.propRes.generalInformation.tpTenure == "0"
      ) {
        body.IsStandaloneOD = true;
      }
      if (
        this.paramDataValues.iPartnerLogin.isSubagent != null ||
        this.paramDataValues.iPartnerLogin.isSubagent != undefined
      ) {
        body.IsSubagent = true;
        body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
      } else {
        console.log("blank");
      }
      let str = JSON.stringify(body);
      this.cs.post("Policy/AddPolicyDetails", body).then((res: any) => {
        resolve();
      });
    });
  }

  updateInstaPaymentStatus(): Promise<any> {
    return new Promise((resolve: any) => {
      this.PFPayReq = JSON.parse(localStorage.getItem("PFReq"));
      this.PFPayRes = JSON.parse(localStorage.getItem("PFRes"));

      /**
       * DigitalPOS Change
       */
      if (this.IsPOSTransaction_flag == true) {
        this.posDealID = JSON.parse(localStorage.getItem("DealID"));
        this.deal_Id = this.posDealID;
      } else {
        this.deal_Id = this.propReq.DealId;
      }

      let body = {
        ID: this.propReq.id,
        policyNo: this.PFPayRes.pF_PolicyNo,
        coverNoteNo: this.PFPayRes.pF_CoverNoteNo,
        proposalNo: this.propRes.generalInformation.proposalNumber,
        dealID: this.deal_Id,
        customerID: this.propRes.generalInformation.customerId,
        pfPaymentID: null,
        paymentEntryErrorID: null,
        paymentEntryErrorText: null,
        paymentEntryStatus: null,
        paymentTagErrorID: null,
        paymentTagErrorText: this.PFPayRes.errorText,
        paymentTagStatus: "",
        message: this.PFPayRes.errorText,
        PaymentMode: this.PaymentMode,
        statusMessage: this.PFPayRes.status, // add status to errorText
        paymsRequestID: null,
        paymentRS: JSON.stringify(this.PFPayRes), // here pass full response string which is coming from Mihir api
        corelationID: this.propRes.correlationId, // pass correlation from payment api response
      };
      let str = JSON.stringify(body);
      // this.updatePolicyStatus();
      this.cs.post("Payment/UpdatePaymentDetails", str).then((res: any) => {
        this.updatePayment = res;
        resolve();
      });
    });
  }

  updatePolicyStatusCustLink() {
    this.totalTax = Math.round(this.propRes.totalTax);
    if (this.productCode == "2320" || this.productCode == "2319") {
      this.propRes.specialDiscount = "0";
      this.propRes.finalPremium = JSON.stringify(this.propRes.finalPremium);
      this.propRes.policyType = 1;
    }
    if (this.productCode == "2320") {
      this.propRes.policySubType = 9;
    } else if (this.productCode == "2319") {
      this.propRes.policySubType = 10;
    }
    let policysubType = this.propRes.policySubType;
    let breakinFlag: any = localStorage.getItem("breakinFlag");
    if (breakinFlag == "true") {
      let breakinData: any = JSON.parse(localStorage.getItem("breakinData"));
      this.SubProductType = breakinData.policySubType;
    } else {
      if (this.productCode == "2312") {
        this.SubProductType = "1";
      } else if (this.productCode == "2311") {
        this.SubProductType = "2";
      } else if (this.productCode == "2320") {
        this.SubProductType = "9";
      } else if (this.productCode == "2319") {
        this.SubProductType = "10";
      }
    }

    /**
     * DigitalPOS Change
     */
    if (this.IsPOSTransaction_flag == true) {
      this.posDealID = JSON.parse(localStorage.getItem("DealID"));
      this.deal_Id = this.posDealID;
    } else {
      this.deal_Id = this.propReq.DealId;
    }

    let body;
    body = {
      DealID: this.deal_Id,
      PolicyType: "1", //String
      PolicySubType: this.SubProductType, //String
      ProposalNumber: this.propRes.generalInformation.proposalNumber,
      PolicyStartDate: this.propReq.PolicyStartDate,
      PolicyEndDate: this.propReq.PolicyEndDate,
      Status: this.propRes.statusID,
      BasicPremium: this.propRes.totalLiabilityPremium,
      Discount: parseFloat(this.propRes.specialDiscount), //String
      ServiceTax: this.totalTax,
      TotalPremium: parseFloat(this.propRes.finalPremium), //String
      AddressID: "",
      Isactive: "1",
      PaymentID: JSON.stringify(this.updatePayment.paymentID),
      SalesTax: "",
      Surcharge: "",
      TotalTax: this.totalTax, //String
      TransFor: this.propRes.generalInformation.transactionType,
      TaxRate: "",
      CustomerType: this.propReq.CustomerType,
      PreviousPolicyNumber: null,
      PFCustomerID: this.propRes.generalInformation.customerId,
      CustomerName: this.propReq.CustomerDetails.CustomerName,
      PFPaymentID: "",
      SumInsured: null,
      Planname: null,
      PolicyTenure: this.propRes.generalInformation.tenure,
      PolicyTPTenure: this.propRes.generalInformation.tpTenure,
      PolicyPACoverTenure: JSON.stringify(this.propReq.PACoverTenure),
      UTGSTAmount: "0",
      TotalGSTAmount: "0",
      CGSTAmountRate: null,
      SGSTAmountRate: null,
      IGSTAmountRate: null,
      UTGSTAmountRate: null,
      TOTALGSTAmountRate: null,
      TransOn: "2020-03-18",
      CreatedBy: null,
      CreatedOn: "2020-03-18",
      ModifiedBy: null,
      ModifiedOn: "2020-03-18",
      CorelationID: this.propRes.correlationId,
      IsPayLinkSent: this.IsPayLinkSent,
      PayLinkReciever: this.PayLinkReciever,
      CustomerPayLink: this.CustomerPayLink,
      smsreciver: this.custMobile,
      isSMSSent: this.isSMSSent,
      // "IsStandaloneOD" : null,
      // "IsSubagent": null,
      // "SubagentIpartnerUserID": null
    };

    if (localStorage.getItem("isPendingPayment")) {
      // let instaData = JSON.parse(localStorage.getItem('InstaData'));
      body.PolicyNumber = this.instaData.instaPolicyNo;
      body.PFPolicyNo = this.instaData.instaPolicyNo;
      body.PFProposalNo = this.instaData.policyNo;
      body.PFCovernoteNo = this.instaData.instaCovernoteNo;
      body.policyID = this.instaData.policyID;
      body.isInstaDone = this.instaData.isInstaPayment;
    } else {
      body.PolicyNumber = "";
      body.PFPolicyNo = "";
      body.PFProposalNo = "";
      body.PFCovernoteNo = "";
    }

    if (this.quoteReq != null) {
      if (this.quoteReq.Tenure == "1" && this.quoteReq.TPTenure == "0") {
        body.IsStandaloneOD = true;
      }
    } else {
      body.IsStandaloneOD = this.instaData.isStandaloneOD;
    }

    if (
      this.paramDataValues.iPartnerLogin.isSubagent != null ||
      this.paramDataValues.iPartnerLogin.isSubagent != undefined
    ) {
      body.IsSubagent = true;
      body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
    }
    let str = JSON.stringify(body);
    this.cs.post("Policy/AddPolicyDetails", body).then((res: any) => {
      this.cs.clearLocalStorage();
      this.cs.clearQuoteDate();
      this.cs.clearPropData();
    });
  }

  // Lombard Pay APIS

  addmoney(payMode: any) {
    this.cs.getlombardPay().then((resp: any) => {
      localStorage.setItem(
        "LombardPayAuthToken",
        JSON.stringify(resp.accessToken)
      );
      this.CreateWalletOrder(payMode).then(() => {
        this.userDetails();
      });
    });
  }

  userDetails() {
    this.cs.get2("LombardPay/GetCustomerDetails").then((res: any) => {
      this.userdetails = res;
    });
  }

  sendAgentData() {
    if (this.cs.isUndefineORNull(this.paymentDetails.Name)) {
      // this.isName = false;
      swal({ closeOnClickOutside: false, text: "Enter Name" });
    } else if (this.cs.isUndefineORNull(this.paymentDetails.Email)) {
      // this.isEmail = false;
      swal({ closeOnClickOutside: false, text: "Enter Email" });
    } else if (this.cs.isUndefineORNull(this.paymentDetails.Contact)) {
      // this.isContact = false;
      swal({ closeOnClickOutside: false, text: "Enter contact" });
    } else {
      // this.isName = true; this.isEmail = true; this.isContact = true;
      let body = {
        Name: this.paymentDetails.Name,
        Email: this.paymentDetails.Email,
        Contact: this.paymentDetails.Contact,
      };
      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs.post2("LombardPay/CreateCustomer", str).then((response: any) => {
        this.cs.loaderStatus = false;
        $("#myModalmoney").modal("show");
      });
    }
  }

  CreateWalletOrder(mode: any): Promise<any> {
    return new Promise((resolve: any) => {
      let body = {
        amount: JSON.parse(this.paymentDetails.amount),
        identifier: "wallet order test",
      };
      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs
        .post2("LombardPay/CreateWalletOrder", str)
        .then((response: any) => {
          if (response.statusType == "Success") {
            this.orderId = response.razorOrderID;
            this.LombardRazorPay(this.router, mode);
            this.cs.loaderStatus = false;
            resolve();
          } else {
            $("#myModal").modal("show");
            resolve();
            this.cs.loaderStatus = false;
          }
        });
    });
  }

  // RazorPay Call
  LombardRazorPay(route: any, payMode: any) {
    var desc = "ICICI Lombard - Motor Insurance";
    let self = this;
    this.options = {
      description: desc,
      image: "https://www.icicilombard.com/mobile/mclaim/images/favicon.ico",
      currency: "INR",
      key: commonData.LombardKey,
      order_id: this.orderId,
      method: {
        netbanking: {
          order: ["ICIC", "HDFC", "SBIN", "UTIB", "IDFB", "IBKL"],
        },
      },
      prefill: {
        email: this.userdetails.email,
        contact: this.userdetails.contact,
        name: this.userdetails.name,
        method: payMode,
      },
      theme: {
        color: "#E04844",
        hide_topbar: true,
      },
      handler: function (response: any) {
        self.loadBalance(response, self, route);
      },
      modal: {
        ondismiss: function () {
          this.cs.loaderStatus = false;
        },
      },
    };
    var rzp1 = new Razorpay(this.options);
    rzp1.open();
  }

  loadBalance(res: any, _self: any, route: any) {
    return new Promise((resolve: any) => {
      let body = {
        razorpay_order_id: res.razorpay_order_id,
        razorpay_payment_id: res.razorpay_payment_id,
        razorpay_signature: res.razorpay_signature,
      };
      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs
        .post2("LombardPay/TransferToWallet", str)
        .then((response: any) => {
          if (response.statusType == "SUCCESS") {
            this.cs.getlombardPay().then((resp: any) => {
              localStorage.setItem(
                "LombardPayAuthToken",
                JSON.stringify(resp.accessToken)
              );
              this.showBalance();
            });
            this.addlombardmoneyFlag = false;
            this.paymentDetails.amount = "";
            this.showBalance();
            resolve();
          } else {
            this.addlombardmoneyFlag = false;
            this.paymentDetails.amount = "";
            this.showBalance();
            this.cs.loaderStatus = false;
            resolve();
          }
        })
        .catch((err: any) => {
          this.cs.loaderStatus = false;
          swal({
            closeOnClickOutside: false,
            text: err,
          });
        });
    });
  }

  showBalance() {
    return new Promise((resolve: any) => {
      this.cs.get2("LombardPay/GetCustomerBalance").then((response: any) => {
        if (response.statusType == "SUCCESS") {
          this.lombardpaybalance = response.balance;
          this.changeDetector.detectChanges();
        } else {
          this.lombardpaybalance = "0";
        }
      });
    });
  }

  payWalletAmount() {
    (<any>window).ga("send", "event", {
      eventCategory: "Lombard Pay",
      eventLabel:
        "Lombard Pay Clicked" + "" + this.product + "" + this.policyType,
      eventAction: "Lombard Pay Clicked",
      eventValue: 10,
    });

    this.cs.getlombardPay().then((resp: any) => {
      localStorage.setItem(
        "LombardPayAuthToken",
        JSON.stringify(resp.accessToken)
      );
      // this.checkBankingid().then(() => {
      this.PayLombardPay();
      // if (this.cs.isUndefineORNull(this.instaData)) {
      //   this.dataEntryPayment();
      //   }
      // });
    });
  }

  dataEntryPayment() {
    let bankType = localStorage.getItem("bankType");
    this.IsInstaDone = false;
    /**
     * DigitalPOS Change
     */
    if (this.IsPOSTransaction_flag == true) {
      this.posDealID = JSON.parse(localStorage.getItem("DealID"));
      this.deal_Id = this.posDealID;
    } else {
      this.deal_Id = this.propReq.DealId;
    }

    let body;
    if (
      bankType == "INT" &&
      (this.instaData == "" ||
        this.instaData == undefined ||
        this.instaData == null)
    ) {
      body = {
        isMappingRequired: true,
        isTaggingRequired: true,
        CorrelationId: this.propRes.correlationId,
        DealId: this.deal_Id,
        PaymentEntry: {
          onlineDAEntry: {
            CorrelationId: this.propRes.correlationId,
            DealId: this.deal_Id,
            AuthCode: "",
            CustomerID: this.propRes.generalInformation.customerId,
            MerchantID: commonData.lombardMerchant,
            TransactionId: commonData.lombardTransactionId,
            PaymentAmount: String(this.finalamount),
            InstrumentDate: this.startDate,
            ReceiptDate: this.startDate,
          },
        },
        PaymentTagging: {
          customerProposal: [
            {
              CustomerID: this.propRes.generalInformation.customerId,
              ProposalNo: this.propRes.generalInformation.proposalNumber,
            },
          ],
        },
        PaymentMapping: {
          customerProposal: [
            {
              CustomerID: this.propRes.generalInformation.customerId,
              ProposalNo: this.propRes.generalInformation.proposalNumber,
            },
          ],
        },
      };
    } else if (bankType == "INT" && this.instaData.instaPolicyNo) {
      body = {
        isMappingRequired: true,
        isTaggingRequired: false,
        CorrelationId: this.propRes.correlationId,
        DealId: this.deal_Id,
        PaymentEntry: {
          onlineDAEntry: {
            CorrelationId: this.propRes.correlationId,
            DealId: this.deal_Id,
            AuthCode: "",
            CustomerID: this.propRes.generalInformation.customerId,
            MerchantID: commonData.lombardMerchant,
            TransactionId: commonData.lombardTransactionId,
            PaymentAmount: String(this.finalamount),
            InstrumentDate: this.startDate,
            ReceiptDate: this.startDate,
          },
        },
        PaymentMapping: {
          customerProposal: [
            {
              CustomerID: this.propRes.generalInformation.customerId,
              ProposalNo: this.propRes.generalInformation.proposalNumber,
            },
          ],
        },
      };
    } else {
      body = {
        isMappingRequired: false,
        isTaggingRequired: true,
        CorrelationId: this.propRes.correlationId,
        DealId: this.deal_Id,
        PaymentEntry: {
          onlineDAEntry: {
            CorrelationId: this.propRes.correlationId,
            DealId: this.deal_Id,
            AuthCode: "",
            CustomerID: this.propRes.generalInformation.customerId,
            MerchantID: commonData.lombardMerchant,
            TransactionId: commonData.lombardTransactionId,
            PaymentAmount: String(this.finalamount),
            InstrumentDate: this.startDate,
            ReceiptDate: this.startDate,
          },
        },
        PaymentTagging: {
          customerProposal: [
            {
              CustomerID: this.propRes.generalInformation.customerId,
              ProposalNo: this.propRes.generalInformation.proposalNumber,
            },
          ],
        },
      };
    }

    if (
      !this.cs.isUndefineORNull(this.instaData) &&
      this.instaData.isInstaPayment
    ) {
      let str = JSON.stringify(body);
      let req = {
        CorrelationID: this.propRes.correlationId,
        ProposalNo: this.propRes.generalInformation.proposalNumber,
        ReqBody: str,
        ReqURL: "",
        ErrorMsg: "LombardPay Pay from wallet",
      };
      let str1 = JSON.stringify(req);
      this.cs
        .postpaymentreq("PaymsPayment/AddPaymsRequestDetails", str1)
        .then((res: any) => {});
    }
  }

  // need to change
  PayLombardPay() {
    // this.saveorderReq();
    // this.dataEntryPayment();
    if (this.lombardpaybalance < this.finalamount) {
      swal({
        closeOnClickOutside: false,
        text: "Your Lombard Pay wallet balance is low. Kindly add money",
      });
    } else {
      let body = {
        amount: this.finalamount,
        description: this.propRes.generalInformation.proposalNumber,
      };
      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs
        .post2("LombardPay/PayFromWallet", str)
        .then((response: any) => {
          this.PaymentMode = "3";
          this.paylombard = response;
          this.cs.loaderStatus = false;
          this.createLombardPFPayment(response);
          // if (this.cs.isUndefineORNull(this.instaData)) {
          //   this.dataEntryPayment();
          //   }
        })
        .catch((err: any) => {
          swal({ text: err.error }).then(() => {
            this.router.navigateByUrl("quote");
          });
          this.cs.loaderStatus = false;
        });
    }
  }

  // LombardPayPayment // need to change
  createLombardPFPayment(res: any): Promise<any> {
    return new Promise((resolve: any) => {
      let authCode = res.razorPaymentID.split("_");
      let bankType = localStorage.getItem("bankType");
      this.IsInstaDone = false;

      /**
       * DigitalPOS Change
       */
      if (this.IsPOSTransaction_flag == true) {
        this.posDealID = JSON.parse(localStorage.getItem("DealID"));
        this.deal_Id = this.posDealID;
      } else {
        this.deal_Id = this.propReq.DealId;
      }

      let body;
      if (
        bankType == "INT" &&
        (this.instaData == "" ||
          this.instaData == undefined ||
          this.instaData == null)
      ) {
        body = {
          isMappingRequired: true,
          isTaggingRequired: true,
          CorrelationId: this.propRes.correlationId,
          DealId: this.deal_Id,
          // "RazorPayResponse": {
          //   "razorpay_order_id": res.razorpay_order_id,
          //   "razorpay_payment_id": res.razorpay_payment_id,
          //   "razorpay_signature": res.razorpay_signature
          // },
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.propRes.correlationId,
              DealId: this.deal_Id,
              AuthCode: authCode[1],
              CustomerID: this.propRes.generalInformation.customerId,
              MerchantID: commonData.lombardMerchant,
              TransactionId: commonData.lombardTransactionId,
              PaymentAmount: String(this.finalamount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
            },
          },
          PaymentTagging: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
          PaymentMapping: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
        };
      } else if (bankType == "INT" && this.instaData.instaPolicyNo) {
        body = {
          isMappingRequired: true,
          isTaggingRequired: false,
          CorrelationId: this.propRes.correlationId,
          DealId: this.deal_Id,
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.propRes.correlationId,
              DealId: this.deal_Id,
              AuthCode: authCode[1],
              CustomerID: this.propRes.generalInformation.customerId,
              MerchantID: commonData.lombardMerchant,
              TransactionId: commonData.lombardTransactionId,
              PaymentAmount: String(this.finalamount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
            },
          },
          PaymentMapping: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
        };
      } else {
        body = {
          isMappingRequired: false,
          isTaggingRequired: true,
          CorrelationId: this.propRes.correlationId,
          DealId: this.deal_Id,
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.propRes.correlationId,
              DealId: this.deal_Id,
              AuthCode: authCode[1],
              CustomerID: this.propRes.generalInformation.customerId,
              MerchantID: commonData.lombardMerchant,
              TransactionId: commonData.lombardTransactionId,
              PaymentAmount: String(this.finalamount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
            },
          },
          PaymentTagging: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
        };
      }

      let str = JSON.stringify(body);
      let loader = document.getElementById("loaderdiv") as HTMLInputElement;
      loader.style.display = "block";
      this.cs
        .post1("Payment/PFPayment_LomPay", str)
        .then((response: any) => {
          localStorage.setItem("PFReq", JSON.stringify(body));
          localStorage.setItem("PFRes", JSON.stringify(response));

          if (response.statusMessage == "Success") {
            if (response.paymentEntryResponse.status == "Failed") {
              swal({
                closeOnClickOutside: false,
                title: "Error!",
                text: response.paymentEntryResponse.errorText,
              });
            } else {
              if (
                response.paymentTagResponse != null &&
                (response.paymentTagResponse.paymentTagResponseList[0].status ==
                  "Failed" ||
                  response.paymentTagResponse.paymentTagResponseList[0]
                    .status == "FAILED")
              ) {
                swal({
                  closeOnClickOutside: false,
                  title: "Error!",
                  text: response.paymentTagResponse.paymentTagResponseList[0]
                    .errorText,
                }).then(() => {});
              } else {
                localStorage.setItem(
                  "newProposalNumber",
                  JSON.stringify(this.propRes.generalInformation.proposalNumber)
                );
                if (response.paymentTagResponse == null) {
                  localStorage.setItem(
                    "newPolicyNumber",
                    JSON.stringify(
                      response.paymentMappingResponse.paymentMapResponseList[0]
                        .policyNo
                    )
                  );
                } else {
                  localStorage.setItem(
                    "newPolicyNumber",
                    JSON.stringify(
                      response.paymentTagResponse.paymentTagResponseList[0]
                        .policyNo
                    )
                  );
                }

                localStorage.setItem("instaPayment", JSON.stringify(false));

                // if(response.paymentTagResponse.paymentTagResponseList[0].policyNo == "" || response.paymentTagResponse.paymentTagResponseList[0].policyNo == null || response.paymentTagResponse.paymentTagResponseList[0].policyNo == undefined)
                // { swal({ title: "Error!", text: 'Payment Unsuccessful. Please Try again' }).then(()=> {
                //   this.router.navigateByUrl('quote'); }); } else
                // {
                // this.router.navigateByUrl('payment-confirmation');
                // location.href = PaymentURL.URL + "payment-confirmation";

                this.cs.pointingChange();
                // Local URL
                // location.href = "http://localhost:4200/#/payment-confirmation";
                // Sanity URL
                // location.href = "https://nysa-uat.insurancearticlez.com/nysaui/#/payment-confirmation"
                // Production URL
                // location.href = "https://ipartnerms.icicilombard.com/nysa/#/payment-confirmation"
                // location.href = "https://nysa.icicilombard.com/nysa/#/payment-confirmation"

                // }
                resolve();
              }
            }
          } else {
            // this.payLater();
            swal({
              closeOnClickOutside: false,
              title: "Error!",
              text: "Payment Unsuccessful. Please Try again",
            });
          }
          // Prajakta
          this.updatePaymentStatus().then(() => {
            this.updatePolicyStatus();
          });
          loader.style.display = "none";
          // this.router.navigateByUrl('/payment-confirmation');
        })
        .catch((err: any) => {
          loader.style.display = "none";
          swal({
            closeOnClickOutside: false,
            title: "Error!",
            text: err,
          });
          resolve();
        });
    });
  }

  instaPaymentID() {
    this.checkBankingid().then(() => {
      // this.getinstaDetails().then(()=> {

      this.difference =
        parseFloat(this.bankingidData.creditBalance) -
        parseFloat(this.bankingidData.balance);
      if (this.bankingidData.balance < this.finalamount) {
        swal({
          closeOnClickOutside: false,
          title: "Error!",
          text:
            "Your Insta balance is low. Balance: " + this.bankingidData.balance,
        });
      } else {
        this.showInstadiv = true;
        // swal({
        //
        //   text: "Your Insta Balance is: " + this.bankingidData.balance + '\n'  + " Your Credit balance is: " +
        //   this.bankingidData.creditBalance + '\n' + " Your consume balance is: " + this.difference
        // }).then(()=> {
        //   this.createInstaPFPayment()
        // });
      }
      // })
    });
  }

  createInstaPFPayment() {
    // let authCode = (res.razorPaymentID).split('_');
    let bankType = localStorage.getItem("bankType");
    this.IsInstaDone = true;
    this.PaymentMode = "2";

    /**
     * DigitalPOS Change
     */
    if (this.IsPOSTransaction_flag == true) {
      this.posDealID = JSON.parse(localStorage.getItem("DealID"));
      this.deal_Id = this.posDealID;
    } else {
      this.deal_Id = this.propReq.DealId;
    }
    let body;
    body = {
      isMappingRequired: false,
      isTaggingRequired: true,
      CorrelationId: this.propRes.correlationId,
      DealId: this.deal_Id,
      PaymentTagging: {
        customerProposal: [
          {
            CustomerID: this.propRes.generalInformation.customerId,
            ProposalNo: this.propRes.generalInformation.proposalNumber,
          },
        ],
      },
    };

    let str = JSON.stringify(body);
    let loader = document.getElementById("loaderdiv") as HTMLInputElement;
    loader.style.display = "block";
    this.cs
      .post1("Payment/PFPaymentInsta", str)
      .then((response: any) => {
        localStorage.setItem("PFReq", JSON.stringify(body));
        localStorage.setItem("PFRes", JSON.stringify(response));
        if (response.is_Tagging_Success == true) {
          this.updateInstaPaymentStatus().then(() => {
            this.updateInstaPolicyStatus().then(() => {
              this.cs.pointingChange();
            });
          });
          localStorage.setItem(
            "newProposalNumber",
            JSON.stringify(response.pF_ProposalNo)
          );
          localStorage.setItem(
            "newPolicyNumber",
            JSON.stringify(response.pF_PolicyNo)
          );
          localStorage.setItem("instaPayment", JSON.stringify(true));
          // if(response.paymentTagResponse.paymentTagResponseList[0].policyNo == "" || response.paymentTagResponse.paymentTagResponseList[0].policyNo == null || response.paymentTagResponse.paymentTagResponseList[0].policyNo == undefined)
          // { swal({ title: "Error!", text: 'Payment Unsuccessful. Please Try again' }).then(()=> {
          //   this.router.navigateByUrl('quote'); }); } else
          {
            // this.cs.pointingChange();
            // this.router.navigateByUrl('payment-confirmation');
            // location.href = PaymentURL.URL + "payment-confirmation";
            // Local URL
            // location.href = "http://localhost:4200/#/payment-confirmation";
            // Sanity URL
            // location.href = "https://nysa-uat.insurancearticlez.com/nysaui/#/payment-confirmation"
            // Production URL
            // location.href = "https://ipartnerms.icicilombard.com/nysa/#/payment-confirmation"
            // location.href = "https://nysa.icicilombard.com/nysa/#/payment-confirmation"
          }
        } else {
          this.updateInstaPaymentStatus().then(() => {
            this.updateInstaPolicyStatus();
          });
          let errorbody = {
            RequestJson: JSON.stringify(body),
            ResponseJson: JSON.stringify(response),
            ServiceURL: commonData.bizURL + URL,
            CorrelationID: this.propRes.correlationId,
          };
          this.api.adderrorlogs(errorbody);

          // this.payLater();
          swal({
            closeOnClickOutside: false,
            text: response.errorText,
          });
        }
        loader.style.display = "none";
        // this.router.navigateByUrl('/payment-confirmation');
      })
      .catch((err: any) => {
        this.updateInstaPaymentStatus().then(() => {
          this.updateInstaPolicyStatus();
        });
        loader.style.display = "none";
        swal({
          closeOnClickOutside: false,
          text: err,
        });
      });
  }

  // Go to Dashboard
  goToDashboard() {
    this.cs.geToDashboard();
  }

  // MobileNo Validation
  numberOnly(event: any): boolean {
    console.log('payment component number only')
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  goToProposal() {
    this.router.navigateByUrl("proposal");
  }
}
