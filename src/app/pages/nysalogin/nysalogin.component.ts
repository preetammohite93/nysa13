import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { CommonService } from "src/app/services/common.service";
import swal from "sweetalert";
import { timer } from "rxjs";
import { commonData } from "src/app/commonData/commonData";
declare var $: any;

@Component({
  selector: "app-nysalogin",
  templateUrl: "./nysalogin.component.html",
  styleUrls: ["./nysalogin.component.css"],
  // styles: ['body { padding-top: 20px; font-family: "Muli", sans-serif !important;overflow-x: hidden; }']
  // encapsulation: ViewEncapsulation.None,
})
export class NysaloginComponent implements OnInit {
  timeLeft: number = 120;
  interval;
  subscribeTimer: any;
  isUserNameValid = true;
  isPasswordValid = true;

  config = {
    length: 6,
    allowNumbersOnly: true,
    inputStyles: {
      width: "35px",
      height: "40px",
      "font-family": "Muli",
    },
  };

  loginForm: any = {};
  login = {
    userID: "",
    pwd: "",
  };
  loginOTP = {
    OTP: "",
  };
  nysaUrl: any;
  error: any;
  showlogin: boolean = true;
  mobileNo: any;
  lastdigit: any;
  showOTP: boolean = false;
  OTPres: any;
  otp: string;
  showpass = false;
  isLoginError = false;

  constructor(
    public router: Router,
    public cs: CommonService,
    public activeRoute: ActivatedRoute
  ) {}

  oberserableTimer() {
    const source = timer(1000, 2000);
    const abc = source.subscribe((val) => {
      this.subscribeTimer = this.timeLeft - val;
    });
  }

  ngOnInit() {
    // localStorage.removeItem("paramData");
    this.cs.clearLocalStorage();
    this.cs.clearPropData();
    this.cs.clearQuoteDate();
    $(".reveal").on("click", function () {
      var $pwd = $(".pwd");
      if ($pwd.attr("type") === "password") {
        $pwd.attr("type", "text");
      } else {
        $pwd.attr("type", "password");
      }
    });
  }

  toggleShow() {
    // console.log(this.showpass);
    let pass = document.getElementById("passType") as HTMLInputElement;
    console.log(pass);
    this.showpass = !this.showpass;
    // this.showpass ? 'text' : 'password';
    if (this.showpass) {
      pass.type = "text";
    } else {
      pass.type = "password";
    }
    console.log(this.showpass);
  }

  startTimer() {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        clearInterval(this.interval);
        this.timeLeft = 120;
        this.showOTP = false;
      }
    }, 1000);
  }

  validateUsername(e: any) {
    this.isLoginError = false;
    this.login.userID = e.target.value;
    if (this.cs.isUndefineORNull(this.login.userID)) {
      this.isUserNameValid = false;
    } else {
      this.isUserNameValid = true;
    }
  }

  validatePass(e) {
    this.login.pwd = e.target.value;
    if (this.cs.isUndefineORNull(this.login.pwd)) {
      this.isPasswordValid = false;
    } else {
      this.isPasswordValid = true;
    }
  }

  logintoNysa() {
    this.cs.loaderStatus = true;
    let pwd = btoa(this.login.pwd);
    let username = btoa(this.login.userID);
    this.cs
      .getWithAuth(
        "NysaLogin/AuthenticateUser?userID=" + username + "&pwd=" + pwd
      )
      .then((res: any) => {
        if (res.status == "Success") {
          this.cs.loaderStatus = false;

          // this.nysaUrl = res.output;
          // var parts = this.nysaUrl.split("#", 2);
          // window.location.href = commonData.plutusSanityUI + parts[1];

          window.location.href = res.output;
        } else {
          this.cs.loaderStatus = false;
          this.error = res.message;
          this.isLoginError = true;
        }
      })
      .catch((err: any) => {
        this.isLoginError = true;
      });
    // }
  }

  // logintoNysa() {
  //   if (this.cs.isUndefineORNull(this.login.userID)) {
  //     // swal({ text: "Username is required" });
  //     this.isUserNameValid = false;
  //   } else if (this.cs.isUndefineORNull(this.login.pwd)) {
  //     this.isUserNameValid = true;
  //     this.isPasswordValid = false;
  //     // swal({ text: "Password is required" });
  //   } else {
  //     this.isUserNameValid = true;
  //     this.isPasswordValid = true;
  //     let pwd = btoa(this.login.pwd);
  //     let username = btoa(this.login.userID);
  //     this.cs
  //       .getWithAuth(
  //         "NysaLogin/AuthenticateUser?userID=" + username + "&pwd=" + pwd
  //       )
  //       .then((res: any) => {
  //         this.cs.loaderStatus = true;
  //         if (res.status == "Success") {
  //           this.cs.loaderStatus = false;

  //           // this.nysaUrl = res.output;
  //           // var parts = this.nysaUrl.split('#', 2);
  //           // window.location.href = commonData.plutusSanityUI + parts[1];

  //           window.location.href = res.output;
  //         } else {
  //           this.cs.loaderStatus = false;

  //           this.error = res.message;
  //           swal({
  //             // title: "Error!",
  //             text: res.message,
  //           });
  //         }
  //       })
  //       .catch((err: any) => {
  //         swal({
  //           // title: "Error!",
  //           text: "Login Failed",
  //         });
  //       });
  //   }
  // }

  loginusingOTP() {
    this.showlogin = false;
  }

  loginusingpass() {
    this.showlogin = true;
    this.showOTP = false;
  }

  getOTP() {
    clearInterval(this.interval);
    this.timeLeft = 120;
    let body = {
      IMID: this.login.userID,
    };
    let str = JSON.stringify(body);
    this.cs.post("NysaLogin/GetOTP ", str).then((res: any) => {
      if (res.status == "Success") {
        this.OTPres = res;
        this.mobileNo = res.mobileNo;
        this.lastdigit = this.mobileNo.slice(6, 10);
        this.showOTP = true;
        this.startTimer();
      } else {
        this.error = res.message;
        swal({
          text: res.message,
        });
      }
    });
  }

  onOtpChange(otp) {
    this.otp = otp;
  }

  validateOTP() {
    let body = {
      AuthID: this.OTPres.authCode,
      OTP: this.otp,
      IMID: this.login.userID,
    };
    let str = JSON.stringify(body);
    this.cs.post("NysaLogin/ValidateOTPRequest ", str).then((res: any) => {
      this.cs.loaderStatus = true;
      if (res.status == "Success") {
        this.cs.loaderStatus = false;
        this.nysaUrl = res.output;
        window.location.href = this.nysaUrl;
      } else {
        this.cs.loaderStatus = false;
        this.error = res.message;
        swal({
          text: res.message,
        });
      }
    });
  }
}
