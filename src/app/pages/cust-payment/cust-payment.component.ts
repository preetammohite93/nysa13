import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import * as moment from "moment";
import { commonData } from "../../commonData/commonData";
import { CommonService } from "src/app/services/common.service";
import { PaymentURL } from "src/environments/env";
declare var Razorpay: any;
import swal from "sweetalert";
declare var $: any;

@Component({
  selector: "app-cust-payment",
  templateUrl: "./cust-payment.component.html",
  styleUrls: ["./cust-payment.component.css"],
})
export class CustPaymentComponent implements OnInit {
  CustToken: any;
  custLinkDetails: any;
  quoteReq: any;
  quoteRes: any;
  propReq: any;
  propRes: any;
  custName: any;
  custMobile: any;
  custEmail: any;
  paymentMode: any;
  startDate: any;
  finalamount: any;
  todayDate: any;
  orderId: any;
  options: any;
  PayKey: any;
  transactionID: any;
  showPaymentOpyions: boolean = true;
  dontShowPage: boolean = true;
  showncb: boolean = true;
  bankType: any;
  isPlutus: boolean = false;
  correlationID: any;
  successpayment: boolean = false;

  /**
   * DigitalPOS variables
   */
  IsPOSTransaction_flag: boolean = false;
  deal_Id: any;

  constructor(
    public router: Router,
    public cs: CommonService,
    public activeRoute: ActivatedRoute,
    private changeDetector: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.PayKey = commonData.PayKey;
    this.cs.getPayAuth().then((resp: any) => {
      localStorage.setItem("PayAuthToken", JSON.stringify(resp.accessToken));
    });

    this.getParams().then(() => {
      this.getCustomerPaymentDetails(this.CustToken).then(() => {
        if (this.quoteReq == undefined || this.quoteReq == null) {
        }
      });
    });
  }

  clearLocal() {
    localStorage.removeItem("PropDatares");
    localStorage.removeItem("PropDatareq");
  }

  getParams(): Promise<any> {
    return new Promise((resolve: any) => {
      this.activeRoute.queryParams.forEach((params) => {
        this.clearLocal();
        this.CustToken = params.CustToken;
        localStorage.setItem("custToken", this.CustToken);
        resolve();
      });
    });
  }

  getCustomerPaymentDetails(custToken): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs
        .getWithAuth(
          "CustomerPayment/GetCustomerPaymentDetails?CustToken=" + custToken
        )
        .then((res: any) => {
          this.custLinkDetails = res;
          if (res.status != "Failed") {
            console.log(this.custLinkDetails.quoteRS);
            console.log(this.custLinkDetails.proposalRS);
            this.IsPOSTransaction_flag = res.isDigitalPOSTrans; // setting digitalPOS variable to hide or show the payment modes on ui
            this.quoteReq = JSON.parse(this.custLinkDetails.quoteRQ);
            this.quoteRes = JSON.parse(this.custLinkDetails.quoteRS);
            this.propReq = JSON.parse(this.custLinkDetails.proposalRQ);
            this.propRes = JSON.parse(this.custLinkDetails.proposalRS);
            this.correlationID = this.custLinkDetails.correlationID;
            /**
             * digitalPOS Change For Calling The Correct The API According To The Customer Type For POS And Nysa
             */
            if (this.IsPOSTransaction_flag == true) {
              this.cs.encryptCustomerData(res.ipartnerUserID).then((ipuid) => {
                this.cs.getPaymsTokenForPOSCustomer(ipuid).then((resp: any) => {
                  localStorage.setItem(
                    "paymsToken",
                    JSON.stringify(resp.accessToken)
                  );
                });
              });
            } else {
              this.cs.encryptCustomerData(res.ipartnerUserID).then((ipuid) => {
                this.cs.getPaymsTokenForCustomer(ipuid).then((resp: any) => {
                  localStorage.setItem(
                    "paymsToken",
                    JSON.stringify(resp.accessToken)
                  );
                });
              });
            }

            if (this.custLinkDetails.isPlutusEnable == true) {
              this.isPlutus = true;
            }
            this.bankType = this.custLinkDetails.bankType;
            // localStorage.setItem('PropDatares',(this.custLinkDetails.proposalRS));
            //  localStorage.setItem('PropDatareq',(this.custLinkDetails.proposalRQ));
            //  localStorage.setItem('calQuoteReq',(this.custLinkDetails.quoteRQ));
            //  localStorage.setItem('calQuoteRes',(this.custLinkDetails.quoteRS));

            /**
             * DigitalPOS change
             */
            if (this.IsPOSTransaction_flag == true) {
              this.finalamount = this.custLinkDetails.finalPremium;
            } else {
              this.finalamount = Math.round(this.propRes.finalPremium);
            }

            this.startDate = moment(new Date()).format("YYYY-MM-DD");
            this.todayDate = moment(new Date()).format("YYYY-MM-DD");
            resolve();
          } else {
            swal({
              title: "Error!",
              text: res.message,
            });
            this.dontShowPage = false;
            resolve();          
          }
        });
      
    }).catch((err: any) => {
      swal({
        title: "Error!",
        text: "Your Payment link has been expired",
      });
      this.dontShowPage = false;
    });
  }

  getPaymentBreakUP() {
    let openbreakup =
      commonData.custpaymentBreakUPURL +
      "?CorrelationID=" +
      this.custLinkDetails.corelationID;
    window.open(
      openbreakup,
      "Ratting",
      "width=750,height=600,0,status=0,scrollbars=1, toolbar=yes, menubar=yes "
    );
  }

  customerData(mode: any) {
    this.custName = this.propReq.CustomerDetails.CustomerName;
    this.custMobile = this.propReq.CustomerDetails.MobileNumber;
    this.custEmail = this.propReq.CustomerDetails.Email;
    this.paymentMode = mode;
    this.pay();
  }

  pay() {
    this.cs
      .getPayAuth()
      .then((resp: any) => {
        localStorage.setItem("PayAuthToken", JSON.stringify(resp.accessToken));
      })
      .then(() => {
        this.createOrder().then(() => {
          this.payByRazorPay(this.router);
        });
      });
  }

  createOrder(): Promise<any> {
    return new Promise((resolve: any) => {
      this.saveorderReq();
      let body = {
        CorrelationID: this.propRes.correlationId, //CorelationID
        amount: JSON.stringify(this.finalamount), //Premium Amount
        // "amount": this.cs.encrypt(JSON.stringify(this.finalamount), commonData.aesnysaKey),
        identifier:
          "ipartner_policy_" + this.propRes.generalInformation.proposalNumber, //Proposal Number
      };
      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs.post1("Payment/CreateOrder", str).then((response: any) => {
        if (response.status == "created") {
          this.orderId = response.id;
          localStorage.setItem("orderReq", JSON.stringify(body));
          localStorage.setItem("orderRes", JSON.stringify(response));
          this.cs.loaderStatus = false;
          resolve();
        } else {
          resolve();
          this.cs.loaderStatus = false;
        }
      });
    });
  }

  saveorderReq() {
    let body;
    if (
      this.bankType == "INT" &&
      this.custLinkDetails.isInstaDone == false &&
      (this.custLinkDetails.instaPolicyNo == "" ||
        this.custLinkDetails.instaPolicyNo == null ||
        this.custLinkDetails.instaPolicyNo == undefined)
    ) {
      body = {
        isMappingRequired: true,
        isTaggingRequired: true,
        CorrelationId: this.propRes.correlationId,
        DealId: this.propReq.DealId,
        PaymentEntry: {
          onlineDAEntry: {
            CorrelationId: this.propRes.correlationId,
            DealId: this.propReq.DealId,
            CustomerID: this.propRes.generalInformation.customerId,
            MerchantID: commonData.MerchantID,
            TransactionId: commonData.TransactionId,
            PaymentAmount: String(this.finalamount),
            InstrumentDate: this.startDate,
            ReceiptDate: this.startDate,
          },
        },
        PaymentTagging: {
          DealID: this.propReq.DealId,
          CorrelationID: this.propRes.correlationId,
          customerProposal: [
            {
              CustomerID: this.propRes.generalInformation.customerId,
              ProposalNo: this.propRes.generalInformation.proposalNumber,
            },
          ],
        },
        PaymentMapping: {
          DealID: this.propReq.DealId,
          CorrelationID: this.propRes.correlationId,
          customerProposal: [
            {
              CustomerID: this.propRes.generalInformation.customerId,
              ProposalNo: this.propRes.generalInformation.proposalNumber,
            },
          ],
        },
      };
    } else if (
      this.bankType == "INT" &&
      this.custLinkDetails.isInstaDone == true &&
      this.custLinkDetails.instaPolicyNo
    ) {
      body = {
        isMappingRequired: true,
        isTaggingRequired: false,
        CorrelationId: this.propRes.correlationId,
        DealId: this.propReq.DealId,
        PaymentEntry: {
          onlineDAEntry: {
            CorrelationId: this.propRes.correlationId,
            DealId: this.propReq.DealId,
            CustomerID: this.propRes.generalInformation.customerId,
            MerchantID: commonData.MerchantID,
            TransactionId: commonData.TransactionId,
            PaymentAmount: String(this.finalamount),
            InstrumentDate: this.startDate,
            ReceiptDate: this.startDate,
          },
        },
        PaymentMapping: {
          DealID: this.propReq.DealId,
          CorrelationID: this.propRes.correlationId,
          customerProposal: [
            {
              CustomerID: this.propRes.generalInformation.customerId,
              ProposalNo: this.propRes.generalInformation.proposalNumber,
            },
          ],
        },
      };
    } else {
      body = {
        isMappingRequired: false,
        isTaggingRequired: true,
        CorrelationId: this.propRes.correlationId,
        DealId: this.propReq.DealId,
        PaymentEntry: {
          onlineDAEntry: {
            CorrelationId: this.propRes.correlationId,
            DealId: this.propReq.DealId,
            CustomerID: this.propRes.generalInformation.customerId,
            MerchantID: commonData.MerchantID,
            TransactionId: commonData.TransactionId,
            PaymentAmount: String(this.finalamount),
            InstrumentDate: this.startDate,
            ReceiptDate: this.startDate,
          },
        },
        PaymentTagging: {
          DealID: this.propReq.DealId,
          CorrelationID: this.propRes.correlationId,
          customerProposal: [
            {
              CustomerID: this.propRes.generalInformation.customerId,
              ProposalNo: this.propRes.generalInformation.proposalNumber,
            },
          ],
        },
        PaymentMapping: null,
      };
    }

    let str = JSON.stringify(body);
    this.cs
      .post1("Payment/SaveOrderRequestBody", str)
      .then((response: any) => {});
  }

  // RazorPay Call
  payByRazorPay(route: any) {
    var desc = "ICICI Lombard - Motor Insurance";
    let self = this;
    this.options = {
      description: desc,
      image: "https://www.icicilombard.com/mobile/mclaim/images/favicon.ico",
      currency: "INR",
      key: this.PayKey,
      order_id: this.orderId,
      method: {
        netbanking: {
          order: ["ICIC", "HDFC", "SBIN", "UTIB", "IDFB", "IBKL"],
        },
      },
      prefill: {
        email: this.custEmail,
        contact: this.custMobile,
        name: this.custName,
        method: this.paymentMode,
      },
      theme: {
        color: "#E04844",
        hide_topbar: true,
      },
      handler: function (response: any) {
        self.createPFPaymentAsync(response, self, route);
      },
    };
    var rzp1 = new Razorpay(this.options);
    rzp1.open();
  }

  createPFPaymentAsync(res: any, _self: any, route: any): Promise<any> {
    return new Promise((resolve: any) => {
      let authCode = res.razorpay_payment_id.split("_");
      this.transactionID = res.razorpay_payment_id.split("_");
      let body;
      if (
        this.bankType == "INT" &&
        this.custLinkDetails.isInstaDone == false &&
        (this.custLinkDetails.instaPolicyNo == "" ||
          this.custLinkDetails.instaPolicyNo == null ||
          this.custLinkDetails.instaPolicyNo == undefined)
      ) {
        body = {
          isMappingRequired: true,
          isTaggingRequired: true,
          CorrelationId: this.propRes.correlationId,
          DealId: this.propReq.DealId,
          RazorPayResponse: {
            razorpay_order_id: res.razorpay_order_id,
            razorpay_payment_id: res.razorpay_payment_id,
            razorpay_signature: res.razorpay_signature,
          },
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.propRes.correlationId,
              DealId: this.propReq.DealId,
              AuthCode: authCode[1],
              CustomerID: this.propRes.generalInformation.customerId,
              MerchantID: commonData.MerchantID,
              TransactionId: commonData.TransactionId,
              PaymentAmount: String(this.finalamount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
            },
          },
          PaymentTagging: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
          PaymentMapping: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
        };
      } else if (
        this.bankType == "INT" &&
        this.custLinkDetails.isInstaDone == true &&
        this.custLinkDetails.instaPolicyNo
      ) {
        body = {
          isMappingRequired: true,
          isTaggingRequired: false,
          CorrelationId: this.propRes.correlationId,
          DealId: this.propReq.DealId,
          RazorPayResponse: {
            razorpay_order_id: res.razorpay_order_id,
            razorpay_payment_id: res.razorpay_payment_id,
            razorpay_signature: res.razorpay_signature,
          },
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.propRes.correlationId,
              DealId: this.propReq.DealId,
              AuthCode: authCode[1],
              CustomerID: this.propRes.generalInformation.customerId,
              MerchantID: commonData.MerchantID,
              TransactionId: commonData.TransactionId,
              PaymentAmount: String(this.finalamount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
            },
          },
          PaymentMapping: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
        };
      } else {
        body = {
          isMappingRequired: false,
          isTaggingRequired: true,
          CorrelationId: this.propRes.correlationId,
          DealId: this.propReq.DealId,
          RazorPayResponse: {
            razorpay_order_id: res.razorpay_order_id,
            razorpay_payment_id: res.razorpay_payment_id,
            razorpay_signature: res.razorpay_signature,
          },
          PaymentEntry: {
            onlineDAEntry: {
              CorrelationId: this.propRes.correlationId,
              DealId: this.propReq.DealId,
              AuthCode: authCode[1],
              CustomerID: this.propRes.generalInformation.customerId,
              MerchantID: commonData.MerchantID,
              TransactionId: commonData.TransactionId,
              PaymentAmount: String(this.finalamount),
              InstrumentDate: this.startDate,
              ReceiptDate: this.startDate,
            },
          },
          PaymentTagging: {
            customerProposal: [
              {
                CustomerID: this.propRes.generalInformation.customerId,
                ProposalNo: this.propRes.generalInformation.proposalNumber,
              },
            ],
          },
        };
      }
      let str = JSON.stringify(body);
      let loader = document.getElementById("loaderdiv") as HTMLInputElement;
      loader.style.display = "block";
      _self.cs
        .post1("Payment/PFPaymentAsync", str)
        .then((response: any) => {
          if (response == "Request Sent") {
            localStorage.setItem("PFReq", JSON.stringify(body));
            localStorage.setItem("PFRes", JSON.stringify(response));
            this.successpayment = true;
            this.changeDetector.detectChanges();
            window.top.close();
            this.showPaymentOpyions = false;
            this.dontShowPage = false;
            loader.style.display = "none";
          } else {
            swal({
              closeOnClickOutside: false,
              title: "Error!",
              text: "There seems to be a problem with the payment. Please try again with a new proposal creation.",
            }).then((result: any) => {
              loader.style.display = "none";
              this.changeDetector.detectChanges();
              window.top.close();
              this.showPaymentOpyions = false;
              this.dontShowPage = false;
              
            });
            let req = {
              CorrelationID: this.propRes.correlationId,
              CustToken: this.CustToken,
              ProposalNo: this.propRes.generalInformation.proposalNumber,
              ReqBody: str,
              ReqURL: commonData.baseURL1 + "Payment/PFPaymentAsync",
              ErrorMsg: response,
            };
            let str1 = JSON.stringify(req);
            this.cs
              .getWithAuth1("PaymsPayment/AddCustomerPaymsRequestDetails", str1)
              .then((res: any) => {});
          }
        })
        .catch((err: any) => {
          loader.style.display = "none";
          swal({
            title: "Error!",
            text: "There seems to be a problem with the payment. Please try again with a new proposal creation",
          });

          let req = {
            CorrelationID: this.propRes.correlationId,
            CustToken: this.CustToken,
            ProposalNo: this.propRes.generalInformation.proposalNumber,
            ReqBody: str,
            ReqURL: commonData.baseURL1 + "Payment/PFPaymentAsync",
            ErrorMsg: JSON.stringify(err),
          };
          let str1 = JSON.stringify(req);
          this.cs
            .getWithAuth1("PaymsPayment/AddCustomerPaymsRequestDetails", str1)
            .then((res: any) => {});
          resolve();
        });
    });
  }

  /**
   * fetching the paymsToken from api before redirecting to plutus payment page
   * Author :- Sumit
   * date :- 21-01-2022
   */
  PlutusPaymentPaymsToken() {
    this.cs.getPaymsToken().then((resp: any) => {
      localStorage.setItem("paymsToken", JSON.stringify(resp.accessToken));
      this.redirectToPlutusPayment();
    });
  }

  redirectToPlutusPayment() {
    /**
     * DigitalPOS Change
     */
    let bankType;
    if (this.IsPOSTransaction_flag == true) {
      (bankType = "MAT"), (this.deal_Id = this.custLinkDetails.dealid);
    } else {
      bankType = this.custLinkDetails.bankType;
      this.deal_Id = this.quoteReq.DealId;
    }

    let instaFlag;
    if (bankType == "INT") {
      instaFlag = 1;
    } else {
      instaFlag = 0;
    }

    let email = this.cs.encrypt(
      this.propReq.CustomerDetails.Email,
      commonData.aesSecretKey
    );
    let phone = this.cs.encrypt(
      this.propReq.CustomerDetails.MobileNumber,
      commonData.aesSecretKey
    );

    let body = {
      CorrelationID: this.propRes.correlationId,
      Amount: this.propRes.finalPremium,
      ProposalNo: this.propRes.generalInformation.proposalNumber,
      DealID: this.deal_Id,
      CustomerID: this.propRes.generalInformation.customerId,
      Email: email,
      ContactNo: phone,
      UserFlag: 1,
      IsInsta: instaFlag,
      MultiFlag: 0,
      PidFlag: 0,
      PreInsta: 0
    };

    let str = JSON.stringify(body);

    this.cs
      .postPayms("Redirection/AddPaymentRequest", str)
      .then((res: any) => {
        if (res.Status == "success") {
          if (this.IsPOSTransaction_flag == true) {
            location.href =
              res.URL +
              "&MOBILENO=" +
              this.custLinkDetails.ipartnerUserID +
              "&channelName=DigitalPOS";
          } else {
            location.href = res.URL;
          }
        } else {
          this.isPlutus = false;
        }
      })
      .catch((err: any) => {
        this.isPlutus = false;
      });
  }
}
