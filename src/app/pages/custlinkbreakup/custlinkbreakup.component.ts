import { DOCUMENT } from "@angular/common";
import { Component, Inject, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ApiServiceService } from "src/app/services/api-service.service";
import { CommonService } from "src/app/services/common.service";
import swal from "sweetalert";

@Component({
  selector: "app-custlinkbreakup",
  templateUrl: "./custlinkbreakup.component.html",
  styleUrls: ["./custlinkbreakup.component.css"],
})
export class CustlinkbreakupComponent implements OnInit {
  correlationID: any;
  policy_Subtype: any;
  calculatedQuote: any;
  quoteResp: any;
  quoteReq: any;
  quotere: any;
  dealid: any;
  bankType: any;
  dealDetailsfromIM: any;
  stateCD: any;
  isRSAPlan: any;
  productCode: string;
  ODandTPAmount: any;
  totalDiscount: any;
  premiumWithoutDiscount: any;

  constructor(
    public activeRoute: ActivatedRoute,
    public cs: CommonService,
    public api: ApiServiceService,
    @Inject(DOCUMENT) private document
  ) {
    this.cs.loaderStatus = true;
  }

  ngOnInit() {
    this.document.body.style.padding = "10px 10px";
    this.cs.loaderStatus = true;
    this.activeRoute.queryParams.forEach((params) => {
      this.correlationID = params.CorrelationID;
      this.getPriceBreakUP();
    });
  }

  getPriceBreakUP() {
    this.cs
      .getBreakUPDetails(
        "customer/GetPremiumBreakupDetails?correlationID=" + this.correlationID
      )
      .then((res: any) => {
        if (res.status == "Failed") {
          swal({ text: res.message });
        } else {
          this.quoteReq = JSON.parse(res.quoteRQ);
          this.calculatedQuote = JSON.parse(res.proposalRS);
          this.quotere = JSON.parse(res.quoteRS);
          this.policy_Subtype = this.quotere.PolicySubType;
          if (this.policy_Subtype == "1" || this.policy_Subtype == "12") {
            this.productCode = "2312";
          } else if (
            this.policy_Subtype == "2" ||
            this.policy_Subtype == "13"
          ) {
            this.productCode = "2311";
          } else if (this.policy_Subtype == "9") {
            this.productCode = "2320";
          } else if (this.policy_Subtype == "10") {
            this.productCode = "2319";
          }
          this.dealid = this.quoteReq.DealId;
          this.quoteResp = JSON.parse(res.quoteRS);
          this.ODandTPAmount =
            this.quoteResp.riskDetails.basicOD +
            this.quoteResp.riskDetails.basicTP;
          let antiTheftDiscount = this.calculatedQuote.riskDetails
            .antiTheftDiscount
            ? this.calculatedQuote.riskDetails.antiTheftDiscount
            : 0;
          let automobileAssociationDiscount = this.calculatedQuote.riskDetails
            .automobileAssociationDiscount
            ? this.calculatedQuote.riskDetails.automobileAssociationDiscount
            : 0;
          let handicappedDiscount = this.calculatedQuote.riskDetails
            .handicappedDiscount
            ? this.calculatedQuote.riskDetails.handicappedDiscount
            : 0;
          let bonusDiscount = this.calculatedQuote.riskDetails.bonusDiscount
            ? this.calculatedQuote.riskDetails.bonusDiscount
            : 0;
          let voluntaryDiscount = this.calculatedQuote.riskDetails
            .voluntaryDiscount
            ? this.calculatedQuote.riskDetails.voluntaryDiscount
            : 0;
          let tppD_Discount = this.calculatedQuote.riskDetails.tppD_Discount
            ? this.calculatedQuote.riskDetails.tppD_Discount
            : 0;
          this.totalDiscount =
            antiTheftDiscount +
            automobileAssociationDiscount +
            handicappedDiscount +
            bonusDiscount +
            voluntaryDiscount +
            tppD_Discount;
          let pkgPrem = this.calculatedQuote.packagePremium
            ? this.calculatedQuote.packagePremium
            : this.calculatedQuote.totalLiabilityPremium;
          this.premiumWithoutDiscount = pkgPrem + this.totalDiscount;
          if (this.cs.isUndefineORNull(this.quoteReq.RSAPlanName)) {
            this.isRSAPlan = false;
          } else {
            this.isRSAPlan = true;
          }
        }
        this.cs.loaderStatus = false;
      })
      .catch((err: any) => {
        this.cs.loaderStatus = false;
      });
  }

  //Get Deal Details from deal
  getDealDetails() {
    this.api.getDealDetails(this.dealid).subscribe((res: any) => {
      if (res.status == "FAILED") {
        swal({ text: "Deal Details not found" });
      } else {
        this.dealDetailsfromIM = res;
        this.stateCD = this.dealDetailsfromIM.stateCD;
        this.bankType = this.dealDetailsfromIM.bankType;
        localStorage.setItem("bankType", this.bankType);
      }
    });
  }

  get getODSubTotal() {
    let total = 0;
    let count;
    if (this.productCode == "2312") {
      count = 8;
    } else if (this.productCode == "2311") {
      count = 14;
    }
    for (let i = 0; i < count; i++) {
      let data = String(document.getElementById(`odSub-${i + 1}`).innerHTML);
      total += Number(data.replace(/\D/g, ""));
    }
    return total;
  }

  get getTpTotal() {
    let total = 0;
    for (let i = 0; i < 3; i++) {
      let data = String(document.getElementById(`tpSub-${i + 1}`).innerHTML);
      total += Number(data.replace(/\D/g, ""));
    }
    return total;
  }

  get getliabTpTotal() {
    let total = 0;
    for (let i = 0; i < 4; i++) {
      let data = String(
        document.getElementById(`liabtpSub-${i + 1}`).innerHTML
      );
      total += Number(data.replace(/\D/g, ""));
    }
    return total;
  }

  //  totalDiscount(){
  //   let total = this.calculatedQuote.riskDetails.antiTheftDiscount + this.calculatedQuote.riskDetails.automobileAssociationDiscount + this.calculatedQuote.riskDetails.handicappedDiscount + this.calculatedQuote.riskDetails.bonusDiscount + this.calculatedQuote.riskDetails.voluntaryDiscount + this.calculatedQuote.riskDetails.tppD_Discount;
  //  }

  //  get getdiscountTotal(){
  //   let total = 0;
  //   for( let i = 0; i < 5; i++) {
  //     let data = String(document.getElementById(`disc-${i+1}`).innerHTML);
  //     total += (Number(data.replace(/\D/g, "")));
  //   }
  //   // this.premiumWithoutDiscount = this.calculatedQuote.packagePremium + total;
  //   return total;
  //  }
}
