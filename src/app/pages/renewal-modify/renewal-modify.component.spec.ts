import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenewalModifyComponent } from './renewal-modify.component';

describe('RenewalModifyComponent', () => {
  let component: RenewalModifyComponent;
  let fixture: ComponentFixture<RenewalModifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenewalModifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenewalModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
