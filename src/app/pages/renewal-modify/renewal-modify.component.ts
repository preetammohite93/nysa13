import { Component, OnInit } from "@angular/core";
import { CommonService } from "src/app/services/common.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ApiServiceService } from "src/app/services/api-service.service";
import swal from "sweetalert";
import { commonData } from "src/app/commonData/commonData";
import * as moment from "moment";
import { CommonMessageService } from "src/app/services/common-message.service";


@Component({
  selector: "app-renewal-modify",
  templateUrl: "./renewal-modify.component.html",
  styleUrls: ["./renewal-modify.component.css"],
})
export class RenewalModifyComponent implements OnInit {
  // data:any;
  SAODflag: any;
  isProposalModify: boolean = false;
  biFuelODTP: any;
  errormsg: any;
  finalResp: any;
  failedModify: boolean = true;
  comparePremiumResp: any;
  comparePremiumStatus: any;
  comparePremiumPropNo: any;
  modifyPropData: any;
  modifyData: any;
  pinCode: any;
  pincodeData: any;
  isGSTDetails: boolean = false;
  GSTDetails: any;
  showSendLink: boolean = false;
  moberrorFlag: boolean = false;
  mobNumberPattern = "^(0/91)?[6-9][0-9]{9}$";
  productCode: any;
  stateName: any;
  city: any;
  loanLeaseHypo: boolean = false;
  addNomineeVehicle: boolean = false;
  isValidPincode = false;
  // nomineeDetails:boolean = false;
  totalDiscount: any;
  premiumWithDiscount: any;
  sendDetails = {
    Email: "",
    mobileno: "",
  };
  nomineeDetails = {
    NameOfNominee: "",
    Relationship: "",
    Age: "",
  };
  // breakIn
  breakinDays: any;
  breakinID: any;
  errorMessage: any;
  PreBreakINData: any;
  breakintype = {
    breakin: "selfinspection",
    breakincustom: "selfinspection1",
    isPreApprovedBreakIN: false,
    breakINID: "",
  };
  breakinDetails = {
    inspectionMode: "",
    subLocation: "",
  };
  IsSelfInspection: boolean = true;
  // addons
  isZDCount = 1;
  isRSACount = 1;
  isCSCount = 1;
  isKPCount = 1;
  isTPPDCount = 1;
  isRTICount = 1;
  isTPCount = 1;
  isEPCount = 1;
  isPACWCount = 1;
  isLOPBCount = 1;
  isVDCount = 1;
  isLLPDCount = 1;
  isLLPECount = 1;
  isCGLGCount = 1;
  isNCBCount = 1;
  isGCCount = 1;
  isPACUPCount = 1;
  isEACount = 1;
  isNEACount = 1;
  isATDCount = 1;
  isECCount = 1;
  isEMIPCount = 1;
  showBreakInPopUP = false;

  IsConsumables = false;
  engineProtect = false
  isZD = false;
  isRSA = false;
  ZeroDepPlanName: any;
  rsaPlanName: any;
  GarageCashPlanName: any;
  VoluntaryDeductiblePlanName: any;
  rsaPlans: any;
  KeyProtectPlan: any;
  isKeyProtectPlan: boolean = false;
  isTppdDiscount = false;
  IsRTIApplicableflag = false;
  IsTyreProtect = false;
  IsVoluntaryDeductible = false;
  IsLegalLiabilityToPaidDriver = false;
  IsLegalLiabilityToPaidEmployee = false;
  IsVehicleHaveLPG = false;
  IsVehicleHaveCNG = false;
  IsNCBProtect = false;
  IsGarageCash = false;
  LossOfPersonalBelongingPlanName = false;
  IsPACoverUnnamedPassenger = false;
  IsPACoverWaiver = false;
  IsPACoverOwnerDriver = false;
  IsHaveElectricalAccessories = false;
  IsHaveNonElectricalAccessories = false;
  isAntiTheftDisc = false;
  geographicalExtension: any;
  IsExtensionCountry = false;
  isEMIProtect = false;
  isNomineeChange = false;
  nomineeData: any;
  isFinaceChange = false;
  isCustomerChange = false;
  financeData: any;
  customerData: any;
  isGSTINChange = false;
  gstinData: any;
  isAddOnChange = false;
  addOnData: any;
  spinloader: boolean = false;
  spinpayloader: boolean = false;
  disabled1: any;
  disabled: any;
  cNGLPGTOggle: boolean = false


  // Deviation Changes
  iasIntRes: any;
  ineractionSpinner = false;
  isIDVIntRaised = false;
  isODIntRaised = false;
  fetchProposalData: any;
  selectedSRT: any;
  srt = [];
  iasProcess = [];
  isIASRaised = false;
  // quoteReq: any;
  dealDetailsfromIM: any;
  iasODReq: any;
  iasODRes: any;
  iasIDVReq: any;
  iasIDVRes: any;
  upperLimit: any;
  lowerLimit: any;
  exshowroomprice: any;
  value: any;
  idv: any;
  paramDataValues: any;
  quoteDeviation: false;
  otherDiscount: '0';
  isRecalculate: boolean = false;
  modifyDataReq: any;
  vehicleType: any;
  CityCode: any;
  StateCode: any;
  CountryCode: any;
  selectedRSAPlan = "";
  selectedZdPlan = "";
  selectedGarageCashPlan = "";
  selectedKeyProtect = "";
  selectedLossOfPersonalBelongingPlanName = "";
  selectedVoluntaryDeductiblePlanName = 0;
  // selectedCNGLPG: any;
  selectedIsVehicleHaveCNGLPG = "";
  selectedSIVehicleHaveLPG_CNG = "1000";
  NCBProtectPlanName = "";
  SIPACoverUnnamedPassenger = 0;
  PACovervalue: any;
  // PACovervalue1:any;
  NoOfDriver = 0;
  NoOfEmployee = 0;
  emiAmount: any;
  noOfEmi: '1';
  timeExcessInDays: '7';
  //breakin
  breakinFlag: boolean = false;
  subLocation: any;
  sublocations: any;
  policySubType: any;
  saveRNProposalStatus: any;
  breakinStatus: any;
  PreBreakINDataresponse: any;
  //Nominee & gst & financier details
  nomineeName: any;
  nomineeAge: any;
  nomineeRelationship: any;
  // nomineeNameFlag = false;
  // nomineeAgeFlag = false;
  // nomineeRelationshipFlag = false;
  gstinNumber: any;
  // gstinNumberFlag = false;
  agreementType: any;
  financierName: any;
  branchName: any;
  // agreementTypeFlag = false;
  // financierNameFlag = false;
  // branchNameFlage = false;
  LossOfPersonalBelongingPlan: any;
  isFromMyBreakIN = false;
  isFromMyIAS = false;
  isIDVChangeMSG = false;
  isNomineeAgeValidate = true;
  isGSTINValid = true;
  constructor(
    private cs: CommonService,
    private router: Router,
    public api: ApiServiceService,
    public msgService: CommonMessageService,
    public activeRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    // changes for idv by monika
    localStorage.setItem('isRenewalProposalModify', 'false');
    let breakInFlag = JSON.parse(localStorage.getItem("breakinFlag"));
    let iasFlag = JSON.parse(localStorage.getItem("isIASFlag"));
    if (this.cs.isUndefineORNull(breakInFlag)) {
      this.isFromMyBreakIN = false;
    } else {
      this.isFromMyBreakIN = true;
    }
    if (this.cs.isUndefineORNull(iasFlag)) {
      this.isFromMyIAS = false;
    } else {
      this.isFromMyIAS = true;
    }

    console.log(breakInFlag, iasFlag);
    if (breakInFlag) {
      let renewReq = JSON.parse(localStorage.getItem('renewBreakInData'));
      let propRq = JSON.parse(renewReq.proposalRQ);
      console.log(propRq);
      localStorage.setItem('renewalData', JSON.parse(renewReq.proposalRQ));
      this.readProposalForModifyPdf().then(() => {
        this.cs.createIASToken().then(() => {
          this.getZeroDepData();
          // this.getNoClaimBonus(this.vehicleType);
          this.getRSAData();
          this.getgarageCashData();
          this.getKeyProtectData();
          this.getlossofPerData();
          this.getVoluntaryDeductableVal(this.vehicleType);
        })
      })
    } else if (iasFlag) {
      let renewReq = JSON.parse(localStorage.getItem('renewBreakInData'));
      let propRq = JSON.parse(renewReq.bizRequest);
      console.log(propRq);
      localStorage.setItem('renewalData', JSON.parse(renewReq.bizRequest));
      this.readProposalForModifyPdf().then(() => {
        this.cs.createIASToken().then(() => {
          this.getZeroDepData();
          // this.getNoClaimBonus(this.vehicleType);
          this.getRSAData();
          this.getgarageCashData();
          this.getKeyProtectData();
          this.getlossofPerData();
          this.getVoluntaryDeductableVal(this.vehicleType);
        })
      })
    } else {
      this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
      this.readProposalForModifyPdf().then(() => {
        this.cs.createIASToken().then(() => {
          this.getZeroDepData();
          // this.getNoClaimBonus(this.vehicleType);
          this.getRSAData();
          this.getgarageCashData();
          this.getKeyProtectData();
          this.getlossofPerData();
          this.getVoluntaryDeductableVal(this.vehicleType);
        })
        // this.productCode = JSON.parse(localStorage.getItem('ProductCode'));
      })
    }
    // changes for idv by monika
    let readpropForIdv = JSON.parse(localStorage.getItem("payRenewalDataRes"));
    if (!this.cs.isUndefineORNull(readpropForIdv.IDVDetails)) {
      this.exshowroomprice = readpropForIdv.VehicleDetails.ShowRoomPrice;

      // let depreciationRate = readpropForIdv.IDVDetails.idvdepreciationpercent;
      // let requiredIDV = readpropForIdv.VehicleDetails.DepriciatedIDV;
      // let newDepreciationRate = 1 - depreciationRate;
      // let newExshowRoom = Math.round(parseInt(requiredIDV) / newDepreciationRate);
      // console.log('exshowroomprice', newExshowRoom);
      // this.exshowroomprice = newExshowRoom;

      this.lowerLimit = readpropForIdv.IDVDetails.minidv;
      this.upperLimit = readpropForIdv.IDVDetails.maxidv;
      // this.value = readpropForIdv.IDVDetails.minimumprice;
      // let idv = this.exshowroomprice - this.exshowroomprice * readpropForIdv.IDVDetails.idvdepreciationpercent;
      // console.log("IDV", idv, Math.floor(idv));
      // this.idv = Math.floor(idv);
      this.idv = readpropForIdv.VehicleDetails.DepriciatedIDV;
    }


  }

  goToDashborad() {
    this.router.navigateByUrl("renewals");
    let dashBoardData = JSON.parse(localStorage.getItem("dashboardData"));
    localStorage.removeItem('payRenewalData');
    console.log(dashBoardData);
    this.cs.isBack = true;

    // localStorage.removeItem("dashboardData");
    // this.cs.fromModify = true;

  }

  //Tokan for ZD
  createAddonBizToken(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.get("middleware/token?Scope=5").subscribe((res: any) => {
        localStorage.setItem("addOnbizToken", JSON.stringify(res));
        resolve();
      });
    }).catch((err: any) => {
      swal({ text: err });
      this.cs.loaderStatus = false;
    });
  }
  //Tokan For RSA
  createRSAAddonBizToken(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.get("middleware/token?Scope=4").subscribe((res: any) => {
        localStorage.setItem("addOnbizToken", JSON.stringify(res));
        resolve();
      });
    }).catch((err: any) => {
      swal({ text: err });
      this.cs.loaderStatus = false;
    });
  }
  //Tokan for Garage Cash
  createGarageCashAddonBizToken(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.get("middleware/token?Scope=3").subscribe((res: any) => {
        localStorage.setItem("addOnbizToken", JSON.stringify(res));
        resolve();
      });
    }).catch((err: any) => {
      swal({ text: err });
      this.cs.loaderStatus = false;
    });
  }

  // Number only valid
  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  //Mobile validation
  mobileValidate() {
    let isNumberValidate = this.cs.mobileValidation(this.sendDetails.mobileno);
    if (isNumberValidate && this.sendDetails.mobileno.length == 10) {
      this.sendDetails.mobileno = this.sendDetails.mobileno;
      this.moberrorFlag = false;
    } else {
      this.sendDetails.mobileno = "";
      this.moberrorFlag = true;
    }
  }
  //sendpdf link
  sendPropPdf() {
    // let data = JSON.parse(localStorage.getItem("pdfRenewalData"));
    // let vehType = data.VEH_TYPE.replace(/\s/g, "");

    let body = {
      propNo: this.modifyPropData.proposalNumber,
      product:
        this.productCode == "2311" || this.productCode == "2312" ? "PP" : "TP",
      vehType: (this.productCode == "2312" || this.productCode == "2320") ? 'TWOWHEELER' : 'PVTCAR',
      policyNo: this.modifyPropData.PolicyNumber,
      phoneNo: this.sendDetails.mobileno, //this.modifyPropData.CustomerDetails.MobileNumber,
      emailId: this.sendDetails.Email, //this.modifyPropData.CustomerDetails.Email,
      premium: this.modifyPropData.finalPremium,
      regNo: this.modifyPropData.RegistrationNumber,
    };
    let strBody = JSON.stringify(body);
    this.cs.postForRenewals("sendProposalPDFV2", strBody).then((res: any) => {
      if (res.emailStatus) {
        swal({ text: "Customer link has been sent successfully." });
      } else {
        swal({ text: "Failed to send customer link." });
      }
    });
  }


  validate() {
    this.checkCustomerData().then(() => {
      this.checkNomineeData().then(() => {
        this.checkFinanceData().then(() => {
          this.checkGSTINData().then(() => {
            // this.checkAddOnData().then(() => {
            this.modifyProposal();
            // })
          })
        })
      })
    })
  }

  checkGSTINValid(){
    let regTest = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/.test(this.gstinNumber);
    console.log(regTest);
    this.isGSTINValid = regTest;
    if(!this.isGSTINValid){
      this.gstinNumber = "";
    }else{
      this.gstinNumber = this.gstinNumber;
    }
  }

  checkNomineeData(): Promise<any> {
    return new Promise((resolve: any) => {
      let oldBody = JSON.parse(localStorage.getItem('payRenewalData'));
      console.log('OLDBODY', oldBody);
      let obj = {
        NameOfNominee: this.nomineeName,
        Age: this.nomineeAge,
        Relationship: this.nomineeRelationship
      }
      let newBody = obj;
      if (!this.cs.isUndefineORNull(oldBody.NomineeDetails)) {
        let oldNomData = JSON.stringify(oldBody.NomineeDetails);
        let newNomData = JSON.stringify(newBody);
        if (JSON.stringify(this.api.sortObjectByKey(newBody)) === JSON.stringify(this.api.sortObjectByKey(oldBody.NomineeDetails))) {
          // if (oldNomData === newNomData) {
          this.isNomineeChange = false;
          this.nomineeData = oldNomData;
          resolve();
        } else {
          this.isNomineeChange = true;
          this.nomineeData = newNomData;
          resolve();
        }
      } else {
        if (!this.cs.isUndefineORNull(newBody) && newBody.NameOfNominee != '' && newBody.Age != '' && newBody.Relationship != '') {
          this.isNomineeChange = true;
          this.nomineeData = JSON.stringify(newBody);
          resolve();
        } else {
          this.isNomineeChange = false;
          this.nomineeData = null;
          resolve();
        }
      }

    });
  }

  checkFinanceData(): Promise<any> {
    return new Promise((resolve: any) => {
      let oldBody = JSON.parse(localStorage.getItem('payRenewalData'));
      let obj = {
        AgreementType: this.agreementType,
        BranchName: this.branchName,
        FinancierName: this.financierName
      }
      let newBody = obj;
      if (!this.cs.isUndefineORNull(oldBody.FinancierDetails)) {
        let oldFinData = JSON.stringify(oldBody.FinancierDetails);
        let newFinData = JSON.stringify(newBody);
        if (JSON.stringify(this.api.sortObjectByKey(newBody)) === JSON.stringify(this.api.sortObjectByKey(oldBody.FinancierDetails))) {
          // if (oldFinData === newFinData) {
          this.isFinaceChange = false;
          this.financeData = oldFinData;
          resolve();
        } else {
          this.isFinaceChange = true;
          this.financeData = newFinData;
          resolve();
        }
      } else {
        if (!this.cs.isUndefineORNull(newBody) && newBody.AgreementType != '' && newBody.BranchName != '' && newBody.FinancierName != '') {
          this.isFinaceChange = true;
          this.financeData = JSON.stringify(newBody);
          resolve();
        } else {
          this.isFinaceChange = false;
          this.financeData = null;
          resolve();
        }
      }
    });
  }

  checkCustomerData(): Promise<any> {
    return new Promise((resolve: any) => {
      let oldBody = JSON.parse(localStorage.getItem('payRenewalData'));
      let obj = {
        // CustomerType: this.modifyPropData.CustomerDetails.CustomerType,
        // CustomerName: this.modifyPropData.CustomerDetails.CustomerName,
        // PinCode: this.pinCode,
        // Email: this.sendDetails.Email,
        // MobileNumber: this.sendDetails.mobileno,
        // AddressLine1: this.modifyPropData.CustomerDetails.AddressLine1,
        // CityCode: this.CityCode,
        // StateCode: this.StateCode,
        // CountryCode: this.CountryCode
        CustomerType: "Individual",
        CustomerName: this.modifyPropData.CustomerDetails.CustomerName,
        PinCode: this.pinCode,//this.modifyPropData.CustomerDetails.PinCode,
        Email: this.sendDetails.Email,//this.modifyPropData.CustomerDetails.Email,
        MobileNumber: this.sendDetails.mobileno,//this.modifyPropData.CustomerDetails.MobileNumber,
        AddressLine1: this.modifyPropData.CustomerDetails.AddressLine1,
        CityCode: this.CityCode,
        StateCode: this.StateCode,
        CountryCode: this.CountryCode
      }
      let newBody = obj;
      

      if (!this.cs.isUndefineORNull(oldBody.CustomerDetails)) {
        console.log(JSON.stringify(oldBody.CustomerDetails), JSON.stringify(newBody))
        let oldFinData = JSON.stringify(oldBody.CustomerDetails);
        let newFinData = JSON.stringify(newBody);
        if (JSON.stringify(this.api.sortObjectByKey(newBody)) === JSON.stringify(this.api.sortObjectByKey(oldBody.CustomerDetails))) {
          this.isCustomerChange = false;
          this.customerData = oldFinData;
          resolve();
        } else {
          this.isCustomerChange = true;
          this.customerData = newFinData;
          resolve();
        }
      } else {
        if (!this.cs.isUndefineORNull(newBody) && newBody.CustomerType != '' && newBody.CustomerName != '' && newBody.PinCode != '' && newBody.Email != '' && newBody.MobileNumber != '' && newBody.AddressLine1 != '' && newBody.CityCode != '' && newBody.StateCode != '' && newBody.CountryCode != '') {
          this.isCustomerChange = true;
          this.customerData = JSON.stringify(newBody);
          resolve();
        } else {
          this.isCustomerChange = false;
          this.customerData = null;
          resolve();
        }
      }
    });
  }

  checkGSTINData(): Promise<any> {
    return new Promise((resolve: any) => {
      let gstinNumber:any;
      let oldBody = JSON.parse(localStorage.getItem('payRenewalData'));
      let obj: any;
      if(!this.cs.isUndefineORNull(this.gstinNumber)){
        obj = {
          GstInNumber: this.gstinNumber,
          GSTToState: this.pincodeData.stateName
        }
      }
     
      let newBody = obj;
      console.log(this.isGSTDetails, oldBody);
      if(this.isGSTDetails){
        if(this.cs.isUndefineORNull(oldBody.GstDetails) || this.cs.isUndefineORNull(oldBody.CustomerDetails.GstDetails)){
          gstinNumber = '';
        }else{
          gstinNumber = oldBody.GstDetails ? oldBody.GstDetails.GstInNumber : oldBody.CustomerDetails.GstDetails.GstInNumber;
        }        
      }else{
        gstinNumber = '';
      }
      
      if (!this.cs.isUndefineORNull(gstinNumber)) {
        let gstDetails = oldBody.GstDetails ? oldBody.GstDetails : oldBody.CustomerDetails.GstDetails;
        let oldGSTData = JSON.stringify(gstDetails);
        let newGSTData = JSON.stringify(newBody);
        if (oldGSTData === newGSTData) {
          this.isGSTINChange = false;
          this.gstinData = oldGSTData;
          resolve();
        } else {
          this.isGSTINChange = true;
          this.gstinData = newGSTData;
          resolve();
        }
      } else {
        if (!this.cs.isUndefineORNull(newBody)) {
          this.isGSTINChange = true;
          this.gstinData = JSON.stringify(newBody);
          resolve();
        } else {
          this.isGSTINChange = false;
          this.gstinData = null;
          resolve();
        }
      }
    });
  }

  // checkAddOnData(): Promise<any> {
  //   return new Promise((resolve: any) => {
  //     let oldBody = JSON.parse(localStorage.getItem('payRenewalData'));
  //     let newBody = {
  //       antiTheftDiscount: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.antiTheftDiscount) ? 0 : this.modifyPropData.riskDetails.antiTheftDiscount,
  //       automobileAssociationDiscount: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.automobileAssociationDiscount) ? 0 : this.modifyPropData.riskDetails.automobileAssociationDiscount,
  //       basicOD: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.basicOD) ? 0 : this.modifyPropData.riskDetails.basicOD,
  //       basicTP: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.basicTP) ? 0 : this.modifyPropData.riskDetails.basicTP,
  //       biFuelKitOD: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.biFuelKitOD) ? 0 : this.modifyPropData.riskDetails.biFuelKitOD,
  //       biFuelKitTP: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.biFuelKitTP) ? 0 : this.modifyPropData.riskDetails.biFuelKitTP,
  //       bonusDiscount: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.bonusDiscount) ? 0 : this.modifyPropData.riskDetails.bonusDiscount,
  //       breakinLoadingAmount: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.breakinLoadingAmount) ? 0 : this.modifyPropData.riskDetails.breakinLoadingAmount,
  //       consumables: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.consumables) ? 0 : this.modifyPropData.riskDetails.consumables,
  //       electricalAccessories: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.electricalAccessories) ? 0 : this.modifyPropData.riskDetails.electricalAccessories,
  //       emeCover: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.emeCover) ? 0 : this.modifyPropData.riskDetails.emeCover,
  //       emiProtect: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.emiProtect) ? 0 : this.modifyPropData.riskDetails.emiProtect,
  //       employeesOfInsured: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.employeesOfInsured) ? 0 : this.modifyPropData.riskDetails.employeesOfInsured,
  //       engineProtect: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.engineProtect) ? 0 : this.modifyPropData.riskDetails.engineProtect,
  //       fibreGlassFuelTank: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.fibreGlassFuelTank) ? 0 : this.modifyPropData.riskDetails.fibreGlassFuelTank,
  //       garageCash: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.garageCash) ? 0 : this.modifyPropData.riskDetails.garageCash,
  //       geographicalExtensionOD: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.geographicalExtensionOD) ? 0 : this.modifyPropData.riskDetails.geographicalExtensionOD,
  //       geographicalExtensionTP: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.geographicalExtensionTP) ? 0 : this.modifyPropData.riskDetails.geographicalExtensionTP,
  //       handicappedDiscount: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.handicappedDiscount) ? 0 : this.modifyPropData.riskDetails.handicappedDiscount,
  //       keyProtect: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.keyProtect) ? 0 : this.modifyPropData.riskDetails.keyProtect,
  //       lossOfPersonalBelongings: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.lossOfPersonalBelongings) ? 0 : this.modifyPropData.riskDetails.lossOfPersonalBelongings,
  //       ncbPercentage: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.ncbPercentage) ? 0 : this.modifyPropData.riskDetails.ncbPercentage,
  //       nonElectricalAccessories: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.nonElectricalAccessories) ? 0 : this.modifyPropData.riskDetails.nonElectricalAccessories,
  //       paCoverForOwnerDriver: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.paCoverForOwnerDriver) ? 0 : this.modifyPropData.riskDetails.paCoverForOwnerDriver,
  //       paCoverForUnNamedPassenger: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.paCoverForUnNamedPassenger) ? 0 : this.modifyPropData.riskDetails.paCoverForUnNamedPassenger,
  //       paCoverWaiver: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.paCoverWaiver) ? false : this.modifyPropData.riskDetails.paCoverWaiver,
  //       paidDriver: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.paidDriver) ? 0 : this.modifyPropData.riskDetails.paidDriver,
  //       returnToInvoice: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.returnToInvoice) ? 0 : this.modifyPropData.riskDetails.returnToInvoice,
  //       roadSideAssistance: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.roadSideAssistance) ? 0 : this.modifyPropData.riskDetails.roadSideAssistance,
  //       tppD_Discount: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.tppD_Discount) ? 0 : this.modifyPropData.riskDetails.tppD_Discount,
  //       tyreProtect: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.tyreProtect) ? 0 : this.modifyPropData.riskDetails.tyreProtect,
  //       voluntaryDiscount: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.voluntaryDiscount) ? 0 : this.modifyPropData.riskDetails.voluntaryDiscount,
  //       zeroDepreciation: this.cs.isUndefineORNull(this.modifyPropData.riskDetails.zeroDepreciation) ? 0 : this.modifyPropData.riskDetails.zeroDepreciation
  //     }

  //     if (!this.cs.isUndefineORNull(oldBody.riskDetails)) {
  //       let oldAddOnData = JSON.stringify(oldBody.riskDetails);
  //       let newAddOnData = JSON.stringify(newBody);
  //       if (oldAddOnData === newAddOnData) {
  //         this.isAddOnChange = false;
  //         this.addOnData = oldAddOnData;
  //         resolve();
  //       } else {
  //         this.isAddOnChange = true;
  //         this.addOnData = newAddOnData;
  //         resolve();
  //       }
  //     } else {
  //       this.isAddOnChange = false;
  //       this.addOnData = null;
  //       resolve();
  //     }
  //   });
  // }


  modifyProposal() {
    let otherDiscount: any
    if ((this.productCode == '2312' || this.productCode == '2320') && this.otherDiscount != '0') {
      let OD = this.otherDiscount;
      console.log(OD, typeof (OD));
      let OD1 = Number(OD) / 100;
      this.isRecalculate = true;
      otherDiscount = OD1;
    } else {
      this.isRecalculate = true;
      otherDiscount = this.otherDiscount;
    }
    console.log(this.modifyPropData);
    localStorage.setItem("renewalData", JSON.stringify(this.modifyPropData));
    this.spinloader = true;
    let body: any;
    body = {
      CorrelationID: this.modifyPropData.CorrelationId,
      policyNo: this.modifyPropData.PolicyNumber,
      IsCustomerModified: this.isCustomerChange,
      CustomerDetails: {
        CustomerType: "Individual",
        CustomerName: this.modifyPropData.CustomerDetails.CustomerName,
        PinCode: this.pinCode,//this.modifyPropData.CustomerDetails.PinCode,
        Email: this.sendDetails.Email,//this.modifyPropData.CustomerDetails.Email,
        MobileNumber: this.sendDetails.mobileno,//this.modifyPropData.CustomerDetails.MobileNumber,
        AddressLine1: this.modifyPropData.CustomerDetails.AddressLine1,
        CityCode: this.CityCode,
        StateCode: this.StateCode,
        CountryCode: this.CountryCode,
      },
      IsNomineeModified: this.isNomineeChange,
      NomineeDetails: JSON.parse(this.nomineeData),
      IsGSTModified: (this.isGSTINChange && this.isGSTDetails) ? true : false,
      GstDetails: (this.isGSTINChange && this.isGSTDetails) ? JSON.parse(this.gstinData) : null, //this.gstinData,
      IsFinancierModified: this.isFinaceChange,
      FinancierDetails: JSON.parse(this.financeData),
      IsAddonsModified: true,//this.isAddOnChange,
      Addons: {
        ZeroDepPlanName: this.isZD ? this.selectedZdPlan : "",
        // roadSideAssistance: this.isRSA ? this.selectedRSAPlan : "",
        RSAPlanName: this.isRSA ? this.selectedRSAPlan : "",//this.selectedRSAPlan,
        IsEngineProtectPlus: this.engineProtect,
        IsConsumables: this.IsConsumables,
        TPPDDiscount: this.isTppdDiscount,
        IsRTIApplicableflag: this.IsRTIApplicableflag,
        IsTyreProtect: this.IsTyreProtect,
        IsVoluntaryDeductible: this.IsVoluntaryDeductible,
        VoluntaryDeductiblePlanName: this.selectedVoluntaryDeductiblePlanName,//this.IsVoluntaryDeductible ? '3500' : undefined,        
        IsLegalLiabilityToPaidDriver: this.IsLegalLiabilityToPaidDriver,
        NoOfDriver: this.NoOfDriver,
        IsLegalLiabilityToPaidEmployee: this.IsLegalLiabilityToPaidEmployee,
        NoOfEmployee: this.NoOfEmployee,
        IsVehicleHaveLPG: (this.selectedIsVehicleHaveCNGLPG == 'LPG') ? true : false,
        IsVehicleHaveCNG: (this.selectedIsVehicleHaveCNGLPG == 'CNG') ? true : false,
        // IsVehicleHaveCNGLPG: (this.IsVehicleHaveLPG || this.IsVehicleHaveCNG) ? this.selectedIsVehicleHaveCNGLPG : "",
        SIVehicleHaveLPG_CNG: this.selectedSIVehicleHaveLPG_CNG,
        //siVehicleHaveLPG_CNG: this.selectedCNGLPG,//(this.IsVehicleHaveLPG || this.IsVehicleHaveCNG) ? '1000' : '0',
        IsNCBProtect: this.IsNCBProtect,
        NCBProtectPlanName: this.IsNCBProtect ? this.NCBProtectPlanName : "",
        IsGarageCash: this.IsGarageCash,
        GarageCashPlanName: this.selectedGarageCashPlan,//this.IsGarageCash ? 'MBCON' : undefined,
        IsPACoverUnnamedPassenger: this.IsPACoverUnnamedPassenger,
        SIPACoverUnnamedPassenger: this.PACovervalue,
        IsPACoverWaiver: this.IsPACoverWaiver,
        KeyProtectPlan: this.isKeyProtectPlan ? this.selectedKeyProtect : "",
        IsAntiTheftDisc: this.isAntiTheftDisc,
        LossOfPersonalBelongingPlanName: this.LossOfPersonalBelongingPlanName ? this.selectedLossOfPersonalBelongingPlanName : "",
        IsExtensionCountry: this.IsExtensionCountry,
        ExtensionCountryName: this.IsExtensionCountry ? "Bhutan, Bangladesh, Nepal, Pakistan, Maldives, SriLanka" : undefined,
        IsEMIProtect: this.isEMIProtect,
        EMIAmount: this.isEMIProtect ? this.emiAmount : undefined,
        NoOfEMI: this.isEMIProtect ? parseInt(this.noOfEmi) : undefined,
        TimeExcessindays: this.isEMIProtect ? parseInt(this.timeExcessInDays) : undefined,
        OtherDiscount: this.otherDiscount ? this.otherDiscount : "",
        //breakin request
        IsSelfInspection: this.IsSelfInspection,
        ExShowRoomPrice: parseInt(this.exshowroomprice),
      },
    };
    this.modifyDataReq = body;
    localStorage.setItem("payRenewalData", JSON.stringify(body));
    let str = JSON.stringify(body);
    this.cs.postForRenewals("modifyProposal", str).then((res: any) => {
      this.spinloader = false;
      console.log(res);
      this.isProposalModify = true;
      console.log('Modify', this.finalResp);
      localStorage.setItem('isRenewalProposalModify', 'true');
      localStorage.setItem('Riskdetails', JSON.stringify(res.riskDetails));
      if (res.status == "Success") {
        this.finalResp = this.modifyData = res;
        this.failedModify = false;
        this.spinloader = false;
        this.breakinFlag = res.breakingFlag;
        this.showBreakInPopUP = res.breakingFlag;
        this.quoteDeviation = res.isQuoteDeviation;
        let antiTheftDiscount = this.modifyData.riskDetails.antiTheftDiscount ? this.modifyData.riskDetails.antiTheftDiscount : 0;
        let bonusDiscount = this.modifyData.riskDetails.bonusDiscount ? this.modifyData.riskDetails.bonusDiscount : 0;
        let voluntaryDiscount = this.modifyData.riskDetails.voluntaryDiscount ? this.modifyData.riskDetails.voluntaryDiscount : 0;
        let tppD_Discount = this.modifyData.riskDetails.tppD_Discount ? this.modifyData.riskDetails.tppD_Discount : 0;
        this.totalDiscount = antiTheftDiscount + bonusDiscount + voluntaryDiscount + tppD_Discount;
        this.premiumWithDiscount = !this.isProposalModify ? this.modifyPropData.packagePremium + this.totalDiscount : this.modifyData.packagePremium + this.totalDiscount;

        if (res.isQuoteDeviation == true) {
          this.getProposalDetails(res.generalInformation.proposalNumber);
          this.getDealDetails()
        } else {
          // console.log("Deviation is not working")
        }
        this.spinloader = false;
      } else {
        console.log(this.modifyData);
        this.spinloader = false;
        console.log('modifyerrormsg', res.Error);
        if (res.Error) {
          swal({ text: "System failure kindly check after some time" })
        } else {
          if (res.status == "Failed" && res.message.includes("Renewed Proposal No")) {
            let splitMsg = res.message.split(":");
            let propNo = splitMsg[2];
            let policyMsg = "Your policy has been Renewed. Please find renewed Proposal no: "
            swal({
              text: policyMsg + "" + propNo,
            })
          } else {
            swal({ text: res.message });
          }
        }
        this.errormsg = res.message;
        this.failedModify = true;
        // Common MSG
        // this.api.handleerrors(errormsg);
        if (res.status == "Failed" && res.message.includes("Renewed Proposal No")) {
          let splitMsg = res.message.split(":");
          let propNo = splitMsg[2];
          let policyMsg = "Your policy has been Renewed. Please find renewed Proposal no: "
          swal({
            text: policyMsg + "" + propNo,
          })
        } else {
          let errMsg = this.msgService.errorMsg(this.errormsg)
          swal({ closeOnClickOutside: false, text: errMsg });
        }

        let errorbody = {
          RequestJson: JSON.stringify(body),
          ResponseJson: JSON.stringify(res),
          ServiceURL: commonData.renewalURL + "modifyProposal",
          CorrelationID: res.correlationId,
        };
        this.api.adderrorlogs(errorbody);
      }
    })
  }

  checkOD() {
    if (!this.cs.isUndefineORNull(this.otherDiscount)) {
      if (parseInt(this.otherDiscount) > 0) {
        this.isAddOnChange = true;
      } else {
        this.isAddOnChange = true;
      }
    } else {
      this.isAddOnChange = false;
    }
  }

  inputValidation() {
    if (this.cs.isUndefineORNull(this.modifyPropData.CustomerDetails.AddressLine1)) {
      swal({ text: "Kindly enter customer Address" });
    } else if (this.cs.isUndefineORNull(this.pinCode)) {
      swal({ text: "Kindly enter pincode" });
    } else if (this.cs.isUndefineORNull(this.sendDetails.mobileno)) {
      swal({ text: "Kindly enter mobile number" });
    } else if (this.cs.isUndefineORNull(this.sendDetails.Email)) {
      swal({ text: "Kindly enter email" });
    } else if (this.isGSTDetails == true && this.cs.isUndefineORNull(this.gstinNumber)) {
      // this.gstinNumberFlag = true;
      swal({ text: "Kindly enter GST number" });
    } else if (this.addNomineeVehicle == true && this.cs.isUndefineORNull(this.nomineeName)) {
      // this.nomineeNameFlag = true;
      swal({ text: "Kindly enter nominee name" });
    } else if (this.addNomineeVehicle == true && this.cs.isUndefineORNull(this.nomineeAge)) {
      // this.nomineeAgeFlag = true;
      swal({ text: "Kindly enter nominee age" });
    } else if (this.addNomineeVehicle == true && this.cs.isUndefineORNull(this.nomineeRelationship)) {
      // this.nomineeRelationshipFlag = true;
      swal({ text: "Kindly enter nominee relationship" });
    } else if (this.loanLeaseHypo == true && this.cs.isUndefineORNull(this.agreementType)) {
      // this.agreementTypeFlag = true;
      swal({ text: "Kindly enter agreement type" });
    } else if (this.loanLeaseHypo == true && this.cs.isUndefineORNull(this.financierName)) {
      // this.financierNameFlag = true;
      swal({ text: "Kindly enter financier name" });
    } else if (this.loanLeaseHypo == true && this.cs.isUndefineORNull(this.branchName)) {
      // this.branchNameFlag = true;
      swal({ text: "Kindly enter financier branch" });
    } else {
      this.validate();
    }
  }

  readProposalForModifyPdf(): Promise<any> {
    return new Promise((resolve: any) => {
      let data = JSON.parse(localStorage.getItem('renewalData'));
      let polNo = data.POLICY_NO ? data.POLICY_NO : data.policyNo;
      let body = {
        policyNo: polNo ? polNo : data.PolicyNumber,//'3001/52046382/00/B00',
      };

      let str = JSON.stringify(body);
      this.cs.postForRenewals("readProposalV2", str).then((res: any) => {
        console.log('Response2', res);
        this.isProposalModify = false;
        this.productCode = res.ProductCode;
        this.vehicleType = (this.productCode == '2311' || this.productCode == '2319') ? '4wheeler' : '2wheeler';
        localStorage.setItem('payRenewalDataRes', JSON.stringify(res));
        localStorage.setItem('payRenewalData', JSON.stringify(res));
        localStorage.setItem('Riskdetails', JSON.stringify(res.riskDetails));
        if (this.cs.isUndefineORNull(res.Error)) {
          this.finalResp = this.modifyPropData = res;
          if (!this.cs.isUndefineORNull(res.IDVDetails)) {
            this.exshowroomprice = res.VehicleDetails.ShowRoomPrice;

            // let depreciationRate = res.IDVDetails.idvdepreciationpercent;
            // let requiredIDV = res.VehicleDetails.DepriciatedIDV;
            // let newDepreciationRate = 1 - depreciationRate;
            // let newExshowRoom = Math.round(parseInt(requiredIDV) / newDepreciationRate);
            // console.log('exshowroomprice', newExshowRoom);
            // this.exshowroomprice = newExshowRoom;

            this.lowerLimit = res.IDVDetails.minidv;
            this.upperLimit = res.IDVDetails.maxidv;
            // this.value = res.IDVDetails.minimumprice;
            // let idv = this.exshowroomprice - this.exshowroomprice * res.IDVDetails.idvdepreciationpercent;
            // console.log("IDV", idv, Math.floor(idv));
            // this.idv = Math.floor(idv);
          }
          this.idv = res.VehicleDetails.DepriciatedIDV;
          this.otherDiscount = res.OtherDiscount;
          setTimeout(() => {
            this.checkedCovers(this.modifyPropData);
          }, 100);
          let policyNumberOD = res.PolicyNumber.split('/');
          this.SAODflag = policyNumberOD.length;
          console.log('saod', this.SAODflag);
          console.log('TPP', this.finalResp.finalPremium);
          if (!this.cs.isUndefineORNull(this.modifyPropData.riskDetails.biFuelKitOD) || !this.cs.isUndefineORNull(this.modifyPropData.riskDetails.biFuelKitTP)) {
            this.biFuelODTP = this.modifyPropData.riskDetails.biFuelKitOD + this.modifyPropData.riskDetails.biFuelKitTP;
          } else {
            this.biFuelODTP = 0;
          }
          if (!this.cs.isUndefineORNull(this.modifyPropData.riskDetails.geographicalExtensionOD) || !this.cs.isUndefineORNull(this.modifyPropData.riskDetails.geographicalExtensionTP)) {
            this.geographicalExtension = this.modifyPropData.riskDetails.geographicalExtensionOD + this.modifyPropData.riskDetails.geographicalExtensionTP;
          } else {
            this.geographicalExtension = 0;
          }
          let antiTheftDiscount = this.modifyPropData.riskDetails.antiTheftDiscount ? this.modifyPropData.riskDetails.antiTheftDiscount : 0;
          let bonusDiscount = this.modifyPropData.riskDetails.bonusDiscount ? this.modifyPropData.riskDetails.bonusDiscount : 0;
          let voluntaryDiscount = this.modifyPropData.riskDetails.voluntaryDiscount ? this.modifyPropData.riskDetails.voluntaryDiscount : 0;
          let tppD_Discount = this.modifyPropData.riskDetails.tppD_Discount ? this.modifyPropData.riskDetails.tppD_Discount : 0;
          this.totalDiscount = antiTheftDiscount + bonusDiscount + voluntaryDiscount + tppD_Discount;
          this.premiumWithDiscount = !this.isProposalModify ? this.modifyPropData.packagePremium + this.totalDiscount : this.modifyData.packagePremium + this.totalDiscount;
          // console.log('addons',this.premiumWithDiscount);
          this.pinCode = this.modifyPropData.CustomerDetails.PinCode;
          // let gst = document.getElementById('gstdetl') as HTMLInputElement;
          // let nominee = document.getElementById('addNmDt') as HTMLInputElement;
          // let finance = document.getElementById('leaseDt') as HTMLInputElement;
          if (this.cs.isUndefineORNull(this.modifyPropData.NomineeDetails)) {
            this.nomineeName = "";
            this.nomineeAge = "";
            this.nomineeRelationship = "";
            // this.nomineeNameFlag = false;
            // nominee.checked = false;
          } else {
            this.nomineeName = this.modifyPropData.NomineeDetails.NameOfNominee;
            this.nomineeAge = this.modifyPropData.NomineeDetails.Age;
            this.nomineeRelationship = this.modifyPropData.NomineeDetails.Relationship.toUpperCase();
            let nominee = document.getElementById('addNmDt') as HTMLInputElement;
            // this.nomineeNameFlag = true;
            nominee.checked = true;
          }

          if (this.cs.isUndefineORNull(this.modifyPropData.CustomerDetails.GstDetails.GstInNumber)) {
            this.gstinNumber = "";
            this.isGSTDetails = false;
          } else {
            this.gstinNumber = this.modifyPropData.CustomerDetails.GstDetails.GstInNumber;
            this.isGSTDetails = true;
            let gst = document.getElementById('gstdetl') as HTMLInputElement;
            gst.checked = true;
          }

          if (this.cs.isUndefineORNull(this.modifyPropData.FinancierDetails)) {
            this.financierName = "";
            this.agreementType = "";
            this.branchName = "";
          } else {
            this.financierName = this.modifyPropData.FinancierDetails.FinancierName;
            this.agreementType = this.modifyPropData.FinancierDetails.AgreementType;
            this.branchName = this.modifyPropData.FinancierDetails.BranchName;
          }


          this.addNomineeVehicle = this.cs.isUndefineORNull(
            this.modifyPropData.NomineeDetails
          )
            ? false
            : true;
          this.loanLeaseHypo = this.cs.isUndefineORNull(
            this.modifyPropData.FinancierDetails
          )
            ? false
            : true;
          if (this.modifyPropData.CustomerDetails.PinCode.length == 6) {
            this.getPincodeData(this.modifyPropData.CustomerDetails.PinCode);
            this.isValidPincode = true;
          } else {
            this.isValidPincode = false;
          }
          this.sendDetails.mobileno =
            this.modifyPropData.CustomerDetails.MobileNumber;
          this.sendDetails.Email = this.modifyPropData.CustomerDetails.Email;


          resolve();
        } else {
          this.modifyPropData = {};
          resolve();
        }
      });
    });
  }

  comparePremium(): Promise<any> {
    return new Promise((resolve: any) => {
      this.spinloader = true;
      let inspectionID = JSON.parse(localStorage.getItem("breakinData"));
      let body = {
        policyNo: this.modifyPropData.PolicyNumber,
        finalPremium: this.modifyData ? this.modifyData.finalPremium : this.modifyPropData.finalPremium,
        inspectionId: this.isFromMyBreakIN ? JSON.stringify(inspectionID.inspectionID) : "",
      };
      let str = JSON.stringify(body);
      this.cs.postForRenewals("comparePremiumV2", str).then((res: any) => {
        this.spinloader = false;
        this.comparePremiumResp = res;
        this.comparePremiumPropNo = res.newProposal;
        if (res.status == "Failed" && res.message == "Cannot proceed to payment due to breakin or deviation scenario") {
          this.spinpayloader = false;
          swal({
            text: "Cannot proceed to payment due to breakin or deviation scenario."
          })
        }
        else if (res.status != 'Failed') {
          this.saveProposalForPayment().then(() => {
            if (this.saveRNProposalStatus == "Success") {
              this.PlutusPaymentPaymsToken();
              resolve();
            } else {
              swal({
                text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
              });
              resolve();
            }
          });
        } else {
          // this.saveProposalForPayment();
          resolve();
          // if(this.quoteDeviation){
          //   this.getProposalDetails(this.comparePremiumPropNo);
          //   resolve();
          // }else{
          // this.saveProposalForPayment();
          // resolve();
          // }

          // if (this.modifyData.breakingFlag && !this.modifyData.isQuoteDeviation) {
          //   if (this.cs.isUndefineORNull(this.breakinID) || this.breakinStatus == 'Fail') {
          //     swal({
          //       text:
          //         "Breakin Id could not be generated to this vehicle. Reason: " +
          //         this.errorMessage,
          //     }).then(() => {
          //     });
          //     resolve();
          //   } else {
          //     swal({
          //       text:
          //         "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases, if recommended proceed for payment. Your Proposal Number: " +
          //         this.modifyData.generalInformation.proposalNumber +
          //         " and your Breakin ID is: " +
          //         this.breakinID,
          //     }).then(() => {
          //     });
          //     resolve();
          //   }
          // } else if (!this.modifyData.breakingFlag && this.modifyData.isQuoteDeviation) {
          //   this.saveProposalForPayment();
          //   resolve();
          // } else {
          //   // if (this.fetchProposalData.length > 1 && this.cs.isUndefineORNull(this.breakinID)) {
          //   //   // For both deviation on same time
          //   //   if (this.fetchProposalData[0].srTvalue == 'NA ' || this.fetchProposalData[1].srTvalue == 'NA ' || this.fetchProposalData[0].subSRTvalue == 'NA ' || this.fetchProposalData[1].subSRTvalue == 'NA ') {
          //   //     if (this.fetchProposalData[0].srTvalue != 'NA ' || this.fetchProposalData[0].subSRTvalue != 'NA ') {
          //   //       this.saveProposalForPayment();
          //   //       resolve();
          //   //     } else if (this.fetchProposalData[1].srTvalue != 'NA ' || this.fetchProposalData[1].subSRTvalue != 'NA ') {
          //   //       this.saveProposalForPayment();
          //   //       resolve();
          //   //     } else {
          //   //       swal({
          //   //         text: 'Unable to raised interaction due to srt and subsrt value are not matched',
          //   //       })
          //   //       resolve();
          //   //     }
          //   //   } else {
          //   //     // Multiple deviation both having SRT and SubSRT
          //   //     this.saveProposalForPayment();
          //   //     resolve();
          //   //   }
          //   // } else {
          //   //   this.saveProposalForPayment();
          //   //   resolve();
          //   // }

          // }

        }
      });
    });

  }

  // comparePremium(): Promise<any> {
  //   return new Promise((resolve: any) => {
  //     let body = {
  //       policyNo: this.modifyPropData.PolicyNumber,
  //       finalPremium: this.modifyData ? this.modifyData.finalPremium : this.modifyPropData.finalPremium,
  //     };
  //     let str = JSON.stringify(body);
  //     this.cs.postForRenewals("comparePremiumV2", str).then((res: any) => {
  //       this.comparePremiumPropNo = res.newProposal;
  //       this.comparePremiumResp = res;
  //       this.comparePremiumStatus = res.status == "Failed";
  //       console.log('pn', this.comparePremiumPropNo);
  //       this.spinpayloader = false;
  //       if (res.status == "Failed") {
  //         if (res.message == "Deviation Triggered" && this.cs.isUndefineORNull(this.breakinID)) {
  //           this.saveProposalForPayment();
  //           resolve();
  //         } else if (res.message == "Premium mismatch" && this.cs.isUndefineORNull(this.breakinID)) {
  //           let compareMsg1 = "Your old premium amount of Rs."
  //           let compareMsg2 = " has been changed to Rs.";
  //           let compareMsg3 = ". Do you want to continue ?";
  //           let finalPremium = this.modifyPropData.finalPremium;
  //           swal({
  //             text: compareMsg1 + '' + finalPremium + '' + compareMsg2 + '' + res.newPremium + '' + compareMsg3,
  //             dangerMode: true,
  //             closeOnClickOutside: false,
  //             buttons: ["Yes", "No"],
  //           }).then((click) => {
  //             if (!click) {
  //               console.log("Continue");
  //               this.saveProposalForPayment().then(() => {
  //                 this.PlutusPaymentPaymsToken();
  //                 resolve();
  //               })
  //             } else {
  //               console.log("Go Back");
  //               resolve();
  //               // location.href = "redirect1.html"
  //             }
  //           });
  //           resolve();
  //         } else if (!this.cs.isUndefineORNull(res.blazeMessage)) {
  //           if (res.blazeMessage.includes("Renewed Proposal No")) {
  //             let splitMsg = res.blazeMessage.split(":");
  //             let propNo = splitMsg[2];
  //             let policyMsg = "Your policy has been Renewed. Please find renewed Proposal no: "
  //             swal({
  //               text: policyMsg + "" + propNo,
  //             })
  //           }
  //           resolve();
  //         } else if (res.message == "Premium API returned failure" && this.cs.isUndefineORNull(this.breakinID)) {
  //           this.saveProposalForPayment();
  //           swal({
  //             text: res.blazeMessage,
  //           })
  //           resolve();
  //         }else{
  //           this.saveProposalForPayment().then(() => {
  //             if (this.saveRNProposalStatus == "Success") {
  //               if (this.modifyData.isQuoteDeviation) {
  //                 swal({
  //                   text:
  //                     "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases, if recommended proceed for payment. Your Proposal Number: " +
  //                     this.modifyData.generalInformation.proposalNumber +
  //                     " and your Breakin ID is: " +
  //                     this.breakinID +
  //                     " Raised Interaction from below.",
  //                 }).then(() => { });
  //               } else {
  //                 swal({
  //                   text:
  //                     "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases, if recommended proceed for payment. Your Proposal Number: " +
  //                     this.modifyData.generalInformation.proposalNumber +
  //                     " and your Breakin ID is: " +
  //                     this.breakinID,
  //                 }).then(() => {
  //                   // localStorage.setItem('myBreakinPolicy', 'true');
  //                   // this.changedEventListner.emit(true);
  //                   this.router.navigateByUrl("/renewals").then(() => {
  //                     // this.cs.clearLocalStorage();
  //                     // this.cs.clearQuoteDate();
  //                     // this.cs.clearPropData();
  //                     // this.changedEventListner.emit(true);
  //                     this.router
  //                       .navigateByUrl("/", { skipLocationChange: true })
  //                       .then(() => this.router.navigate(["/renewals"]));
  //                   });
  //                 });
  //               }
  //               resolve();
  //             } else {
  //               swal({
  //                 text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
  //               });
  //               resolve();
  //             }
  //           })            
  //         }
  //       } else {
  //         if (this.cs.isUndefineORNull(this.breakinID)) {
  //           this.saveProposalForPayment().then(() => {
  //             this.PlutusPaymentPaymsToken();
  //             resolve();
  //           })
  //         } else {
  //           resolve();
  //         }
  //       }
  //     });
  //   });
  // }


  saveProposalForPayment(): Promise<any> {
    return new Promise((resolve: any) => {
      this.spinloader = true;
      let odindex = this.srt.indexOf('Other Discounting ');
      let idvindex = this.srt.indexOf('IDV Deviation ');
      console.log(odindex, idvindex, this.srt, this.iasODRes);
      let data = JSON.parse(localStorage.getItem('payRenewalData'))
      if (this.productCode == "2312" || (data.POLICY_TYPE == 'PP' && data.VEH_TYPE == 'TWOWHEELER')) {
        this.policySubType = "1";
      } else if (this.productCode == "2311" || (data.POLICY_TYPE == 'PP' && data.VEH_TYPE == 'PVTCAR')) {
        this.policySubType = "2";
      } else if (this.productCode == "2319" || (data.POLICY_TYPE == 'TP' && data.VEH_TYPE == 'PVTCAR')) {
        this.policySubType = "10";
      } else if (this.productCode == "2320" || (data.POLICY_TYPE == 'TP' && data.VEH_TYPE == 'TWOWHEELER')) {
        this.policySubType = "9";
      }
      let sortedRes;
      if (!this.cs.isUndefineORNull(this.modifyData.deviationMessage)) {
        sortedRes = this.modifyData.deviationMessage.replace(/%/g, "");
      } else {
        sortedRes = "";
      }
      this.modifyData.deviationMessage = sortedRes;
      let srt: any, iasProcess: any;
      if (this.cs.isUndefineORNull(this.fetchProposalData)) {
        srt = "";
        iasProcess = "";
      } else {
        if (this.fetchProposalData.length > 1) {
          srt = this.srt[0];
          iasProcess = this.iasProcess[0];
        } else {
          srt = this.srt;
          iasProcess = this.iasProcess;
        }
      }
      let body = {
        PolicyType: 1,
        PolicySubType: parseInt(this.policySubType),
        ProposalNumber: this.comparePremiumPropNo ? this.comparePremiumPropNo : this.modifyData.generalInformation.proposalNumber,//this.modifyData.generalInformation.proposalNumber,
        PolicyStartDate: this.comparePremiumResp ? this.comparePremiumResp.internalRequest.ProposalDetails.PolicyStartDate : this.modifyData.reqPacket.ProposalDetails.PolicyStartDate,
        PolicyEndDate: this.comparePremiumResp ? this.comparePremiumResp.internalRequest.ProposalDetails.PolicyEndDate : this.modifyData.reqPacket.ProposalDetails.PolicyEndDate,
        BasicPremium: this.comparePremiumResp ? this.comparePremiumResp.internalResponse.riskDetails.basicOD : this.modifyData.riskDetails.basicOD,
        Discount: this.comparePremiumResp ? this.comparePremiumResp.internalResponse.riskDetails.bonusDiscount : this.modifyData.riskDetails.bonusDiscount,
        ServiceTax: this.comparePremiumResp ? this.comparePremiumResp.internalResponse.totalTax : this.modifyData.riskDetails.totalTax,
        TotalPremium: this.comparePremiumResp ? this.comparePremiumResp.internalResponse.finalPremium : this.modifyData.riskDetails.finalPremium,
        Status: this.comparePremiumStatus ? 6 : 4,
        DealID: this.comparePremiumResp ? this.comparePremiumResp.internalRequest.DealID : this.modifyData.reqPacket.DealID,
        TOTALTAX: this.comparePremiumResp ? this.comparePremiumResp.internalResponse.totalTax : this.modifyData.riskDetails.totalTax,
        TransFor: "Renewal",
        PolicyTenure: this.comparePremiumResp ? parseInt(this.comparePremiumResp.internalResponse.generalInformation.tenure) : parseInt(this.modifyData.generalInformation.tenure),
        PolicyTpTenure: this.comparePremiumResp ? parseInt(this.comparePremiumResp.internalResponse.generalInformation.tpTenure) : parseInt(this.modifyData.generalInformation.tpTenure),
        PolicyPACoverTenure: !this.cs.isUndefineORNull(this.comparePremiumResp) ? this.comparePremiumResp.internalRequest.ProposalDetails.CustomerDetails.PACoverTenure : 1,
        CustomerID: this.comparePremiumResp ? parseInt(this.comparePremiumResp.internalRequest.CustomerID) : parseInt(this.modifyData.generalInformation.customerId),
        PFCustomerID: this.comparePremiumResp ? parseInt(this.comparePremiumResp.internalResponse.generalInformation.customerId) : parseInt(this.modifyData.generalInformation.customerId),
        CustomerName: this.modifyPropData.CustomerDetails.CustomerName,
        PackagePremium: this.comparePremiumResp ? this.comparePremiumResp.internalResponse.packagePremium : this.modifyData.packagePremium,
        TotalLiablityPremium: this.comparePremiumResp ? this.comparePremiumResp.internalResponse.totalLiabilityPremium : this.modifyData.totalLiabilityPremium,
        ProposalRS: this.comparePremiumResp ? JSON.stringify(this.comparePremiumResp.internalResponse) : JSON.stringify(this.modifyData),
        CorrelationId: this.comparePremiumResp ? JSON.stringify(this.comparePremiumResp.internalRequest.CorrelationId) : this.modifyData.correlationId,
        CustomerType: this.comparePremiumResp ? JSON.stringify(this.comparePremiumResp.internalRequest.ProposalDetails.CustomerDetails.CustomerType) : this.modifyData.reqPacket.ProposalDetails.CustomerDetails.CustomerType,
        ProposalRQ: this.comparePremiumResp ? JSON.stringify(this.comparePremiumResp.internalRequest) : JSON.stringify(this.modifyDataReq),
        InspectionID: this.breakinID ? parseInt(this.breakinID) : 0,
        InspectionStatus: 0,
        InspectionType: this.breakintype.breakin,
        IsStandaloneOD: 0,
        subLocation: this.breakinDetails.subLocation ? this.breakinDetails.subLocation : "",
        TPStartDate: this.comparePremiumResp ? this.comparePremiumResp.internalRequest.ProposalDetails.TPStartDate : this.modifyData.reqPacket.ProposalDetails.TPStartDate,
        TPEndDate: this.comparePremiumResp ? this.comparePremiumResp.internalRequest.ProposalDetails.TPEndDate : this.modifyData.reqPacket.ProposalDetails.TPEndDate,
        TPPolicyNum: "",//this.comparePremiumResp,
        TPInsurerName: this.comparePremiumResp ? this.comparePremiumResp.internalRequest.ProposalDetails.TPInsurerName : this.modifyData.reqPacket.ProposalDetails.TPInsurerName,
        IsSubAgent: 0,
        SubAgentIpartnerUserID: "",
        IDVIASID: this.iasIDVRes ? this.iasIDVRes.srRequestNo : '',
        IDVIASSRT: this.iasIDVReq ? this.iasIDVReq.SRT : '',
        IDVIASSUBSRT: this.iasIDVReq ? this.iasIDVReq.Process : '',
        ODIASID: this.iasODRes ? this.iasODRes.srRequestNo : '',
        ODIASSRT: this.iasODReq ? this.iasODReq.SRT : '',
        ODIASSUBSRT: this.iasODReq ? this.iasODReq.Process : '',
        ODIASRQ: this.iasODReq ? JSON.stringify(this.iasODReq) : '',
        ODIASRS: this.iasODRes ? JSON.stringify(this.iasODRes) : '',
        IDVIASRQ: this.iasIDVReq ? JSON.stringify(this.iasIDVReq) : '',
        IDVIASRS: this.iasIDVRes ? JSON.stringify(this.iasIDVRes) : '',

      }
      let str = JSON.stringify(body);
      this.cs.postForRenewalsPayment("Proposal/SaveRNProposal", str).then((res: any) => {
        this.spinloader = false;
        console.log(res);
        this.saveRNProposalStatus = res.message;
        resolve();

      })
    })
  }

  PlutusPaymentPaymsToken() {
    this.cs.getPaymsToken().then((resp: any) => {
      localStorage.setItem("paymsToken", JSON.stringify(resp.accessToken));
      this.redirectToPlutusPayment();
    });
  }

  redirectToPlutusPayment() {
    let bankType = this.modifyPropData.BankType;
    // console.log('banktype',bankType);
    let instaFlag;
    if (bankType == "INT") {
      instaFlag = 1;
    } else {
      instaFlag = 0;
    }
    let email = this.cs.encrypt(
      this.modifyPropData.CustomerDetails.Email,
      commonData.aesSecretKey
    );
    let phone = this.cs.encrypt(
      this.modifyPropData.CustomerDetails.MobileNumber,
      commonData.aesSecretKey
    );
    let body = {
      CorrelationID: this.modifyData ? this.modifyData.correlationId : this.modifyPropData.CorrelationId,
      Amount: this.modifyData ? this.modifyData.finalPremium : this.modifyPropData.finalPremium,
      ProposalNo: this.comparePremiumPropNo,//this.modifyPropData.proposalNumber,
      DealID: this.modifyPropData.DealID,
      CustomerID: this.modifyPropData.CustomerID,
      Email: email,
      ContactNo: phone,
      UserFlag: 0,
      IsInsta: instaFlag,
      MultiFlag: 0,
      PidFlag: 0,
      PreInsta: 0,
      notes: {
        PreviousPolicyNo: this.modifyPropData.PolicyNumber,
        IsRenewal: "1"
      }
    };

    let str = JSON.stringify(body);
    this.cs.postPayms("Redirection/AddPaymentRequest", str).then((res: any) => {
      // console.log(res);
      if (res.Status == "Success" || res.Status == "success") {
        window.location.href = res.URL;
      } else {
        swal({ text: "Try again later!" });
      }
    });
  }

  // breakinInspectionClear(): Promise<any> {
  //   return new Promise((resolve: any) => {
  //     let breakindata = JSON.parse(localStorage.getItem("breakinData"));
  //     let dealId = JSON.parse(localStorage.getItem("DealID"));
  //     let startDate = moment(new Date()).format("DD-MM-YYYY");

  //     /**
  //      * digitalPOS change
  //      * get values from localStorage for checking if conditions in html file
  //      * date :- 29-07-2021
  //      */
  //     if (this.IsPOSTransaction_flag == true) {
  //       this.posDealID = JSON.parse(localStorage.getItem("DealID"));
  //       this.deal_Id = this.posDealID;
  //     } else {
  //       this.deal_Id = this.quoteReq.DealId;
  //     }
  //     let body = {
  //       InspectionId: JSON.parse(breakindata.inspectionID),
  //       DealNo: this.deal_Id,
  //       ReferenceDate: startDate, //this.propRes.generalInformation.proposalDate,
  //       InspectionStatus: "OK",
  //       CorrelationId: breakindata.corelationID,
  //       ReferenceNo: this.calculatedData.generalInformation.proposalNumber,
  //     };
  //     let str = JSON.stringify(body);
  //     this.cs.postBiz("/breakin/clearinspectionstatus", str).subscribe(
  //       (res: any) => {
  //         resolve();
  //       },
  //       (err) => {
  //         swal({ closeOnClickOutside: false, text: err });
  //       }
  //     );
  //   });
  // }

  paymentValidate() {
    this.spinpayloader = true;
    // console.log(this.modifyPropData)
    // this.readProposalForModifyPdf().then(()=>{
    this.comparePremium().then(() => {
      // this.redirectToPlutusPayment();
    });
    // })
  }

  getPincodeData(pincode: any) {
    this.cs
      .get("Pincode/GetPincodeList?pinCode=" + pincode)
      .subscribe((res: any) => {
        let addNom = document.getElementById('addNmDt') as HTMLInputElement;
        let addGST = document.getElementById('gstdetl') as HTMLInputElement;
        let addLES = document.getElementById('leaseDt') as HTMLInputElement;
        console.log('Nominee', addNom);
        if (!this.cs.isUndefineORNull(this.modifyPropData.NomineeDetails)) {
          addNom.checked = true;
        } else {
          addNom.checked = false;
        }
        if (!this.cs.isUndefineORNull(this.modifyPropData.FinancierDetails)) {
          addLES.checked = true;
        } else {
          addLES.checked = false;
        }
        if (!this.cs.isUndefineORNull(this.modifyPropData.CustomerDetails.GstDetails.GstInNumber)) {
          addGST.checked = true;
        } else {
          addGST.checked = false;
        }

        console.log(res);
        this.pincodeData = res[0];
        if (this.pincodeData.status == "Success") {
          this.CityCode = this.pincodeData.cityDistrictId;
          this.StateCode = this.pincodeData.stateId;
          this.CountryCode = this.pincodeData.countryId;
          this.pinCode = this.pincodeData.pincode;
        }
      });
  }

  // redirectPropPdf() {
  //   this.cs.isFromRenewalModify = true;
  //   this.router.navigate(["renewalPayment"]);
  // }

  pincodeAPI(pincode: any) {
    console.log("Pincode", pincode, pincode.length);
    if (pincode.length > 6 || pincode.length > 5) {
      console.log("Valid Pincode");
      this.isValidPincode = true;
      this.getPincodeData(pincode);
    } else {
      console.log("Invalid Pincode");
      this.isValidPincode = false;
    }
  }

  //Nominee Deatils
  // addNomineeDetails(ev:any){
  //   if (ev.target.checked) {
  //     this.modifyData.IsNomineeModified = true ;
  //   } else {
  //     this.modifyData.IsNomineeModified = false;
  //     this.modifyData.NomineeDetails.NameOfNominee = "";
  //     this.modifyData.NomineeDetails.Relationship = "";
  //     this.modifyData.NomineeDetails.Age = "";
  //   }
  // }

  // GST Details
  enterGSTDetails(ev: any) {
    if (ev.target.checked) {
      this.isGSTDetails = true;
    } else {
      this.isGSTDetails = false;
    }
  }
  // Loan Details
  loanLeaseHypoCheck(ev: any) {
    if (ev.target.checked) {
      this.loanLeaseHypo = true;
    } else {
      this.loanLeaseHypo = false;
    }
  }
  // Add Other Vehicle Details
  addnomineeVehicle1(ev: any) {
    if (ev.target.checked) {
      this.addNomineeVehicle = true;
    } else {
      this.addNomineeVehicle = false;
      this.modifyPropData.NomineeDetails.NameOfNominee = "";
      this.modifyPropData.NomineeDetails.Relationship = "";
      this.modifyPropData.NomineeDetails.Age = "";
    }
  }

  //Addons
  isZdChecked(ev: any) {
    this.isZDCount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.isZD = ev.isTrusted
      console.log('addon Check', this.isZDCount, this.api.isCheck(this.isZDCount));
      this.isAddOnChange = this.api.isCheck(this.isZDCount);
    } else {
      this.isZD = ev;
      console.log('addon Check', this.isZDCount, this.api.isCheck(this.isZDCount));
      this.isAddOnChange = this.api.isCheck(this.isZDCount);
    }
  }

  isRSA1(ev: any) {
    // this.isAddOnChange = true;
    this.isRSACount++;
    console.log('Event', ev, this.modifyPropData.riskDetails.roadSideAssistance, this.selectedRSAPlan)
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.isRSA = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isRSACount);
    } else {
      this.isRSA = ev;
      this.isAddOnChange = this.api.isCheck(this.isRSACount);
    }
    // this.getRSAData();
  }

  isConsu(ev: any) {
    // this.isAddOnChange = true;
    this.isCSCount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.IsConsumables = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isCSCount);
    } else {
      this.IsConsumables = ev;
      this.isAddOnChange = this.api.isCheck(this.isCSCount);
    }
    // this.IsConsumables = ev;
  }

  isKeyProtect(ev: any) {
    // this.isAddOnChange = true;
    this.isKPCount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.isKeyProtectPlan = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isKPCount);
    } else {
      this.isKeyProtectPlan = ev;
      this.isAddOnChange = this.api.isCheck(this.isKPCount);
    }
  }

  istppdDiscount(ev: any) {
    // this.isAddOnChange = true;
    this.isTPPDCount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.isTppdDiscount = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isTPPDCount);
    } else {
      this.isTppdDiscount = ev;
      this.isAddOnChange = this.api.isCheck(this.isTPPDCount);
    }
  }


  isRtiFlag(ev: any) {
    // this.isAddOnChange = true;
    this.isRTICount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.IsRTIApplicableflag = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isRTICount);
    } else {
      this.IsRTIApplicableflag = ev;
      this.isAddOnChange = this.api.isCheck(this.isRTICount);
    }
  }

  isTyreProtect(ev: any) {
    // this.isAddOnChange = true;
    this.isTPCount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.IsTyreProtect = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isTPCount);
    } else {
      this.IsTyreProtect = ev;
      this.isAddOnChange = this.api.isCheck(this.isTPCount);
    }

  }

  isEngineProtect(ev: any) {
    // this.isAddOnChange = true;
    // this.engineProtect = ev;
    this.isEPCount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.engineProtect = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isEPCount);
    } else {
      this.engineProtect = ev;
      this.isAddOnChange = this.api.isCheck(this.isEPCount);
    }
  }

  // isPACoverOd(ev: any) {
  //   this.IsPACoverOwnerDriver = ev;
  //   if(!this.cs.isUndefineORNull(ev.isTrusted)){
  //     this.IsPACoverOwnerDriver = ev.isTrusted
  //   }else{
  //     this.IsPACoverOwnerDriver = ev;
  //   }
  // }
  isPACoverWavier(ev: any) {
    // this.isAddOnChange = true;
    this.isPACWCount++;
    let paWaiver = document.getElementById('paCoverWaiver') as HTMLInputElement;
    let paCover = document.getElementById('PAcover') as HTMLInputElement;
    if (!this.cs.isUndefineORNull(ev.isTrusted) || ev) {
      this.IsPACoverWaiver = false;
      paWaiver.checked = false;
      paCover.checked = true;
      this.isAddOnChange = this.api.isCheck(this.isPACWCount);
    } else {
      paWaiver.checked = true;
      paCover.checked = false;
      this.IsPACoverWaiver = true;
      this.isAddOnChange = this.api.isCheck(this.isPACWCount);
    }
  }

  isLossOfPB(ev: any) {
    // this.isAddOnChange = true;
    this.isLOPBCount++;
    console.log('lossprbl', this.selectedLossOfPersonalBelongingPlanName);
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.LossOfPersonalBelongingPlanName = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isLOPBCount);
    } else {
      this.LossOfPersonalBelongingPlanName = ev;
      this.isAddOnChange = this.api.isCheck(this.isLOPBCount);
    }
  }
  isVolDeduct(ev: any) {
    // this.isAddOnChange = true;
    this.isVDCount++;
    console.log('eventvolded', ev, ev.isTrusted);
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.IsVoluntaryDeductible = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isVDCount);
    } else {
      this.IsVoluntaryDeductible = ev;
      this.isAddOnChange = this.api.isCheck(this.isVDCount);
    }
  }

  isLLPaidDriver(ev: any) {
    // this.isAddOnChange = true;
    this.isLLPDCount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.IsLegalLiabilityToPaidDriver = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isLLPDCount);
    } else {
      this.IsLegalLiabilityToPaidDriver = ev;
      this.isAddOnChange = this.api.isCheck(this.isLLPDCount);
    }
  }

  isLLPaidEmployee(ev: any) {
    // this.isAddOnChange = true;
    this.isLLPECount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.IsLegalLiabilityToPaidEmployee = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isLLPECount);
    } else {
      this.IsLegalLiabilityToPaidEmployee = ev;
      this.isAddOnChange = this.api.isCheck(this.isLLPECount);
    }
  }

  isVehicleHaveCNGLPG(ev: any) {
    // this.isAddOnChange = true;
    this.isCGLGCount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.IsVehicleHaveCNG = ev.isTrusted;
      this.IsVehicleHaveLPG = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isCGLGCount);
    } else {
      this.IsVehicleHaveCNG = ev;
      this.IsVehicleHaveLPG = ev;
      if (!this.IsVehicleHaveLPG && !this.IsVehicleHaveCNG) {
        this.selectedIsVehicleHaveCNGLPG = "";
      }
      this.isAddOnChange = this.api.isCheck(this.isCGLGCount);
    }
  }

  CNGLPGToggle(){
    console.log('CNG LPG', this.cNGLPGTOggle)
    this.cNGLPGTOggle = !this.cNGLPGTOggle
  }

  isNCBProtect(ev: any) {
    // this.isAddOnChange = true;
    this.isNCBCount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.IsNCBProtect = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isNCBCount);
    } else {
      this.IsNCBProtect = ev;
      this.isAddOnChange = this.api.isCheck(this.isNCBCount);
    }
  }

  isGarageCash(ev: any) {
    // this.isAddOnChange = true;
    this.isGCCount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.IsGarageCash = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isGCCount);
    } else {
      this.IsGarageCash = ev;
      this.isAddOnChange = this.api.isCheck(this.isGCCount);
    }
  }

  isPACoverUP(ev: any) {
    // this.isAddOnChange = true;
    this.isPACUPCount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.IsPACoverUnnamedPassenger = ev.isTrusted;
      this.PACovervalue = this.SIPACoverUnnamedPassenger * this.modifyPropData.SeatingCapacity;
      this.isAddOnChange = this.api.isCheck(this.isPACUPCount);
    } else {
      this.IsPACoverUnnamedPassenger = ev;
      this.PACovervalue = this.SIPACoverUnnamedPassenger * this.modifyPropData.SeatingCapacity;
      this.isAddOnChange = this.api.isCheck(this.isPACUPCount);
    }
  }

  isHaveElectAccess(ev: any) {
    // this.isAddOnChange = true;
    this.isEACount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.IsHaveElectricalAccessories = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isEACount);
    } else {
      this.IsHaveElectricalAccessories = ev;
      this.isAddOnChange = this.api.isCheck(this.isEACount);
    }
  }

  isHaveNonElecAccess(ev: any) {
    // this.isAddOnChange = true;
    this.isNEACount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.IsHaveNonElectricalAccessories = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isNEACount);
    } else {
      this.IsHaveNonElectricalAccessories = ev;
      this.isAddOnChange = this.api.isCheck(this.isNEACount);
    }
  }

  isAntiTheftDis(ev: any) {
    // this.isAddOnChange = true;
    this.isATDCount++;
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.isAntiTheftDisc = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isATDCount);
    } else {
      this.isAntiTheftDisc = ev;
      this.isAddOnChange = this.api.isCheck(this.isATDCount);
    }
  }

  isExtensionCon(ev: any) {
    // this.isAddOnChange = true;
    this.isECCount++;
    console.log(this.isECCount, this.api.isCheck(this.isECCount));
    if (!this.cs.isUndefineORNull(ev.isTrusted)) {
      this.IsExtensionCountry = ev.isTrusted;
      this.isAddOnChange = this.api.isCheck(this.isECCount);
    } else {
      this.IsExtensionCountry = ev;
      this.isAddOnChange = this.api.isCheck(this.isECCount);
    }
  }

  closeEmiProtect() {
    console.log('hi');
    let emiProtect = document.getElementById('epaCover') as HTMLInputElement;
    emiProtect.checked = false;
    this.isEMIProtect = false;
  }

  isEmiProtect(ev: any) {
    // this.isAddOnChange = true;
    console.log("EMI", ev)
    this.isEMIPCount++;
    // if (!this.cs.isUndefineORNull(ev.isTrusted)) {
    //   this.isEMIProtect = ev.isTrusted;
    //   this.isAddOnChange = this.api.isCheck(this.isEMIPCount);
    // } else {
    this.isEMIProtect = ev;
    this.isAddOnChange = this.api.isCheck(this.isEMIPCount);
    console.log(this.isAddOnChange);
    // }

  }

  // Deviation Changes
  // getProposalDetails(proposalNo: any) {
  //   // this.cs.loaderStatus = true;
  //   // proposalNo = this.comparePremiumPropNo;
  //   this.spinloader = true;
  //   this.cs
  //     .getBitLy("Proposal/fw/FetchProposal?proposalNo=" + proposalNo + '&productCode=' + this.productCode)
  //     .then((res: any) => {
  //       // +'&productCode'+this.productCode
  //       // console.log("Get Proposal Details", res, res.length);
  //       this.isODIntRaised = false;
  //       this.fetchProposalData = res;
  //       if (this.fetchProposalData.length > 1) {
  //         this.selectedSRT = this.fetchProposalData[0].srTvalue == 'NA ' ? this.fetchProposalData[1].srTvalue : this.fetchProposalData[0].srTvalue;
  //         this.fetchProposalData.forEach((ias: any) => {
  //           if (ias.srTvalue == 'NA ' || ias.subSRTvalue == 'NA ') {
  //             this.isIASRaised = false;
  //           } else {
  //             this.srt.push(ias.srTvalue);
  //             this.iasProcess.push(ias.subSRTvalue);
  //             this.isIASRaised = true;
  //           }
  //         });
  //         if (this.srt.includes('NA ') || this.iasProcess.includes('NA ')) {
  //           this.isIASRaised = false;
  //         } else {
  //           this.isIASRaised = true;
  //         }
  //       } else {
  //         this.srt = this.fetchProposalData[0].srTvalue;
  //         this.selectedSRT = this.fetchProposalData[0].srTvalue;
  //         this.iasProcess = this.fetchProposalData[0].subSRTvalue;
  //         if (this.srt.includes('NA ') || this.iasProcess.includes('NA ')) {
  //           this.isIASRaised = false;
  //         } else {
  //           this.isIASRaised = true;
  //         }
  //       }
  //       this.cs.loaderStatus = false;
  //     }).catch((err: any) => {
  //       console.log("Error", err);
  //       this.cs.loaderStatus = false;
  //     })
  // }

  getProposalDetails(proposalNo: any) {
    this.cs.loaderStatus = true;
    console.log(this.breakinFlag, "this.breakInDeviationFlag")
    this.cs
      .getBitLy(
        "Proposal/fw/FetchProposal?proposalNo=" +
        proposalNo +
        "&productCode=" +
        this.productCode
      )
      .then((res: any) => {
        // +'&productCode='+this.productCode
        // console.log("Get Proposal Details", res, res.length);
        this.isIDVIntRaised = false;
        this.isODIntRaised = false;
        this.fetchProposalData = res;
        this.srt = [];
        this.iasProcess = [];
        if (!this.breakinFlag) {
          this.fetchProposalData = res;
          if (this.fetchProposalData.length > 1) {
            this.selectedSRT =
              this.fetchProposalData[0].srTvalue == "NA "
                ? this.fetchProposalData[1].srTvalue
                : this.fetchProposalData[0].srTvalue;
            this.fetchProposalData.forEach((ias: any) => {
              if (ias.srTvalue == "NA " || ias.subSRTvalue == "NA ") {
                this.isIASRaised = false;
              } else {
                this.srt.push(ias.srTvalue);
                this.iasProcess.push(ias.subSRTvalue);
                this.isIASRaised = true;
              }
            });
            if (this.srt.includes("NA ") || this.iasProcess.includes("NA ")) {
              this.isIASRaised = false;
            } else {
              this.isIASRaised = true;
            }
          } else {
            this.srt = this.fetchProposalData[0].srTvalue;
            this.selectedSRT = this.fetchProposalData[0].srTvalue;
            this.iasProcess = this.fetchProposalData[0].subSRTvalue;
            if (this.srt.includes("NA ") || this.iasProcess.includes("NA ")) {
              this.isIASRaised = false;
            } else {
              this.isIASRaised = true;
            }
          }
        } else {
          if (this.fetchProposalData.length > 1) {
            this.selectedSRT =
              this.fetchProposalData[0].srTvalue == "NA "
                ? this.fetchProposalData[1].srTvalue
                : this.fetchProposalData[0].srTvalue;
            this.fetchProposalData.forEach((ias: any) => {
              if (ias.srTvalue == "NA " || ias.subSRTvalue == "NA ") {
                this.isIASRaised = false;
              } else {
                this.srt.push(ias.srTvalue);
                this.iasProcess.push(ias.subSRTvalue);
                this.isIASRaised = true;
                if (this.srt.includes("IDV Deviation ")) {
                  this.cs.createIASToken().then(() => {
                    this.raisedIDVInteration();
                    this.isIASRaised = true;
                  })
                } else if (this.srt.includes("Other Discounting ")) {
                  this.cs.createIASToken().then(() => {
                    this.raisedODInteraction();
                    this.isIASRaised = true;
                  })
                } else {
                  this.isIASRaised = false;
                }
              }
            });
            if (this.srt.includes("NA ") || this.iasProcess.includes("NA ")) {
              this.isIASRaised = false;
            } else {
              this.isIASRaised = true;
            }
          } else {
            this.srt = this.fetchProposalData[0].srTvalue;
            this.selectedSRT = this.fetchProposalData[0].srTvalue;
            this.iasProcess = this.fetchProposalData[0].subSRTvalue;
            if (this.srt.includes("NA ") || this.iasProcess.includes("NA ")) {
              this.isIASRaised = false;
              swal({
                text:
                  "Your proposal number is " + proposalNo + " and goes into deviation for SRT " + this.srt + " and Process " + this.iasProcess + " Unable to raised interaction",
              })
            } else {
              this.isIASRaised = true;
              this.cs.createIASToken().then(() => {
                this.raiseInteraction();
              });
            }
          }
        }

        this.cs.loaderStatus = false;
      })
      .catch((err: any) => {
        console.log("Error", err);
        this.cs.loaderStatus = false;
      });
  }

  getDealDetails() {
    this.api.getDealDetails(this.modifyPropData.DealID).subscribe((res: any) => {
      if (res.status == "FAILED") {
        swal({ text: "Deal Details not found" });
        let errorbody = {
          RequestJson: this.modifyPropData.DealID,
          ResponseJson: JSON.stringify(res),
          ServiceURL:
            "Deal/GetDealDetailsFromDeal?dealID=" + this.modifyPropData.DealID,
          CorrelationID: this.modifyPropData.CorrelationId,
        };
        this.api.adderrorlogs(errorbody);
      } else {
        this.dealDetailsfromIM = res;
        // console.clear();
      }
    });
  }

  getSrt(srt: any) {
    console.log("SRT", srt);
    this.selectedSRT = srt;
  }

  createToken() {
    this.cs.createIASToken().then(() => {
      this.raiseInteraction();
    });
  }

  raiseInteraction() {
    if (
      this.srt.includes("IDV Deviation ") &&
      !this.isIDVIntRaised &&
      this.selectedSRT == "IDV Deviation "
    ) {
      this.raisedIDVInteration();
      this.isIASRaised = true;
    } else if (
      this.srt.includes("Other Discounting ") &&
      !this.isODIntRaised &&
      this.selectedSRT == "Other Discounting "
    ) {
      this.raisedODInteraction();
      this.isIASRaised = true;
    } else {
      this.isIASRaised = false;
    }
  }

  raisedIDVInteration() {
    let productName;
    let vehicleType;
    let authData = JSON.parse(localStorage.getItem("AuthToken"));
    if (this.productCode == "2312" || this.productCode == "2320") {
      productName = "TW";
      vehicleType = "MOTORCYCLE";
    } else if (this.productCode == "2311" || this.productCode == "2319") {
      productName = "Pvt Car";
      vehicleType = "PRIVATE CAR";
    }

    let exShowData = JSON.parse(localStorage.getItem("exShowData"));



    let body = {
      CorrelationId: this.modifyData.correlationId,
      Actcode: "000",
      ApplicationName: "IAS",
      SRT: "IDV Deviation",
      Process: this.iasProcess.includes("IDV Deviation UW Approval Level II ") ? "IDV Deviation UW Approval Level II " : "IDV Deviation PVT Car Level I ",
      CreatorId: this.dealDetailsfromIM.hR_REF_NO,
      ActionType: "INSERT",
      UI: [
        {
          Legend: "Proposal No.",
          Value: this.comparePremiumPropNo ? this.comparePremiumPropNo : this.modifyData.generalInformation.proposalNumber,//this.modifyData.generalInformation.proposalNumber,
        },
        {
          Legend: "Cover Note Mode",
          Value: "AUTO",
        },
        {
          Legend: "Location",
          Value: this.dealDetailsfromIM.officeName,
        },
        {
          Legend: "Business Type",
          Value: "Rollover",
        },
        {
          Legend: "No of Vehicles for which approval is required",
          Value: "1",
        },
        {
          Legend: "Manufacturing year",
          Value: this.modifyPropData.ManufacturingYear,
        },
        {
          Legend: "RTO Location",
          Value: this.modifyData.generalInformation.rtoLocation,
        },
        {
          Legend: "Manufacturer",
          Value: this.modifyData.generalInformation.manufacturerName,
        },
        {
          Legend: "Model",
          Value: this.modifyData.generalInformation.vehicleModel,
        },
        {
          Legend: "Remarks",
          Value: "User entry in IAS",
        },
        {
          Legend: "Previous year NCB",
          Value: "0",
        },
        {
          Legend: "Current year NCB",
          Value: "20",
        },
        {
          Legend: "Deal No",
          Value: this.dealDetailsfromIM.dealID,
        },
        {
          Legend: "Vertical",
          Value: "AGENCY",
        },
        {
          Legend: "Requestor name",
          Value: this.dealDetailsfromIM.intermediaryName,
        },
        {
          Legend: "Policy Type",
          Value: "Comprehensive", //this.policyType,
        },
        {
          Legend: "Product",
          Value: productName,
        },
        {
          Legend: "Transaction Type",
          Value: "Intermediary",
        },
        {
          Legend: "Agent Name",
          Value: this.dealDetailsfromIM.employeeName,
        },
        {
          Legend: "Insured Name",
          Value: this.modifyPropData.CustomerDetails.CustomerName,
        },
        {
          Legend: "ECN OR Registration No",
          Value: this.modifyPropData.RegistrationNumber,
        },
        {
          Legend: "Vehicle type",
          Value: vehicleType,
        },
        {
          Legend: "SM Vertical",
          Value: "Agency",
        },
        {
          Legend: "SM Name",
          Value: this.dealDetailsfromIM.intermediaryName,
        },
        {
          Legend: "Cover Note No",
          Value: "0",
        },
        {
          Legend: "Policy No",
          Value: this.comparePremiumPropNo ? this.comparePremiumPropNo : this.modifyData.generalInformation.proposalNumber,//this.modifyData.generalInformation.proposalNumber,
        },
        {
          Legend: "Deal ID",
          Value: this.dealDetailsfromIM.dealID,
        },
        {
          Legend: "Agent Code",
          Value: this.dealDetailsfromIM.intermediaryCode,
        },
        {
          Legend: "Showroom price",
          Value: this.modifyPropData.VehicleDetails.ShowRoomPrice,
        },
        {
          Legend: "IDV (as per system)",
          Value: this.modifyPropData.VehicleDetails.DepriciatedIDV,
        },
        {
          Legend: "IDV (as required)",
          Value: this.modifyPropData.VehicleDetails.DepriciatedIDV,
        },
        {
          Legend: "IDV deviation Side",
          Value: "Higher Side",
        },
        {
          Legend: "ECN / registration details",
          Value: this.modifyPropData.RegistrationNumber,
        },
        {
          Legend: "Total IDV deviation (in %)",
          Value: "0.3",
        },
      ],
    };

    this.ineractionSpinner = true;
    this.iasIDVReq = body;
    this.cs.postILSBIAS("/Genus/IASInteractionID", body).subscribe(
      (res: any) => {
        console.log(res);
        if (res.status == "Success") {
          this.iasIntRes = res;
          this.iasIDVRes = res;
          this.ineractionSpinner = false;
          this.isIDVIntRaised = true;
          if (!this.breakinFlag) {
            // this.comparePremium().then(() => {
            if (this.fetchProposalData.length == 1) {
              this.saveProposalForPayment();
            } else {
              if (this.srt.includes("Other Discounting ")) {
                if (!this.cs.isUndefineORNull(this.iasODRes.srRequestNo)) {
                  this.saveProposalForPayment();
                }
              }

            }

            // })
          }
        } else {
          // if (!this.breakinFlag && this.fetchProposalData.length == 1) {
          //   this.comparePremium().then(() => {
          //     this.saveProposalForPayment();
          //   })
          // }
          this.router.navigateByUrl("quote");
          this.ineractionSpinner = false;
        }
      },
      (err) => {
        swal({ closeOnClickOutside: false, text: err });
        this.ineractionSpinner = false;
      }
    );
  }

  raisedODInteraction() {
    let productName;
    let vehicleType;
    let body;
    let authData = JSON.parse(localStorage.getItem("AuthToken"));
    if (this.productCode == "2312" || this.productCode == "2320") {
      productName = "TW";
      vehicleType = "MOTORCYCLE";
    } else if (this.productCode == "2311" || this.productCode == "2319") {
      productName = "Pvt Car";
      vehicleType = "PRIVATE CAR";
    }

    if (this.iasProcess.includes("Other Discounting process related approvals")) {
      body = {
        CorrelationId: this.modifyData.correlationId,
        Actcode: "000",
        ApplicationName: "IAS",
        SRT: this.srt,
        Process: this.iasProcess,
        CreatorId: this.dealDetailsfromIM.hR_REF_NO,
        ActionType: "INSERT",
        UI: [
          {
            Legend: "Proposal No.",
            Value: this.comparePremiumPropNo ? this.comparePremiumPropNo : this.modifyData.generalInformation.proposalNumber,//this.modifyData.generalInformation.proposalNumber,
          },
          {
            Legend: "Cover Note Mode",
            Value: "AUTO",
          },
          {
            Legend: "Location",
            Value: this.dealDetailsfromIM.officeName,
          },
          {
            Legend: "Business Type",
            Value: 'Rollover',
          },
          {
            Legend: "No of Vehicles for which approval is required",
            Value: "1",
          },
          {
            Legend: "Manufacturing year",
            Value: this.modifyPropData.ManufacturingYear, // Need to change before prod
          },
          {
            Legend: "RTO Location",
            Value: this.modifyData.generalInformation.rtoLocation,
          },
          {
            Legend: "Manufacturer",
            Value: this.modifyData.generalInformation.manufacturerName,
          },
          {
            Legend: "Model",
            Value: this.modifyData.generalInformation.vehicleModel,
          },
          {
            Legend: "Remarks",
            Value: "User entry in IAS",
          },
          {
            Legend: "Previous year NCB",
            Value: "0",
          },
          {
            Legend: "Current year NCB",
            Value: "20",
          },
          {
            Legend: "Deal No",
            Value: this.dealDetailsfromIM.dealID,
          },
          {
            Legend: "Vertical",
            Value: "AGENCY",
          },
          {
            Legend: "Policy Type",
            Value: "Package", //this.policyType
          },
          {
            Legend: "Product",
            Value: productName,
          },
          {
            Legend: "Transaction Type",
            Value: "Intermediary",
          },
          {
            Legend: "Agent Name",
            Value: this.dealDetailsfromIM.employeeName,
          },
          {
            Legend: "Insured Name",
            Value: this.modifyPropData.CustomerDetails.CustomerName,
          },
          {
            Legend: "ECN OR Registration No",
            Value: this.modifyPropData.RegistrationNumber,
          },
          {
            Legend: "Vehicle type",
            Value: vehicleType,
          },
          {
            Legend: "SM Vertical",
            Value: "Agency",
          },
          {
            Legend: "SM Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Cover Note No",
            Value: "0",
          },
          {
            Legend: "Policy No",
            Value: this.comparePremiumPropNo ? this.comparePremiumPropNo : this.modifyData.generalInformation.proposalNumber,//this.modifyData.generalInformation.proposalNumber,
          },
          {
            Legend: "Payout involved (in %)",
            Value: "0",
          },
          {
            Legend: "Vehicle type/Subclass",
            Value: "Private",
          },
          {
            Legend: "Intermediary Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Proposal No",
            Value: this.comparePremiumPropNo ? this.comparePremiumPropNo : this.modifyData.generalInformation.proposalNumber,//this.modifyData.generalInformation.proposalNumber,
          },
          {
            Legend: "ECN / registration no. details",
            Value: this.modifyPropData.RegistrationNumber,
          },
          {
            Legend: "System built in discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Required discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Requestor name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "IM ID",
            Value: authData.username,
          },
          {
            Legend: "Intermediary code",
            Value: this.dealDetailsfromIM.intermediaryCode,
          },
          {
            Legend: "Vehicle Usage",
            Value: "Others",
          },
          {
            Legend: "Area of Operation",
            Value: "INTER-CITY",
          },
          {
            Legend: "Vehicle Driven By",
            Value: "Others",
          },
          {
            Legend: "Approval process",
            Value: "Select",
          },
          {
            Legend: "Zero Dep covered",
            Value: "No",
          },
          {
            Legend: "Geo Sub vertical",
            Value: "Others",
          },
          {
            Legend: "Car segment",
            Value: 'non premium segment',//this.fetchProposalData[0].carSegment,
          },
        ],
      };
    } else if (this.iasProcess.includes("Other discounting Non New Hyundai")) {
      body = {
        CorrelationId: this.modifyData.correlationId,
        Actcode: "000",
        ApplicationName: "IAS",
        SRT: this.srt,
        Process: this.iasProcess,
        CreatorId: this.dealDetailsfromIM.hR_REF_NO,
        ActionType: "INSERT",
        UI: [
          {
            Legend: "Proposal No.",
            Value: this.comparePremiumPropNo ? this.comparePremiumPropNo : this.modifyData.generalInformation.proposalNumber,//this.modifyData.generalInformation.proposalNumber,
          },
          {
            Legend: "Cover Note Mode",
            Value: "AUTO",
          },
          {
            Legend: "Location",
            Value: this.dealDetailsfromIM.officeName,
          },
          {
            Legend: "Business Type",
            Value: 'Rollover',
          },
          {
            Legend: "No of Vehicles for which approval is required",
            Value: "1",
          },
          {
            Legend: "Manufacturing year",
            Value: this.modifyPropData.ManufacturingYear, // Need to change before prod
          },
          {
            Legend: "RTO Location",
            Value: this.modifyData.generalInformation.rtoLocation,
          },
          {
            Legend: "Manufacturer",
            Value: this.modifyData.generalInformation.manufacturerName,
          },
          {
            Legend: "Model",
            Value: this.modifyData.generalInformation.vehicleModel,
          },
          {
            Legend: "Remarks",
            Value: "User entry in IAS",
          },
          {
            Legend: "Previous year NCB",
            Value: "0",
          },
          {
            Legend: "Current year NCB",
            Value: "20",
          },
          {
            Legend: "Deal No",
            Value: this.dealDetailsfromIM.dealID,
          },
          {
            Legend: "Vertical",
            Value: "AGENCY",
          },
          {
            Legend: "Policy Type",
            Value: "Package", //this.policyType
          },
          {
            Legend: "Product",
            Value: productName,
          },
          {
            Legend: "Transaction Type",
            Value: "Intermediary",
          },
          {
            Legend: "Agent Name",
            Value: this.dealDetailsfromIM.employeeName,
          },
          {
            Legend: "Insured Name",
            Value: this.modifyPropData.CustomerDetails.CustomerName,
          },
          {
            Legend: "ECN OR Registration No",
            Value: this.modifyPropData.RegistrationNumber,
          },
          {
            Legend: "Vehicle type",
            Value: vehicleType,
          },
          {
            Legend: "SM Vertical",
            Value: "Agency",
          },
          {
            Legend: "SM Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Cover Note No",
            Value: "0",
          },
          {
            Legend: "Policy No",
            Value: this.comparePremiumPropNo ? this.comparePremiumPropNo : this.modifyData.generalInformation.proposalNumber,//this.modifyData.generalInformation.proposalNumber,
          },
          {
            Legend: "Payout involved (in %)",
            Value: "0",
          },
          {
            Legend: "Vehicle type/Subclass",
            Value: "Private",
          },
          {
            Legend: "Intermediary Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Proposal No",
            Value: this.comparePremiumPropNo ? this.comparePremiumPropNo : this.modifyData.generalInformation.proposalNumber,//this.modifyData.generalInformation.proposalNumber,
          },
          {
            Legend: "ECN / registration no. details",
            Value: this.modifyPropData.RegistrationNumber,
          },
          {
            Legend: "System built in discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Required discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Requestor name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "IM ID",
            Value: authData.username,
          },
          {
            Legend: "Intermediary code",
            Value: this.dealDetailsfromIM.intermediaryCode,
          },
          {
            Legend: "Vehicle Usage",
            Value: "Others",
          },
          {
            Legend: "Area of Operation",
            Value: "INTER-CITY",
          },
          {
            Legend: "Vehicle Driven By",
            Value: "Others",
          },
          {
            Legend: "Approval process",
            Value: "Select",
          },
          {
            Legend: "Zero Dep covered",
            Value: "No",
          },
          {
            Legend: "Geo Sub vertical",
            Value: "M2",
          },
          {
            Legend: "Car segment",
            Value: 'non premium segment',//this.fetchProposalData[0].carSegment,
          },
        ],
      };
    } else if (this.iasProcess.includes("Other discounting Non New Toyota")) {
      body = {
        CorrelationId: this.modifyData.correlationId,
        Actcode: "000",
        ApplicationName: "IAS",
        SRT: this.srt,
        Process: this.iasProcess,
        CreatorId: this.dealDetailsfromIM.hR_REF_NO,
        ActionType: "INSERT",
        UI: [
          {
            Legend: "Proposal No.",
            Value: this.comparePremiumPropNo ? this.comparePremiumPropNo : this.modifyData.generalInformation.proposalNumber,//this.modifyData.generalInformation.proposalNumber,
          },
          {
            Legend: "Cover Note Mode",
            Value: "AUTO",
          },
          {
            Legend: "Location",
            Value: this.dealDetailsfromIM.officeName,
          },
          {
            Legend: "Business Type",
            Value: 'Rollover',
          },
          {
            Legend: "No of Vehicles for which approval is required",
            Value: "1",
          },
          {
            Legend: "Manufacturing year",
            Value: this.modifyPropData.ManufacturingYear, // Need to change before prod
          },
          {
            Legend: "RTO Location",
            Value: this.modifyData.generalInformation.rtoLocation,
          },
          {
            Legend: "Manufacturer",
            Value: this.modifyData.generalInformation.manufacturerName,
          },
          {
            Legend: "Model",
            Value: this.modifyData.generalInformation.vehicleModel,
          },
          {
            Legend: "Remarks",
            Value: "User entry in IAS",
          },
          {
            Legend: "Previous year NCB",
            Value: "0",
          },
          {
            Legend: "Current year NCB",
            Value: "20",
          },
          {
            Legend: "Deal No",
            Value: this.dealDetailsfromIM.dealID,
          },
          {
            Legend: "Vertical",
            Value: "AGENCY",
          },
          {
            Legend: "Policy Type",
            Value: "Package", //this.policyType
          },
          {
            Legend: "Product",
            Value: productName,
          },
          {
            Legend: "Transaction Type",
            Value: "Intermediary",
          },
          {
            Legend: "Agent Name",
            Value: this.dealDetailsfromIM.employeeName,
          },
          {
            Legend: "Insured Name",
            Value: this.modifyPropData.CustomerDetails.CustomerName,
          },
          {
            Legend: "ECN OR Registration No",
            Value: this.modifyPropData.RegistrationNumber,
          },
          {
            Legend: "Vehicle type",
            Value: vehicleType,
          },
          {
            Legend: "SM Vertical",
            Value: "Agency",
          },
          {
            Legend: "SM Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Cover Note No",
            Value: "0",
          },
          {
            Legend: "Policy No",
            Value: this.comparePremiumPropNo ? this.comparePremiumPropNo : this.modifyData.generalInformation.proposalNumber,//this.modifyData.generalInformation.proposalNumber,
          },
          {
            Legend: "Payout involved (in %)",
            Value: "0",
          },
          {
            Legend: "Vehicle type/Subclass",
            Value: "PRIVATE CAR",
          },
          {
            Legend: "Intermediary Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Proposal No",
            Value: this.comparePremiumPropNo ? this.comparePremiumPropNo : this.modifyData.generalInformation.proposalNumber,//this.modifyData.generalInformation.proposalNumber,
          },
          {
            Legend: "ECN / registration no. details",
            Value: this.modifyPropData.RegistrationNumber,
          },
          {
            Legend: "System built in discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Required discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Requestor name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "IM ID",
            Value: authData.username,
          },
          {
            Legend: "Intermediary code",
            Value: this.dealDetailsfromIM.intermediaryCode,
          },
          {
            Legend: "Vehicle Usage",
            Value: "Others",
          },
          {
            Legend: "Area of Operation",
            Value: "INTER-CITY",
          },
          {
            Legend: "Vehicle Driven By",
            Value: "Others",
          },
          {
            Legend: "Approval process",
            Value: "Select",
          },
          {
            Legend: "Zero Dep covered",
            Value: "No",
          },
          {
            Legend: "Geo Sub vertical",
            Value: "M2",
          },
          {
            Legend: "Car segment",
            Value: 'non premium segment',//this.fetchProposalData[0].carSegment,
          },
        ],
      };
    } else {
      body = {
        CorrelationId: this.modifyData.correlationId,
        Actcode: "000",
        ApplicationName: "IAS",
        SRT: "Other Discounting",
        Process: "Other Discounting exceptional approvals",
        CreatorId: this.dealDetailsfromIM.hR_REF_NO,
        ActionType: "INSERT",
        UI: [
          {
            Legend: "Proposal No.",
            Value: this.comparePremiumPropNo ? this.comparePremiumPropNo : this.modifyData.generalInformation.proposalNumber,//this.modifyData.generalInformation.proposalNumber,
          },
          {
            Legend: "Cover Note Mode",
            Value: "AUTO",
          },
          {
            Legend: "Location",
            Value: this.dealDetailsfromIM.officeName,
          },
          {
            Legend: "Business Type",
            Value: 'Rollover',
          },
          {
            Legend: "No of Vehicles for which approval is required",
            Value: "1",
          },
          {
            Legend: "Manufacturing year",
            Value: this.modifyPropData.ManufacturingYear, // Need to change before prod
          },
          {
            Legend: "RTO Location",
            Value: this.modifyData.generalInformation.rtoLocation,
          },
          {
            Legend: "Manufacturer",
            Value: this.modifyData.generalInformation.manufacturerName,
          },
          {
            Legend: "Model",
            Value: this.modifyData.generalInformation.vehicleModel,
          },
          {
            Legend: "Remarks",
            Value: "User entry in IAS",
          },
          {
            Legend: "Previous year NCB",
            Value: "0",
          },
          {
            Legend: "Current year NCB",
            Value: "20",
          },
          {
            Legend: "Deal No",
            Value: this.dealDetailsfromIM.dealID,
          },
          {
            Legend: "Vertical",
            Value: "AGENCY",
          },
          {
            Legend: "Policy Type",
            Value: "Package", //this.policyType
          },
          {
            Legend: "Product",
            Value: productName,
          },
          {
            Legend: "Transaction Type",
            Value: "Intermediary",
          },
          {
            Legend: "Agent Name",
            Value: this.dealDetailsfromIM.employeeName,
          },
          {
            Legend: "Insured Name",
            Value: this.modifyPropData.CustomerDetails.CustomerName,
          },
          {
            Legend: "ECN OR Registration No",
            Value: this.modifyPropData.RegistrationNumber,
          },
          {
            Legend: "Vehicle type",
            Value: vehicleType,
          },
          {
            Legend: "SM Vertical",
            Value: "Agency",
          },
          {
            Legend: "SM Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Cover Note No",
            Value: "0",
          },
          {
            Legend: "Policy No",
            Value: this.comparePremiumPropNo ? this.comparePremiumPropNo : this.modifyData.generalInformation.proposalNumber,//this.modifyData.generalInformation.proposalNumber,
          },
          {
            Legend: "Payout involved (in %)",
            Value: "0",
          },
          {
            Legend: "Vehicle type/Subclass",
            Value: "Private",
          },
          {
            Legend: "Intermediary Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Proposal No",
            Value: this.comparePremiumPropNo ? this.comparePremiumPropNo : this.modifyData.generalInformation.proposalNumber,//this.modifyData.generalInformation.proposalNumber,
          },
          {
            Legend: "ECN / registration no. details",
            Value: this.modifyPropData.RegistrationNumber,
          },
          {
            Legend: "System built in discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Required discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Requestor name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Pathfinder IM ID",
            Value: authData.username,
          },
          {
            Legend: "Intermediary code or ID",
            Value: this.dealDetailsfromIM.intermediaryCode,
          },
          {
            Legend: "Vehicle Usage",
            Value: "Others",
          },
          {
            Legend: "Area of Operation",
            Value: "INTER-CITY",
          },
          {
            Legend: "Vehicle Driven By",
            Value: "Others",
          },
          // comment below code while production pointing
          {
            "Legend": "Approval process",
            "Value": "Select"
          },
          {
            "Legend": "Zero Dep covered",
            "Value": "NO"
          },
          {
            "Legend": "Geo Sub vertical",
            "Value": "M2"
          },
          {
            "Legend": "Car segment",
            "Value": "non premium segment"
          },
          {
            "Legend": "Current policy start date",
            "Value": "2021-12-31"
          },
          {
            "Legend": "Current year IDV",
            "Value": "563452"
          },
          {
            "Legend": "Discount required (in %)",
            "Value": this.otherDiscount ? this.otherDiscount : '20'
          },
          {
            "Legend": "OD Rate (as per system)",
            "Value": "45"
          },
          {
            "Legend": "OD Rate (as required)",
            "Value": this.otherDiscount ? this.otherDiscount : '20'
          },
          {
            "Legend": "Approval type",
            "Value": "Other Discounting exceptional approvals"
          },
          {
            "Legend": "No. of vehicles for which approval is required",
            "Value": "0"
          }
        ],
      };
    }

    this.ineractionSpinner = true;
    this.iasODReq = body;
    this.cs.postILSBIAS("/Genus/IASInteractionID", JSON.stringify(body)).subscribe(
      (res: any) => {
        console.log(res);
        // this.iasIntRes = res;
        // this.iasODRes = res;
        if (res.status == "Success") {
          this.isODIntRaised = true;
          this.iasIntRes = res;
          this.iasODRes = res;
          this.ineractionSpinner = false;
          if (!this.breakinFlag) {
            if (this.fetchProposalData.length == 1) {
              this.saveProposalForPayment();
            } else {
              if (this.srt.includes("IDV Deviation ")) {
                if (!this.cs.isUndefineORNull(this.iasIDVRes.srRequestNo)) {
                  this.saveProposalForPayment();
                }
              }

            }
          }

          // this.router.navigateByUrl("quote");
        } else {
          this.router.navigateByUrl("quote");
          this.ineractionSpinner = false;
        }
      },
      (err) => {
        swal({ closeOnClickOutside: false, text: err });
        this.ineractionSpinner = false;
        // this.router.navigateByUrl("quote");
      }
    );
  }

  // saveofflineProposal(): Promise<any> {
  //   return new Promise((resolve: any) => {
  //     let body, URL;
  //     let odindex = this.srt.indexOf('Other Discounting ');
  //     let idvindex = this.srt.indexOf('IDV Deviation ');
  //     console.log(odindex, idvindex, this.srt);
  //     let sortedRes;
  //     if (!this.cs.isUndefineORNull(this.modifyData.deviationMessage)) {
  //       sortedRes = this.modifyData.deviationMessage.replace(/%/g, "");
  //     } else {
  //       sortedRes = "";
  //     }
  //     this.modifyData.deviationMessage = sortedRes;
  //     let srt: any, iasProcess: any;
  //     if (this.cs.isUndefineORNull(this.fetchProposalData)) {
  //       srt = '';
  //       iasProcess = '';
  //     } else {
  //       if (this.fetchProposalData.length > 1) {
  //         srt = this.srt[0];
  //         iasProcess = this.iasProcess[0];
  //       } else {
  //         srt = this.srt;
  //         iasProcess = this.iasProcess;
  //       }
  //     }
  //     let bankType = localStorage.getItem("bankType");
  //     body = {
  //       proposalRQ: this.modifyDataReq,
  //       proposalRS: this.modifyData,
  //       bankType: bankType,
  //       IasRQ: {
  //         OdIasID: this.iasODRes ? this.iasODRes.srRequestNo : '',
  //         OdIasSrt: ((this.iasODRes && odindex < 0) || (this.iasODRes && idvindex < 0)) ? srt : this.srt[odindex],
  //         OdIasSubSrt: ((this.iasODRes && odindex < 0) || (this.iasODRes && idvindex < 0)) ? iasProcess : this.iasProcess[odindex],
  //         IdvIasID: '',
  //         IdvIasSrt: '',
  //         IdvIasSubSrt: '',
  //         OdIasRQ: this.iasODRes ? this.iasODReq : '',
  //         OdIasRS: this.iasODRes ? JSON.stringify(this.iasODRes) : '',
  //         IdvIasRQ: '',
  //         IdvIasRS: '',
  //       },
  //     };
  //     let str = JSON.stringify(body);
  //     this.cs.loaderStatus = true;
  //     this.comparePremium().then(() => {
  //       this.cs
  //         .post("Proposal/SaveRNProposal", str)
  //         .then((res: any) => {
  //           if (res.status == "Success") {
  //             // this.saveProposalStatus = res.status;
  //             // this.getIASData();
  //           } else {
  //             let errorbody = {
  //               RequestJson: JSON.stringify(body),
  //               ResponseJson: JSON.stringify(res),
  //               ServiceURL: URL,
  //               CorrelationID: this.modifyData.correlationId,
  //             };
  //             this.api.adderrorlogs(errorbody);
  //           }
  //         })
  //       this.cs.loaderStatus = false;
  //       resolve();
  //     })
  //       .catch((err: any) => {
  //         swal({ text: err });
  //         this.cs.loaderStatus = false;
  //       });
  //   });
  // }

  //breakIn
  inspectionType(ev: any) {
    // this.IsExtensionCountry = false;
    // this.IsSelfInspection = false;
    // this.isZD = false;
    // this.isRSA = false;
    // this.IsRTIApplicableflag = false;
    // this.IsConsumables = false;
    this.breakinDetails.inspectionMode = ev.target.value;
    if (ev.target.value == "selfinspection") {
      this.IsSelfInspection = true;
      this.isAddOnChange = true;
      this.breakintype.isPreApprovedBreakIN = false;
      // this.openBrekinModal();
      this.modifyProposal();
    } else if (ev.target.value == "ilinspection") {
      this.IsSelfInspection = false;
      this.isAddOnChange = true;
      this.breakintype.isPreApprovedBreakIN = false;
      // this.openBrekinModal();
      this.modifyProposal();
    } else {
      this.IsSelfInspection = false;
      this.isAddOnChange = true;
      this.breakintype.isPreApprovedBreakIN = true;
    }
  }
  getSubLocationCities(ev?) {
    if (ev.target.value.length < 3 || ev.key == "Backspace") {
    } else {
      let postObj = ev.target.value;
      this.api.getSubLocation(postObj).subscribe((res: any) => {
        this.sublocations = res;
      });
    }
  }

  checkBreakINStatus(ev: any) {
    if (this.productCode == "2312") {
      this.policySubType = "1";
    } else if (this.productCode == "2311") {
      this.policySubType = "2";
    } else if (this.productCode == "2319") {
      this.policySubType = "10";
    } else if (this.productCode == "2320") {
      this.policySubType = "9";
    }
    this.cs
      .getURL(
        "BreakIn/GetPreApprovedBreakInStatus?breakinID=" +
        this.breakintype.breakINID +
        "&correlationID=5ffc4341-1453-4212-b795-9010969d6c1c"
      )
      .then((res: any) => {
        if (res.breakInStatus != "Recommended") {
          this.breakintype.breakINID = "";
          swal({
            text: "Break-In Id not recommended. Kindly use another BreakIn",
            closeOnClickOutside: false,
          });
        } else {
          if (res.policySubType != this.policySubType) {
            swal({
              text: "Break-In Id does not belong to this product",
              closeOnClickOutside: false,
            });
            this.breakintype.breakINID = "";
          } else {
            localStorage.setItem("PreBreakINData", JSON.stringify(res));
            this.PreBreakINDataresponse = res;
            this.IsSelfInspection = false;
            this.modifyProposal();
            this.breakintype.breakin = "preApprovedBreakIN";
          }
        }
      })
      .catch((err: any) => {
        console.log(err);
      });
  }

  // submitBreakin(inspectionMode: any, subLocation: any) {
  //   // $("#renewalModifyBreakin_modal").modal("hide");
  //   this.createBreakInId(this.modifyData).then(() => {
  //     if (
  //       this.breakinID == undefined ||
  //       this.breakinID == 0 ||
  //       this.breakinID == null
  //     ) {
  //       swal({
  //         text:
  //           "Breakin Id could not be generated to this vehicle. Reason: " +
  //           this.errorMessage,
  //       }).then(() => {
  //         this.comparePremium().then(()=>{
  //           this.saveProposalForPayment().then(() => {
  //             if (this.saveRNProposalStatus == "Success") {
  //               console.log("fail breakin");
  //             } else {
  //               swal({
  //                 text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
  //               });
  //             }
  //           });
  //         })

  //       });
  //     } else {
  //       this.comparePremium().then(()=>{
  //         this.saveProposalForPayment().then(() => {
  //           if (this.saveRNProposalStatus == "Success") {
  //             if (this.modifyData.isQuoteDeviation) {
  //               swal({
  //                 text:
  //                   "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases, if recommended proceed for payment. Your Proposal Number: " +
  //                   this.modifyData.generalInformation.proposalNumber +
  //                   " and your Breakin ID is: " +
  //                   this.breakinID +
  //                   " Raised Interaction from below.",
  //               }).then(() => { });
  //             } else {
  //               swal({
  //                 text:
  //                   "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases, if recommended proceed for payment. Your Proposal Number: " +
  //                   this.modifyData.generalInformation.proposalNumber +
  //                   " and your Breakin ID is: " +
  //                   this.breakinID,
  //               }).then(() => {
  //                 // localStorage.setItem('myBreakinPolicy', 'true');
  //                 // this.changedEventListner.emit(true);
  //                 this.router.navigateByUrl("/renewals").then(() => {
  //                   // this.cs.clearLocalStorage();
  //                   // this.cs.clearQuoteDate();
  //                   // this.cs.clearPropData();
  //                   // this.changedEventListner.emit(true);
  //                   this.router
  //                     .navigateByUrl("/", { skipLocationChange: true })
  //                     .then(() => this.router.navigate(["/renewals"]));
  //                 });
  //               });
  //             }
  //           } else {
  //             swal({
  //               text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
  //             });
  //           }
  //         });
  //       })
  //     }
  //   });
  // }

  submitBreakin(inspectionMode: any, subLocation: any) {
    // $("#breakin_modal").modal("hide");
    this.showBreakInPopUP = false;
    this.createBreakInId(this.modifyData).then(() => {
      if (
        this.breakinID == undefined ||
        this.breakinID == 0 ||
        this.breakinID == null
      ) {
        if (this.modifyData.isQuoteDeviation) {
          if (this.fetchProposalData[0].srTvalue == ' NA' || this.fetchProposalData[0].subSRTvalue == ' NA') {
            swal({
              text:
                "Breakin Id could not be generated to this vehicle. Reason: " +
                this.errorMessage + ' and your proposal ' + this.modifyData.generalInformation.proposalNumber + ' go into deviation with SRT or Sub SRT NA, So inetraction could not be generated.',
            }).then(() => {

            })
          } else {
            swal({
              text:
                "Breakin Id could not be generated to this vehicle. Reason: " +
                this.errorMessage + 'and your proposal ' + this.modifyData.generalInformation.proposalNumber + ' go into deviation. Kindly check in IAS tab.',
            }).then(() => {
              if (this.isFromMyBreakIN) {
                this.comparePremium().then(() => {
                  this.saveProposalForPayment();
                })
              } else {
                this.saveProposalForPayment();
              }
            });
          }
        } else {
          swal({
            text:
              "Breakin Id could not be generated to this vehicle. Reason: " +
              this.errorMessage,
          }).then(() => {
          });
        }
      } else {
        if (this.modifyData.isQuoteDeviation) {
          console.log('HIIIII', this.fetchProposalData.length, this.fetchProposalData);
          if (!this.cs.isUndefineORNull(this.fetchProposalData)) {
            if (this.fetchProposalData.length > 1) {
              swal({
                text:
                  "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases and IAS. Your Breakin ID is: " +
                  this.breakinID +
                  " and Your interaction ID id: " + this.iasODRes.srRequestNo + " , " + this.iasIDVRes.srRequestNo + " for srt " + this.iasODReq.SRT + " , " + this.iasIDVReq.SRT + " and process " + this.iasODReq.Process + " , " + this.iasIDVReq.Process + " , if recommended proceed for payment."
              }).then(() => {
                if (this.isFromMyBreakIN) {
                  this.comparePremium().then(() => {
                    this.saveProposalForPayment();
                  })
                } else {
                  this.saveProposalForPayment();
                }
              });

            } else {
              swal({
                text:
                  "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases and IAS. Your Breakin ID is:"
                  + this.breakinID +
                  " and Your interaction ID id: " + this.iasODRes.srRequestNo + " for srt " + this.srt + " and process " + this.iasProcess + ", if recommended proceed for payment."
              }).then(() => {
                if (this.isFromMyBreakIN) {
                  this.comparePremium().then(() => {
                    this.saveProposalForPayment();
                  })
                } else {
                  this.saveProposalForPayment();
                }
              });

            }
          } else {
            swal({
              text:
                "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases, if recommended proceed for payment. Your Breakin ID is: "
                + this.breakinID +
                "but we are unable to raised interaction for same.",
            }).then(() => {
              if (this.isFromMyBreakIN) {
                this.comparePremium().then(() => {
                  this.saveProposalForPayment();
                })
              } else {
                this.saveProposalForPayment();
              }
            });
          }
        } else {
          if (this.breakinStatus == 'Success') {
            // this.comparePremium().then(() => {
            this.saveProposalForPayment().then(() => {
              if (this.saveRNProposalStatus == "Success") {
                if (this.modifyData.isQuoteDeviation) {
                  swal({
                    text:
                      "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases, if recommended proceed for payment. Your  Breakin ID is: " +
                      this.breakinID +
                      " Raised Interaction from below.",
                  }).then(() => { });
                } else {
                  swal({
                    text:
                      "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases, if recommended proceed for payment. Your  Breakin ID is: " +
                      this.breakinID,
                  }).then(() => {
                    // localStorage.setItem('myBreakinPolicy', 'true');
                    // this.changedEventListner.emit(true);
                    this.router.navigateByUrl("/renewals").then(() => {
                      // this.cs.clearLocalStorage();
                      // this.cs.clearQuoteDate();
                      // this.cs.clearPropData();
                      // this.changedEventListner.emit(true);
                      this.router
                        .navigateByUrl("/", { skipLocationChange: true })
                        .then(() => this.router.navigate(["/renewals"]));
                    });
                  });
                }
              } else {
                swal({
                  text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
                });
              }
            });
            // });
          } else {

          }
        }
      }
    });
  }

  createBreakInId(data: any): Promise<any> {
    return new Promise((resolve: any) => {
      let body;
      let vehicleType;
      if (this.modifyPropData.IsNoPrevInsurance == false) {
        this.breakinDays = moment(this.modifyPropData.PolicyStartDate).diff(
          moment(this.modifyPropData.PreviousPolicyEndDate),
          "days");
        console.log('breakinDays', this.breakinDays);
      } else {
        this.breakinDays = 0;
      }
      if (this.productCode == "2312") {
        vehicleType = "MOTORCYCLE";
      } else if (this.productCode == "2311") {
        vehicleType = "PRIVATE CAR";
      }
      // let pType;
      // if (this.policyType == "ROLL") {
      //   pType = "ROLLOVER";
      // } else {
      //   pType = "NEW";
      // }

      /**
       * digitalPOS change
       * get values from localStorage for checking if conditions in html file
       * date :- 29-07-2021
       */
      // if (this.IsPOSTransaction_flag == true) {
      //   this.posDealID = JSON.parse(localStorage.getItem("DealID"));
      //   this.deal_Id = this.posDealID;
      // } else {
      //   this.deal_Id = this.quoteReq.DealId;
      // }
      body = {
        CorrelationId: data.correlationId,
        BreakInType: "Break-in Policy lapse",
        BreakInDays: this.breakinDays,
        CustomerName: this.modifyPropData.CustomerDetails.CustomerName,
        CustomerAddress: this.modifyPropData.CustomerDetails.AddressLine1,
        State: this.pincodeData.stateName,
        City: this.pincodeData.cityName,
        MobileNumber: this.sendDetails.mobileno,
        TypeVehicle: vehicleType,
        VehicleMake: this.modifyPropData.VehicleDetails.ManufactureName,
        VehicleModel: this.modifyPropData.VehicleDetails.VehicleModel,
        ManufactureYear: String(this.modifyPropData.ManufacturingYear),
        RegistrationNo: this.modifyPropData.RegistrationNumber,
        EngineNo: this.modifyPropData.VehicleDetails.EngineNumber,
        ChassisNo: this.modifyPropData.VehicleDetails.ChassisNumber,
        SubLocation: this.breakinDetails.subLocation,
        DistributorInterID: "",
        DistributorName: "Emmet",
        InspectionType: "ROLLOVER",
        DealId: this.modifyPropData.DealID,
      };
      console.log('MM', this.modifyPropData.VehicleDetails.ManufactureName);
      // if (
      //   this.breakinDetails.inspectionMode == "self" ||
      //   this.quoteReq.IsSelfInspection == true
      // ) {
      //   body.SelfInspection = "Yes";
      // }

      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      if (
        this.breakintype.breakin == "selfinspection" ||
        this.breakintype.breakin == "ilinspection"
      ) {
        this.cs.postBiz("/breakin/createbreakinid", str).subscribe(
          (res: any) => {
            this.breakinStatus = res.status;
            if (res.status == "Success") {
              this.saveOfflineBreakinId(res);
              this.cs.loaderStatus = false;
              this.breakinID = res.brkId;
              resolve();
            } else {
              this.breakinID = res.brkId;
              this.errorMessage = res.message;
              this.cs.loaderStatus = false;
              this.cs.loaderStatus = false;
              swal({ text: res.message });
              resolve();
            }
          },
          (err) => {
            resolve();
          }
        );
      } else {
        this.PreBreakINData = JSON.parse(
          localStorage.getItem("PreBreakINData")
        );
        this.saveOfflineBreakinId(this.PreBreakINDataresponse);
      }
    });
  }

  saveOfflineBreakinId(breakinResp: any) {
    let body = breakinResp;

    if (JSON.parse(localStorage.getItem("PreBreakINData"))) {
      body.BreakInType = "Pre-Approved";
      body.brkId = JSON.parse(breakinResp.breakInID);
      body.correlationID = this.modifyData.correlationId;
    } else if (this.modifyData.IsSelfInspection == true) {
      body.BreakInType = "Self Inspection by Customer";
    } else if (this.modifyData.IsSelfInspection == false) {
      body.BreakInType = "IL Inspection";
    }

    // if (this.breakintype.breakin == 'selfinspection') {
    //   body.BreakInType = "Self Inspection by Customer";
    // } if(this.breakintype.breakin == 'ilinspection'){
    //   body.BreakInType = "IL Inspection";
    // }else{
    //   body.BreakInType = "Pre-Approved";
    //   body.brkId = JSON.parse(breakinResp.breakInID);
    //   body.correlationID = this.quoteRes.correlationId;
    // }

    // correlationId
    body.SubLocation = this.subLocation;
    let str = JSON.stringify(body);
    this.cs.loaderStatus = true;
    this.cs.post("BreakIn/UpdateBreakinID", str).then((res: any) => {
      this.cs.loaderStatus = false;
      if (body.BreakInType == "Pre-Approved") {
        swal({
          text:
            "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases, if recommended proceed for payment. Your Breakin ID is: " +
            body.brkId,
        }).then(() => {
          // this.router.navigateByUrl("/quote");
          // localStorage.setItem('myBreakinPolicy', 'true');
          // this.changedEventListner.emit(true);
          this.router.navigateByUrl("/renewals").then(() => {
            this.cs.clearLocalStorage();
            this.cs.clearQuoteDate();
            this.cs.clearPropData();
            // this.changedEventListner.emit(true);
            this.router
              .navigateByUrl("/", { skipLocationChange: true })
              .then(() => this.router.navigate(["/renewals"]));
          });
        });
      } else {
        // this.saveProposalForPayment();
      }
    });
  }

  openBrekinModal() {
    let breakinModal = document.getElementById("renewalModifyBreakin_modal") as HTMLInputElement;
    breakinModal.style.display = 'block';
  }

  checkedCovers(readPropData: any) {
    let PACover = document.getElementById('PAcover') as HTMLInputElement;
    let paCoverWaiver = document.getElementById('paCoverWaiver') as HTMLInputElement;
    let geoCover = document.getElementById('geCover') as HTMLInputElement;
    // let geoCoverTp = document.getElementById('geoCoverTp') as HTMLInputElement;
    let paUNPCover = document.getElementById('pauCover') as HTMLInputElement;
    let biFuelCover = document.getElementById('cpkCover') as HTMLInputElement;
    let paidDriver = document.getElementById('paidDriver') as HTMLInputElement;
    let LPECover = document.getElementById('lpeCover') as HTMLInputElement;
    let VDCover = document.getElementById('vdCover') as HTMLInputElement;
    let ncbProtect = document.getElementById('ncbProtect') as HTMLInputElement;
    let rsaCover = document.getElementById('rsaCover') as HTMLInputElement;
    let gCashCover = document.getElementById('gcCover') as HTMLInputElement;
    let zdCover = document.getElementById('zdCover') as HTMLInputElement;
    let keyProCover = document.getElementById('kpcCover') as HTMLInputElement;
    let lopbCover = document.getElementById('lpbCover') as HTMLInputElement;
    let rtiCover = document.getElementById('rtiCover') as HTMLInputElement;
    let consuCover = document.getElementById('consuCover') as HTMLInputElement;
    let tyreProCover = document.getElementById('tyCover') as HTMLInputElement;
    let engProCover = document.getElementById('eppCover') as HTMLInputElement;
    let emiProCover = document.getElementById('epaCover') as HTMLInputElement;
    let TPPD = document.getElementById('tppdCover') as HTMLInputElement;
    let antTheft = document.getElementById('atdCover') as HTMLInputElement;
    // if (!this.cs.isUndefineORNull(readPropData.riskDetails.paCoverForOwnerDriver)) {
    //   PACover.checked = true;
    // } else {
    //   PACover.checked = false;
    // }
    if (!this.cs.isUndefineORNull(readPropData.riskDetails.paCoverForOwnerDriver)) {
      PACover.checked = true;
      paCoverWaiver.checked = false;

    } else {
      PACover.checked = false;
      paCoverWaiver.checked = true;
    }
    if (!this.cs.isUndefineORNull(readPropData.riskDetails.geographicalExtensionOD || readPropData.riskDetails.geographicalExtensionTP)) {
      geoCover.checked = true;
    } else {
      geoCover.checked = false;
    }
    if (!this.cs.isUndefineORNull(readPropData.riskDetails.paCoverForUnNamedPassenger)) {
      paUNPCover.checked = true;
      this.IsPACoverUnnamedPassenger = true;
      this.PACovervalue = readPropData.SIPACoverUnnamedPassenger * readPropData.SeatingCapacity;
      // this.modifyDataReq.SIPACoverUnnamedPassenger = readPropData.SIPACoverUnnamedPassenger;
      console.log('UnnamedPass', this.PACovervalue);
      this.SIPACoverUnnamedPassenger = readPropData.SIPACoverUnnamedPassenger;

    } else {
      paUNPCover.checked = false;
      this.IsPACoverUnnamedPassenger = false;
    }
    if (this.productCode == '2311' || this.productCode == '2319') {
      if (!this.cs.isUndefineORNull(readPropData.riskDetails.biFuelKitOD || readPropData.riskDetails.biFuelKitTP)) {
        this.IsVehicleHaveLPG = this.IsVehicleHaveCNG = true;
        biFuelCover.checked = true;
        if (readPropData.IsVehicleHaveCNG == true) {
          this.selectedIsVehicleHaveCNGLPG = 'CNG';
          this.selectedSIVehicleHaveLPG_CNG = '1000';
        } else if (readPropData.IsVehicleHaveLPG == true) {
          this.selectedIsVehicleHaveCNGLPG = 'LPG';
          this.selectedSIVehicleHaveLPG_CNG = '1000';
        }
      } else {
        this.IsVehicleHaveLPG = this.IsVehicleHaveCNG = false;
        this.selectedIsVehicleHaveCNGLPG = "";
        biFuelCover.checked = false;
      }
    }
    // if (this.productCode == '2320' || this.productCode == '2319') {
    if (!this.cs.isUndefineORNull(readPropData.riskDetails.paidDriver)) {
      paidDriver.checked = true;
      this.IsLegalLiabilityToPaidDriver = true;
      this.NoOfDriver = readPropData.NoOfDriver == 0 ? 1 : readPropData.NoOfDriver;
    } else {
      paidDriver.checked = false;
      this.IsLegalLiabilityToPaidDriver = false;
      this.NoOfDriver = 0;
    }
    // }
    if (!this.cs.isUndefineORNull(readPropData.riskDetails.employeesOfInsured)) {
      LPECover.checked = true;
      this.IsLegalLiabilityToPaidEmployee = true;
      this.NoOfEmployee = readPropData.NoOfEmployee;
    } else {
      LPECover.checked = false;
      this.IsLegalLiabilityToPaidEmployee = false;
    }
    if (!this.cs.isUndefineORNull(readPropData.riskDetails.voluntaryDiscount)) {
      VDCover.checked = true;
      this.IsVoluntaryDeductible = true;
      this.selectedVoluntaryDeductiblePlanName = readPropData.VoluntaryDeductiblePlanName;
      this.getVoluntaryDeductableVal(this.vehicleType);
    } else {
      VDCover.checked = false;
      this.IsVoluntaryDeductible = false;
    }
    if (!this.cs.isUndefineORNull(readPropData.NCBProtectPlanName)) {
      ncbProtect.checked = true;
      this.NCBProtectPlanName = readPropData.NCBProtectPlanName;
      this.IsNCBProtect = true;
      this.getNoClaimBonus(this.vehicleType);
    } else {
      ncbProtect.checked = false;
      this.IsNCBProtect = false;
    }
    if (!this.cs.isUndefineORNull(readPropData.riskDetails.roadSideAssistance)) {
      rsaCover.checked = true;
      this.isRSA = true;
      this.selectedRSAPlan = readPropData.RSAPlanName;
      this.getRSAData();
    } else {
      rsaCover.checked = false;
      this.isRSA = false;
    }
    if (!this.cs.isUndefineORNull(readPropData.riskDetails.garageCash)) {
      gCashCover.checked = true;
      this.selectedGarageCashPlan = readPropData.GarageCashPlanName;
      this.IsGarageCash = true;
      this.getgarageCashData();
    } else {
      gCashCover.checked = false;
      this.IsGarageCash = false;
    }
    if (!this.cs.isUndefineORNull(readPropData.riskDetails.zeroDepreciation)) {
      zdCover.checked = true;
      this.isZD = true;
      this.selectedZdPlan = readPropData.ZeroDepPlanName;
      this.getZeroDepData();
    } else {
      zdCover.checked = false;
      this.isZD = false;
    }
    if (this.productCode == '2311' || this.productCode == '2319') {
      if (!this.cs.isUndefineORNull(readPropData.riskDetails.keyProtect)) {
        keyProCover.checked = true;
        this.isKeyProtectPlan = true;
        this.selectedKeyProtect = readPropData.KeyProtectPlan;
        this.getKeyProtectData();
      } else {
        keyProCover.checked = false;
        this.isKeyProtectPlan = false;
      }
    }
    if (!this.cs.isUndefineORNull(readPropData.riskDetails.lossOfPersonalBelongings)) {
      lopbCover.checked = true;
      this.LossOfPersonalBelongingPlanName = true;
      this.selectedLossOfPersonalBelongingPlanName = readPropData.LossOfPersonalBelongingPlanName;
      this.getlossofPerData();
    } else {
      lopbCover.checked = false;

    }
    if (!this.cs.isUndefineORNull(readPropData.riskDetails.returnToInvoice)) {
      rtiCover.checked = true;
    } else {
      rtiCover.checked = false;
    }
    if (!this.cs.isUndefineORNull(readPropData.riskDetails.consumables)) {
      consuCover.checked = true;
    } else {
      consuCover.checked = false;
    }
    if (!this.cs.isUndefineORNull(readPropData.riskDetails.engineProtect)) {
      engProCover.checked = true;
    } else {
      engProCover.checked = false;
    }

    if (!this.cs.isUndefineORNull(readPropData.riskDetails.tyreProtect)) {
      tyreProCover.checked = true;
    } else {
      tyreProCover.checked = false;
    }

    if (!this.cs.isUndefineORNull(readPropData.riskDetails.emiProtect)) {
      emiProCover.checked = true;
      this.isEMIProtect = true;
    } else {
      emiProCover.checked = false;
      this.isEMIProtect = false;
    }

    if (!this.cs.isUndefineORNull(readPropData.riskDetails.tppD_Discount)) {
      TPPD.checked = true;
    } else {
      TPPD.checked = false;
    }

    if (!this.cs.isUndefineORNull(readPropData.riskDetails.antiTheftDiscount)) {
      antTheft.checked = true;
    } else {
      antTheft.checked = false;
    }
  }

  getZeroDepData() {
    // this.productCode = JSON.parse(localStorage.getItem('ProductCode'));
    this.createAddonBizToken().then(() => {
      this.cs.getBizAdd("/pfmaster/getzerodepreciationdetails?productcode=" + this.productCode).then((res: any) => {
        console.log('ZD', res);
        this.ZeroDepPlanName = res.zeroDepreciation;
      });
    });
  }

  getNoClaimBonus(vehicle) {
    this.cs
      .getAddons("MotorMaster/GetCovers?CoverName=ncbprotection" + "&ProductName=" + this.vehicleType).then((res: any) => {
        console.log(res);
        this.NCBProtectPlanName = res.ncbProtectPlans;
      })
  }

  getRSAData() {
    this.createRSAAddonBizToken().then(() => {
      this.cs.getBizAdd("/pfmaster/getroadsideassistance?productcode=" + this.productCode).then((res: any) => {
        console.log('RSA', res);
        this.rsaPlanName = res.roadSideAssistance;
      });
    });
  }

  getgarageCashData() {
    this.createGarageCashAddonBizToken().then(() => {
      this.cs
        .getBizAdd1("/pfmaster/fetchmotorgaragecashdetails")
        .then((res: any) => {
          this.GarageCashPlanName = res.motorGarageCashDetails;
        });
    });
  }

  getKeyProtectData() {
    this.createGarageCashAddonBizToken().then(() => {
      this.cs
        .getBizAdd1("/pfmaster/fetchmotorkeyprotectdetails")
        .then((res: any) => {
          this.KeyProtectPlan = res.motorKeyProtectDetails;
        });
    });
  }

  getlossofPerData() {
    this.createGarageCashAddonBizToken().then(() => {
      this.cs
        .getBizAdd1("/pfmaster/fetchmotorpersonalbelongdetails")
        .then((res: any) => {
          this.LossOfPersonalBelongingPlan = res.motorPersonalBelongModel;
        });
    });
  }

  getVoluntaryDeductableVal(vehicle) {
    this.cs
      .getAddons("MotorMaster/Voluntarydeductible?VehicleType=" + this.vehicleType)
      .then((res: any) => {
        this.VoluntaryDeductiblePlanName = res[0].Key;
      });
  }

  isPdfModalOpen() {
    this.cs.isPdfModal = true;
    this.cs.isShowBackButton = false;
    this.readProposalForModifyPdf();
  }

  closeModal1() {
    this.cs.isPdfModal = false;
  }

  closeModal() {
    // this.breakinFlag = !this.breakinFlag;
    this.showBreakInPopUP = false;
  }

  // IDV change by--monika
  getRenewalIDVinput(ev: any) {
    this.lowerLimit = this.modifyPropData.IDVDetails.minidv;
    this.upperLimit = this.modifyPropData.IDVDetails.maxidv
    let y = JSON.parse(ev.target.value);
    let min = JSON.parse(this.lowerLimit);
    let max = JSON.parse(this.upperLimit);
    if (y < min || y > max) {
      this.isIDVChangeMSG = true;
      if (y < min) {
        this.idv = this.lowerLimit;
        // let vehicleAge = this.modifyPropData.IDVDetails.vehicleage;
        let depreciationRate = this.modifyPropData.IDVDetails.idvdepreciationpercent;
        let requiredIDV = this.lowerLimit;
        let newDepreciationRate = 1 - depreciationRate;
        console.log('Vehicle Age', requiredIDV, newDepreciationRate);
        let newExshowRoom = Math.round(parseInt(requiredIDV) / newDepreciationRate);
        console.log('exshowroomprice', newExshowRoom);
        this.exshowroomprice = newExshowRoom;
      } else if (y > max) {
        this.idv = this.upperLimit;
        // let vehicleAge = this.modifyPropData.IDVDetails.vehicleage;
        let depreciationRate = this.modifyPropData.IDVDetails.idvdepreciationpercent;
        let requiredIDV = this.upperLimit;
        let newDepreciationRate = 1 - depreciationRate;
        console.log('Vehicle Age', requiredIDV, newDepreciationRate);
        let newExshowRoom = Math.round(parseInt(requiredIDV) / newDepreciationRate);
        console.log('exshowroomprice', newExshowRoom);
        this.exshowroomprice = newExshowRoom;
      } else {
        this.idv = y;
      }
    } else {
      this.isIDVChangeMSG = false;
      // let vehicleAge = this.modifyPropData.IDVDetails.vehicleage;
      let depreciationRate = this.modifyPropData.IDVDetails.idvdepreciationpercent;
      let requiredIDV = y;
      let newDepreciationRate = 1 - depreciationRate;
      console.log('Vehicle Age', requiredIDV, newDepreciationRate);
      let newExshowRoom = Math.round(parseInt(requiredIDV) / newDepreciationRate);
      console.log('exshowroomprice', newExshowRoom);
      this.exshowroomprice = newExshowRoom;
      this.idv = y;
    }
  }

  getRenewalIDV(ev: any) {
    let x = document.getElementById("sirange") as HTMLInputElement;
    let y = x.value;
    this.lowerLimit = this.modifyPropData.IDVDetails.minidv;
    this.upperLimit = this.modifyPropData.IDVDetails.maxidv;
    this.isIDVChangeMSG = false;
    console.log('range', document.getElementById("sirangevalue"), y, x);
    document.getElementById("sirangevalue").innerHTML = y;
    console.log(y);
    let min = JSON.parse(this.lowerLimit);
    let max = JSON.parse(this.upperLimit);
    // let vehicleAge = this.modifyPropData.IDVDetails.vehicleage;
    let depreciationRate = this.modifyPropData.IDVDetails.idvdepreciationpercent;
    console.log(depreciationRate);
    let requiredIDV = y;
    let newDepreciationRate = 1 - depreciationRate;
    console.log('Vehicle Age', requiredIDV, newDepreciationRate);
    let newExshowRoom = Math.round(parseInt(requiredIDV) / newDepreciationRate);
    console.log('exshowroomprice', newExshowRoom);
    // this.exShowroomPrice = newExshowRoom;
    this.exshowroomprice = newExshowRoom;
    this.idv = y;
  }

}
