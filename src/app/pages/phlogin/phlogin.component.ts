import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { commonData } from "src/app/commonData/commonData";
import { CommonService } from "src/app/services/common.service";
import swal from "sweetalert";

@Component({
  selector: "app-phlogin",
  templateUrl: "./phlogin.component.html",
  styleUrls: ["./phlogin.component.css"],
})
export class PHLoginComponent implements OnInit {
  login = {
    username: "",
    password: "",
  };
  error: any;
  PhToken: any;
  constructor(
    public router: Router,
    public cs: CommonService,
    public activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {}

  logintoPHAdmin() {
    let username = this.cs.encrypt(this.login.username, commonData.aesnysaKey);
    let password = this.cs.encrypt(this.login.password, commonData.aesnysaKey);
    let body = {
      userID: this.cs.encrypt(this.login.username, commonData.aesnysaKey),
      pwd: this.cs.encrypt(this.login.password, commonData.aesnysaKey),
    };
    let str1 = JSON.stringify(body);
    this.cs
      .getWithAuth1("PHAdmin/PHAdminAuthentication", str1)
      .then((res: any) => {
        if (res.token) {
          this.PhToken = res.token;
          localStorage.setItem("phToken", JSON.stringify(this.PhToken));
          localStorage.setItem("phdetails", JSON.stringify(res));
          this.router.navigateByUrl("PHDashboard");
        } else {
          swal({
            text: res,
          });
        }
      })
      .catch((err: any) => {
        swal({
          text: err.error,
        });
      });
  }
}
