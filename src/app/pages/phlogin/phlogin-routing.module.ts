import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PHLoginComponent } from "./phlogin.component";

const routes: Routes = [
  {
    path: "",
    component: PHLoginComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PhloginRoutingModule {}
