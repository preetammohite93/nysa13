import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PhloginRoutingModule } from "./phlogin-routing.module";
import { PHLoginComponent } from "./phlogin.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [PHLoginComponent],
  imports: [CommonModule, PhloginRoutingModule, SharedModule],
})
export class PhloginModule {}
