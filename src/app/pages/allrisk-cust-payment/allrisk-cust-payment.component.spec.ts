import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllriskCustPaymentComponent } from './allrisk-cust-payment.component';

describe('AllriskCustPaymentComponent', () => {
  let component: AllriskCustPaymentComponent;
  let fixture: ComponentFixture<AllriskCustPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllriskCustPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllriskCustPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
