import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { CommonService } from "src/app/services/common.service";
import { ActivatedRoute, Router } from "@angular/router";
import swal from "sweetalert";
import * as moment from "moment";
import { ApiServiceService } from "src/app/services/api-service.service";
import { commonData } from "src/app/commonData/commonData";
declare var $: any;

@Component({
  selector: "app-payment-confirmation",
  templateUrl: "./payment-confirmation.component.html",
  styleUrls: ["./payment-confirmation.component.css"],
})
export class PaymentConfirmationComponent implements OnInit {

  quoteReq: any;
  quoteRes: any;
  propReq: any;
  propRes: any;
  newPolicyNumber: any;
  newProposalNumber: any;
  discount: any;
  finalamount: any;
  ordReq: any;
  ordRes: any;
  PFPayReq: any;
  PFPayRes: any;
  totalTax: any;
  ImId: any;
  pdfResponse: any;
  auth: any;
  paramDataValues: any;
  authtok: any;
  policyType: any;
  productCode: any;
  hospiFundDealID: any;
  type: any;
  product: any;
  finalPremium: any;
  PolicyStartDate: any;
  PolicyEndDate: any;
  policySubType: any;
  feedBackResponse: any;
  addPolicyDetails: any;
  updatePaymentDetails: any;
  podetails: any;
  paydetails: any;
  timeTaken: any;
  starRatingFeed: any;
  timeseconds: any;
  checkInsta: any;
  CoverSI: any;
  coverblock: any;

  isMultiProposal: boolean = false;
  multipleProposal: any;
  proposalNos: any;
  transactionId: any;
  isSiteDown: any;
  // All Risk - Sejal - 5-4-2022
  orderDataPlutus: any ;
  proposalNumber: any


  constructor(
    public router: Router,
    public cs: CommonService,
    public api: ApiServiceService,
    private location: Location,
    public activeRoute: ActivatedRoute
  ) {
    // this.router.onSameUrlNavigation='reload'
    let dateStart = JSON.parse(localStorage.getItem("AppStartTime"));

    // let period = moment.utc(moment(dateStart).diff(moment())).format("HH:mm:ss");

    let date2 = new Date();

    let timeDiff = date2.getTime() - dateStart;
    this.convertTime(timeDiff);
  }

  ngOnInit() {
    this.getParams();
    $("#smileys input").on("click", function () {
      $("#result").html($(this).val());
    });

    $("aside").removeClass("active");

    window.scrollTo(0, 0);

    (<any>window).ga("send", "event", {
      eventCategory: "Payment Confirmation",
      eventLabel: "" + this.product + "" + this.policyType,
      eventAction: "Policy issued",
      eventValue: 10,
    });

    this.starRatingFeed = "5";

    localStorage.removeItem("DisablecustLink");



    // this.bnIdle.startWatching(600).subscribe((isTimedOut: boolean) => {
    //   if (isTimedOut) {
    //     swal({
    //
    //       text: 'Your session has ended as you were idle'
    //     }).then(()=> {
    //       this.goToDashboard();
    //       this.cs.clearLocalStorage();
    //     });
    //   }
    // });

    // let localAuth = JSON.parse(localStorage.getItem('AuthToken'));
    // if (!this.cs.isUndefineORNull(localAuth)) {
    //   if (this.cs.checkTokenValidity()) {
    //     this.cs.geToLogin();
    //   } else {
    //   }
    // } else {
    // }

    localStorage.removeItem("dataforBack");

    this.checkInsta = JSON.parse(localStorage.getItem("instaPayment"));

    this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
    this.authtok = JSON.parse(localStorage.getItem("AuthToken"));
    this.policyType = this.paramDataValues.iPartnerLogin.policy;
    this.productCode = this.paramDataValues.iPartnerLogin.product;
    if (this.policyType == "NEW") {
      this.type = "New";
    } else {
      this.type = "Rollover";
    }
    if (this.productCode == "2312") {
      this.product = "Two Wheeler";
    } else {
      this.product = "Four Wheeler";
    }

    this.newPolicyNumber = JSON.parse(localStorage.getItem("newPolicyNumber"));
    this.newProposalNumber = JSON.parse(
      localStorage.getItem("newProposalNumber")
    );
    // this.quoteReq = JSON.parse(localStorage.getItem('calQuoteReq'));
    // this.quoteRes = JSON.parse(localStorage.getItem('calQuoteRes'));
    // if (JSON.parse(localStorage.getItem('calQuoteReq'))) {
    //   this.quoteReq = JSON.parse(localStorage.getItem('calQuoteReq'));
    //   this.quoteRes = JSON.parse(localStorage.getItem('calQuoteRes'));
    // } else {
    //   this.quoteReq = JSON.parse(localStorage.getItem('calQuoteTPReq'));
    //   this.quoteRes = JSON.parse(localStorage.getItem('calQuoteTPRes'));
    // }

    // this.PolicyStartDate = moment(this.quoteReq.PolicyStartDate).format('DD-MM-YYYY');
    // this.PolicyEndDate = moment(this.quoteReq.PolicyEndDate).format('DD-MM-YYYY');
    // this.ordReq = JSON.parse(localStorage.getItem('orderReq'));
    // this.ordRes = JSON.parse(localStorage.getItem('orderRes'));
    this.PFPayReq = JSON.parse(localStorage.getItem("PFReq"));
    this.PFPayRes = JSON.parse(localStorage.getItem("PFRes"));
    this.auth = JSON.parse(localStorage.getItem("AuthToken"));
    this.orderDataPlutus = JSON.parse(localStorage.getItem("orderdataPlutus"));


    if (localStorage.getItem("multipleProposal")) {
      this.isMultiProposal = true;
      this.multipleProposal = JSON.parse(
        localStorage.getItem("multipleProposal")
      );
      this.proposalNos = this.multipleProposal.map((obj) => obj.ProposalNo);
      this.finalPremium = JSON.parse(localStorage.getItem("multiTotalAmount"));
      this.transactionId = localStorage.getItem("transactionId");
    } else if (localStorage.getItem("allriskmultipleProposal")) {
      this.isMultiProposal = true;
      this.multipleProposal = JSON.parse(
        localStorage.getItem("allriskmultipleProposal")
      );
      this.proposalNos = this.multipleProposal.map((obj) => obj.ProposalNo);
      if (!this.cs.isUndefineORNull(localStorage.getItem("transactionId"))) {
        this.transactionId = localStorage.getItem("transactionId");
      } else {
        this.orderDataPlutus = JSON.parse(localStorage.getItem("orderdataPlutus"));
        this.transactionId = this.orderDataPlutus.TransactionID;
      }

      if (localStorage.getItem("allriskmultiTotalAmount") != null) {
        this.finalPremium = JSON.parse(localStorage.getItem("allriskmultiTotalAmount"));
      } else {
        this.finalPremium = this.orderDataPlutus.Amount;
      }

    } else {
      // All Risk - Sejal - 5-4-2022
      if (this.productCode == commonData.allriskProductCode) {
        this.propReq = JSON.parse(localStorage.getItem("EVPropDatareq"));
        this.propRes = JSON.parse(localStorage.getItem("EVPropDatares"));
        this.orderDataPlutus = JSON.parse(localStorage.getItem("orderdataPlutus"));
        this.finalPremium = Math.round(this.propRes.gstserviceresponse.gstservicedetails.finalamount);
        this.transactionId = this.orderDataPlutus.TransactionID;
        this.proposalNumber = this.propRes.proposalNumber;
      } else {
        this.propReq = JSON.parse(localStorage.getItem("PropDatareq"));
        this.propRes = JSON.parse(localStorage.getItem("PropDatares"));
        this.orderDataPlutus = JSON.parse(localStorage.getItem("orderdataPlutus"));
        this.finalPremium = Math.round(this.propRes.finalPremium);
        this.transactionId = this.orderDataPlutus.TransactionID;
        this.proposalNumber = this.propRes.generalInformation.proposalNumber;
      }
    }

    // this.podetails = JSON.parse(localStorage.getItem('addPolicyDetails'));
    // this.paydetails = JSON.parse(localStorage.getItem('updatePaymentDetails'));

    // this.discount = Math.round(this.propRes.specialDiscount);
    // this.finalamount = Math.round(this.propRes.finalPremium);
    // this.totalTax = Math.round(this.propRes.totalTax);
    if (this.isMultiProposal == false) {
      this.starRating("0");
      if (this.checkInsta != true) {
        this.sendEmailMob();
      }
    }

    this.getHospiDeal();

    if (this.productCode == "2311") {
      this.CoverSI = 3000;
    } else if (this.productCode == "2312") {
      this.CoverSI = 1500;
    }
    this.coverblock = [
      { CoverId: 1, CoverSI: this.CoverSI, Column1: 5 },
      { CoverId: 2, CoverSI: this.CoverSI, Column1: 5 },
      { CoverId: 3, CoverSI: this.CoverSI, Column1: 5 },
      { CoverId: 5, CoverSI: this.CoverSI, Column1: 5 },
      { CoverId: 6, CoverSI: this.CoverSI, Column1: 5 },
    ];
  }

  getParams(): Promise<any> {
    return new Promise((resolve: any) => {
      this.activeRoute.queryParams.forEach((params) => {
        this.proposalNos = params.proposalNo;
        resolve();
      });
    });
  }

  // Check crawler enable or not
  checkCrawler() {
    // //CreateToken + GetApplicationAccess merging code - Sejal
    if (!this.cs.isUndefineORNull(localStorage.getItem("AuthToken"))) {
      let encryptedTokenaccess = JSON.parse(localStorage.getItem("AuthToken"));
      let Parsedtokenaccess: any = this.api.decrypt(
        encryptedTokenaccess.applicationAccess
      );
      let tokenaccess = JSON.parse(Parsedtokenaccess);
      this.cs.isPlutusEnable = tokenaccess.isEnablePlutus;
      this.isSiteDown = tokenaccess.isSiteDown;
    }

    // this.cs.get('Access/GetApplicationAccess').subscribe((res: any) => {
    //   console.log('Response', res);
    //   localStorage.setItem('AccessFor', JSON.stringify(res));
    //   this.isSiteDown = res.isSiteDown;
    // })
  }

  // Go to Dashboard
  goToDashboard() {
    this.starRating("0");
    this.cs.geToDashboard();
  }

  goBack() {
    if (this.isMultiProposal == false) {
      this.starRating("0");
    }
    this.router.navigateByUrl("quote");
    this.cs.clearLocalStorage();
    this.cs.clearQuoteDate();
    this.cs.clearPropData();
    localStorage.setItem("myNysaPolicy", "true");
  }

  starRating(ev: any) {
    this.starRatingFeed = ev;
    let breakinFlag: any = localStorage.getItem("breakinFlag");
    if (breakinFlag == "true") {
      let breakinData: any = JSON.parse(localStorage.getItem("breakinData"));
      this.policySubType = breakinData.policySubType;
    } else {
      if (this.productCode == "2312") {
        this.policySubType = 1;
      } else if (this.productCode == "2311") {
        this.policySubType = 2;
      } else if (this.productCode == "2320") {
        this.policySubType = 9;
      } else if (this.productCode == "2319") {
        this.policySubType = 10;
      }
    }

    let body = {
      PolicySubType: JSON.stringify(this.policySubType), // mandatory
      ProposalNumber: this.proposalNos, //this.propRes.generalInformation.proposalNumber, // mandatory
      StarRating: JSON.parse(this.starRatingFeed), // mandatory
      PolicyCreatedIn: this.timeTaken,
      Feedback: "", // non mandatory , do not show on screen
      PolicyCreatedInSecond: JSON.stringify(this.timeseconds),
    };
        // if (this.productCode == commonData.allriskProductCode) {
    //   body.ProposalNumber = this.propRes.proposalNumber; // mandatory
    // } else {
    //   body.ProposalNumber = this.propRes.generalInformation.proposalNumber; // mandatory
    // }

    let str = JSON.stringify(body);
    this.cs.post("Feedback/GetUserFeedback", str).then((res: any) => {
      this.feedBackResponse = res;

      (<any>window).ga("send", "event", {
        eventCategory: "Policy Punched",
        eventLabel:
          "" + this.product + "" + this.policyType + "" + this.timeseconds,
        eventAction: "Policy Punched",
        eventValue: 10,
      });
    });
  }

  //time convert
  convertTime(timeDiff) {
    var msec = timeDiff;
    this.timeseconds = Math.round(msec / 1000);

    var hh = Math.floor(msec / 1000 / 60 / 60);
    msec -= hh * 1000 * 60 * 60;
    var mm = Math.floor(msec / 1000 / 60);
    msec -= mm * 1000 * 60;
    var ss = Math.floor(msec / 1000);
    msec -= ss * 1000;

    if (hh == 0) {
      this.timeTaken = mm + " Minutes " + ss + " Seconds ";
    } else {
      this.timeTaken = hh + " Hours " + mm + " Minutes " + ss + " Seconds ";
    }
    let clock = setInterval(() => {
      clearInterval(clock);
      clock = null;
      if (mm >= 5) {
        document.getElementById("timeTaken").style.color = "red";
      } else if (mm < 2) {
        document.getElementById("timeTaken").style.color = "green";
      } else {
        document.getElementById("timeTaken").style.color = "orange";
      }
    }, 200);
  }

  // Download PDF Policy
  showPolicy() {
    this.cs.loaderStatus = true;
    this.cs
      .get(
        "PolicySchedule/GeneratePolicySchedule?policyNo=" +
          this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].policyNo
      )
      .subscribe(
        (res) => {
          this.pdfResponse = res;
          if (
            this.pdfResponse != null &&
            this.pdfResponse != undefined &&
            this.pdfResponse.status == "SUCCESS"
          ) {
            var fileName =
              this.PFPayRes.paymentTagResponse.paymentTagResponseList[0].policyNo
                .split("/")
                .join("_") + ".pdf";
            this.cs.save(
              "data:application/pdf;base64," + this.pdfResponse.buffer,
              fileName
            );
            this.cs.loaderStatus = false;
          } else {
            swal({
              text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
            });
          }
          this.cs.loaderStatus = false;
        },
        (err) => {
          swal({
            text: err.error.ExceptionMessage,
          });
          this.cs.loaderStatus = false;
          document.getElementById("myNav4").style.height = "0%";
        }
      );
  }

  // Hospifund Code

  // getHospifundDeal
  getHospiDeal() {
    this.ImId = this.authtok.username;
    this.api.getHospi(this.ImId).subscribe((res: any) => {
      this.hospiFundDealID = res.hospiFundDealID;
    });
  }

  sendEmailMob() {
    let body = {
      ProposalNumber: this.proposalNos, //this.propRes.generalInformation.proposalNumber,
      TotalPremium: JSON.stringify(this.propRes.finalPremium),
      TransactionID: this.PFPayReq.PaymentEntry.onlineDAEntry.AuthCode,
      SMSSendFor: "Payment", // default Value
      MessageType: "text", // default Value
      CorrelationID: this.propRes.correlationId,
      PolicyType: "Motor",
      PolicyStartDate: this.propReq.PolicyStartDate,
      PolicyEndDate: this.propReq.PolicyEndDate,
    };
    if (this.productCode == commonData.allriskProductCode) {
      //body.ProposalNumber = this.propRes.proposalNumber;
      body.TotalPremium = JSON.stringify(Math.round(this.propRes.gstserviceresponse.gstservicedetails.finalamount));
    } else {
      //body.ProposalNumber = this.propRes.generalInformation.proposalNumber;
      body.TotalPremium = JSON.stringify(this.propRes.finalPremium);
    }

    this.api.sendmailsms(body).then((res: any) => {});
  }

  checkplans(ev: any) {
    if (ev == "sltPlan_plan1") {
      if (this.productCode == "2311") {
        this.CoverSI = 2000;
      } else if (this.productCode == "2312") {
        this.CoverSI = 500;
      }
      this.coverblock = [
        { CoverId: 1, CoverSI: this.CoverSI, Column1: 5 },
        { CoverId: 2, CoverSI: this.CoverSI, Column1: 5 },
        { CoverId: 3, CoverSI: this.CoverSI, Column1: 5 },
      ];
    } else if (ev == "sltPlan_plan2") {
      if (this.productCode == "2311") {
        this.CoverSI = 2500;
      } else if (this.productCode == "2312") {
        this.CoverSI = 1000;
      }
      this.coverblock = [
        { CoverId: 1, CoverSI: this.CoverSI, Column1: 5 },
        { CoverId: 2, CoverSI: this.CoverSI, Column1: 5 },
        { CoverId: 3, CoverSI: this.CoverSI, Column1: 5 },
        { CoverId: 4, CoverSI: this.CoverSI, Column1: 5 },
      ];
    } else if (ev == "sltPlan_plan3") {
      if (this.productCode == "2311") {
        this.CoverSI = 3000;
      } else if (this.productCode == "2312") {
        this.CoverSI = 1500;
      }
      this.coverblock = [
        { CoverId: 1, CoverSI: this.CoverSI, Column1: 5 },
        { CoverId: 2, CoverSI: this.CoverSI, Column1: 5 },
        { CoverId: 3, CoverSI: this.CoverSI, Column1: 5 },
        { CoverId: 5, CoverSI: this.CoverSI, Column1: 5 },
        { CoverId: 6, CoverSI: this.CoverSI, Column1: 5 },
      ];
    }
  }

  saveHF() {
    let customerDetails = JSON.parse(localStorage.getItem("CustomerDetails"));
    let body = {
      DealId: this.hospiFundDealID,
      CorrelationID: this.propRes.correlationId,
      ImId: this.ImId,
      CustStateName: customerDetails.state,
      DealStateName: "",
      InsuredName: customerDetails.CustomerName,
      InsuredDob: "",
      InsuredGender: "",
      InsuredRelation: "",
      PfCustomerID: this.propRes.generalInformation.customerId,
      CustomerName: customerDetails.CustomerName,
      CustomerDob: null,
      CustomerGender: null,
      CustomerPanNo: customerDetails.panCardNo,
      CustomerMobileNo: customerDetails.CustomerMobile,
      CustomerEmail: customerDetails.Email,
      CustomerAddress: customerDetails.Address,
      CustomerStateCode: this.propReq.CustomerDetails.StateCode,
      CustomerCityCode: this.propReq.CustomerDetails.CityCode,
      Module: "NAYSA",
      BasicPremium: this.propRes.packagePremium,
      TotalTax: this.propRes.totalTax,
      TotalPremium: this.propRes.finalPremium,
      cvrDetails: this.coverblock,
    };

    this.api.savehf(body).then((res: any) => {
      window.location.href = commonData.hospiURL + res.token;
    });
  }
}
