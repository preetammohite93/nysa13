import { Component, OnInit } from "@angular/core";
import swal from "sweetalert";
import { CommonService } from "src/app/services/common.service";
import * as moment from "moment";
import { Router } from "@angular/router";
import { ApiServiceService } from "src/app/services/api-service.service";
import { CommonMessageService } from "src/app/services/common-message.service";
import { commonData } from "../../commonData/commonData";

@Component({
  selector: "app-mybreakins",
  templateUrl: "./mybreakins.component.html",
  styleUrls: ["./mybreakins.component.css"],
})
export class MybreakinsComponent implements OnInit {
  ProductType: any;
  term: any;
  page: any;
  memmaxDOB: any;
  startDate1: any;
  startDate: any;
  mybreakinData: any;
  norecordflag: boolean = false;
  paramDataValues: any;
  proposalRQ: any;
  proposalRS: any;
  quoteRQ: any;
  deal_Id: any;
  quoteRS: any;
  spinloader: boolean = false;
  IsPOSTransaction_flag: boolean = false;
  sendDetails = {
    Email: "",
    mobileno: "",
  };
  IsPayLinkSent: any;
  PayLinkReciever: any;
  isSMSSent: any;
  CustomerPayLink: any;
  updatePayment: any;
  custLink: any;
  moberrorFlag: boolean = false;
  mobNumberPattern = "^(0/91)?[6-9][0-9]{9}$";
  // GetDealFromIM? changes - Sejal - 01-02-2022
  authtoken: any;
  dealDetails: any;
  dealDetailsfromIM: any;
  EmpId: any;
  EmpName: any;
  productCode = "2312";
  policySubType = "1";
  classCode = "37";
  posDealID: any;
  dataForCustLink: any;

  constructor(
    private cs: CommonService,
    private router: Router,
    public api: ApiServiceService,
    public msgService: CommonMessageService
  ) {
    if (localStorage.getItem("IsPOSTransaction")) {
      this.IsPOSTransaction_flag = JSON.parse(
        localStorage.getItem("IsPOSTransaction")
      );
    }
  }

  myBreakinDetails = {
    FromDate: "",
    ToDate: "",
  };

  ngOnInit() {
    this.ProductType = "1";
    this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
    this.startDate = moment(new Date()).format("DD/MM/YYYY");
    this.startDate1 = moment(this.startDate, "DD/MM/YYYY").format("MM/DD/YYYY");
    // GetDealFromIM? changes - Sejal - 01-02-2022
    this.authtoken = JSON.parse(localStorage.getItem("AuthToken"));
    this.productCode = this.paramDataValues.iPartnerLogin.product;
    // this.memmaxDOB = new Date();
    // this.memmaxDOB.setDate(this.memmaxDOB.getDate() + 0);
  }

  // validate()
  // {
  //   if(this.myBreakinDetails.FromDate == null || this.myBreakinDetails.FromDate == undefined || this.myBreakinDetails.FromDate == "")
  //   { swal({  text: 'Kindly insert from date' });}
  //   else if(this.myBreakinDetails.ToDate == null || this.myBreakinDetails.ToDate == undefined || this.myBreakinDetails.ToDate == "")
  //   { swal({  text: 'Kindly insert to date' });}
  //   else
  //   { this.getMyBreakins(); }
  // }

  vehicletype(ev: any) {
    if (ev.target.id == "breakselectVehicle_twoWheeler") {
      this.ProductType = "1";
      this.mybreakinData = "";
      this.productCode = "2312";
      this.policySubType = "1";
      this.classCode = "37";
      if(!this.IsPOSTransaction_flag){
        this.getDeal();
      }      
    } else if (ev.target.id == "breakselectVehicle_fourWheeler") {
      this.ProductType = "2";
      this.mybreakinData = "";
      this.productCode = "2311";
      this.policySubType = "2";
      this.classCode = "45";
      if(!this.IsPOSTransaction_flag){
        this.getDeal();
      }
    }

    // GetDealFromIM? changes - Sejal - 01-02-2022
    // let data: any;
    // if (!this.cs.isUndefineORNull(localStorage.getItem("DealID"))) {
    //   let demo = JSON.parse(localStorage.getItem("DealID"));
    //   data = demo.split("/");
    //   if (
    //     data[0] == "DL-3001" &&
    //     ev.target.id == "breakselectVehicle_twoWheeler"
    //   ) {
    //     this.productCode = "2312";
    //     this.policySubType = "1";
    //     this.classCode = "37";
    //     this.getDeal().then((res: any) => {
    //       this.getDealDetails();
    //     });
    //   } else if (
    //     data[0] == "DL-3005" &&
    //     ev.target.id == "breakselectVehicle_fourWheeler"
    //   ) {
    //     this.productCode = "2311";
    //     this.policySubType = "2";
    //     this.classCode = "45";
    //     this.getDeal().then((res: any) => {
    //       this.getDealDetails();
    //     });
    //   }
    // }
  }

  getDeal(): Promise<void> {
    return new Promise((resolve) => {
      this.api
        .getDeal(this.authtoken.username, this.productCode)
        .subscribe((res: any) => {
          this.dealDetails = res;
          //localStorage.setItem("DealID", JSON.stringify(this.dealDetails));
          let dealid = this.dealDetails.dealID;
          localStorage.setItem("DealID", JSON.stringify(dealid));
          resolve();
        });
    });
  }

  // getDealDetails() {
  //   this.api.getDealDetails(this.dealDetails.dealID).subscribe((res: any) => {
  //     if (res.status == "FAILED") {
  //       swal({ text: "Deal Details not found" });
  //       this.cs.loaderStatus = false;
  //     } else {
  //       this.dealDetailsfromIM = res;
  //       this.EmpId = this.dealDetailsfromIM.hR_REF_NO;
  //       this.EmpName = this.dealDetailsfromIM.employeeName;
  //       this.cs.loaderStatus = false;
  //     }
  //   });
  // }

  mobileValidate() {
    let isNumberValidate = this.cs.mobileValidation(this.sendDetails.mobileno);
    if (isNumberValidate && this.sendDetails.mobileno.length == 10) {
      this.sendDetails.mobileno = this.sendDetails.mobileno;
      this.moberrorFlag = false;
    } else {
      // this.sendDetails.mobileno = "";
      this.moberrorFlag = true;
    }
  }

  getDataForBrekinLink(data: any) {
    console.log(data);
    this.dataForCustLink = data;
  }

  getMyBreakins() {
    this.spinloader = true;
    let body;
    body = {
      SubProductType: this.ProductType,
    };
    if (
      this.paramDataValues.iPartnerLogin.isSubagent != null ||
      this.paramDataValues.iPartnerLogin.isSubagent != undefined
    ) {
      body.IsSubagent = true;
      body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
    }
    let str = JSON.stringify(body);
    this.cs.post("BreakIn/MyBreakinCases", str).then((res: any) => {
      if (res.status == null) {
        this.norecordflag = true;
      } else {
        this.norecordflag = false;
        this.mybreakinData = res.breakInDetails;
      }
      this.spinloader = false;
      // this.cs.loaderStatus = false;
    });
  }

  convertBreakin(data) {
    let propRes; let quoteRes; let date1; let transOnDate:any;
    if (data.transactionType == 'Renewal') {
      let RQ = JSON.parse(data.proposalRQ);
      let RQ1 = JSON.parse(RQ);
      let RS = propRes = JSON.parse(data.proposalRS);
      this.proposalRQ = RQ1;
      this.proposalRS = propRes = JSON.parse(RS);
      this.quoteRQ = RQ1;
      this.quoteRS = quoteRes = JSON.parse(RS);
      transOnDate = data.transOn;
    } else {
      this.proposalRQ = JSON.parse(data.proposalRQ);
      this.proposalRS = propRes = JSON.parse(data.proposalRS);
      this.quoteRQ = JSON.parse(data.quoteRQ);
      this.quoteRS = quoteRes = JSON.parse(data.quoteRS);
      transOnDate = data.transOn;
    }
    if (
      (this.quoteRQ.BusinessType == "Roll Over" || data.transactionType == 'Renewal') &&
      this.quoteRQ.PreviousPolicyDetails == null
    ) {
      this.quoteRQ.isNoPrevInsurance = true;
    }

    if (data.tpPolicyNo) {
      this.quoteRQ.TPStartDate = moment(data.tpStartDate).format("YYYY-MM-DD");
      this.quoteRQ.TPEndDate = moment(data.tpEndDate).format("YYYY-MM-DD");
      this.quoteRQ.TPPolicyNo = data.tpPolicyNo;
      this.quoteRQ.TPInsurerName = data.tpInsurerName;
    }
    let currentDate = moment().format("YYYY-MM-DD");

    if (data.transactionType == 'Renewal') {
      date1 = moment(
        transOnDate,
        "MM/DD/YYYY"
      ).format("YYYY-MM-DD");
    } else {
      date1 = moment(
        transOnDate,
        "MM/DD/YYYY"
      ).format("YYYY-MM-DD");
    }

    let proposalDate = moment(currentDate).diff(date1, "days");
    localStorage.setItem("breakinFlag", "true");
    localStorage.setItem("breakinData", JSON.stringify(data));
    localStorage.setItem("bankType", data.bankType);

    let isInsta;
    if (data.bankType == "INT") {
      isInsta = 1;
    } else {
      isInsta = 0;
    }

    if (proposalDate == 0 && data.isPayEligible) {
      localStorage.setItem("PropDatareq", JSON.stringify(this.proposalRQ));
      localStorage.setItem("PropDatares", JSON.stringify(propRes));
      localStorage.setItem("calQuoteReq", JSON.stringify(this.quoteRQ));
      localStorage.setItem("calQuoteRes", JSON.stringify(quoteRes));
      this.breakinInspectionClear().then(() => {
        if (data.transactionType == 'Renewal') {
          this.cs.redirectToPlutusBreakin(this.proposalRQ, propRes, isInsta, 0, false, 0);
        } else {
          this.cs.redirectToPlutus(this.proposalRQ, propRes, isInsta, 0, false, 0);
        }
      });
    } else {
      if (data.transactionType == 'Renewal') {
        localStorage.setItem("renewBreakInData", JSON.stringify(data));
        this.router.navigateByUrl("renewalmodifyproposal");
      } else {
        localStorage.setItem("PropDatareq", JSON.stringify(this.proposalRQ));
        // localStorage.setItem('PropDatares',JSON.stringify(propRes));
        localStorage.setItem("calQuoteReq", JSON.stringify(this.quoteRQ));
        localStorage.setItem("calQuoteRes", JSON.stringify(quoteRes));
        this.router.navigateByUrl("proposal");
      }

    }
  }

  breakinInspectionClear(): Promise<any> {
    return new Promise((resolve: any) => {
      let breakindata = JSON.parse(localStorage.getItem("breakinData"));
      let dealId = JSON.parse(localStorage.getItem("DealID"));
      let startDate = moment(new Date()).format("DD-MM-YYYY");

      /**
       * digitalPOS change
       * get values from localStorage for checking if conditions in html file
       * date :- 29-07-2021
       */
      if (this.IsPOSTransaction_flag == true) {
        this.posDealID = JSON.parse(localStorage.getItem("DealID"));
        this.deal_Id = this.posDealID;
      } else {
        this.deal_Id = this.quoteRQ.DealId;
      }
      let body = {
        InspectionId: breakindata.transactionType == 'Renewal' ? breakindata.inspectionID : JSON.parse(breakindata.inspectionID),
        DealNo: this.deal_Id,
        ReferenceDate: startDate, //this.propRes.generalInformation.proposalDate,
        InspectionStatus: "OK",
        CorrelationId: breakindata.corelationID,
        ReferenceNo: this.proposalRS.generalInformation.proposalNumber,
      };
      let str = JSON.stringify(body);
      this.cs.postBiz("/breakin/clearinspectionstatus", str).subscribe(
        (res: any) => {
          resolve();
        },
        (err) => {
          // swal({ closeOnClickOutside: false, text: err });
          let errMsg = this.msgService.errorMsg(err)
          swal({ closeOnClickOutside: false, text: errMsg });
        }
      );
    });
  }

  createBreakIN() {
    this.router.navigateByUrl("createBreakIN");
  }

  breakINDash() {
    this.router.navigateByUrl("breakINDashboard");
  }

  sendPaymentLinkforBoth() {
    if (this.dataForCustLink.transactionType == 'Renewal') {
      this.sendPaymentLinkRenew();
    } else {
      this.sendPaymentLink();
    }
  }

  sendPaymentLink() {
    this.proposalRQ = JSON.parse(this.dataForCustLink.proposalRQ);
    this.proposalRS = JSON.parse(this.dataForCustLink.proposalRS);
    this.quoteRQ = JSON.parse(this.dataForCustLink.quoteRQ);
    this.quoteRS = JSON.parse(this.dataForCustLink.quoteRS);

    let body = {
      CorrelationID: this.proposalRQ.CorrelationId,
      ProposalNo: this.proposalRS.generalInformation.proposalNumber,
      EmailID: this.cs.encrypt(this.sendDetails.Email, commonData.aesnysaKey),
      MobileNo: this.cs.encrypt(this.sendDetails.mobileno, commonData.aesnysaKey),
    };
    let url = "CustomerPayment/getLink";
    let str = JSON.stringify(body);
    let status = "N";
    let smsStatus = "N";
    this.cs.loaderStatus = true;

    this.cs.post(url, str).then((res: any) => {
      const emailStatus = res.isLinkSent ? true : false;
      const mobileStatus = res.isSMSSent ? true : false;
      const checkBoth = emailStatus && mobileStatus ? true : false;
      this.paymentStatusDetails(status, smsStatus, res, this.sendDetails.Email);
      let msg;
      let maskedEmail = this.sendDetails.Email.replace(
        /(.{2})(.*)(?=@)/,
        function (gp1, gp2, gp3) {
          for (let i = 0; i < gp3.length; i++) {
            gp2 += "X";
          }
          return gp2;
        }
      );
      let maskedMobile =
        this.sendDetails.mobileno.slice(0, 2) + this.sendDetails.mobileno.slice(2).replace(/.(?=...)/g, "X");
      if (checkBoth) {
        msg = `Payment link has been sent to ${maskedEmail} and ${maskedMobile}.`;
        status = "Y";
        smsStatus = "Y";
      } else {
        if (emailStatus) {
          msg = `Payment link has been sent to ${maskedEmail}.`;
          status = "Y";
          smsStatus = "N";
        } else if (mobileStatus) {
          msg = `Payment link has been sent to ${maskedMobile}.`;
          status = "N";
          smsStatus = "Y";
        } else if (!checkBoth) {
          msg = `Failed to send payment link to ${maskedEmail} and ${maskedMobile}.`;
          status = "N";
          smsStatus = "N";
        }
      }
      swal({ text: msg }).then(() => {
        this.cs.loaderStatus = false;
      });
    });
  }

  sendPaymentLinkRenew() {
    let RQ = JSON.parse(this.dataForCustLink.proposalRQ);
    let RQ1 = JSON.parse(RQ);
    let RS = JSON.parse(this.dataForCustLink.proposalRS);
    this.proposalRQ = RQ1;
    this.proposalRS = JSON.parse(RS);
    this.quoteRQ = RQ1;
    this.quoteRS = JSON.parse(RS);
    console.log(this.quoteRQ, this.quoteRS, this.dataForCustLink, RQ1);
    let regNO = this.quoteRS.reqPacket ? this.quoteRS.reqPacket.ProposalDetails.RegistrationNumber : this.quoteRQ.ProposalDetails.RegistrationNumber;
    let polNO = RQ1.PolicyNumber ? RQ1.PolicyNumber : RQ1.policyNo;
    let body = {
      propNo: this.quoteRS.generalInformation.proposalNumber,
      policyNo: polNO,
      phoneNo: this.sendDetails.mobileno,
      emailId: this.sendDetails.Email,
      premium: this.quoteRS.finalPremium,
      regNo: regNO,
    };
    let str = JSON.stringify(body);
    this.cs.postForRenewals("sendPaymentLink", str).then((res: any) => {
      console.log(res);
      if (res.emailStatus) {
        swal({ text: "Payment link has been sent successfully." });
      } else {
        swal({ text: "Failed to send link." });
      }
    });
  }

  paymentStatusDetails(emailStatus, smsStatus, res, email) {
    this.IsPayLinkSent = emailStatus;
    this.PayLinkReciever = email;
    this.isSMSSent = smsStatus;
    this.CustomerPayLink = res.paymentLink;
    this.custLink = res;
    this.updatePaymentStatuscustlink().then(() => {
      this.updatePolicyStatusCustLink();
    });
  }

  // Update Policy Payment Data to database
  updatePaymentStatuscustlink(): Promise<any> {
    return new Promise((resolve: any) => {
      console.log(this.dataForCustLink);
      if (this.dataForCustLink.transactionType == 'Renewal') {
        let RQ = JSON.parse(this.dataForCustLink.proposalRQ);
        let RQ1 = JSON.parse(RQ);
        let RS = JSON.parse(this.dataForCustLink.proposalRS);
        this.proposalRQ = RQ1.ProposalDetails;
        this.proposalRS = JSON.parse(RS);
        this.quoteRQ = RQ1.ProposalDetails;
        this.quoteRS = JSON.parse(RS);
      } else {
        this.proposalRQ = JSON.parse(this.dataForCustLink.proposalRQ);
        this.proposalRS = JSON.parse(this.dataForCustLink.proposalRS);
        this.quoteRQ = JSON.parse(this.dataForCustLink.quoteRQ);
        this.quoteRS = JSON.parse(this.dataForCustLink.quoteRS);
      }
      // let propRes = JSON.parse(this.dataForCustLink.bizResponse);
      // let propReq = JSON.parse(this.dataForCustLink.bizRequest);
      let body = {
        policyNo: "",
        coverNoteNo: "",
        proposalNo: this.proposalRS.generalInformation.proposalNumber,
        dealID: this.cs.isUndefineORNull(this.proposalRQ.DealId) ? this.dealDetails.dealID : this.proposalRQ.DealId, //this.dataForCustLink.deaL_ID,
        customerID: this.proposalRS.generalInformation.customerId,
        pfPaymentID: "",
        paymentEntryErrorID: "",
        paymentEntryErrorText: "",
        paymentEntryStatus: "",
        paymentTagErrorID: "",
        paymentTagErrorText: "",
        paymentTagStatus: "",
        message: "",
        PaymentMode: '4',
        statusMessage: "",
        paymsRequestID: "",
        paymentRS: "", // here pass full response string which is coming from Mihir api
        corelationID: this.proposalRQ.CorrelationId, // pass correlation from payment api response
      };
      let str = JSON.stringify(body);
      this.cs.post("Payment/UpdatePaymentDetails", str).then((res: any) => {
        this.updatePayment = res;
        resolve();
      });
    });
  }

  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    console.log(event.target.value.length);
    let length = event.target.value.length;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  updatePolicyStatusCustLink() {
    let SubProductType = this.dataForCustLink.policySubType;
    let propRes: any; let propReq: any; let totalTax: any;

    if (this.dataForCustLink.transactionType == 'Renewal') {
      let RQ = JSON.parse(this.dataForCustLink.proposalRQ);
      let RQ1 = JSON.parse(RQ);
      let RS = JSON.parse(this.dataForCustLink.proposalRS);
      propReq = RQ1.ProposalDetails;
      propRes = JSON.parse(RS);
      totalTax = propRes.totalTax
    } else {
      propReq = JSON.parse(this.dataForCustLink.proposalRQ);
      propRes = JSON.parse(this.dataForCustLink.proposalRS);
      totalTax = propRes.totalTax
    }
    console.log("My Break in", propRes);
    console.log("My Break in", propReq);
    console.log("My Break in", this.dataForCustLink);

    let body;
    body = {
      DealID: this.cs.isUndefineORNull(propReq.DealId) ? this.dealDetails.dealID : propReq.DealId,
      PolicyType: "1", //String
      PolicySubType: SubProductType, //String
      ProposalNumber: propRes.generalInformation.proposalNumber,
      PolicyNumber: "",
      PolicyStartDate: propReq.PolicyStartDate,
      PolicyEndDate: propReq.PolicyEndDate,
      Status: propRes.StatusID,
      BasicPremium: propRes.totalLiabilityPremium,
      Discount: parseFloat(propRes.specialDiscount), //String
      ServiceTax: totalTax,
      TotalPremium: parseFloat(propRes.finalPremium), //String
      AddressID: "",
      Isactive: "1",
      PaymentID: JSON.stringify(this.updatePayment.paymentID),
      SalesTax: "",
      Surcharge: "",
      TotalTax: totalTax, //String
      TransFor: propRes.generalInformation.transactionType,
      TaxRate: "",
      CustomerType: propReq.CustomerType,
      PreviousPolicyNumber: null,
      PFPolicyNo: "",
      PFProposalNo: "",
      PFCovernoteNo: "",
      PFCustomerID: propRes.generalInformation.customerId,
      CustomerName: propReq.CustomerDetails.CustomerName,
      PFPaymentID: "",
      SumInsured: null,
      Planname: null,
      PolicyTenure: propRes.generalInformation.tenure,
      PolicyTPTenure: propRes.generalInformation.tpTenure,
      PolicyPACoverTenure: JSON.stringify(propReq.PACoverTenure),
      UTGSTAmount: "0",
      TotalGSTAmount: "0",
      CGSTAmountRate: null,
      SGSTAmountRate: null,
      IGSTAmountRate: null,
      UTGSTAmountRate: null,
      TOTALGSTAmountRate: null,
      TransOn: "2020-03-18",
      CreatedBy: null,
      CreatedOn: "2020-03-18",
      ModifiedBy: null,
      ModifiedOn: "2020-03-18",
      CorelationID: propReq.CorrelationId,
      IsPayLinkSent: this.IsPayLinkSent,
      PayLinkReciever: this.PayLinkReciever,
      CustomerPayLink: this.CustomerPayLink,
      smsreciver: this.sendDetails.mobileno,
      isSMSSent: this.isSMSSent,
    };
    if (propReq.Tenure == "1" && propReq.TPTenure == "0") {
      body.IsStandaloneOD = true;
    }


    if (
      this.paramDataValues.iPartnerLogin.isSubagent != null ||
      this.paramDataValues.iPartnerLogin.isSubagent != undefined
    ) {
      body.IsSubagent = true;
      body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
    }
    let str = JSON.stringify(body);
    this.cs.post("Policy/AddPolicyDetails", body).then((res: any) => { });
  }
}
