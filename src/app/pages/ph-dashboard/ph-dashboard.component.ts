import { Component, OnInit } from "@angular/core";
import * as moment from "moment";
import { CommonService } from "src/app/services/common.service";
import swal from "sweetalert";
import { SendMessageService } from "src/app/services/send-message.service";
import { Router } from "@angular/router";
import * as XLSX from "xlsx";

@Component({
  selector: "app-ph-dashboard",
  templateUrl: "./ph-dashboard.component.html",
  styleUrls: ["./ph-dashboard.component.css"],
})
export class PhDashboardComponent implements OnInit {
  phdash: any = {};
  spinloader: boolean = false;
  documentlist: any;
  memmaxDOB: any;
  page: any;
  term: any;
  documentlist_proto: any;
  documentlist_proto1: any;
  norecord: boolean;
  username: any;
  filtername: string;
  documentlist1: any;
  fileName = "ExcelSheet.xlsx";
  excelData = [];

  constructor(
    private cs: CommonService,
    private messageService: SendMessageService,
    private router: Router
  ) {}

  ngOnInit() {
    this.memmaxDOB = new Date();
    this.memmaxDOB.setDate(this.memmaxDOB.getDate() + 0);
    let startDate = moment(new Date()).format("YYYY-MM-DD");
    this.phdash.FromDate = startDate;
    this.phdash.ToDate = startDate;
    let detail = JSON.parse(localStorage.getItem("phdetails"));
    this.username = detail.username;
  }

  getQcCases() {
    let body = {
      FromDate: moment(this.phdash.FromDate).format("DD-MMMM-YYYY"),
      ToDate: moment(this.phdash.ToDate).format("DD-MMMM-YYYY"),
    };
    let str = JSON.stringify(body);
    this.spinloader = true;
    this.cs
      .postPHT("PHAdmin/GetDocumentList", str)
      .then((res: any) => {
        if (res.status == "Success") {
          this.norecord = false;
          this.spinloader = false;
          this.documentlist_proto = res.documentDetails;
          this.documentlist = this.documentlist_proto;
          this.getFilter(this.filtername);
        } else {
          this.spinloader = false;
          this.norecord = true;
        }
      })
      .catch((err: any) => {
        this.spinloader = false;
      });
  }

  downloadExcel() {
    this.getExcelData().then(() => {
      this.convertExcel();
    });
  }

  getExcelData(): Promise<any> {
    return new Promise((resolve: any) => {
      let body = {
        FromDate: moment(this.phdash.FromDate).format("DD-MMMM-YYYY"),
        ToDate: moment(this.phdash.ToDate).format("DD-MMMM-YYYY"),
      };
      let str = JSON.stringify(body);
      this.spinloader = true;
      this.cs
        .postPHT("PHAdmin/GetQCReport", str)
        .then((res: any) => {
          if (res.status == "Success") {
            this.spinloader = false;
            this.documentlist1 = res.qcReportDetails;
            this.getFilter(this.filtername);
          } else {
            this.spinloader = false;
          }
          resolve();
        })
        .catch((err: any) => {
          this.spinloader = false;
          resolve();
        });
    });
  }

  getlistdata(data: any) {
    // this.messageService.sendphdata(data);
    localStorage.setItem("documentdata", JSON.stringify(data));
    this.router.navigateByUrl("viewdocument");
  }

  logoffPht() {
    this.router.navigateByUrl("PHLogin");
  }

  convertExcel() {
    console.log(this.documentlist1);
    var result = this.documentlist1.map((excelData) => ({
      // "Date & Time": excelData.transactionDate,
      // "Covernote Number / Policy Number": excelData.policyNo,
      // "Proposal Number" : excelData.proposalNo,
      // "Customer Name" : excelData.customerName,
      // "Reason for QC" : "Manual modification",
      // "Pendency Remark": excelData.standardRemark,
      // "QC Status": excelData.qcStatus,
      // "Document Accepted / Rejected By": excelData.docApprovedBy
      "Sr. No": excelData.srNo,
      "Customer Name": excelData.customerName,
      "Policy Number": excelData.policyNo,
      "Proposal Number": excelData.proposalNo,
      "Total Premium": excelData.totalPremium,
      "IPartner User ID": excelData.ipartnerUserID,
      "Deal ID": excelData.dealID,
      "Deal Type": excelData.dealType,
      "Product Type": excelData.productType,
      "Transaction Type": excelData.transactionType,
      "Product Code": excelData.pfProductCode,
      "Transaction Date":
        excelData.transactionDate == "0001-01-01T00:00:00"
          ? ""
          : moment(excelData.transactionDate).format("DD-MM-YYYY hh:mm:ss"),
      "Policy Start Date":
        excelData.policyStartDate == "0001-01-01T00:00:00"
          ? ""
          : moment(excelData.policyStartDate).format("DD-MM-YYYY hh:mm:ss"),
      "Policy End Date":
        excelData.policyEndDate == "0001-01-01T00:00:00"
          ? ""
          : moment(excelData.policyEndDate).format("DD-MM-YYYY hh:mm:ss"),
      "Payment ID": excelData.pfPaymentID,
      "Payment Mode": excelData.paymentMode,
      "Intermediary ID": excelData.intermediaryID,
      "Intermediary Name": excelData.intermediaryName,
      "IL Location": excelData.ilLocation,
      "IL Location Code": excelData.ilLocationCode,
      "Primary Vertical": excelData.primaryVertical,
      "Secondary Vertical": excelData.secondaryVertical,
      "RM Name": excelData.rmName,
      "RM ID": excelData.rmId,
      "QC Status": excelData.qcStatus,
      Remark: excelData.remark,
      "Pendency Remark": excelData.pendencyRemark,
      "QC Approved By": excelData.qcApprovedBy,
      "QC Rejected By": excelData.qcRejectedBy,
      "QC Approved On":
        excelData.qcApprovedON == "0001-01-01T00:00:00"
          ? ""
          : moment(excelData.qcApprovedON).format("DD-MM-YYYY hh:mm:ss"),
      "QC Rejected On":
        excelData.qcRejectedON == "0001-01-01T00:00:00"
          ? ""
          : moment(excelData.qcRejectedON).format("DD-MM-YYYY hh:mm:ss"),
      "Document Upload On":
        excelData.docUploadOn == "0001-01-01T00:00:00"
          ? ""
          : moment(excelData.docUploadOn).format("DD-MM-YYYY hh:mm:ss"),
      "Document ReUpload On":
        excelData.docReuploadON == "0001-01-01T00:00:00"
          ? ""
          : moment(excelData.docReuploadON).format("DD-MM-YYYY hh:mm:ss"),
    }));

    /* table id is passed over here */
    // let element = document.getElementById('excel-table');
    // const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);
    // /* generate workbook and add the worksheet */
    // const wb: XLSX.WorkBook = XLSX.utils.book_new();
    // XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    // /* save to file */
    // XLSX.writeFile(wb, this.fileName);

    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(result);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Sheet1");
    XLSX.writeFile(wb, this.fileName);
  }

  exportToExcel(tableId: string, name?: string) {
    let timeSpan = new Date().toISOString();
    let prefix = name || "ExportResult";
    let fileName = `${prefix}-${timeSpan}`;
    let targetTableElm = document.getElementById(tableId);
    let wb = XLSX.utils.table_to_book(targetTableElm, <XLSX.Table2SheetOpts>{
      sheet: prefix,
    });
    XLSX.writeFile(wb, `${fileName}.xlsx`);
  }

  getFilter(ev) {
    this.filtername = ev;
    if (this.documentlist) {
      switch (this.filtername) {
        case "allCases":
          this.documentlist = this.documentlist_proto;
          break;
        case "appCases_1":
          this.documentlist = this.documentlist_proto.filter(
            (res) => res.qcStatus == "Approved"
          );
          break;
        case "pendCases_1":
          this.documentlist = this.documentlist_proto.filter(
            (res) => res.qcStatus == "Pending"
          );
          break;
        default:
          break;
      }
    }
  }
}
