import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PhDashboardRoutingModule } from "./ph-dashboard-routing.module";
import { PhDashboardComponent } from "./ph-dashboard.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [PhDashboardComponent],
  imports: [CommonModule, PhDashboardRoutingModule, SharedModule],
})
export class PhDashboardModule {}
