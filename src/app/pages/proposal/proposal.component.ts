import {
  Component,
  HostListener,
  OnInit,
  AfterViewInit,
  DoCheck,
  ChangeDetectorRef,
  Output,
  EventEmitter,
  AfterViewChecked,
} from "@angular/core";
import { commonData } from "../../commonData/commonData";
import { CommonService } from "src/app/services/common.service";
import { Router } from "@angular/router";
import swal from "sweetalert";
import * as moment from "moment";
declare var $: any;
import { ApiServiceService } from "src/app/services/api-service.service";
import {
  trigger,
  transition,
  animate,
  keyframes,
  style,
} from "@angular/animations";
import { CommonMessageService } from "src/app/services/common-message.service";

declare var jspdf: any;
declare var html2canvas: any;

@Component({
  selector: "app-proposal",
  templateUrl: "./proposal.component.html",
  styleUrls: ["./proposal.component.css"],
  animations: [
    trigger("flyInOut", [
      transition("void => fly", [
        animate(
          500,
          keyframes([
            style({ transform: "translateY(100%)", opacity: 0 }),
            style({ transform: "translateY(0)", opacity: 1 }),
          ])
        ),
      ]),
    ]),
  ],
})
export class ProposalComponent
  implements OnInit, AfterViewInit, DoCheck, AfterViewChecked {
  @Output() changedEventListner: EventEmitter<any> = new EventEmitter();

  addOtherVehicle: boolean = false;
  addNomineeVehicle: boolean = false;
  GSTDetails: boolean = false;
  UINDetails: boolean = false;
  calculatedData: any;
  gender: any;
  IsValidDrivingLicense: boolean = false;
  registrationType: any;
  portabilityPeriod: any;
  quoteReq: any;
  premiumWithoutDiscount: any;
  recalquotereq: any;
  quoteRes: any;
  pincodeDetails: any;
  pinData: Object;
  pinFlag: boolean = false;
  stateName: any;
  addressLine: any;
  CityCode: any;
  StateCode: any;
  CountryCode: any;
  custDetails: any;
  customerID: any;
  cityName: any;
  rtoDetailValues: any;
  paramDataValues: any;
  policyType: any;
  productCode: any;
  classCode: any;
  searchid: boolean = false;
  searchName: boolean = false;
  customerName: any;
  custDetailsName: any;
  custFlag: boolean = false;
  public primaryAddress: string;
  constName: any;
  custType: any;
  gstName: any;
  stateMaster: any;
  GSTstateName: any;
  type: any;
  product: any;
  propReq: any;
  propReqrem: any;
  dataforBack: any;
  PolicyStartDate: any;
  FirstRegistrationDate: any;
  finalPremium: any;
  paymentFlag: boolean = false;
  liablitySubTotal: any;
  memmaxDOB: any;
  rtoLocation: any;
  tenure: any;
  tpTenure: any;
  previousPolicyValue: boolean = false;
  ClaimOnPreviousPolicy: boolean = true;
  rollpolicyType: any = "Comprehensive";
  previousPolicyTen: any;
  insurerlist: any;
  companyCode: any;
  claimNCB: boolean = false;
  ncbValue: any;
  cpaTenure: any;
  disabled: boolean = false;
  disabled1: boolean = false;
  finalPremium_2: any;
  finalPremium_3: any;
  policyTenure: any;
  basicLiabilityPremium: any;
  paCoverPremium: any;
  legalLiabilityAmount: any;
  tppdDeductionAmount: any;
  totalLiabilityPremium: any;
  totalGSTAmount: any;
  totalPremium: any;
  policyEndDate: any;
  policyTunure: any;
  ppemmaxDOB: any;
  showPrev: boolean = false;
  biFuelKitTP: any;
  PreviousPolicyDetails: any = {};
  Claims: any;
  breakinID: any;
  rtoName: any;
  breakinFlag: any;
  iasFlag: any;
  vehicleName: any;
  saveProposalStatus: any;
  errorMessage: any;
  saveFlagTP: any;
  exshow: any;
  policySubType: any;
  previousPolicyTenOffline: any;
  breakin: any;
  voluntaryDiscount: any;
  saveQuote: any;
  financierDetailsVal: boolean = false;
  sublocations: any;
  savequoteData: any;
  saveQuoteedit: any;
  financierDetails: any = {};
  chassisflag: boolean = false;
  engingerr: boolean = false;
  showsaod: boolean = false;
  prevtenure = 1;
  refreshDiv: boolean = true;
  showPPDDiv: boolean = false;
  isRecalc: boolean = true;
  showRecal: boolean = false;
  showPPDTenure: boolean = false;
  sendCustPaymentLinkFlag: boolean = false;
  isInstaProposalRequired = false;
  // AML
  isAMLRequired = false;
  isSalaried=false;
  isSelfEmployed=false;  
  businessType:string ="";
  isAmlAggrement=false;
  instaPolicyNumber: any;
  isIDVDeviation = false;
  isODDeviation = false;
  showInstaPolicyUI = false;
  instaProposalBody: any;
  // IAS Changes
  ineractionSpinner = false;
  srt = [];
  iasProcess = [];
  iasIntRes: any;
  isPropDeviation: boolean = false;
  deviationMsg: any;
  isIDVChangeDev = false;
  isOtherDiscDev = false;
  isODIntRaised = false;
  isIDVIntRaised = false;
  selectedSRT: any;
  iasIDVReq: any;
  iasODReq: any;
  iasIDVRes: any;
  iasODRes: any;
  isIASRaised = false;
  //send payment link
  finalamount: any;
  showSendLink: boolean = true;
  showPaymentFlag: boolean = false;
  propRes: any;
  customerEmail: any;
  custMobile: any;
  PaymentMode: any;
  customLink = { custEmail: "" };
  starRatingFeed: any;
  // policySubType: any;
  feedBackResponse: any;
  IsPayLinkSent: string;
  PayLinkReciever: any;
  moberrorFlag: boolean = false;
  CustomerPayLink: any;
  isSMSSent: any;
  custLink: any;
  updatePayment: any;
  sendLinkstatus: any;
  SubProductType: any;
  totalTax: any;
  instaData: any;
  PreBreakINData: any;
  watchMe: any;
  mobNumberPattern = "^(0/91)?[6-9][0-9]{9}$";
  localAuth;
  emailPattern = "^[1-9][0-9]{5}$";
  breakintype = {
    breakin: "selfinspection",
    isPreApprovedBreakIN: false,
    breakINID: "",
  };
  panNumberFlag = false;
  panNumberLength: any;
  annualIncomeDetails: boolean = true;
	selfEmployed : boolean = false;
  // Proposal body
  proposalDetails = {
    EngineNumber: "",
    ChassisNumber: "",
    NameOfNominee: "",
    Relationship: "",
    Age: "",
    CustomerName: "",
    DateOfBirth: "",
    AddressLine1: "",
    AddressLine2: "",
    cityName: "",
    cName: "",
    stateName: "",
    PinCode: "",
    Email: "",
    MobileNumber: "",
    PANCardNo: "",
    AadharNumber: "",
    IsValidDrivingLicense: "",
    CustomerType: "", //Registration Type
    searchid: "",
    searchname: "",
    title: "",
    // GST Details
    GSTInNumber: "",
    GSTToState: "",
    ConstitutionOfBusiness: "",
    gstCustomerType: "",
    PanDetails: "",
    GSTRegistrationStatus: "",
    // Rollover Tags
    PropPreviousPolicyDetails: false,
    PreviousPolicyStartDate: "",
    PreviousPolicyEndDate: "",
    PreviousPolicyType: "",
    BonusOnPreviousPolicy: "0",
    PreviousPolicyNumber: "",
    ClaimOnPreviousPolicy: false,
    TotalNoOfODClaims: "0",
    NoOfClaimsOnPreviousPolicy: "",
    PreviousInsurerName: "",
    PreviousVehicleSaleDate: "",
    PreviousPolicyTenure: 1,
    claimyes: "",
    claimno: "",
    claims: "claimyes",
    companyCode: "",

    // Addons
    IsExtensionCountry: false,
    ExtensionCountryName: "",
    IsPACoverUnnamedPassenger: false,
    SIPACoverUnnamedPassenger: "",
    RSAPlanName: "",
    ZeroDepPlanName: "",
    IsLegalLiabilityToPaidDriver: false,
    NoOfDriver: 1,
    IsLegalLiabilityToPaidEmployee: false,
    NoOfEmployee: 1,
    IsConsumables: false,
    isVoluntaryDeductible: false,
    voluntaryDeductiblePlanName: "",
    isNCBProtect: false,
    ncbProtectPlanName: "",
    isGarageCash: false,
    garageCashPlanName: "",
    IsRTIApplicableflag: false,
    PreviousPolicyDetails: false,
    // SAOD TAGS
    TPStartDate: "",
    TPEndDate: "",
    TPPolicyNo: "",
    TPInsurerName: "",

    //All Risk Electric Vehicle - Sejal - 28-02-2022
    batterysaver: "",
    Manufacturer: "",
    model: "",
    kilowatt: "",
    RiskSIComponent: "",
    // AML
    employeeName :"",
    officeAddress :"",
  };

  breakinDetails = {
    inspectionMode: "IL",
    subLocation: "",
  };
  isRSAPlan: any;
  sendDetails = {
    Email: "",
    mobileno: "",
  };
  usedcar: any;
  breakinStatus: any;
  dealDetailsfromIM: any;
  basicbreak: any;
  chasismsg: string;
  valuelength: any;
  chasislength: any;

  showAddon: boolean = false;
  showAddon1: boolean = false;
  systemQC: boolean = false;
  claimdiv: boolean = true;
  TodayDate: string;
  PreBreakINDataresponse: any;
  claimsmade: string;
  isSoftCopySelected = false;
  accessFor: any;
  fetchProposalData: any;
  /**
   * Digital POS variables
   */
  IsPOSTransaction_flag: any;
  posDealID: any;
  deal_Id: any;
  POSmobileNumber: any;
  ODandTPAmount: any;
  loanLeaseHypo = false;
  isSiteDown = false;
  backupPreviousInsurerList = [];
  showPYP = false;
  breakInDeviationFlag = false;

  // All Risk Electric Bike
  allriskEBModel: any;
  EBVehicleModel: any;
  modelname: any;
  ebmanulist: any;
  EBManufacturerList: any;
  manuname: any;
  KiloWattList: any;
  EVQuoteReq: any;
  EVQuoteRes: any;
  evfinalamount: any;
  evProposalNo: any;
  policy: any;
  scpaQuoteReq: any;
  scpaQuoteRes: any;
  scpacalculatedData: any;
  scpafinalamount: any
  scpaProposalNo: any;
  isSCPA: any;
  bundleID: any;
  proposalData = [];
  multitotalPremium = [];
  amtData = [];
  totalsum: number;
  isMulti = false;
  scpapropReq: any;
  scpapropRes: any;
  evpropReq: any;
  evpropRes: any;
  TransactionType: any;
  isRollover = false;
  isAllRiskCreated = false;
  SCPARes: any;
  isSCPARequired = false;
  scpaResponse: any;
  scpaRequest: any;
  allRisk_productCode:any;
  odDeviationReq: any;
  idvDeviationReq: any;
  showSCPAinNysa = false;
  annualIncome:"";

  batteryVolt: any;

  constructor(
    public router: Router,
    // public actRoute: ActivatedRoute,
    public cs: CommonService,
    public api: ApiServiceService,
    public msgService: CommonMessageService,
    private changeDetector: ChangeDetectorRef
  ) {
    window.scrollTo(0, 0);
    /**
     * Digital POS change
     * get values from localStorage for checking if conditions in html file
     * author :- digitalPOS
     * date :- 15-06-2021
     */
    if (localStorage.getItem("IsPOSTransaction")) {
      this.IsPOSTransaction_flag = JSON.parse(
        localStorage.getItem("IsPOSTransaction")
      );
      this.POSmobileNumber = JSON.parse(
        localStorage.getItem("POSmobileNumber")
      );
    }
  }

  ngOnInit() {
    localStorage.removeItem("senCustLink"); 
    let scpaData = JSON.parse(localStorage.getItem('scpaData'));   
    this.showSCPAinNysa = scpaData.isSCPAAccess;
    this.proposalDetails.title = "Mr";
    this.gender = "MALE";
    this.createBizToken();
    // this.cs.createIASToken();  // Create IAS token on Raise Interaction - Sejal
    this.localAuth = JSON.parse(localStorage.getItem("AuthToken"));
    this.isIDVChangeDev = JSON.parse(localStorage.getItem("IDVTriggred"));
    this.isOtherDiscDev = JSON.parse(localStorage.getItem("ODTriggred"));
    this.scpaResponse = JSON.parse(localStorage.getItem("resSAODSCPA"));
    this.scpaRequest = JSON.parse(localStorage.getItem("reqSAODSCPA"));
    this.isSCPARequired = JSON.parse(localStorage.getItem("isSCPARequired"));
    this.quoteRes = JSON.parse(localStorage.getItem("calQuoteRes"));
    if(this.isSCPARequired){
      this.finalamount = this.quoteRes.finalPremium + this.scpaResponse.totalPremium;
    }
    
    let Parsedtokenaccess: any = this.api.decrypt(
      this.localAuth.applicationAccess
    );
    this.accessFor = JSON.parse(Parsedtokenaccess);
    this.memmaxDOB = new Date();
    this.memmaxDOB.setDate(this.memmaxDOB.getDate() + 0);

    this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
    this.productCode = this.paramDataValues.iPartnerLogin.product;
    this.policyType = this.paramDataValues.iPartnerLogin.policy;
    this.allRisk_productCode = commonData.allriskProductCode;
    if (this.productCode == "2312" || this.productCode == "2320") {
      this.classCode = "37";
    } else if (this.productCode == "2311" || this.productCode == "2319") {
      this.classCode = "45";
    }  // All Risk Electric Bike - Sejal - 09-03-2022
    else if (this.productCode == commonData.allriskProductCode) {
      // this.gender = "MALE";
      this.EVQuoteReq = JSON.parse(localStorage.getItem("EVQuoteReq"));
      this.EVQuoteRes = JSON.parse(localStorage.getItem("EVQuoteRes"));
      this.scpaQuoteReq = localStorage.getItem("scpaQuoteReq");
      this.scpaQuoteRes = localStorage.getItem("scpaQuoteRes");
      let selectedEVmodel = JSON.parse(localStorage.getItem("SelectedEVModel"));
      console.log(selectedEVmodel[0].battery_Kv);
      let batteryVolt1 = selectedEVmodel[0].battery_Kv.split("-");
      let batteryVolt2 = selectedEVmodel[0].battery_Kv.split("/");
      if (batteryVolt1.length > 1) {
        this.proposalDetails.kilowatt = batteryVolt1[0];
      } else {
        this.proposalDetails.kilowatt = batteryVolt2[0];
      }

      this.batteryVolt = this.proposalDetails.kilowatt;

      this.proposalDetails.batterysaver = this.EVQuoteReq.BatteryNumber;
      this.proposalDetails.RiskSIComponent = this.EVQuoteReq.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].RiskSIComponent;
      this.proposalDetails.model = this.EVQuoteReq.Model;
      this.proposalDetails.Manufacturer = this.EVQuoteReq.ManuFacturer;
      this.proposalDetails.stateName = this.EVQuoteReq.CustomerState;
      this.isSCPA = JSON.parse(localStorage.getItem("IsSCPA"));
      this.TransactionType = this.EVQuoteReq.TransactionType;

      if (this.isSCPA == true) {
        this.isMulti = true;
      } else {
        this.isMulti = false;
      }

      if (this.TransactionType == "New") {
        this.isRollover = false;
      } else {
        this.isRollover = true;
      }
      this.bundleID = localStorage.getItem("BundleID");
    }

    if (this.policyType == "NEW") {
      this.type = "New";
    } else if (this.policyType == "USED") {
      this.type = "Used";
      this.usedcar = true;
    } else {
      this.type = "Rollover";
    }
    if (this.productCode == "2312") {
      this.product = "Two Wheeler";
    } else if (this.productCode == "2311") {
      this.product = "Four Wheeler";
    } else if (this.productCode == "2320") {
      this.product = "Two Wheeler Liability";
    } else if (this.productCode == "2319") {
      this.product = "Four Wheeler Liability";
    } else if (this.productCode == commonData.allriskProductCode) {
      this.product = "Risk Electric Bike";
    }


    if (JSON.parse(localStorage.getItem("calQuoteReq"))) {
      this.quoteReq = JSON.parse(localStorage.getItem("calQuoteReq"));
      this.isIDVDeviation = JSON.parse(localStorage.getItem("IDVTriggred"));
      this.isODDeviation = JSON.parse(localStorage.getItem("ODTriggred"));
      
this.quoteRes = JSON.parse(localStorage.getItem("calQuoteRes"));
      if (this.quoteReq.BusinessType == "New Business") {
        if (!this.cs.isUndefineORNull(this.quoteReq.PreviousPolicyDetails)) {
          this.showPYP = true;
        } else {
          this.showPYP = false;
        }
      } else if (this.quoteReq.BusinessType == "Roll Over" || this.quoteReq.BusinessType == "Used") {
        this.showPYP = !this.quoteReq.isNoPrevInsurance;
      } else {
        this.showPYP = false;
      }
      if (this.cs.isUndefineORNull(this.quoteReq.RSAPlanName)) {
        this.isRSAPlan = false;
      } else {
        this.isRSAPlan = true;
      }

      this.ODandTPAmount =
        this.quoteRes.riskDetails.basicOD + this.quoteRes.riskDetails.basicTP;
      this.recalquotereq = JSON.parse(localStorage.getItem("calQuoteReq"));

      if (this.recalquotereq.TPStartDate) {
        this.showsaod = true;
      } else {
        this.showsaod = false;
      }

      if (JSON.parse(localStorage.getItem("PreBreakINData"))) {
        this.breakintype.breakin = "preApprovedBreakIN";
      } else if (this.quoteReq.quoteReq == true) {
        this.breakintype.breakin = "selfinspection";
      } else if (this.quoteReq.quoteReq == false) {
        this.breakintype.breakin = "ilinspection";
      }
      console.log(this.quoteReq);
      if (this.quoteReq.PreviousPolicyDetails) {
        this.proposalDetails.PropPreviousPolicyDetails = true;
        if (this.quoteReq.PreviousPolicyDetails.ClaimOnPreviousPolicy == true) {
          this.claimsmade = "Yes";
        } else if (
          this.quoteReq.PreviousPolicyDetails.ClaimOnPreviousPolicy == false
        ) {
          this.claimsmade = "No";
        }
      }

      if (
        this.quoteRes.riskDetails.breakinLoadingAmount &&
        this.quoteRes.riskDetails.breakinLoadingAmount != 0
      ) {
        this.basicbreak =
          this.quoteRes.riskDetails.basicOD +
          this.quoteRes.riskDetails.breakinLoadingAmount;
      } else {
        this.basicbreak = this.quoteRes.riskDetails.basicOD;
      }
      if (this.quoteRes.riskDetails.voluntaryDiscount < 1) {
        this.voluntaryDiscount = "1";
      } else if (this.quoteRes.riskDetails.voluntaryDiscount > 1) {
        this.voluntaryDiscount = Math.round(
          this.quoteRes.riskDetails.voluntaryDiscount
        );
      }
      this.rtoLocation = this.quoteRes.generalInformation.rtoLocation;
      this.tenure = this.quoteRes.generalInformation.tenure;
      this.tpTenure = this.quoteRes.generalInformation.tpTenure;
      this.vehicleName =
        this.quoteRes.generalInformation.manufacturerName +
        " " +
        this.quoteRes.generalInformation.vehicleModel;
    } else if (this.productCode == commonData.allriskProductCode) {
      this.quoteReq = JSON.parse(localStorage.getItem("EVQuoteReq"));
      this.quoteRes = JSON.parse(localStorage.getItem("EVQuoteRes"));
      // this.rtoLocation = this.quoteReq.rtoLocation;    //Sejal - 29-03-2022 commented for All Risk
      // this.tenure = 1;
      // this.tpTenure = 1;
      // this.policyTenure = this.quoteReq.PolicyTenure;   //Sejal - 29-03-2022 commented for All Risk
    } else {
      this.quoteReq = JSON.parse(localStorage.getItem("calQuoteTPReq"));
      this.quoteRes = JSON.parse(localStorage.getItem("calQuoteTPRes"));
      this.rtoLocation = this.quoteReq.rtoLocation;
      this.tenure = 1;
      this.tpTenure = 1;
      this.policyTenure = this.quoteReq.PolicyTenure;
    }

    // if(this.quoteReq.IsSelfInspection == true){
    //   this.breakinDetails.inspectionMode = "self";
    // } else if(this.quoteReq.IsSelfInspection == false){
    //   this.breakinDetails.inspectionMode = "IL";
    // }

    /**
     * digitalPOS change
     * call getDealDetails function only for nysa not for POS
     * date :- 29-07-2021
     */
    if (this.IsPOSTransaction_flag == true) {
      this.posDealID = JSON.parse(localStorage.getItem("DealID"));
      this.getDealDetails();
    } else {
      this.getDealDetails();
    }
    this.iasFlag = JSON.parse(localStorage.getItem("isIASFlag"));
    this.breakinFlag = JSON.parse(localStorage.getItem("breakinFlag"));
    this.saveQuote = JSON.parse(localStorage.getItem("savedQuotes"));
    this.saveQuoteedit = JSON.parse(localStorage.getItem("savedQuotes"));
    if (
      this.saveQuote == true ||
      this.breakinFlag == true ||
      this.iasFlag == true
    ) {
      let breakINData: any;
      if (this.saveQuote == true) {
        breakINData = JSON.parse(localStorage.getItem("saveQuoteData"));
      } else if (this.breakinFlag == true) {
        breakINData = JSON.parse(localStorage.getItem("breakinData"));
      } else if (this.iasFlag == true) {
        breakINData = JSON.parse(localStorage.getItem("IASDataProp"));
        if(!this.cs.isUndefineORNull(breakINData.idvIasID)){
          this.isODDeviation = true;
          this.idvDeviationReq = JSON.parse(breakINData.idvIasRQ)
        }else{
          this.isODDeviation = false;
        }

        if(!this.cs.isUndefineORNull(breakINData.odIasID)){
          this.isODDeviation = true;
          this.odDeviationReq = JSON.parse(breakINData.odIasRQ)
        }else{
          this.isODDeviation = false;
        }
        
      }
      // save quote issue 18032022
      // let quoteReq = JSON.parse(breakINData.quoteRQ);
      // let propReq = JSON.parse(breakINData.proposalRQ);
      if (this.productCode == commonData.allriskProductCode) {
        this.quoteReq = JSON.parse(localStorage.getItem("EVQuoteReq"));
        this.propReq = JSON.parse(localStorage.getItem("EVPropDatareq"));
        console.log("Chassis Number", this.propReq.FieldsGrid[3].FieldValue, this.propReq.FieldsGrid[3]);
        this.proposalDetails.ChassisNumber = this.propReq.FieldsGrid[3].FieldValue;
        console.log("Chassis Number", this.proposalDetails.ChassisNumber);

        if (!this.cs.isUndefineORNull(localStorage.getItem("scpaPropDatareq"))) {
          this.isSCPA = true;
        } else {
          this.isSCPA = false;
        }
        localStorage.setItem("IsSCPA", this.isSCPA.toString());
      } else if (this.iasFlag == true) {
        this.quoteReq = JSON.parse(breakINData.bizRequest);
        this.propReq = JSON.parse(breakINData.bizRequest);
        if (this.quoteReq.BusinessType == "New Business") {
          if (!this.cs.isUndefineORNull(this.quoteReq.PreviousPolicyDetails)) {
            this.showPYP = true;
          } else {
            this.showPYP = false;
          }
        } else if (this.quoteReq.BusinessType == "Roll Over" || this.quoteReq.BusinessType == "Used") {
          this.showPYP = !this.quoteReq.isNoPrevInsurance;
        } else {
          this.showPYP = false;
        }
      } else {
        this.quoteReq = JSON.parse(breakINData.quoteRQ);
        this.propReq = JSON.parse(breakINData.proposalRQ);
        if (this.quoteReq.BusinessType == "New Business") {
          if (!this.cs.isUndefineORNull(this.quoteReq.PreviousPolicyDetails)) {
            this.showPYP = true;
          } else {
            this.showPYP = false;
          }
        } else if (this.quoteReq.BusinessType == "Roll Over" || this.quoteReq.BusinessType == "Used") {
          this.showPYP = !this.quoteReq.isNoPrevInsurance;
        } else {
          this.showPYP = false;
        }
      }
      console.log("No call", this.quoteReq);
      console.log("No call", this.propReq);
    } else {
      if (localStorage.getItem("rtoDetail") != undefined) {
        this.rtoDetailValues = JSON.parse(localStorage.getItem("rtoDetail"));
        if (this.rtoDetailValues.isNewRTOVendorCall == true) {
          this.AadrillaData();
        } else if (this.rtoDetailValues.isZoopRTOVendorCall == true) {
          // this.showPrev = true;
          this.ZoopData();
        } else if (
          this.rtoDetailValues.API == "ZOOP" &&
          this.accessFor.isEnableCarInfoNewService
        ) {
          this.VahaanData();
        } else {
          if (this.productCode != commonData.allriskProductCode) {
            this.proposalDetails.EngineNumber = this.rtoDetailValues.eng_no;
            this.proposalDetails.ChassisNumber = this.rtoDetailValues.chasi_no;
            this.proposalDetails.CustomerName = this.rtoDetailValues.owner_sr;
            this.proposalDetails.employeeName =  this.proposalDetails.CustomerName;
            this.rtoData();
          }
        }
      }
    }

    if (JSON.parse(localStorage.getItem("PropDatareq"))) {
      this.propReq = JSON.parse(localStorage.getItem("PropDatareq"));
      if (this.propReq.softCopyFlag == "Yes") {
        this.isSoftCopySelected = true;
      } else {
        this.isSoftCopySelected = false;
      }
    }

    /**
     * digitalPOS change
     * call getDealDetails function only for nysa not for POS
     * date :- 29-07-2021
     */
     if (this.IsPOSTransaction_flag == true) {
      this.posDealID = JSON.parse(localStorage.getItem("DealID"));
      this.getDealDetails();
    } else {
      this.getDealDetails();
    }

    // All Risk Electric Bike - Sejal - 11-4-2022
    if (this.productCode == commonData.allriskProductCode && JSON.parse(localStorage.getItem("EVPropDatareq"))) {
      this.propReq = JSON.parse(localStorage.getItem("EVPropDatareq"));
      // this.bundleID = localStorage.getItem("BundleID").toString();
      if (this.propReq.softCopyFlag == "Yes") {
        this.isSoftCopySelected = true;
      } else {
        this.isSoftCopySelected = false;
      }
    }

    if (this.breakinFlag == true) {
      this.backRTO(this.quoteReq.RTOLocationCode);
      this.backManuModel();
      if (
        this.propReq.BusinessType == "Roll Over" &&
        this.propReq.PreviousPolicyDetails != null
      ) {
        this.cs
          .get(
            "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
            this.propReq.PreviousPolicyDetails.PreviousPolicyStartDate +
            "&prevEndDate=" +
            this.propReq.PreviousPolicyDetails.PreviousPolicyEndDate
          )
          .subscribe((res: any) => {
            this.previousPolicyTenOffline = res;
          });
      }
    }

    if (
      this.saveQuote == true ||
      this.breakinFlag == true ||
      this.iasFlag == true
    ) {
      this.backData();
      this.proposalDetails.MobileNumber =
        this.propReq.CustomerDetails.MobileNumber;
      this.proposalDetails.Email = this.propReq.CustomerDetails.Email;
      this.proposalDetails.PinCode = this.propReq.CustomerDetails.PinCode;
      this.getPincodeDetailssave(this.propReq.CustomerDetails.PinCode);
      this.productCode = this.propReq.productCode;
      // Break IN changes
      if (
        this.quoteReq.PreviousPolicyDetails &&
        !this.quoteReq.IsNoPrevInsurance
      ) {
        this.proposalDetails.PreviousPolicyEndDate = moment(
          this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate
        ).format("YYYY-MM-DD");
        this.proposalDetails.PropPreviousPolicyDetails = true;
        // this.cs
        //   .get("PrevPolicy/GetPreviousInsurer")
        //   .subscribe((prevres: any) => {
        //     this.insurerlist = prevres;
        //     let compname;
        //     compname = prevres.find(
        //       (c) =>
        //         // Changes
        //         c.shortName ==
        //         this.propReq.PreviousPolicyDetails.PreviousInsurerName
        //     ).companyName;
        //     this.proposalDetails.PreviousInsurerName = compname;
        //   });
        let previousInsurer: any = this.cs.PreviousInsurerList;;
        // let compname;
        // compname = previousInsurer.find(
        //   (c) =>
        //     c.shortName ==
        //     this.propReq.PreviousPolicyDetails.PreviousInsurerName
        // ).companyName;
        // this.proposalDetails.PreviousInsurerName = compname;

        let compname;
        let compname1;
        compname = previousInsurer.find(
          (c) => c.shortName == this.propReq.PreviousPolicyDetails.PreviousInsurerName
        );
        compname1 = previousInsurer.find(
          (c) => c.companyCode == this.propReq.PreviousPolicyDetails.PreviousInsurerName
        );
        if (this.cs.isUndefineORNull(compname)) {
          this.proposalDetails.PreviousInsurerName = compname1.companyName;
        } else {
          this.proposalDetails.PreviousInsurerName = compname.companyName;
        }

      } else {
        //Break IN Changes
        this.proposalDetails.PropPreviousPolicyDetails = false;
      }
      if (
        this.propReq.NomineeDetails != undefined ||
        this.propReq.NomineeDetails != null
      ) {
        let id = document.getElementById("addNmDt") as HTMLInputElement;
        id.checked = true;
        this.addNomineeVehicle = true;
        this.proposalDetails.NameOfNominee =
          this.propReq.NomineeDetails.NameOfNominee;
        this.proposalDetails.Age = this.propReq.NomineeDetails.Age;
        this.proposalDetails.Relationship =
          this.propReq.NomineeDetails.Relationship;
      }
    } else {
      let previousInsurer: any = this.cs.PreviousInsurerList;;
      // let compname;
      // compname = previousInsurer.find(
      //   (c) =>
      //     c.shortName == this.propReq.PreviousPolicyDetails.PreviousInsurerName
      // ).companyName;
      // this.proposalDetails.PreviousInsurerName = compname;
      console.log(this.recalquotereq, this.propReq)
      let compname;
      let compname1;
      compname = previousInsurer.find(
        (c) => c.shortName == this.propReq.PreviousPolicyDetails.PreviousInsurerName
      );
      compname1 = previousInsurer.find(
        (c) => c.companyCode == this.propReq.PreviousPolicyDetails.PreviousInsurerName
      );
      if (this.cs.isUndefineORNull(compname)) {
        this.proposalDetails.PreviousInsurerName = compname1.companyName;
      } else {
        this.proposalDetails.PreviousInsurerName = compname.companyName;
      }
    }

    if (this.breakinFlag != true) {
      this.propReqrem = JSON.parse(localStorage.getItem("propdataforBack"));
      this.dataforBack = JSON.parse(localStorage.getItem("dataforBack"));
    }

    this.FirstRegistrationDate = moment(
      this.quoteReq.DeliveryOrRegistrationDate
    ).format("DD-MM-YYYY");
    this.PolicyStartDate = moment(this.quoteReq.PolicyStartDate).format(
      "DD-MM-YYYY"
    );

    if (localStorage.getItem("calQuoteReq")) {
      this.finalPremium = Math.round(this.quoteRes.finalPremium);
      this.liablitySubTotal =
        Math.round(this.quoteRes.riskDetails.basicTP) +
        Math.round(this.quoteRes.riskDetails.paCoverForOwnerDriver);
    } else {
      this.finalPremium = Math.round(this.quoteRes.totalPremium);
    }

    if (this.propReq != null) {
      this.backData();
    } else {
      console.log("No Data Found");
    }

    this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));

    if (this.breakinFlag == true || this.iasFlag == true) {
      if (this.quoteReq.BusinessType == "New Business") {
        this.policyType = "NEW";
      } else if (this.quoteReq.BusinessType == "Used") {
        this.policyType = "Used";
      } else {
        this.policyType = "ROLL";
      }
    } else {
      this.policyType = this.paramDataValues.iPartnerLogin.policy;
    }

    if (this.saveQuote == true) {
      if (this.quoteRes.PolicySubType == "1") {
        this.showPPDTenure = true;
      } else {
        this.showPPDTenure = false;
      }
    } else {
      if (this.productCode == "2312") {
        this.showPPDTenure = true;
      } else {
        this.showPPDTenure = false;
      }
    }

    let OldReferenceNo = JSON.parse(localStorage.getItem("breakinData"));
    if (
      OldReferenceNo != undefined ||
      OldReferenceNo != null ||
      this.iasFlag == true ||
      this.breakinFlag == true ||
      this.saveQuote == true
    ) {
      if (this.quoteReq.BusinessType == "New Business") {
        let policyStartDate = moment(new Date()).format("YYYY-MM-DD");
        if (
          this.quoteRes.PolicySubType == "1" ||
          this.quoteRes.PolicySubType == "9" ||
          this.quoteRes.PolicySubType == 1 ||
          this.quoteRes.PolicySubType == 9
        ) {
          this.policyEndDate = moment(policyStartDate)
            .add(5, "years")
            .subtract(1, "days")
            .format("YYYY-MM-DD");
        } else if (
          this.quoteRes.PolicySubType == "2" ||
          this.quoteRes.PolicySubType == "10"
        ) {
          this.policyEndDate = moment(policyStartDate)
            .add(3, "years")
            .subtract(1, "days")
            .format("YYYY-MM-DD");
        } else {
          this.policyEndDate = moment(policyStartDate)
            .add(1, "years")
            .subtract(1, "days")
            .format("YYYY-MM-DD");
        }
        this.quoteReq.PolicyStartDate = policyStartDate;
        this.quoteReq.PolicyEndDate = this.policyEndDate;
      } else if (
        (this.quoteReq.BusinessType == "Roll Over" &&
          (this.quoteRes.PolicySubType == "1" ||
            this.quoteRes.PolicySubType == "2")) ||
        (this.quoteReq.BusinessType == "Used" &&
          this.quoteRes.PolicySubType == "2")
      ) {
        if (
          this.quoteReq.PreviousPolicyDetails &&
          !this.quoteReq.IsNoPrevInsurance
        ) {
          let policystartdate: any = moment(
            this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate
          )
            .add(1, "days")
            .format("YYYY-MM-DD");
          let startDate: any = moment(new Date());
          let policyenddate: any = moment(
            new Date(this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate)
          );
          let diff = startDate.diff(policyenddate, "days");
          if (diff <= 0) {
            let policystartdate: any = moment(
              this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate
            )
              .add(1, "days")
              .format("YYYY-MM-DD");
            this.quoteReq.PolicyStartDate = policystartdate;
            this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
              .add(1, "years")
              .subtract(1, "days")
              .format("YYYY-MM-DD");
          } else if (diff > 0) {
            let todaydate = moment(new Date()).format("YYYY-MM-DD");
            this.quoteReq.PolicyStartDate = todaydate;
            this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
              .add(1, "years")
              .subtract(1, "days")
              .format("YYYY-MM-DD");
            // For break-IN policy end date
            this.quoteReq.PolicyEndDate = this.policyEndDate;
          } else {
            console.log("Smaller");
          }
        } else {
          let todaydate = moment(new Date())
            .add(1, "days")
            .format("YYYY-MM-DD");
          this.quoteReq.PolicyStartDate = todaydate;
          this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
            .add(1, "years")
            .subtract(1, "days")
            .format("YYYY-MM-DD");
        }
      } else if (
        this.quoteReq.BusinessType == "Roll Over" &&
        (this.quoteRes.PolicySubType == "9" ||
          this.quoteRes.PolicySubType == "10")
      ) {
        let policyStartDate = moment(new Date()).format("YYYY-MM-DD");
        this.policyEndDate = moment(policyStartDate)
          .add(1, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
        this.quoteReq.PolicyStartDate = policyStartDate;
        this.quoteReq.PolicyEndDate = this.policyEndDate;
      }
    }

    let startDate = moment(new Date()).format("YYYY-MM-DD");
    this.TodayDate = moment(startDate).format("DD-MM-YYYY");

    if (this.policyType != "NEW") {
      this.getInsurerName();
      if (this.quoteReq.PreviousPolicyDetails != null)
        this.Claims = this.quoteReq.PreviousPolicyDetails.ClaimOnPreviousPolicy;
    } else {
      console.log("Not rollover");
    }

    this.previousPolicyValue = true;
    this.proposalDetails.NoOfClaimsOnPreviousPolicy = "0";
    this.proposalDetails.PreviousPolicyType = "Comprehensive Package";
    this.ClaimOnPreviousPolicy = true;

    if (this.productCode == "2320" && this.policyType == "ROLL") {
      this.finalPremium_2 = Math.round(this.quoteRes.totalPremium_FORPT2);
      this.finalPremium_3 = Math.round(this.quoteRes.totalPremium_FORPT3);
    }
    // console.clear();
    this.checkCrawler();
  }

  // Check crawler enable or not
  checkCrawler() {
    // //CreateToken + GetApplicationAccess merging code - Sejal
    if (!this.cs.isUndefineORNull(localStorage.getItem("AuthToken"))) {
      let encryptedTokenaccess = JSON.parse(localStorage.getItem("AuthToken"));
      let Parsedtokenaccess: any = this.api.decrypt(
        encryptedTokenaccess.applicationAccess
      );
      let tokenaccess = JSON.parse(Parsedtokenaccess);
      this.cs.isPlutusEnable = tokenaccess.isEnablePlutus;
      this.isSiteDown = tokenaccess.isSiteDown;
    }

    // this.cs.get("Access/GetApplicationAccess").subscribe((res: any) => {
    //   localStorage.setItem("AccessFor", JSON.stringify(res));
    //   this.isSiteDown = res.isSiteDown;
    // });
  }

  ngDoCheck() {
    if (localStorage.getItem("calQuoteReq")) {
      this.quoteReq = JSON.parse(localStorage.getItem("calQuoteReq"));
      this.isIDVDeviation = JSON.parse(localStorage.getItem("IDVTriggred"));
      this.isODDeviation = JSON.parse(localStorage.getItem("ODTriggred"));
      this.isSCPARequired = JSON.parse(localStorage.getItem("isSCPARequired"));
      console.log(this.SCPARes)
      if(this.cs.isUndefineORNull(this.SCPARes) && this.isSCPARequired){
        this.finalamount = this.quoteRes.finalPremium + this.scpaResponse.totalPremium;
      }
      // if (this.cs.isUndefineORNull(this.quoteReq.OtherDiscount)) {
      //   if (this.quoteReq.OtherDiscount > 0) {
      //     this.isODDeviation = true;
      //   } else {
      //     this.isODDeviation = false;
      //   }
      // } else {
      //   this.isODDeviation = false;
      // }
      this.quoteRes = JSON.parse(localStorage.getItem("calQuoteRes"));
      this.recalquotereq = JSON.parse(localStorage.getItem("calQuoteReq"));
      if (this.quoteReq.BusinessType == "New Business") {
        if (!this.cs.isUndefineORNull(this.quoteReq.PreviousPolicyDetails)) {
          this.showPYP = true;
        } else {
          this.showPYP = false;
        }
      } else if (this.quoteReq.BusinessType == "Roll Over" || this.quoteReq.BusinessType == "Used") {
        this.showPYP = !this.quoteReq.isNoPrevInsurance;
      } else {
        this.showPYP = false;
      }
    }

    if (localStorage.getItem("calQuoteReq")) {
      this.finalPremium = Math.round(this.quoteRes.finalPremium);
      this.liablitySubTotal =
        Math.round(this.quoteRes.riskDetails.basicTP) +
        Math.round(this.quoteRes.riskDetails.paCoverForOwnerDriver);
    } else {
      if (this.quoteRes) {
        this.finalPremium = Math.round(this.quoteRes.totalPremium);
      }
    }
    // this.PolicyStartDate = moment(this.quoteReq.PolicyStartDate).format('DD-MM-YYYY');
    // this.changeDetector.detectChanges();
  }

  ngAfterViewChecked() {

  }

  VBData() {
    if (this.quoteReq.BusinessType == "New Business") {
      let policyStartDate = moment(new Date()).format("YYYY-MM-DD");
      if (
        this.quoteRes.PolicySubType == "1" ||
        this.quoteRes.PolicySubType == "9" ||
        this.quoteRes.PolicySubType == 1 ||
        this.quoteRes.PolicySubType == 9
      ) {
        this.policyEndDate = moment(policyStartDate)
          .add(5, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      } else if (
        this.quoteRes.PolicySubType == "2" ||
        this.quoteRes.PolicySubType == "10"
      ) {
        this.policyEndDate = moment(policyStartDate)
          .add(3, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      } else {
        this.policyEndDate = moment(policyStartDate)
          .add(1, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      }
      this.quoteReq.PolicyStartDate = policyStartDate;
      this.quoteReq.PolicyEndDate = this.policyEndDate;
    } else if (
      (this.quoteReq.BusinessType == "Roll Over" &&
        (this.quoteRes.PolicySubType == "1" ||
          this.quoteRes.PolicySubType == "2" ||
          this.quoteRes.PolicySubType == 1 ||
          this.quoteRes.PolicySubType == 2)) ||
      (this.quoteReq.BusinessType == "Roll Over" &&
        this.quoteRes.PolicySubType == "2")
    ) {
      if (
        this.quoteReq.PreviousPolicyDetails &&
        !this.quoteReq.IsNoPrevInsurance
      ) {
        let policystartdate: any = moment(
          this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate
        )
          .add(1, "days")
          .format("YYYY-MM-DD");
        let startDate: any = moment(new Date());
        let policyenddate: any = moment(
          new Date(this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate)
        );
        let diff = startDate.diff(policyenddate, "days");
        if (diff <= 0) {
          let policystartdate: any = moment(
            this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate
          )
            .add(1, "days")
            .format("YYYY-MM-DD");
          this.quoteReq.PolicyStartDate = policystartdate;
          this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
            .add(1, "years")
            .subtract(1, "days")
            .format("YYYY-MM-DD");
          this.quoteReq.PolicyEndDate = this.policyEndDate;
        } else if (diff > 0) {
          let todaydate = moment(new Date()).format("YYYY-MM-DD");
          this.quoteReq.PolicyStartDate = todaydate;
          this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
            .add(1, "years")
            .subtract(1, "days")
            .format("YYYY-MM-DD");
          // For break-IN policy end date
          this.quoteReq.PolicyEndDate = this.policyEndDate;
        } else {
          console.log("Smaller");
        }
      } else {
        let todaydate = moment(new Date()).add(1, "days").format("YYYY-MM-DD");
        this.quoteReq.PolicyStartDate = todaydate;
        this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
          .add(1, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
        this.quoteReq.PolicyEndDate = this.policyEndDate;
      }
    } else if (
      this.quoteReq.BusinessType == "Roll Over" &&
      (this.quoteRes.PolicySubType == "9" ||
        this.quoteRes.PolicySubType == "10")
    ) {
      let policyStartDate = moment(new Date()).format("YYYY-MM-DD");
      this.policyEndDate = moment(policyStartDate)
        .add(1, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
      this.quoteReq.PolicyStartDate = policyStartDate;
      this.quoteReq.PolicyEndDate = this.policyEndDate;
    }
  }

  ngAfterViewInit() {
    if (JSON.parse(localStorage.getItem("calQuoteReq"))) {
      this.quoteReq = JSON.parse(localStorage.getItem("calQuoteReq"));
      this.isIDVDeviation = JSON.parse(localStorage.getItem("IDVTriggred"));
      this.isODDeviation = JSON.parse(localStorage.getItem("ODTriggred"));
      // if (this.cs.isUndefineORNull(this.quoteReq.OtherDiscount)) {
      //   if (this.quoteReq.OtherDiscount > 0) {
      //     this.isODDeviation = true;
      //   } else {
      //     this.isODDeviation = false;
      //   }
      // } else {
      //   this.isODDeviation = false;
      // }
      this.quoteRes = JSON.parse(localStorage.getItem("calQuoteRes"));
      this.recalquotereq = JSON.parse(localStorage.getItem("calQuoteReq"));
      if (this.quoteReq.BusinessType == "New Business") {
        if (!this.cs.isUndefineORNull(this.quoteReq.PreviousPolicyDetails)) {
          this.showPYP = true;
        } else {
          this.showPYP = false;
        }
      } else if (this.quoteReq.BusinessType == "Roll Over" || this.quoteReq.BusinessType == "Used") {
        this.showPYP = !this.quoteReq.isNoPrevInsurance;
      } else {
        this.showPYP = false;
      }
    }
   if ( this.quoteRes.generalInformation.depriciatedIDV >=10000000 && (this.quoteRes.generalInformation.customerType =="INDIVIDUAL"
    || this.quoteRes.generalInformation.customerType =="Individual"))    
    {
      this.isAMLRequired =true;
      this.isSalaried=true;
      this.isSelfEmployed=false;
    }
    else
    {
      this.isAMLRequired =false;
    }
  }

  getDataFromPlutus(proposalNo: any) {
    this.cs
      .get1("Proposal/getProposalDetails?proposalNo=" + proposalNo)
      .then((res: any) => {
        this.propReq = JSON.parse(res.proposalRQ);
        this.propRes = JSON.parse(res.proposalRS);
        this.quoteReq = JSON.parse(res.proposalRQ);
        this.quoteRes = JSON.parse(res.proposalRS);
        this.finalPremium = Math.round(this.propRes.finalPremium);
        if (this.quoteReq.BusinessType == "New Business") {
          if (!this.cs.isUndefineORNull(this.quoteReq.PreviousPolicyDetails)) {
            this.showPYP = true;
          } else {
            this.showPYP = false;
          }
        } else if (this.quoteReq.BusinessType == "Roll Over" || this.quoteReq.BusinessType == "Used") {
          this.showPYP = !this.quoteReq.isNoPrevInsurance;
        } else {
          this.showPYP = false;
        }
        console.log("Proposal Req", this.propReq);
        console.log("Proposal Res", this.propRes.generalInformation);
      });
  }

  getDealDetails() {
    console.log('123', this.quoteReq);
    let dealId = this.quoteReq.DealId ? this.quoteReq.DealId : this.quoteReq.dealId;
    this.api.getDealDetails(dealId).subscribe((res: any) => {
      if (res.status == "FAILED") {
        swal({ text: "Deal Details not found" });
        let errorbody = {
          RequestJson: dealId,
          ResponseJson: JSON.stringify(res),
          ServiceURL:
            "Deal/GetDealDetailsFromDeal?dealID=" + dealId,
          CorrelationID: dealId,
        };
        this.api.adderrorlogs(errorbody);
      } else {
        this.dealDetailsfromIM = res;
        // console.clear();
      }
    });
  }

  goToQuote() {
    this.router.navigateByUrl("quote");
    (<any>window).ga("send", "event", {
      eventCategory: "Edit Quote",
      eventLabel:
        this.quoteReq.RegistrationNumber +
        "" +
        this.product +
        "" +
        this.policyType,
      eventAction: "Edit Quote Clicked",
      eventValue: 10,
    });
  }

  clickEvent(ev: any) {
    this.proposalDetails.PreviousPolicyDetails = ev.target.checked;
  }

  decCheck(ev: any) {
    if (ev.target.checked == false) {
      ev.target.checked = true;
    }
  }

  changeNoofClaims(ev: any) {
    this.showAddon = false;
    this.systemQC = false;
  }

  changePolicyNumber(ev: any) {
    this.showAddon = false;
    this.showAddon1 = false;
    this.systemQC = false;
  }

  // getInsurerName1(ev: any) {
  //   if (ev.target.value.length < 3 || ev.key == "Backspace") {
  //   } else {
  //     this.cs
  //       .get("PrevPolicy/GetPreviousInsurer?insurerName=" + ev.target.value)
  //       .subscribe((res: any) => {
  //         this.insurerlist = res;
  //       });
  //   }
  // }

  getInsurerName1(ev: any) {
    if (localStorage.getItem("PreviousInsurerList")) {
      let decryptedPreviousInsurerList = localStorage.getItem(
        "PreviousInsurerList"
      );
      let decryptedPreviousInsurerList_V1 = this.api.decrypt(
        decryptedPreviousInsurerList
      );
      this.backupPreviousInsurerList = JSON.parse(
        decryptedPreviousInsurerList_V1
      );
    }

    if (ev.target.value.length < 3 || ev.key == "Backspace") {
      this.insurerlist = this.backupPreviousInsurerList;
    } else {
      if (localStorage.getItem("PreviousInsurerList")) {
        let decryptedPreviousInsurerList = localStorage.getItem(
          "PreviousInsurerList"
        );
        let decryptedPreviousInsurerList_V1 = this.api.decrypt(
          decryptedPreviousInsurerList
        );
        let PreviousInsurerList = JSON.parse(decryptedPreviousInsurerList_V1);

        let event = ev.target.value.toUpperCase();
        this.insurerlist = [];
        PreviousInsurerList.forEach((element) => {
          if (event == element.companyCode) {
            this.insurerlist.push(element);
          }
        });
      } else {
        this.insurerlist = this.api.getInsurerName(ev.target.value);
      }
    }
  }

  onBlurSelectFirstVal(ev: any) {
    let previousInsurer: any = this.cs.PreviousInsurerList;;
    this.insurerlist = previousInsurer;
    let obj = this.insurerlist.find((insList) => insList.companyName === ev);
    if (obj == undefined || obj == "" || obj == null) {
      this.proposalDetails.PreviousInsurerName =
        this.insurerlist[0].companyName;
    }
  }

  backRTO(ev: any) {
    if (this.quoteRes.PolicySubType == 1) {
      this.classCode = "37";
    } else if (this.quoteRes.PolicySubType == 2) {
      this.classCode = "45";
    }
    this.cs
      .get(
        "RTO/GetRTOLocationName?rtoLocationCode=" +
        ev +
        "&classCode=" +
        this.classCode
      )
      .subscribe((res: any) => {
        this.rtoName = res;
        this.rtoLocation = this.rtoName;
      });
  }

  backManuModel() {
    this.api
      .getModelName(this.quoteReq.VehicleModelCode, this.classCode)
      .subscribe((res: any) => { });
  }

  // Restore Data for back button
  backData() {
    if (this.productCode != commonData.allriskProductCode) {
      this.proposalDetails.EngineNumber = this.propReq.EngineNumber;
      this.proposalDetails.ChassisNumber = this.propReq.ChassisNumber;
    }

    this.gender = this.propReq.CustomerDetails.Gender;
    if (this.gender == "MALE") {
      this.proposalDetails.title = "Mr";
    } else {
      this.proposalDetails.title = "Mrs";
    }
    this.proposalDetails.CustomerName =
      this.propReq.CustomerDetails.CustomerName;
      this.proposalDetails.employeeName=this.proposalDetails.CustomerName;
    this.proposalDetails.AddressLine1 =
      this.propReq.CustomerDetails.AddressLine1;
    this.proposalDetails.PinCode = this.propReq.CustomerDetails.PinCode;
    this.getPincodeDetailssave(this.propReq.CustomerDetails.PinCode);
    this.proposalDetails.Email = this.propReq.CustomerDetails.Email;
    this.proposalDetails.MobileNumber =
      this.propReq.CustomerDetails.MobileNumber;
    this.proposalDetails.PANCardNo = this.propReq.CustomerDetails.PANCardNo;
    if (this.propReq.CustomerDetails.GSTDetails != undefined) {
      document.getElementById("gstdetl").click();
      this.GSTDetails = true;
      this.proposalDetails.GSTInNumber =
        this.propReq.CustomerDetails.GSTDetails.GSTInNumber;
    }

    if (this.propReq.PreviousPolicyDetails != undefined) {
      this.proposalDetails.PreviousPolicyEndDate =
        this.propReq.PreviousPolicyDetails.previousPolicyEndDate;
      let prevPolStartDate = moment(this.proposalDetails.PreviousPolicyEndDate)
        .subtract(1, "years")
        .add(1, "days")
        .format("YYYY-MM-DD");
      this.proposalDetails.PreviousPolicyStartDate = prevPolStartDate;
      let prevStartDate = moment(
        this.proposalDetails.PreviousPolicyStartDate
      ).format("DD-MMM-YYYY");
      let prevEndDate = moment(
        this.proposalDetails.PreviousPolicyEndDate
      ).format("DD-MMM-YYYY");
      this.cs
        .get(
          "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
          prevStartDate +
          "&prevEndDate=" +
          prevEndDate
        )
        .subscribe((res: any) => {
          this.previousPolicyTen = res;
          this.proposalDetails.PreviousPolicyTenure = this.previousPolicyTen;
        });

      // if (this.policyType != 'NEW') {
      //   this.getInsurerName();
      // }
      // this.proposalDetails.PreviousInsurerName = this.propReq.PreviousPolicyDetails.PreviousInsurerName;
      // this.proposalDetails.PreviousPolicyNumber = this.propReq.PreviousPolicyDetails.PreviousPolicyNumber;
      if (
        this.quoteReq.PreviousPolicyDetails &&
        !this.quoteReq.IsNoPrevInsurance
      ) {
        this.proposalDetails.PreviousPolicyEndDate = moment(
          this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate
        ).format("YYYY-MM-DD");
        this.proposalDetails.PropPreviousPolicyDetails = true;
        this.proposalDetails.PreviousPolicyNumber =
          this.propReq.PreviousPolicyDetails.PreviousPolicyNumber;
        let previousInsurer: any = this.cs.PreviousInsurerList;;
        console.log(
          this.propReq.PreviousPolicyDetails.PreviousInsurerName,
          previousInsurer
        );
        let compname;
        let compname1;
        compname = previousInsurer.find(
          (c) =>
            c.shortName ==
            this.propReq.PreviousPolicyDetails.PreviousInsurerName
        );
        compname1 = previousInsurer.find(
          (c) =>
            c.companyCode ==
            this.propReq.PreviousPolicyDetails.PreviousInsurerName
        );
        if (this.cs.isUndefineORNull(compname)) {
          this.proposalDetails.PreviousInsurerName = compname1.companyName;
        } else {
          this.proposalDetails.PreviousInsurerName = compname.companyName;
        }
      }
    }

    if (this.propReq.NomineeDetails != undefined) {
      document.getElementById("addNmDt").click();
      this.addNomineeVehicle = true;
      this.proposalDetails.NameOfNominee =
        this.propReq.NomineeDetails.NameOfNominee;
      this.proposalDetails.Relationship =
        this.propReq.NomineeDetails.Relationship;
      this.proposalDetails.Age = this.propReq.NomineeDetails.Age;
    }
  }

  // Add Previous Detail hide/show flag
  click(ev: any) {
    if (ev.target.checked) {
      this.previousPolicyValue = true;
      this.proposalDetails.NoOfClaimsOnPreviousPolicy = "0";
      this.proposalDetails.PreviousPolicyType = "Comprehensive Package";
      this.ClaimOnPreviousPolicy = true;
    } else {
      this.previousPolicyValue = false;
    }
  }
  // Add Other Vehicle Details
  addotherVehicle(ev: any) {
    if (ev.target.checked) {
      this.addOtherVehicle = true;
    } else {
      this.addOtherVehicle = false;
    }
  }
  // Add Other Vehicle Details
  addnomineeVehicle(ev: any) {
    if (ev.target.checked) {
      this.addNomineeVehicle = true;
    } else {
      this.addNomineeVehicle = false;
      this.proposalDetails.NameOfNominee = "";
      this.proposalDetails.Relationship = "";
      this.proposalDetails.Age = "";
    }
  }
  // GST Details
  enterGSTDetails(ev: any) {
    if (ev.target.checked) {
      this.GSTDetails = true;
    } else {
      this.GSTDetails = false;
    }
  }
  // Loan Details
  loanLeaseHypoCheck(ev: any) {
    if (ev.target.checked) {
      this.loanLeaseHypo = true;
    } else {
      this.loanLeaseHypo = false;
      this.quoteRes.generalInformation.depriciatedIDV >=10000000 && (this.quoteRes.generalInformation.customerType =="INDIVIDUAL" 
      || this.quoteRes.generalInformation.customerType =="Individual") ?
       this.isAMLRequired =true: this.isAMLRequired =false;
    }
  }
  // UIN Details
  enterUINDetails(ev: any) {
    if (ev.target.checked) {
      this.UINDetails = true;
    } else {
      this.UINDetails = false;
    }
  }
  // Customer Title
  customerTitle(ev: any) {
    if (ev.target.value == "Mr") {
      this.gender = "MALE";
    } else if (ev.target.value == "Mrs") {
      this.gender = "FEMALE";
    } else if (ev.target.value == "Ms") {
      this.gender = "FEMALE";
    } else {
      this.gender = "FEMALE";
    }
  }
  // Valid License
  validlicense(ev: any) {
    this.IsValidDrivingLicense = ev.target.checked;
  }

  // Prev Policy Type
  // prevPolType(ev: any) {
  //   if (ev.target.id == 'comprehensive') { this.rollpolicyType = "Comprehensive"; this.proposalDetails.PreviousPolicyType = "Comprehensive Package"; }
  //   else {
  //     this.rollpolicyType = "Bundled"; this.proposalDetails.PreviousPolicyType = "TP";
  //   }
  // }

  prevPolType(ev: any) {
    // this.showAddon = false;
    if (ev.target.id == "comprehensive") {
      this.rollpolicyType = "Comprehensive";
      this.proposalDetails.PreviousPolicyType = "Comprehensive Package";
      this.claimNCB = true;
      this.ClaimOnPreviousPolicy = false;
      this.proposalDetails.TotalNoOfODClaims = "0";
    } else {
      this.rollpolicyType = "Bundled";
      this.proposalDetails.PreviousPolicyType = "TP";
      this.claimNCB = false;
      this.proposalDetails.NoOfClaimsOnPreviousPolicy = "0";
      this.ClaimOnPreviousPolicy = true;
    }
  }

  // Previous Policy Date
  getPrevStartDate(ev: any) {
    let prevPolEndDate = moment(this.proposalDetails.PreviousPolicyStartDate)
      .add(1, "years")
      .subtract(1, "days")
      .format("YYYY-MM-DD");
    this.proposalDetails.PreviousPolicyEndDate = prevPolEndDate;
    let prevStartDate = moment(
      this.proposalDetails.PreviousPolicyStartDate
    ).format("DD-MMM-YYYY");
    let prevEndDate = moment(this.proposalDetails.PreviousPolicyEndDate).format(
      "DD-MMM-YYYY"
    );
    this.cs
      .get(
        "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
        prevStartDate +
        "&prevEndDate=" +
        prevEndDate
      )
      .subscribe((res: any) => {
        this.previousPolicyTen = res;
        this.proposalDetails.PreviousPolicyTenure = this.previousPolicyTen;
      });
  }

  getPrevEndDate(ev: any) {
    let policystartdate: any = moment(
      this.proposalDetails.PreviousPolicyEndDate
    )
      .add(1, "days")
      .format("YYYY-MM-DD");
    let startDate: any = moment(new Date());
    let policyenddate: any = moment(
      new Date(this.proposalDetails.PreviousPolicyEndDate)
    );
    let diff = startDate.diff(policyenddate, "days");
    if (diff <= 0) {
      let policystartdate: any = moment(
        this.proposalDetails.PreviousPolicyEndDate
      )
        .add(1, "days")
        .format("YYYY-MM-DD");
      this.quoteReq.PolicyStartDate = policystartdate;
      this.quoteReq.PolicyEndDate = moment(this.quoteReq.PolicyStartDate)
        .add(1, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    } else if (diff > 0) {
      let todaydate = moment(new Date()).format("YYYY-MM-DD");
      this.quoteReq.PolicyStartDate = todaydate;
      this.quoteReq.PolicyEndDate = moment(this.quoteReq.PolicyStartDate)
        .add(1, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    } else {
      console.log("Smaller");
    }

    let prevPolStartDate = moment(this.proposalDetails.PreviousPolicyEndDate)
      .subtract(1, "years")
      .add(1, "days")
      .format("YYYY-MM-DD");
    this.proposalDetails.PreviousPolicyStartDate = prevPolStartDate;
    let prevStartDate = moment(
      this.proposalDetails.PreviousPolicyStartDate
    ).format("DD-MMM-YYYY");
    let prevEndDate = moment(this.proposalDetails.PreviousPolicyEndDate).format(
      "DD-MMM-YYYY"
    );
    this.cs
      .get(
        "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
        prevStartDate +
        "&prevEndDate=" +
        prevEndDate
      )
      .subscribe((res: any) => {
        this.previousPolicyTen = res;
        this.proposalDetails.PreviousPolicyTenure = this.previousPolicyTen;
      });
  }

  ncbPerc(ev: any) {
    this.showAddon = false;
    this.ncbValue = ev.target.value;
  }

  changePrevEndDate(ev: any) {
    this.showAddon = false;
    this.showAddon1 = false;
    this.systemQC = false;
  }

  // Add Previous Detail hide/show flag
  clickclaim(ev: any) {
    if (ev.target.value == "claimno") {
      this.claimNCB = true;
      this.ClaimOnPreviousPolicy = false;
    } else {
      this.claimNCB = false;
      this.proposalDetails.NoOfClaimsOnPreviousPolicy = "0";
      this.ClaimOnPreviousPolicy = true;
    }
  }

  // Previous Insurer Name
  getInsurerName() {
    // this.cs.get("PrevPolicy/GetPreviousInsurer").subscribe((res: any) => {
    //   this.insurerlist = res;
    //   this.companyCode = this.insurerlist.find(
    //     (c) => c.companyName == this.proposalDetails.PreviousInsurerName
    //   ).companyCode;
    // });
    /**
     * Get Previous InsurerName api change
     * Author :- Sumit
     * date :- 12-01-2022
     */
    console.log(this.proposalDetails, typeof (this.proposalDetails.PreviousPolicyDetails));
    if (typeof (this.proposalDetails.PreviousPolicyDetails) == 'boolean') {
      if (this.proposalDetails.PreviousPolicyDetails) {
        let previousInsurer: any = this.cs.PreviousInsurerList;;
        this.insurerlist = previousInsurer;
        if (this.insurerlist != undefined && this.insurerlist != null) {
          this.companyCode = this.insurerlist.find(
            (c) => c.companyName == this.proposalDetails.PreviousInsurerName
          ).companyCode;
        }
      } else {

      }
    } else {
      let previousInsurer: any = this.cs.PreviousInsurerList;;
      this.insurerlist = previousInsurer;
      if (this.insurerlist != undefined && this.insurerlist != null) {
        this.companyCode = this.insurerlist.find(
          (c) => c.companyName == this.proposalDetails.PreviousInsurerName
        ).companyCode;
      }
    }

  }

  // Validation
  validate(isInstaProposalNeeded: any) {
    this.isInstaProposalRequired = isInstaProposalNeeded;
    // this.isIDVChangeDev = JSON.parse(localStorage.getItem("IDVTriggred"));
    // this.isOtherDiscDev = JSON.parse(localStorage.getItem("ODTriggred"));
    let regemailtest =
      /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(
        this.proposalDetails.Email
      );
    if (
      (this.proposalDetails.EngineNumber == null ||
        this.proposalDetails.EngineNumber == undefined ||
        this.proposalDetails.EngineNumber == "") &&
      this.productCode != commonData.allriskProductCode
    ) {
      swal({ text: "Kindly insert engine Number" });
    } else if (this.productCode != commonData.allriskProductCode && (this.proposalDetails.EngineNumber.length < 5)) {
      {
        swal({
          text: "Engine Number cannot be less than 5 digit, enter complete engine number",
        });
      }
    } else if (
      this.proposalDetails.ChassisNumber == null ||
      this.proposalDetails.ChassisNumber == undefined ||
      this.proposalDetails.ChassisNumber == ""
    ) {
      swal({ text: "Kindly insert Chassis Number" });
    }
    // else if(this.proposalDetails.ChassisNumber.length < 17 && this.policyType == "NEW" && this.quoteRes.generalInformation.transactionType != "Roll Over")
    // { { swal({  text: 'Chassis Number cannot be less than 17 digit. Enter complete chassis number' }); } }
    else if (
      this.proposalDetails.ChassisNumber.length < 5 &&
      this.policyType == "ROLL" &&
      this.quoteRes.generalInformation.transactionType == "Roll Over"
    ) {
      swal({
        text: "Chassis Number cannot be less than 5 digit. Enter complete chassis number",
      });
    } else if (
      (this.proposalDetails.Manufacturer == null ||
        this.proposalDetails.Manufacturer == undefined ||
        this.proposalDetails.Manufacturer == "") &&
      this.productCode == commonData.allriskProductCode
    ) {
      swal({ text: "Kindly select Mamufacturer" });
    } else if (
      this.proposalDetails.model == null ||
      this.proposalDetails.model == undefined ||
      this.proposalDetails.model == "" &&
      this.productCode == commonData.allriskProductCode
    ) {
      swal({ text: "Kindly select Model" });
    } else if (
      this.financierDetailsVal == true &&
      (this.financierDetails.FinancierName == "" ||
        this.financierDetails.FinancierName == null ||
        this.financierDetails.FinancierName == undefined)
    ) {
      swal({ text: "Please enter financier name" });
    } else if (
      this.financierDetailsVal == true &&
      (this.financierDetails.BranchName == "" ||
        this.financierDetails.BranchName == null ||
        this.financierDetails.BranchName == undefined)
    ) {
      swal({ text: "Please enter financier branch name" });
    } else if (
      this.proposalDetails.title == null ||
      this.proposalDetails.title == undefined ||
      this.proposalDetails.title == ""
    ) {
      swal({ text: "Select Title/Salutation from dropdown" });
    } else if (
      this.proposalDetails.CustomerName == null ||
      this.proposalDetails.CustomerName == undefined ||
      this.proposalDetails.CustomerName == ""
    ) {
      swal({ text: "Enter customer name" });
    }
    // else if (this.proposalDetails.CustomerName.length < 4) {
    //   swal({ closeOnClickOutside: false, text: "Insured name should have at least 4 consecutive character" });
    // }
    else if (
      this.proposalDetails.MobileNumber == null ||
      this.proposalDetails.MobileNumber == undefined ||
      this.proposalDetails.MobileNumber == ""
    ) {
      swal({ text: "Enter customer mobile number" });
    } else if (this.proposalDetails.MobileNumber.length < 10) {
      swal({ text: "Enter valid customer mobile number" });
    } else if (
      this.proposalDetails.Email == null ||
      this.proposalDetails.Email == undefined ||
      this.proposalDetails.Email == ""
    ) {
      swal({ text: "Enter customer email" });
    } else if (!regemailtest) {
      swal({ text: "Enter valid customer email" });
    } else if (
      this.proposalDetails.AddressLine1 == null ||
      this.proposalDetails.AddressLine1 == undefined ||
      this.proposalDetails.AddressLine1 == ""
    ) {
      swal({ text: "Enter customer Address" });
    } else if (
      this.proposalDetails.PinCode == null ||
      this.proposalDetails.PinCode == undefined ||
      this.proposalDetails.PinCode == ""
    ) {
      swal({ text: "Enter customer pincode" });
    } else if (this.proposalDetails.PinCode.length < 6) {
      swal({ text: "Enter correct pincode" });
    } else if (
      this.proposalDetails.cityName == null ||
      this.proposalDetails.cityName == undefined ||
      this.proposalDetails.cityName == ""
    ) {
      swal({ text: "Enter customer city" });
    } else if (
      this.proposalDetails.stateName == null ||
      this.proposalDetails.stateName == undefined ||
      this.proposalDetails.stateName == ""
    ) {
      swal({ text: "Enter customer state" });
    } else if (this.GSTDetails == true) {
      let regGSTTest =
        /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/.test(
          this.proposalDetails.GSTInNumber
        );
      if (
        this.proposalDetails.GSTInNumber == null ||
        this.proposalDetails.GSTInNumber == undefined ||
        this.proposalDetails.GSTInNumber == ""
      ) {
        swal({ text: "Enter GSTIN Number" });
      } else if (!regGSTTest) {
        swal({ text: "GSTIN Number is not valid" });
      } else {
        this.validate1();
      }
    } else if (this.cs.isUndefineORNull(this.proposalDetails.PANCardNo) && this.finalPremium >= 100000) {
      swal({ text: "As premium is above 1 Lac kindly enter PAN number " });
    }
    else {
      this.validate1();
    }
  }

  validate1() {
    if (this.addNomineeVehicle == true) {
      if (
        this.proposalDetails.NameOfNominee == null ||
        this.proposalDetails.NameOfNominee == undefined ||
        this.proposalDetails.NameOfNominee == ""
      ) {
        swal({ text: "Enter nominee Name" });
      } else if (
        this.proposalDetails.Age == null ||
        this.proposalDetails.Age == undefined ||
        this.proposalDetails.Age == ""
      ) {
        swal({ text: "Enter nominee age" });
      } else if (
        this.proposalDetails.Relationship == null ||
        this.proposalDetails.Relationship == undefined ||
        this.proposalDetails.Relationship == ""
      ) {
        swal({ text: "Select nominee relationship" });
      } else {
        this.calculateProposal();
      }
    } else if (
      this.policyType == "ROLL" &&
      this.showPrev == true &&
      (this.productCode == "2320" || this.productCode == "2319")
    ) {
      if (
        this.proposalDetails.PreviousPolicyEndDate == null ||
        this.proposalDetails.PreviousPolicyEndDate == undefined ||
        this.proposalDetails.PreviousPolicyEndDate == ""
      ) {
        swal({ text: "Previous policy end date is not entered" });
      } else if (
        this.proposalDetails.PreviousInsurerName == null ||
        this.proposalDetails.PreviousInsurerName == undefined ||
        this.proposalDetails.PreviousInsurerName == ""
      ) {
        swal({ text: "Select previous policy insurance company name" });
      } else if (
        this.proposalDetails.PreviousPolicyNumber == null ||
        this.proposalDetails.PreviousPolicyNumber == undefined ||
        this.proposalDetails.PreviousPolicyNumber == ""
      ) {
        swal({ text: "Previous policy number is not entered" });
      } else {
        this.calculateProposal();
      }
    } else if (
      this.quoteReq.PreviousPolicyDetails &&
      this.showsaod == false &&
      (this.productCode == "2311" || this.productCode == "2312")
    ) {
      if (
        this.proposalDetails.PreviousInsurerName != null ||
        this.proposalDetails.PreviousInsurerName != undefined
      ) {
        this.getInsurerName();
      }
      if (
        this.proposalDetails.PreviousInsurerName == null ||
        this.proposalDetails.PreviousInsurerName == undefined ||
        this.proposalDetails.PreviousInsurerName == ""
      ) {
        swal({ text: "Select previous policy insurance company name" });
      } else if (
        this.proposalDetails.PreviousPolicyNumber == null ||
        this.proposalDetails.PreviousPolicyNumber == undefined ||
        this.proposalDetails.PreviousPolicyNumber == ""
      ) {
        swal({ text: "Previous policy number is not entered" });
        // } else if (
        //   this.insurerlist == undefined ||
        //   this.insurerlist == null ||
        //   this.insurerlist == ""
        // ) {
        //   swal({ text: "Insert Previous insurer number correctly" });
      } else if (this.ValidateAml()==true) {
        this.calculateProposal();
      }
    }    
    else if (this.ValidateAml()==true) {
      console.log(this.insurerlist);     
      this.calculateProposal();
    }
  }

  panNumberValue(ev: any) {
    this.panNumberFlag = false;
    this.panNumberLength = ev.target.value.length;
    let pattern = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}$/;
    let value = ev.target.value;
    if (pattern.test(value) && ev.target.value.length == 10) {
      this.panNumberFlag = true;
    } else {
      this.panNumberFlag = false;
      // console.log('false');
    }
  }

  enginevalue(ev: any) {
    this.engingerr = false;
    this.valuelength = ev.target.value.length;
    // if(ev.target.value.length < 5)
    // {
    //   { swal({  text: 'Engine Number cannot be less than 5 digit, enter complete engine number' }); }
    // } else {
    var format = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    let value = ev.target.value;
    if (format.test(value)) {
      let text = this.cs.removeSpecial(value);
      this.proposalDetails.EngineNumber = text;
      this.engingerr = true;
    } else {
      console.log("false");
    }
    // }
  }

  chasisvalue(ev: any) {
    this.chassisflag = false;
    this.chasislength = ev.target.value.length;
    if (this.productCode == "2312" || this.productCode == "2320") {
      if (ev.target.value.length < 11 && this.policyType == "NEW") {
        // swal({  text: 'Chassis Number should be atleast 12 digit' });
        this.chasismsg = "Chassis Number should be atleast 12 digit";
      } else if (
        ev.target.value.length >= 11 &&
        ev.target.value.length <= 17 &&
        this.policyType == "NEW"
      ) {
        for (let i = ev.target.value.length; i < 17; i++) {
          let addval = 0;
          let newval = addval + this.proposalDetails.ChassisNumber;
          this.proposalDetails.ChassisNumber = newval;
          this.chassisflag = true;
        }
      }
      //   else if(ev.target.value.length < 5 && this.policyType == "ROLL")
      //   { swal({  text: 'Chassis Number cannot be less than 5 digit' }); }
      // } else if(this.productCode == '2311' || this.productCode == '2319'){
      //   if(ev.target.value.length < 17 && this.policyType == "ROLL")
      //   { swal({  text: 'Chassis Number cannot be less than 17 digit' }); }
      //   else if(ev.target.value.length < 5 && this.policyType == "ROLL")
      //   { swal({  text: 'Chassis Number cannot be less than 5 digit' }); }
    }
  }

  kilowattValue(ev: any) {
    let kw = ev.target.value;
    if (parseInt(this.proposalDetails.kilowatt) < 250 && this.proposalDetails.kilowatt != "0" && this.proposalDetails.kilowatt != "") {
      this.proposalDetails.kilowatt = kw+'V';
      this.productCode = commonData.allriskProductCode;
    } else {
      //this.productCode = "2312";
      swal({
        text: "Entered Kilowatts is greater than 0.25KW. Do you want to switch to Two-Wheeler ?",
        dangerMode: true,
        closeOnClickOutside: false,
        buttons: ["Yes", "No"],
      }).then((willDelete) => {
        if (willDelete) {
          console.log("Stay");
          this.productCode = commonData.allriskProductCode;
          let selectedEVmodel = JSON.parse(localStorage.getItem("SelectedEVModel"));
          let batteryVolt1 = selectedEVmodel[0].battery_Kv.split("-");
          let batteryVolt2 = selectedEVmodel[0].battery_Kv.split("/");
          if (batteryVolt1.length > 1) {
            this.proposalDetails.kilowatt = batteryVolt1[0];
          } else {
            this.proposalDetails.kilowatt = batteryVolt2[0];
          }
          // this.KiloWattList.push(this.proposalDetails.kilowatt);
          // this.proposalDetails.kilowatt = "249W";
          //this.freezeRTOCall();
        } else {
          this.productCode = "2312";
          this.getDealDetails();
          this.classCode = "37";
          this.policySubType = "1";
          this.refreshToken("roll", "RTO");
        }
      });
    }
  }

  calculateProposal() {
    (<any>window).ga("send", "event", {
      eventCategory: "Calculate Proposal",
      eventLabel: "" + this.product + "" + this.policyType,
      eventAction: "Proposal calculated",
      eventValue: 10,
    });
    console.log(this.quoteReq);
    if (
      this.quoteReq.PreviousPolicyDetails &&
      !this.quoteReq.IsNoPrevInsurance &&
      this.showsaod == false
    ) {
      this.quoteReq.PreviousPolicyDetails.PreviousPolicyNumber =
        this.proposalDetails.PreviousPolicyNumber;
      console.log(this.insurerlist);
      let previousInsurer: any = this.cs.PreviousInsurerList;;
      this.insurerlist = previousInsurer;
      this.companyCode = this.insurerlist.find(
        (c) => c.companyName == this.proposalDetails.PreviousInsurerName
      ).companyCode;
      this.quoteReq.PreviousPolicyDetails.PreviousInsurerName =
        this.companyCode;
    }

    this.paymentFlag = false;
    this.addressLine = this.proposalDetails.AddressLine1;
    let propdataforBack;
    propdataforBack = {
      AddressLine1: this.proposalDetails.AddressLine1,
      TotalNoOfODClaims: this.proposalDetails.TotalNoOfODClaims,
    };
    localStorage.setItem("propdataforBack", JSON.stringify(propdataforBack));
    let pType = JSON.parse(localStorage.getItem("breakinFlag"));

    let CustomerDetails = {
      CustomerName: this.proposalDetails.CustomerName,
      Gender: this.gender,
      CustomerMobile: this.proposalDetails.MobileNumber,
      Email: this.proposalDetails.Email,
      Address: this.proposalDetails.AddressLine1,
      pincode: this.proposalDetails.PinCode,
      city: this.proposalDetails.cityName,
      state: this.proposalDetails.stateName,
      panCardNo: this.proposalDetails.PANCardNo,
    };
    localStorage.setItem("CustomerDetails", JSON.stringify(CustomerDetails));

    if (pType == true) {
      this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
      this.productCode = this.paramDataValues.iPartnerLogin.product;
      if (this.quoteRes.PolicySubType == "1") {
        this.calculate();
      } else if (this.quoteRes.PolicySubType == "2") {
        this.calculate4w();
      } else if (this.productCode == "2320" || this.productCode == "2319") {
        this.calculate2WTP();
      } else if (this.productCode == commonData.allriskProductCode) {
        this.calculateAllRiskEBProposal();
      }
    } else {
      let savequote = JSON.parse(localStorage.getItem("savedQuotes"));
      if (savequote == true) {
        this.productCode = this.propReq.productCode;
      } else {
        this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
        this.productCode = this.paramDataValues.iPartnerLogin.product;
      }
      if (this.productCode == "2312") {
        this.calculate();
      } else if (this.productCode == "2311") {
        this.calculate4w();
      } else if (this.productCode == "2320" || this.productCode == "2319") {
        this.calculate2WTP();
      } else if (this.productCode == commonData.allriskProductCode) {
        this.calculateAllRiskEBProposal();
      }
    }
  }

  // Calculate 2w proposal
  calculate(): Promise<any> {
    return new Promise((resolve: any) => {
      let pType = JSON.parse(localStorage.getItem("breakinFlag"));
      let savequote = JSON.parse(localStorage.getItem("savedQuotes"));
      if (pType == true || savequote == true) {
        this.policyType = this.quoteReq.BusinessType;
      }
      if (pType == true || savequote == true) {
        this.exshow = this.quoteReq.ExShowRoomPrice;
      } else {
        this.exshow = String(this.quoteReq.ExShowRoomPrice);
      }
      let currentyear = moment(this.quoteReq.PolicyStartDate).format("YYYY");
      let leapcheck = moment(currentyear).isLeapYear();
      if (this.productCode == "2320" && this.policyType == "NEW") {
        this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
          .add(5, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      } else if (this.productCode == "2319" && this.policyType == "NEW") {
        this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
          .add(3, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      } else {
        this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
          .add(1, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      }
      if (
        this.breakinFlag == true ||
        this.saveQuote == true ||
        this.iasFlag == true
      ) {
        this.VBData();
      }

      /**
       * digitalPOS change
       * get values from localStorage for checking if conditions in html file
       * date :- 29-07-2021
       */
      if (this.IsPOSTransaction_flag == true) {
        this.posDealID = JSON.parse(localStorage.getItem("DealID"));
        this.deal_Id = this.posDealID;
      } else {
        this.deal_Id = this.quoteReq.DealId;
      }

      let body;
      // 2w new Request Body
      body = {
        //"BusinessType":"New Business",
        DealId: this.deal_Id,
        DeliveryOrRegistrationDate: this.quoteReq.DeliveryOrRegistrationDate,
        ManufacturingYear: String(this.quoteReq.ManufacturingYear),
        FirstRegistrationDate: this.quoteReq.FirstRegistrationDate,
        PolicyStartDate: this.quoteReq.PolicyStartDate,
        PolicyEndDate: this.policyEndDate,
        RegistrationNumber: this.quoteReq.RegistrationNumber,
        EngineNumber: this.proposalDetails.EngineNumber,
        ChassisNumber: this.proposalDetails.ChassisNumber,
        VehicleModelCode: this.quoteReq.VehicleModelCode,
        RTOLocationCode: String(this.quoteReq.RTOLocationCode),
        ExShowRoomPrice: this.exshow,
        VehicleMakeCode: this.quoteReq.VehicleMakeCode,
        Tenure: parseInt(this.quoteReq.Tenure),
        TPTenure: parseInt(this.quoteReq.TPTenure),
        PACoverTenure: parseInt(this.quoteReq.PACoverTenure),
        GSTToState: this.quoteReq.GSTToState,
        CustomerType: this.quoteReq.CustomerType,
        PreviousPolicyDetails: null,
        CorrelationId: this.quoteRes.correlationId,
        CustomerDetails: {
          CustomerType: "Individual",
          CustomerName: this.proposalDetails.CustomerName,
          // "DateOfBirth": moment(this.proposalDetails.DateOfBirth).format('YYYY-MM-DD'),
          PinCode: this.proposalDetails.PinCode,
          PANCardNo: this.proposalDetails.PANCardNo,
          Email: this.proposalDetails.Email,
          MobileNumber: this.proposalDetails.MobileNumber,
          AddressLine1: this.addressLine,
          CountryCode: this.CountryCode,
          StateCode: this.StateCode,
          CityCode: this.CityCode,
          MobileISD: "91",
          Gender: this.gender,
          AadharNumber: "",
        },
        QuoteID: this.quoteRes.quoteID,
        IsExtensionCountry: this.quoteReq.IsExtensionCountry,
        ExtensionCountryName: this.quoteReq.ExtensionCountryName,
        IsPACoverUnnamedPassenger: this.quoteReq.IsPACoverUnnamedPassenger,
        SIPACoverUnnamedPassenger: this.quoteReq.SIPACoverUnnamedPassenger,
        RSAPlanName: this.quoteReq.RSAPlanName,
        ZeroDepPlanName: this.quoteReq.ZeroDepPlanName,
        TPPDLimit: this.quoteReq.TPPDLimit,
        IsConsumables: this.quoteReq.IsConsumables,
        IsVoluntaryDeductible: this.quoteReq.IsVoluntaryDeductible,
        // "IsValidDrivingLicense":this.quoteReq.IsValidDrivingLicense,
        VoluntaryDeductiblePlanName: this.quoteReq.VoluntaryDeductiblePlanName,
        isNCBProtect: this.quoteReq.isNCBProtect,
        ncbProtectPlanName: this.quoteReq.ncbProtectPlanName,
        isGarageCash: this.quoteReq.isGarageCash,
        garageCashPlanName: this.quoteReq.garageCashPlanName,
        IsRTIApplicableflag: this.quoteReq.IsRTIApplicableflag,
        IsLegalLiabilityToPaidEmployee:
          this.quoteReq.IsLegalLiabilityToPaidEmployee,
        NoOfEmployee: this.quoteReq.NoOfEmployee,
        IsAntiTheftDisc: this.quoteReq.IsAntiTheftDisc,
        IsAutomobileAssocnFlag: this.quoteReq.IsAutomobileAssocnFlag,
        ChannelSource: "IAGENT",
        softCopyFlag: this.isSoftCopySelected ? "Yes" : "No",
      };

      if (this.quoteReq.Tenure == "1" && this.quoteReq.TPTenure == "0") {
        body.TPStartDate = this.quoteReq.TPStartDate;
        body.TPEndDate = this.quoteReq.TPEndDate;
        body.TPPolicyNo = this.quoteReq.TPPolicyNo;
        body.TPInsurerName = this.quoteReq.TPInsurerName;
        // For SAOD
        let dataInsurename = JSON.parse(localStorage.getItem('dataforBack'));
        let dataInsurename1 = JSON.parse(localStorage.getItem('calQuoteReq'));
        let previousInsurer: any = this.cs.PreviousInsurerList;
        if (!this.cs.isUndefineORNull(dataInsurename)) {
          //console.log(this.cs.PreviousInsurerList);
          this.insurerlist = previousInsurer;
          let data1 = this.insurerlist.find(
            (c) => c.companyName == dataInsurename.PreviousInsurerName
          );
          //console.log('Data1', data1);
          this.quoteReq.PreviousPolicyDetails.PreviousInsurerName = data1.companyCode;
          this.quoteReq.PreviousPolicyDetails.PreviousPolicyNumber = dataInsurename.PreviousPolicyNumber;
        }else{
          this.insurerlist = previousInsurer;
          let data1 = this.insurerlist.find(
            (c) => c.companyCode == dataInsurename1.PreviousPolicyDetails.PreviousInsurerName
          );
          //console.log('Data1', data1);
          this.quoteReq.PreviousPolicyDetails.PreviousInsurerName = data1.companyCode;
          this.quoteReq.PreviousPolicyDetails.PreviousPolicyNumber = dataInsurename1.PreviousPolicyDetails.PreviousPolicyNumber;
        }


      }
      body.IsValidDrivingLicense = this.quoteReq.IsValidDrivingLicense;

      let NomineeDetails;
      if (
        this.addNomineeVehicle == true &&
        this.proposalDetails.NameOfNominee != ""
      ) {
        NomineeDetails = {
          NomineeType: "PA-Owner Driver",
          NameOfNominee: this.proposalDetails.NameOfNominee,
          Age: this.proposalDetails.Age,
          Relationship: this.proposalDetails.Relationship,
        };
      }
      let GSTDetails;
      if (this.GSTDetails == true && this.proposalDetails.GSTInNumber != "") {
        GSTDetails = {
          GSTExemptionApplicable: "No",
          GSTInNumber: this.proposalDetails.GSTInNumber, // GSTIN/ UIN Number   // text box
          GSTToState: this.quoteReq.GSTToState, // no need to show on screen
          ConstitutionOfBusiness: this.constName, // Contrubution of business  // drop down
          CustomerType: this.custType, // Customer Type   // dropdown
          PanDetails: this.proposalDetails.PANCardNo, // Pan Details // text box
          GSTRegistrationStatus: this.gstName, // GST Registration status // Dropdown
        };
      }
      body.CustomerDetails.GSTDetails = GSTDetails;
      body.NomineeDetails = NomineeDetails;

      if (this.financierDetailsVal == true) {
        body.financierDetails = this.financierDetails;
      } else {
        body.financierDetails = null;
      }

      if (pType) {
        let OldReferenceNo = JSON.parse(localStorage.getItem("breakinData"));
        if (!this.cs.isUndefineORNull(OldReferenceNo)) {
          body.OldReferenceNo = OldReferenceNo.proposalNo;
        } else {
          body.OldReferenceNo = null;
        }
      } else {
        body.OldReferenceNo = null;
      }

      if (
        this.policyType == "ROLL" ||
        this.quoteRes.generalInformation.transactionType == "Roll Over"
      ) {
        if (this.usedcar == true) {
          body.BusinessType = "Used";
        } else {
          body.BusinessType = "Roll Over";
        }

        body.IsQCByPass = this.quoteReq.IsQCByPass;
        body.IsSelfInspection = this.quoteReq.IsSelfInspection;
        body.isNoPrevInsurance = this.cs.isUndefineORNull(
          this.quoteReq.IsNoPrevInsurance
        )
          ? this.quoteReq.isNoPrevInsurance
          : this.quoteReq.IsNoPrevInsurance;
        if (
          this.quoteReq.isNoPrevInsurance == false ||
          this.quoteReq.IsNoPrevInsurance == false
        ) {
          this.PreviousPolicyDetails.PreviousPolicyStartDate =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyStartDate : this.quoteReq.previousPolicyDetails.PreviousPolicyStartDate;
          this.PreviousPolicyDetails.PreviousPolicyEndDate =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate : this.quoteReq.previousPolicyDetails.PreviousPolicyEndDate;
          this.PreviousPolicyDetails.PreviousPolicyType =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyType : this.quoteReq.previousPolicyDetails.PreviousPolicyType;
          this.PreviousPolicyDetails.BonusOnPreviousPolicy =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.BonusOnPreviousPolicy : this.quoteReq.previousPolicyDetails.BonusOnPreviousPolicy;
          this.PreviousPolicyDetails.PreviousPolicyNumber =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyNumber : this.quoteReq.previousPolicyDetails.PreviousPolicyNumber;
          this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.ClaimOnPreviousPolicy : this.quoteReq.previousPolicyDetails.ClaimOnPreviousPolicy;
          this.PreviousPolicyDetails.TotalNoOfODClaims =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.TotalNoOfODClaims : this.quoteReq.previousPolicyDetails.TotalNoOfODClaims;
          this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy : this.quoteReq.previousPolicyDetails.NoOfClaimsOnPreviousPolicy;
          this.PreviousPolicyDetails.PreviousInsurerName =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousInsurerName : this.quoteReq.previousPolicyDetails.PreviousInsurerName;
          this.PreviousPolicyDetails.PreviousVehicleSaleDate =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousVehicleSaleDate : this.quoteReq.previousPolicyDetails.PreviousVehicleSaleDate;
          this.PreviousPolicyDetails.PreviousPolicyTenure =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyTenure : this.quoteReq.previousPolicyDetails.PreviousPolicyTenure;
          if (this.breakinFlag == true) {
            this.PreviousPolicyDetails.PreviousPolicyNumber =
              this.propReq.PreviousPolicyDetails.PreviousPolicyNumber;
          }
          body.PreviousPolicyDetails = this.PreviousPolicyDetails;
        }
      } else {
        if (this.usedcar == true) {
          body.BusinessType = "Used";
        } else {
          body.BusinessType = "New Business";
        }

        if (
          (this.quoteReq.PreviousPolicyDetails != undefined ||
            this.quoteReq.PreviousPolicyDetails != null) &&
          !this.quoteReq.IsNoPrevInsurance
        ) {
          this.PreviousPolicyDetails.PreviousPolicyType =
            this.quoteReq.PreviousPolicyDetails.PreviousPolicyType;
          this.PreviousPolicyDetails.PreviousPolicyStartDate =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyStartDate : this.quoteReq.previousPolicyDetails.PreviousPolicyStartDate;
          this.PreviousPolicyDetails.PreviousPolicyEndDate =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate : this.quoteReq.previousPolicyDetails.PreviousPolicyEndDate;
          this.PreviousPolicyDetails.PreviousPolicyType =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyType : this.quoteReq.previousPolicyDetails.PreviousPolicyType;
          this.PreviousPolicyDetails.BonusOnPreviousPolicy =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.BonusOnPreviousPolicy : this.quoteReq.previousPolicyDetails.BonusOnPreviousPolicy;
          this.PreviousPolicyDetails.PreviousPolicyNumber =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyNumber : this.quoteReq.previousPolicyDetails.PreviousPolicyNumber;
          this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.ClaimOnPreviousPolicy : this.quoteReq.previousPolicyDetails.ClaimOnPreviousPolicy;
          this.PreviousPolicyDetails.TotalNoOfODClaims =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.TotalNoOfODClaims : this.quoteReq.previousPolicyDetails.TotalNoOfODClaims;
          this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy : this.quoteReq.previousPolicyDetails.NoOfClaimsOnPreviousPolicy;
          this.PreviousPolicyDetails.PreviousInsurerName =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousInsurerName : this.quoteReq.previousPolicyDetails.PreviousInsurerName;
          this.PreviousPolicyDetails.PreviousVehicleSaleDate =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousVehicleSaleDate : this.quoteReq.previousPolicyDetails.PreviousVehicleSaleDate;
          this.PreviousPolicyDetails.PreviousPolicyTenure =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyTenure : this.quoteReq.previousPolicyDetails.PreviousPolicyTenure;
          body.PreviousPolicyDetails = this.PreviousPolicyDetails;
        }
        body.IsTransferOfNCB = this.quoteReq.IsTransferOfNCB;
        body.TransferOfNCBPercent = this.quoteReq.TransferOfNCBPercent;
      }

      if (this.quoteReq.OtherDiscount) {
        body.OtherDiscount = this.quoteReq.OtherDiscount;
      }

      if (this.isInstaProposalRequired) {
        body.isAutoInstaTagging = true;
      }

      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      if (this.quoteRes.breakingFlag) {
        this.createBreakInReq(str);
      } else {
        this.getINSTAProposalPKG(str, "");
      }
      // this.cs.postBiz("/proposal/twowheelercreateproposal", str).subscribe(
      //   (res: any) => {
      //     if (res.status == "Success") {
      //       localStorage.setItem("PropDatareq", JSON.stringify(body));
      //       localStorage.setItem("PropDatares", JSON.stringify(res));
      //       this.calculatedData = res;
      //       this.breakInDeviationFlag = res.breakingFlag;
      //       if (res.isQuoteDeviation == true) {
      //         this.cs.loaderStatus = false;
      //         this.isPropDeviation = true;
      //         if (res.breakingFlag == true) {                
      //           if (this.breakintype.breakin != "preApprovedBreakIN") {
      //             this.cs.loaderStatus = false;
      //             this.openBreakinModal();
      //             // this.saveofflineProposal();
      //           } else {
      //             this.saveofflineProposal().then(() => {
      //               if (this.saveProposalStatus == "Success") {
      //                 this.cs.loaderStatus = false;
      //                 this.PreBreakINDataresponse = JSON.parse(
      //                   localStorage.getItem("PreBreakINData")
      //                 );
      //                 this.saveOfflineBreakinId(this.PreBreakINDataresponse);
      //               } else {
      //                 this.cs.loaderStatus = false;
      //               }
      //             });
      //           }
      //           this.deviationMsg =
      //             "This case would go into deviation" + res.deviationMessage;
      //           this.getProposalDetails(res.generalInformation.proposalNumber);
      //         } else {
      //           if (this.productCode == "2312" && this.policyType != "NEW") {
      //             this.deviationMsg =
      //               "This case would go into deviation" + res.deviationMessage;
      //             this.getProposalDetails(
      //               res.generalInformation.proposalNumber
      //             );
      //           } else {
      //             swal({
      //               text: "This case would go into deviation. Would you like to to continue?",
      //               dangerMode: true,
      //               closeOnClickOutside: false,
      //               buttons: ["Yes", "No"],
      //             }).then((willDelete) => {
      //               if (willDelete) {
      //                 console.log("Stay");
      //               } else {
      //                 let type;
      //                 if (this.policyType == "NEW") {
      //                   type = "N";
      //                 } else {
      //                   type = "R";
      //                 }
      //                 this.cs.geToDashboardapproval(this.productCode, type);
      //               }
      //             });
      //           }
      //         }
      //       } else {
      //         this.calculatedData = res;
      //         this.finalamount = Math.round(this.calculatedData.finalPremium);
      //         this.paymentFlag = true;
      //         // localStorage.setItem("PropDatareq", JSON.stringify(body));
      //         // localStorage.setItem("PropDatares", JSON.stringify(res));

      //         if (
      //           this.calculatedData.breakingFlag == true &&
      //           this.breakinFlag != true
      //         ) {
      //           if (this.breakintype.breakin != "preApprovedBreakIN") {
      //             this.cs.loaderStatus = false;
      //             // this.getSubLocationCities();
      //             this.openBreakinModal();
      //           } else {
      //             this.saveofflineProposal().then(() => {
      //               if (this.saveProposalStatus == "Success") {
      //                 this.cs.loaderStatus = false;
      //                 this.PreBreakINDataresponse = JSON.parse(
      //                   localStorage.getItem("PreBreakINData")
      //                 );
      //                 this.saveOfflineBreakinId(this.PreBreakINDataresponse);
      //               } else {
      //                 this.cs.loaderStatus = false;
      //               }
      //             });
      //           }
      //         } else {
      //           this.saveofflineProposal().then(() => {
      //             if (this.saveProposalStatus == "Success") {
      //               this.checkForNewPaymentModule();
      //             } else {
      //               swal({
      //                 text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
      //               });
      //             }
      //           });
      //         }
      //       }

      //       // this.cs.loaderStatus = false;
      //       resolve();
      //     } else {
      //       this.cs.loaderStatus = false;
      //       let errormsg = res.message;
      //       // Common MSG
      //       // this.api.handleerrors(errormsg);
      //       let errMsg = this.msgService.errorMsg(errormsg)
      //       swal({ closeOnClickOutside: false, text: errMsg });
      //       let errorbody = {
      //         RequestJson: JSON.stringify(body),
      //         ResponseJson: JSON.stringify(res),
      //         ServiceURL:
      //           commonData.bizURL + "/proposal/twowheelercreateproposal",
      //         CorrelationID: this.quoteRes.correlationId,
      //       };
      //       this.api.adderrorlogs(errorbody);
      //       this.calculatedData = null;
      //       resolve();
      //     }
      //   },
      //   (err) => {
      //     let errMsg = this.msgService.errorMsg(err)
      //     swal({ closeOnClickOutside: false, text: errMsg });
      //     // swal({ closeOnClickOutside: false, text: 'At present system response is slow, kindly try after sometime. We regret the inconvenience' })
      //     resolve();
      //   }
      // );
      // .catch((err: any) => {
      //   this.cs.loaderStatus = false;
      //   swal({
      //     text: 'At present system response is slow, kindly try after sometime. We regret the inconvenience'
      //   })
      //   resolve();
      // });
    });
  }

  getSrt(srt: any) {
    console.log("SRT", srt);
    this.selectedSRT = srt;
  }

  // Create IAS token on Raise Interaction - Sejal,
  // called createToken in html at Raise Interaction button instead of raiseInteraction()
  createToken() {
    this.cs.createIASToken().then(() => {
      this.raiseInteraction();
    });
  }

  // Deviation Changes
  raiseInteraction() {
    if (
      this.srt.includes("IDV Deviation ") &&
      !this.isIDVIntRaised &&
      this.selectedSRT == "IDV Deviation "
    ) {
      this.raisedIDVInteration();
      this.isIASRaised = true;
    } else if (
      this.srt.includes("Other Discounting ") &&
      !this.isODIntRaised &&
      this.selectedSRT == "Other Discounting "
    ) {
      this.raisedODInteraction();
      this.isIASRaised = true;
    } else {
      this.isIASRaised = false;
    }
  }

  raisedIDVInteration() {
    let productName;
    let vehicleType;
    let authData = JSON.parse(localStorage.getItem("AuthToken"));
    if (this.productCode == "2312" || this.productCode == "2320") {
      productName = "TW";
      vehicleType = "MOTORCYCLE";
    } else if (this.productCode == "2311" || this.productCode == "2319") {
      productName = "Pvt Car";
      vehicleType = "PRIVATE CAR";
    }

    let exShowData = JSON.parse(localStorage.getItem("exShowData"));
    console.log("IAS Req", this.quoteReq);
    console.log(this.iasProcess, this.iasProcess.includes("IDV Deviation UW Approval Level II "));
    let body = {
      CorrelationId: this.quoteRes.correlationId,
      Actcode: "000",
      ApplicationName: "IAS",
      SRT: "IDV Deviation ",
      Process: this.iasProcess.includes("IDV Deviation UW Approval Level II ") ? "IDV Deviation UW Approval Level II " : "IDV Deviation PVT Car Level I ",
      CreatorId: this.dealDetailsfromIM.hR_REF_NO,
      ActionType: "INSERT",
      UI: [
        {
          Legend: "Proposal No.",
          Value: this.calculatedData.generalInformation.proposalNumber,
        },
        {
          Legend: "Cover Note Mode",
          Value: "AUTO",
        },
        {
          Legend: "Location",
          Value: this.dealDetailsfromIM.officeName,
        },
        {
          Legend: "Business Type",
          Value:
            this.quoteReq.BusinessType == "New Business" ? "New" : "Rollover",
        },
        {
          Legend: "No of Vehicles for which approval is required",
          Value: "1",
        },
        {
          Legend: "Manufacturing year",
          Value: String(this.quoteReq.ManufacturingYear),
        },
        {
          Legend: "RTO Location",
          Value: this.quoteRes.generalInformation.rtoLocation,
        },
        {
          Legend: "Manufacturer",
          Value: this.quoteRes.generalInformation.manufacturerName,
        },
        {
          Legend: "Model",
          Value: this.quoteRes.generalInformation.vehicleModel,
        },
        {
          Legend: "Remarks",
          Value: "User entry in IAS",
        },
        {
          Legend: "Previous year NCB",
          Value: "0",
        },
        {
          Legend: "Current year NCB",
          Value: "20",
        },
        {
          Legend: "Deal No",
          Value: this.dealDetailsfromIM.dealID,
        },
        {
          Legend: "Vertical",
          Value: this.dealDetailsfromIM.primaryVerticalName,//"AGENCY",
        },
        {
          Legend: "Requestor name",
          Value: this.dealDetailsfromIM.intermediaryName,
        },
        {
          Legend: "Policy Type",
          Value: "Comprehensive", //this.policyType,
        },
        {
          Legend: "Product",
          Value: productName,
        },
        {
          Legend: "Transaction Type",
          Value: "Intermediary",
        },
        {
          Legend: "Agent Name",
          Value: this.dealDetailsfromIM.employeeName,
        },
        {
          Legend: "Insured Name",
          Value: this.proposalDetails.CustomerName,
        },
        {
          Legend: "ECN OR Registration No",
          Value: this.quoteReq.RegistrationNumber,
        },
        {
          Legend: "Vehicle type",
          Value: vehicleType,
        },
        {
          Legend: "SM Vertical",
          Value: this.dealDetailsfromIM.primaryVerticalName,//"Agency",
        },
        {
          Legend: "SM Name",
          Value: this.dealDetailsfromIM.intermediaryName,
        },
        {
          Legend: "Cover Note No",
          Value: "0",
        },
        {
          Legend: "Policy No",
          Value: this.calculatedData.generalInformation.proposalNumber,
        },
        {
          Legend: "Deal ID",
          Value: this.dealDetailsfromIM.dealID,
        },
        {
          Legend: "Agent Code",
          Value: this.dealDetailsfromIM.intermediaryCode,
        },
        {
          Legend: "Showroom price",
          Value: exShowData.maximumprice,
        },
        {
          Legend: "IDV (as per system)",
          Value:
            exShowData.maximumprice -
            exShowData.maximumprice * exShowData.idvdepreciationpercent,
        },
        {
          Legend: "IDV (as required)",
          Value:
            exShowData.maximumprice -
            exShowData.maximumprice * exShowData.idvdepreciationpercent,
        },
        {
          Legend: "IDV deviation Side",
          Value:
            exShowData.minidv >
              exShowData.maximumprice -
              exShowData.maximumprice * exShowData.idvdepreciationpercent
              ? "Lower Side"
              : "Higher Side",
        },
        {
          Legend: "ECN / registration details",
          Value: this.calculatedData.generalInformation.proposalNumber,
        },
        {
          Legend: "Total IDV deviation (in %)",
          Value: "0.3",
        },
      ],
    };

    this.ineractionSpinner = true;
    this.iasIDVReq = JSON.stringify(body);
    this.cs.postILSBIAS("/Genus/IASInteractionID", body).subscribe(
      (res: any) => {
        console.log(res);
        // this.iasIntRes = res;
        // this.iasIDVRes = res;
        if (res.status == "Success") {
          this.iasIntRes = res;
          this.iasIDVRes = res;
          this.ineractionSpinner = false;
          this.isIDVIntRaised = true;
          if (!this.breakinFlag) {
            this.saveofflineProposal();
          }

          // this.router.navigateByUrl("/quote").then(() => {
          //   this.cs.clearLocalStorage();
          //   this.cs.clearQuoteDate();
          //   this.cs.clearPropData();
          //   // window.location.reload();
          //   this.router
          //     .navigateByUrl("/", { skipLocationChange: true })
          //     .then(() => this.router.navigate(["/quote"]));
          // });
        } else {
          // this.router.navigateByUrl("/quote").then(() => {
          //   this.router
          //     .navigateByUrl("/", { skipLocationChange: true })
          //     .then(() => this.router.navigate(["/quote"]));
          // });
          if (!this.breakinFlag) {
            this.saveofflineProposal();
          }
          this.ineractionSpinner = false;
        }
      },
      (err) => {
        let errMsg = this.msgService.errorMsg(err)
        swal({ closeOnClickOutside: false, text: errMsg });
        this.ineractionSpinner = false;
        this.router.navigateByUrl("/quote").then(() => {
          this.router
            .navigateByUrl("/", { skipLocationChange: true })
            .then(() => this.router.navigate(["/quote"]));
        });
      }
    );
  }

  raisedODInteraction() {
    let productName;
    let vehicleType;
    let body;
    let authData = JSON.parse(localStorage.getItem("AuthToken"));
    if (this.productCode == "2312" || this.productCode == "2320") {
      productName = "TW";
      vehicleType = "MOTORCYCLE";
    } else if (this.productCode == "2311" || this.productCode == "2319") {
      productName = "Pvt Car";
      vehicleType = "PRIVATE CAR";
    }

    if (
      this.iasProcess.includes("Other Discounting process related approvals")
    ) {
      body = {
        CorrelationId: this.quoteRes.correlationId,
        Actcode: "000",
        ApplicationName: "IAS",
        SRT: this.srt,
        Process: this.iasProcess,
        CreatorId: this.dealDetailsfromIM.hR_REF_NO,
        ActionType: "INSERT",
        UI: [
          {
            Legend: "Proposal No.",
            Value: this.calculatedData.generalInformation.proposalNumber,
          },
          {
            Legend: "Cover Note Mode",
            Value: "AUTO",
          },
          {
            Legend: "Location",
            Value: this.dealDetailsfromIM.officeName,
          },
          {
            Legend: "Business Type",
            Value:
              this.quoteReq.BusinessType == "New Business" ? "New" : "Rollover",
          },
          {
            Legend: "No of Vehicles for which approval is required",
            Value: "1",
          },
          {
            Legend: "Manufacturing year",
            Value: String(this.quoteReq.ManufacturingYear),
          },
          {
            Legend: "RTO Location",
            Value: this.quoteRes.generalInformation.rtoLocation,
          },
          {
            Legend: "Manufacturer",
            Value: this.quoteRes.generalInformation.manufacturerName,
          },
          {
            Legend: "Model",
            Value: this.quoteRes.generalInformation.vehicleModel,
          },
          {
            Legend: "Remarks",
            Value: "User entry in IAS",
          },
          {
            Legend: "Previous year NCB",
            Value: "0",
          },
          {
            Legend: "Current year NCB",
            Value: "20",
          },
          {
            Legend: "Deal No",
            Value: this.dealDetailsfromIM.dealID,
          },
          {
            Legend: "Vertical",
            Value: this.dealDetailsfromIM.primaryVerticalName,//"AGENCY",
          },
          {
            Legend: "Policy Type",
            Value: "Package", //this.policyType
          },
          {
            Legend: "Product",
            Value: productName,
          },
          {
            Legend: "Transaction Type",
            Value: "Intermediary",
          },
          {
            Legend: "Agent Name",
            Value: this.dealDetailsfromIM.employeeName,
          },
          {
            Legend: "Insured Name",
            Value: this.proposalDetails.CustomerName,
          },
          {
            Legend: "ECN OR Registration No",
            Value: this.quoteReq.RegistrationNumber,
          },
          {
            Legend: "Vehicle type",
            Value: vehicleType,
          },
          {
            Legend: "SM Vertical",
            Value: this.dealDetailsfromIM.primaryVerticalName,//"Agency",
          },
          {
            Legend: "SM Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Cover Note No",
            Value: "0",
          },
          {
            Legend: "Policy No",
            Value: this.calculatedData.generalInformation.proposalNumber,
          },
          {
            Legend: "Payout involved (in %)",
            Value: "0",
          },
          {
            Legend: "Vehicle type/Subclass",
            Value: "Private",
          },
          {
            Legend: "Intermediary Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Proposal No",
            Value: this.calculatedData.generalInformation.proposalNumber,
          },
          {
            Legend: "ECN / registration no. details",
            Value: this.quoteReq.RegistrationNumber,
          },
          {
            Legend: "System built in discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Required discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Requestor name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "IM ID",
            Value: authData.username,
          },
          {
            Legend: "Intermediary code",
            Value: this.dealDetailsfromIM.intermediaryCode,
          },
          {
            Legend: "Vehicle Usage",
            Value: "Others",
          },
          {
            Legend: "Area of Operation",
            Value: "INTER-CITY",
          },
          {
            Legend: "Vehicle Driven By",
            Value: "Others",
          },
          {
            Legend: "Approval process",
            Value: "Select",
          },
          {
            Legend: "Zero Dep covered",
            Value: "No",
          },
          {
            Legend: "Geo Sub vertical",
            Value: "Others",
          },
          {
            Legend: "Car segment",
            Value: "non premium segment", //this.fetchProposalData[0].carSegment,
          },
        ],
      };
    } else if (this.iasProcess.includes("Other discounting Non New Hyundai")) {
      body = {
        CorrelationId: this.quoteRes.correlationId,
        Actcode: "000",
        ApplicationName: "IAS",
        SRT: this.srt,
        Process: this.iasProcess,
        CreatorId: this.dealDetailsfromIM.hR_REF_NO,
        ActionType: "INSERT",
        UI: [
          {
            Legend: "Proposal No.",
            Value: this.calculatedData.generalInformation.proposalNumber,
          },
          {
            Legend: "Cover Note Mode",
            Value: "AUTO",
          },
          {
            Legend: "Location",
            Value: this.dealDetailsfromIM.officeName,
          },
          {
            Legend: "Business Type",
            Value:
              this.quoteReq.BusinessType == "New Business" ? "New" : "Rollover",
          },
          {
            Legend: "No of Vehicles for which approval is required",
            Value: "1",
          },
          {
            Legend: "Manufacturing year",
            Value: String(this.quoteReq.ManufacturingYear),
          },
          {
            Legend: "RTO Location",
            Value: this.quoteRes.generalInformation.rtoLocation,
          },
          {
            Legend: "Manufacturer",
            Value: this.quoteRes.generalInformation.manufacturerName,
          },
          {
            Legend: "Model",
            Value: this.quoteRes.generalInformation.vehicleModel,
          },
          {
            Legend: "Remarks",
            Value: "User entry in IAS",
          },
          {
            Legend: "Previous year NCB",
            Value: "0",
          },
          {
            Legend: "Current year NCB",
            Value: "20",
          },
          {
            Legend: "Deal No",
            Value: this.dealDetailsfromIM.dealID,
          },
          {
            Legend: "Vertical",
            Value: this.dealDetailsfromIM.primaryVerticalName,//"AGENCY",
          },
          {
            Legend: "Policy Type",
            Value: "Package", //this.policyType
          },
          {
            Legend: "Product",
            Value: productName,
          },
          {
            Legend: "Transaction Type",
            Value: "Intermediary",
          },
          {
            Legend: "Agent Name",
            Value: this.dealDetailsfromIM.employeeName,
          },
          {
            Legend: "Insured Name",
            Value: this.proposalDetails.CustomerName,
          },
          {
            Legend: "ECN OR Registration No",
            Value: this.quoteReq.RegistrationNumber,
          },
          {
            Legend: "Vehicle type",
            Value: vehicleType,
          },
          {
            Legend: "SM Vertical",
            Value: this.dealDetailsfromIM.primaryVerticalName,//"Agency",
          },
          {
            Legend: "SM Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Cover Note No",
            Value: "0",
          },
          {
            Legend: "Policy No",
            Value: this.calculatedData.generalInformation.proposalNumber,
          },
          {
            Legend: "Payout involved (in %)",
            Value: "0",
          },
          {
            Legend: "Vehicle type/Subclass",
            Value: "Private",
          },
          {
            Legend: "Intermediary Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Proposal No",
            Value: this.calculatedData.generalInformation.proposalNumber,
          },
          {
            Legend: "ECN / registration no. details",
            Value: this.quoteReq.RegistrationNumber,
          },
          {
            Legend: "System built in discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Required discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Requestor name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "IM ID",
            Value: authData.username,
          },
          {
            Legend: "Intermediary code",
            Value: this.dealDetailsfromIM.intermediaryCode,
          },
          {
            Legend: "Vehicle Usage",
            Value: "Others",
          },
          {
            Legend: "Area of Operation",
            Value: "INTER-CITY",
          },
          {
            Legend: "Vehicle Driven By",
            Value: "Others",
          },
          {
            Legend: "Approval process",
            Value: "Select",
          },
          {
            Legend: "Zero Dep covered",
            Value: "No",
          },
          {
            Legend: "Geo Sub vertical",
            Value: "M2",
          },
          {
            Legend: "Car segment",
            Value: "non premium segment", //this.fetchProposalData[0].carSegment,
          },
        ],
      };
    } else if (this.iasProcess.includes("Other discounting Non New Toyota")) {
      body = {
        CorrelationId: this.quoteRes.correlationId,
        Actcode: "000",
        ApplicationName: "IAS",
        SRT: this.srt,
        Process: this.iasProcess,
        CreatorId: this.dealDetailsfromIM.hR_REF_NO,
        ActionType: "INSERT",
        UI: [
          {
            Legend: "Proposal No.",
            Value: this.calculatedData.generalInformation.proposalNumber,
          },
          {
            Legend: "Cover Note Mode",
            Value: "AUTO",
          },
          {
            Legend: "Location",
            Value: this.dealDetailsfromIM.officeName,
          },
          {
            Legend: "Business Type",
            Value:
              this.quoteReq.BusinessType == "New Business" ? "New" : "Rollover",
          },
          {
            Legend: "No of Vehicles for which approval is required",
            Value: "1",
          },
          {
            Legend: "Manufacturing year",
            Value: String(this.quoteReq.ManufacturingYear),
          },
          {
            Legend: "RTO Location",
            Value: this.quoteRes.generalInformation.rtoLocation,
          },
          {
            Legend: "Manufacturer",
            Value: this.quoteRes.generalInformation.manufacturerName,
          },
          {
            Legend: "Model",
            Value: this.quoteRes.generalInformation.vehicleModel,
          },
          {
            Legend: "Remarks",
            Value: "User entry in IAS",
          },
          {
            Legend: "Previous year NCB",
            Value: "0",
          },
          {
            Legend: "Current year NCB",
            Value: "20",
          },
          {
            Legend: "Deal No",
            Value: this.dealDetailsfromIM.dealID,
          },
          {
            Legend: "Vertical",
            Value: this.dealDetailsfromIM.primaryVerticalName,//"AGENCY",
          },
          {
            Legend: "Policy Type",
            Value: "Package", //this.policyType
          },
          {
            Legend: "Product",
            Value: productName,
          },
          {
            Legend: "Transaction Type",
            Value: "Intermediary",
          },
          {
            Legend: "Agent Name",
            Value: this.dealDetailsfromIM.employeeName,
          },
          {
            Legend: "Insured Name",
            Value: this.proposalDetails.CustomerName,
          },
          {
            Legend: "ECN OR Registration No",
            Value: this.quoteReq.RegistrationNumber,
          },
          {
            Legend: "Vehicle type",
            Value: vehicleType,
          },
          {
            Legend: "SM Vertical",
            Value: this.dealDetailsfromIM.primaryVerticalName,//"Agency",
          },
          {
            Legend: "SM Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Cover Note No",
            Value: "0",
          },
          {
            Legend: "Policy No",
            Value: this.calculatedData.generalInformation.proposalNumber,
          },
          {
            Legend: "Payout involved (in %)",
            Value: "0",
          },
          {
            Legend: "Vehicle type/Subclass",
            Value: "PRIVATE CAR",
          },
          {
            Legend: "Intermediary Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Proposal No",
            Value: this.calculatedData.generalInformation.proposalNumber,
          },
          {
            Legend: "ECN / registration no. details",
            Value: this.quoteReq.RegistrationNumber,
          },
          {
            Legend: "System built in discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Required discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Requestor name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "IM ID",
            Value: authData.username,
          },
          {
            Legend: "Intermediary code",
            Value: this.dealDetailsfromIM.intermediaryCode,
          },
          {
            Legend: "Vehicle Usage",
            Value: "Others",
          },
          {
            Legend: "Area of Operation",
            Value: "INTER-CITY",
          },
          {
            Legend: "Vehicle Driven By",
            Value: "Others",
          },
          {
            Legend: "Approval process",
            Value: "Select",
          },
          {
            Legend: "Zero Dep covered",
            Value: "No",
          },
          {
            Legend: "Geo Sub vertical",
            Value: "M2",
          },
          {
            Legend: "Car segment",
            Value: "non premium segment", //this.fetchProposalData[0].carSegment,
          },
        ],
      };
    } else {
      body = {
        CorrelationId: this.quoteRes.correlationId,
        Actcode: "000",
        ApplicationName: "IAS",
        SRT: "Other Discounting",
        Process: "Other Discounting exceptional approvals",
        CreatorId: this.dealDetailsfromIM.hR_REF_NO,
        ActionType: "INSERT",
        UI: [
          {
            Legend: "Proposal No.",
            Value: this.calculatedData.generalInformation.proposalNumber,
          },
          {
            Legend: "Cover Note Mode",
            Value: "AUTO",
          },
          {
            Legend: "Location",
            Value: this.dealDetailsfromIM.officeName,
          },
          {
            Legend: "Business Type",
            Value:
              this.quoteReq.BusinessType == "New Business" ? "New" : "Rollover",
          },
          {
            Legend: "No of Vehicles for which approval is required",
            Value: "1",
          },
          {
            Legend: "Manufacturing year",
            Value: String(this.quoteReq.ManufacturingYear),
          },
          {
            Legend: "RTO Location",
            Value: this.quoteRes.generalInformation.rtoLocation,
          },
          {
            Legend: "Manufacturer",
            Value: this.quoteRes.generalInformation.manufacturerName,
          },
          {
            Legend: "Model",
            Value: this.quoteRes.generalInformation.vehicleModel,
          },
          {
            Legend: "Remarks",
            Value: "User entry in IAS",
          },
          {
            Legend: "Previous year NCB",
            Value: "0",
          },
          {
            Legend: "Current year NCB",
            Value: "20",
          },
          {
            Legend: "Deal No",
            Value: this.dealDetailsfromIM.dealID,
          },
          {
            Legend: "Vertical",
            Value: this.dealDetailsfromIM.primaryVerticalName,//"AGENCY",
          },
          {
            Legend: "Policy Type",
            Value: "Package", //this.policyType
          },
          {
            Legend: "Product",
            Value: productName,
          },
          {
            Legend: "Transaction Type",
            Value: "Intermediary",
          },
          {
            Legend: "Agent Name",
            Value: this.dealDetailsfromIM.employeeName,
          },
          {
            Legend: "Insured Name",
            Value: this.proposalDetails.CustomerName,
          },
          {
            Legend: "ECN OR Registration No",
            Value: this.quoteReq.RegistrationNumber,
          },
          {
            Legend: "Vehicle type",
            Value: vehicleType,
          },
          {
            Legend: "SM Vertical",
            Value: this.dealDetailsfromIM.primaryVerticalName,//"Agency",
          },
          {
            Legend: "SM Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Cover Note No",
            Value: "0",
          },
          {
            Legend: "Policy No",
            Value: this.calculatedData.generalInformation.proposalNumber,
          },
          {
            Legend: "Payout involved (in %)",
            Value: "0",
          },
          {
            Legend: "Vehicle type/Subclass",
            Value: "Private",
          },
          {
            Legend: "Intermediary Name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Proposal No",
            Value: this.calculatedData.generalInformation.proposalNumber,
          },
          {
            Legend: "ECN / registration no. details",
            Value: this.quoteReq.RegistrationNumber,
          },
          {
            Legend: "System built in discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Required discount on IMT (in %)",
            Value: this.fetchProposalData[0].imtDiscount,
          },
          {
            Legend: "Requestor name",
            Value: this.dealDetailsfromIM.intermediaryName,
          },
          {
            Legend: "Pathfinder IM ID",
            Value: authData.username,
          },
          {
            Legend: "Intermediary code or ID",
            Value: this.dealDetailsfromIM.intermediaryCode,
          },
          {
            Legend: "Vehicle Usage",
            Value: "Others",
          },
          {
            Legend: "Area of Operation",
            Value: "INTER-CITY",
          },
          {
            Legend: "Vehicle Driven By",
            Value: "Others",
          },
        ],
      };
    }

    this.ineractionSpinner = true;
    this.iasODReq = JSON.stringify(body);
    this.cs.postILSBIAS("/Genus/IASInteractionID", body).subscribe(
      (res: any) => {
        console.log(res);
        // this.iasIntRes = res;
        // this.iasODRes = res;
        if (res.status == "Success") {
          this.isODIntRaised = true;
          this.iasIntRes = res;
          this.iasODRes = res;
          this.ineractionSpinner = false;
          if (!this.breakinFlag) {
            this.saveofflineProposal();
          }
          // this.saveofflineProposal();
          // this.router.navigateByUrl("quote");
        } else {
          this.router.navigateByUrl("quote");
          this.ineractionSpinner = false;
          if (!this.breakinFlag) {
            this.saveofflineProposal();
          }
        }
      },
      (err) => {
        let errMsg = this.msgService.errorMsg(err)
        swal({ closeOnClickOutside: false, text: errMsg });
        this.ineractionSpinner = false;
        this.router.navigateByUrl("quote");
      }
    );
  }

  // Calculate 2WTP Proposal
  calculate2WTP() {
    if (this.productCode == "2320" && this.policyType == "NEW") {
      this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
        .add(5, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    } else if (this.productCode == "2319" && this.policyType == "NEW") {
      this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
        .add(3, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    } else {
      this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
        .add(1, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    }
    if (
      this.breakinFlag == true ||
      this.saveQuote == true ||
      this.iasFlag == true
    ) {
      this.VBData();
    }
    let body, URL;
    this.policyEndDate = this.quoteReq.PolicyEndDate;

    /**
     * Digital POS change
     * get values from localStorage for checking if conditions in html file
     * author :- digitalPOS
     * date :- 15-07-2021
     */
    if (this.IsPOSTransaction_flag == true) {
      this.posDealID = JSON.parse(localStorage.getItem("DealID"));
      this.deal_Id = this.posDealID;
    } else {
      this.deal_Id = this.quoteReq.DealId;
    }
    body = {
      ChannelSource: "IAGENT",
      softCopyFlag: this.isSoftCopySelected ? "Yes" : "No",
      DealId: this.deal_Id,
      DeliveryOrRegistrationDate: this.quoteReq.DeliveryOrRegistrationDate,
      FirstRegistrationDate: this.quoteReq.FirstRegistrationDate,
      PolicyStartDate: this.quoteReq.PolicyStartDate,
      PolicyEndDate: this.policyEndDate,
      ManufacturingYear: String(this.quoteReq.ManufacturingYear),
      VehicleModelCode: String(this.quoteReq.VehicleModelCode),
      RTOLocationCode: String(this.quoteReq.RTOLocationCode),
      // IsNoPrevInsurance: false,
      TPPDLimit: this.quoteReq.TPPDLimit,
      ExShowRoomPrice: String(this.quoteReq.ExShowRoomPrice),
      VehicleMakeCode: String(this.quoteReq.VehicleMakeCode),
      GSTToState: this.quoteReq.GSTToState,
      CustomerType: this.quoteReq.CustomerType,
      CorrelationId: this.quoteRes.correlationId,
      EngineNumber: this.proposalDetails.EngineNumber,
      ChassisNumber: this.proposalDetails.ChassisNumber,
      RegistrationNumber: this.quoteReq.RegistrationNumber,
      CustomerDetails: {
        CustomerType: "INDIVIDUAL",
        CustomerName: this.proposalDetails.CustomerName,
        // "DateOfBirth": moment(this.proposalDetails.DateOfBirth).format('YYYY-MM-DD'),
        PinCode: this.proposalDetails.PinCode,
        PANCardNo: this.proposalDetails.PANCardNo,
        Email: this.proposalDetails.Email,
        MobileNumber: this.proposalDetails.MobileNumber,
        AddressLine1: this.addressLine,
        CountryCode: this.CountryCode,
        StateCode: this.StateCode,
        CityCode: this.CityCode,
        Gender: this.gender,
        GSTDetails: null,
        AadharNumber: "",
        IsCollectionofform60: false,
        AadharEnrollmentNo: null,
        eIA_Number: "",
      },
    };

    if (this.quoteReq.IsLegalLiabilityToPaidDriver == true) {
      body.IsLegalLiabilityToPaidDriver = true;
      body.NoOfDriver = this.quoteReq.NoOfDriver;
    } else {
      body.IsLegalLiabilityToPaidDriver = false;
    }

    if (this.quoteReq.IsLegalLiabilityToPaidEmployee == true) {
      body.IsLegalLiabilityToPaidEmployee = true;
      body.NoOfEmployee = this.quoteReq.NoOfEmployee;
    } else {
      body.IsLegalLiabilityToPaidEmployee = false;
    }

    body.IsExtensionCountry = this.quoteReq.IsExtensionCountry;
    body.ExtensionCountryName = this.quoteReq.ExtensionCountryName;

    body.IsPACoverUnnamedPassenger = this.quoteReq.IsPACoverUnnamedPassenger;
    body.SIPACoverUnnamedPassenger = this.quoteReq.SIPACoverUnnamedPassenger;

    body.PACoverTenure = this.quoteReq.PACoverTenure;
    body.IsPACoverWaiver = this.quoteReq.IsPACoverWaiver
      ? this.quoteReq.IsPACoverWaiver
      : "false";

    body.IsVehicleHaveLPG = this.quoteReq.IsVehicleHaveLPG;
    body.IsVehicleHaveCNG = this.quoteReq.IsVehicleHaveCNG;
    body.SIVehicleHaveLPG_CNG = this.quoteReq.SIVehicleHaveLPG_CNG;
    let pType = JSON.parse(localStorage.getItem("breakinFlag"));
    if (pType) {
      let OldReferenceNo = JSON.parse(localStorage.getItem("breakinData"));
      if (!this.cs.isUndefineORNull(OldReferenceNo)) {
        body.OldReferenceNo = OldReferenceNo.proposalNo;
      } else {
        body.OldReferenceNo = null;
      }
    } else {
      body.OldReferenceNo = null;
    }

    let NomineeDetails;
    if (
      this.addNomineeVehicle == true &&
      this.proposalDetails.NameOfNominee != ""
    ) {
      NomineeDetails = {
        NomineeType: "PA-Owner Driver",
        NameOfNominee: this.proposalDetails.NameOfNominee,
        Age: this.proposalDetails.Age,
        Relationship: this.proposalDetails.Relationship,
      };
    }
    let GSTDetails;
    if (this.GSTDetails == true && this.proposalDetails.GSTInNumber != "") {
      GSTDetails = {
        GSTExemptionApplicable: "No",
        GSTInNumber: this.proposalDetails.GSTInNumber, // GSTIN/ UIN Number   // text box
        GSTToState: this.quoteReq.GSTToState, // no need to show on screen
        ConstitutionOfBusiness: this.constName, // Contrubution of business  // drop down
        CustomerType: this.custType, // Customer Type   // dropdown
        PanDetails: this.proposalDetails.PANCardNo, // Pan Details // text box
        GSTRegistrationStatus: this.gstName, // GST Registration status // Dropdown
      };
    }
    body.CustomerDetails.GSTDetails = GSTDetails;
    body.NomineeDetails = NomineeDetails;

    if (this.financierDetailsVal == true) {
      body.financierDetails = this.financierDetails;
    } else {
      body.financierDetails = null;
    }

    let PreviousPolicyDetails;
    if (
      this.policyType == "NEW" &&
      this.quoteReq.BusinessType == "New Business"
    ) {
      body.BusinessType = "New Business";
      body.isNoPrevInsurance = false;
    } else if (
      this.policyType == "Used" &&
      this.quoteReq.BusinessType == "Used"
    ) {
      body.BusinessType = "Used";
      body.isNoPrevInsurance = false;
    } else {
      body.BusinessType = "Roll Over";
      console.log(
        this.quoteReq.isNoPrevInsurance,
        this.quoteReq.IsNoPrevInsurance
      );
      if (this.previousPolicyValue == true) {
        if (this.showPrev == true) {
          PreviousPolicyDetails = {
            previousPolicyStartDate: moment(
              this.proposalDetails.PreviousPolicyStartDate
            ).format("YYYY-MM-DD"),
            previousPolicyEndDate: moment(
              this.proposalDetails.PreviousPolicyEndDate
            ).format("YYYY-MM-DD"),
            ClaimOnPreviousPolicy: this.ClaimOnPreviousPolicy,
            PreviousPolicyType: this.proposalDetails.PreviousPolicyType,
            PreviousInsurerName: this.companyCode,
            PreviousPolicyNumber: this.proposalDetails.PreviousPolicyNumber,
          };
          body.isNoPrevInsurance = false;
          body.PreviousPolicyDetails = PreviousPolicyDetails;
        } else if (this.showPrev == false) {
          // let startDate = "2014-06-10"; let endDate = "2015-06-09"
          // PreviousPolicyDetails = {
          //   "previousPolicyStartDate": startDate,
          //   "previousPolicyEndDate": endDate,
          //   "ClaimOnPreviousPolicy": this.quoteReq.PreviousPolicyDetails.ClaimOnPreviousPolicy,
          //   "PreviousPolicyType": this.quoteReq.PreviousPolicyDetails.PreviousPolicyType,
          //   "PreviousInsurerName": this.quoteReq.PreviousPolicyDetails.PreviousInsurerName,
          //   "PreviousPolicyNumber": this.quoteReq.PreviousPolicyDetails.PreviousPolicyNumber,
          // }
          body.isNoPrevInsurance = true;
        }
      } else {
        body.isNoPrevInsurance = this.cs.isUndefineORNull(
          this.quoteReq.IsNoPrevInsurance
        )
          ? this.quoteReq.isNoPrevInsurance
          : this.quoteReq.IsNoPrevInsurance;
      }
    }

    // if (this.productCode == "2320") {
    //   URL = "/proposal/twowheelertp";
    // }

    // if (this.productCode == "2319") {
    //   URL = "/proposal/pvtcartp";
    // }
    if (this.isInstaProposalRequired) {
      body.isAutoInstaTagging = true;
    }

    let str = JSON.stringify(body);
    this.cs.loaderStatus = true;
    this.getINSTAProposalTP(str);
    // this.cs.postBiz(URL, str).subscribe(
    //   (res: any) => {
    //     if (res.status == "Success") {
    //       this.calculatedData = res;
    //       this.finalamount = Math.round(this.calculatedData.finalPremium);

    //       localStorage.setItem("PropDatareq", JSON.stringify(body));
    //       localStorage.setItem("PropDatares", JSON.stringify(res));

    //       this.paymentFlag = true;
    //       // this.cs.loaderStatus = false;
    //       this.offlineSaveProposal().then(() => {
    //         this.cs.loaderStatus = false;
    //         if (this.saveFlagTP == true) {
    //           this.checkForNewPaymentModule();
    //         } else {
    //           swal({
    //             text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
    //           });
    //         }
    //       });

    //       // this.cs.loaderStatus = false;
    //     } else {
    //       this.cs.loaderStatus = false;
    //       let errormsg = res.message;
    //       // Common MSG
    //         // this.api.handleerrors(errormsg);
    //         let errMsg = this.msgService.errorMsg(errormsg)
    //         swal({ closeOnClickOutside: false, text: errMsg });
    //       let errorbody = {
    //         RequestJson: JSON.stringify(body),
    //         ResponseJson: JSON.stringify(res),
    //         ServiceURL: commonData.bizURL + URL,
    //         CorrelationID: this.quoteRes.correlationId,
    //       };
    //       this.api.adderrorlogs(errorbody);
    //       this.calculatedData = null;

    //       // this.calculatedData = null;
    //     }
    //   },
    //   (err) => {
    //     this.cs.loaderStatus = false;
    //     let errMsg = this.msgService.errorMsg(err)
    //     swal({ closeOnClickOutside: false, text: errMsg });
    //     // swal({
    //     //   closeOnClickOutside: false, text: 'At present system response is slow, kindly try after sometime. We regret the inconvenience'
    //     // });

    //     // .catch((err: any) => {
    //     //   this.cs.loaderStatus = false;
    //     //   swal({ text: 'At present system response is slow, kindly try after sometime. We regret the inconvenience'
    //     //   })
    //   }
    // );
  }

  // Calculate 4W proposal
  calculate4w(): Promise<any> {
    let pType = JSON.parse(localStorage.getItem("breakinFlag"));
    let savequote = JSON.parse(localStorage.getItem("savedQuotes"));
    if (pType == true || savequote == true || this.iasFlag) {
      this.policyType = this.quoteReq.BusinessType;
    } else {
      this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
      this.productCode = this.paramDataValues.iPartnerLogin.product;
      this.policyType = this.paramDataValues.iPartnerLogin.policy;
    }
    if (pType == true || savequote == true || this.iasFlag) {
      this.exshow = this.quoteReq.ExShowRoomPrice;
    } else {
      this.exshow = this.quoteReq.ExShowRoomPrice;
    }
    return new Promise((resolve: any) => {
      let startDate = moment(new Date()).format("YYYY-MM-DD");

      let currentyear = moment(this.quoteReq.PolicyStartDate).format("YYYY");
      let leapcheck = moment(currentyear).isLeapYear();
      if (this.productCode == "2320" && this.policyType == "NEW") {
        this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
          .add(5, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      } else if (this.productCode == "2319" && this.policyType == "NEW") {
        this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
          .add(3, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      } else {
        this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
          .add(1, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      }
      if (
        this.breakinFlag == true ||
        this.saveQuote == true ||
        this.iasFlag == true
      ) {
        this.VBData();
      }
      let body;
      /**
       * Digital POS change
       * get values from localStorage for checking if conditions in html file
       * author :- digitalPOS
       * date :- 15-07-2021
       */
      if (this.IsPOSTransaction_flag == true) {
        this.posDealID = JSON.parse(localStorage.getItem("DealID"));
        this.deal_Id = this.posDealID;
      } else {
        this.deal_Id = this.quoteReq.DealId;
      }
      if (this.policyType == "NEW") {
        body = {
          EngineNumber: this.proposalDetails.EngineNumber,
          ChassisNumber: this.proposalDetails.ChassisNumber,
          RegistrationNumber: this.quoteReq.RegistrationNumber,
          CustomerDetails: {
            CustomerType: "Individual",
            CustomerName: this.proposalDetails.CustomerName,
            // "DateOfBirth": moment(this.proposalDetails.DateOfBirth).format('YYYY-MM-DD'),
            PinCode: this.proposalDetails.PinCode,
            PANCardNo: this.proposalDetails.PANCardNo,
            Email: this.proposalDetails.Email,
            MobileNumber: this.proposalDetails.MobileNumber,
            AddressLine1: this.addressLine,
            CountryCode: this.CountryCode,
            StateCode: this.StateCode,
            CityCode: this.CityCode,
            MobileISD: "91",
            Gender: this.gender,
            AadharNumber: "",
          },

          IsProposal: true,
          ExShowRoomPrice: this.exshow,

          IsAutomobileAssocnFlag: this.quoteReq.IsAutomobileAssocnFlag,

          IsAntiTheftDisc: this.quoteReq.IsAntiTheftDisc,

          Tenure: parseInt(this.quoteReq.Tenure),
          TPTenure: parseInt(this.quoteReq.TPTenure),

          PolicyStartDate: this.quoteReq.PolicyStartDate,
          PolicyEndDate: this.policyEndDate,
          DealId: this.deal_Id,
          VehicleModelCode: this.quoteReq.VehicleModelCode,
          VehicleMakeCode: this.quoteReq.VehicleMakeCode,
          RTOLocationCode: String(this.quoteReq.RTOLocationCode),
          ManufacturingYear: String(this.quoteReq.ManufacturingYear),
          DeliveryOrRegistrationDate: this.quoteReq.DeliveryOrRegistrationDate,
          FirstRegistrationDate: this.quoteReq.FirstRegistrationDate,

          CustomerType: this.quoteReq.CustomerType,

          PACoverTenure: parseInt(this.quoteReq.PACoverTenure),
          CorrelationId: this.quoteRes.correlationId,

          GSTToState: this.quoteReq.GSTToState,

          NoOfDriver: 1,
          TPPDLimit: this.quoteReq.TPPDLimit,

          QuoteID: this.quoteRes.quoteID,

          IsLegalLiabilityToPaidEmployee:
            this.quoteReq.IsLegalLiabilityToPaidEmployee,
          NoOfEmployee: this.quoteReq.NoOfEmployee,
          LossOfPersonalBelongingPlanName:
            this.quoteReq.LossOfPersonalBelongingPlanName,
          IsPACoverUnnamedPassenger: this.quoteReq.IsPACoverUnnamedPassenger,
          SIPACoverUnnamedPassenger: this.quoteReq.SIPACoverUnnamedPassenger,
          IsEngineProtectPlus: this.quoteReq.IsEngineProtectPlus,
          IsConsumables: this.quoteReq.IsConsumables,
          KeyProtectPlan: this.quoteReq.KeyProtectPlan,
          RSAPlanName: this.quoteReq.RSAPlanName,
          IsExtensionCountry: this.quoteReq.IsExtensionCountry,
          ExtensionCountryName: this.quoteReq.ExtensionCountryName,
          IsRTIApplicableflag: this.quoteReq.IsRTIApplicableflag,
          ZeroDepPlanName: this.quoteReq.ZeroDepPlanName,
          IsVoluntaryDeductible: this.quoteReq.IsVoluntaryDeductible,
          VoluntaryDeductiblePlanName:
            this.quoteReq.VoluntaryDeductiblePlanName,
          isGarageCash: this.quoteReq.isGarageCash,
          garageCashPlanName: this.quoteReq.garageCashPlanName,
          isNCBProtect: this.quoteReq.isNCBProtect,
          ncbProtectPlanName: this.quoteReq.ncbProtectPlanName,
          ChannelSource: "IAGENT",
          IsTyreProtect: this.quoteReq.IsTyreProtect,
          IsVehicleHaveLPG: this.quoteReq.IsVehicleHaveLPG,
          IsVehicleHaveCNG: this.quoteReq.IsVehicleHaveCNG,
          SIVehicleHaveLPG_CNG: this.quoteReq.SIVehicleHaveLPG_CNG,
          softCopyFlag: this.isSoftCopySelected ? "Yes" : "No",
        };

        body.IsLegalLiabilityToPaidDriver =
          this.quoteReq.IsLegalLiabilityToPaidDriver;
        body.isPACoverWaiver = this.quoteReq.IsPACoverWaiver
          ? this.quoteReq.IsPACoverWaiver
          : "false";
        body.isValidDrivingLicense = this.quoteReq.isValidDrivingLicense;

        body.IsEMIProtect = this.quoteReq.IsEMIProtect;
        if (this.quoteReq.IsEMIProtect == true) {
          body.EMIAmount = this.quoteReq.EMIAmount;
          body.NoofEMI = this.quoteReq.NoofEMI;
          body.TimeExcessindays = this.quoteReq.TimeExcessindays;
        }

        let NomineeDetails;
        if (
          this.addNomineeVehicle == true &&
          this.proposalDetails.NameOfNominee != ""
        ) {
          NomineeDetails = {
            NomineeType: "PA-Owner Driver",
            NameOfNominee: this.proposalDetails.NameOfNominee,
            Age: this.proposalDetails.Age,
            Relationship: this.proposalDetails.Relationship,
          };
        }

        let GSTDetails;
        if (this.GSTDetails == true && this.proposalDetails.GSTInNumber != "") {
          GSTDetails = {
            GSTExemptionApplicable: "No",
            GSTInNumber: this.proposalDetails.GSTInNumber, // GSTIN/ UIN Number   // text box
            GSTToState: this.quoteReq.GSTToState, // no need to show on screen
            ConstitutionOfBusiness: this.constName, // Contrubution of business  // drop down
            CustomerType: this.custType, // Customer Type   // dropdown
            PanDetails: this.proposalDetails.PANCardNo, // Pan Details // text box
            GSTRegistrationStatus: this.gstName, // GST Registration status // Dropdown
          };
        }
        body.CustomerDetails.GSTDetails = GSTDetails;
        body.NomineeDetails = NomineeDetails;

        if (pType) {
          let OldReferenceNo = JSON.parse(localStorage.getItem("breakinData"));
          if (!this.cs.isUndefineORNull(OldReferenceNo)) {
            body.OldReferenceNo = OldReferenceNo.proposalNo;
          } else {
            body.OldReferenceNo = null;
          }
        } else {
          body.OldReferenceNo = null;
        }

        if (this.financierDetailsVal == true) {
          body.financierDetails = this.financierDetails;
        } else {
          body.financierDetails = null;
        }
      } else {
        body = {
          EngineNumber: this.proposalDetails.EngineNumber,
          ChassisNumber: this.proposalDetails.ChassisNumber,
          RegistrationNumber: this.quoteReq.RegistrationNumber,
          CustomerDetails: {
            CustomerType: "Individual",
            CustomerName: this.proposalDetails.CustomerName,
            // "DateOfBirth": moment(this.proposalDetails.DateOfBirth).format('YYYY-MM-DD'),
            PinCode: this.proposalDetails.PinCode,
            PANCardNo: this.proposalDetails.PANCardNo,
            Email: this.proposalDetails.Email,
            MobileNumber: this.proposalDetails.MobileNumber,
            AddressLine1: this.addressLine,
            CountryCode: this.CountryCode,
            StateCode: this.StateCode,
            CityCode: this.CityCode,
            MobileISD: "91",
            Gender: this.gender,
            AadharNumber: "",
          },
          IsProposal: true,
          ExShowRoomPrice: this.exshow,
          IsAutomobileAssocnFlag: this.quoteReq.IsAutomobileAssocnFlag,
          IsAntiTheftDisc: this.quoteReq.IsAntiTheftDisc,
          Tenure: parseInt(this.quoteReq.Tenure),
          TPTenure: parseInt(this.quoteReq.TPTenure),
          PolicyStartDate: this.quoteReq.PolicyStartDate,
          PolicyEndDate: this.quoteReq.PolicyEndDate,
          DealId: this.deal_Id,
          VehicleModelCode: this.quoteReq.VehicleModelCode,
          VehicleMakeCode: this.quoteReq.VehicleMakeCode,
          RTOLocationCode: String(this.quoteReq.RTOLocationCode),
          ManufacturingYear: String(this.quoteReq.ManufacturingYear),
          DeliveryOrRegistrationDate: this.quoteReq.DeliveryOrRegistrationDate,
          FirstRegistrationDate: this.quoteReq.FirstRegistrationDate
            ? this.quoteReq.FirstRegistrationDate
            : this.quoteReq.DeliveryOrRegistrationDate,
          CustomerType: this.quoteReq.CustomerType,
          PACoverTenure: parseInt(this.quoteReq.PACoverTenure),
          TPPDLimit: this.quoteReq.TPPDLimit,
          CorrelationId: this.quoteRes.correlationId,
          GSTToState: this.quoteReq.GSTToState,
          QuoteID: this.quoteRes.quoteID,
          LossOfPersonalBelongingPlanName:
            this.quoteReq.LossOfPersonalBelongingPlanName,
          IsPACoverUnnamedPassenger: this.quoteReq.IsPACoverUnnamedPassenger,
          SIPACoverUnnamedPassenger: this.quoteReq.SIPACoverUnnamedPassenger,
          IsEngineProtectPlus: this.quoteReq.IsEngineProtectPlus,
          IsConsumables: this.quoteReq.IsConsumables,
          KeyProtectPlan: this.quoteReq.KeyProtectPlan,
          RSAPlanName: this.quoteReq.RSAPlanName,
          IsExtensionCountry: this.quoteReq.IsExtensionCountry,
          ExtensionCountryName: this.quoteReq.ExtensionCountryName,
          IsRTIApplicableflag: this.quoteReq.IsRTIApplicableflag,
          ZeroDepPlanName: this.quoteReq.ZeroDepPlanName,
          IsVoluntaryDeductible: this.quoteReq.IsVoluntaryDeductible,
          VoluntaryDeductiblePlanName:
            this.quoteReq.VoluntaryDeductiblePlanName,
          isGarageCash: this.quoteReq.isGarageCash,
          garageCashPlanName: this.quoteReq.garageCashPlanName,
          isNCBProtect: this.quoteReq.isNCBProtect,
          ncbProtectPlanName: this.quoteReq.ncbProtectPlanName,
          IsVehicleHaveLPG: this.quoteReq.IsVehicleHaveLPG,
          IsVehicleHaveCNG: this.quoteReq.IsVehicleHaveCNG,
          SIVehicleHaveLPG_CNG: this.quoteReq.SIVehicleHaveLPG_CNG,
          ChannelSource: "IAGENT",
          softCopyFlag: this.isSoftCopySelected ? "Yes" : "No",
        };

        if (this.quoteReq.Tenure == "1" && this.quoteReq.TPTenure == "0") {
          (body.TPStartDate = this.quoteReq.TPStartDate),
            (body.TPEndDate = this.quoteReq.TPEndDate),
            (body.TPPolicyNo = this.quoteReq.TPPolicyNo);
          body.TPInsurerName = this.quoteReq.TPInsurerName;
          // For SAOD
          let dataInsurename = JSON.parse(localStorage.getItem('dataforBack'));
          let dataInsurename1 = JSON.parse(localStorage.getItem('calQuoteReq'));
          let previousInsurer: any = this.cs.PreviousInsurerList;
          if (!this.cs.isUndefineORNull(dataInsurename)) {
            //console.log(this.cs.PreviousInsurerList);
            this.insurerlist = previousInsurer;
            let data1 = this.insurerlist.find(
              (c) => c.companyName == dataInsurename.PreviousInsurerName
            );
            //console.log('Data1', data1);
            this.quoteReq.PreviousPolicyDetails.PreviousInsurerName = data1.companyCode;
            this.quoteReq.PreviousPolicyDetails.PreviousPolicyNumber = dataInsurename.PreviousPolicyNumber;
          }else{
            this.insurerlist = previousInsurer;
            let data1 = this.insurerlist.find(
              (c) => c.companyCode == dataInsurename1.PreviousPolicyDetails.PreviousInsurerName
            );
            //console.log('Data1', data1);
            this.quoteReq.PreviousPolicyDetails.PreviousInsurerName = data1.companyCode;
            this.quoteReq.PreviousPolicyDetails.PreviousPolicyNumber = dataInsurename1.PreviousPolicyDetails.PreviousPolicyNumber;
          }
        }

        body.IsEMIProtect = this.quoteReq.IsEMIProtect;
        if (this.quoteReq.IsEMIProtect == true) {
          body.EMIAmount = this.quoteReq.EMIAmount;
          body.NoofEMI = this.quoteReq.NoofEMI;
          body.TimeExcessindays = this.quoteReq.TimeExcessindays;
        }

        body.IsPACoverWaiver = this.quoteReq.IsPACoverWaiver
          ? this.quoteReq.IsPACoverWaiver
          : "false";
        body.IsTyreProtect = this.quoteReq.IsTyreProtect;
        body.isValidDrivingLicense = this.quoteReq.isValidDrivingLicense;
        body.NoOfDriver = this.quoteReq.NoOfDriver;
        body.NoOfEmployee = this.quoteReq.NoOfEmployee;
        body.IsLegalLiabilityToPaidDriver =
          this.quoteReq.IsLegalLiabilityToPaidDriver;
        body.IsLegalLiabilityToPaidEmployee =
          this.quoteReq.IsLegalLiabilityToPaidEmployee;

        if (pType) {
          let OldReferenceNo = JSON.parse(localStorage.getItem("breakinData"));
          if (!this.cs.isUndefineORNull(OldReferenceNo)) {
            body.OldReferenceNo = OldReferenceNo.proposalNo;
          } else {
            body.OldReferenceNo = null;
          }
        } else {
          body.OldReferenceNo = null;
        }

        let NomineeDetails;
        if (
          this.addNomineeVehicle == true &&
          this.proposalDetails.NameOfNominee != ""
        ) {
          NomineeDetails = {
            NomineeType: "PA-Owner Driver",
            NameOfNominee: this.proposalDetails.NameOfNominee,
            Age: this.proposalDetails.Age,
            Relationship: this.proposalDetails.Relationship,
          };
        }

        let GSTDetails;
        if (this.GSTDetails == true && this.proposalDetails.GSTInNumber != "") {
          GSTDetails = {
            GSTExemptionApplicable: "No",
            GSTInNumber: this.proposalDetails.GSTInNumber, // GSTIN/ UIN Number   // text box
            GSTToState: this.quoteReq.GSTToState, // no need to show on screen
            ConstitutionOfBusiness: this.constName, // Contrubution of business  // drop down
            CustomerType: this.custType, // Customer Type   // dropdown
            PanDetails: this.proposalDetails.PANCardNo, // Pan Details // text box
            GSTRegistrationStatus: this.gstName, // GST Registration status // Dropdown
          };
        }
        body.CustomerDetails.GSTDetails = GSTDetails;
        body.NomineeDetails = NomineeDetails;

        if (this.financierDetailsVal == true) {
          body.financierDetails = this.financierDetails;
        } else {
          body.financierDetails = null;
        }
      }

      if (
        this.policyType == "ROLL" ||
        this.quoteRes.generalInformation.transactionType == "Roll Over" ||
        this.policyType == "Used" ||
        this.quoteRes.generalInformation.transactionType == "Used"
      ) {
        body.BusinessType =
          this.policyType == "ROLL" ||
            this.quoteRes.generalInformation.transactionType == "Roll Over"
            ? "Roll Over"
            : "Used";
        console.log("no Previous insure");
        body.isNoPrevInsurance = this.cs.isUndefineORNull(
          this.quoteReq.IsNoPrevInsurance
        )
          ? this.quoteReq.isNoPrevInsurance
          : this.quoteReq.IsNoPrevInsurance;

        body.IsQCByPass = this.quoteReq.IsQCByPass;
        body.IsSelfInspection = this.quoteReq.IsSelfInspection;
        // body.BusinessType = "Roll Over";
        if (
          (this.quoteReq.isNoPrevInsurance == false ||
            this.quoteReq.IsNoPrevInsurance == false) &&
          !this.quoteReq.IsNoPrevInsurance
        ) {
          this.PreviousPolicyDetails.PreviousPolicyStartDate =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyStartDate : this.quoteReq.previousPolicyDetails.PreviousPolicyStartDate;
          this.PreviousPolicyDetails.PreviousPolicyEndDate =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate : this.quoteReq.previousPolicyDetails.PreviousPolicyEndDate;
          this.PreviousPolicyDetails.PreviousPolicyType =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyType : this.quoteReq.previousPolicyDetails.PreviousPolicyType;
          this.PreviousPolicyDetails.BonusOnPreviousPolicy =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.BonusOnPreviousPolicy : this.quoteReq.previousPolicyDetails.BonusOnPreviousPolicy;
          this.PreviousPolicyDetails.PreviousPolicyNumber =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyNumber : this.quoteReq.previousPolicyDetails.PreviousPolicyNumber;
          this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.ClaimOnPreviousPolicy : this.quoteReq.previousPolicyDetails.ClaimOnPreviousPolicy;
          this.PreviousPolicyDetails.TotalNoOfODClaims =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.TotalNoOfODClaims : this.quoteReq.previousPolicyDetails.TotalNoOfODClaims;
          this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy : this.quoteReq.previousPolicyDetails.NoOfClaimsOnPreviousPolicy;
          this.PreviousPolicyDetails.PreviousInsurerName =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousInsurerName : this.quoteReq.previousPolicyDetails.PreviousInsurerName;
          this.PreviousPolicyDetails.PreviousVehicleSaleDate =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousVehicleSaleDate : this.quoteReq.previousPolicyDetails.PreviousVehicleSaleDate;
          this.PreviousPolicyDetails.PreviousPolicyTenure =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyTenure : this.quoteReq.previousPolicyDetails.PreviousPolicyTenure;
          if (this.breakinFlag == true) {
            this.PreviousPolicyDetails.PreviousPolicyNumber =
            this.propReq.PreviousPolicyDetails ? this.propReq.PreviousPolicyDetails.PreviousPolicyNumber : this.propReq.previousPolicyDetails.PreviousPolicyNumber;
          }
          body.PreviousPolicyDetails = this.PreviousPolicyDetails;
        } else {
        }
      } else {
        body.BusinessType = "New Business";
        if (
          this.quoteReq.PreviousPolicyDetails != undefined ||
          this.quoteReq.PreviousPolicyDetails != null
        ) {
          this.PreviousPolicyDetails.PreviousPolicyType =
            this.quoteReq.PreviousPolicyDetails.PreviousPolicyType;
          this.PreviousPolicyDetails.PreviousPolicyStartDate =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyStartDate : this.quoteReq.previousPolicyDetails.PreviousPolicyStartDate;
          this.PreviousPolicyDetails.PreviousPolicyEndDate =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate : this.quoteReq.previousPolicyDetails.PreviousPolicyEndDate;
          this.PreviousPolicyDetails.PreviousPolicyType =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyType : this.quoteReq.previousPolicyDetails.PreviousPolicyType;
          this.PreviousPolicyDetails.BonusOnPreviousPolicy =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.BonusOnPreviousPolicy : this.quoteReq.previousPolicyDetails.BonusOnPreviousPolicy;
          this.PreviousPolicyDetails.PreviousPolicyNumber =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyNumber : this.quoteReq.previousPolicyDetails.PreviousPolicyNumber;
          this.PreviousPolicyDetails.ClaimOnPreviousPolicy =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.ClaimOnPreviousPolicy : this.quoteReq.previousPolicyDetails.ClaimOnPreviousPolicy;
          this.PreviousPolicyDetails.TotalNoOfODClaims =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.TotalNoOfODClaims : this.quoteReq.previousPolicyDetails.TotalNoOfODClaims;
          this.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy : this.quoteReq.previousPolicyDetails.NoOfClaimsOnPreviousPolicy;
          this.PreviousPolicyDetails.PreviousInsurerName =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousInsurerName : this.quoteReq.previousPolicyDetails.PreviousInsurerName;
          this.PreviousPolicyDetails.PreviousVehicleSaleDate =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousVehicleSaleDate : this.quoteReq.previousPolicyDetails.PreviousVehicleSaleDate;
          this.PreviousPolicyDetails.PreviousPolicyTenure =
          this.quoteReq.PreviousPolicyDetails ? this.quoteReq.PreviousPolicyDetails.PreviousPolicyTenure : this.quoteReq.previousPolicyDetails.PreviousPolicyTenure;
          body.PreviousPolicyDetails = this.PreviousPolicyDetails;
        }
        body.IsTransferOfNCB = this.quoteReq.IsTransferOfNCB;
        body.TransferOfNCBPercent = this.quoteReq.TransferOfNCBPercent;
      }

      if (this.quoteReq.OtherDiscount) {
        body.OtherDiscount = this.quoteReq.OtherDiscount;
      }

      if (this.isInstaProposalRequired) {
        body.isAutoInstaTagging = true;
      }

      let str = JSON.stringify(body);

      this.cs.loaderStatus = true;
      if (this.quoteRes.breakingFlag) {
        this.createBreakInReq(str);
      } else {
        this.getINSTAProposalPKG(str, "");
      }

      // this.cs.postBiz("/proposal/privatecarcreateproposal", str).subscribe(
      //   (res: any) => {
      //     if (res.status == "Success") {
      //       localStorage.setItem("PropDatareq", JSON.stringify(body));
      //       localStorage.setItem("PropDatares", JSON.stringify(res));
      //       this.calculatedData = res;
      //       this.breakInDeviationFlag = res.breakingFlag;
      //       if (res.isQuoteDeviation == true) {
      //         this.cs.loaderStatus = false;
      //         this.isPropDeviation = true;
      //         if (res.breakingFlag == true) {
      //           if (this.breakintype.breakin != "preApprovedBreakIN") {
      //             this.cs.loaderStatus = false;
      //             this.openBreakinModal();
      //             // this.saveofflineProposal();
      //           } else {
      //             this.saveofflineProposal().then(() => {
      //               if (this.saveProposalStatus == "Success") {
      //                 this.cs.loaderStatus = false;
      //                 this.PreBreakINDataresponse = JSON.parse(
      //                   localStorage.getItem("PreBreakINData")
      //                 );
      //                 this.saveOfflineBreakinId(this.PreBreakINDataresponse);
      //               } else {
      //                 this.cs.loaderStatus = false;
      //               }
      //             });
      //           }
      //           this.deviationMsg =
      //             "This case would go into deviation" + res.deviationMessage;
      //           this.getProposalDetails(res.generalInformation.proposalNumber);
      //         } else {
      //           if (this.productCode == "2311" && this.policyType != "NEW") {
      //             this.deviationMsg =
      //               "This case would go into deviation" + res.deviationMessage;
      //             this.getProposalDetails(
      //               res.generalInformation.proposalNumber
      //             );
      //           } else {
      //             swal({
      //               text: "This case would go into deviation. Would you like to to continue?",
      //               dangerMode: true,
      //               closeOnClickOutside: false,
      //               buttons: ["Yes", "No"],
      //             }).then((willDelete) => {
      //               if (willDelete) {
      //                 console.log("Stay");
      //               } else {
      //                 let type;
      //                 if (this.policyType == "NEW") {
      //                   type = "N";
      //                 } else {
      //                   type = "R";
      //                 }
      //                 this.cs.geToDashboardapproval(this.productCode, type);
      //               }
      //             });
      //           }
      //         }
      //       } else {
      //         this.finalamount = Math.round(this.calculatedData.finalPremium);
      //         this.paymentFlag = true;
      //         if (
      //           this.calculatedData.breakingFlag == true &&
      //           this.breakinFlag != true
      //         ) {
      //           if (this.breakintype.breakin != "preApprovedBreakIN") {
      //             this.cs.loaderStatus = false;
      //             this.openBreakinModal();
      //           } else {
      //             this.saveofflineProposal().then(() => {
      //               if (this.saveProposalStatus == "Success") {
      //                 this.cs.loaderStatus = false;
      //                 this.PreBreakINDataresponse = JSON.parse(
      //                   localStorage.getItem("PreBreakINData")
      //                 );
      //                 this.saveOfflineBreakinId(this.PreBreakINDataresponse);
      //               } else {
      //                 this.cs.loaderStatus = false;
      //               }
      //             });
      //           }
      //         } else {
      //           this.saveofflineProposal().then(() => {
      //             if (this.saveProposalStatus == "Success") {
      //               this.cs.loaderStatus = false;
      //               this.checkForNewPaymentModule();
      //             } else {
      //               this.cs.loaderStatus = false;
      //               swal({
      //                 text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
      //               });
      //             }
      //           });
      //         }
      //       }

      //       resolve();
      //     } else {
      //       this.cs.loaderStatus = false;
      //       let errormsg = res.message;
      //       // Common MSG
      //       // this.api.handleerrors(errormsg);
      //       let errMsg = this.msgService.errorMsg(errormsg)
      //       swal({ closeOnClickOutside: false, text: errMsg });
      //       let errorbody = {
      //         RequestJson: JSON.stringify(body),
      //         ResponseJson: JSON.stringify(res),
      //         ServiceURL:
      //           commonData.bizURL + "/proposal/privatecarcreateproposal",
      //         CorrelationID: this.quoteRes.correlationId,
      //       };
      //       this.api.adderrorlogs(errorbody);

      //       this.calculatedData = null;

      //       resolve();
      //     }
      //   },
      //   (err) => {
      //     resolve();
      //     this.cs.loaderStatus = false;
      //     let errMsg = this.msgService.errorMsg(err)
      //     swal({ closeOnClickOutside: false, text: errMsg });
      //   }
      // );
    });
  }

  getProposalDetails(proposalNo: any) {
    this.cs.loaderStatus = true;
    console.log(this.breakInDeviationFlag, "this.breakInDeviationFlag")
    this.cs
      .getBitLy(
        "Proposal/fw/FetchProposal?proposalNo=" +
        proposalNo +
        "&productCode=" +
        this.productCode
      )
      .then((res: any) => {
        this.isIDVIntRaised = false;
        this.isODIntRaised = false;
        this.fetchProposalData = res;
        if (!this.breakInDeviationFlag) {
          this.fetchProposalData = res;
          if (this.fetchProposalData.length > 1) {
            this.selectedSRT =
              this.fetchProposalData[0].srTvalue == "NA "
                ? this.fetchProposalData[1].srTvalue
                : this.fetchProposalData[0].srTvalue;
            this.fetchProposalData.forEach((ias: any) => {
              if (ias.srTvalue == "NA " || ias.subSRTvalue == "NA ") {
                this.isIASRaised = false;
              } else {
                this.srt.push(ias.srTvalue);
                this.iasProcess.push(ias.subSRTvalue);
                this.isIASRaised = true;
              }
            });
            if (this.srt.includes("NA ") || this.iasProcess.includes("NA ")) {
              this.isIASRaised = false;
            } else {
              this.isIASRaised = true;
            }
          } else {
            this.srt = this.fetchProposalData[0].srTvalue;
            this.selectedSRT = this.fetchProposalData[0].srTvalue;
            this.iasProcess = this.fetchProposalData[0].subSRTvalue;
            if (this.srt.includes("NA ") || this.iasProcess.includes("NA ")) {
              this.isIASRaised = false;
            } else {
              this.isIASRaised = true;
            }
          }
        } else {
          if (this.fetchProposalData.length > 1) {
            this.selectedSRT =
              this.fetchProposalData[0].srTvalue == "NA "
                ? this.fetchProposalData[1].srTvalue
                : this.fetchProposalData[0].srTvalue;
            this.fetchProposalData.forEach((ias: any) => {
              if (ias.srTvalue == "NA " || ias.subSRTvalue == "NA ") {
                this.isIASRaised = false;
              } else {
                this.srt.push(ias.srTvalue);
                this.iasProcess.push(ias.subSRTvalue);
                this.isIASRaised = true;
                if (this.srt.includes("IDV Deviation ")) {
                  this.cs.createIASToken().then(() => {
                    this.raisedIDVInteration();
                    this.isIASRaised = true;
                  })
                } else if (this.srt.includes("Other Discounting ")) {
                  this.cs.createIASToken().then(() => {
                    this.raisedODInteraction();
                    this.isIASRaised = true;
                  })
                } else {
                  this.isIASRaised = false;
                }
              }
            });
            if (this.srt.includes("NA ") || this.iasProcess.includes("NA ")) {
              this.isIASRaised = false;
            } else {
              this.isIASRaised = true;
            }
          } else {
            this.srt = this.fetchProposalData[0].srTvalue;
            this.selectedSRT = this.fetchProposalData[0].srTvalue;
            this.iasProcess = this.fetchProposalData[0].subSRTvalue;
            if (this.srt.includes("NA ") || this.iasProcess.includes("NA ")) {
              this.isIASRaised = false;
              if (this.breakInDeviationFlag) {

              } else {
                swal({
                  text:
                    "Your proposal number is " + this.calculatedData.generalInformation.proposalNumber + " and goes into deviation for SRT " + this.srt + " and Process " + this.iasProcess + " Unable to raised interaction",
                }).then(() => {

                })
                this.saveofflineProposal();
              }


            } else {
              this.isIASRaised = true;
              this.cs.createIASToken().then(() => {
                this.raiseInteraction();
              });
            }
          }
        }

        this.cs.loaderStatus = false;
      })
      .catch((err: any) => {
        console.log("Error", err);
        //   this.fetchProposalData = [
        //     {
        //         "deviationType": "OD Discounting ",
        //         "subSRTvalue": "Other Discounting process related approvals ",
        //         "srTvalue": "Other Discounting ",
        //         "imtDiscount": "0.75",
        //     "carSegment": "Family Cars C"
        //     }
        // ]
        // this.srt = this.fetchProposalData[0].srTvalue;
        //     this.selectedSRT = this.fetchProposalData[0].srTvalue;
        //     this.iasProcess = this.fetchProposalData[0].subSRTvalue;
        //     if(this.srt.includes('NA ') || this.iasProcess.includes('NA ')){
        //       this.isIASRaised = false;
        //     }else{
        //       this.isIASRaised = true;
        //     }
        this.cs.loaderStatus = false;
      });
  }

  financierData(ev: any) {
    if (ev.target.value != "None") {
      this.financierDetailsVal = true;
      this.financierDetails.AgreementType = ev.target.value;
      this.financierDetails.FinancierName = "";
      this.financierDetails.BranchName = "";
    } else {
      this.financierDetailsVal = false;
      this.financierDetails.FinancierName = "";
      this.financierDetails.BranchName = "";
    }
    // Jaganath
    if (ev.target.value=="None" && (this.quoteRes.generalInformation.customerType =="INDIVIDUAL" || this.quoteRes.generalInformation.customerType =="Individual") && this.quoteRes.generalInformation.depriciatedIDV >=10000000  )
    {
      this.isAMLRequired=true;
      this.selfEmployed =false;
      this.isSalaried =true;
      this.businessType="";
      this.annualIncome="";

    }
    else{
      this.isAMLRequired=false;
      this.businessType="";
      this.annualIncome="";
    }
  }

  openBreakinModal() {
    $("#breakin_modal").modal("show");
  }

  getSubLocationCities(ev?) {
    if (ev.target.value.length < 3 || ev.key == "Backspace") {
    } else {
      let postObj = ev.target.value;
      this.api.getSubLocation(postObj).subscribe((res: any) => {
        this.sublocations = res;
      });
    }
  }

  getSubLocationCity(ev: any) {
    if (ev.target.value.length < 3 || ev.key == "Backspace") {
    } else {
      let postObj = ev.target.value;
      this.api.getSubLocation(postObj).subscribe((res: any) => {
        this.sublocations = res;
      });
    }
  }

  submitBreakin(inspectionMode: any, subLocation: any) {
    $("#breakin_modal").modal("hide");
    this.createBreakInId(this.calculatedData).then(() => {
      if (
        this.breakinID == undefined ||
        this.breakinID == 0 ||
        this.breakinID == null
      ) {
        if (this.calculatedData.isQuoteDeviation) {
          swal({
            text:
              "Breakin Id could not be generated to this vehicle. Reason: " +
              this.errorMessage + 'and your proposal ' + this.calculatedData.generalInformation.proposalNumber + ' go into deviation. Kindly check in IAS tab.',
          }).then(() => {
            this.saveofflineProposal().then(() => {
              if (this.saveProposalStatus == "Success") {
                console.log("fail breakin");
              } else {
                swal({
                  text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
                });
              }
            });
          });
        } else {
          swal({
            text:
              "Breakin Id could not be generated to this vehicle. Reason: " +
              this.errorMessage,
          }).then(() => {
            this.saveofflineProposal().then(() => {
              if (this.saveProposalStatus == "Success") {
                console.log("fail breakin");
              } else {
                swal({
                  text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
                });
              }
            });
          });
        }

      } else {
        if (this.calculatedData.isQuoteDeviation) {
          console.log('HIIIII');
          if (!this.cs.isUndefineORNull(this.fetchProposalData)) {
            if (this.fetchProposalData.length > 1) {
              swal({
                text:
                  "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases and IAS. Your Proposal Number: " +
                  this.calculatedData.generalInformation.proposalNumber +
                  " and your Breakin ID is: " +
                  this.breakinID +
                  " also Your interaction ID id: " + this.iasODRes.srRequestNo + " , " + this.iasIDVRes.srRequestNo + " for srt " + this.iasODReq.SRT + " , " + this.iasIDVReq.SRT + " and process " + this.iasODReq.Process + " , " + this.iasIDVReq.Process + " , if recommended proceed for payment."
              }).then(() => {

              });
              this.saveofflineProposal();
            } else {
              swal({
                text:
                  "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases and IAS. Your Proposal Number: " +
                  this.calculatedData.generalInformation.proposalNumber +
                  " and your Breakin ID is: " +
                  this.breakinID +
                  " also Your interaction ID id: " + this.iasODRes.srRequestNo + " for srt " + this.srt + " and process " + this.iasProcess + ", if recommended proceed for payment."
              }).then(() => {

              });
              this.saveofflineProposal();
            }
          } else {
            swal({
              text:
                "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases, if recommended proceed for payment. Your Proposal Number: " +
                this.calculatedData.generalInformation.proposalNumber +
                " and your Breakin ID is: " +
                this.breakinID +
                "but we are unable to raised interaction for same.",
            }).then(() => {

            });
            this.saveofflineProposal();
          }
        } else {
          this.saveofflineProposal().then(() => {
            if (this.saveProposalStatus == "Success") {
              if (this.calculatedData.isQuoteDeviation) {
                swal({
                  text:
                    "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases, if recommended proceed for payment. Your Proposal Number: " +
                    this.calculatedData.generalInformation.proposalNumber +
                    " and your Breakin ID is: " +
                    this.breakinID +
                    " Raised Interaction from below.",
                }).then(() => { });
              } else {
                swal({
                  text:
                    "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases, if recommended proceed for payment. Your Proposal Number: " +
                    this.calculatedData.generalInformation.proposalNumber +
                    " and your Breakin ID is: " +
                    this.breakinID,
                }).then(() => {
                  // localStorage.setItem('myBreakinPolicy', 'true');
                  // this.changedEventListner.emit(true);
                  this.router.navigateByUrl("/quote").then(() => {
                    this.cs.clearLocalStorage();
                    this.cs.clearQuoteDate();
                    this.cs.clearPropData();
                    this.changedEventListner.emit(true);
                    this.router
                      .navigateByUrl("/", { skipLocationChange: true })
                      .then(() => this.router.navigate(["/quote"]));
                  });
                });
              }
            } else {
              swal({
                text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
              });
            }
          });
        }
      }
    });
  }

  createBreakInId(data: any): Promise<any> {
    return new Promise((resolve: any) => {
      let body;
      let vehicleType;
      let breakinDays;
      if (
        this.policyType == "ROLL" &&
        this.quoteReq.isNoPrevInsurance == false
      ) {
        breakinDays = moment(this.quoteReq.PolicyStartDate).diff(
          moment(this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate),
          "days"
        );
      } else {
        breakinDays = 0;
      }
      if (this.productCode == "2312") {
        vehicleType = "MOTORCYCLE";
      } else if (this.productCode == "2311") {
        vehicleType = "PRIVATE CAR";
      }
      let pType;
      if (this.policyType == "ROLL") {
        pType = "ROLLOVER";
      } else {
        pType = "NEW";
      }

      /**
       * digitalPOS change
       * get values from localStorage for checking if conditions in html file
       * date :- 29-07-2021
       */
      if (this.IsPOSTransaction_flag == true) {
        this.posDealID = JSON.parse(localStorage.getItem("DealID"));
        this.deal_Id = this.posDealID;
      } else {
        this.deal_Id = this.quoteReq.DealId;
      }
      body = {
        CorrelationId: data.correlationId,
        BreakInType: "Break-in Policy lapse",
        BreakInDays: breakinDays,
        CustomerName: this.proposalDetails.CustomerName,
        CustomerAddress: this.addressLine,
        State: this.proposalDetails.stateName,
        City: this.proposalDetails.cityName,
        MobileNumber: this.proposalDetails.MobileNumber,
        TypeVehicle: vehicleType,
        VehicleMake: this.quoteRes.generalInformation.manufacturerName,
        VehicleModel: this.quoteRes.generalInformation.vehicleModel,
        ManufactureYear: String(this.quoteReq.ManufacturingYear),
        RegistrationNo: this.quoteReq.RegistrationNumber,
        EngineNo: this.proposalDetails.EngineNumber,
        ChassisNo: this.proposalDetails.ChassisNumber,
        SubLocation: this.breakinDetails.subLocation,
        DistributorInterID: "",
        DistributorName: "Emmet",
        InspectionType: pType,
        DealId: this.deal_Id,
      };

      if (
        this.breakinDetails.inspectionMode == "self" ||
        this.quoteReq.IsSelfInspection == true
      ) {
        body.SelfInspection = "Yes";
      }

      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      if (
        this.breakintype.breakin == "selfinspection" ||
        this.breakintype.breakin == "ilinspection"
      ) {
        this.cs.postBiz("/breakin/createbreakinid", str).subscribe(
          (res: any) => {
            this.breakinStatus = res.status;
            if (res.status == "Success") {
              this.saveOfflineBreakinId(res);
              // this.saveofflineProposal()
              this.cs.loaderStatus = false;
              this.breakinID = res.brkId;
              resolve();
            } else {
              if (this.calculatedData.isQuoteDeviation) {
                this.errorMessage = res.message;
                this.cs.loaderStatus = false;
                if (this.srt.includes('NA ') || this.iasProcess.includes('NA ')) {
                  // swal({ text: res.message + ' and your iteraction for srt ' + this.srt + ', process ' + this.iasProcess + ' so unable to raised interaction ID.' });
                } else {
                  swal({ text: res.message + ', Also your proposal ' + this.calculatedData.generalInformation.proposalNumber + ' goes into  deviation, So kindly check in IAS tab.' });
                }
                resolve();
              } else {
                this.errorMessage = res.message;
                this.cs.loaderStatus = false;
                swal({ text: res.message });
                resolve();
              }
            }
          },
          (err) => {
            resolve();
          }
        );
      } else {
        this.PreBreakINData = JSON.parse(
          localStorage.getItem("PreBreakINData")
        );
        this.saveOfflineBreakinId(this.PreBreakINDataresponse);
      }
    });
  }

  saveOfflineBreakinId(breakinResp: any) {
    let body = breakinResp;

    if (JSON.parse(localStorage.getItem("PreBreakINData"))) {
      body.BreakInType = "Pre-Approved";
      body.brkId = JSON.parse(breakinResp.breakInID);
      body.correlationID = this.quoteRes.correlationId;
    } else if (this.quoteReq.IsSelfInspection == true) {
      body.BreakInType = "Self Inspection by Customer";
    } else if (this.quoteReq.IsSelfInspection == false) {
      body.BreakInType = "IL Inspection";
    }

    // if (this.breakintype.breakin == 'selfinspection') {
    //   body.BreakInType = "Self Inspection by Customer";
    // } if(this.breakintype.breakin == 'ilinspection'){
    //   body.BreakInType = "IL Inspection";
    // }else{
    //   body.BreakInType = "Pre-Approved";
    //   body.brkId = JSON.parse(breakinResp.breakInID);
    //   body.correlationID = this.quoteRes.correlationId;
    // }

    // correlationId
    body.SubLocation = this.breakinDetails.subLocation;
    let str = JSON.stringify(body);
    this.cs.loaderStatus = true;
    this.cs.post("BreakIn/UpdateBreakinID", str).then((res: any) => {
      this.cs.loaderStatus = false;
      if (body.BreakInType == "Pre-Approved") {
        swal({
          text:
            "The vehicle would need surveyor inspection, Please check the status of Inspection from My Break-in cases, if recommended proceed for payment. Your Proposal Number: " +
            this.calculatedData.generalInformation.proposalNumber +
            " and your Breakin ID is: " +
            body.brkId,
        }).then(() => {
          // this.router.navigateByUrl("/quote");
          // localStorage.setItem('myBreakinPolicy', 'true');
          // this.changedEventListner.emit(true);
          this.router.navigateByUrl("/quote").then(() => {
            this.cs.clearLocalStorage();
            this.cs.clearQuoteDate();
            this.cs.clearPropData();
            this.changedEventListner.emit(true);
            this.router
              .navigateByUrl("/", { skipLocationChange: true })
              .then(() => this.router.navigate(["/quote"]));
          });
        });
      }
    });
  }

  getPolicyTenure(ev) {
    this.policyTenure = JSON.parse(ev.target.value);
    this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
      .add(this.policyTenure, "years")
      .subtract(1, "days")
      .format("YYYY-MM-DD");
    this.premiumBreakup(this.quoteRes);
  }

  // offline save proposal
  offlineSaveProposal(): Promise<any> {
    return new Promise((resolve: any) => {
      let body;

      let propReq = JSON.parse(localStorage.getItem("PropDatareq"));
      let propRes = JSON.parse(localStorage.getItem("PropDatares"));
      let bankType = localStorage.getItem("bankType");
      body = {
        STATUS: 2,
        PROPOSAL_NUMBER: propRes.generalInformation.proposalNumber,
        BASIC_PREMIUM: "0",
        SERVICE_TAX: "0",
        bankType: bankType,
        TOTAL_PREMIUM: JSON.stringify(propRes.finalPremium),
        TOTAL_TAX: JSON.stringify(propRes.totalTax),
        PACKAGE_PREMIUM: "0",
        START_DATE: this.quoteReq.PolicyStartDate,
        END_DATE: this.quoteReq.PolicyEndDate,
        TOTAL_LIABILITY_PREMIUM: JSON.stringify(propRes.totalLiabilityPremium),
        MODIFIED_BY: "",
        CUSTOMER_ID: propRes.generalInformation.customerId,
        CORELATION_ID: this.quoteRes.correlationId,
        PROPOSAL_RS: JSON.stringify(propRes),
        PROPOSAL_RQ: JSON.stringify(propReq),
        QUOTE_ID: JSON.stringify(this.quoteRes.quoteID),
        CustDetails: {
          CustomerName: propReq.CustomerDetails.CustomerName,
          CustomerType: propReq.CustomerDetails.CustomerType, //string
          DateOfBirth: "01/01/1999", //propReq.CustomerDetails.DateOfBirth,//string
          PinCode: propReq.CustomerDetails.PinCode, //string
          PANCardNo: propReq.CustomerDetails.PANCardNo, //string
          Email: propReq.CustomerDetails.Email, //string
          MobileNumber: propReq.CustomerDetails.MobileNumber, //string
          AddressLine1: propReq.CustomerDetails.AddressLine1, //string
          CountryCode: propReq.CustomerDetails.CountryCode, //string
          StateCode: propReq.CustomerDetails.StateCode, //string
          CityCode: propReq.CustomerDetails.CityCode, //string
          MobileISD: "91", //string
          Gender: propReq.CustomerDetails.Gender, //string
          AadharNumber: propReq.CustomerDetails.AadharNumber, //string
          GSTDetails: propReq.CustomerDetails.GSTDetails,
        },
      };

      if (
        this.paramDataValues.iPartnerLogin.isSubagent != null ||
        this.paramDataValues.iPartnerLogin.isSubagent != undefined
      ) {
        body.IsSubagent = true;
        body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
      }

      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs
        .post("proposal/offlinesave/TP", str)
        .then((res: any) => {
          this.saveFlagTP = res.isRecordInserted;
          resolve();
          this.cs.loaderStatus = false;
        })
        .catch((err: any) => {
          swal({
            text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
          });
        });
    });
  }

  saveofflineProposal(): Promise<any> {
    return new Promise((resolve: any) => {
      let body, URL;
      let odindex = this.srt.indexOf("Other Discounting ");
      let idvindex = this.srt.indexOf("IDV Deviation ");
      console.log(odindex, idvindex, this.srt);
      let propReq = JSON.parse(localStorage.getItem("PropDatareq"));
      let pType = JSON.parse(localStorage.getItem("breakinFlag"));
      let savequote = JSON.parse(localStorage.getItem("savedQuotes"));
      if (savequote == true) {
        this.productCode = this.propReq.productCode;
      }

      if (pType == true || savequote == true) {
        if (this.productCode == "2312") {
          let exShowroom = JSON.stringify(propReq.ExShowRoomPrice);
          propReq.ExShowRoomPrice = exShowroom;
        }
        if (this.productCode == "2311") {
          let exShowroom = JSON.parse(propReq.ExShowRoomPrice);
          propReq.ExShowRoomPrice = exShowroom;
        }

        if (
          propReq.BusinessType == "Roll Over" &&
          this.propReq.PreviousPolicyDetails != null
        ) {
          propReq.PreviousPolicyDetails.PreviousPolicyTenure =
            this.previousPolicyTenOffline;
        }
      }
      let propRes = JSON.parse(localStorage.getItem("PropDatares"));
      let sortedRes;
      if (!this.cs.isUndefineORNull(propRes.deviationMessage)) {
        sortedRes = propRes.deviationMessage.replace(/%/g, "");
      } else {
        sortedRes = "";
      }
      propRes.deviationMessage = sortedRes;
      let srt: any, iasProcess: any;
      if (this.cs.isUndefineORNull(this.fetchProposalData)) {
        srt = "";
        iasProcess = "";
      } else {
        if (this.fetchProposalData.length > 1) {
          srt = this.srt[0];
          iasProcess = this.iasProcess[0];
        } else {
          srt = this.srt;
          iasProcess = this.iasProcess;
        }
      }
      let bankType = localStorage.getItem("bankType");
      console.log("IAS", this.iasIntRes);
      body = {
        proposalRQ: propReq,
        proposalRS: propRes,
        bankType: bankType,
        IasRQ: {
          OdIasID: this.iasODRes ? this.iasODRes.srRequestNo : "",
          OdIasSrt:
            (this.iasODRes && odindex < 0) || (this.iasODRes && idvindex < 0)
              ? srt
              : this.srt[odindex],
          OdIasSubSrt:
            (this.iasODRes && odindex < 0) || (this.iasODRes && idvindex < 0)
              ? iasProcess
              : this.iasProcess[odindex],
          IdvIasID: this.iasIDVRes ? this.iasIDVRes.srRequestNo : "",
          IdvIasSrt:
            (this.iasIDVRes && odindex < 0) || (this.iasIDVRes && idvindex < 0)
              ? srt
              : this.srt[idvindex],
          IdvIasSubSrt:
            (this.iasIDVRes && odindex < 0) || (this.iasIDVRes && idvindex < 0)
              ? iasProcess
              : this.iasProcess[idvindex],
          OdIasRQ: this.iasODReq ? this.iasODReq : "",
          OdIasRS: this.iasODRes ? JSON.stringify(this.iasODRes) : "",
          IdvIasRQ: this.iasIDVReq ? this.iasIDVReq : "",
          IdvIasRS: this.iasIDVRes ? JSON.stringify(this.iasIDVRes) : "",
        },
      };

      if (this.quoteReq.Tenure == "1" && this.quoteReq.TPTenure == "0") {
        body.IsStandaloneOD = true;
        let standaloneODRQ = {
          TPStartDate: this.quoteReq.TPStartDate,
          TPEndDate: this.quoteReq.TPEndDate,
          TPPolicyNo: this.quoteReq.TPPolicyNo,
          TPInsurerName: this.quoteReq.TPInsurerName,
        };
        body.standaloneODRQ = standaloneODRQ;
      }

      let savequoteData = JSON.parse(localStorage.getItem("saveQuoteData"));
      if (savequote == true) {
        if (savequoteData.isSubAgent == true) {
          body.IsSubagent = savequoteData.isSubAgent;
          body.SubagentIpartnerUserID = savequoteData.subagentIpartnerUserID;
        } else {
          body.IsSubagent = savequoteData.isSubAgent;
        }
      } else {
        if (
          this.paramDataValues.iPartnerLogin.isSubagent != null ||
          this.paramDataValues.iPartnerLogin.isSubagent != undefined
        ) {
          body.IsSubagent = true;
          body.SubagentIpartnerUserID =
            this.paramDataValues.iPartnerLogin.subAID;
        }
      }

      if (this.productCode == "2312") {
        URL = "Proposal/tw/SaveProposal";
      }
      if (this.productCode == "2311") {
        URL = "Proposal/fw/SaveProposal";
      }
      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs
        .post(URL, str)
        .then((res: any) => {
          if (res.status == "Success") {
            this.saveProposalStatus = res.status;
            // this.getIASData();
          } else {
            let errorbody = {
              RequestJson: JSON.stringify(body),
              ResponseJson: JSON.stringify(res),
              ServiceURL: URL,
              CorrelationID: this.quoteRes.correlationId,
            };
            this.api.adderrorlogs(errorbody);
          }

          this.cs.loaderStatus = false;
          resolve();
        })
        .catch((err: any) => {
          // swal({ text: err });
          let errMsg = this.msgService.errorMsg(err)
          swal({ closeOnClickOutside: false, text: errMsg });
          this.cs.loaderStatus = false;
        });
    });
  }

  getIASData() {
    let startDate = moment(new Date()).format("DD/MM/YYYY");
    localStorage.setItem("startDate", JSON.stringify(startDate));
    this.cs.get("IAS/GetIASList").subscribe((res: any) => {
      console.log(res);
      localStorage.setItem("IASData", JSON.stringify(res));
    });
  }

  premiumBasedCoverClick(ev: any) {
    if (ev.target.id == "noValidLicense") {
      this.disabled1 = true;
      this.disabled = false;
    } else {
      this.disabled = true;
      this.disabled1 = false;
    }
    if (ev.target.checked == true) {
      this.cpaTenure = 0;
      this.premiumBasedOnCover(this.cpaTenure);
    } else {
      this.cpaTenure = this.quoteReq.CPATenure;
      this.disabled1 = false;
      this.disabled = false;
      if (JSON.parse(localStorage.getItem("calQuoteRes"))) {
        this.quoteRes = JSON.parse(localStorage.getItem("calQuoteRes"));
      } else {
        this.quoteRes = JSON.parse(localStorage.getItem("calQuoteTPRes"));
      }
      this.finalPremium = Math.round(this.quoteRes.totalPremium);
      this.finalPremium_2 = Math.round(this.quoteRes.totalPremium_FORPT2);
      this.finalPremium_3 = Math.round(this.quoteRes.totalPremium_FORPT3);

      this.premiumBreakup(this.quoteRes);
    }
  }

  //For No valid Driving license & personal accedent cover
  premiumBasedOnCover(cpaTenure) {
    let body;
    body = {
      QuoteID: this.quoteRes.quoteID,
      MotorSubType: this.quoteReq.MotorSubType,
      VehicleCC: this.quoteReq.VehicleCC,
      PolicyTenure: this.policyTenure,
      CPATenure: cpaTenure,
      TransFor: this.quoteReq.TransFor,
      // "TPPDSum":6000,
      IsInterstate: this.quoteReq.IsInterstate,
      // "LLTenure":"Legal_Liability_1"
    };
    if (this.productCode == "2319") {
      if (this.policyType == "NEW") {
        body.LLTenure = "Legal_Liability_3";
      } else {
        body.LLTenure = "Legal_Liability_1";
      }
    }

    let str = JSON.stringify(body);
    // this.cs.loaderStatus = true;
    this.cs
      .post("Quote/tp/GetTPQuotePremiumBasedOnCover", str)
      .then((res: any) => {
        let quoteID, correlationId;
        if (res.status == "Success") {
          this.cs.loaderStatus = false;
          this.finalPremium = Math.round(res.totalPremium);
          this.finalPremium_2 = Math.round(res.totalPremium_FORPT2);
          this.finalPremium_3 = Math.round(res.totalPremium_FORPT3);
          quoteID = this.quoteRes.quoteID;
          correlationId = this.quoteRes.correlationId;
          this.quoteRes = res;
          this.premiumBreakup(res);
          this.quoteRes.quoteID = quoteID;
          this.quoteRes.correlationId = correlationId;
        } else {
          this.cs.loaderStatus = false;
          this.finalPremium = Math.round(this.quoteRes.totalPremium);

          swal({
            text: this.cs.isUndefineORNull(res.message)
              ? "Looks like session has expired. Kindly login again"
              : res.message,
          });
        }
      });
  }

  premiumBreakup(data) {
    switch (this.policyTenure) {
      case 1:
        this.basicLiabilityPremium = data.basicLiabilityPremium;
        this.paCoverPremium = data.paCoverPremium;
        this.legalLiabilityAmount = data.legalLiabilityAmount;
        this.tppdDeductionAmount = data.tppdDeductionAmount;
        this.totalLiabilityPremium = data.totalLiabilityPremium;
        this.totalGSTAmount = data.totalGSTAmount;
        this.totalPremium = data.totalPremium;
        this.biFuelKitTP = data.biFuelKitTP;
        break;

      case 2:
        this.basicLiabilityPremium = data.basicLiabilityPremium_FORPT2;
        this.paCoverPremium = data.paCoverPremium;
        this.legalLiabilityAmount = data.legalLiabilityAmount;
        this.tppdDeductionAmount = data.tppdDeductionAmount;
        this.totalLiabilityPremium = data.totalLiabilityPremium_FORPT2;
        this.totalGSTAmount = data.totalGSTAmount_FORPT2;
        this.totalPremium = data.totalPremium_FORPT2;
        this.biFuelKitTP = data.biFuelKitTP;
        break;

      case 3:
        if (this.productCode == "2319") {
          this.basicLiabilityPremium = data.basicLiabilityPremium;
          this.paCoverPremium = data.paCoverPremium;
          this.legalLiabilityAmount = data.legalLiabilityAmount;
          this.tppdDeductionAmount = data.tppdDeductionAmount;
          this.totalLiabilityPremium = data.totalLiabilityPremium;
          this.totalGSTAmount = data.totalGSTAmount;
          this.totalPremium = data.totalPremium;
          this.biFuelKitTP = data.biFuelKitTP;
        } else {
          this.basicLiabilityPremium = data.basicLiabilityPremium_FORPT3;
          this.paCoverPremium = data.paCoverPremium;
          this.legalLiabilityAmount = data.legalLiabilityAmount;
          this.tppdDeductionAmount = data.tppdDeductionAmount;
          this.totalLiabilityPremium = data.totalLiabilityPremium_FORPT3;
          this.totalGSTAmount = data.totalGSTAmount_FORPT3;
          this.totalPremium = data.totalPremium_FORPT3;
          this.biFuelKitTP = data.biFuelKitTP;
        }

        break;

      default:
        this.basicLiabilityPremium = data.basicLiabilityPremium;
        this.paCoverPremium = data.paCoverPremium;
        this.legalLiabilityAmount = data.legalLiabilityAmount;
        this.tppdDeductionAmount = data.tppdDeductionAmount;
        this.totalLiabilityPremium = data.totalLiabilityPremium;
        this.totalGSTAmount = data.totalGSTAmount;
        this.totalPremium = data.totalPremium;
        this.biFuelKitTP = data.biFuelKitTP;
        break;
    }
  }

  // Pincode Details
  getPincodeDetails(ev: any) {
    if (ev.target.value.length < 6 || ev.key == "Backspace") {
      this.proposalDetails.cityName = "";
      this.proposalDetails.stateName = "";
    } else {
      this.cs
        .get("Pincode/GetPincodeList?pinCode=" + ev.target.value)
        .subscribe((res: any) => {
          this.pincodeDetails = res;
          this.stateName = this.pincodeDetails[0].stateName;
          this.proposalDetails.cityName = res[0].cityName;
          this.proposalDetails.stateName = this.stateName;
          this.CityCode = this.pincodeDetails.find(
            (c) => c.cityName == this.proposalDetails.cityName
          ).cityDistrictId;
          this.StateCode = this.pincodeDetails.find(
            (c) => c.cityName == this.proposalDetails.cityName
          ).stateId;
          this.CountryCode = this.pincodeDetails.find(
            (c) => c.cityName == this.proposalDetails.cityName
          ).countryId;
          this.proposalDetails.employeeName = this.proposalDetails.CustomerName;
          this.proposalDetails.officeAddress = this.proposalDetails.AddressLine1 + " " + res[0].cityName + " " + this.pincodeDetails[0].stateName  + " " + res[0].pincode;

          let instaProposal = document.getElementById('instaProposal') as HTMLInputElement;
          if (this.cs.isUndefineORNull(instaProposal)) {
            this.isInstaProposalRequired = false;
          } else {
            if (this.dealDetailsfromIM.bankType == 'INT' && !this.isIDVDeviation && !this.isODDeviation) {
              instaProposal.checked = true;
              this.isInstaProposalRequired = true;
            } else {
              instaProposal.checked = false;
              this.isInstaProposalRequired = false;
            }
          }
        });
    }
  }

  getPincodeDetailssave(ev: any) {
    this.cs
      .get("Pincode/GetPincodeList?pinCode=" + ev)
      .subscribe((res: any) => {
        this.pincodeDetails = res;
        this.stateName = this.pincodeDetails[0].stateName;
        this.proposalDetails.cityName = res[0].cityName;
        this.proposalDetails.stateName = this.stateName;
        this.CityCode = this.pincodeDetails.find(
          (c) => c.cityName == this.proposalDetails.cityName
        ).cityDistrictId;
        this.StateCode = this.pincodeDetails.find(
          (c) => c.cityName == this.proposalDetails.cityName
        ).stateId;
        this.CountryCode = this.pincodeDetails.find(
          (c) => c.cityName == this.proposalDetails.cityName
        ).countryId;
        this.proposalDetails.officeAddress = this.proposalDetails.AddressLine1 ; 
        
        let instaProposal = document.getElementById('instaProposal') as HTMLInputElement;
        console.log('instaProposal', instaProposal, this.dealDetailsfromIM);
        console.log('instaProposal', instaProposal);
        if (this.cs.isUndefineORNull(instaProposal)) {
          this.isInstaProposalRequired = false;
        } else {
          if (this.dealDetailsfromIM.bankType == 'INT') {
            instaProposal.checked = true;
            this.isInstaProposalRequired = true;
          } else {
            instaProposal.checked = false;
            this.isInstaProposalRequired = false;
          }
        }
      });

  }

  // MobileNo Validation
  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  mobileValidate() {
    let isNumberValidate = this.cs.mobileValidation(
      this.proposalDetails.MobileNumber
    );
    if (isNumberValidate) {
      this.proposalDetails.MobileNumber = this.proposalDetails.MobileNumber;
    } else {
      this.proposalDetails.MobileNumber = "";
      // swal({
      //   closeOnClickOutside: false,
      //   text: "Mobile number entered is not valid."
      // });
    }
  }

  mobileValidate1() {
    let isNumberValidate = this.cs.mobileValidation(this.sendDetails.mobileno);
    if (isNumberValidate) {
      this.sendDetails.mobileno = this.sendDetails.mobileno;
      this.moberrorFlag = false;
    } else {
      this.sendDetails.mobileno = "";
      this.moberrorFlag = true;
      // swal({
      //   closeOnClickOutside: false,
      //   text: "Mobile number entered is not valid."
      // });
    }
  }

  insuredNamevalidate() {
    if (this.proposalDetails.CustomerName.length < 4) {
      swal({
        closeOnClickOutside: false,
        text: "Insured name should have at least 4 consecutive character",
      });
    }
  }

  // //create token for 2.0 apis

  createBizToken() {
    this.cs.get("middleware/token?Scope=2").subscribe((res: any) => {
      localStorage.setItem("bizToken", JSON.stringify(res));
    });
    // .catch((err:any) => { swal({  text: err });
    //   this.cs.loaderStatus = false; });
  }

  createAllRiskBizToken(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.get("middleware/token?Scope=14").subscribe((res: any) => {
        localStorage.setItem("allRiskbizToken", JSON.stringify(res));
        resolve();
      });
    }).catch((err: any) => {
      swal({ text: err });
      this.cs.loaderStatus = false;
    });
  }

  // Get Customer ID
  custID(ev: any) {
    this.searchName = false;
    this.searchid = true;
    this.customerID = ev.target.value;
    this.proposalDetails.searchname = "";
  }

  // Get Customer Name
  custName(ev: any) {
    this.searchid = false;
    this.searchName = true;
    this.customerName = ev.target.value;
    this.proposalDetails.searchid = "";
  }

  searchext() {
    this.proposalDetails.searchid = "";
    this.proposalDetails.searchname = "";
    this.custFlag = false;
    this.customerID = "";
  }

  // Search Customer
  searchCustomer() {
    this.cs.loaderStatus = true;
    this.cs
      .get("Customer/GetCustomerDetailsById?Id=" + this.customerID)
      .subscribe((res: any) => {
        if (res.status == "SUCCESS") {
          this.custDetails = res;
          localStorage.setItem("customerData", JSON.stringify(res));
          this.cs.loaderStatus = false;
          this.proposalDetails.CustomerName = this.custDetails.customerName;
          this.proposalDetails.employeeName =this.proposalDetails.CustomerName;
          this.proposalDetails.PANCardNo = this.custDetails.pan;
          this.proposalDetails.Email = this.custDetails.emailId;
          this.proposalDetails.MobileNumber = this.custDetails.mobilenumber;
          this.proposalDetails.PANCardNo = this.custDetails.pan;
          this.proposalDetails.AddressLine1 =
            this.custDetails.mailingaddresslinE1;
          this.proposalDetails.PinCode = this.custDetails.mailingpincode;
          if (this.custDetails.gender == "Male") {
            this.proposalDetails.title = "Mr";
          } else {
            this.proposalDetails.title = "Mrs";
          }
          if (
            this.custDetails.mailingpincode != null ||
            res.status != "SUCCESS" ||
            this.custDetails.mailingpincode != undefined ||
            this.custDetails.mailingpincode != ""
          ) {
            this.getPincodeDetails(this.custDetails.mailingpincode);
          }
        } else {
          this.cs.loaderStatus = false;
          swal({
            text: "No Data Found",
          });
        }
      });
    // .catch((err:any) => {
    //   swal({

    //     text: err
    //   });
    //   this.cs.loaderStatus = false;
    // });
  }

  searchModalName(ev: any) {
    this.searchCustomerbyName();
  }

  // Search Customer
  searchCustomerbyName() {
    this.cs.loaderStatus = true;
    this.cs
      .get("Customer/GetCustomerList?customerName=" + this.customerName)
      .subscribe((res: any) => {
        if (res.length != 0) {
          this.custDetailsName = res;
          this.custFlag = true;
          this.cs.loaderStatus = false;
        } else {
          this.cs.loaderStatus = false;
          swal({
            text: "No Data Found",
          });
        }
      });
    // .catch((err:any) => {
    //   swal({

    //     text: err
    //   });
    //   this.cs.loaderStatus = false;
    // });
  }

  // Fetch Details for Name
  fetchdetails(index: any) {
    let customerID = this.custDetailsName[index].pfCustomerID;
    this.customerID = customerID;
    this.searchCustomer();
  }

  prevpol(ev: any) {
    // this.cs.get("PrevPolicy/GetPreviousInsurer").subscribe((prevres: any) => {
    //   this.insurerlist = prevres;
    //   let compname;
    //   compname = prevres.find(
    //     (c) =>
    //       // Changes
    //       c.shortName == this.rtoDetailValues.previousInsurerCode
    //   ).companyName;
    //   this.proposalDetails.PreviousInsurerName = compname;
    // });
    /**
     * Get Previous InsurerName api change
     * Author :- Sumit
     * date :- 12-01-2022
     */
    let previousInsurer: any = this.cs.PreviousInsurerList;;
    this.insurerlist = previousInsurer;
    // let compname;
    // compname = previousInsurer.find(
    //   (c) => c.shortName == this.rtoDetailValues.previousInsurerCode
    // ).companyName;
    // this.proposalDetails.PreviousInsurerName = compname;
    console.log(this.rtoDetailValues);
    if (!this.cs.isUndefineORNull(this.rtoDetailValues)) {
      let compname;
      let compname1;
      let compname2;
      compname = previousInsurer.find(
        (c) => c.shortName == this.rtoDetailValues.previousInsurerCode
      );
      compname1 = previousInsurer.find(
        (c) => c.companyCode == this.rtoDetailValues.previousInsurerCode
      );
      compname2 = previousInsurer.find(
        (c) => c.companyName == this.rtoDetailValues.previousInsurer
      );
      console.log(this.rtoDetailValues.previousInsurer, this.rtoDetailValues.previousInsurer, compname, compname1, compname2);
      if (!this.cs.isUndefineORNull(compname)) {
        this.proposalDetails.PreviousInsurerName = compname.companyName;
      } else if (!this.cs.isUndefineORNull(compname1)) {
        this.proposalDetails.PreviousInsurerName = compname1.companyName;
      } else if (!this.cs.isUndefineORNull(compname2)) {
        this.proposalDetails.PreviousInsurerName = compname2.companyName;
      } else {
        this.proposalDetails.PreviousInsurerName = '';
      }
    } else {
      this.proposalDetails.PreviousInsurerName = ''
    }


    if (ev.target.checked == true) {
      this.showPrev = true;
    } else if (ev.target.checked == false) {
      this.showPrev = false;
    }
  }

  // Constitution of Business GST
  getConstName(ev: any) {
    if (ev.target.value == "0") {
      this.constName = "Constitution of Business";
    } else if (ev.target.value == "1") {
      this.constName = "Non Resident Entity";
    } else if (ev.target.value == "2") {
      this.constName = "Foreign company registered in India";
    } else if (ev.target.value == "3") {
      this.constName = "Foreign LLP";
    } else if (ev.target.value == "4") {
      this.constName = "Government Department";
    } else if (ev.target.value == "5") {
      this.constName = "Hindu Undivided Family";
    } else if (ev.target.value == "6") {
      this.constName = "LLP Partnership";
    } else if (ev.target.value == "7") {
      this.constName = "Local Authorities";
    } else if (ev.target.value == "8") {
      this.constName = "Partnership";
    } else if (ev.target.value == "9") {
      this.constName = "Private Limited Company";
    } else if (ev.target.value == "10") {
      this.constName = "Proprietorship";
    } else {
      this.constName = "Others";
    }
  }

  // Customer Type GST
  getCustTypeName(ev: any) {
    if (ev.target.value == "0") {
      this.custType = "Customer Type";
    } else if (ev.target.value == "21") {
      this.custType = "General";
    } else if (ev.target.value == "22") {
      this.custType = "EOU/STP/EHTP";
    } else if (ev.target.value == "23") {
      this.custType = "Government";
    } else if (ev.target.value == "24") {
      this.custType = "Overseas";
    } else if (ev.target.value == "25") {
      this.custType = "Related parties";
    } else if (ev.target.value == "26") {
      this.custType = "SEZ";
    } else {
      this.custType = "Others";
    }
  }

  // Registration Status GST
  getGSTName(ev: any) {
    if (ev.target.value == "0") {
      this.gstName = "GST Registration Status";
    } else if (ev.target.value == "41") {
      this.gstName = "ARN Generated";
    } else if (ev.target.value == "42") {
      this.gstName = "Provision ID Obtained";
    } else if (ev.target.value == "43") {
      this.gstName = "To be commenced";
    } else if (ev.target.value == "44") {
      this.gstName = "Enrolled";
    } else {
      this.gstName = "Not applicable";
    }
  }

  // Go to Dashboard
  goToDashboard() {
    this.cs.geToDashboard();
  }

  // For addonCOvers
  forAddonCover() {
    this.router.navigateByUrl("quote");
  }

  // State Data
  getStateData(): Promise<any> {
    return new Promise((resolve: any) => {
      /**
       * Author :- Sumit
       * date :- 12-01-2021
       * added the condition for calling an State/GetStateList api
       */
      this.stateMaster = this.cs.StateListData;
      // if (localStorage.getItem("StateList")) {
      //   let demo = localStorage.getItem("StateList");
      //   let decryptedStateList = this.api.decrypt(demo);
      //   this.stateMaster = JSON.parse(decryptedStateList);
      // } else {
      //   this.cs.get("State/GetStateList").subscribe((res: any) => {
      //     this.stateMaster = res;
      //     let encryptedStateList = this.api.encrypt(res);
      //     localStorage.setItem("StateList", encryptedStateList);
      //   });
      // }
      resolve();
    });
  }

  // GST STate Data
  GSTState(ev: any) {
    let regTest = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/.test(
      ev.target.value
    );
    if (regTest) {
      let inputText = ev.target.value;
      let gstno = inputText.slice(0, 2);
      this.getStateData().then(() => {
        this.GSTstateName = this.stateMaster.find(
          (c) => c.gsT_STATE_CODE == gstno
        ).statE_NAME;
        this.proposalDetails.GSTToState = this.GSTstateName;
      });
    } else {
      swal({
        text: "Invalid GSTIN number. Kindly enter correct GSTIN number",
      });
      document.getElementById("gstinno").focus();
      let gstno = document.getElementById("gstinno") as HTMLInputElement;
      gstno.select();
    }
  }

  getProposalData(): Promise<any> {
    return new Promise((resolve: any) => {
      if (this.productCode == commonData.allriskProductCode) {
        this.evpropReq = JSON.parse(localStorage.getItem("EVPropDatareq"));
        this.evpropRes = JSON.parse(localStorage.getItem("EVPropDatares"));
        this.propReq = this.evpropReq;
        this.propRes = this.evpropRes;

        // if ((localStorage.getItem("IsSCPA") == "true") && 
        //     (this.cs.isUndefineORNull(localStorage.getItem("scpaPropDatareq")))
        // ) {
        //   this.scpapropReq = JSON.parse(localStorage.getItem("scpaPropDatareq"));
        //   this.scpapropRes = JSON.parse(localStorage.getItem("scpaPropDatares"));
        // }
      } else {
        this.propReq = JSON.parse(localStorage.getItem("PropDatareq"));
        this.propRes = JSON.parse(localStorage.getItem("PropDatares"));
      }
      resolve();
    });
  }

  sendCustLink() {
    this.getProposalData().then(() => {
      if (!this.cs.isUndefineORNull(this.propReq.CustomerDetails.Email)) {
        this.customerEmail = this.propReq.CustomerDetails.Email;
      }

      if (
        !this.cs.isUndefineORNull(this.propReq.CustomerDetails.MobileNumber)
      ) {
        this.custMobile = this.propReq.CustomerDetails.MobileNumber;
      }

      // $('#sendLinkModal').modal('show');
      this.sendCustPaymentLinkFlag = true;
      this.watchMe = "fly";
    });
  }

  closeLink() {
    // this.paymentModeFlag = true;
    this.sendCustPaymentLinkFlag = false;
    this.watchMe = "fade";
  }

  validateEmail(emailData: any) {
    var email = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    return email.test(String(emailData).toLowerCase());
  }
  // send customer link

  sendLink() {
    let custEmail, custMobile;

    (<any>window).ga("send", "event", {
      eventCategory: "Customer Link",
      eventLabel:
        "Customer Link Clicked" + "" + this.product + "" + this.policyType,
      eventAction: "Customer Link Clicked",
      eventValue: 10,
    });

    // if (email) {
    //   custEmail = btoa(email);
    // } else {
    //   custEmail = btoa(this.customerEmail);
    // }

    // let custMobile = btoa(this.custMobile);

    custEmail = this.customerEmail;
    custMobile = this.custMobile;

    // AES Solution1
    // let custEmail = CryptoJS.AES.encrypt(JSON.stringify(this.customerEmail),'aXBhcnRuZXJOeXNhU2VjcmV0QDA3S2V5').toString();
    // let custMobile = CryptoJS.AES.encrypt(JSON.stringify(this.custMobile), 'aXBhcnRuZXJOeXNhU2VjcmV0QDA3S2V5').toString();

    let regemailtest =
      /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(
        this.customerEmail
      );

    if (
      this.customerEmail == "" ||
      this.customerEmail == undefined ||
      this.customerEmail == null ||
      !regemailtest
    ) {
      this.cs.loaderStatus = false;
      swal({
        closeOnClickOutside: false,
        text: "Insert Email Id",
      }).then(() => {
        // $('#sendLinkModal').modal('show');
      });
    } else if (
      this.custMobile == "" ||
      this.custMobile == undefined ||
      this.custMobile == null
    ) {
      this.cs.loaderStatus = false;
      swal({
        closeOnClickOutside: false,
        text: "Insert Mobile Number",
      }).then(() => {
        // $('#sendLinkModal').modal('show');
      });
    } else {
      if (this.breakinFlag == true) {
        this.breakinInspectionClear().then(() => {
          this.getCustomerLink(custEmail, custMobile);
          this.PaymentMode = "4";
          localStorage.setItem("senCustLink", "true");
        })
      } else if (this.productCode == commonData.allriskProductCode || this.isSCPARequired) {
        this.getAllRiskCustomerLink(custEmail, custMobile);
        this.PaymentMode = "4";
        localStorage.setItem("senCustLink", "true");
      } else {
        this.getCustomerLink(custEmail, custMobile);
        this.PaymentMode = "4";
        localStorage.setItem("senCustLink", "true");
      }


    }

    // this.getCustomerLink(custEmail, custMobile);
    // this.PaymentMode = "4";
    // localStorage.setItem('senCustLink',"true");
  }

  getCustomerLink(email: any, mobile: any) {
    let body = {
      CorrelationID: this.propRes.correlationId,
      ProposalNo: this.propRes.generalInformation.proposalNumber,
      EmailID: this.cs.encrypt(email, commonData.aesnysaKey),
      MobileNo: this.cs.encrypt(mobile, commonData.aesnysaKey),
    };
    let url = "CustomerPayment/getLink";
    let str = JSON.stringify(body);
    let status = "N";
    let smsStatus = "N";
    this.cs.loaderStatus = true;

    this.cs.post(url, str).then((res: any) => {
      const emailStatus = res.isLinkSent ? true : false;
      const mobileStatus = res.isSMSSent ? true : false;
      const checkBoth = emailStatus && mobileStatus ? true : false;
      this.paymentStatusDetails(status, smsStatus, res, email);
      let msg;
      let maskedEmail = email.replace(
        /(.{2})(.*)(?=@)/,
        function (gp1, gp2, gp3) {
          for (let i = 0; i < gp3.length; i++) {
            gp2 += "X";
          }
          return gp2;
        }
      );
      let maskedMobile =
        mobile.slice(0, 2) + mobile.slice(2).replace(/.(?=...)/g, "X");
      if (checkBoth) {
        msg = `Payment link has been sent to ${maskedEmail} and ${maskedMobile}.`;
        status = "Y";
        smsStatus = "Y";
      } else {
        if (emailStatus) {
          msg = `Payment link has been sent to ${maskedEmail}.`;
          status = "Y";
          smsStatus = "N";
        } else if (mobileStatus) {
          msg = `Payment link has been sent to ${maskedMobile}.`;
          status = "N";
          smsStatus = "Y";
        } else if (!checkBoth) {
          msg = `Failed to send payment link to ${maskedEmail} and ${maskedMobile}.`;
          status = "N";
          smsStatus = "N";
        }
      }
      swal({ text: msg }).then(() => {
        this.cs.loaderStatus = false;
      });
    });
  }

  paymentStatusDetails(emailStatus, smsStatus, res, email) {
    this.IsPayLinkSent = emailStatus;
    this.PayLinkReciever = email;
    this.isSMSSent = smsStatus;
    this.CustomerPayLink = res.paymentLink;
    this.custLink = res;

    if (this.productCode == commonData.allriskProductCode) {
      this.propReq = this.evpropReq;
      this.propRes = this.evpropRes;
      this.updateAllRiskPaymentStatuscustlink().then(() => {
        this.updateAllRiskPolicyStatusCustLink();
      });

      if ((localStorage.getItem("IsSCPA") == "true") &&
        (this.cs.isUndefineORNull(this.scpapropReq))
      ) {
        this.scpapropReq = JSON.parse(localStorage.getItem("scpaPropDatareq"));
        this.scpapropRes = JSON.parse(localStorage.getItem("scpaPropDatares"));
        this.updateSCPAPaymentStatuscustlink().then(() => {
          this.updateSCPAPolicyStatusCustLink();
        });
      }
    } else {
      this.updatePaymentStatuscustlink().then(() => {
        this.updatePolicyStatusCustLink();
      });
    }

    this.cs.showProposal = false;
    this.changedEventListner.emit(true);
    this.router.navigateByUrl("/quote").then(() => {
      this.cs.clearLocalStorage();
      this.cs.clearQuoteDate();
      this.cs.clearPropData();
      this.router
        .navigateByUrl("/", { skipLocationChange: true })
        .then(() => this.router.navigate(["/quote"]));
    });
  }

  // Update Policy Payment Data to database
  updatePaymentStatuscustlink(): Promise<any> {
    return new Promise((resolve: any) => {
      console.log("Proppp Request", this.propReq, this.scpapropReq, this.evpropReq);
      let body;
      body = {
        policyNo: "",
        coverNoteNo: "",
        //proposalNo: this.propRes.generalInformation.proposalNumber,
        dealID: this.propReq.DealId,
        // customerId: this.propRes.generalInformation.customerId,
        pfPaymentID: "",
        paymentEntryErrorID: "",
        paymentEntryErrorText: "",
        paymentEntryStatus: "",
        paymentTagErrorID: "",
        paymentTagErrorText: "",
        paymentTagStatus: "",
        message: "",
        PaymentMode: this.PaymentMode,
        statusMessage: "",
        paymsRequestID: "",
        paymentRS: "", // here pass full response string which is coming from Mihir api
        corelationID: this.propRes.correlationId, // pass correlation from payment api response
      };

      if (this.productCode == commonData.allriskProductCode) {
        body.proposalNo = this.propRes.proposalNumber;
        body.customerId = this.propRes.customerId;
      } else {
        body.proposalNo = this.propRes.generalInformation.proposalNumber;
        body.customerId = this.propRes.generalInformation.customerId;
      }
      let str = JSON.stringify(body);
      this.cs.post("Payment/UpdatePaymentDetails", str).then((res: any) => {
        this.updatePayment = res;
        resolve();
      });
    });
  }

  updatePolicyStatusCustLink() {
    console.info("PropRes", this.propRes);
    console.info("PropReq", this.propReq);
    this.totalTax = Math.round(this.propRes.totalTax);
    let scpaReq = JSON.parse(localStorage.getItem('scpaPropDatareq'));
    let scpaRes = JSON.parse(localStorage.getItem('scpaPropDatares'));
    let AllRiskReq = JSON.parse(localStorage.getItem('EVPropDatareq'));
    let AllRiskRes = JSON.parse(localStorage.getItem('EVPropDatares'));
    if (this.productCode == "2320" || this.productCode == "2319") {
      this.propRes.specialDiscount = "0";
      this.propRes.finalPremium = JSON.stringify(this.propRes.finalPremium);
      this.propRes.policyType = 1;
    }
    if (this.productCode == "2320") {
      this.propRes.policySubType = 9;
    } else if (this.productCode == "2319") {
      this.propRes.policySubType = 10;
    }
    let policysubType = this.propRes.policySubType;
    let breakinFlag: any = localStorage.getItem("breakinFlag");
    if (breakinFlag == "true") {
      let breakinData: any = JSON.parse(localStorage.getItem("breakinData"));
      this.SubProductType = breakinData.policySubType;
    } else {
      if (this.productCode == "2312") {
        this.SubProductType = "1";
      } else if (this.productCode == "2311") {
        this.SubProductType = "2";
      } else if (this.productCode == "2320") {
        this.SubProductType = "9";
      } else if (this.productCode == "2319") {
        this.SubProductType = "10";
      } else if (this.productCode == commonData.allriskProductCode) {
        this.SubProductType = "11";
      }
    }


    if (this.productCode == commonData.allriskProductCode) {
      this.totalTax = Math.round(this.propRes.gstserviceresponse.gstservicedetails.totaltax);
      let policysubType = "11";
    }
    let body;
    body = {
      DealID: this.propReq.DealId,
      PolicyType: "1", //String
      PolicySubType: this.SubProductType, //String
      // ProposalNumber: this.propRes.generalInformation.proposalNumber,
      PolicyNumber: "",
      PolicyStartDate: this.propReq.PolicyStartDate,
      PolicyEndDate: this.propReq.PolicyEndDate,
      Status: this.propRes.statusID,
      // BasicPremium: this.propRes.totalLiabilityPremium,
      // Discount: parseFloat(this.propRes.specialDiscount), //String
      ServiceTax: this.totalTax,
      TotalPremium: parseFloat(this.propRes.finalPremium), //String
      AddressID: "",
      Isactive: "1",
      PaymentID: JSON.stringify(this.updatePayment.paymentID),
      SalesTax: "",
      Surcharge: "",
      TotalTax: this.totalTax, //String
      // TransFor: this.propRes.generalInformation.transactionType,
      TaxRate: "",
      CustomerType: this.propReq.CustomerType,
      PreviousPolicyNumber: null,
      PFPolicyNo: "",
      PFProposalNo: "",
      PFCovernoteNo: "",
      // PFCustomerID: this.propRes.generalInformation.customerId,
      CustomerName: this.propReq.CustomerDetails.CustomerName,
      PFPaymentID: "",
      SumInsured: null,
      Planname: null,
      // PolicyTenure: this.propRes.generalInformation.tenure,
      // PolicyTPTenure: this.propRes.generalInformation.tpTenure,
      PolicyPACoverTenure: JSON.stringify(this.propReq.PACoverTenure),
      UTGSTAmount: "0",
      TotalGSTAmount: "0",
      CGSTAmountRate: null,
      SGSTAmountRate: null,
      IGSTAmountRate: null,
      UTGSTAmountRate: null,
      TOTALGSTAmountRate: null,
      TransOn: "2020-03-18",
      CreatedBy: null,
      CreatedOn: "2020-03-18",
      ModifiedBy: null,
      ModifiedOn: "2020-03-18",
      CorelationID: this.propRes.correlationId,
      IsPayLinkSent: this.IsPayLinkSent,
      PayLinkReciever: this.PayLinkReciever,
      CustomerPayLink: this.CustomerPayLink,
      smsreciver: this.custMobile,
      isSMSSent: this.isSMSSent,
    };

    if (this.SubProductType == "11") {
      if (this.cs.isUndefineORNull(AllRiskReq) || AllRiskRes)
        body.ProposalNumber = this.propRes.proposalNumber;
      body.BasicPremium = Math.round(this.propRes.gstserviceresponse.gstservicedetails.grossamount);
      body.TransFor = this.propReq.BusinessType;
      body.PFCustomerID = this.propRes.customerId;
      body.PolicyTenure = this.propReq.PolicyTenure;
      body.PolicyTPTenure = this.propReq.PolicyTenure;
      body.TotalPremium = Math.round(this.propRes.gstserviceresponse.gstservicedetails.finalamount);
      body.Discount = 0;
    } else {
      body.ProposalNumber = this.propRes.generalInformation.proposalNumber;
      body.BasicPremium = this.propRes.totalLiabilityPremium;
      body.TransFor = this.propRes.generalInformation.transactionType;
      body.PFCustomerID = this.propRes.generalInformation.customerId;
      body.PolicyTenure = this.propRes.generalInformation.tenure;
      body.PolicyTPTenure = this.propRes.generalInformation.tpTenure;
      body.Discount = parseFloat(this.propRes.specialDiscount);
    }

    if (this.quoteReq != null) {
      if (this.quoteReq.Tenure == "1" && this.quoteReq.TPTenure == "0") {
        body.IsStandaloneOD = true;
      }
    } else {
      body.IsStandaloneOD = this.instaData.isStandaloneOD;
    }

    if (
      this.paramDataValues.iPartnerLogin.isSubagent != null ||
      this.paramDataValues.iPartnerLogin.isSubagent != undefined
    ) {
      body.IsSubagent = true;
      body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
    }
    let str = JSON.stringify(body);
    this.cs.post("Policy/AddPolicyDetails", body).then((res: any) => { });
  }

  // All Risk - send cust link
  getAllRiskCustomerLink(email: any, mobile: any) {
    let body;
    let bundleIDSAOD = JSON.parse(localStorage.getItem("bundleIDSAOD"));
    body = {
      ProposalNo: this.isSCPARequired ? this.calculatedData.generalInformation.proposalNumber : this.propRes.proposalNumber,
      CorrelationID: this.propRes.correlationId,
      BundleID: this.isSCPARequired ? bundleIDSAOD : this.bundleID,
      EmailID: this.cs.encrypt(email, commonData.aesnysaKey),
      MobileNo: this.cs.encrypt(mobile, commonData.aesnysaKey),
    };

    let url = "CustomerPayment/getLink";
    let str = JSON.stringify(body);
    let status = "N";
    let smsStatus = "N";
    this.cs.loaderStatus = true;

    this.cs.post(url, str).then((res: any) => {
      const emailStatus = res.isLinkSent ? true : false;
      const mobileStatus = res.isSMSSent ? true : false;
      const checkBoth = emailStatus && mobileStatus ? true : false;
      this.paymentStatusDetails(status, smsStatus, res, email);
      let msg;
      let maskedEmail = email.replace(
        /(.{2})(.*)(?=@)/,
        function (gp1, gp2, gp3) {
          for (let i = 0; i < gp3.length; i++) {
            gp2 += "X";
          }
          return gp2;
        }
      );
      let maskedMobile =
        mobile.slice(0, 2) + mobile.slice(2).replace(/.(?=...)/g, "X");
      if (checkBoth) {
        msg = `Payment link has been sent to ${maskedEmail} and ${maskedMobile}.`;
        status = "Y";
        smsStatus = "Y";
      } else {
        if (emailStatus) {
          msg = `Payment link has been sent to ${maskedEmail}.`;
          status = "Y";
          smsStatus = "N";
        } else if (mobileStatus) {
          msg = `Payment link has been sent to ${maskedMobile}.`;
          status = "N";
          smsStatus = "Y";
        } else if (!checkBoth) {
          msg = `Failed to send payment link to ${maskedEmail} and ${maskedMobile}.`;
          status = "N";
          smsStatus = "N";
        }
      }
      swal({ text: msg }).then(() => {
        this.cs.loaderStatus = false;
      });
    });
  }

  // Update all risk policy to database
  updateAllRiskPaymentStatuscustlink(): Promise<any> {
    return new Promise((resolve: any) => {
      // console.log("Proppp Request", this.propReq, this.scpapropReq, this.evpropReq);
      let AllRiskReq = JSON.parse(localStorage.getItem('EVPropDatareq'));
      let AllRiskRes = JSON.parse(localStorage.getItem('EVPropDatares'));

      this.propReq = AllRiskReq;
      this.propRes = AllRiskRes;

      let body;
      body = {
        policyNo: "",
        coverNoteNo: "",
        proposalNo: this.propRes.proposalNumber,
        dealID: this.propReq.DealId,
        customerId: this.propRes.customerId,
        pfPaymentID: "",
        paymentEntryErrorID: "",
        paymentEntryErrorText: "",
        paymentEntryStatus: "",
        paymentTagErrorID: "",
        paymentTagErrorText: "",
        paymentTagStatus: "",
        message: "",
        PaymentMode: this.PaymentMode,
        statusMessage: "",
        paymsRequestID: "",
        paymentRS: "", // here pass full response string which is coming from Mihir api
        corelationID: this.propRes.correlationId, // pass correlation from payment api response
      };

      let str = JSON.stringify(body);
      this.cs.post("Payment/UpdatePaymentDetails", str).then((res: any) => {
        this.updatePayment = res;
        resolve();
      });
    });
  }

  updateAllRiskPolicyStatusCustLink() {
    console.info("PropRes", this.propRes);
    console.info("PropReq", this.propReq);
    // let AllRiskReq = JSON.parse(localStorage.getItem('EVPropDatareq'));
    // let AllRiskRes = JSON.parse(localStorage.getItem('EVPropDatares'));

    // this.propReq = AllRiskReq;
    // this.propRes = AllRiskRes;
    // let policysubType = this.propRes.policySubType;
    // if (this.productCode == commonData.allriskProductCode) {
    this.SubProductType = "11";
    // }

    this.totalTax = Math.round(this.propRes.gstserviceresponse.gstservicedetails.totaltax);

    let body;
    body = {
      DealID: this.propReq.DealId,
      PolicyType: "1", //String
      PolicySubType: this.SubProductType, //String
      ProposalNumber: this.propRes.proposalNumber,
      PolicyNumber: "",
      PolicyStartDate: this.propReq.PolicyStartDate,
      PolicyEndDate: this.propReq.PolicyEndDate,
      Status: this.propRes.statusID,
      BasicPremium: Math.round(this.propRes.gstserviceresponse.gstservicedetails.grossamount),
      Discount: 0.0, //String
      ServiceTax: this.totalTax,
      TotalPremium: parseFloat(this.propRes.finalPremium), //String
      AddressID: "",
      Isactive: "1",
      PaymentID: JSON.stringify(this.updatePayment.paymentID),
      SalesTax: "",
      Surcharge: "",
      TotalTax: this.totalTax, //String
      TransFor: this.propReq.BusinessType,
      TaxRate: "",
      CustomerType: this.propReq.CustomerType,
      PreviousPolicyNumber: null,
      PFPolicyNo: "",
      PFProposalNo: "",
      PFCovernoteNo: "",
      PFCustomerID: this.propRes.customerId,
      CustomerName: this.propReq.CustomerDetails.CustomerName,
      PFPaymentID: "",
      SumInsured: null,
      Planname: null,
      PolicyTenure: this.propReq.PolicyTenure,
      PolicyTPTenure: this.propReq.PolicyTenure,
      PolicyPACoverTenure: JSON.stringify(this.propReq.PACoverTenure),
      UTGSTAmount: "0",
      TotalGSTAmount: "0",
      CGSTAmountRate: null,
      SGSTAmountRate: null,
      IGSTAmountRate: null,
      UTGSTAmountRate: null,
      TOTALGSTAmountRate: null,
      TransOn: "2020-03-18",
      CreatedBy: null,
      CreatedOn: "2020-03-18",
      ModifiedBy: null,
      ModifiedOn: "2020-03-18",
      CorelationID: this.propRes.correlationId,
      IsPayLinkSent: this.IsPayLinkSent,
      PayLinkReciever: this.PayLinkReciever,
      CustomerPayLink: this.CustomerPayLink,
      smsreciver: this.custMobile,
      isSMSSent: this.isSMSSent,
    };

    body.TotalPremium = Math.round(this.propRes.gstserviceresponse.gstservicedetails.finalamount);

    if (this.quoteReq != null) {
      if (this.quoteReq.Tenure == "1" && this.quoteReq.TPTenure == "0") {
        body.IsStandaloneOD = true;
      }
    } else {
      body.IsStandaloneOD = this.instaData.isStandaloneOD;
    }

    if (
      this.paramDataValues.iPartnerLogin.isSubagent != null ||
      this.paramDataValues.iPartnerLogin.isSubagent != undefined
    ) {
      body.IsSubagent = true;
      body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
    }
    let str = JSON.stringify(body);
    this.cs.post("Policy/AddPolicyDetails", body).then((res: any) => { });
  }

  // Update scpa policy to database
  updateSCPAPaymentStatuscustlink(): Promise<any> {
    return new Promise((resolve: any) => {
      // console.log("Proppp Request", this.propReq, this.scpapropReq, this.evpropReq);
      let scpaReq = JSON.parse(localStorage.getItem('scpaPropDatareq'));
      let scpaRes = JSON.parse(localStorage.getItem('scpaPropDatares'));

      this.scpapropReq = scpaReq;
      this.scpapropRes = scpaRes;

      let body;
      body = {
        policyNo: "",
        coverNoteNo: "",
        proposalNo: this.scpapropRes.proposalNumber,
        dealID: this.scpapropReq.DealId,
        customerId: this.scpapropRes.customerId,
        pfPaymentID: "",
        paymentEntryErrorID: "",
        paymentEntryErrorText: "",
        paymentEntryStatus: "",
        paymentTagErrorID: "",
        paymentTagErrorText: "",
        paymentTagStatus: "",
        message: "",
        PaymentMode: this.PaymentMode,
        statusMessage: "",
        paymsRequestID: "",
        paymentRS: "", // here pass full response string which is coming from Mihir api
        corelationID: this.scpapropRes.correlationId, // pass correlation from payment api response
      };

      let str = JSON.stringify(body);
      this.cs.post("Payment/UpdatePaymentDetails", str).then((res: any) => {
        this.updatePayment = res;
        resolve();
      });
    });
  }

  updateSCPAPolicyStatusCustLink() {
    console.info("SCPAPropRes", this.scpapropRes);
    console.info("SCPAPropReq", this.scpapropReq);
    // let scpaReq = JSON.parse(localStorage.getItem('scpaPropDatareq'));
    // let scpaRes = JSON.parse(localStorage.getItem('scpaPropDatares'));

    // this.scpapropReq = scpaReq;
    // this.scpapropRes = scpaRes;

    // let policysubType = this.propRes.policySubType;
    // if (this.productCode == commonData.allriskProductCode) {
    this.SubProductType = "11";
    // }

    this.totalTax = Math.round(this.scpapropRes.gstserviceresponse.gstservicedetails.totaltax);

    let body;
    body = {
      DealID: this.scpapropReq.DealId,
      PolicyType: "1", //String
      PolicySubType: this.SubProductType, //String
      ProposalNumber: this.scpapropRes.proposalNumber,
      PolicyNumber: "",
      PolicyStartDate: this.scpapropReq.PolicyStartDate,
      PolicyEndDate: this.scpapropReq.PolicyEndDate,
      Status: this.scpapropRes.statusID,
      BasicPremium: Math.round(this.scpapropRes.gstserviceresponse.gstservicedetails.grossamount),
      Discount: 0.0, //String
      ServiceTax: this.totalTax,
      TotalPremium: parseFloat(this.scpapropRes.finalPremium), //String
      AddressID: "",
      Isactive: "1",
      PaymentID: JSON.stringify(this.updatePayment.paymentID),
      SalesTax: "",
      Surcharge: "",
      TotalTax: this.totalTax, //String
      TransFor: this.scpapropReq.BusinessType,
      TaxRate: "",
      CustomerType: this.scpapropReq.CustomerType,
      PreviousPolicyNumber: null,
      PFPolicyNo: "",
      PFProposalNo: "",
      PFCovernoteNo: "",
      PFCustomerID: this.scpapropRes.customerId,
      CustomerName: this.scpapropReq.CustomerDetails.CustomerName,
      PFPaymentID: "",
      SumInsured: null,
      Planname: null,
      PolicyTenure: this.scpapropReq.PolicyTenure,
      PolicyTPTenure: this.scpapropReq.PolicyTenure,
      PolicyPACoverTenure: JSON.stringify(this.scpapropReq.PACoverTenure),
      UTGSTAmount: "0",
      TotalGSTAmount: "0",
      CGSTAmountRate: null,
      SGSTAmountRate: null,
      IGSTAmountRate: null,
      UTGSTAmountRate: null,
      TOTALGSTAmountRate: null,
      TransOn: "2020-03-18",
      CreatedBy: null,
      CreatedOn: "2020-03-18",
      ModifiedBy: null,
      ModifiedOn: "2020-03-18",
      CorelationID: this.scpapropRes.correlationId,
      IsPayLinkSent: this.IsPayLinkSent,
      PayLinkReciever: this.PayLinkReciever,
      CustomerPayLink: this.CustomerPayLink,
      smsreciver: this.custMobile,
      isSMSSent: this.isSMSSent,
    };

    body.TotalPremium = Math.round(this.scpapropRes.gstserviceresponse.gstservicedetails.finalamount);

    if (this.quoteReq != null) {
      if (this.quoteReq.Tenure == "1" && this.quoteReq.TPTenure == "0") {
        body.IsStandaloneOD = true;
      }
    } else {
      body.IsStandaloneOD = this.instaData.isStandaloneOD;
    }

    if (
      this.paramDataValues.iPartnerLogin.isSubagent != null ||
      this.paramDataValues.iPartnerLogin.isSubagent != undefined
    ) {
      body.IsSubagent = true;
      body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
    }
    let str = JSON.stringify(body);
    this.cs.post("Policy/AddPolicyDetails", body).then((res: any) => { });
  }


  // plutus api call
  checkForNewPaymentModule() {
    if (this.productCode != commonData.allriskProductCode) {
      let paymentDetailsForPOS = {
        CorrelationID: this.calculatedData.correlationId,
        amount: this.calculatedData.correlationId,
        identifier:
          "ipartner_policy_" +
          this.calculatedData.generalInformation.proposalNumber,
        totalLibilityAmount: this.calculatedData.totalLiabilityPremium,
        totalPremium: this.calculatedData.finalPremium,
        proposalNumber: this.calculatedData.generalInformation.proposalNumber,
        registrationDate: this.calculatedData.generalInformation.registrationDate,
      };
    }

    // //CreateToken + GetApplicationAccess merging code - Sejal
    if (!this.cs.isUndefineORNull(localStorage.getItem("AuthToken"))) {
      let encryptedTokenaccess = JSON.parse(localStorage.getItem("AuthToken"));
      let Parsedtokenaccess: any = this.api.decrypt(
        encryptedTokenaccess.applicationAccess
      );
      let tokenaccess = JSON.parse(Parsedtokenaccess);
      this.cs.isPlutusEnable = tokenaccess.isEnablePlutus;
      /**
       * DigitalPOS Change
       * Added the condition for redirection to plutus page for payment
       */
      if (
        this.cs.isPlutusEnable == true ||
        this.IsPOSTransaction_flag == true
      ) {
        this.showPaymentFlag = true;
        let scrollingElement = document.scrollingElement || document.body;
        $(scrollingElement).animate(
          {
            scrollTop: document.body.scrollHeight,
          },
          500
        );
      } else {
        this.showPaymentFlag = false;
        // this.router.navigateByUrl("payment");
      }
    }

    // this.cs.get1("Access/GetApplicationAccess").then((resp: any) => {
    //   /**
    //    * DigitalPOS Change For Adding the OR Condition For ALL POS User's to get redirected to plutus payment page
    //    */
    //   this.cs.isPlutusEnable = resp.isEnablePlutus;
    //   if (resp.isEnablePlutus == true || this.IsPOSTransaction_flag == true) {
    //     this.showPaymentFlag = true;
    //     let scrollingElement = document.scrollingElement || document.body;
    //     $(scrollingElement).animate(
    //       {
    //         scrollTop: document.body.scrollHeight,
    //       },
    //       500
    //     );
    //   } else {
    //     this.showPaymentFlag = false;
    //     this.router.navigateByUrl("payment");
    //   }
    // });
  }

  /**
   * Checking the paymsToken Avialable or not
   * Author :- Sumit
   * date :- 21-01-2022
   */
  PlutusPaymentPaymsToken() {
    this.cs.getPaymsToken().then((resp: any) => {
      localStorage.setItem("paymsToken", JSON.stringify(resp.accessToken));
      this.redirectToPlutusPayment();
    });
  }

  redirectToPlutusPayment() {
    let bankType = localStorage.getItem("bankType");
    console.log(this.dealDetailsfromIM.bankType);
    let instaFlag;
    if (this.dealDetailsfromIM.bankType == "INT") {
      instaFlag = 1;
    } else {
      instaFlag = 0;
    }

    let email = this.cs.encrypt(
      this.proposalDetails.Email,
      commonData.aesSecretKey
    );
    let phone = this.cs.encrypt(
      this.proposalDetails.MobileNumber,
      commonData.aesSecretKey
    );

    let body;

    body = {
      CorrelationID: this.calculatedData.correlationId,
      Email: email,
      ContactNo: phone,
      UserFlag: 0,
      IsInsta: instaFlag,
      PidFlag: 0,
      PreInsta: 0
    };

    if (this.productCode == commonData.allriskProductCode || this.isSCPARequired) {
      if(!this.isSCPARequired){
        body.DealID = this.EVQuoteReq.DealId;
      }else{
        body.DealID = this.scpaRequest.DealId;
      }
      

      if ((this.isSCPA == true && this.isMulti == true) || this.isSCPARequired) {
        let multiProp = JSON.parse(localStorage.getItem("allriskmultipleProposal"));
        let totalAmt = localStorage.getItem("allriskmultiTotalAmount");
        body.Amount = Math.round(JSON.parse(totalAmt));
        body.MultiFlag = 1;
        body.MultiProposal = multiProp;
      } else {
        body.Amount = Math.round(this.calculatedData.gstserviceresponse.gstservicedetails.finalamount);
        body.ProposalNo = this.calculatedData.proposalNumber;
        body.MultiFlag = 0;
        body.CustomerID = this.calculatedData.customerId;
      }
    } else {
      body.Amount = this.calculatedData.finalPremium;
      body.ProposalNo = this.calculatedData.generalInformation.proposalNumber;
      body.DealID = this.quoteReq.DealId;
      body.CustomerID = this.calculatedData.generalInformation.customerId;
      body.MultiFlag = 0;
    }

    let str = JSON.stringify(body);
    this.cs.loaderStatus = true;

    this.cs
      .postPayms("Redirection/AddPaymentRequest", str)
      .then((res: any) => {
        if (res.Status == "success") {
          this.cs.loaderStatus = false;
          if (this.breakinFlag == true) {
            this.breakinInspectionClear().then(() => {
              if (this.IsPOSTransaction_flag == true) {
                location.href =
                  res.URL +
                  "&MOBILENO=" +
                  this.POSmobileNumber +
                  "&channelName=DigitalPOS"; // Extended the url for POS
              } else {
                location.href = res.URL;
              }
            });
          } else {
            if (this.IsPOSTransaction_flag == true) {
              location.href =
                res.URL +
                "&MOBILENO=" +
                this.POSmobileNumber +
                "&channelName=DigitalPOS"; // Extended the url for POS
            } else {
              location.href = res.URL;
            }
          }
        } else {
          // this.router.navigateByUrl("payment");
          this.cs.loaderStatus = false;
        }
      })
      .catch((err: any) => {
        // this.router.navigateByUrl("payment");
        this.cs.loaderStatus = false;
      });
  }

  getMultiProposalData(evProposalNo, CustomerID, scpaProposalNo, CustomerID1, scpafinalamount, evfinalamount, finalamount) {
    // let checked = ev.target.checked;
    // if (checked) {
    //this.singleData.push(singledata);
    this.proposalData = [];
    this.proposalData.push({
      CustomerID: CustomerID,
      ProposalNo: evProposalNo,
    });

    this.proposalData.push({
      CustomerID: CustomerID1,
      ProposalNo: scpaProposalNo,
    });
    this.multitotalPremium.push({ totalPremium: evfinalamount });
    this.multitotalPremium.push({ totalPremium: scpafinalamount });

    if (finalamount == null) {
      finalamount = evfinalamount + scpafinalamount;
    }
    localStorage.setItem("allriskmultipleProposal", JSON.stringify(this.proposalData));
    localStorage.setItem("allriskmultiTotalAmount", JSON.stringify(finalamount));
  }


  generateAndSendPDF() {
    if (this.productCode == "2312") {
      this.policySubType = "1";
    } else if (this.productCode == "2311") {
      this.policySubType = "2";
    } else if (this.productCode == "2319") {
      this.policySubType = "10";
    } else if (this.productCode == "2320") {
      this.policySubType = "9";
    }
    // Logic to send this BTOA string to backend service
    if (
      this.sendDetails.mobileno == "" ||
      this.sendDetails.mobileno == undefined ||
      this.sendDetails.mobileno == null
    ) {
      this.cs.loaderStatus = false;
      swal({
        closeOnClickOutside: false,
        text: "Insert mobile number",
      });
    } else if (
      this.sendDetails.Email == "" ||
      this.sendDetails.Email == undefined ||
      this.sendDetails.Email == null
    ) {
      this.cs.loaderStatus = false;
      swal({
        closeOnClickOutside: false,
        text: "Insert Email Id",
      });
    } else {
      this.cs.loaderStatus = true;
      let DATA = document.getElementById("premium_breakup");
      html2canvas(DATA).then((canvas) => {
        const FILEURI = canvas.toDataURL("image/png");
        var imgWidth = 210;
        var pageHeight = 295;

        var imgHeight = (canvas.height * imgWidth) / canvas.width;
        var heightLeft = imgHeight;

        var doc = new jspdf("p", "mm", "a4", true);
        var position = 0;

        doc.addImage(
          FILEURI,
          "PNG",
          0,
          position,
          imgWidth,
          imgHeight,
          "",
          "FAST"
        );
        heightLeft -= pageHeight;

        while (heightLeft >= 0) {
          position = heightLeft - imgHeight;
          doc.addPage();
          doc.addImage(
            FILEURI,
            "PNG",
            0,
            position,
            imgWidth,
            imgHeight,
            "",
            "FAST"
          );
          heightLeft -= pageHeight;
        }
        // doc.save('Nysa.pdf');
        const pdfDoc = btoa(doc.output());
        let dataToSend = {
          files: this.cs.encryptPDF(pdfDoc, commonData.aesnysaKey),
          PolicySubType: this.policySubType,
          EmailTo: this.cs.encrypt(
            this.sendDetails.Email,
            commonData.aesnysaKey
          ),
          MobileNo: this.cs.encrypt(
            this.sendDetails.mobileno,
            commonData.aesnysaKey
          ),
          TotalPremium: JSON.stringify(this.quoteRes.finalPremium),
          VehicleRegNo: this.quoteReq.RegistrationNumber,
          CorrelationID: this.quoteRes.correlationId,
        };
        this.cs
          .post("PolicySchedule/SendPremiumBreakUpPdf", dataToSend)
          .then((res: any) => {
            let msg;

            this.cs.loaderStatus = false;
            if (res.status == "Success" && res.is_Email_Sent) {
              msg = "Link has been sent successfully";
              $("#premium_breakup").modal("hide");
              $("#emiProt_modal").modal("hide");
            } else {
              msg = "Failed to send email";
            }

            swal({ closeOnClickOutside: false, text: msg });
          });

        // return doc;
        // doc.save('Nysa.pdf');
      });

      // var elem = document.getElementById('premiumBreakup');
      // var img = document.getElementById('lomblogo')
      // let doc = this.cs.convertToPdf(elem, img);
    }
  }

  get getODSubTotal() {
    let total = 0;
    let count;
    if (this.productCode == "2312") {
      count = 8;
    } else if (this.productCode == "2311") {
      count = 14;
    }
    for (let i = 0; i < count; i++) {
      let data = String(document.getElementById(`odSub-${i + 1}`).innerHTML);
      total += Number(data.replace(/\D/g, ""));
    }
    return total;
  }

  get getTpTotal() {
    let total = 0;
    for (let i = 0; i < 3; i++) {
      let data = String(document.getElementById(`tpSub-${i + 1}`).innerHTML);
      total += Number(data.replace(/\D/g, ""));
    }
    return total;
  }

  get getliabTpTotal() {
    let total = 0;
    for (let i = 0; i < 4; i++) {
      let data = String(
        document.getElementById(`liabtpSub-${i + 1}`).innerHTML
      );
      total += Number(data.replace(/\D/g, ""));
    }
    return total;
  }

  get getdiscountTotal() {
    let total = 0;
    for (let i = 0; i < 5; i++) {
      let data = String(document.getElementById(`disc-${i + 1}`).innerHTML);
      total += Number(data.replace(/\D/g, ""));
    }
    this.premiumWithoutDiscount = this.quoteRes.packagePremium + total;
    return total;
  }

  breakinInspectionClear(): Promise<any> {
    return new Promise((resolve: any) => {
      let breakindata = JSON.parse(localStorage.getItem("breakinData"));
      let dealId = JSON.parse(localStorage.getItem("DealID"));
      let startDate = moment(new Date()).format("DD-MM-YYYY");

      /**
       * digitalPOS change
       * get values from localStorage for checking if conditions in html file
       * date :- 29-07-2021
       */
      if (this.IsPOSTransaction_flag == true) {
        this.posDealID = JSON.parse(localStorage.getItem("DealID"));
        this.deal_Id = this.posDealID;
      } else {
        this.deal_Id = this.quoteReq.DealId;
      }
      let body = {
        InspectionId: JSON.parse(breakindata.inspectionID),
        DealNo: this.deal_Id,
        ReferenceDate: startDate, //this.propRes.generalInformation.proposalDate,
        InspectionStatus: "OK",
        CorrelationId: breakindata.corelationID,
        ReferenceNo: this.calculatedData.generalInformation.proposalNumber,
      };
      let str = JSON.stringify(body);
      this.cs.postBiz("/breakin/clearinspectionstatus", str).subscribe(
        (res: any) => {
          resolve();
        },
        (err) => {
          let errMsg = this.msgService.errorMsg(err)
          swal({ closeOnClickOutside: false, text: errMsg });
        }
      );
    });
  }

  // homeEvent(ev){
  //   this.cs.getBreadcrumbData(ev);
  // }

  // PPD Details code for Package
  addpropPrev(ev: any) {
    this.isRecalc = true;
    this.showRecal = true;
    if (ev.target.value == "true") {
      this.proposalDetails.PropPreviousPolicyDetails = true;
      this.PackgetPrevEndDate(0);
      this.getInsurerName();
    } else {
      this.proposalDetails.PropPreviousPolicyDetails = false;
      this.recalculateQuote();
      // this.cs.getQuote(this.proposalDetails,this.recalquotereq,this.quoteRes,this.productCode)
      this.ngDoCheck();
    }
  }

  // Prev Policy Type
  PackprevPolType(ev: any) {
    this.isRecalc = true;
    this.showRecal = true;
    this.showAddon = false;
    if (ev.target.id == "comprehensive") {
      this.rollpolicyType = "Comprehensive";
      this.proposalDetails.PreviousPolicyType = "Comprehensive Package";
      this.claimNCB = true;
      this.ClaimOnPreviousPolicy = false;
      this.proposalDetails.TotalNoOfODClaims = "0";
    } else {
      this.rollpolicyType = "Bundled";
      this.proposalDetails.PreviousPolicyType = "TP";
      this.claimNCB = false;
      this.proposalDetails.NoOfClaimsOnPreviousPolicy = "0";
      this.ClaimOnPreviousPolicy = true;
    }
  }

  PrevEndDateget() {
    this.isRecalc = true;
    this.showRecal = true;
    let policystartdate: any = moment(
      this.proposalDetails.PreviousPolicyEndDate
    )
      .add(1, "days")
      .format("YYYY-MM-DD");
    let startDate: any = moment(new Date());
    let policyenddate: any = moment(
      new Date(this.proposalDetails.PreviousPolicyEndDate)
    );
    let diff = startDate.diff(policyenddate, "days");
    if (diff <= 0) {
      let policystartdate: any = moment(
        this.proposalDetails.PreviousPolicyEndDate
      )
        .add(1, "days")
        .format("YYYY-MM-DD");
      this.quoteReq.PolicyStartDate = policystartdate;
      // this.claimdiv = true
      this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
        .add(1, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
      // this.findexshowroom();
    } else if (diff > 0) {
      let todaydate = moment(new Date()).format("YYYY-MM-DD");
      this.quoteReq.PolicyStartDate = todaydate;
      this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
        .add(1, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
      // For break-IN policy end date
      this.quoteReq.PolicyEndDate = this.policyEndDate;
    } else {
      console.log("Smaller");
      this.changeprev(1);
    }
    if (this.prevtenure == 1) {
      this.changeprev(1);
    }
  }

  PackgetPrevEndDate(ev: any) {
    this.isRecalc = true;
    this.showRecal = true;
    let policystartdate: any = moment(
      this.proposalDetails.PreviousPolicyEndDate
    )
      .add(1, "days")
      .format("YYYY-MM-DD");
    let startDate: any = moment(new Date());
    let policyenddate: any = moment(
      new Date(this.proposalDetails.PreviousPolicyEndDate)
    );
    let diff = startDate.diff(policyenddate, "days");
    if (diff <= 0) {
      let policystartdate: any = moment(
        this.proposalDetails.PreviousPolicyEndDate
      )
        .add(1, "days")
        .format("YYYY-MM-DD");
      this.quoteReq.PolicyStartDate = policystartdate;
      this.claimdiv = true;
      this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
        .add(1, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
      // this.findexshowroom();
    } else if (diff > 0) {
      let todaydate = moment(new Date()).format("YYYY-MM-DD");
      this.quoteReq.PolicyStartDate = todaydate;
      this.policyEndDate = moment(this.quoteReq.PolicyStartDate)
        .add(1, "years")
        .subtract(1, "days")
        .format("YYYY-MM-DD");
      if (diff > 90) {
        this.claimNCB = true;
        this.proposalDetails.NoOfClaimsOnPreviousPolicy = "0";
        this.ClaimOnPreviousPolicy = false;
        this.claimdiv = false;
        this.proposalDetails.claims = "claimno";
      } else {
        // this.claimNCB = false;
        // this.proposalDetails.claims = 'claimyes';
        this.claimdiv = true;
      }
      // this.findexshowroom();
    } else {
      console.log("Smaller");
      this.changeprev(1);
    }

    if (this.prevtenure == 1) {
      this.changeprev(1);
    }
  }

  PackchangePrevEndDate(ev: any) {
    this.systemQC = false;
    this.isRecalc = true;
    this.showRecal = true;
  }
  PackchangePolicyNumber(ev: any) {
    this.systemQC = false;
    this.isRecalc = true;
    this.showRecal = true;
  }
  PackchangeNoofClaims(ev: any) {
    this.systemQC = false;
    this.isRecalc = true;
    this.showRecal = true;
  }
  PackncbPerc(ev: any) {
    this.isRecalc = true;
    this.showRecal = true;
    this.ncbValue = ev.target.value;
    this.proposalDetails.BonusOnPreviousPolicy = ev.target.value;
  }
  Packclickclaim(ev: any) {
    this.isRecalc = true;
    this.showRecal = true;
    if (ev.target.value == "claimno") {
      this.claimNCB = true;
      this.ClaimOnPreviousPolicy = false;
      this.proposalDetails.ClaimOnPreviousPolicy = false;
    } else {
      this.claimNCB = false;
      this.proposalDetails.NoOfClaimsOnPreviousPolicy = "0";
      this.ClaimOnPreviousPolicy = true;
      this.proposalDetails.ClaimOnPreviousPolicy = true;
    }
  }

  changeprev(ev: any) {
    this.isRecalc = true;
    this.showRecal = true;
    this.prevtenure = ev;
    if (this.prevtenure == 1) {
      let prevPolStartDate = moment(this.proposalDetails.PreviousPolicyEndDate)
        .subtract(1, "years")
        .add(1, "days")
        .format("YYYY-MM-DD");
      this.proposalDetails.PreviousPolicyStartDate = prevPolStartDate;
    } else if (this.prevtenure == 2) {
      let prevPolStartDate = moment(this.proposalDetails.PreviousPolicyEndDate)
        .subtract(2, "years")
        .add(1, "days")
        .format("YYYY-MM-DD");
      this.proposalDetails.PreviousPolicyStartDate = prevPolStartDate;
    } else if (this.prevtenure == 3) {
      let prevPolStartDate = moment(this.proposalDetails.PreviousPolicyEndDate)
        .subtract(3, "years")
        .add(1, "days")
        .format("YYYY-MM-DD");
      this.proposalDetails.PreviousPolicyStartDate = prevPolStartDate;
    }
    let prevStartDate = moment(
      this.proposalDetails.PreviousPolicyStartDate
    ).format("YYYY-MM-DD");
    let prevEndDate = moment(this.proposalDetails.PreviousPolicyEndDate).format(
      "YYYY-MM-DD"
    );
    this.cs
      .get(
        "PrevPolicy/CalculatePrevPolicyTenure?prevStartDate=" +
        prevStartDate +
        "&prevEndDate=" +
        prevEndDate
      )
      .subscribe((res: any) => {
        this.previousPolicyTen = res;
      });
  }

  PropgetInsurerName(ev: any) {
    this.isRecalc = true;
    this.showRecal = true;
    // if (localStorage.getItem("PreviousInsurerList")) {
    //   let decryptedPreviousInsurerList = localStorage.getItem(
    //     "PreviousInsurerList"
    //   );
    //   let decryptedPreviousInsurerList_V1 = this.api.decrypt(
    //     decryptedPreviousInsurerList
    //   );
    //   this.backupPreviousInsurerList = JSON.parse(
    //     decryptedPreviousInsurerList_V1
    //   );
    // }

    if (ev.target.value.length < 3 || ev.key == "Backspace") {
      this.backupPreviousInsurerList = this.cs.PreviousInsurerList;;
      this.insurerlist = this.backupPreviousInsurerList;
    } else {
      /**
       * Get Previous InsurerName api change and search functionality handled for companyCode or companyName
       * Author :- Sumit
       * date :- 13-01-2022
       */

      // if (localStorage.getItem("PreviousInsurerList")) {
      let PreviousInsurerList = this.cs.PreviousInsurerList;;
      const filterValue = ev.target.value.toLowerCase();
      this.insurerlist = PreviousInsurerList.filter((option) => {
        return (
          option.companyCode.toLowerCase().includes(filterValue) ||
          option.companyName.toLowerCase().includes(filterValue)
        );
      });
      // } else {
      //   this.insurerlist = this.api.getInsurerName(ev.target.value);
      // }
    }
  }

  ProponBlurSelectFirstVal(ev, type) {
    this.isRecalc = true;
    this.showRecal = true;
    if (type == "insurer") {
      let previousInsurer: any = this.cs.PreviousInsurerList;;
      this.insurerlist = previousInsurer;
      let obj = this.insurerlist.find((insList) => insList.companyName === ev);
      if (obj == undefined || obj == "" || obj == null) {
        this.proposalDetails.PreviousInsurerName =
          this.insurerlist[0].companyName;
      }
    }
  }

  PackdecCheck(ev: any) {
    if (ev.target.checked == false) {
      ev.target.checked = true;
    }
  }

  recalculateQuote() {
    if (this.proposalDetails.PropPreviousPolicyDetails == true) {
      if (
        this.proposalDetails.PreviousInsurerName == "" ||
        this.proposalDetails.PreviousInsurerName == null ||
        this.proposalDetails.PreviousInsurerName == undefined
      ) {
        swal({
          text: "Insert Previous Insurer name",
          closeOnClickOutside: false,
        });
      } else if (
        this.proposalDetails.PreviousPolicyType == "" ||
        this.proposalDetails.PreviousPolicyType == null ||
        this.proposalDetails.PreviousPolicyType == undefined
      ) {
        swal({
          text: "Kindly select Previous Policy Type",
          closeOnClickOutside: false,
        });
      } else if (
        this.proposalDetails.PreviousPolicyEndDate == "" ||
        this.proposalDetails.PreviousPolicyEndDate == null ||
        this.proposalDetails.PreviousPolicyEndDate == undefined
      ) {
        swal({
          text: "Kindly insert Previous Policy End date",
          closeOnClickOutside: false,
        });
      } else if (
        this.proposalDetails.PreviousPolicyNumber == "" ||
        this.proposalDetails.PreviousPolicyNumber == null ||
        this.proposalDetails.PreviousPolicyNumber == undefined
      ) {
        swal({
          text: "Kindly insert Previous Policy number",
          closeOnClickOutside: false,
        });
      } else {
        this.getInsurerName();
        this.companyCode = this.insurerlist.find(
          (c) => c.companyName == this.proposalDetails.PreviousInsurerName
        ).companyCode;
        this.proposalDetails.companyCode = this.companyCode;
        this.PrevEndDateget();
        this.recalquotereq.PolicyStartDate = this.quoteReq.PolicyStartDate;
        this.recalquotereq.PolicyEndDate = this.policyEndDate;
        this.proposalDetails.ClaimOnPreviousPolicy = this.ClaimOnPreviousPolicy;
        if (this.systemQC == true) {
          this.recalquotereq.IsQCByPass = true;
        } else {
          this.recalquotereq.IsQCByPass = false;
        }
        if (this.saveQuote == true || this.breakinFlag == true) {
          this.recalquotereq.NoOfDriver = undefined;
          this.recalquotereq.OtherDiscount = undefined;
          this.recalquotereq.OtherLoading = undefined;
          this.recalquotereq.EMIAmount = undefined;
        }
        this.isRecalc = false;
        this.showRecal = true;
        this.cs.getQuote(
          this.proposalDetails,
          this.recalquotereq,
          this.quoteRes,
          this.productCode
        );
        this.ngDoCheck();
      }
    } else {
      this.recalquotereq.PolicyStartDate = this.recalquotereq.PolicyStartDate;
      this.recalquotereq.PolicyEndDate = this.recalquotereq.PolicyEndDate;
      if (
        this.saveQuote == true ||
        this.breakinFlag == true ||
        this.iasFlag == true
      ) {
        this.recalquotereq.NoOfDriver = undefined;
        this.recalquotereq.OtherDiscount = undefined;
        this.recalquotereq.OtherLoading = undefined;
        this.recalquotereq.EMIAmount = undefined;
      }
      this.isRecalc = false;
      this.showRecal = true;
      this.cs.getQuote(
        this.proposalDetails,
        this.recalquotereq,
        this.quoteRes,
        this.productCode
      );
      this.ngDoCheck();
      // this.changeDetector.markForCheck();

      // let showrecalbutton = document.getElementById('recal_quote') as HTMLInputElement;
      // if(this.isRecalc == false){showrecalbutton.style.display = 'none'}
      // this.ngOnInit();
    }
  }

  inspectionType(ev: any) {
    if (ev.target.value == "selfinspection") {
      this.recalquotereq.IsSelfInspection = true;
      this.breakintype.isPreApprovedBreakIN = false;
      // this.cs.getQuote(this.proposalDetails,this.recalquotereq,this.quoteRes,this.productCode)
      this.recalculateQuote();
    } else if (ev.target.value == "ilinspection") {
      this.recalquotereq.IsSelfInspection = false;
      this.breakintype.isPreApprovedBreakIN = false;
      // this.cs.getQuote(this.proposalDetails,this.recalquotereq,this.quoteRes,this.productCode)
      this.recalculateQuote();
    } else {
      this.recalquotereq.IsSelfInspection = false;
      this.breakintype.isPreApprovedBreakIN = true;
    }
    this.ngDoCheck();
    // this.ngOnInit(); this.refreshDiv = true;
    // this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
    //       this.router.navigate(['/proposal']));
  }

  rtoData() {
    this.proposalDetails.PreviousPolicyEndDate = moment(
      this.rtoDetailValues.insuranceUpto
    ).format("YYYY-MM-DD");
    this.proposalDetails.PreviousPolicyNumber =
      this.rtoDetailValues.previousInsurerPolicyNo;
    // this.cs.get("PrevPolicy/GetPreviousInsurer").subscribe((prevres: any) => {
    //   this.insurerlist = prevres;
    //   let compname;
    //   compname = prevres.find(
    //     (c) => c.shortName == this.rtoDetailValues.previousInsurerCode
    //   ).companyName;
    //   this.proposalDetails.PreviousInsurerName = compname;
    /**
     * Get Previous InsurerName api change
     * Author :- Sumit
     * date :- 12-01-2022
     */
    let previousInsurer: any = this.cs.PreviousInsurerList;;
    this.insurerlist = previousInsurer;
    // let compname;
    // compname = previousInsurer.find(
    //   (c) => c.shortName == this.rtoDetailValues.previousInsurerCode
    // ).companyName;
    // this.proposalDetails.PreviousInsurerName = compname;
    let compname;
    let compname1;
    let compname2;
    compname = previousInsurer.find(
      (c) => c.shortName == this.rtoDetailValues.previousInsurerCode
    );
    compname1 = previousInsurer.find(
      (c) => c.companyCode == this.rtoDetailValues.previousInsurerCode
    );
    compname2 = previousInsurer.find(
      (c) => c.companyName == this.rtoDetailValues.previousInsurer
    );
    console.log(this.rtoDetailValues.previousInsurer, this.rtoDetailValues.previousInsurer, compname, compname1, compname2);
    if (!this.cs.isUndefineORNull(compname)) {
      this.proposalDetails.PreviousInsurerName = compname.companyName;
    } else if (!this.cs.isUndefineORNull(compname1)) {
      this.proposalDetails.PreviousInsurerName = compname1.companyName;
    } else if (!this.cs.isUndefineORNull(compname2)) {
      this.proposalDetails.PreviousInsurerName = compname2.companyName;
    } else {
      this.proposalDetails.PreviousInsurerName = 'OTHER';
    }

    if (this.quoteReq.PreviousPolicyDetails) {
      this.proposalDetails.PropPreviousPolicyDetails = true;

      this.PackgetPrevEndDate(0);
      this.showRecal = false;
      this.isRecalc = false;
      this.api
        .getiibServices(this.recalquotereq.RegistrationNumber)
        .subscribe((res: any) => {
          if (res.isClaimMade == "Y") {
            this.claimNCB = false;
            this.ClaimOnPreviousPolicy = true;
            this.proposalDetails.claims = "claimyes";
            this.proposalDetails.TotalNoOfODClaims = JSON.stringify(
              res.totalClaims
            );
          } else if (res.isClaimMade == "N") {
            this.claimNCB = true;
            this.ClaimOnPreviousPolicy = false;
            this.proposalDetails.claims = "claimno";
            if (this.ncbValue == 0) {
              this.systemQC = true;
              swal({
                text: "Congratulations, your policy will not be triggered for QC. Proceed for payment to get your instant policy",
                closeOnClickOutside: false,
              });
            }
          }
        });
    } else {
      this.showRecal = true;
    }
    // });
  }

  VahaanData() {
    this.proposalDetails.EngineNumber = this.cs.decryptUsingTripleDES(
      this.rtoDetailValues.eng_no
    );
    this.proposalDetails.ChassisNumber = this.cs.decryptUsingTripleDES(
      this.rtoDetailValues.chasi_no
    );
    this.proposalDetails.CustomerName = this.cs.decryptUsingTripleDES(
      this.rtoDetailValues.owner_sr
    );
    this.proposalDetails.employeeName =this.proposalDetails.CustomerName;
    // this.proposalDetails.MobileNumber = this.cs.decryptUsingTripleDES(this.rtoDetailValues.mobile_no);
    this.proposalDetails.AddressLine1 = this.cs.decryptUsingTripleDES(
      this.rtoDetailValues.PermenantAddress
    );
    this.proposalDetails.PreviousPolicyNumber =
      this.rtoDetailValues.previousInsurerPolicyNo;
    this.proposalDetails.PreviousPolicyEndDate = moment(
      this.rtoDetailValues.insuranceUpto
    ).format("YYYY-MM-DD");
    this.financierDetails.BranchName = this.rtoDetailValues.financer;
    this.PackgetPrevEndDate(this.rtoDetailValues.insuranceUpto);
    // this.proposalDetails.MobileNumber = this.cs.decryptUsingTripleDES(this.rtoDetailValues.mobile_no);
    if (this.rtoDetailValues.Pincode) {
      this.proposalDetails.PinCode = this.cs.decryptUsingTripleDES(
        this.rtoDetailValues.Pincode
      );
      let pincode = this.cs.decryptUsingTripleDES(this.rtoDetailValues.Pincode);
      console.log(pincode);
      if (!this.cs.isUndefineORNull(pincode)) {
        this.getPincodeDetailssave(
          this.cs.decryptUsingTripleDES(this.rtoDetailValues.Pincode)
        );
      }
    }
    // this.cs.get("PrevPolicy/GetPreviousInsurer").subscribe((prevres: any) => {
    //   let compname;
    //   compname = prevres.find(
    //     (c) => c.shortName == this.rtoDetailValues.previousInsurerCode
    //   ).companyName;
    //   this.proposalDetails.PreviousInsurerName = compname;
    /**
     * Get Previous InsurerName api change
     * Author :- Sumit
     * date :- 12-01-2022
     */
    let previousInsurer: any = this.cs.PreviousInsurerList;;
    // let compname;
    // compname = previousInsurer.find(
    //   (c) => c.shortName == this.rtoDetailValues.previousInsurerCode
    // ).companyName;
    // this.proposalDetails.PreviousInsurerName = compname;

    let compname;
    let compname1;
    let compname2;
    compname = previousInsurer.find(
      (c) => c.shortName == this.rtoDetailValues.previousInsurerCode
    );
    compname1 = previousInsurer.find(
      (c) => c.companyCode == this.rtoDetailValues.previousInsurerCode
    );
    compname2 = previousInsurer.find(
      (c) => c.companyName == this.rtoDetailValues.previousInsurer
    );
    console.log(this.rtoDetailValues.previousInsurer, this.rtoDetailValues.previousInsurer, compname, compname1, compname2);
    if (!this.cs.isUndefineORNull(compname)) {
      this.proposalDetails.PreviousInsurerName = compname.companyName;
    } else if (!this.cs.isUndefineORNull(compname1)) {
      this.proposalDetails.PreviousInsurerName = compname1.companyName;
    } else if (!this.cs.isUndefineORNull(compname2)) {
      this.proposalDetails.PreviousInsurerName = compname2.companyName;
    } else {
      this.proposalDetails.PreviousInsurerName = 'OTHER';
    }

    if (
      this.proposalDetails.PreviousPolicyEndDate != "" &&
      this.proposalDetails.PreviousPolicyNumber != ""
    ) {
      this.proposalDetails.PropPreviousPolicyDetails = true;
      // this.api
      //   .getiibServices(this.recalquotereq.RegistrationNumber)
      //   .subscribe((res: any) => {
      //     if (res.isClaimMade == "Y") {
      //       this.claimNCB = false;
      //       this.ClaimOnPreviousPolicy = true;
      //       this.proposalDetails.claims = "claimyes";
      //       this.proposalDetails.TotalNoOfODClaims = JSON.stringify(
      //         res.totalClaims
      //       );
      //     } else {
      //       this.claimNCB = true;
      //       this.ClaimOnPreviousPolicy = false;
      //       this.proposalDetails.claims = "claimno";
      //       if (this.ncbValue == 0) {
      //         this.systemQC = true;
      //         swal({
      //           text: "Congratulations, your policy will not be triggered for QC. Proceed for payment to get your instant policy",
      //           closeOnClickOutside: false,
      //         });
      //       }
      //     }
      //   });
    }
    // });
  }

  ZoopData() {
    this.proposalDetails.EngineNumber = this.cs.decryptUsingTripleDES(
      this.rtoDetailValues.eng_no
    );
    this.proposalDetails.ChassisNumber = this.cs.decryptUsingTripleDES(
      this.rtoDetailValues.chasi_no
    );
    this.proposalDetails.CustomerName = this.cs.decryptUsingTripleDES(
      this.rtoDetailValues.owner_sr
    );
    this.proposalDetails.employeeName =this.proposalDetails.CustomerName; 
    // this.proposalDetails.MobileNumber = this.cs.decryptUsingTripleDES(this.rtoDetailValues.mobile_no);
    this.proposalDetails.AddressLine1 = this.cs.decryptUsingTripleDES(
      this.rtoDetailValues.permenantAddress
    );
    this.proposalDetails.PreviousPolicyNumber =
      this.rtoDetailValues.previousInsurerPolicyNo;
    this.proposalDetails.PreviousPolicyEndDate = moment(
      this.rtoDetailValues.insuranceUpto
    ).format("YYYY-MM-DD");
    this.financierDetails.BranchName = this.rtoDetailValues.financer;
    this.PackgetPrevEndDate(this.rtoDetailValues.insuranceUpto);
    // this.proposalDetails.MobileNumber = this.cs.decryptUsingTripleDES(this.rtoDetailValues.mobile_no);
    if (this.rtoDetailValues.pincode) {
      this.proposalDetails.PinCode = this.cs.decryptUsingTripleDES(
        this.rtoDetailValues.pincode
      );
      this.getPincodeDetailssave(
        this.cs.decryptUsingTripleDES(this.rtoDetailValues.pincode)
      );
    }
    // this.cs.get("PrevPolicy/GetPreviousInsurer").subscribe((prevres: any) => {
    //   // console.log(prevres);
    //   let compname;
    //   compname = prevres.find(
    //     (c) => c.shortName == this.rtoDetailValues.previousInsurerCode
    //   ).companyName;
    //   this.proposalDetails.PreviousInsurerName = compname;
    /**
     * Get Previous InsurerName api change
     * Author :- Sumit
     * date :- 12-01-2022
     */
    let previousInsurer: any = this.cs.PreviousInsurerList;;
    // let compname;
    // compname = previousInsurer.find(
    //   (c) => c.shortName == this.rtoDetailValues.previousInsurerCode
    // ).companyName;
    // this.proposalDetails.PreviousInsurerName = compname;
    let compname;
    let compname1;
    let compname2;
    compname = previousInsurer.find(
      (c) => c.shortName == this.rtoDetailValues.previousInsurerCode
    );
    compname1 = previousInsurer.find(
      (c) => c.companyCode == this.rtoDetailValues.previousInsurerCode
    );
    compname2 = previousInsurer.find(
      (c) => c.companyName == this.rtoDetailValues.previousInsurer
    );
    console.log(this.rtoDetailValues.previousInsurer, this.rtoDetailValues.previousInsurer, compname, compname1, compname2);
    if (!this.cs.isUndefineORNull(compname)) {
      this.proposalDetails.PreviousInsurerName = compname.companyName;
    } else if (!this.cs.isUndefineORNull(compname1)) {
      this.proposalDetails.PreviousInsurerName = compname1.companyName;
    } else if (!this.cs.isUndefineORNull(compname2)) {
      this.proposalDetails.PreviousInsurerName = compname2.companyName;
    } else {
      this.proposalDetails.PreviousInsurerName = 'OTHER';
    }

    if (
      this.proposalDetails.PreviousPolicyEndDate != "" &&
      this.proposalDetails.PreviousPolicyNumber != ""
    ) {
      this.proposalDetails.PropPreviousPolicyDetails = true;
      this.api
        .getiibServices(this.recalquotereq.RegistrationNumber)
        .subscribe((res: any) => {
          if (res.isClaimMade == "Y") {
            this.claimNCB = false;
            this.ClaimOnPreviousPolicy = true;
            this.proposalDetails.claims = "claimyes";
            this.proposalDetails.TotalNoOfODClaims = JSON.stringify(
              res.totalClaims
            );
          } else {
            this.claimNCB = true;
            this.ClaimOnPreviousPolicy = false;
            this.proposalDetails.claims = "claimno";
            if (this.ncbValue == 0) {
              this.systemQC = true;
              swal({
                text: "Congratulations, your policy will not be triggered for QC. Proceed for payment to get your instant policy",
                closeOnClickOutside: false,
              });
            }
          }
        });
    }
    // });
  }

  AadrillaData() {
    this.api
      .getAadrillaData(this.recalquotereq.RegistrationNumber)
      .subscribe((res: any) => {
        if (res.statusMsg == "SUCCESS") {
          this.proposalDetails.EngineNumber = res.eng_no;
          this.proposalDetails.ChassisNumber = res.chasi_no;
          this.proposalDetails.CustomerName = this.cs.decryptUsingTripleDES(
            res.owner_name
          );
          this.proposalDetails.employeeName =this.proposalDetails.CustomerName;
          this.proposalDetails.MobileNumber = this.cs.decryptUsingTripleDES(
            res.mobile_no
          );
          this.proposalDetails.AddressLine1 = this.cs.decryptUsingTripleDES(
            res.permanent_address
          );
          this.proposalDetails.PreviousPolicyNumber = res.insurance_policy_no;
          this.proposalDetails.PreviousPolicyEndDate = moment(
            res.insurance_upto
          ).format("YYYY-MM-DD");
          this.financierDetails.BranchName = res.financer;
          this.PackgetPrevEndDate(res.insurance_upto);
          this.proposalDetails.MobileNumber = this.cs.decryptUsingTripleDES(
            res.mobile_no
          );
          this.proposalDetails.PinCode = this.cs.decryptUsingTripleDES(
            res.pin_code
          );
          this.getPincodeDetailssave(
            this.cs.decryptUsingTripleDES(res.pin_code)
          );
          /**
           * Get Previous InsurerName api change
           * Author :- Sumit
           * date :- 12-01-2022
           */
          let previousInsurer: any = this.cs.PreviousInsurerList;;
          this.insurerlist = previousInsurer;
          // let compname;
          // compname = previousInsurer.find(
          //   (c) => c.shortName == res.previous_Ins_ShortName
          // ).companyName;
          // this.proposalDetails.PreviousInsurerName = compname;

          let compname;
          let compname1;
          compname = previousInsurer.find(
            (c) => c.shortName == res.previous_Ins_ShortName
          );
          compname1 = previousInsurer.find(
            (c) => c.companyCode == res.previous_Ins_ShortName
          );
          if (this.cs.isUndefineORNull(compname)) {
            this.proposalDetails.PreviousInsurerName = compname1.companyName;
          } else {
            this.proposalDetails.PreviousInsurerName = compname.companyName;
          }

          if (
            this.proposalDetails.PreviousPolicyEndDate != "" &&
            this.proposalDetails.PreviousPolicyNumber != ""
          ) {
            this.proposalDetails.PropPreviousPolicyDetails = true;
            this.api
              .getiibServices(this.recalquotereq.RegistrationNumber)
              .subscribe((res: any) => {
                if (res.isClaimMade == "Y") {
                  this.claimNCB = false;
                  this.ClaimOnPreviousPolicy = true;
                  this.proposalDetails.claims = "claimyes";
                  this.proposalDetails.TotalNoOfODClaims = JSON.stringify(
                    res.totalClaims
                  );
                } else {
                  this.claimNCB = true;
                  this.ClaimOnPreviousPolicy = false;
                  this.proposalDetails.claims = "claimno";
                  if (this.ncbValue == 0) {
                    this.systemQC = true;
                    swal({
                      text: "Congratulations, your policy will not be triggered for QC. Proceed for payment to get your instant policy",
                      closeOnClickOutside: false,
                    });
                  }
                }
              });
          }
          // });
        } else {
          let errorbody = {
            RequestJson: this.recalquotereq.RegistrationNumber,
            ResponseJson: JSON.stringify(res),
            ServiceURL:
              "RTODetails/AadrilaGetRTODetails?registrationNumber=" +
              this.recalquotereq.RegistrationNumber,
            CorrelationID: this.quoteRes.correlationId,
          };
          this.api.adderrorlogs(errorbody);
        }
      });
  }

  // End of Code for PPD Details

  // Pre-Approved Breakin Code
  checkBreakINStatus(ev: any) {
    let policysub;
    if (this.productCode == "2312") {
      policysub = "1";
    } else if (this.productCode == "2311") {
      policysub = "2";
    }
    this.cs
      .getURL(
        "BreakIn/GetPreApprovedBreakInStatus?breakinID=" +
        this.breakintype.breakINID +
        "&correlationID=5ffc4341-1453-4212-b795-9010969d6c1c"
      )
      .then((res: any) => {
        if (res.breakInStatus != "Recommended") {
          swal({
            text: "Break-In Id not recommended. Kindly use another BreakIn",
            closeOnClickOutside: false,
          });
          this.breakintype.breakINID = "";
        } else {
          if (res.policySubType != policysub) {
            swal({
              text: "Break-In Id does not belong to this product",
              closeOnClickOutside: false,
            });
            this.breakintype.breakINID = "";
          } else {
            this.PreBreakINDataresponse = res;
            this.recalquotereq.IsSelfInspection = false;
            this.recalculateQuote();
            this.breakintype.breakin = "preApprovedBreakIN";
          }
        }
      })
      .catch((err: any) => {
        console.log(err);
      });
  }

  // End of Pre Approved Code

  // Soft Copy changes
  enterSoftCopyDetails(ev: any) {
    console.log("SOftcopy", ev.target.checked);
    this.isSoftCopySelected = ev.target.checked;
  }

  // For Insta Proposal
  isInstaProposalReq(ev: any) {
    console.log("SOftcopy", ev.target.checked);
    this.isInstaProposalRequired = ev.target.checked;
  }

  // For AML 
  isAMLReq(ev: any) {
    console.log("SOftcopy", ev.target.checked);
    this.isAMLRequired  = ev.target.checked;
  }

  createBreakInReq(data: any) {
    let body;
    let vehicleType;
    let breakinDays;
    if (
      this.policyType == "ROLL" &&
      this.quoteReq.isNoPrevInsurance == false
    ) {
      breakinDays = moment(this.quoteReq.PolicyStartDate).diff(
        moment(this.quoteReq.PreviousPolicyDetails.PreviousPolicyEndDate),
        "days"
      );
    } else {
      breakinDays = 0;
    }
    if (this.productCode == "2312") {
      vehicleType = "MOTORCYCLE";
    } else if (this.productCode == "2311") {
      vehicleType = "PRIVATE CAR";
    }
    let pType;
    if (this.policyType == "ROLL") {
      pType = "ROLLOVER";
    } else {
      pType = "NEW";
    }

    /**
     * digitalPOS change
     * get values from localStorage for checking if conditions in html file
     * date :- 29-07-2021
     */
    let subLocation = JSON.parse(localStorage.getItem('SubLocation'));
    if (this.IsPOSTransaction_flag == true) {
      this.posDealID = JSON.parse(localStorage.getItem("DealID"));
      this.deal_Id = this.posDealID;
    } else {
      this.deal_Id = this.quoteReq.DealId;
    }
    let data1 = JSON.parse(data);
    console.log(data1, "HIIIIIIIIIIIIIIIIIIIIII");
    body = {
      CorrelationId: data1.CorrelationId,
      BreakInType: "Break-in Policy lapse",
      BreakInDays: breakinDays,
      CustomerName: this.proposalDetails.CustomerName,
      CustomerAddress: this.addressLine,
      State: this.proposalDetails.stateName,
      City: this.proposalDetails.cityName,
      MobileNumber: this.proposalDetails.MobileNumber,
      TypeVehicle: vehicleType,
      VehicleMake: this.quoteRes.generalInformation.manufacturerName,
      VehicleModel: this.quoteRes.generalInformation.vehicleModel,
      ManufactureYear: String(this.quoteReq.ManufacturingYear),
      RegistrationNo: this.quoteReq.RegistrationNumber,
      EngineNo: this.proposalDetails.EngineNumber,
      ChassisNo: this.proposalDetails.ChassisNumber,
      SubLocation: subLocation,
      DistributorInterID: "",
      DistributorName: "Emmet",
      InspectionType: pType,
      DealId: this.deal_Id,
    };

    if (
      this.breakinDetails.inspectionMode == "self" ||
      this.quoteReq.IsSelfInspection == true
    ) {
      body.SelfInspection = "Yes";
    }

    let str = JSON.stringify(body);
    console.log(str);
    this.getINSTAProposalPKG(data, str)
  }

  getINSTAProposalPKG(propRQ: any, breakinRQ: any) {
    let idv :any; let maximumPrice:any; let deviationIDV:any;
    let authData = JSON.parse(localStorage.getItem("AuthToken"));
    let exShowData = JSON.parse(localStorage.getItem("exShowData"));
    let dataforBack = JSON.parse(localStorage.getItem("dataforBack"));
    let IASDataProp = JSON.parse(localStorage.getItem("IASDataProp"));
    if(!this.cs.isUndefineORNull(IASDataProp)){
      if(!this.cs.isUndefineORNull(IASDataProp.idvIasID)){
        this.isIDVDeviation = true; 
      }else{
        this.isIDVDeviation = false;
      }

      if(!this.cs.isUndefineORNull(IASDataProp.odIasID)){
        this.isODDeviation = true; 
      }else{
        this.isODDeviation = false; 
      }
    }
    console.log(this.quoteRes);
    if (this.cs.isUndefineORNull(dataforBack)) {
      if (!this.cs.isUndefineORNull(this.quoteReq.TPStartDate)) {
        this.showsaod = true;
		if(this.productCode == "2312"){
          this.isODDeviation = this.quoteRes.isQuoteDeviation;
          this.isIDVDeviation = false;
        }
      } else {
        this.showsaod = false;
      }
    } else {
      this.showsaod = dataforBack.showsaod;      
      if(this.productCode == "2312" && this.showsaod){
        this.isODDeviation = this.quoteRes.isQuoteDeviation;
        this.isIDVDeviation = false;
      }
    }
    let data = JSON.parse(propRQ);
    localStorage.setItem("PropDatareq", propRQ);
    console.log(this.odDeviationReq, this.idvDeviationReq);
    if(!this.cs.isUndefineORNull(exShowData)){
      idv = exShowData.maximumprice - exShowData.maximumprice * exShowData.idvdepreciationpercent;
      maximumPrice = (typeof (exShowData.maximumprice) == 'number') ? JSON.stringify(exShowData.maximumprice) : exShowData.maximumprice;
      deviationIDV = exShowData.minidv > idv ? "Lower Side" : "Higher Side";
    }else{
      if(!this.cs.isUndefineORNull(this.idvDeviationReq)){
      idv = this.idvDeviationReq.UI[29].Value;
      maximumPrice = this.idvDeviationReq.UI[30].Value;
      deviationIDV = this.idvDeviationReq.UI[31].Value;
      }
    }
    
    let URL: any;
    console.log(this.isIDVDeviation, this.isODDeviation)
    let body = {
      "isBreakIn": this.breakinFlag ? false : this.quoteRes.breakingFlag,
      "isOtherDiscount": this.cs.isUndefineORNull(this.isODDeviation) ? false : this.isODDeviation,
      "isIDVDeviation": this.cs.isUndefineORNull(this.isIDVDeviation) ? false : this.isIDVDeviation,
      "isInstaPolicy": this.isInstaProposalRequired,
      "isAML": this.isAMLRequired, // if IDV is >= 1 crore
      "proposalRQ": propRQ,
      "breakInRQ": (this.quoteRes.breakingFlag && !this.breakinFlag) ? breakinRQ : "",
      "corelationID": data.CorrelationId,
      "bankTpe": this.dealDetailsfromIM.bankType,
      "isSaodPolicy": this.cs.isUndefineORNull(dataforBack) ? this.showsaod : dataforBack.showsaod,
      "odIASRQ": null,
      "aMLRQ": null
    }

    let ODRQ = {
      "vertical": this.dealDetailsfromIM.primaryVerticalName,
      "creatorID": this.dealDetailsfromIM.hR_REF_NO,
      "officeName": this.dealDetailsfromIM.officeName,
      "BusinessType": this.quoteReq.BusinessType == "New Business" ? "New" : "Rollover",
      "manufacturingYear": String(this.quoteReq.ManufacturingYear),
      "rtoLocation": this.quoteRes.generalInformation.rtoLocation,
      "manufacturerName": this.quoteRes.generalInformation.manufacturerName,
      "vehicleModel": this.quoteRes.generalInformation.vehicleModel,
      "prevYearNCB": this.cs.isUndefineORNull(this.quoteReq.PreviousPolicyDetails) ? '0' : this.quoteReq.PreviousPolicyDetails.NoOfClaimsOnPreviousPolicy,
      "CurrYearNCB": (typeof (this.quoteRes.riskDetails.ncbPercentage) == 'number') ? JSON.stringify(this.quoteRes.riskDetails.ncbPercentage) : this.quoteRes.riskDetails.ncbPercentage,
      "employeeName": this.dealDetailsfromIM.employeeName,
      "customerName": this.proposalDetails.CustomerName,
      "registrationNumber": this.quoteReq.RegistrationNumber,
      "vehicleType": (this.productCode == "2312" || this.productCode == "2320") ? 'MOTORCYCLE' : 'PRIVATE CAR',
      "intermediaryName": this.dealDetailsfromIM.intermediaryName,
      "imID": authData.username,
      "intermediaryCode": this.dealDetailsfromIM.intermediaryCode,
      "productName": (this.productCode == "2312" || this.productCode == "2320") ? 'TW' : 'Pvt Car',
      "maximumPrice": maximumPrice,
      "systemIDV": (typeof (idv) == 'number') ? JSON.stringify(idv) : idv,
      "requireIDV": (typeof (idv) == 'number') ? JSON.stringify(idv) : idv,
      "deviationIDV": deviationIDV,
      "totalIDV": "0.3"
    }
    if (this.isODDeviation || this.isIDVDeviation) {
      body.odIASRQ = ODRQ;
    } else {
      body.odIASRQ = null;
    }

    let AMLRQ=
    {
      "isSalaried": this.isSalaried, // if salaried is checked
      "isSelfEmplyed": this.isSelfEmployed,// if self-employed is checked
      "annualIncome": this.annualIncome,
      "employeeName": this.proposalDetails.employeeName,
      "officeAddress":   this.proposalDetails.officeAddress,
      "businessType": this.businessType,
      "registrationType": this.registrationType 
    }

    if (this.isAMLRequired)
    {
      body.aMLRQ = AMLRQ; 
    } else {
      body.aMLRQ = null;
    }

    if (this.productCode == "2312") {
      URL = 'Proposal/TwCreateProposal';
    } else if (this.productCode == "2311") {
      URL = 'Proposal/FwCreateProposal';
    }
    this.instaProposalBody = body;
    let str = JSON.stringify(body);
    this.cs.post(URL, str).then((res: any) => {
      console.log(res);
      let response = !this.cs.isUndefineORNull(res.fWProposalRS) ? res.fWProposalRS : res.tWProposalRS;
      this.calculatedData = response;
      let isSCPAApply = JSON.parse(localStorage.getItem('isSCPARequired'));
      if (isSCPAApply && this.showSCPAinNysa) {
        this.proposalForSCPA();
      } else {
  
      }

      if (res.statusResponse.statusCode == 1) {
        if (res.isInstaPolicySuccess && (!body.isBreakIn || !body.isOtherDiscount || !body.isIDVDeviation)) {
          this.calculatedData = response;
          localStorage.setItem("PropDatares", JSON.stringify(response));
          this.finalamount = Math.round(this.calculatedData.finalPremium);
          this.instaPolicyNumber = !this.cs.isUndefineORNull(this.calculatedData.policyNo) ? this.calculatedData.policyNo : '';
          this.showInstaPolicyUI = true;
        } else if (!res.isInstaPolicySuccess && (body.isBreakIn || body.isOtherDiscount || body.isIDVDeviation)) {
          this.calculatedData = response;
          localStorage.setItem("PropDatares", JSON.stringify(response));
          this.showInstaPolicyUI = false;
          if ((body.isBreakIn && body.isOtherDiscount) || (body.isBreakIn && body.isIDVDeviation)) {
            swal({
              text: "Your proposal number generated is - " + this.calculatedData.generalInformation.proposalNumber + " . But we can't proceed because case would go into Break In with IAS. So Kindly check in Break In and IAS tab.",
            });
          } else if (body.isBreakIn) {
            swal({
              text: "Your proposal number generated is - " + this.calculatedData.generalInformation.proposalNumber + " . But we can't proceed because case would go into Break In. So Kindly check in Break In tab.",
            });
          } else {
            swal({
              text: "Your proposal number generated is - " + this.calculatedData.generalInformation.proposalNumber + " . But we can't proceed because case would go into IAS. So Kindly check in IAS tab.",
            });
          }
        } else {
          this.calculatedData = response;
          localStorage.setItem("PropDatares", JSON.stringify(response));
          this.finalamount = Math.round(this.calculatedData.finalPremium);
          this.instaPolicyNumber = !this.cs.isUndefineORNull(this.calculatedData.policyNo) ? this.calculatedData.policyNo : '';
          this.showInstaPolicyUI = false;
          if (this.isInstaProposalRequired) {
            swal({
              text: "Since your banking id is locked, we couldn't generate your insta policy. Proposal number generated is - " + this.calculatedData.generalInformation.proposalNumber + " . You can proceed with online payment.",
            });
          }
        }
      } else {
        this.calculatedData = response;
        localStorage.setItem("PropDatares", JSON.stringify(response));
        this.showInstaPolicyUI = false;
        swal({
          text: res.statusResponse.statusDesc,
        });
      }
      this.cs.loaderStatus = false;
    }).catch((err: any) => {
      console.log(err);
      swal({
        text: err.message,
      });
      this.cs.loaderStatus = false;
    })
  }

  getINSTAProposalTP(propRQ: any) {
    let bankType = localStorage.getItem("bankType");
    let URL: any;
    let data = JSON.parse(propRQ);
    localStorage.setItem("PropDatareq", propRQ);
    let body = {
      "corelationID": data.CorrelationId,
      "proposalRQ": propRQ,
      "isInstaPolicy": this.isInstaProposalRequired,
      "isAML": this.isAMLRequired, // if IDV is >= 1 crore
      "bankType": bankType,
      "isBreakIn": false,
      "isOtherDiscount": false,
      "isIDVDeviation": false,
      "aMLRQ": null
    }

    if (this.productCode == "2319") {
      URL = 'Proposal/FwTPCreateProposal';
    } else if (this.productCode == "2320") {
      URL = 'Proposal/TwTPCreateProposal';
    }

    let AMLRQ=
    {
      "isSalaried": this.isSalaried, // if salaried is checked
      "isSelfEmplyed": this.isSelfEmployed,// if self-employed is checked
      "annualIncome": this.annualIncome,
      "employeeName": this.proposalDetails.employeeName,
      "officeAddress":   this.proposalDetails.officeAddress,
      "businessType": this.businessType,
      "registrationType": this.registrationType 
    }

    if (this.isAMLRequired)
    {
      body.aMLRQ = AMLRQ; 
    } else {
      body.aMLRQ = null;
    }
    
    this.instaProposalBody = body;
    let str = JSON.stringify(body);

    this.cs.post(URL, str).then((res: any) => {
      console.log(res.proposalRS);
      if (res.statusResponse.statusCode == 1) {
        if (res.isInstaPolicySuccess) {
          this.calculatedData = res.proposalRS;
          localStorage.setItem("PropDatares", JSON.stringify(res.proposalRS));
          this.finalamount = Math.round(this.calculatedData.finalPremium);
          this.instaPolicyNumber = !this.cs.isUndefineORNull(this.calculatedData.policyNo) ? this.calculatedData.policyNo : '';
          this.showInstaPolicyUI = true;
        } else {
          this.calculatedData = res.proposalRS;
          localStorage.setItem("PropDatares", JSON.stringify(res.proposalRS));
          this.finalamount = Math.round(this.calculatedData.finalPremium);
          this.instaPolicyNumber = !this.cs.isUndefineORNull(this.calculatedData.policyNo) ? this.calculatedData.policyNo : '';
          this.showInstaPolicyUI = false;
          if (this.isInstaProposalRequired) {
            swal({
              text: "Since your banking id is locked, we couldn't generate your insta policy. Proposal number generated is - " + this.calculatedData.generalInformation.proposalNumber + " . You can proceed with online payment.",
            });
          }
        }
      } else {
        this.calculatedData = res.proposalRS;
        localStorage.setItem("PropDatares", JSON.stringify(res.proposalRS));
        this.showInstaPolicyUI = false;
        swal({
          text: res.statusResponse.statusDesc,
        });
      }
      this.cs.loaderStatus = false;
    }).catch((err: any) => {
      console.log(err);
      swal({
        text: err.message,
      });
      this.cs.loaderStatus = false;
    })
  }

  goToMyNysaPolicy() {
    // this.router.navigateByUrl("quote");
    this.router
      .navigateByUrl("/", { skipLocationChange: true })
      .then(() => this.router.navigate(["/quote"]));
    this.cs.clearLocalStorage();
    this.cs.clearQuoteDate();
    this.cs.clearPropData();
    localStorage.setItem("myNysaPolicy", "true");
  }

  proposalForSCPA() {
    let vehicleType: any;
    let scpaData = JSON.parse(localStorage.getItem('scpaData'));
    if (this.productCode == "2312") {
      vehicleType = "Motorized Two Wheeler (Private and Commercial)";
    } else if (this.productCode == "2311") {
      vehicleType = "PRIVATE CAR";
    }
    let quoteReq = JSON.parse(localStorage.getItem("reqSAODSCPA"));
    let quoteRes = JSON.parse(localStorage.getItem("resSAODSCPA"));
    let bundleIDSAOD = JSON.parse(localStorage.getItem("bundleIDSAOD"));
    let startDate = moment(new Date()).format('YYYY-MM-DD');
    let endDate = moment(new Date())
      .add(1, "years")
      .subtract(1, "days")
      .format("YYYY-MM-DD");

    let body = {
      "ModeOfOperation": null,
      "BusinessType": null,
      "PolicyTenure": "1",
      "PolicyStartDate": startDate,
      "PolicyEndDate": endDate,
      "ILProductCode": "3012",
      "SubProductCode": commonData.scpaProductCode,
      "FinancierDetails_PrefixSufix": null,
      "FinancierDetails_Remarks": null,
      "CoinsuranceType": null,
      "SpecialConditionDescription": null,
      "DealId": scpaData.scpaDeal,//"DL-3012/1488226",
      "StampDutyApplicability": false,
      "Commission": 0.0,
      "ReferenceNumber": null,
      "PolicyNumberChar": null,
      "InteractionID": null,
      "EffectiveDate": null,
      "EffectiveTime": null,
      "EndorsementWording": null,
      "NILEndorsementTypeCode": 0.0,
      "NILEndorsementType": null,
      "EndorsementTypeCode": 0.0,
      "TypeofEndorsement": null,
      "TypeOfCalculation": null,
      "RetainCancellationPremium": null,
      "AlternatePolicyNo": null,
      "BaseProductProposalNo": null,
      "FromHour": null,
      "ToHour": null,
      "FieldsGrid": [
        {
          "FieldName": "RegistrationNo",
          "FieldValue": this.quoteReq.RegistrationNumber
        },
        {
          "FieldName": "NoofVehiclesownedbyInsured",
          "FieldValue": "0"
        },
        {
          "FieldName": "TypeofVehicle",
          "FieldValue": vehicleType
        },
        {
          "FieldName": "MakeofVehicle",
          "FieldValue": this.quoteRes.generalInformation.manufacturerName
        },
        {
          "FieldName": "Model",
          "FieldValue": this.quoteRes.generalInformation.vehicleModel
        },
        {
          "FieldName": "EngineNo",
          "FieldValue": this.proposalDetails.EngineNumber
        },
        {
          "FieldName": "ChassisNo",
          "FieldValue": this.proposalDetails.ChassisNumber
        },
        {
          "FieldName": "NomineeRequired",
          "FieldValue": JSON.stringify(this.addNomineeVehicle)
        },
        {
          "FieldName": "InsuredName",
          "FieldValue": this.proposalDetails.NameOfNominee
        },
        {
          "FieldName": "InsuredGender",
          "FieldValue": "F"
        },
        {
          "FieldName": "InsuredRelation",
          "FieldValue": this.proposalDetails.Relationship
        }
      ],
      "riskDetails": {
        "PlanCode": commonData.scpaPlancode,
        "Product24062544": null,
        "UniqueInwardNumber": null,
        "InsuredDateOfBirth": "2000-02-02T00:00:00",
        "InwardDate": "2022-05-18T00:00:00",
        "MasterPolicyNumber": null,
        "PreExistingIllness": null,
        "NomineeName": null,
        "NomineeRelationship": null,
        "NomineeDOB": null,
        "AppointeeName": null,
        "AppointeeRelationship": null,
        "AppointeeDOB": null,
        "RisksList": [
          {
            "RiskSIComponent": "Person",
            "BasisOfSumInsured": "Reinstatement Value",
            "Rate": 0.0,
            "SumInsured": 0.0,
            "Premium": 0.0,
            "DifferentialSI": 0.0,
            "EndorsementAmount": 0.0,
            "IsOptionalCover": false,
            "coverDetailsList": [
              {
                "CoverDescription": "Compulsory Personal Accident Owner Driver Cover",
                "SumInsured": 1500000.0,
                "column1": "",
                "column2": "",
                "column3": null,
                "column4": null,
                "column5": null,
                "Applicability80D": null,
                "Rate": 0.0,
                "Premium": 0.0,
                "DifferentialSI": 0.0,
                "EndorsementAmount": 0.0,
                "IsDataDeleted": false
              }
            ]
          }
        ]
      },
      "CustomerDetails": {
        "CustomerType": this.quoteReq.CustomerType ? this.quoteReq.CustomerType : this.quoteReq.customerType,
        "CustomerName": this.proposalDetails.CustomerName,
        "DateOfBirth": "2000-04-19T00:00:00",
        "PinCode": this.proposalDetails.PinCode,
        "PANCardNo": this.proposalDetails.PANCardNo,
        "Email": this.proposalDetails.Email,
        "MobileNumber": this.proposalDetails.MobileNumber,
        "AddressLine1": this.addressLine,
        "CountryCode": this.CountryCode,
        "StateCode": this.StateCode,
        "CityCode": this.CityCode,
        "Gender": this.gender,
        "MobileISD": "91",
        "GSTDetails": null,
        "AadharNumber": null,
        "IsCollectionofform60": false,
        "AadharEnrollmentNo": null,
        "eIA_Number": null,
        "CorelationId": null,
        "CustomerID": null
      },
      "insuredDataDetails": null,
      "warranty": null,
      "exclusionDetails": null,
      "conditionDetails": null,
      "clauseDetails": null,
      "coinsuranceDetails": null,
      "financierDetailse": null,
      "loadingDiscount": null,
      "installmentUserData": null,
      "SpDetails": null,
      "CorrelationId": quoteReq.CorrelationId ? quoteReq.CorrelationId : quoteReq.correlationId
    }

    let str = JSON.stringify(body);
    this.cs.loaderStatus = true;
    localStorage.setItem('reqPropSAODSCPA', str);
    this.cs.postSCPA('/Proposal/Create', str).then((res: any) => {
      localStorage.setItem('resPropSAODSCPA', JSON.stringify(res));
      console.log("Response", res, this.calculatedData.finalPremium, parseInt(res.gctotalpremium));
      if(res.status == 'SUCCESS'){
        this.scpaResponse = this.SCPARes = res;
        this.finalamount = this.calculatedData.finalPremium + parseInt(res.gctotalpremium);
        console.log(this.finalamount);
        this.saveofflineSAODProposal();
      }else{
        this.finalamount = Math.round(this.calculatedData.finalPremium)
      }
      this.cs.loaderStatus = false;
    }).catch((err:any) => {
      this.cs.loaderStatus = false;
      this.finalamount = Math.round(this.calculatedData.finalPremium)
    })
  }

  saveofflineSAODProposal(): Promise<any> {
    return new Promise((resolve: any) => {
      let body, URL;
      let quoteReq = JSON.parse(localStorage.getItem("reqPropSAODSCPA"));
      let quoteRes = JSON.parse(localStorage.getItem("resPropSAODSCPA"));
      let pType = JSON.parse(localStorage.getItem("breakinFlag"));
      let savequote = JSON.parse(localStorage.getItem("savedQuotes"));
      if (savequote == true) {
        this.productCode = this.propReq.productCode;
      }

      let bankType = localStorage.getItem("bankType");
      console.log("IAS", this.iasIntRes);
      body = {
        proposalRQ: quoteReq,
        proposalRS: quoteRes,
        bankType: bankType,
        IasRQ: {
          OdIasID: "",
          OdIasSrt: "",
          OdIasSubSrt: "",
          IdvIasID: this.iasIDVRes ? this.iasIDVRes.srRequestNo : "",
          IdvIasSrt: "",
          IdvIasSubSrt: "",
          OdIasRQ: "",
          OdIasRS: "",
          IdvIasRQ: "",
          IdvIasRS: "",
        },
      };

      if (this.quoteReq.Tenure == "1" && this.quoteReq.TPTenure == "0") {
        body.IsStandaloneOD = true;
        let standaloneODRQ = {
          TPStartDate: this.quoteReq.TPStartDate,
          TPEndDate: this.quoteReq.TPEndDate,
          TPPolicyNo: this.quoteReq.TPPolicyNo,
          TPInsurerName: this.quoteReq.TPInsurerName,
        };
        body.standaloneODRQ = standaloneODRQ;
      }

      let savequoteData = JSON.parse(localStorage.getItem("saveQuoteData"));
      if (savequote == true) {
        if (savequoteData.isSubAgent == true) {
          body.IsSubagent = savequoteData.isSubAgent;
          body.SubagentIpartnerUserID = savequoteData.subagentIpartnerUserID;
        } else {
          body.IsSubagent = savequoteData.isSubAgent;
        }
      } else {
        if (
          this.paramDataValues.iPartnerLogin.isSubagent != null ||
          this.paramDataValues.iPartnerLogin.isSubagent != undefined
        ) {
          body.IsSubagent = true;
          body.SubagentIpartnerUserID =
            this.paramDataValues.iPartnerLogin.subAID;
        }
      }

      // if (this.productCode == "2312") {
      URL = "Proposal/twev/SaveProposal";
      // }
      // if (this.productCode == "2311") {
      //   URL = "Proposal/fw/SaveProposal";
      // }
      let str = JSON.stringify(body);
      this.cs.loaderStatus = true;
      this.cs
        .post(URL, str)
        .then((res: any) => {
          this.getMultiProposalData(this.calculatedData.generalInformation.proposalNumber, this.calculatedData.generalInformation.customerId, this.scpaResponse.proposalNumber, this.scpaResponse.customerId, this.calculatedData.finalPremium, this.scpaResponse.gctotalpremium, this.finalamount);
          if (res.status == "Success") {
            this.saveProposalStatus = res.status;
          } else {
            // let errorbody = {
            //   RequestJson: JSON.stringify(body),
            //   ResponseJson: JSON.stringify(res),
            //   ServiceURL: URL,
            //   CorrelationID: this.quoteRes.correlationId,
            // };
            // this.api.adderrorlogs(errorbody);
          }
          this.cs.loaderStatus = false;
          resolve();
        })
        .catch((err: any) => {
          let errMsg = this.msgService.errorMsg(err)
          swal({ closeOnClickOutside: false, text: errMsg });
          this.cs.loaderStatus = false;
        });
    });
  }


  // All Risk Electric Bike - Calculate Proposal - Sejal - 13-03-2022
  calculateAllRiskEBProposal(): Promise<any> {
    return new Promise((resolve: any) => {
      let body: any;
      console.info(this.gender);

      this.deal_Id = this.EVQuoteReq.DealId;
      console.info("EV QUOTE RESP", this.EVQuoteRes);

      if (!this.cs.isUndefineORNull(localStorage.getItem("savedQuotes"))) {
        let savequote = JSON.parse(localStorage.getItem("savedQuotes"));
        let oldreference = JSON.parse(localStorage.getItem("oldreference"));
        let today = moment(new Date()).format("YYYY-MM-DD");
        if (this.EVQuoteReq.PolicyStartDate != today) {
          this.PolicyStartDate = today;

          this.policyEndDate = moment(this.PolicyStartDate)
            .add(1, "years")
            .subtract(1, "days")
            .format("YYYY-MM-DD");
        } else {
          this.PolicyStartDate = this.EVQuoteReq.PolicyStartDate;
          this.policyEndDate = this.EVQuoteReq.PolicyEndDate;
        }
      } else {
        this.PolicyStartDate = this.EVQuoteReq.PolicyStartDate;
        this.policyEndDate = this.EVQuoteReq.PolicyEndDate;
      }


      body = {
        ModeOfOperation: "NEWPOLICY",
        //BusinessType: "New Business",
        ReferenceNumber: null,
        PolicyTenure: "1", //parseInt(this.EVQuoteReq.Tenure),				//"1"
        PolicyStartDate: this.PolicyStartDate, //this.EVQuoteReq.PolicyStartDate,				//"2022-02-08T00:00:00"
        PolicyEndDate: this.policyEndDate, //this.EVQuoteReq.PolicyEndDate,				//"2023-02-07T00:00:00"
        ILProductCode: "4001/E",
        SubProductCode: this.productCode,
        FinancierDetails_PrefixSufix: null,
        FinancierDetails_Remarks: null,
        CoinsuranceType: null,
        SpecialConditionDescription: null,
        DealId: this.deal_Id,								//"DL-4001/E/1365912"
        StampDutyApplicability: false,
        Commission: 0.0,
        PolicyNumberChar: null,
        InteractionID: null,
        EffectiveDate: null,
        EffectiveTime: null,
        EndorsementWording: null,
        NILEndorsementTypeCode: 0.0,
        NILEndorsementType: null,
        EndorsementTypeCode: 0.0,
        TypeofEndorsement: null,
        TypeOfCalculation: null,
        RetainCancellationPremium: null,
        AlternatePolicyNo: "",
        BaseProductProposalNo: null,
        FieldsGrid: [       //mandatory fields
          {
            FieldName: "Make ",
            FieldValue: this.proposalDetails.Manufacturer		//"AMO MOBILITY SOLUTIONS PRIVATE LIMITED
          },
          {
            FieldName: "Model",
            FieldValue: this.proposalDetails.model				//"Inspirer"
          },
          {
            FieldName: "MotorNumberorBatteryNumber",
            FieldValue: this.proposalDetails.batterysaver				//"23423423"
          },
          {
            FieldName: "ChassisNumber",
            FieldValue: this.proposalDetails.ChassisNumber			//"CN324423"
          },
          {
            FieldName: "PurchaseorInvoiceDate",
            FieldValue: this.EVQuoteReq.PurchaseDate					//"07/02/2022"
          },
          {
            FieldName: "KiloWatt",
            FieldValue: this.proposalDetails.kilowatt				//"110"
          },
          {
            FieldName: "Invoicenumber",
            FieldValue: this.EVQuoteReq.InvoiceNo				//"123235345"
          },
          {
            FieldName: "Yearofmanufacture",
            FieldValue: this.EVQuoteReq.ManufacturingYear			//"2022"
          },
          {
            FieldName: "GSTINInvoiceDate",
            FieldValue: moment(new Date()).format("YYYY-MM-DD"), //"05/02/2022"
          }
        ],
        riskDetails: {
          PlanCode: this.EVQuoteReq.ProductDetails.PlanDetails[0].PlanCode,
          InsuredDateOfBirth: "1990-12-24",
          InwardDate: moment(new Date()).format("YYYY-MM-DD"), //this.EVQuoteReq.PolicyStartDate,         //"2022-02-08",
          MasterPolicyNumber: null,
          PreExistingIllness: null,
          // NomineeName: this.proposalDetails.NameOfNominee,					//"Nominee"
          // NomineeRelationship: this.proposalDetails.Relationship,				//"MOTHER"
          // NomineeDOB: "8/9/1987 12:00:00 AM",
          AppointeeName: null,
          AppointeeRelationship: null,
          AppointeeDOB: null,
          RisksList: [
            {
              RiskSIComponent: this.proposalDetails.RiskSIComponent,
              BasisOfSumInsured: "Agreed Value",
              // coverDetailsList: [
              //     {
              //         CoverDescription: this.EVQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].coverName,
              //         SumInsured: this.EVQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].coverSI,
              //         column1: this.EVQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column1,
              //         column2: this.EVQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column2,
              //         column3: this.EVQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column3,
              //         column4: this.EVQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column4,
              //         column5: this.EVQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column5
              //         // CoverDescription: this.EVQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].CoverName,
              //         // SumInsured: this.EVQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].CoverSI,
              //         // column1: this.EVQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].Column1,
              //         // column2: this.EVQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].Column2,
              //         // column3: this.EVQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].Column3,
              //         // column4: this.EVQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].Column4,
              //         // column5: this.EVQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].Column5
              //     }
              // ]
            }
          ]
        },
        CustomerDetails: {
          CustomerType: this.EVQuoteReq.CustomerType,					//"Individual"
          CustomerName: this.proposalDetails.CustomerName,				//"Testuser"
          DateOfBirth: Date.parse(moment(this.proposalDetails.DateOfBirth).format('YYYY-MM-DD')),	//"1987-08-09T00:00:00"
          PinCode: this.proposalDetails.PinCode,						//"400034"
          PANCardNo: null,                                  //this.proposalDetails.PANCardNo,
          Email: this.proposalDetails.Email,						//"test@gmail.com"
          MobileNumber: this.proposalDetails.MobileNumber,				//"9999999999"
          AddressLine1: this.addressLine,							//"gjgkdjhvkf"
          CountryCode: this.CountryCode,							//100
          StateCode: this.StateCode,								//55
          CityCode: this.CityCode,								//147
          Gender: this.gender,									//"Male"
          MobileISD: "91",
          // GSTDetails: {
          //     GSTExemptionApplicable: "No",
          //     //GSTInNumber: "11AAAAA1111A1Z1",  //this.proposalDetails.GSTInNumber,
          //     GSTToState: this.proposalDetails.stateName,					//"MAHARASHTRA"
          //     ConstitutionOfBusiness: this.constName,					//"NON RESIDENT ENTITY"
          //     CustomerType: this.custType,							//"GENERAL"
          //     PanDetails: this.proposalDetails.PANCardNo,				//"AAAAA1111A"
          //     GSTRegistrationStatus: this.gstName,					//"ARN GENERATED"
          //     GSTToStateCode: 0
          // },
          AadharNumber: "",
          IsCollectionofform60: false,
          AadharEnrollmentNo: null,
          eIA_Number: null,
          //CorelationId: this.EVQuoteRes.CorrelationId,					//"{{$guid}}"
          CorrelationId: this.EVQuoteRes.correlationId,
          // CustomerID: "100984017905"
        },
        warranty: null,
        exclusionDetails: null,
        conditionDetails: null,
        clauseDetails: null,
        coinsuranceDetails: null,
        //financierDetailse: null,
        loadingDiscount: null,
        installmentUserData: null,
        //SpDetails: null,
        //_CorrelationId: "{{$guid}}",
        CorrelationId: this.EVQuoteRes.correlationId,     //"1c3c7bd5-5b61-4dbc-9188-aa52ad350ab8"
      }

      if (this.EVQuoteReq.TransactionType == "New") {
        body.BusinessType = "New Business";
      } else {
        body.BusinessType = "Roll Over";
      }

      let coverDetailsList = [];
      if (this.EVQuoteRes.ProductDetails || this.EVQuoteRes.CorrelationId) {
        coverDetailsList = [
          {
            "CoverDescription": this.EVQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].CoverName,
            "SumInsured": this.EVQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].CoverSI,
            "column1": this.EVQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].Column1,
            "column2": this.EVQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].Column2,
            "column3": this.EVQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].Column3,
            "column4": this.EVQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].Column4,
            "column5": this.EVQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].Column5
          }
        ]

        body.riskDetails.RisksList[0].coverDetailsList = coverDetailsList;
        body.CorrelationId = this.EVQuoteRes.CorrelationId;
      } else {
        coverDetailsList = [
          {
            CoverDescription: this.EVQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].coverName,
            SumInsured: this.EVQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].coverSI,
            column1: this.EVQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column1,
            column2: this.EVQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column2,
            column3: this.EVQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column3,
            column4: this.EVQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column4,
            column5: this.EVQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column5
          }
        ]

        body.riskDetails.RisksList[0].coverDetailsList = coverDetailsList;
        body.CorrelationId = this.EVQuoteRes.correlationId;
      }

      //let NomineeDetails;
      let currentDate, birthDate, today, currentYear, birthYear, currDay, currMonth;
      if (
        this.addNomineeVehicle == true &&
        this.proposalDetails.NameOfNominee != ""
      ) {
        // NomineeDetails = {
        //   NomineeType: "PA-Owner Driver",
        //   NameOfNominee: this.proposalDetails.NameOfNominee,
        //   Age: this.proposalDetails.Age,
        //   Relationship: this.proposalDetails.Relationship,
        // };
        today = new Date();
        currentDate = moment(today).format("DD-MM-YYYY");
        currDay = moment(today).format("DD");
        currMonth = moment(today).format("MM");
        currentYear = moment(today).format("YYYY");
        birthYear = currentYear - parseInt(this.proposalDetails.Age);
        let date = new Date(birthYear, currMonth, currDay);
        birthDate = moment(date);
	  let dob = birthDate.format("YYYY-MM-DD");
        //console.info("Birth Date",birthDate);

        body.riskDetails.NomineeName = this.proposalDetails.NameOfNominee;
        body.riskDetails.NomineeRelationship = this.proposalDetails.Relationship;
        //body.riskDetails.Age = this.proposalDetails.Age;
        body.riskDetails.NomineeDOB = dob;

      }
      let GSTDetails;
      if (this.GSTDetails == true && this.proposalDetails.GSTInNumber != "") {
        GSTDetails = {
          GSTExemptionApplicable: "No",
          GSTInNumber: this.proposalDetails.GSTInNumber, // GSTIN/ UIN Number   // text box
          GSTToState: this.proposalDetails.stateName, // no need to show on screen
          ConstitutionOfBusiness: this.constName, // Contrubution of business  // drop down
          CustomerType: this.custType, // Customer Type   // dropdown
          PanDetails: this.proposalDetails.PANCardNo, // Pan Details // text box
          GSTRegistrationStatus: this.gstName, // GST Registration status // Dropdown
          GSTToStateCode: 0
        };
      }
      body.CustomerDetails.GSTDetails = GSTDetails;
      // body.NomineeDetails = NomineeDetails;

      if (this.financierDetailsVal == true) {
        body.financierDetails = this.financierDetails;
      } else {
        body.financierDetails = null;
      }


      let str = JSON.stringify(body);
      // this.cs.loaderStatus = true;

      // localStorage.setItem('scpaPropDatareq')
      // localStorage.setItem('scpaPropDatares')

      this.isSCPA = JSON.parse(localStorage.getItem("IsSCPA"));

      this.createAllRiskBizToken().then(() => {
        this.cs.loaderStatus = true;
        //this.spinloader = true; 
        this.cs.allRiskEBpostBiz("/zerotat/Proposal/Create", str).subscribe(
          (res: any) => {
            if (res.statusMessage == "Success") {
              this.cs.loaderStatus = false;
              if (this.isSCPA == true) {
                this.calculateSCPAProposal();
              } else {
                this.scpafinalamount = 0;
              }
              localStorage.setItem("EVPropDatareq", JSON.stringify(body));
              localStorage.setItem("EVPropDatares", JSON.stringify(res));
              this.calculatedData = res;
              this.evfinalamount = Math.round(this.calculatedData.gstserviceresponse.gstservicedetails.finalamount);
              console.info("EV AMT", this.evfinalamount);
              if (this.evfinalamount && this.scpafinalamount) {
                this.finalamount = Math.round(this.evfinalamount + this.scpafinalamount);
              } else {
                this.finalamount = Math.round(this.evfinalamount);
              }
              this.evProposalNo = this.calculatedData.proposalNumber;
              this.paymentFlag = true;
              this.customerID = res.customerId;
              console.info("EV Proposal", this.evProposalNo);
              this.allriskSaveOfflineProposal().then(() => {
                if (this.saveProposalStatus == "Success") {
                  this.isAllRiskCreated = true;
                  this.showPaymentFlag = true;
                  let scrollingElement = document.scrollingElement || document.body;
                  $(scrollingElement).animate(
                    {
                      scrollTop: document.body.scrollHeight,
                    },
                    500
                  );
                } else {
                  this.isAllRiskCreated = false;
                  swal({
                    text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
                  });
                }
              });
              resolve();
            } else {
              this.isAllRiskCreated = false;
              this.showPaymentFlag = false;
              this.cs.loaderStatus = false;
              let errormsg = res.message;
              this.api.handleerrors(errormsg);
              let errorbody = {
                RequestJson: JSON.stringify(body),
                ResponseJson: JSON.stringify(res),
                ServiceURL:
                  commonData.bizURL1 + "/zerotat/Proposal/Create",
                CorrelationID: this.EVQuoteRes.correlationId,
              };
              this.api.adderrorlogs(errorbody);
              this.calculatedData = null;
              resolve();
            }
          },
          (err) => {
            swal({ closeOnClickOutside: false, text: err });
            // swal({ closeOnClickOutside: false, text: 'At present system response is slow, kindly try after sometime. We regret the inconvenience' })
            resolve();
          });
      });
    });
  }

  allriskSaveOfflineProposal(): Promise<any> {
    return new Promise((resolve: any) => {
      let body: any;
      let propReq1 = JSON.parse(localStorage.getItem("EVPropDatareq"));
      let propRes1 = JSON.parse(localStorage.getItem("EVPropDatares"));
      let bankType = localStorage.getItem("bankType");

      body = {
        // BundleID: this.bundleID,
        proposalRQ: propReq1,
        proposalRS: propRes1,
        bankType: bankType
      };
      let str = JSON.stringify(body);
      this.cs.post("Proposal/twev/SaveProposal", str).then((res: any) => {
        if (res.status == "Success") {
          this.saveProposalStatus = res.status;
        } else {
          let errorbody = {
            RequestJson: JSON.stringify(body),
            ResponseJson: JSON.stringify(res),
            ServiceURL: URL,
            CorrelationID: this.EVQuoteRes.correlationId,
          };
          this.api.adderrorlogs(errorbody);
        }

        this.cs.loaderStatus = false;
        resolve();
      })
        .catch((err: any) => {
          swal({ text: err });
          this.cs.loaderStatus = false;
        });
    });
  }

  calculateSCPAProposal() {

    this.scpaQuoteReq = JSON.parse(localStorage.getItem("scpaQuoteReq"));
    this.scpaQuoteRes = JSON.parse(localStorage.getItem("scpaQuoteRes"));

    let body: any;
    this.deal_Id = this.scpaQuoteReq.DealId;

    if (!this.cs.isUndefineORNull(localStorage.getItem("savedQuotes"))) {
      // let savequote = JSON.parse(localStorage.getItem("savedQuotes"));
      // let oldreference = JSON.parse(localStorage.getItem("oldreference"));
      body = JSON.parse(localStorage.getItem("scpaPropDatareq"));
      let today = moment(new Date()).format("YYYY-MM-DD");
      if (this.EVQuoteReq.PolicyStartDate != today) {
        this.PolicyStartDate = today;

        this.policyEndDate = moment(this.PolicyStartDate)
          .add(1, "years")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      } else {
        this.PolicyStartDate = this.EVQuoteReq.PolicyStartDate;
        this.policyEndDate = this.EVQuoteReq.PolicyEndDate;
      }
    } else {
      this.PolicyStartDate = this.EVQuoteReq.PolicyStartDate;
      this.policyEndDate = this.EVQuoteReq.PolicyEndDate;
    }

    body = {
      ModeOfOperation: "NEWPOLICY",
      //BusinessType: "New Business",
      ReferenceNumber: null,
      PolicyTenure: "1", //parseInt(this.EVQuoteReq.Tenure),				//"1"
      PolicyStartDate: this.scpaQuoteReq.PolicyStartDate,				//"2022-02-08T00:00:00"
      PolicyEndDate: this.scpaQuoteReq.PolicyEndDate,				//"2023-02-07T00:00:00"
      ILProductCode: "4001/E",
      SubProductCode: commonData.scpaProductCode,        //this.productCode,
      FinancierDetails_PrefixSufix: null,
      FinancierDetails_Remarks: null,
      CoinsuranceType: null,
      SpecialConditionDescription: null,
      DealId: this.deal_Id,								//"DL-4001/E/1491882"
      StampDutyApplicability: false,
      Commission: 0.0,
      PolicyNumberChar: null,
      InteractionID: null,
      EffectiveDate: null,
      EffectiveTime: null,
      EndorsementWording: null,
      NILEndorsementTypeCode: 0.0,
      NILEndorsementType: null,
      EndorsementTypeCode: 0.0,
      TypeofEndorsement: null,
      TypeOfCalculation: null,
      RetainCancellationPremium: null,
      AlternatePolicyNo: "",
      BaseProductProposalNo: null,
      FieldsGrid: [       //mandatory fields
        {
          FieldName: "Make ",
          FieldValue: this.proposalDetails.Manufacturer		//"AMO MOBILITY SOLUTIONS PRIVATE LIMITED
        },
        {
          FieldName: "Model",
          FieldValue: this.proposalDetails.model				//"Inspirer"
        },
        {
          FieldName: "MotorNumberorBatteryNumber",
          FieldValue: this.proposalDetails.batterysaver				//"23423423"
        },
        {
          FieldName: "ChassisNumber",
          FieldValue: this.proposalDetails.ChassisNumber			//"CN324423"
        },
        {
          FieldName: "PurchaseorInvoiceDate",
          FieldValue: this.EVQuoteReq.PurchaseDate					//"07/02/2022"
        },
        {
          FieldName: "KiloWatt",
          FieldValue: this.proposalDetails.kilowatt				//"110"
        },
        {
          FieldName: "Invoicenumber",
          FieldValue: this.EVQuoteReq.InvoiceNo				//"123235345"
        },
        {
          FieldName: "Yearofmanufacture",
          FieldValue: this.EVQuoteReq.ManufacturingYear			//"2022"
        },
        {
          FieldName: "GSTINInvoiceDate",
          FieldValue: moment(new Date()).format("YYYY-MM-DD"), //"05/02/2022"
        }
      ],
      riskDetails: {
        PlanCode: this.scpaQuoteReq.ProductDetails.PlanDetails[0].PlanCode,   //"10619"
        InsuredDateOfBirth: "1990-12-24",
        InwardDate: moment(new Date()).format("YYYY-MM-DD"), //"2022-02-08",
        MasterPolicyNumber: null,
        PreExistingIllness: null,
        // NomineeName: this.proposalDetails.NameOfNominee,					//"Nominee"
        // NomineeRelationship: this.proposalDetails.Relationship,				//"MOTHER"
        // NomineeDOB: "8/9/1987 12:00:00 AM",
        AppointeeName: null,
        AppointeeRelationship: null,
        AppointeeDOB: null,
        RisksList: [
          {
            RiskSIComponent: this.scpaQuoteReq.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].RiskSIComponent, //"Person",
            BasisOfSumInsured: "Agreed Value",
            // coverDetailsList: [
            //     {
            //       CoverDescription: this.scpaQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].coverName,
            //       SumInsured: this.scpaQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].coverSI,
            //       column1: this.scpaQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column1,
            //       column2: this.scpaQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column2,
            //       column3: this.scpaQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column3,
            //       column4: this.scpaQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column4,
            //       column5: this.scpaQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column5
            //     }
            // ]
          }
        ]
      },
      CustomerDetails: {
        CustomerType: this.EVQuoteReq.CustomerType,					//"Individual"
        CustomerName: this.proposalDetails.CustomerName,				//"Testuser"
        DateOfBirth: Date.parse(moment(this.proposalDetails.DateOfBirth).format('YYYY-MM-DD')),	//"1987-08-09T00:00:00"
        PinCode: this.proposalDetails.PinCode,						//"400034"
        PANCardNo: null,                                  //this.proposalDetails.PANCardNo,
        Email: this.proposalDetails.Email,						//"test@gmail.com"
        MobileNumber: this.proposalDetails.MobileNumber,				//"9999999999"
        AddressLine1: this.addressLine,							//"gjgkdjhvkf"
        CountryCode: this.CountryCode,							//100
        StateCode: this.StateCode,								//55
        CityCode: this.CityCode,								//147
        Gender: this.gender,									//"Male"
        MobileISD: "91",
        // GSTDetails: {
        //     GSTExemptionApplicable: "No",
        //     //GSTInNumber: "11AAAAA1111A1Z1",  //this.proposalDetails.GSTInNumber,
        //     GSTToState: this.proposalDetails.stateName,					//"MAHARASHTRA"
        //     ConstitutionOfBusiness: this.constName,					//"NON RESIDENT ENTITY"
        //     CustomerType: this.custType,							//"GENERAL"
        //     PanDetails: this.proposalDetails.PANCardNo,				//"AAAAA1111A"
        //     GSTRegistrationStatus: this.gstName,					//"ARN GENERATED"
        //     GSTToStateCode: 0
        // },
        AadharNumber: "",
        IsCollectionofform60: false,
        AadharEnrollmentNo: null,
        eIA_Number: null,
        // CorelationId: this.scpaQuoteRes.correlationId,					//"{{$guid}}"
        CorrelationId: this.scpaQuoteRes.correlationId,
        // CustomerID: "100984017905"
      },
      warranty: null,
      exclusionDetails: null,
      conditionDetails: null,
      clauseDetails: null,
      coinsuranceDetails: null,
      //financierDetailse: null,
      loadingDiscount: null,
      installmentUserData: null,
      //SpDetails: null,
      //_CorrelationId: "{{$guid}}",
      CorrelationId: this.scpaQuoteRes.correlationId,     //"1c3c7bd5-5b61-4dbc-9188-aa52ad350ab8"
    }

    if (this.EVQuoteReq.TransactionType == "New") {
      body.BusinessType = "New Business";
    } else {
      body.BusinessType = "Roll Over";
    }

    let coverDetailsList = [];
    if (this.scpaQuoteRes.ProductDetails || this.scpaQuoteRes.CorrelationId) {
      coverDetailsList = [
        {
          "CoverDescription": this.scpaQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].CoverName,
          "SumInsured": this.scpaQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].CoverSI,
          "column1": this.scpaQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].Column1,
          "column2": this.scpaQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].Column2,
          "column3": this.scpaQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].Column3,
          "column4": this.scpaQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].Column4,
          "column5": this.scpaQuoteRes.ProductDetails.PlanDetails[0].RiskSIComponentDetails[0].CoverDetails[0].Column5
        }
      ]

      body.riskDetails.RisksList[0].coverDetailsList = coverDetailsList;
      body.CorrelationId = this.scpaQuoteRes.CorrelationId;
    } else {
      coverDetailsList = [
        {
          CoverDescription: this.scpaQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].coverName,
          SumInsured: this.scpaQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].coverSI,
          column1: this.scpaQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column1,
          column2: this.scpaQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column2,
          column3: this.scpaQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column3,
          column4: this.scpaQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column4,
          column5: this.scpaQuoteRes.productDetails.planDetails[0].riskSIComponentDetails[0].coverDetails[0].column5
        }
      ]

      body.riskDetails.RisksList[0].coverDetailsList = coverDetailsList;
      body.CorrelationId = this.scpaQuoteRes.correlationId;
    }

    //let NomineeDetails;
    let currentDate, birthDate, today, currentYear, birthYear, currDay, currMonth;
    if (
      this.addNomineeVehicle == true &&
      this.proposalDetails.NameOfNominee != ""
    ) {
      // NomineeDetails = {
      //   NomineeType: "PA-Owner Driver",
      //   NameOfNominee: this.proposalDetails.NameOfNominee,
      //   Age: this.proposalDetails.Age,
      //   Relationship: this.proposalDetails.Relationship,
      // };
      today = new Date();
      currentDate = moment(today).format("DD-MM-YYYY");
      currDay = moment(today).format("DD");
      currMonth = moment(today).format("MM");
      currentYear = moment(today).format("YYYY");
      birthYear = currentYear - parseInt(this.proposalDetails.Age);
      let date = new Date(birthYear, currMonth, currDay);
	birthDate = moment(date).format("YYYY-MM-DD");
      //console.info("Birth Date",birthDate);

      body.riskDetails.NomineeName = this.proposalDetails.NameOfNominee;
      body.riskDetails.NomineeRelationship = this.proposalDetails.Relationship;
      //body.riskDetails.Age = this.proposalDetails.Age;
      body.riskDetails.NomineeDOB = birthDate;

    }
    let GSTDetails;
    if (this.GSTDetails == true && this.proposalDetails.GSTInNumber != "") {
      GSTDetails = {
        GSTExemptionApplicable: "No",
        GSTInNumber: this.proposalDetails.GSTInNumber, // GSTIN/ UIN Number   // text box
        GSTToState: this.proposalDetails.stateName, // no need to show on screen
        ConstitutionOfBusiness: this.constName, // Contrubution of business  // drop down
        CustomerType: this.custType, // Customer Type   // dropdown
        PanDetails: this.proposalDetails.PANCardNo, // Pan Details // text box
        GSTRegistrationStatus: this.gstName, // GST Registration status // Dropdown
        GSTToStateCode: 0
      };
    }
    body.CustomerDetails.GSTDetails = GSTDetails;
    // body.NomineeDetails = NomineeDetails;

    if (this.financierDetailsVal == true) {
      body.financierDetails = this.financierDetails;
    } else {
      body.financierDetails = null;
    }


    let str = JSON.stringify(body);
    this.cs.loaderStatus = true;
    this.createAllRiskBizToken().then(() => {
      // this.cs.loaderStatus = true;
      //this.spinloader = true; 
      this.cs.allRiskEBpostBiz("/zerotat/Proposal/Create", str).subscribe(
        (res: any) => {
          if (res.statusMessage == "Success") {
            this.cs.loaderStatus = false;
            localStorage.setItem("scpaPropDatareq", JSON.stringify(body));
            localStorage.setItem("scpaPropDatares", JSON.stringify(res));
            this.scpacalculatedData = res;
            this.scpafinalamount = Math.round(this.scpacalculatedData.gstserviceresponse.gstservicedetails.finalamount);
            console.info("SCPA AMT", this.scpafinalamount);
            if (this.evfinalamount && this.scpafinalamount) {
              this.finalamount = Math.round(this.evfinalamount + this.scpafinalamount);
            } else {
              this.finalamount = Math.round(this.evfinalamount);
            }
            this.scpaProposalNo = this.scpacalculatedData.proposalNumber;
            this.paymentFlag = true;

            console.info("SCPA Proposal", this.scpaProposalNo);

            this.scpaSaveOfflineProposal().then(() => {
              if (this.isAllRiskCreated = true) {
                this.showPaymentFlag = true;
                let scrollingElement = document.scrollingElement || document.body;
                this.getMultiProposalData(this.evProposalNo, this.customerID, this.scpaProposalNo, this.customerID, this.scpafinalamount, this.evfinalamount, this.finalamount);
                $(scrollingElement).animate(
                  {
                    scrollTop: document.body.scrollHeight,
                  },
                  500
                );
              } else {
                this.showPaymentFlag = false;
                swal({
                  text: "At present system response is slow, kindly try after sometime. We regret the inconvenience",
                });
              }
            });
          }
          else {
            this.cs.loaderStatus = false;
            let errormsg = res.message;
            this.api.handleerrors(errormsg);
            let errorbody = {
              RequestJson: JSON.stringify(body),
              ResponseJson: JSON.stringify(res),
              ServiceURL:
                commonData.bizURL1 + "/zerotat/Proposal/Create",
              CorrelationID: this.scpaQuoteRes.correlationId,
            };
            this.api.adderrorlogs(errorbody);
            this.calculatedData = null;
            //resolve();
          }
        },
        (err) => {
          swal({ closeOnClickOutside: false, text: err });
          // swal({ closeOnClickOutside: false, text: 'At present system response is slow, kindly try after sometime. We regret the inconvenience' })
          //resolve();
        }
      );
    });
  }

  scpaSaveOfflineProposal(): Promise<any> {
    return new Promise((resolve: any) => {
      let body: any;
      let propReq1 = JSON.parse(localStorage.getItem("scpaPropDatareq"));
      let propRes1 = JSON.parse(localStorage.getItem("scpaPropDatares"));
      let bankType = localStorage.getItem("bankType");

      body = {
        // BundleID: this.bundleID,
        proposalRQ: propReq1,
        proposalRS: propRes1,
        bankType: bankType
      };


      // let savequoteData = JSON.parse(localStorage.getItem("saveQuoteData"));
      // if (savequote == true) {
      //   if (savequoteData.isSubAgent == true) {
      //     body.IsSubagent = savequoteData.isSubAgent;
      //     body.SubagentIpartnerUserID = savequoteData.subagentIpartnerUserID;
      //   } else {
      //     body.IsSubagent = savequoteData.isSubAgent;
      //   }
      // } else {
      //   if (
      //     this.paramDataValues.iPartnerLogin.isSubagent != null ||
      //     this.paramDataValues.iPartnerLogin.isSubagent != undefined
      //   ) {
      //     body.IsSubagent = true;
      //     body.SubagentIpartnerUserID =
      //       this.paramDataValues.iPartnerLogin.subAID;
      //   }
      // }

      let str = JSON.stringify(body);
      this.cs.post("Proposal/twev/SaveProposal", str).then((res: any) => {
        if (res.status == "Success") {
          this.saveProposalStatus = res.status;
        } else {
          let errorbody = {
            RequestJson: JSON.stringify(body),
            ResponseJson: JSON.stringify(res),
            ServiceURL: URL,
            CorrelationID: this.scpaQuoteRes.correlationId,
          };
          this.api.adderrorlogs(errorbody);
        }

        this.cs.loaderStatus = false;
        resolve();
      })
        .catch((err: any) => {
          swal({ text: err });
          this.cs.loaderStatus = false;
        });
    });
  }

  refreshToken(ev: any, ev1: any): Promise<any> {
    return new Promise((resolve: any) => {
      this.api.refreshToken().subscribe(
        (res: any) => {
          if (ev == "new" && this.productCode == "2312") {
            this.policy = "NEW";
          }
          if (ev == "roll" && this.productCode == "2312") {
            this.policy = "ROLL";
          }

          let Username,
            Password,
            Expiry,
            Signature,
            iPartner_Token,
            view,
            isSubagent,
            subAID;
          let existingtoken = JSON.parse(localStorage.getItem("AuthToken"));

          if (!this.cs.isUndefineORNull(res)) {
            // //CreateToken + GetApplicationAccess merging code - Sejal
            let body1 = {
              expiry: res.expiry,
              token: res.token,
              username: existingtoken.username,
              applicationAccess: existingtoken.applicationAccess,
            };
            localStorage.setItem("AuthToken", JSON.stringify(body1));
            let paramData = JSON.parse(localStorage.getItem("paramData"));
            paramData.iPartnerLogin.policy = this.policy;
            paramData.iPartnerLogin.product = this.product;
            Username = paramData.Username;
            Password = paramData.Password;
            Expiry = paramData.iPartnerLogin.Expiry;
            Signature = paramData.iPartnerLogin.Signature;
            iPartner_Token = paramData.iPartnerLogin.iPartner_Token;
            view = paramData.iPartnerLogin.view;
            isSubagent = paramData.iPartnerLogin.isSubagent;
            subAID = paramData.iPartnerLogin.subAID;
            let body;
            body = {
              Username: Username,
              Password: Password,
              iPartnerLogin: {
                Expiry: Expiry,
                Signature: Signature,
                iPartner_Token: iPartner_Token,
                product: this.productCode,
                policy: this.policy,
                view: view,
              },
            };
            if (isSubagent == "Y") {
              body.iPartnerLogin.isSubagent = isSubagent;
              body.iPartnerLogin.subAID = subAID;
            }
            localStorage.setItem("paramData", JSON.stringify(body));
            // window.location.reload();
            this.router
              .navigateByUrl("/", { skipLocationChange: true })
              .then(() => this.router.navigate(["/quote"]));

            this.cs.data$.next({
              flag: true,
            });
          }
          resolve();
        },
        (err) => {
          this.cs.loaderStatus = false;
        }
      );
    });
  }

  // All Risk Electric Bike starts
  // Sejal - 03-03-2022
  getEBModelList(ev: any) {
    this.modelname = ev.target.value.toLowerCase();
    this.allriskEBModel = [
      { id: 1, text: "INSPIRER" },
      { id: 2, text: "JAUNTY" },
      { id: 3, text: "SPIN LED" }
    ]
    // ["INSPIRER","JAUNTY","SPIN LED"];
    this.EBVehicleModel = this.allriskEBModel.filter((option) => {
      return (
        option.text.toLowerCase().includes(this.modelname)
        //&& option.vehicleclasscode == this.classCode
      );
    });
  }

  // Sejal - 03-03-2022
  getManufacturerList(ev: any) {
    //this.manuname = ev.target.value.toLowerCase();
    this.ebmanulist = [
      { id: 1, text: "AMO MOBILITY SOLUTIONS PRIVATE LIMITED" }
    ]

    // Sejal - 03-03-2022 
    // this.EBManufacturerList = this.ebmanulist.filter((option) => {
    //   return (
    //     option.text.toLowerCase().includes(this.manuname)
    //   );
    // });
    this.EBManufacturerList = this.ebmanulist;
  }

  // getKiloWattList(ev: any) {
  //   this.KiloWattList.push(this.proposalDetails.kilowatt);
  //   }

  // All Risk Electric Bike ends

// AML Implementation 
// Show and hide income Detail 
clickIncome(ev: any) {   
  if (ev.target.value == "Salaried") {
   this.annualIncomeDetails= true;
   this.selfEmployed  = false;
   this.isSalaried =true
   this.isSelfEmployed=false;
    this.annualIncome="";
    this.businessType="";
  } else {
    this. annualIncomeDetails= false;
    this.selfEmployed  = true;
    this.isSalaried =false;
    this.isSelfEmployed=true;   
  }
}

clickAnualIncome(ev: any) { 
  this.annualIncome =ev.target.value;
}
clickNatureOfBusiness(ev: any) { 
  this.businessType=ev.target.value;
}
oncheckValue(checkValue : string){
  console.log(checkValue);
}

  // Soft Copy changes
  clickTerm(ev: any) {
    this.isAmlAggrement = ev.target.checked;    
  }

ValidateAml() : boolean {
  let isvalid =true;
  
    if (this.isAMLRequired ==true)
    {
      
      if (this.isSalaried ==false && this.isSelfEmployed ==false)
      {
        swal({ text: "Please Select Income Type" });
        isvalid =false;
      }
  
    else if (this.isSalaried ==true && this.cs.isUndefineORNull(this.annualIncome)==true)    
    {
      swal({ text: "Please select anually Income" });
      isvalid =false
    }
    else if (this.isSelfEmployed ==true &&  this.businessType =="")    
    {
      swal({ text: "Please select nature of bussiness" });  
      isvalid =false;
    }
    else if (this.proposalDetails.employeeName =="" && this.cs.isUndefineORNull(this.proposalDetails.employeeName)==true)    
    {
      swal({ text: "Please enter name" });  
      isvalid =false;
    }
    else if (this.proposalDetails.officeAddress =="" && this.cs.isUndefineORNull(this.proposalDetails.officeAddress)==true)    
    {
    swal({ text: "Please enter address" });  
    isvalid =false;
  }
  else if (this.isAmlAggrement == false)    
    {
    swal({ text: "Please select terms and condition" });  
    isvalid =false;
  }  
  }  
    return isvalid;
  }

  //(ngModelChange)="EmployeeNameChange($event)"
  EmployeeNameChange(ev: any) {
    this.proposalDetails.employeeName=ev.target.value;
  }
  //(ngModelChange)="OfficeAddressChange($event)"
  OfficeAddressChange(ev: any) {
    this.proposalDetails.officeAddress=ev.target.value;
  } 





// AML Implementation End
}
