import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "src/app/shared/shared.module";
import { ClaimsRoutingModule } from "./claims-routing.module";
import { ClaimIntimationStatusComponent } from "./claim-intimation-status/claim-intimation-status.component";
import { ClaimsComponent } from "./claims.component";

@NgModule({
  imports: [CommonModule, ClaimsRoutingModule, SharedModule],
  exports: [ClaimIntimationStatusComponent, ClaimsComponent],
  declarations: [ClaimIntimationStatusComponent, ClaimsComponent],
})
export class ClaimsModule {}
