import { Component, OnInit } from "@angular/core";
import { CommonService } from "src/app/services/common.service";
import { commonData } from "../../commonData/commonData";
import { Variable } from "@angular/compiler/src/render3/r3_ast";
import { Router } from "@angular/router";
declare var $: any;

@Component({
  selector: "app-claims",
  templateUrl: "./claims.component.html",
  styleUrls: ["./claims.component.css"],
})
export class ClaimsComponent implements OnInit {
  claimData: any = {};
  raiseClaim: boolean = false;
  claimStatus: boolean = false;

  constructor(private cs: CommonService, private _router: Router) {}

  ngOnInit() {
    if (this._router.url == "/claims") {
      var body = document.body;
      body.classList.add("claim-body");
    }
  }

  public ngOnDestroy() {
    var body = document.body;
    body.classList.remove("claim-body");
  }
}
