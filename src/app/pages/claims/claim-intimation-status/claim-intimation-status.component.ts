import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { commonData } from "src/app/commonData/commonData";

declare var $: any;
@Component({
  selector: "app-claim-intimation-status",
  templateUrl: "./claim-intimation-status.component.html",
  styleUrls: ["./claim-intimation-status.component.css"],
})
export class ClaimIntimationStatusComponent implements OnInit {
  pageName: any;
  claimData: any = {};

  constructor(private _router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    if (
      this._router.url == "claims/claim-status" ||
      this._router.url == "claims/claim-intimation"
    ) {
      var body = document.body;
      body.classList.add("claim-body");
    }
    this.route.data.subscribe((data: any) => {
      this.pageName = data.pageName;
    });

    $(".input-field input, select").focus(function () {
      $(this).parents(".input-field").addClass("focused");
    });

    $(".input-field input, select").blur(function () {
      var inputValue = $(this).val();
      if (inputValue == "") {
        $(this).removeClass("filled");
        $(this).parents(".input-field").removeClass("focused");
      } else {
        $(this).addClass("filled");
      }
    });
  }

  claimIntiation() {
    if (this.pageName == "claimIntimation") {
      window.open(
        commonData.claimUrl +
          "ClaimIntimation?polnum=" +
          this.claimData.policyNumber +
          "&vehno=" +
          this.claimData.vehNo +
          "&token=iEqEF/TTY2XHFuwZmAOZ8Q==",
        "_blank"
      );
    } else if (this.pageName == "claimStatus") {
      window.open(
        commonData.claimUrl +
          "ClaimStatus?polnum=" +
          this.claimData.policyNumber +
          "&vehno=" +
          this.claimData.vehNo +
          "&token=iEqEF/TTY2XHFuwZmAOZ8Q==",
        "_blank"
      );
    } else {
      this._router.navigateByUrl("/claims");
    }
    this.claimData = {};
  }

  public ngOnDestroy() {
    var body = document.body;
    body.classList.remove("claim-body");
  }
}
