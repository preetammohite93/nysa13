import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ClaimIntimationStatusComponent } from "./claim-intimation-status/claim-intimation-status.component";
import { ClaimsComponent } from "./claims.component";

const routes: Routes = [
  {
    path: "",
    component: ClaimsComponent,
    children: [
      {
        path: "/claim-status",
        component: ClaimIntimationStatusComponent,
        data: { pageName: "claimStatus" },
      },
      {
        path: "/claim-intimation",
        component: ClaimIntimationStatusComponent,
        data: { pageName: "claimIntimation" },
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClaimsRoutingModule {}
