import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QcCasesComponent } from './qc-cases.component';

describe('QcCasesComponent', () => {
  let component: QcCasesComponent;
  let fixture: ComponentFixture<QcCasesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QcCasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QcCasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
