import { Component, OnInit } from "@angular/core";
import * as moment from "moment";
import { findIndex } from "rxjs/operators";
import { CommonService } from "src/app/services/common.service";
import swal from "sweetalert";

declare var $: any;
@Component({
  selector: "app-qc-cases",
  templateUrl: "./qc-cases.component.html",
  styleUrls: ["./qc-cases.component.css"],
})
export class QcCasesComponent implements OnInit {
  qcObj: any = {};
  spinloader: boolean = false;
  successMsg: boolean = false;
  message: any;
  qcCasesList: any;
  qcCasesList_proto: any;
  filesData: any = [];
  fileObj: any;
  uploadedFile: any;
  fakeImgPath: any = [];
  postObj: any;
  fileAddStatus: boolean = false;
  fileAddStatus1: boolean = false;
  fileAddStatus2: boolean = false;
  fileAddStatus3: boolean = false;
  fileAddStatus4: boolean = false;
  amount: any;
  term: any;
  page: any;
  memmaxDOB: any;
  showTable: boolean = false;
  filterCase: string;
  QCData: any;
  QCDataLength: any;
  clearanceremarks: any;
  constructor(private cs: CommonService) {}

  ngOnInit() {
    this.memmaxDOB = new Date();
    this.memmaxDOB.setDate(this.memmaxDOB.getDate() + 0);
    let startDate = moment(new Date()).format("YYYY-MM-DD");
    this.qcObj.FromDate = startDate;
    this.qcObj.ToDate = startDate;
  }

  getQcCases() {
    let body = {
      FromDate: moment(this.qcObj.FromDate).format("DD-MMMM-YYYY"),
      ToDate: moment(this.qcObj.ToDate).format("DD-MMMM-YYYY"),
    };

    let str = JSON.stringify(body);
    this.spinloader = true;

    this.cs
      .post("DocUpload/GetCoverNoteDetails", str)
      .then((res: any) => {
        if (res.status == "Success") {
          this.showTable = false;
          this.spinloader = false;
          this.qcCasesList_proto = res.coverNoteDetails;
          this.qcCasesList = this.qcCasesList_proto;
          this.getFilter(this.filterCase);
        } else {
          this.spinloader = false;
          this.showTable = true;
        }
      })
      .catch((err: any) => {
        this.spinloader = false;
      });
  }

  removeImg(ev) {
    let index, fileType, filename;
    // console.log(ev);

    this.filesData.forEach((ele, i) => {
      // console.log(ele);
      fileType = ele.name.split(".");
      if (fileType[0] == ev.target.id) {
        filename = fileType[0];
        index = i;
      }
    });

    // index = i;
    if (filename == "Previous_Policy_Insurance_Copy") {
      this.fileAddStatus = false;
    } else if (filename == "RC_Book_Copy") {
      this.fileAddStatus1 = false;
    } else if (filename == "pan_card_copy") {
      this.fileAddStatus2 = false;
    } else if (filename == "approval_mails") {
      this.fileAddStatus3 = false;
    } else if (filename == "other_doc") {
      this.fileAddStatus4 = false;
    }
    this.filesData.splice(index, 1);
  }

  getFilter(ev) {
    // this.getQcCases();

    this.filterCase = ev;
    if (this.qcCasesList) {
      switch (this.filterCase) {
        case "allCases":
          this.qcCasesList = this.qcCasesList_proto;
          break;
        case "appCases_1":
          this.qcCasesList = this.qcCasesList_proto.filter(
            (res) => res.qcStatus == "Approved"
          );
          break;

        case "pendCases_1":
          this.qcCasesList = this.qcCasesList_proto.filter(
            (res) => res.qcStatus == "Pending"
          );
          break;

        default:
          break;
      }
    }
  }

  handleFileInput(ev: any) {
    if (ev.target.files.length > 0) {
      this.fileObj = ev.target.files[0];
      let fileType = this.fileObj;
      if (fileType && fileType.name) {
        fileType = fileType.name.toString().toLowerCase();
        fileType = fileType.split(".");
        fileType[0] = ev.target.id;
        fileType.name = fileType.join(".");
        const myfile = new File([this.fileObj], fileType.name);
        this.filesData.push(myfile);
        this.successMsg = false;
        this.message = "";
        // this.uploadedFile = ev.target.files[0];
        if (ev.target.id == "Previous_Policy_Insurance_Copy") {
          this.fileAddStatus = true;
        } else if (ev.target.id == "RC_Book_Copy") {
          this.fileAddStatus1 = true;
        } else if (ev.target.id == "pan_card_copy") {
          this.fileAddStatus2 = true;
        } else if (fileType[0] == "approval_mails") {
          this.fileAddStatus3 = true;
        } else if (fileType[0] == "other_doc") {
          this.fileAddStatus4 = true;
        }
      }
    }
  }

  // handleFileInput(ev:any){

  //   this.fakeImgPath = [];
  //   this.fileObj.files = [];
  //   if (ev.target.files.length > 0 && ev.target.files.length <= 4) {
  //     this.filesData = ev.target.files;
  //     for (let i = 0; i < this.filesData.length; i++) {

  //       if (this.filesData[i].size <= 5*1024*1024) {
  //         let fileType = this.filesData[i];
  //         if (fileType && fileType.name) {
  //           fileType = fileType.name.toString().toLowerCase();
  //           fileType = fileType.split('.');
  //           const type = fileType;
  //           if (type && type.length > 0 &&
  //             (type[type.length - 1] === 'jpg' ||
  //               type[type.length - 1] === 'png' ||
  //               type[type.length - 1] === 'jpeg' ||
  //               type[type.length - 1] === 'pdf')) {
  //             // this.uploadedFile = ev.target.files[0];
  //             this.fileObj.files.push(this.filesData[i]);
  //           } else {
  //             swal({
  //               title: "Alert!",
  //               text:  `Sorry, '${this.filesData[i].name}' is invalid,
  //                allowed extensions are: (.jpg, .jpeg, .png) files`
  //             }).then(()=> {
  //               this.fakeImgPath = [];
  //             })

  //           }
  //         }
  //       }else{
  //         swal({
  //           title: "Alert!",
  //           text: 'File Size Should be less than 5 MB'
  //         })
  //       }

  //     }
  //   }else{
  //     swal({
  //       title: "Alert!",
  //       text: 'Maximum 4 images are allowed'
  //     })
  //   }
  // }

  openFile(data: any) {
    console.log("QC Data", data, this.clearanceremarks);
    let pendencyID = 0;
    // this.QCData = data.pendencyRemarks;
    this.QCData = data.pendencyRemarks.sort((n1, n2) => {
      return n1.pendencyID - n2.pendencyID;
    });
    console.log("Sorted", this.QCData);
    this.QCDataLength = data.pendencyRemarks.length;
    this.QCData.forEach((data: any) => {
      if (this.cs.isUndefineORNull(data.pendencyClearanceRemark)) {
        pendencyID = data.pendencyID;
      }
    });
    this.postObj = {
      ProposalNo: data.proposalNo,
      TransactionType: data.transactionType,
      PolicySubType: data.policySubTypeID,
      PendencyClearanceRemark: this.clearanceremarks,
      PendencyID: pendencyID,
    };
    this.amount = data.totalPremiumAmt;
    this.fileAddStatus = false;
    this.fileAddStatus1 = false;
    this.fileAddStatus2 = false;
    this.fileAddStatus3 = false;
    this.fileAddStatus4 = false;
    this.filesData = [];
    $("#ViewDocumentModal").modal("show");
  }

  uploadDocs() {
    console.log("QC Data", this.clearanceremarks);
    if (this.filesData.length > 0) {
      this.spinloader = true;
      this.cs
        .uploadDocument(this.postObj, this.clearanceremarks, this.filesData)
        .subscribe((res) => {
          if (res.status == "Success") {
            this.spinloader = false;
            this.successMsg = true;
            this.message = res.message;
            this.filesData = [];

            setTimeout(() => {
              $("#ViewDocumentModal").modal("hide");
              this.successMsg = false;
              this.fileAddStatus = false;
              this.fileAddStatus1 = false;
              this.fileAddStatus2 = false;
              this.fileAddStatus3 = false;
              this.fileAddStatus4 = false;
              this.getQcCases();
            }, 2000);
          } else {
            this.successMsg = true;
            this.message = res.message;
          }
        });
    } else {
      this.spinloader = false;
      this.successMsg = true;
      this.message = "Please select atleast 1 document";
    }
  }

  // viewDoc() {

  //   let body = {
  //     "PrefixKey":"Nysa/1207196029/",
  //     "Duration":12
  //   }

  //   let str = JSON.stringify(body);
  //   this.cs.loaderStatus = true;

  //   this.cs.postOps('/GetDocumentsByKey', str).then((res: any) => {

  //     if (res.status == 'Success') {
  //       this.cs.loaderStatus = false;
  //       this.qcCasesList = res.coverNoteDetails;
  //     }else{
  //       this.cs.loaderStatus = false;
  //     }

  //   }).catch((err: any) => {
  //     this.cs.loaderStatus = false;
  //   })
  // }
}
