import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { TechopsLoginRoutingModule } from "./techops-login-routing.module";
import { TechopsLoginComponent } from "./techops-login.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [TechopsLoginComponent],
  imports: [CommonModule, TechopsLoginRoutingModule, SharedModule],
})
export class TechopsLoginModule {}
