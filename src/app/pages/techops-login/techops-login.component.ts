import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { CommonService } from "src/app/services/common.service";
import swal from "sweetalert";
import { Location } from "@angular/common";

declare var $: any;
@Component({
  selector: "app-techops-login",
  templateUrl: "./techops-login.component.html",
  styleUrls: ["./techops-login.component.css"],
})
export class TechopsLoginComponent implements OnInit {
  loginObj: any = {
    userName: "",
    pwd: "",
  };

  error: any;

  constructor(
    public cs: CommonService,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit() {
    this.cs.clearLocalStorage();
  }

  login() {
    if (
      this.loginObj.userName == null ||
      this.loginObj.userName == undefined ||
      this.loginObj.userName == ""
    ) {
      swal({ text: "Username is required" });
    } else if (
      this.loginObj.pwd == null ||
      this.loginObj.pwd == undefined ||
      this.loginObj.pwd == ""
    ) {
      swal({ text: "Password is required" });
    } else {
      if (this.router.url == "/techops/login") {
        this.cs
          .loginTechops("AuthenticateUser", this.loginObj)
          .then((res: any) => {
            this.cs.loaderStatus = true;
            if (res.IsValid_User == true) {
              this.cs.loaderStatus = false;
              localStorage.setItem("techopsUser", this.loginObj.userName);
              this.router.navigateByUrl("techops");
            } else {
              this.cs.loaderStatus = false;
              swal({
                text: "Invalid User",
              });
            }
          })
          .catch((err: any) => {
            swal({
              text: "Login Failed",
            });
          });
      } else if (this.router.url == "/dtm/login") {
        this.router.navigate(["/dtm/dashboard"]);
      }
    }
  }
}
