import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TechopsLoginComponent } from "./techops-login.component";

const routes: Routes = [
  {
    path: "",
    component: TechopsLoginComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TechopsLoginRoutingModule {}
