import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiServiceService } from "src/app/services/api-service.service";
import { CommonService } from "src/app/services/common.service";
import swal from "sweetalert";

@Component({
  selector: "app-break-in-dash",
  templateUrl: "./break-in-dash.component.html",
  styleUrls: ["./break-in-dash.component.css"],
})
export class BreakInDashComponent implements OnInit {
  productCode: any;
  BreakINDetails: any;
  BreakInRequest = [];
  term: any;
  authtoken: any;
  dealDetails: any;
  dealDetailsfromIM: any;
  index: any;
  request: any;
  dealData: any;
  isBreakInDash = false;
  page: any;

  /**
   * Digital POS variables
   */
  IsPOSTransaction_flag: boolean = false;

  constructor(
    public cs: CommonService,
    public api: ApiServiceService,
    public router: Router
  ) {
    /**
     * digitalPOS change
     * get values from localStorage for checking if conditions in html file
     * date :- 29-07-2021
     */
    if (localStorage.getItem("IsPOSTransaction")) {
      this.IsPOSTransaction_flag = JSON.parse(
        localStorage.getItem("IsPOSTransaction")
      );
    }
  }

  ngOnInit() {
    let data = JSON.parse(localStorage.getItem("paramData"));
    this.authtoken = JSON.parse(localStorage.getItem("AuthToken"));
    this.productCode = data.iPartnerLogin.product;
    this.getBreakINDetails();
  }

  goToMyBreakIn() {
    this.isBreakInDash = true;
    this.router.navigateByUrl("quote");
    localStorage.setItem("fromBreakInDash", JSON.stringify(this.isBreakInDash));
  }

  getBreakINDetails() {
    this.cs.loaderStatus = true;
    this.cs.getURL("BreakIn/GetBreakinDetails").then((res: any) => {
      this.BreakINDetails = res;
      localStorage.setItem("BreakINData", JSON.stringify(this.BreakINDetails));
      this.BreakINDetails.forEach((req: any) => {
        if (this.cs.isUndefineORNull(req.breakInRQ)) {
          this.BreakInRequest.push(req.breakInRQ);
        } else {
          this.BreakInRequest.push(JSON.parse(req.breakInRQ));
        }
      });
      this.cs.loaderStatus = false;
      /**
       * digitalPOS change
       * call getDeal and detDealDetails function only for nysa not for POS
       * date :- 29-07-2021
       */
      if (this.IsPOSTransaction_flag == true) {
        this.dealDetailsfromIM.primaryVerticalName = "";
        this.dealDetailsfromIM.primaryMOName = "";
      } else {
        this.getDeal().then((res: any) => {
          this.getDealDetails();
        });
      }
    });
  }

  getDeal(): Promise<void> {
    return new Promise((resolve) => {
      this.api
        .getDeal(this.authtoken.username, this.productCode)
        .subscribe((res: any) => {
          this.dealDetails = res;
          resolve();
        });
    });
  }

  getDealDetails() {
    this.api.getDealDetails(this.dealDetails.dealID).subscribe((res: any) => {
      if (res.status == "FAILED") {
        swal({ text: "Deal Details not found" });
      } else {
        this.dealDetailsfromIM = res;
        localStorage.setItem(
          "IMDealData",
          JSON.stringify(this.dealDetailsfromIM)
        );
      }
    });
  }

  showDetails(index: any, dealData: any) {
    this.index = index;
    this.dealData = dealData;
    if (this.cs.isUndefineORNull(this.BreakINDetails[index].breakInRQ)) {
      this.request = this.BreakINDetails[index].breakInRQ;
    } else {
      this.request = JSON.parse(this.BreakINDetails[index].breakInRQ);
    }
  }
}
