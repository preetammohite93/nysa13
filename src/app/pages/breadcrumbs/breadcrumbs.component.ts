import {
  Component,
  OnInit,
  Input,
  Output,
  ViewChild,
  ElementRef,
  AfterViewInit,
  DoCheck,
  EventEmitter,
} from "@angular/core";
import { SwitchLanguageService } from "src/app/services/switch-language.service";
import { CommonService } from "src/app/services/common.service";
import swal from "sweetalert";
import { Router } from "@angular/router";
import { ApiServiceService } from "src/app/services/api-service.service";
import { commonData } from "src/app/commonData/commonData";
declare var $: any;
@Component({
  selector: "app-breadcrumbs",
  templateUrl: "./breadcrumbs.component.html",
  styleUrls: ["./breadcrumbs.component.css"],
})
export class BreadcrumbsComponent implements OnInit, AfterViewInit, DoCheck {
  @Input() productInput: any;
  paramDataValues: any;
  authtok: any;
  policyType: any;
  productCode: any;
  type: any;
  product: any;
  view: any;

  lang = [];
  currentLanguage: string;
  selectedOption: string;
  language: any;
  defaultLang: any;
  currentUrl: any;
  currentPath: any;
  body: any = {};
  isSaveQuote = false;
  isPending = false;
  isBreakIN = false;
  @ViewChild("langSelect", { static: false }) private langSelect: ElementRef;

  IsPOSTransaction_flag: boolean = false;
  posProductType: string = "";
  landingPageValue: any;
  checkLandingPageValueFlag: boolean = false;

  constructor(
    private switchLang: SwitchLanguageService,
    public cs: CommonService,
    private router: Router,
    private api: ApiServiceService
  ) {
    /**
     * digitalPOS change
     * get values from localStorage for checking if conditions in html file
     * hidding the side nav on mobile screen's for digital pos
     * date :- 29-07-2021
     */
    if (localStorage.getItem("IsPOSTransaction")) {
      this.IsPOSTransaction_flag = JSON.parse(
        localStorage.getItem("IsPOSTransaction")
      );
    }

    /**
     * DigitalPOS Change
     * 08-12-2021
     */
    this.cs.data$.subscribe((res: any) => {
      if (res) {
        if (res.flag == true) {
          this.changeBreadcrumbName();
        }
      }
    });
  }

  ngOnChanges() {
    this.productCode = this.productInput;
    this.checkAllProductConditions();
  }

  ngOnInit() {
    this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
    this.isSaveQuote = JSON.parse(localStorage.getItem("savedQuotes"));
    this.isPending = JSON.parse(localStorage.getItem("isPendingPayment"));
    this.isBreakIN = JSON.parse(localStorage.getItem("breakinFlag"));
    this.view = this.paramDataValues.iPartnerLogin.view;
    this.authtok = JSON.parse(localStorage.getItem("AuthToken"));
    this.policyType = this.paramDataValues.iPartnerLogin.policy;
    console.log("USED", this.policyType);
    if (this.isSaveQuote) {
      let data = JSON.parse(localStorage.getItem("saveQuoteData"));
      this.productCode = data.productCode;
    } else if (this.isBreakIN) {
      let data = JSON.parse(localStorage.getItem("breakinData"));
      this.productCode = data.productCode;
    } else if (this.isPending) {
      let data = JSON.parse(localStorage.getItem("PendingData"));
      this.productCode = data.productCode;
    } else {
      this.productCode = this.paramDataValues.iPartnerLogin.product;
    }

    if (this.policyType == "NEW") {
      this.type = "New";
    } else if (this.policyType == "ROLL") {
      this.type = "Rollover";
    } else {
      this.type = "Used";
    }
    if (this.productCode == "2312") {
      this.product = "Two Wheeler";
    } else if (this.productCode == "2311") {
      this.product = "Four Wheeler";
    } else if (this.productCode == "2320") {
      this.product = "Two Wheeler Liability";
    } else if (this.productCode == "2319") {
      this.product = "Four Wheeler Liability";
    } else if (this.productCode == commonData.allriskProductCode) {
      this.product = "Risk Electric Bike";
    } else {
      swal({
        closeOnClickOutside: false,
        text: "Currently we dont have this product",
      }).then(() => {
        this.cs.geToLogin();
      });
    }
    this.prefferedLanguage();
    let url = this.router.url;
    this.currentPath = url.replace(/[^a-zA-Z ]/g, "");
    if (this.currentPath == "quote") {
      this.currentPath = "Quote";
    } else if (this.currentPath == "proposal") {
      this.currentPath = "Proposal";
    // } else if (this.currentPath == "payment") {
    //   this.currentPath = "Payment";
    } else if (
      this.currentPath == "paymentconfirmation" ||
      this.currentPath == "paymentconfirmproposalNo"
    ) {
      this.currentPath = "Payment Confirmation";
    } else if (window.location.href.indexOf("multipayment") > -1) {
      this.currentPath = "Multi payment";
    } else if (this.currentPath == "lombardpaywallet") {
      this.currentPath = "Lombard Pay Wallet";
    } else if (this.currentPath == "createBreakIN") {
      this.currentPath = "Create BreakIn";
    } else if (this.currentPath == "paymentconfirmproposalNoisMulti") {
      this.currentPath = "Multi payment";
    }

    /**
     * DigitalPOS Change
     * 16-09-2021
     */

    if (this.IsPOSTransaction_flag == true) {
      if (localStorage.getItem("landingPage")) {
        this.landingPageValue = JSON.parse(localStorage.getItem("landingPage"));
        if (
          this.landingPageValue != "NysaPolicies" &&
          this.landingPageValue != "PendingPayment" &&
          this.landingPageValue != "BreakIn" &&
          this.landingPageValue != "SavedProposal" &&
          this.landingPageValue != "QC" &&  
          this.landingPageValue != "IAS"
        ) {
          if (this.policyType == "NEW" && this.productCode == "2311") {
            this.posProductType = "PVT Car Comprehensive(New)";
          }
          if (this.policyType == "NEW" && this.productCode == "2312") {
            this.posProductType = "Two Wheeler Comprehensive(New)";
          }

          if (this.policyType == "ROLL" && this.productCode == "2311") {
            this.posProductType = "PVT Car Comprehensive(Rollover)";
          }
          if (this.policyType == "ROLL" && this.productCode == "2312") {
            this.posProductType = "Two Wheeler Comprehensive(Rollover)";
          }

          if (this.policyType == "NEW" && this.productCode == "2319") {
            this.posProductType = "PVT Car Third Party(New)";
          }
          if (this.policyType == "NEW" && this.productCode == "2320") {
            this.posProductType = "Two Wheeler Third Party(New)";
          }

          if (this.policyType == "ROLL" && this.productCode == "2319") {
            this.posProductType = "PVT Car Third Party(Rollover)";
          }
          if (this.policyType == "ROLL" && this.productCode == "2320") {
            this.posProductType = "Two Wheeler Third Party(Rollover)";
          }
        }
      } else {
        if (this.policyType == "NEW" && this.productCode == "2311") {
          this.posProductType = "PVT Car Comprehensive(New)";
        }
        if (this.policyType == "NEW" && this.productCode == "2312") {
          this.posProductType = "Two Wheeler Comprehensive(New)";
        }

        if (this.policyType == "ROLL" && this.productCode == "2311") {
          this.posProductType = "PVT Car Comprehensive(Rollover)";
        }
        if (this.policyType == "ROLL" && this.productCode == "2312") {
          this.posProductType = "Two Wheeler Comprehensive(Rollover)";
        }

        if (this.policyType == "NEW" && this.productCode == "2319") {
          this.posProductType = "PVT Car Third Party(New)";
        }
        if (this.policyType == "NEW" && this.productCode == "2320") {
          this.posProductType = "Two Wheeler Third Party(New)";
        }

        if (this.policyType == "ROLL" && this.productCode == "2319") {
          this.posProductType = "PVT Car Third Party(Rollover)";
        }
        if (this.policyType == "ROLL" && this.productCode == "2320") {
          this.posProductType = "Two Wheeler Third Party(Rollover)";
        }
      }
    }
  }

  ngDoCheck() {
    this.checkAllProductConditions();
  }

  ngAfterViewInit() {
    if(this.IsPOSTransaction_flag == false){
    this.langSelect.nativeElement.value = this.language;
    }
  }

  changeBreadcrumbName() {
    if (localStorage.getItem("landingPage")) {
      this.landingPageValue = JSON.parse(localStorage.getItem("landingPage"));
      if (
        this.landingPageValue != "NysaPolicies" &&
        this.landingPageValue != "PendingPayment" &&
        this.landingPageValue != "BreakIn" &&
        this.landingPageValue != "SavedProposal" &&
        this.landingPageValue != "QC" &&  
        this.landingPageValue != "IAS"
      ) {
        if (this.policyType == "NEW" && this.productCode == "2311") {
          this.posProductType = "PVT Car Comprehensive(New)";
        }
        if (this.policyType == "NEW" && this.productCode == "2312") {
          this.posProductType = "Two Wheeler Comprehensive(New)";
        }

        if (this.policyType == "ROLL" && this.productCode == "2311") {
          this.posProductType = "PVT Car Comprehensive(Rollover)";
        }
        if (this.policyType == "ROLL" && this.productCode == "2312") {
          this.posProductType = "Two Wheeler Comprehensive(Rollover)";
        }

        if (this.policyType == "NEW" && this.productCode == "2319") {
          this.posProductType = "PVT Car Third Party(New)";
        }
        if (this.policyType == "NEW" && this.productCode == "2320") {
          this.posProductType = "Two Wheeler Third Party(New)";
        }

        if (this.policyType == "ROLL" && this.productCode == "2319") {
          this.posProductType = "PVT Car Third Party(Rollover)";
        }
        if (this.policyType == "ROLL" && this.productCode == "2320") {
          this.posProductType = "Two Wheeler Third Party(Rollover)";
        }
      }
    } else {
      if (this.policyType == "NEW" && this.productCode == "2311") {
        this.posProductType = "PVT Car Comprehensive(New)";
      }
      if (this.policyType == "NEW" && this.productCode == "2312") {
        this.posProductType = "Two Wheeler Comprehensive(New)";
      }

      if (this.policyType == "ROLL" && this.productCode == "2311") {
        this.posProductType = "PVT Car Comprehensive(Rollover)";
      }
      if (this.policyType == "ROLL" && this.productCode == "2312") {
        this.posProductType = "Two Wheeler Comprehensive(Rollover)";
      }

      if (this.policyType == "NEW" && this.productCode == "2319") {
        this.posProductType = "PVT Car Third Party(New)";
      }
      if (this.policyType == "NEW" && this.productCode == "2320") {
        this.posProductType = "Two Wheeler Third Party(New)";
      }

      if (this.policyType == "ROLL" && this.productCode == "2319") {
        this.posProductType = "PVT Car Third Party(Rollover)";
      }
      if (this.policyType == "ROLL" && this.productCode == "2320") {
        this.posProductType = "Two Wheeler Third Party(Rollover)";
      }
    }
  }

  checkAllProductConditions() {
    if (this.productCode == "2312") {
      this.product = "Two Wheeler";
    } else if (this.productCode == "2311") {
      this.product = "Four Wheeler";
    } else if (this.productCode == "2320") {
      this.product = "Two Wheeler Liability";
    } else if (this.productCode == "2319") {
      this.product = "Four Wheeler Liability";
    }
    let url = this.router.url;
    console.log("URL", url);
    this.currentPath = url.replace(/[^a-zA-Z ]/g, "");
    if (this.currentPath == "quote") {
      this.currentPath = "Quote";
    } else if (this.currentPath == "proposal") {
      this.currentPath = "Proposal";
    // } else if (this.currentPath == "payment") {
    //   this.currentPath = "Payment";
    } else if (
      this.currentPath == "paymentconfirmation" ||
      this.currentPath == "paymentconfirmproposalNo"
    ) {
      this.currentPath = "Payment Confirmation";
    } else if (window.location.href.indexOf("multipayment") > -1) {
      this.currentPath = "Multi payment";
    } else if (this.currentPath == "lombardpaywallet") {
      this.currentPath = "Lombard Pay Wallet";
    } else if (this.currentPath == "createBreakIN") {
      this.currentPath = "Create BreakIn";
    } else if (this.currentPath == "paymentconfirmproposalNoisMulti") {
      this.currentPath = "Multi payment";
    } else if(this.currentPath == 'renewals'){
      this.currentPath = 'Renewal Dashboard'
    } else if(this.currentPath == 'renewalmodifyproposal'){
      this.currentPath = 'Renewal Modify Proposal'
    } else if(this.currentPath == 'ncbDashboard'){
      this.currentPath = 'NCB Dashboard'
    }
  }

  goToDashboard() {
    if (this.view == "MVIEW") {
      swal({
        text: "Do you want to exit from current screen ?",
        dangerMode: true,
        closeOnClickOutside: false,
        buttons: ["Yes", "No"],
      }).then((willDelete) => {
        if (willDelete) {
          console.log("Continue");
        } else {
          console.log("Go Back");
          location.href = "redirect1.html";
        }
      });
    } else {
      let type;
      if (this.policyType == "NEW") {
        type = "N";
      } else {
        type = "R";
      }
      this.cs.geToDashboard();
      this.cs.clearLocalStorage();
      this.cs.clearQuoteDate();
      this.cs.clearPropData();
    }
  }

  gotoHome() {
    localStorage.setItem("getVehiclDetalsClicked", "true");
    this.router.navigateByUrl("/quote").then(() => {
      this.cs.clearLocalStorage();
      this.cs.clearQuoteDate();
      this.cs.clearPropData();
      // window.location.reload();
      this.router
        .navigateByUrl("/", { skipLocationChange: true })
        .then(() => this.router.navigate(["/quote"]));
    });
  }

  switchLanguage(val) {
    swal({
      closeOnClickOutside: false,
      text: "Do you want it to save as your preffered language",
      buttons: ["No", "Yes"],
    }).then((value) => {
      if (value == true) {
        this.prefferedLanguage(val);
        this.cs.setLanguage(val);
        this.switchLang.translate.use(val);
      } else {
        this.cs.setLanguage(val);
        this.switchLang.translate.use(val);
      }
    });
  }

  loadLanguage() {
    this.lang = this.switchLang.translate.getLangs();
    const browserLang = this.switchLang.translate.getBrowserLang();
    this.switchLang.translate.use(this.language);
  }

  goToLogin() {
    this.cs.geToLogin();
    localStorage.removeItem("calQuoteReq");
    localStorage.removeItem("calQuoteReq4w");
    localStorage.removeItem("calQuoteRes");
    localStorage.removeItem("calQuoteRes4w");
    localStorage.removeItem("dataforBack");
    localStorage.removeItem("PropDatareq");
    localStorage.removeItem("PropDatares");
    localStorage.removeItem("propdataforBack");
    localStorage.removeItem("orderReq");
    localStorage.removeItem("orderRes");
    localStorage.removeItem("PFReq");
    localStorage.removeItem("PFRes");
    // localStorage.removeItem("EVQuoteReq");
    // localStorage.removeItem("EVQuoteRes");
    // localStorage.removeItem("scpaQuoteReq");
    // localStorage.removeItem("scpaQuoteRes");
    // localStorage.removeItem("scpaPropDatareq");
    // localStorage.removeItem("scpaPropDatares");
    // localStorage.removeItem("EVPropDatareq");
    // localStorage.removeItem("EVPropDatares");
  }

  goToIpartnerDashboard() {
    if (this.view == "MVIEW") {
      swal({
        text: "Do you want to exit from current screen ?",
        dangerMode: true,
        closeOnClickOutside: false,
        buttons: ["Yes", "No"],
      }).then((willDelete) => {
        if (willDelete) {
          console.log("Continue");
        } else {
          console.log("Go Back");
          location.href = "redirect1.html";
        }
      });
    } else {
      let type;
      if (this.policyType == "NEW") {
        type = "N";
      } else {
        type = "R";
      }
      this.cs.geToDashboard();
      this.cs.clearLocalStorage();
      this.cs.clearQuoteDate();
      this.cs.clearPropData();
    }
  }

  // prefferedLanguage(val?) {
  //   this.body.IsPrefferedLanguage = true;
  //   if (val && (val != null || val != '')) {
  //     this.body.Language = val;
  //   }
  //   this.cs.post('Language/SetUserLanguage', this.body).then(res => {
  //     let newObj = JSON.parse(JSON.stringify(res));
  //     this.language = newObj.output;
  //     if (this.language == '' || this.language == null || this.language == undefined) {
  //       this.language = 'English';
  //     }
  //     this.loadLanguage();
  //   });
  // }

  /**
   * Removed Language/SetUserLanguage api call. Now we are storing the preffered language string in local Storage just to manage preffered language on page reload
   * Author :- Sumit
   * @param val
   */

  prefferedLanguage(val?) {
    this.body.IsPrefferedLanguage = true;
    if (val && (val != null || val != "")) {
      this.body.Language = val;
      let encryptedLanguageVal = this.api.encrypt(val);
      localStorage.setItem("UserLanguage", encryptedLanguageVal);
    } else {
      if (localStorage.getItem("UserLanguage")) {
        let languageValue = localStorage.getItem("UserLanguage");
        let decryptedLanguageVal = JSON.parse(this.api.decrypt(languageValue));
        val = decryptedLanguageVal;
      } else {
        val = "English";
        let encryptedLanguageVal = this.api.encrypt(val);
        localStorage.setItem("UserLanguage", encryptedLanguageVal);
      }
    }

    // this.cs.post("Language/SetUserLanguage", this.body).then((res) => {
    //   let newObj = JSON.parse(JSON.stringify(res));
    this.language = val;
    if (
      this.language == "" ||
      this.language == null ||
      this.language == undefined
    ) {
      this.language = "English";
    }
    this.loadLanguage();
    // });
  }
}
