import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PhviewdocumentComponent } from "./phviewdocument.component";

const routes: Routes = [
  {
    path: "",
    component: PhviewdocumentComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PhviewdocumentRoutingModule {}
