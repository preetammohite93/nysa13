import { Component, OnInit } from "@angular/core";
import * as moment from "moment";
import { CommonService } from "src/app/services/common.service";
import swal from "sweetalert";
import { SendMessageService } from "src/app/services/send-message.service";
import { Router } from "@angular/router";
import { map } from "rxjs/operators";

@Component({
  selector: "app-phviewdocument",
  templateUrl: "./phviewdocument.component.html",
  styleUrls: ["./phviewdocument.component.css"],
})
export class PhviewdocumentComponent implements OnInit {
  documentdata: any;
  documentdetails: any;
  imageData: any;
  approvalremarks = "";
  qcstatus: any;
  qcapproval: any = "1";
  username: any;
  qcremark: any;
  documentName: any;
  pendencyRemarksArr: any;
  pendencyRemarksArrlen: any;

  constructor(
    private cs: CommonService,
    private messageService: SendMessageService,
    private router: Router
  ) {}

  ngOnInit() {
    let detail = JSON.parse(localStorage.getItem("phdetails"));
    this.username = detail.username;
    this.qcremark = "Docs not uploaded";

    if (localStorage.getItem("documentdata")) {
      this.documentdata = JSON.parse(localStorage.getItem("documentdata"));
      this.pendencyRemarksArr = this.documentdata.pendencyRemarks.sort(
        (n1, n2) => {
          return n1.pendencyID - n2.pendencyID;
        }
      );
      console.log("Sorted", this.pendencyRemarksArr);
      this.pendencyRemarksArrlen = this.pendencyRemarksArr.length;
      console.log(this.documentdata);
    } else {
      // this.messageService.Receievephdata().subscribe((data:any) => {
      //   console.log(data)
      //   this.documentdata = data;
      //   localStorage.setItem('documentdata', JSON.stringify(data));
      // })
    }
    this.getQcCases();
  }

  getQcCases() {
    let body = {
      PrefixKey: this.documentdata.docKey,
    };
    let str = JSON.stringify(body);
    this.cs
      .postPHT("GetDocumentsByKey", str)
      .then((res: any) => {
        if (res.status == "Success") {
          // this.documentdetails = res.files;
          this.documentdetails = res.files.sort(
            (a, b) =>
              new Date(b.docDate).getTime() - new Date(a.docDate).getTime()
          );
          for (let i = 0; i <= this.documentdetails.length; i++) {
            let splitFilename = this.documentdetails[i].fileName.split(".");
            switch (splitFilename[0]) {
              case "RC_Book_Copy":
                this.documentdetails[i].fileName = "RC Book Copy";
                break;
              case "approval_mails":
                this.documentdetails[i].fileName = "Approval Mails (Sales/UW)";
                break;
              case "Previous_Policy_Insurance_Copy":
                this.documentdetails[i].fileName =
                  "Previous Policy Insurance Copy";
                break;
              case "other_doc":
                this.documentdetails[i].fileName = "Other Documents";
                break;
              case "pan_card_copy":
                this.documentdetails[i].fileName = "Pan Card Copy";
                break;
              default:
                break;
            }
          }
        }
      })
      .catch((err: any) => {});
  }

  logoffPht() {
    this.router.navigateByUrl("PHLogin");
  }
  gobacktodash() {
    this.router.navigateByUrl("PHDashboard");
  }

  getimage(data: any) {
    console.log(data);
    this.imageData = data;
  }

  qcStatus(appr: any) {
    if (appr == "vehicle_1") {
      this.qcapproval = "1";
    } else if (appr == "vehicle_2") {
      this.qcapproval = "2";
    }
    console.log(this.qcapproval);
  }

  approvedocument() {
    let body = {
      ProposalNo: this.documentdata.proposalNo,
      QCStatus: this.qcapproval,
      Remark: this.qcremark,
      key: this.documentdata.docKey,
      StandardRemark: this.approvalremarks,
      PendencyRemarks: {
        DocID: this.documentdata.docID,
        PendencyRemark: this.approvalremarks,
      },
    };
    let str = JSON.stringify(body);
    this.cs
      .postPHT("PHAdmin/ApproveDocument", str)
      .then((res: any) => {
        if (res.status == "Success") {
          this.router.navigateByUrl("PHDashboard");
        }
      })
      .catch((err: any) => {});
  }

  saveFile(url, filename) {
    // const downloadLink = document.createElement("a");
    // downloadLink.style.display = "none";
    // document.body.appendChild(downloadLink);
    // downloadLink.setAttribute("href", url);
    // downloadLink.setAttribute("download", filename);
    // downloadLink.click();
    // document.body.removeChild(downloadLink);

    var link = document.createElement("a");
    link.href = url;
    link.download = url.substr(url.lastIndexOf("/") + 1);
    link.click();

    // var xhr = new XMLHttpRequest();
    // xhr.open("GET", url, true);
    // xhr.responseType = "blob";
    // xhr.onload = function(){
    //   var urlCreator = window.URL;
    //   var imageUrl = urlCreator.createObjectURL(this.response);
    //   var tag = document.createElement('a');
    //   tag.href = imageUrl;
    //   tag.download = filename;
    //   document.body.appendChild(tag);
    //   tag.click();
    //   document.body.removeChild(tag);
    // }
    // xhr.send();
  }

  remarkdropdown() {
    this.qcremark;
    console.log(this.qcremark);
  }
}
