import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PhviewdocumentRoutingModule } from "./phviewdocument-routing.module";
import { PhviewdocumentComponent } from "./phviewdocument.component";

@NgModule({
  declarations: [PhviewdocumentComponent],
  imports: [CommonModule, PhviewdocumentRoutingModule],
})
export class PhviewdocumentModule {}
