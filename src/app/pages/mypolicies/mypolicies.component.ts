import { Component, OnInit } from "@angular/core";
import swal from "sweetalert";
import * as moment from "moment";
import { commonData } from "../../commonData/commonData";
import { CommonService } from "src/app/services/common.service";
import { Router } from "@angular/router";
import { ApiServiceService } from "src/app/services/api-service.service";
import { Guid } from "guid-typescript";

@Component({
  selector: "app-mypolicies",
  templateUrl: "./mypolicies.component.html",
  styleUrls: ["./mypolicies.component.css"],
})
export class MypoliciesComponent implements OnInit {
  nysaPolicy: any;
  custid: any;
  pdfResponse: any;
  policyno: any;
  policyStartDate: any;
  norecordflag: any;
  ProductType: any;
  term: any;
  page: any;
  memmaxDOB: any;
  paramDataValues: any;
  spinpolicyloader: boolean = false;
  spinloaderpolicy: boolean = false;
  id: any;
  // buttonDisable:boolean = false;
  policyEndDate: any;
  dateDiffArray = [];
  customerId: any;

  nysapolicyDetails = {
    FromDate: "",
    ToDate: "",
  };

  constructor(
    public router: Router,
    public cs: CommonService,
    public api: ApiServiceService
  ) {}

  ngOnInit() {
    this.ProductType = "1";
    // Maxdate logic
    this.memmaxDOB = new Date();
    this.memmaxDOB.setDate(this.memmaxDOB.getDate() + 0);
    let startDate = moment(new Date()).format("YYYY-MM-DD");
    this.nysapolicyDetails.FromDate = startDate;
    this.nysapolicyDetails.ToDate = startDate;
    this.paramDataValues = JSON.parse(localStorage.getItem("paramData"));
  }

  validate() {
    if (
      this.nysapolicyDetails.FromDate == null ||
      this.nysapolicyDetails.FromDate == undefined ||
      this.nysapolicyDetails.FromDate == ""
    ) {
      swal({ text: "Kindly insert from date" });
    } else if (
      this.nysapolicyDetails.ToDate == null ||
      this.nysapolicyDetails.ToDate == undefined ||
      this.nysapolicyDetails.ToDate == ""
    ) {
      swal({ text: "Kindly insert to date" });
    } else {
      this.getpolicies();
    }
  }

  vehicletype(ev: any) {
    if (ev.target.id == "polselectVehicle_twoWheeler") {
      this.ProductType = "1";
      this.nysaPolicy = "";
    } else if (ev.target.id == "polselectVehicle_fourWheeler") {
      this.ProductType = "2";
      this.nysaPolicy = "";
    } else if (ev.target.id == "polselectVehicle_twoWheelerTP") {
      this.ProductType = "9";
      this.nysaPolicy = "";
    } else if (ev.target.id == "polselectVehicle_fourWheelerTP") {
      this.ProductType = "10";
      this.nysaPolicy = "";
    } else if (ev.target.id == "polselectVehicle_twoWheelerOD") {
      this.ProductType = "12";
      this.nysaPolicy = "";
    } else if (ev.target.id == "polselectVehicle_fourWheelerOD") {
      this.ProductType = "13";
      this.nysaPolicy = "";
    } else if (ev.target.id == "polselectVehicle_allriskElectricBike") {
      this.ProductType = "11";   // All Risk Electric Bike - Sejal - 5-4-2022
      this.nysaPolicy = "";
    }else if(ev.target.id == "polselectVehicle_SCPA") {
      this.ProductType = "11";   // SCPA
      this.nysaPolicy = "";
    }
  }

  // getPolicies
  getpolicies() {
    let from: any;
    let to: any;
    from = moment(this.nysapolicyDetails.FromDate);
    to = moment(this.nysapolicyDetails.ToDate);
    let diff = to.diff(from, "days");
    if (diff < "0") {
      swal({ text: "To Date cannot be less than from date" });
    } else {
      // this.cs.loaderStatus = true;
      this.spinpolicyloader = true;
      let body;
      body = {
        FromDate: moment(this.nysapolicyDetails.FromDate).format("MM/DD/YYYY"), //"4/12/2020",
        ToDate: moment(this.nysapolicyDetails.ToDate)
          .add(1, "days")
          .format("MM/DD/YYYY"), // "4/14/2020",
        ProductType: "1",
        SubProductType: this.ProductType,
        // "IsSubagent": null,
        // "SubagentIpartnerUserID": null
      };
      if (
        this.paramDataValues.iPartnerLogin.isSubagent != null ||
        this.paramDataValues.iPartnerLogin.isSubagent != undefined
      ) {
        body.IsSubagent = true;
        body.SubagentIpartnerUserID = this.paramDataValues.iPartnerLogin.subAID;
      }
      let str = JSON.stringify(body);
      this.cs.post("Policy/GetMyPolicies", str).then((res: any) => {
        if (res.status == null) {
          this.norecordflag = true;
        } else {
          this.norecordflag = false;
          this.nysaPolicy = res.policyDetails;
          // let currentDate = new Date();
          // this.nysaPolicy.forEach((policy:any) => {
          //   let dateSent = new Date(moment(policy.policyStartDate,'DD/MM/YYYY').format('YYYY/MM/DD'));
          //   let difference =  Math.floor((Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()) - Date.UTC(dateSent.getFullYear(), dateSent.getMonth(), dateSent.getDate()) ) /(1000 * 60 * 60 * 24));
          //   console.log(policy.policyStartDate, new Date(moment(policy.policyStartDate,'DD/MM/YYYY').format('YYYY/MM/DD')), difference);
          //   this.dateDiffArray.push(difference);
          // });
          // console.log(this.dateDiffArray);
        }
        // this.cs.loaderStatus = false;
        this.spinpolicyloader = false;
      });
    }
  }

  fetch(ev: any) {
    this.policyno = ev.policyNo;
    this.customerId = ev.pfCustomerID;
    this.showPolicy();
    // this.policyEndDate = new Date;
    // let dd=this.policyEndDate.getDate();
    // let dd1=this.policyEndDate.getDate()+5;
    // if()
  }

  // Download PDF Policy
  showPolicy() {
    // this.cs.loaderStatus = true;
    this.id = Guid.raw();
    this.spinloaderpolicy = true;
    this.cs
      .get(
        "PolicySchedule/GeneratePolicySchedule?policyNo=" +
          this.policyno +
          "&correlationID=" +
          this.id +
          "&customerId=" +
          this.customerId
      )
      .subscribe(
        (res) => {
          this.pdfResponse = res;
          if (
            this.pdfResponse != null &&
            this.pdfResponse != undefined &&
            this.pdfResponse.status == "SUCCESS"
          ) {
            var fileName = this.policyno.split("/").join("_") + ".pdf";
            this.cs.save(
              "data:application/pdf;base64," + this.pdfResponse.buffer,
              fileName
            );
            // this.cs.loaderStatus = false;
            this.spinloaderpolicy = false;
          } else {
            swal({
              text: "Try fetching Policy pdf from My policies dashboard after sometime",
            });
            let errorbody = {
              RequestJson: this.policyno,
              ResponseJson: JSON.stringify(res),
              ServiceURL:
                "PolicySchedule/GeneratePolicySchedule?policyNo=" +
                this.policyno,
              CorrelationID: this.id,
            };
            this.api.adderrorlogs(errorbody);
          }
          this.spinloaderpolicy = false;
          // this.cs.loaderStatus = false;
        },
        (err) => {
          swal({
            text: "Try fetching Policy pdf from My policies dashboard after sometime",
          });
          // this.cs.loaderStatus = false;
          this.spinloaderpolicy = false;
          document.getElementById("myNav4").style.height = "0%";
        }
      );
  }
}
