import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { SwapComponent } from "../app/pages/swap/swap.component";
import { HeaderComponent } from "../app/pages/header/header.component";
import { MultipaymentComponent } from "./pages/multipayment/multipayment.component";
import { ProposalComponent } from "../app/pages/proposal/proposal.component";
import { PaymentComponent } from "../app/pages/payment/payment.component";
import { PaymentConfirmationComponent } from "../app/pages/payment-confirmation/payment-confirmation.component";
import { PendingConfirmationComponent } from "../app/pages/pending-confirmation/pending-confirmation.component";
import { CustPaymentComponent } from "../app/pages/cust-payment/cust-payment.component";
import { ClaimsComponent } from "./pages/claims/claims.component";
import { RedirectionComponent } from "./pages/redirection/redirection.component";
import { NysaloginComponent } from "./pages/nysalogin/nysalogin.component";
import { MypoliciesComponent } from "./pages/mypolicies/mypolicies.component";
import { BreadcrumbsComponent } from "./pages/breadcrumbs/breadcrumbs.component";
import { PlutusPaymentConfirmationComponent } from "./pages/plutus-payment-confirmation/plutus-payment-confirmation.component";
import { PriceBreakUPComponent } from "./pages/price-break-up/price-break-up.component";
import { CustlinkbreakupComponent } from "./pages/custlinkbreakup/custlinkbreakup.component";
import { CreateBreakInComponent } from "./pages/create-break-in/create-break-in.component";
import { BreakInDashComponent } from "./pages/break-in-dash/break-in-dash.component";
import { TechopsLoginComponent } from './pages/techops-login/techops-login.component';
import { PHLoginComponent } from './pages/phlogin/phlogin.component';
import { PhDashboardComponent } from './pages/ph-dashboard/ph-dashboard.component';
import { PhviewdocumentComponent } from './pages/phviewdocument/phviewdocument.component';
import { RenewalModifyComponent } from './pages/renewal-modify/renewal-modify.component';
import { RenewalDashboardComponent } from './pages/renewal-dashboard/renewal-dashboard.component';
import { RenewalPDFComponent } from './pages/renewal-pdf/renewal-pdf.component';
import { NcbDashboardComponent } from './pages/ncb-dashboard/ncb-dashboard.component';
import { NcbPaymentComponent } from './pages/ncb-payment/ncb-payment.component';
import { AllriskCustPaymentComponent } from './pages/allrisk-cust-payment/allrisk-cust-payment.component';


const routes: Routes = [
  { path: "login", component: NysaloginComponent },
  { path: "header", component: HeaderComponent },
  { path: "multipayment", component: MultipaymentComponent },
  {
    path: "quote",
    loadChildren: () =>
      import("./pages/quote/quote.module").then((mod) => mod.QuoteModule),
  },
  { path: "proposal", component: ProposalComponent },
  { path: "payment", component: PaymentComponent },
  { path: "payment-confirmation", component: PaymentConfirmationComponent },
  { path: "payment/confirm", component: PlutusPaymentConfirmationComponent },
  { path: "pending-confirmation", component: PendingConfirmationComponent },
  { path: "cust-payment", component: CustPaymentComponent },
  { path: "createBreakIN", component: CreateBreakInComponent },
  { path: "breakINDashboard", component: BreakInDashComponent },
  { path: "price-BreakUp", component: PriceBreakUPComponent },
  { path: "custlinkbreakup", component: CustlinkbreakupComponent },
  { path: "nysapol", component: MypoliciesComponent },
  { path: "swap", component: SwapComponent },
  { path: "redirection", component: RedirectionComponent },
  { path: "claims", component: ClaimsComponent },
  {
    path: "lombardpaywallet",
    loadChildren: () =>
      import("./pages/lombardpay-wallet/lombardpay-wallet.module").then(
        (mod) => mod.LombardpayWalletModule
      ),
  },
  { path: "breadcrumb", component: BreadcrumbsComponent },
  {
    path: "claims",
    loadChildren: () =>
      import("./pages/claims/claims.module").then((mod) => mod.ClaimsModule),
  },
  {
    path: "techops/login",
    loadChildren: () =>
      import("./pages/techops-login/techops-login.module").then(
        (mod) => mod.TechopsLoginModule
      ),
  },
  {
    path: "dtm/login",
    loadChildren: () =>
      import("./pages/techops-login/techops-login.module").then(
        (mod) => mod.TechopsLoginModule
      ),
  },
  {
    path: "PHLogin",
    loadChildren: () =>
      import("./pages/phlogin/phlogin.module").then((mod) => mod.PhloginModule),
  },
  {
    path: "PHDashboard",
    loadChildren: () =>
      import("./pages/ph-dashboard/ph-dashboard.module").then(
        (mod) => mod.PhDashboardModule
      ),
  },
  {
    path: "viewdocument",
    loadChildren: () =>
      import("./pages/phviewdocument/phviewdocument.module").then(
        (mod) => mod.PhviewdocumentModule
      ),
  },
  {
    path: "techops",
    loadChildren: () =>
      import("./pages/techop/techop.module").then((mod) => mod.TechopModule),
  },
  { path: 'allrisk-cust-payment', component: AllriskCustPaymentComponent }, 
  { path: 'techops/login', component:TechopsLoginComponent},
  { path: 'dtm/login', component:TechopsLoginComponent},
  { path: 'PHLogin', component:PHLoginComponent},
  { path: 'PHDashboard', component:PhDashboardComponent},
  { path: 'viewdocument', component:PhviewdocumentComponent},
  { path: 'renewalmodifyproposal', component:RenewalModifyComponent},
  { path: 'renewals', component:RenewalDashboardComponent},
  { path: 'renewalPayment', component:RenewalPDFComponent},
  { path: 'ncbDashboard', component:NcbDashboardComponent},
  { path: 'ncbPayment', component:NcbPaymentComponent},
  { path: "", redirectTo: "swap", pathMatch: "full" },
];

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: true,
      relativeLinkResolution: "legacy",
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
